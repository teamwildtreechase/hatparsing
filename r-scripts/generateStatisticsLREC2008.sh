 #!/bin/bash

function generateTreeStatistics 
{
 	echo $1 
	java -jar ../generateTreeStatisticsMultiThread.jar $1 3 
}


function generateSureAndPossibleStatistics 
{
	generateTreeStatistics ${1}configfile-en-fr-SureAndPossible.txt
	generateTreeStatistics ${1}configfile-en-pt-SureAndPossible.txt	
	generateTreeStatistics ${1}configfile-en-sp-SureAndPossible.txt
	generateTreeStatistics ${1}configfile-pt-fr-SureAndPossible.txt
	generateTreeStatistics ${1}configfile-pt-sp-SureAndPossible.txt
	generateTreeStatistics ${1}configfile-sp-fr-SureAndPossible.txt
}

function generateSureOnlyStatistics 
{
	generateTreeStatistics ${1}configfile-en-fr-SureOnly.txt	
	generateTreeStatistics ${1}configfile-en-pt-SureOnly.txt	
	generateTreeStatistics ${1}configfile-en-sp-SureOnly.txt	
	generateTreeStatistics ${1}configfile-pt-fr-SureOnly.txt	
	generateTreeStatistics ${1}configfile-pt-sp-SureOnly.txt	
	generateTreeStatistics ${1}configfile-sp-fr-SureOnly.txt
}

function generateAllLRECStaistics
{	
	generateSureAndPossibleStatistics $1
	generateSureOnlyStatistics $2
}

SureAndPossibleConfigs="../AlignmentsGoldLREC2008/ConfigFiles/IgnoringNonReducableTranslationEquivalents/SureAndPossible/"
SureOnlyConfigs="../AlignmentsGoldLREC2008/ConfigFiles/IgnoringNonReducableTranslationEquivalents/SureOnly/"

echo "Make statistics for the setting were non-reducable translation equivalent nodes are ignored"
generateAllLRECStaistics $SureAndPossibleConfigs $SureOnlyConfigs


mkdir treeStatistics-LREC2008-SureAndPossible-IgnoringNonReducableTranslationEquivalents
mkdir treeStatistics-LREC2008-SureOnly-IgnoringNonReducableTranslationEquivalents
mv treeStatistics*LREC2008-*-SureAndPossible*  treeStatistics-LREC2008-SureAndPossible-IgnoringNonReducableTranslationEquivalents
mv treeStatistics*LREC2008-*-SureOnly*  treeStatistics-LREC2008-SureOnly-IgnoringNonReducableTranslationEquivalents


SureAndPossibleConfigs="../AlignmentsGoldLREC2008/ConfigFiles/ConsideringNonReducableTranslationEquivalents/SureAndPossible/"
SureOnlyConfigs="../AlignmentsGoldLREC2008/ConfigFiles/ConsideringNonReducableTranslationEquivalents/SureOnly/"

echo "Make statistics for the setting were non-reducable translation equivalent nodes are considered"
generateAllLRECStaistics $SureAndPossibleConfigs $SureOnlyConfigs


mkdir treeStatistics-LREC2008-SureAndPossible-ConsideringNonReducableTranslationEquivalents
mkdir treeStatistics-LREC2008-SureOnly-ConsideringNonReducableTranslationEquivalents
mv treeStatistics*LREC2008-*-SureAndPossible*  treeStatistics-LREC2008-SureAndPossible-ConsideringNonReducableTranslationEquivalents
mv treeStatistics*LREC2008-*-SureOnly*  treeStatistics-LREC2008-SureOnly-ConsideringNonReducableTranslationEquivalents

