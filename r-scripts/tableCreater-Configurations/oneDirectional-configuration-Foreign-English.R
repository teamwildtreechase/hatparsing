# Settings for the Europarl corpera
selectedBranchingFactors <- list(2,3,6,8,22)
selectedMaxAtomLengths <- list(1,2,3,5,10,20,40)
languagePairsSentence <-  " Scores are given for three differnt language pairs.";

languagePairs <- list("Dutch-English", "French-English", "German-English");
scoresDescriptionString = paste("Below are the scores for the one-directional aligned europarl corpera, using the GIZA++ A3.final alignments."); 
 
