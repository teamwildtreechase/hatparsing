 #!/bin/bash    


function generateTextAndPDF 
{
 	echo $1 
	echo $2
	R < tableCreater.R --no-save --args $1  $1.tex $2
	cd $1-latex-tables
	pdflatex $1.tex
	cd ..
                
}



# Basic Statistics
generateTextAndPDF treeStatistics-English-Foreign-MaxLength40 ./tableCreater-Configurations/basic-configuration-MaxLength30.R
# Basic statistics with max length 30 constraint
generateTextAndPDF treeStatistics-English-Foreign-MaxLength30 ./tableCreater-Configurations/basic-configuration.R

# Statistics for English-Foreign and Foreign-English OneDirectional
generateTextAndPDF treeStatistics-English-Foreign-OneDirectional ./tableCreater-Configurations/oneDirectional-configuration-English-Foreign.R
generateTextAndPDF treeStatistics-Foreign-English-OneDirectional ./tableCreater-Configurations/oneDirectional-configuration-Foreign-English.R

# Statistics for English-Foreign and Foreign-English OneDirectional with max length 30 constraint
generateTextAndPDF treeStatistics-English-Foreign-OneDirectional-MaxLength30 ./tableCreater-Configurations/oneDirectional-configuration-English-Foreign-MaxLength30.R

generateTextAndPDF treeStatistics-Foreign-English-OneDirectional-MaxLength30 ./tableCreater-Configurations/oneDirectional-configuration-Foreign-English-MaxLength30.R

# Hansard Statistics
generateTextAndPDF treeStatistics-Hansards-SureOnly ./tableCreater-Configurations/hansards-SureOnly-configuration.R
generateTextAndPDF treeStatistics-Hansards-SureAndPossible ./tableCreater-Configurations/hansards-SureAndPossible-configuration.R

