#!/usr/bin/env RscriptranslationEquivalentsCoverageTableCaption

#USAGE : 
#  R < tableCreater.R --no-save --args TREESTATISTICS_DATA_FOLDER_NAME TABLES_OUTPUT_NAME CONFIGURATION_FILE_NAME
# i.e. fsadind
#
# e.g: R < tableCreater.R --no-save --args ./NewStyleStatistics/treeStatistics-Hansards latex-tables.tex ./tableCreater-Configurations/hansards-SureAndPossible-configuration.R


# Read the configuration file, which specfies the max branching factors and max atom lengths used in the tables as well 
# ass the used language pair Strings and the scoreDescriptionString
source(commandArgs(T)[[3]]);

readData <- function(inputFolder, inputFileName)
{
	#print(list.files(inputFolder));

 	fullInputFileName <- paste(inputFolder,"/", inputFileName, sep ="");
	print(paste(">>>Reading data from ", fullInputFileName));
	data <- read.table(fullInputFileName, header=T, sep=",");
	print(">>>read");
	return(data); 
}



createScoreSTDDataList <-function(mainFolder,inputFolders, scoresFileName,stdsFileName)
{

	print(paste("\n\n!!!!!!!!!! length(inputFolders)", length(inputFolders))); 
	scoreSTDDataList <- list(list("matrix"));

	print(scoreSTDDataList);

	i <- 1;
	for(inputFolder in inputFolders)
	{
		scoreMatrices <-  list(list("matrix"),2);
		scoreMatrix <- list(matrix=readData(simplePaste(mainFolder,inputFolder),scoresFileName));
		stdMatrix  <- list(matrix=readData(simplePaste(mainFolder,inputFolder),stdsFileName));
		scoreMatrices[1] <- scoreMatrix;
		scoreMatrices[2] <- stdMatrix;			
		scoreSTDDataList[[i]] <- scoreMatrices;
		
		i <- i + 1;	
	
		print(scoreSTDDataList);		

	}

	

	return(scoreSTDDataList);
}

createScoreDataList <-function(mainFolder,inputFolders, scoresFileName)
{
	scoreDataList <- list(list("matrix"));


	i <- 1;
	for(inputFolder in inputFolders)
	{
		print(paste("mainFolder : ", mainFolder));
		print(paste("inputFolder : ", inputFolder));

		scoreDataList[[i]] <- list(matrix=readData(simplePaste(mainFolder,inputFolder),scoresFileName));
		i <- i + 1;	
	}

	print(scoreDataList);
	return(scoreDataList)
}





#getBITTScoreString <- function(scoreDataList, scoreType, maxAtomLength)
#{
#	return(getScoreString(scoreDataList, scoreType, 1, maxAtomLength));
#}


getBITGCoverageString <- function(scoreDataList, scoreType, maxAtomLength)
{
	return(getScoreString(scoreDataList, scoreType, maxAtomLength, 3));
}



getScoreString <- function(scoreDataList, scoreType, i, j)
{
	scoreDataMatrix <- scoreDataList[[1]];

	score <- scoreDataMatrix[i,j];

	if(is.na(score)) # If the score is missing
	{
		# Note: this is an ugly hack to deal with missing scores
		scoreString <- "100.00\\%";
	}
	else if (regexpr("BinarizationCorpusScores", scoreType) > 0)
	{
		scoreString <- sprintf("%.2f",score);
	}
	else if (regexpr("BinarizationScores", scoreType) > 0)
	{
		stdDataMatrix <- scoreDataList[[2]];	
		standardDeviation <- stdDataMatrix[i,j];
		scoreString <- sprintf("%.2f$\\pm$%.2f",score, standardDeviation);
	}
	else	
	{
		scoreString <- paste(sprintf("%.2f",score), "\\%", sep = "");
	}

	return(scoreString);
}

writeOneColumnPerLanguageTableStructure <- function(resultFileName)
{
	write("\\begin{table}\n\\small\n\\begin{center}", file = resultFileName, append = TRUE);

	tabularString <- "\\begin{tabular}{|l|";
	for(i in 1 : (length(getLanguagePairs()) -1))
	{
		tabularString <- paste(tabularString, "c|", sep = "");
	}
	tabularString <- paste(tabularString, "r|}\n\t\\hline", sep = ""); 
	write(tabularString, file = resultFileName, append = TRUE);		


	tableRow <- " ";
	for(languagePair in getLanguagePairs())
	{
		tableRow <- paste(tableRow,"&", languagePair, sep = " ");	
	}
	tableRow <- paste(tableRow," \\\\ \\hline");
	write(tableRow, file = resultFileName, append = TRUE);	
}


writeOneColumnPerLanguageTableEnding <- function(resultFileName, tableCaption, tableLabel)
{
	# Write extra hline
	tableRow <- paste("\t", "\\hline",  sep = "");
	write(tableRow, file = resultFileName, append = TRUE);		
	write("\\end{tabular}", file = resultFileName, append = TRUE);
	write(paste("\\caption{",tableCaption,"}"),file = resultFileName, append = TRUE); 
	write(paste("\\label{",tableLabel,"}"),file = resultFileName, append = TRUE); 
	write("\\end{center}\n\\end{table}", file = resultFileName, append = TRUE);
}
getScoreDataList <- function(mainFolder,inputFolders, scoresFileName,stdsFileName, scoreType)
{
	if(hasSTDs(scoreType))
	{
		return(createScoreSTDDataList(mainFolder,inputFolders, scoresFileName,stdsFileName));
	}
	else
	{
		return(createScoreDataList(mainFolder,inputFolders, scoresFileName));
	}
}



writeBITTResultTables <-function(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel)
{
	writeOneDimensionalResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, selectedMaxAtomLengths,"alpha","row",0)
}

writeOneDimensionalCoverageTable <-function(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, orientation,branchingFactors)
{
	writeOneDimensionalResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, branchingFactors,"beta", orientation,0)

}

writeOneDimensionalExtraAlignmentsCoverageTable <-function(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, orientation,extraAlignments)
{
	
	writeOneDimensionalResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, extraAlignments,"epsilon", orientation,1)

}


getScoreStringForOrientation <- function(scoreData, scoreType, selectedValue, orientation)
{
	if (regexpr("row", orientation) > 0)
	{
		return(getScoreString(scoreData, scoreType, 1, selectedValue));
	}
	else if(regexpr("bitg", orientation) > 0)
	{
		# The first column contains labels and should be skipped
		return(getScoreString(scoreData, scoreType, selectedValue,3));
	}
	else
	{
		# The first column contains labels and should be skipped
		return(getScoreString(scoreData, scoreType, selectedValue,2));
	}
}


createLanguageScoresRow <- function(scoreDataList,scoreType,col,row)
{
	i <- 1;
	result <- "";
	for(scoreData in scoreDataList)
	{
		print(paste("i: " ,i));
		print(paste("Language pair", getLanguagePairs()[[i]]));

		scoreString <- getScoreString(scoreData, scoreType, col,row);
		
		print(paste("scoreString : ", scoreString));
		i <- i +1;

		result <- paste(result, " & ",scoreString ,  sep = "");
	}

	# Add latex end of table line markers
	result <- paste(result," \\\\ \\hline");	

	return(result);
}	

writeOneDimensionalResultTables <-function(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel, selectedValues, parameterValue, orientation,valueIndexOffSet)
{
	
	scoreDataList <- getScoreDataList(mainFolder,inputFolders, scoresFileName,stdsFileName, scoreType);	

	writeOneColumnPerLanguageTableStructure(resultFileName);

	for(selectedValue in selectedValues)
	{
		tableRow <- paste("\t","$\\",parameterValue,"_{max} = ",selectedValue,"$",  sep = "");

		print(paste("inputFolders:", inputFolders));

		if(regexpr("row", orientation) > 0)
		{
			tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,scoreType,1,(selectedValue + valueIndexOffSet))); 
		}
		else
		{
			tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,scoreType,(selectedValue + valueIndexOffSet),2)); 
		}

		print(tableRow);
	
		cat(tableRow); 
	
		write(tableRow, file = resultFileName, append = TRUE);	
	}

	
	writeOneColumnPerLanguageTableEnding(resultFileName,tableCaption, tableLabel);


}

writeBITGCoverageTables <-function(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, scoreType, tableCaption, tableLabel)
{
	scoreDataList <- getScoreDataList(mainFolder,inputFolders, scoresFileName,stdsFileName, scoreType);

	writeOneColumnPerLanguageTableStructure(resultFileName);

	for(maxAtomLength in selectedMaxAtomLengths)
	{
		tableRow <- paste("\t","$\\alpha_{max} = ",maxAtomLength,"$",  sep = "");


		print(paste("inputFolders:", inputFolders));

		tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,scoreType,maxAtomLength,3));
			
		cat(tableRow); 
	
		write(tableRow, file = resultFileName, append = TRUE);	

	}

	writeOneColumnPerLanguageTableEnding(resultFileName,tableCaption, tableLabel);
}

writeTreeTypeCoverageTables <-function(mainFolder,inputFolders, scoresFileName, resultFileName,tableCaption, tableLabel)
{
	scoreDataList <- getScoreDataList(mainFolder,inputFolders, scoresFileName,stdsFileName, "");

	writeOneColumnPerLanguageTableStructure(resultFileName);

	tableRow <- paste("\t","$BITTs$",  sep = "");
	tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,"",1,3));
	write(tableRow, file = resultFileName, append = TRUE);	

	tableRow <- paste("\t","$BITTs \\cup PETs$",  sep = "");
	tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,"",1,5));
	write(tableRow, file = resultFileName, append = TRUE);	

	tableRow <- paste("\t","$BITTs \\cup PETs \\cup HATs$",  sep = "");
	tableRow <- paste(tableRow,createLanguageScoresRow(scoreDataList,"",1,7));
	write(tableRow, file = resultFileName, append = TRUE);	

	writeOneColumnPerLanguageTableEnding(resultFileName,tableCaption, tableLabel);
}


simplePaste <- function(mainFolder,inputFolder)
{

	return(paste(mainFolder, "/", inputFolder, sep = ""));
}

hasSTDs <- function(scoreType)
{
	return (regexpr("BinarizationScores", scoreType) > 0);
}

writeResultSubTable <- function(inputFolder,scoresFileName,stdsFileName, languagePair, resultFileName, scoreType)
{
	
	print(paste("inputFolder : ", inputFolder));
	print(paste("scoresFileName : ", scoresFileName));
	print(paste("stdsFileName : ", stdsFileName));

	scoresData <- readData(inputFolder,scoresFileName);
	
	if(hasSTDs(scoreType))
	{	print(paste("scoreType: ", scoreType));	
		stdsData <- readData(inputFolder,stdsFileName);
	}
	
		

	noRows <- dim(scoresData)[1];
	noColumns <- dim(scoresData)[2];
	
	# Write a header line for the language pair	
	noTableColumns <- length(selectedBranchingFactors) + 1;
	tableRow <- paste("\t \\multicolumn{",noTableColumns, "}{|c|}{", languagePair,"} \\\\ \\hline",  sep = "");
	write(tableRow, file = resultFileName, append = TRUE);
	
	
	for(maxAtomLength in selectedMaxAtomLengths)
		{
		# Add a column header
		tableRow <- paste("\t","$\\alpha_{max} = ",maxAtomLength,"$",  sep = "");
		
		#print(tableRow); 
	
		for(branchingFactor in selectedBranchingFactors)
		{	
			# We must add 1 to maxAtomLenght because the first column contains labels and not data
			score =  scoresData[branchingFactor,maxAtomLength + 1];


			if(is.na(score)) # If the score is missing
			{
				# Note: this is an ugly hack to deal with missing scores
				scoreString <- "100.00\\%";
			}
			else if (regexpr("BinarizationCorpusScores", scoreType) > 0)
			{
				scoreString <- sprintf("%.2f",score);
			}
			else if (regexpr("BinarizationScores", scoreType) > 0)
			{
				standardDeviation <- stdsData[branchingFactor,maxAtomLength + 1];
				scoreString <- sprintf("%.2f$\\pm$%.2f",score, standardDeviation);
			}
			else	
			{
				scoreString <- paste(sprintf("%.2f",score), "\\%", sep = "");
			}

			tableRow <- paste(tableRow, " & ",scoreString ,  sep = "");
		
		}
		# Add latex end of table line markers
		tableRow <- paste(tableRow," \\\\ \\hline");
	
		cat(tableRow); 
	
		write(tableRow, file = resultFileName, append = TRUE);
	}

	# Write extra hline
	tableRow <- paste("\t", "\\hline",  sep = "");
	write(tableRow, file = resultFileName, append = TRUE);
	

}


writeResultTableStructure <- function(resultFileName)
{
	write("\\begin{table}\n\\small\n\\begin{center}", file = resultFileName, append = TRUE);

	tabularString <- "\\begin{tabular}{|l|";
	for(i in 1 : (length(selectedBranchingFactors) -1))
	{
		tabularString <- paste(tabularString, "c|", sep = "");
	}
	tabularString <- paste(tabularString, "r|}\n\t\\hline", sep = ""); 
	
	rowHeaders = "\t";
	for(i in 1 : length(selectedBranchingFactors))
	{
		rowHeaders <- paste(rowHeaders, " & $\\beta_{max} = ", selectedBranchingFactors[i],"$", sep = "");
	}
	rowHeaders <- paste(rowHeaders," \\\\ \\hline");
	
	
	write(tabularString, file = resultFileName, append = TRUE);
	write(rowHeaders, file = resultFileName, append = TRUE);
}

writeResultTable <- function(mainFolder, inputFolders,scoresFileName,stdsFileName, resultFileName, scoreType ,tableCaption, tableLabel)
{
	writeResultTableStructure(resultFileName);

	for(i in 1 : length(getLanguagePairs()))
	{
		inputFolder = paste(mainFolder, "/", inputFolders[i], sep = "");
		languagePair <- getLanguagePairs()[i];

		writeResultSubTable(inputFolder,scoresFileName,stdsFileName,languagePair, resultFileName, scoreType);
	}
	
	write("\\end{tabular}", file = resultFileName, append = TRUE);
	write(paste("\\caption{",tableCaption,"}"),file = resultFileName, append = TRUE); 
	write(paste("\\label{",tableLabel,"}"),file = resultFileName, append = TRUE); 
	write("\\end{center}\n\\end{table}", file = resultFileName, append = TRUE);
}

writeLatexDocumentHeader <- function(resultFileName)
{
	# Make the file empty to start with by writing empty string with append = FALSE
	write("", file = resultFileName, append = FALSE);
	
	write("\\documentclass[a4paper,10pt]{article}", file = resultFileName, append = TRUE);
	write("\n \\usepackage{placeins}", file = resultFileName, append = TRUE);
	write("\\begin{document}", file = resultFileName, append = TRUE);
	write("\n\n\\section{Score Tables}", file = resultFileName, append = TRUE);
	write("\n\n\\subsection{Results Description}", file = resultFileName, append = TRUE);
	write(scoresDescriptionString, file = resultFileName, append = TRUE);

}

writeLatexDocumentFooter <-function(resultFileName)
{
	write("\\end{document}", file = resultFileName, append = TRUE);
}

writeBinarizationScores <- function(nameSuffix, caption1, label1, caption2, label2)
{

	print("\n\n}}} ========= writeBinarizationScores: ");
	scoresFileName <- paste("binarizationScores",nameSuffix,".csv", sep = "");
	stdsFileName <- paste("binarizationScoresSTDs",nameSuffix,".csv", sep = ""); 
	print(paste("scoresFileName: ", scoresFileName, sep =""));
	print(paste("stdsFileName : " , stdsFileName), sep ="");
	writeResultTable(mainFolder, inputFolders,scoresFileName,stdsFileName, resultFileName, "BinarizationScores", 
	caption1, label1);
	
	
	scoresFileName <-   paste("binarizationCorpusScores",nameSuffix,".csv", sep = "");	print(paste("scoresFileName: ", scoresFileName, sep =""));
	writeResultTable(mainFolder, inputFolders,scoresFileName,stdsFileName, resultFileName, "BinarizationCorpusScores", caption2, label2);
}



writeBITGCoverageTable <- function(resultFileName)
{

	write("\n\\FloatBarrier \n \\subsubsection{Pure BITG Coverage Scores} \n\n", file = resultFileName, append = TRUE);


	# Write the ITG Tables
	scoresFileName <- paste("cumulativeRelativeFrequenciesAllHATs","",".csv", sep = "");
	writeBITGCoverageTables(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "",bittCoverageCaption, bittBinarizationScoreTableLabel);
}


writeBITTTables <- function(resultFileName)
{

	write("\n\\FloatBarrier \n \\subsection{BITT Scores} \n\n", file = resultFileName, append = TRUE);


	# Write the ITG Tables
	scoresFileName <- paste("bittBinarizationScores","",".csv", sep = "");
	stdsFileName <- paste("bittBinarizationScoresSTDs","",".csv", sep = ""); 
	writeBITTResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "BinarizationScores",bittBinarizationScoreTableCaption, bittBinarizationScoreTableLabel);

	scoresFileName <- paste("bittBinarizationCorpusScores","",".csv", sep = "");
	stdsFileName <- paste("bittBinarizationCorpusScoresSTDs","",".csv", sep = ""); 
	writeBITTResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName, resultFileName, "BinarizationCorpusScores",bittCorpusBinarizationScoreTableCaption, bittCorpusBinarizationScoreTableLabel);

	scoresFileName <- paste("bittBinarizationScoresRELATIVE","",".csv", sep = "");
	stdsFileName <- paste("bittBinarizationScoresSTDsRELATIVE","",".csv", sep = ""); 
	writeBITTResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "BinarizationScores",bittBinarizationScoreTableCaptionRELATIVE, bittBinarizationScoreTableLabelRELATIVE);

	scoresFileName <- paste("bittBinarizationCorpusScoresRELATIVE","",".csv", sep = "");
	stdsFileName <- paste("bittBinarizationCorpusScoresSTDsRELATIVE","",".csv", sep = ""); 
	writeBITTResultTables(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "BinarizationCorpusScores",bittCorpusBinarizationScoreTableCaptionRELATIVE, bittCorpusBinarizationScoreTableLabelRELATIVE);


}
 

writeTranslationEquivalentsCoverage  <- function(resultFileName)
{
	# Write the translation equivalents coverage scores
	branchingFactors <- oneDimensionalTableBranchingFactors;
	extraAlignments <- selectedExtraAlignments;
	scoresFileName <- paste("translationEquivalentsCoverageGivenMaxBranchingFactorScores","",".csv", sep = "");
	writeOneDimensionalCoverageTable(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "",translationEquivalentsCoverageGivenMaxBranchingFactorTableCaption, translationEquivalentsCoverageGivenMaxBranchingFactorTableLabel, "row",branchingFactors);

	scoresFileName <- paste("translationEquivalentsCoverageGivenExtraAlignmentsScores","",".csv", sep = "");
	writeOneDimensionalExtraAlignmentsCoverageTable(mainFolder,inputFolders, scoresFileName,stdsFileName,  resultFileName, "",translationEquivalentsCoverageGivenExtraAlignmentsTableCaption, translationEquivalentsCoverageGivenExtraAlignmentsTableLabel, "row",extraAlignments);

}

writeCoverageScores <-function(resultFileName)
{
	branchingFactors <- oneDimensionalTableBranchingFactors;
	extraAlignments <- selectedExtraAlignments;
	writeOneDimensionalCoverageTable(mainFolder, inputFolders,"cumulativeMaxBranchingRelativeFrequencies.csv","", resultFileName, "", oneDimensionalCoverageTableCaption, oneDimensionalCoverageTableLabel,"column",branchingFactors);
	writeOneDimensionalExtraAlignmentsCoverageTable(mainFolder, inputFolders,"coverageWithExtraAlignmentsScores.csv","", resultFileName, "", coverageGivenExtraAlignmentsTableCaption, coverageGivenExtraAlignmentsTableLabel,"row",extraAlignments);
	writeTranslationEquivalentsCoverage(resultFileName);
	writeOneDimensionalCoverageTable(mainFolder, inputFolders,"binarizationCorpusScores.csv","", resultFileName, "", corpusBinarizationScoreTableCaptionBranchingFactorOnly, corpusBinarizationScoreTableLabelBranchingFactorOnly,"column",branchingFactors);
	writeTreeTypeCoverageTables(mainFolder,inputFolders, "cumulativeRelativeFrequenciesAllHATs.csv", resultFileName,treeTypeCoverageTableCaption, treeTypeCoverageTableLabel);
}



coverageTableCaption <- paste("Coverage of the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments} and $\\beta_{max}$, the \\emph{Maximal Branching Factor}.", languagePairsSentence);
coverageTableLabel <- "table:coverageScores";


oneDimensionalCoverageTableCaption <- paste("Coverage of the corpus as a function of $\\beta_{max}$, the \\emph{Maximal Branching Factor}.", languagePairsSentence);
oneDimensionalCoverageTableLabel <- "table:translationEquivalentsCoverage";


translationEquivalentsCoverageGivenMaxBranchingFactorTableCaption <- paste("Translation Equivalents Coverage of the corpus as a function of $\\beta_{max}$, the \\emph{Maximal Branching Factor}.", languagePairsSentence);
translationEquivalentsCoverageGivenMaxBranchingFactorTableLabel <- "table:translationEquivalentsCoverageMaxBranchingFactor";


translationEquivalentsCoverageGivenExtraAlignmentsTableCaption <- paste("Translation Equivalents Coverage of the corpus as a function of $\\epsilon_{max}$, the \\emph{Number of Extra Alignments Beyond Pure Permutation}.", languagePairsSentence);
translationEquivalentsCoverageGivenExtraAlignmentsTableLabel <- "table:translationEquivalentsCoverageExtraAlignments";


coverageGivenExtraAlignmentsTableCaption <- paste("Coverage of the corpus as a function of $\\epsilon_{max}$, the \\emph{Number of Extra Alignments Beyond Pure Permutation}.", languagePairsSentence);
coverageGivenExtraAlignmentsTableLabel <- "table:coverageExtraAlignments";




treeTypeCoverageTableCaption  <- paste("Coverage of the corpus for the different subsets of the full HATs set. ", languagePairsSentence);
treeTypeCoverageTableLabel <- "table:treeTypeCoverage";




stdsSentence <- "Behind the scores the standard deviation is displayed";


# Absolute binarization scores labels and captions


absoluteMainCaptionBranchingFactorOnly <- paste("absolute Binarization scores on the corpus as a function of $\\beta_{max}$, the \\emph{Maximal Branching Factor}. These scores are computed taking the length of the sentence minus one as denominator, i.e. the maximal number of internal nodes if the tree were fully binarizable.", languagePairsSentence);

absoluteMainCaption <- paste("absolute Binarization scores on the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments} and $\\beta_{max}$, the \\emph{Maximal Branching Factor}. These scores are computed taking the length of the sentence minus one as denominator, i.e. the maximal number of internal nodes if the tree were fully binarizable.", languagePairsSentence);

bittCoverageCaption <- paste("Pure BITG coverage scores on the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments}.", languagePairsSentence);

bittAbsoluteMainCaption <- paste("absolute Binarization scores on the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments}. These scores are computed taking the length of the sentence minus one as denominator, i.e. the maximal number of internal nodes if the tree were fully binarizable.", languagePairsSentence);

binarizationScoreTableCaption  <- paste("Sentence level", absoluteMainCaption,stdsSentence);
binarizationScoreTableLabel <- "table:binarizationScores";
corpusBinarizationScoreTableCaption  <- paste("Corpus level", absoluteMainCaption);
corpusBinarizationScoreTableLabel <- "table:corpusBinarizationScores";
corpusBinarizationScoreTableCaptionBranchingFactorOnly <-  paste("Corpus level", absoluteMainCaptionBranchingFactorOnly);
corpusBinarizationScoreTableLabelBranchingFactorOnly <- "table:corpusBinarizationScoresBranchingFactorOnly";


bittBinarizationScoreTableCaption  <- paste("BITT sentence level", bittAbsoluteMainCaption,stdsSentence);
bittBinarizationScoreTableLabel <- "table:bittBinarizationScores";

bittCorpusBinarizationScoreTableCaption  <-  paste("BITT corpus level", bittAbsoluteMainCaption);
bittCorpusBinarizationScoreTableLabel <- "table:bittCorpusBinarizationScores";

# Relative binarization scores labels and captions

relativeMainCaption <- paste("relative Binarization scores on the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments} and $\\beta_{max}$, the \\emph{Maximal Branching Factor}. These scores are computed taking the number of internal nodes for its HAT (with no constraints on branching factor) as denominator. I.e. they show what fraction of the nodes that is in principle extractable is actually extractable given certain constraints on the maximal branching factor.", languagePairsSentence);

bittRelativeMainCaption <- paste("relative Binarization scores on the corpus as a function of $\\alpha_{max}$, the \\emph{Maximum Source Length of Atomic Fragments}. These scores are computed taking the number of internal nodes for its HAT (with no constraints on branching factor or node type) as denominator. I.e. they show what fraction of the nodes that is in principle extractable is actually extractable given certain constraints on the maximal branching factor.", languagePairsSentence);

binarizationScoreTableCaptionRELATIVE  <- paste("Sentence level", relativeMainCaption, stdsSentence);
binarizationScoreTableLabelRELATIVE <- "table:binarizationScoresRELATIVE";

corpusBinarizationScoreTableCaptionRELATIVE  <- paste("Corpus level", relativeMainCaption);
corpusBinarizationScoreTableLabelRELATIVE <- "table:corpusBinarizationScoresRELATIVE";

bittBinarizationScoreTableCaptionRELATIVE  <- paste("BITT sentence level", bittRelativeMainCaption, stdsSentence);
bittBinarizationScoreTableLabelRELATIVE <- "table:bittBinarizationScoresRELATIVE";

bittCorpusBinarizationScoreTableCaptionRELATIVE  <- paste("BITT corpus level", bittRelativeMainCaption);
bittCorpusBinarizationScoreTableLabelRELATIVE <- "table:bittCorpusBinarizationScoresRELATIVE";



getInputFolders <- function(mainFolder)
{
	folderNames <- list.files(mainFolder);
	return(folderNames);
}


getLanguagePairs  <- function() 
{
	languagePairs;	
}

writeBinarizationScoreTables <- function(resultFileName)
{
	write("\n\\FloatBarrier \n \\subsection{Absolute Binarization Scores} \n\n", file = resultFileName, append = TRUE);
	writeBinarizationScores("",binarizationScoreTableCaption,binarizationScoreTableLabel,corpusBinarizationScoreTableCaption,corpusBinarizationScoreTableLabel);
	write("\n\\FloatBarrier \n \\subsection{Relative Binarization Scores} \n\n", file = resultFileName, append = TRUE);
	writeBinarizationScores("RELATIVE",binarizationScoreTableCaptionRELATIVE,binarizationScoreTableLabelRELATIVE,
	corpusBinarizationScoreTableCaptionRELATIVE,corpusBinarizationScoreTableLabelRELATIVE);
}

# STEP 1: Get the file names
mainFolder <- commandArgs(T)[[1]];
print(paste("mainFolder : " , mainFolder));
inputFolders <- getInputFolders(mainFolder);
print(paste("inputFolders:", inputFolders));
outputFolder <- paste(mainFolder, "-latex-tables" , sep ="");
print(paste("outputFolder:", outputFolder));
dir.create(outputFolder);
resultFileName <-  paste(outputFolder, "/", commandArgs(T)[[2]], sep ="");
print(paste("resultFileName:", resultFileName));	

#Step 2: Write headers of the latex file
writeLatexDocumentHeader(resultFileName);
#Step 3: Write the tables

write("\n\\FloatBarrier \n \\section{Selected Scores For Paper} \n\n", file = resultFileName, append = TRUE);
writeCoverageScores(resultFileName);

write("\n\n\\FloatBarrier\\subsection{Coverage scores}", file = resultFileName, append = TRUE);
writeBITGCoverageTable(resultFileName);

write("\n\\FloatBarrier \n \\subsubsection{General Coverage Scores} \n\n", file = resultFileName, append = TRUE);
writeResultTable(mainFolder, inputFolders,"cumulativeMaxBranchingRelativeFrequencies.csv","", resultFileName, "", coverageTableCaption, coverageTableLabel);

writeBinarizationScoreTables(resultFileName);
# Write some empty lines to separate the two tables
write("\n\n", file = resultFileName, append = TRUE);
writeBITTTables(resultFileName);
#Step 3: Write latex footer
 writeLatexDocumentFooter(resultFileName);

warnings();
