/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import alignment.Alignment;
import util.InputReading;
import viewer_io.ButtonIOComponent;
import viewer_io.CheckBoxTable;
import viewer_io.MenuInteraction;
import viewer_io.TargetLoader;



public class BasicAlignmentVisualizer extends AlignmentViewer 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BasicAlignmentVisualizer(CheckBoxTable checkBoxTable, AlignmentPanel alignmentPanel)
	{
		super(checkBoxTable, alignmentPanel);
	}
	
	public static BasicAlignmentVisualizer createBasicAlignmentVisualizer()
	{
		CheckBoxTable checkBoxTable = CheckBoxTable.createCheckBoxTable();
		AlignmentPanel alignmentPanel = new AlignmentPanel(checkBoxTable);
		return new BasicAlignmentVisualizer(checkBoxTable, alignmentPanel);
	}
	
		@Override
		protected void setViewerOptions() 
		{
			System.out.println("BasicAlignmentVisualizer.setViewerOptions called");
			
			
			JTextField sourceField = new JTextField("learn by heart");
			JTextField targetField = new JTextField("uit het hoofd leren");
			JTextField alignmentField = new JTextField("0-3 1-0 1-1 1-2 2-0 2-1 2-2");
			
			java.awt.Font f = new java.awt.Font("Arial",Font.BOLD, 18);
			alignmentField.setFont(f);
			
			// TODO: Increase size of permutation textfield
					ioComponents = new ButtonIOComponent[]{
							new ButtonIOComponent(null,sourceField ,null, new JLabel("Source")),
							new ButtonIOComponent(null,targetField ,null, new JLabel("Target")),
							new ButtonIOComponent(new JButton("Show alignments from inputfields"),alignmentField ,null, new JLabel("Alignment:")),
			};
			
		}
			    
	    public static void doBeforeClosing() {	    	
	    	//Parameters.logPrintln("--- end of log");
	    	//Parameters.closeLogFile();
	    }    
		

		/**
	     * Create the GUI and show it.  For thread safety,
	     * this method should be invoked from the
	     * event-dispatching thread.
	     */
	    public static BasicAlignmentVisualizer createAndShowGUI() {
	        //Create and set up the window.
	        JFrame frame = new JFrame("Basic Alignment Visualizer");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        final BasicAlignmentVisualizer alignmentViewer = BasicAlignmentVisualizer.createBasicAlignmentVisualizer();
	        Dimension area = alignmentViewer.alignmentPanel.getArea();
	        frame.setMinimumSize(new Dimension(area.width+5, area.height+5));
	        alignmentViewer.setOpaque(true); //content panes must be opaque
	        frame.setContentPane(alignmentViewer);
	        
	        frame.addWindowListener (new WindowAdapter () {
	            public void windowClosing (WindowEvent e) {
	            	doBeforeClosing(alignmentViewer);
	            }
	        });
       
	      //Menu
	        JMenuBar menuBar = new JMenuBar();
	        JMenu menu = new JMenu("File");
	        JMenuItem openTreebank = new JMenuItem("Open Treebank");
	        
	        
	        MenuInteraction menuInteraction = MenuInteraction.createMenuInteraction(alignmentViewer.alignmentPanel);
	        menuInteraction.addAlignmentTripleLoadItems(alignmentViewer.alignmentPanel, alignmentViewer.alignmentTripleSpinnerPanel);
	        menuInteraction.addExportAlignmentstems(alignmentViewer.alignmentPanel);
	        menuInteraction.addItemsToMenu(menu);
	      
	  
	        JMenuItem quit = new JMenuItem("Quit");
	        
	        openTreebank.setAccelerator(KeyStroke.getKeyStroke('O', java.awt.event.InputEvent.ALT_MASK));
	        quit.setAccelerator(KeyStroke.getKeyStroke('Q', java.awt.event.InputEvent.ALT_MASK));
	     
        
	        alignmentViewer.ioComponents[2].getTheButton().addActionListener(new ActionListener()
	        {
	        	public void actionPerformed(ActionEvent e)
	        	{
	        	
	        		alignmentViewer.showAlignmentsForInputFields();
	        		
	        	}
	        });
	        
	        quit.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent event) {
	            	doBeforeClosing(alignmentViewer);
	                System.exit(0);
	            }
	        });
	            
	        menu.add(openTreebank);
	     
	     
	        //file.add(exportAllToXml);
	        menu.add(quit);
	        openTreebank.setMnemonic('O');  
	             //exportAllToXml.setMnemonic('X');
	        quit.setMnemonic('Q');	        	        

	        menuBar.add(menu);
	        frame.setJMenuBar(menuBar);
	        
	        //Display the window.
	        frame.pack();
	        frame.setVisible(true);
	        
	        return alignmentViewer;
	  
	    }
	  
	    public static void doBeforeClosing(BasicAlignmentVisualizer alignmentViewer) {	    	
	    	//Parameters.logPrintln("--- end of log");
	    	//Parameters.closeLogFile();
	    	saveGuiConfig(alignmentViewer);
	    }   
	    
	    
	    public void showAlignmentsForInputFields()
	    {
			System.out.println(">>> Show alignments for input...");    			
			
			 String sourceString = this.ioComponents[0].getTextfieldContents();
			 String targetString = this.ioComponents[1].getTextfieldContents();
			 String alignmentString = this.ioComponents[2].getTextfieldContents();
			
			 System.out.println("sourceString" + sourceString);
			 System.out.println("targetString" + targetString);
			 System.out.println("alignmentString" + alignmentString);
			 
			 List<List<String>> sourceSentences = InputReading.getOneSenceteSentenceList(sourceString); 
			 List<List<String>> targetSentences = InputReading.getOneSenceteSentenceList(targetString);
			 List<Alignment> alignments = InputReading.getOneSentenceAlignmentsList(alignmentString);
		
			 // Load the source senctence, target sentence and alignments
			 ((AlignmentPanel)this.alignmentPanel).loadSourceSentences(sourceSentences);
			 TargetLoader.loadTargetSentences(targetSentences, ((AlignmentPanel)this.alignmentPanel), this.alignmentTripleSpinnerPanel);

			 
			 ((AlignmentPanel)this.alignmentPanel).loadAlignments(alignments);
			 this.alignmentPanel.goToSentence(0);
			 this.alignmentPanel.loadSentence();
	    }
	    
	    protected static void saveGuiConfig(BasicAlignmentVisualizer alignmentViewer)
		{
			System.out.println("Saving gui configuration...");
			
			try
			{
				BufferedWriter outWriter = new BufferedWriter(new FileWriter("guiCachedConfig"));
				
				outWriter.write("% This is an automatically generated configuration file \n");
				outWriter.write("% The file contains the values of parameters changed in and automatically stored by the GUI \n");
				String configFileLocationString = "configFileLocation =" + alignmentViewer.ioComponents[1].getTextfieldContents();
				
				outWriter.write(configFileLocationString + "\n");
				
				String reorderOutputFileName = "reorderOutputFileName =" + alignmentViewer.ioComponents[0].getTextfieldContents();
				outWriter.write(reorderOutputFileName+ "\n");
				
				outWriter.close();
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	    
	    public static void main(String[] args) {
	        //Schedule a job for the event-dispatching thread:
	        //creating and showing this application's GUI.    	
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
	    }

	    // Mouse interface methods
		public void mouseClicked(MouseEvent e) 
		{
			System.out.println("Mouse clicked!!!");
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) 
		{
			//System.out.println("Mouse pressed");
			
		}

		public void mouseReleased(MouseEvent arg0) 
		{
			//System.out.println("Mouse released");
		}

	}
