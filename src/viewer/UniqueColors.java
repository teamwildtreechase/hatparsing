/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UniqueColors 
{
    Random seed;
    List<Color> colors;
    final int inc = 51;  // 216 unique colors  
    boolean random;
    private int currentIndex;
    private int currentColorIndex;
    private int numColors;
    private GreedyDistanceMaximizingOrder distanceMaximizingOrder;

 
    // Constructor
    public UniqueColors(boolean random, int numColors)
    {
        seed = new Random();
        colors = new ArrayList<Color>();
        currentIndex = 0;
        // take minimally 10 different colors
        this.numColors = Math.max(10, numColors);
        
        // increase the number of colors while the actual number of colors (after rejection of too bright colors)
        // is still smaller than the actual number of colors that is needed
        while(computeNumberAcceptedColors(this.numColors) < numColors)
        {
        	this.numColors += 5;
        }
        
        // Generate a distance maximizing order to help choose maximally distinguishable colors
        distanceMaximizingOrder = new GreedyDistanceMaximizingOrder(0, this.numColors - 1);
    }
    
    public void reset()
    {
    	currentColorIndex = 0;
    }
    
    public Color generateNextColor()
    {
    	Color result;
    	
    	if(currentColorIndex < colors.size())
    	{
    		result = colors.get(currentColorIndex);
    	}
    	else
    	{	
	    	if(random)
	    	{
	    		result =  generateNextRandomColor();
	    	}
	    	else
	    	{
	    		result = generateNextFixedColor();	
	    	}
	    	colors.add(result);
    	}	
	    	
    	currentColorIndex++;
    	return result;
    }

    
    /**
     * Function that computes the number of colors that is accepted if 
     * starting with a certain number numColors, but filtering 
     * out the too bright colors amongst the ones generated
     * @param numColors
     * @return The number of colors that will remain after rejection of too bright colors
     */
    public static int computeNumberAcceptedColors(int numColors)
    {
    	int numAcceptedColors = 0;
    	
    	for(int i = 0; i < numColors; i++)
    	{	
			Color c = Color.getHSBColor((float) i / (float) numColors, 0.85f, 1.0f);
			
			// Reject the color if it is too bright (i.e. not distinguishable from the white background
			if(getBrightness(c) < 235)
			{
				numAcceptedColors++;
			}
    	}
    	
    	return numAcceptedColors;
    }
    
    
    public Color generateNextFixedColor() 
    {
    	Color c = Color.BLACK;
    	boolean accepted = false;
    	
    	while(!accepted)
    	{	
    		int hueValue = distanceMaximizingOrder.get(currentIndex);
    		c = Color.getHSBColor((float) hueValue / (float) numColors, 0.85f, 1.0f);
    		
    		// Reject the color if it is too bright (i.e. not distinguishable from the white background
    		if(getBrightness(c) < 235)
    		{
    			accepted = true;
    		}
    	  	currentIndex++;
    	} 	
    		  
    	return c;
    	
    }
    
    public Color generateNextRandomColor() {
        while(true) 
        {
            Color color = getColor();
            if(!colors.contains(color) && (!color.equals(Color.black)) && (!color.equals(Color.white)) ) 
            {        
                return color;
            }
        }
    }
 
    private Color getColor() {
        int[] n = new int[3];
        for(int j = 0; j < 3; j++) {
            n[j] = seed.nextInt(6);
        }
        return new Color(n[0]*inc, n[1]*inc, n[2]*inc);
    }
    
    // Returns value between 0-255 
    private static int getBrightness(Color c) {
    	return (int) Math.sqrt(
    	  c.getRed() * c.getRed() * .241 +
    	  c.getGreen() * c.getGreen() * .691 +
    	  c.getBlue() * c.getBlue() * .068);
    }
    public static Color getForeGroundColorBasedOnBGBrightness(Color color) {
    	if (getBrightness(color) < 130)
    		return Color.white;
    	else
    		return Color.black;
    }

    
}