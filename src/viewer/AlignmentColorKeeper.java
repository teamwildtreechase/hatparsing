/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;

import viewer_io.CheckBoxTable;

public class AlignmentColorKeeper 
{
	
	public final static Color[] alignmentColors = {Color.blue, Color.cyan, Color.green, Color.magenta, Color.orange, Color.pink,Color.yellow};
	public final static Color[] alignmentGrayColors = {Color.black, Color.gray, Color.lightGray};
	private int alignmentColorIndex;
	private int alignmentGrayIndex;
	
	private CheckBoxTable checkBoxTable;
	
	public  AlignmentColorKeeper(CheckBoxTable checkBoxTable)
	{
		this.checkBoxTable = checkBoxTable;
	}
	
	
	public void setAlignmentColorIndex(int index)
	{
		this.alignmentColorIndex = index;
		this.alignmentGrayIndex = index;
	}
	
	/**
	 *  Method that increments the index of the current alignment color, which loops through a 
	 *  round robing list of possible colors
	 */	
	public void incrementAlignmentColorIndex()
	{
			alignmentColorIndex = (alignmentColorIndex + 1) % alignmentColors.length;
			alignmentGrayIndex = (alignmentGrayIndex + 1) % alignmentGrayColors.length;
	}
	
	/**
	 * Get the current alignment color
	 */
	public Color getCurrentAlignmentColor()
	{
		if(!checkBoxTable.noColors())
		{	
			// extract the color for this alignment
			return alignmentColors[alignmentColorIndex];
		}
		else
		{	
			return alignmentGrayColors[alignmentGrayIndex];
		}	
	}
}