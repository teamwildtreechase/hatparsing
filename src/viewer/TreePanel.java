/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */

package viewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import javax.swing.JPanel;
import viewer_io.CheckBoxTable;
import viewer_io.ExportableCorpusViewPanel;
/** The abstract class responsible for rendering.
 */


@SuppressWarnings("serial")
public abstract class TreePanel<T> extends JPanel implements ExportableCorpusViewPanel, ZoomInterface, ActionListener {

	private Dimension area = new Dimension(0,0);
	public static int minAreaWidth = 175;
	public static int minAreaHeight = 80;
	
	private List<T> treebank;
	
	String flatSentence;
	private int sentenceNumber;
	int lastIndex;
	
	protected CheckBoxTable checkBoxTable;
	
	
	private PanelSizeProperties panelSizeProperties;
	
	
	protected TreePanel(CheckBoxTable checkBoxTable, PanelSizeProperties panelSizeProperties)
	{
		this.checkBoxTable = checkBoxTable;
		this.checkBoxTable.addCheckBoxActionListeners((ActionListener) this);
		this.setPanelSizeProperties(panelSizeProperties);
	}
	
	public abstract HashMap<String, Integer> getVarNumTable();
	
	
	/**
	 * Method that sets the treebank for the panel
	 */

	public int loadTreebank(List<T> treebank) 
	{
		this.setTreebank(treebank);
		lastIndex = treebank.size()-1;
		init();
		return treebank.size();
	}
	
		
	public void init() 
	{
		this.getPanelSizeProperties().loadFont(this);
		loadSentence();
        setPreferredSize(getArea());        
        revalidate();
        repaint();            
	}
    
	public abstract void render(Graphics2D g2);
	
	
	public void setMaxNumLabels(int maxNumLabels)
	{
		this.getPanelSizeProperties().setLevelSizeFactor(maxNumLabels + 2);
	}
	
	public int getSentenceNumber() 
	{
		return sentenceNumber;
	}
	
	public int increaseFontSize() 
	{
		int result = this.getPanelSizeProperties().increaseFontSize();
		this.init();
		return result;
	}
	
	public int decreaseFontSize() 
	{
		int result = this.getPanelSizeProperties().decreaseFontSize();
		this.init();
		return result;
	}
	
	
	public int nextSentence() {
		if (sentenceNumber==lastIndex) return sentenceNumber=0;
		return ++sentenceNumber;
	}
	
	public int previousSentence() {
		if (sentenceNumber==0) return sentenceNumber=lastIndex;
    	return --sentenceNumber;
	}
	
	public int goToSentence(int n) {
		if (n<0) return sentenceNumber=0;
		if (n>lastIndex) return sentenceNumber=lastIndex;		    
    	return sentenceNumber = n;
	}
	
	public int getNumTrees()
	{
		return getTreebank().size();
	}
	
	
	public int getCorpusSize()
	{
		return this.getTreebank().size();
	}
	
	
	public abstract void loadSentence();

	public void setArea(Dimension area) {
		this.area = area;
	}

	public Dimension getArea() {
		return area;
	}

	public void setTreebank(List<T> treebank) {
		this.treebank = treebank;
	}

	public List<T> getTreebank() {
		return treebank;
	}
	
	/** Method that returns the emphasis color for alignment violations, 
	 * depending on whether the noColorCheckBox is checked
	 * 
	 * @return the color
	 */
	
	public Color alignmentViolationColor()
	{
		if(checkBoxTable.noColors())
		{
			return Color.GRAY;
		}	
		else
		{
			return new Color(255,0,200);
		}
	}

	/** Method that returns the emphasis color for nodes that root reordered nodes, 
	 * depending on whether the noColorCheckBox is checked
	 * 
	 * @return the color
	 */
	public Color rootsReorderingColor()
	{
		if(checkBoxTable.noColors())
		{
			return Color.darkGray;		
		}
		else
		{
			return Color.RED;
		}
	}
	
	/** Method that returns the emphasis color for relatively reordered nodes, 
	 * depending on whether the noColorCheckBox is checked
	 * 
	 * @return the color
	 */
	public Color relativelyReorderdColor()
	{
		if(checkBoxTable.noColors())
		{
			return Color.lightGray;
		}
		else
		{
			return  new Color(0,200,200);
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		init();
	}

	public void renderText(Graphics2D g2, String text) {
		g2.setFont(getPanelSizeProperties().font()); 
		g2.setColor(Color.black);
		g2.drawString(text, 0, 0);
	}



	public PanelSizeProperties getPanelSizeProperties() {
		return panelSizeProperties;
	}



	public void setPanelSizeProperties(PanelSizeProperties panelSizeProperties) {
		this.panelSizeProperties = panelSizeProperties;
	}
}
