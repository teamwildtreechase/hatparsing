/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

public class SentenceRenderData 
{
	private List<String> words;
	private List<WordRenderPosition> renderPositions;
	private int sentenceRenderLength;
	
	
	private SentenceRenderData(List<String> words, List<WordRenderPosition> renderPositions, int sentenceLength)
	{
		this.words = words;
		this.renderPositions = renderPositions;
		this.sentenceRenderLength = sentenceLength;
	}

	
	
	private static<T> List<T> createNewListIfNullOrReturnOld(List<T> oldList)
	{
		List<T> newList;
		
		if(oldList != null)
		{	
			newList = new ArrayList<T>(oldList);
		}
		else
		{
			newList = new ArrayList<T>();
		}
		return newList;
	}
	
	
	public static SentenceRenderData createSentenceRenderData(List<String> words,List<String> alignmentSetStrings, PanelSizeProperties panelSizeProperties, boolean showSourceAlignmentSets)
	{
		List<String> words2 = createNewListIfNullOrReturnOld(words);
		int sentenceRenderLength = SentenceRenderData.computeSentenceRenderLength(words2,alignmentSetStrings, panelSizeProperties);
		return new SentenceRenderData(words2, SentenceRenderData.createWordRenderPositions(words2,alignmentSetStrings, panelSizeProperties,showSourceAlignmentSets), sentenceRenderLength);
	}

	public static SentenceRenderData createSentenceRenderData(List<String> words, PanelSizeProperties panelSizeProperties)
	{
		List<String> words2 = createNewListIfNullOrReturnOld(words);
		int sentenceRenderLength = SentenceRenderData.computeSentenceRenderLength(words2, panelSizeProperties);
		return new SentenceRenderData(words2, SentenceRenderData.createWordRenderPositions(words2, panelSizeProperties), sentenceRenderLength);
	}
	
	public int getSentenceRenderLength()
	{
		return this.sentenceRenderLength;
	}
	
	public int getSentenceLength()
	{
		return this.words.size();
	}
	
	public String getWord(int index)
	{
		return this.words.get(index);
	}
	
	public int getXLeftPos(int index)
	{
		return this.renderPositions.get(index).getXLeftPos();
	}

	public int getXMiddlePos(int index)
	{
		return this.renderPositions.get(index).getXMiddlePos();
	}
	

	/** Method that sets the length arrays for rendering given an input sentence and the arrays to 
	 * be filled
	 * 
	 * @param wordXLeftArray   Array to be filled with left word positions
	 * @param wordXMiddleArray Array to be filled with middle of word positions
	 * @param words  Array with words to determine the positions for
	 * @return the right position of the last word plus a margin
	 */
	
	public static int setLengthArrays(int[] wordXLeftArray, int[] wordXMiddleArray, String[] words, PanelSizeProperties panelSizeProperties)
	{
		int currentX = panelSizeProperties.leftMargin();  // replaced 0 by leftMargin to put some space between the border and the first word
	
		for(int i = 0; i < words.length; i++) 
		{    			
			String w = words[i];
			int wordLength = panelSizeProperties.stringWidth(w);
			wordXLeftArray[i] = currentX;    			    			
			wordXMiddleArray[i] = currentX + wordLength/2;
			currentX += wordLength + panelSizeProperties.wordSpace();
		} 	
		return currentX;
	}
	
	
	
	
	
	public static List<WordRenderPosition> createWordRenderPositions(List<String> words,List<String> alignmentSetStrings, PanelSizeProperties panelSizeProperties, boolean showSourceAlignmentSets)
	{
		List<WordRenderPosition> result = new ArrayList<WordRenderPosition>();
		
		int cumulativeLeftPosition = panelSizeProperties.leftMargin();  	
		Assert.assertEquals(words.size(), alignmentSetStrings.size());
		
		for(int i = 0; i < words.size(); i++)
		{
			int wordLength = panelSizeProperties.stringWidth(words.get(i));
			int alignmentSetLength = panelSizeProperties.stringWidth(alignmentSetStrings.get(i));
			int maxLength;
			if(showSourceAlignmentSets){
			  maxLength = Math.max(wordLength, alignmentSetLength); 
			}
			else{
			  maxLength = wordLength;
			}
			 
			int middlePos = cumulativeLeftPosition + maxLength/2;
			int leftWordPos = middlePos - ((int)(0.5 * wordLength));
			result.add(new WordRenderPosition(leftWordPos, middlePos));
			cumulativeLeftPosition += maxLength + panelSizeProperties.wordSpace();		
		}
		return result;
		
	}
	
	public static List<WordRenderPosition> createWordRenderPositions(List<String> words, PanelSizeProperties panelSizeProperties)
	{
		List<WordRenderPosition> result = new ArrayList<WordRenderPosition>();
		
		int cumulativeLeftPosition = panelSizeProperties.leftMargin();  
		
		for(String word : words)
		{
			int wordLength = panelSizeProperties.stringWidth(word);
			result.add(new WordRenderPosition(cumulativeLeftPosition, cumulativeLeftPosition + wordLength/2));
			cumulativeLeftPosition += wordLength + panelSizeProperties.wordSpace();		
		}
		return result;
		
	}
	

	public static int computeSentenceRenderLength(List<String> words,List<String> alignmentSetStrings, PanelSizeProperties panelSizeProperties)
	{
		int result =  panelSizeProperties.leftMargin();  
	
		for(int i = 0; i < words.size(); i++)
		{
			int wordLength = panelSizeProperties.stringWidth(words.get(i));
			int alignmentSetLength = panelSizeProperties.stringWidth(alignmentSetStrings.get(i));
			int maxLength = Math.max(wordLength, alignmentSetLength);
			result += maxLength + panelSizeProperties.wordSpace();		
		}
		return result;
	}

	public static int computeSentenceRenderLength(List<String> words, PanelSizeProperties panelSizeProperties)
	{
		int result =  panelSizeProperties.leftMargin();  
	
		for(String word : words)
		{
			int wordLength = panelSizeProperties.stringWidth(word);
			result += wordLength + panelSizeProperties.wordSpace();		
		}
		return result;
	}
	
	
}
