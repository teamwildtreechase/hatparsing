/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.io.File;
import java.util.List;

import tsg.TSNodeLabel;
import tsg.corpora.Wsj;
import viewer_io.TreebankLoaderInterface;

public class ParsesTable  implements TreebankLoaderInterface<TSNodeLabel>
{
	
	private List<TSNodeLabel> parses;

	private ParsesTable(List<TSNodeLabel> parses)
	{
		this.parses = parses;
	}
	
	public static ParsesTable createParseTable()
	{
		return new ParsesTable(null);
	}
	
	/** 
	 * Method that sets the source parses for the panel
	 * @param sourceSentences
	 */
	public void loadSourceParses(List<TSNodeLabel>  parses) 
	{
		this.parses = parses;
		System.out.println(">>Parses loaded");
	}
	
	
	@Override
	public boolean loadTreebankAndSetSpinner(File file, boolean resetSpinner) 
	{
		List<TSNodeLabel> treeBank  = null;
		try 
		{
			treeBank = Wsj.getTreebank(file);
			loadSourceParses(treeBank);
			return true;
		} catch (Exception e) {
				
			e.printStackTrace();
			return false;
		}		
	}
	
	public boolean hasParses()
	{
		return (parses != null) && (parses.size() > 0);
	}
	
	public TSNodeLabel getParse(int sentenceNumber)
	{
		if(hasParses())
		{	
			return parses.get(sentenceNumber);
		}
		return null;
	}
}
