/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JCheckBox;

import org.junit.Assert;

import extended_bitg.TreeRepresentation;
import tsg.TSNodeLabel;

public abstract class NodeLabelsExtracter {   
	private final JCheckBox showHATReorderingLabelsCheckBox;
	private final JCheckBox showParentRelativeReorderingLabelsCheckBox;
	private final JCheckBox showPreterminalLabelsCheckbox;

	protected NodeLabelsExtracter( JCheckBox showHATReorderingLabelsCheckBox, JCheckBox showParentRelativeReorderingLabelsCheckBox,
	    JCheckBox showPreterminalLabelsCheckbox) {
		this.showHATReorderingLabelsCheckBox = showHATReorderingLabelsCheckBox;
		this.showParentRelativeReorderingLabelsCheckBox = showParentRelativeReorderingLabelsCheckBox;
		this.showPreterminalLabelsCheckbox = showPreterminalLabelsCheckbox;
	}

	
	public boolean showPreTerminalLabels(){
	  return (showPreterminalLabelsCheckbox != null) && showPreterminalLabelsCheckbox.isSelected(); 
	}
	
	
	private String getLabelWithoutLexicalItemProductionsInfo(String label){
	  Assert.assertTrue(label.contains(TreeRepresentation.LEXICAL_PRODUCTIONS_INDICATOR));
	  int lexicalProductionsStartIndex = label.indexOf(TreeRepresentation.LEXICAL_PRODUCTIONS_INDICATOR);
	  return label.substring(0, lexicalProductionsStartIndex);
	}
	
	public List<String> getLabelStringListToRender(TSNodeLabel n) {

		List<String> labelParts = getLabelParts(n);
		
		
		List<String> result;

		System.out.println("TreeVisualizer : --- labelParts:" + labelParts);

		if (n.isPreTerminal() && n.label().contains("=>")) {
		    Assert.assertEquals(1,labelParts.size());
			result = new ArrayList<String>();
			if( showPreTerminalLabels()){			 
			  result.add(getLabelWithoutLexicalItemProductionsInfo(labelParts.get(0)));
			}  
		} else {
			result = labelParts;
		}

		if (this.getCCGLabel(n) != null) {
			result.add(getCCGLabel(n));
		}

		if ((showHATReorderingLabelsCheckBox != null) && (showHATReorderingLabelsCheckBox.isSelected())) {
			String reorderingLabel = getHATReorderingLabel(n);
			System.out.println("HAT ReorderingLabel: " + reorderingLabel);
			if (reorderingLabel != null) {
				result.add(reorderingLabel);
			}
		}

		
		if ((showParentRelativeReorderingLabelsCheckBox != null) && (showParentRelativeReorderingLabelsCheckBox.isSelected())) {
			String reorderingLabel = getParentRelativeReorderingLabel(n);
			System.out.println("Parent-Relative ReorderingLabel: " + reorderingLabel);
			if (reorderingLabel != null) {
				result.add(reorderingLabel);
			}
		}

		return result;

	}

	private String getHATReorderingLabel(TSNodeLabel n) {
		return n.getExtraLabel(TreeRepresentation.HAT_REORDERING_LABEL_PROPERTY);
	}
	
	private String getParentRelativeReorderingLabel(TSNodeLabel n) {
		return n.getExtraLabel(TreeRepresentation.PARENT_RELATIVE_REORDERING_LABEL_PROPERTY);
	}

	protected abstract String getCCGLabel(TSNodeLabel n);

	private static List<String> getLabelParts(TSNodeLabel n) {
		return new ArrayList<String>(Arrays.asList(n.label().split("#")));
	}
}
