/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;
import viewer_io.CheckBoxTable;
import alignment.AlignmentPair;



public abstract class BasicPanel extends JPanel 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Dimension panelSize = new Dimension(640,400);
	public final static int minAreaWidth = 175;
	public final static int minAreaHeight = 80;

	int sentenceNumber;
	int lastIndex = 0;
	
	String flatSentence;
	PanelSizeProperties panelSizeProperties;
	
	
	// the alignments stored as pairs extracted from the input
	ArrayList<AlignmentPair> sentenceAlignments;
		
	
	int wordsYPosition;

	// static affay of colors that can be used to draw alignments
	public final static Color[] alignmentColors = {Color.blue, Color.cyan, Color.green, Color.magenta, Color.orange, Color.pink,Color.yellow};
	public final static Color[] alignmentGrayColors = {Color.black, Color.gray, Color.lightGray};
	protected CheckBoxTable checkBoxTable;

	int yOffset = 0;
		
	public BasicPanel(CheckBoxTable checkBoxTable) 
	{
		this.checkBoxTable = checkBoxTable;
		this.checkBoxTable.addCheckBoxActionListeners((ActionListener) this);
		this.panelSizeProperties = new PanelSizeProperties();
		System.out.println(" BasicPanel default constructor returns");
	}
	
	public void setSentenceNumber(int sn) {
		this.sentenceNumber = sn;
	}
	
	
	public int getSentenceNumber() 
	{
		return sentenceNumber;	
	}
	

	
	public int nextSentence() {
		if (sentenceNumber==lastIndex) return sentenceNumber=0;
		return ++sentenceNumber;
	}
	
	public int previousSentence() {
		if (sentenceNumber==0) return sentenceNumber=lastIndex;
    	return --sentenceNumber;
	}
	

	

	public abstract void render(Graphics2D g2);
	
	
	public void init() {
		panelSizeProperties.loadFont(this);
		loadSentence();
        setPreferredSize(new Dimension(minAreaWidth,minAreaHeight));
        revalidate();
        repaint();
	}

	/**
	 *  Method to render text on the graphics object
	 * @param g2 the graphics object
	 * @param text the text to render
	 */	
	public void renderText(Graphics2D g2, String text) 
	{
		g2.setFont(panelSizeProperties.font()); 
		g2.setColor(Color.black);
		g2.drawString(text, 0, 0);
	}
	

	
	public abstract void loadSentence();    
    
    
    /** Method that sets the length arrays for rendering given an input sentence and the arrays to 
     * be filled
     * 
     * @param wordXLeftArray   Array to be filled with left word positions
     * @param wordXMiddleArray Array to be filled with middle of word positions
     * @param words  Array with words to determine the positions for
     * @return the right position of the last word plus a margin
     */
    
    public int setLengthArrays(int[] wordXLeftArray, int[] wordXMiddleArray, String[] words )
    {
    	int currentX = panelSizeProperties.leftMargin();  // replaced 0 by leftMargin to put some space between the border and the first word
	
		for(int i = 0; i < words.length; i++) 
		{    			
			String w = words[i];
			int wordLength = panelSizeProperties.stringWidth(w);
			wordXLeftArray[i] = currentX;    			    			
			wordXMiddleArray[i] = currentX + wordLength/2;
			currentX += wordLength + panelSizeProperties.wordSpace();
		} 	
		return currentX;
    }
    


	public Dimension getArea() {
		return panelSize;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		init();
	}
	
}
