/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.util.ArrayList;
import java.util.List;

public class NodeRenderPosition 
{
	private int xMiddle;
	private int y;
	private int maxLabelLength;
	private int numLabels;
	private final PanelSizeProperties panelSizeProperties;
	private final boolean drawUpsideDown;
	
	private NodeRenderPosition(int xMiddle, int y, int maxLabelLength,int numLabels, PanelSizeProperties panelSizeProperties, boolean drawUpsideDown)
	{
		this.xMiddle = xMiddle;
		this.y = y;
		this.maxLabelLength = maxLabelLength;
		this.numLabels = numLabels;
		this.panelSizeProperties = panelSizeProperties;
		this.drawUpsideDown = drawUpsideDown;
	}
	
	
	public static NodeRenderPosition createNodeRenderPosition(PanelSizeProperties panSizeProperties, final boolean drawUpsideDown)
	{
		return new NodeRenderPosition(-1,-1,-1,1, panSizeProperties, drawUpsideDown);
	}
	
	public static List<NodeRenderPosition> createNodeRenderPositionList(int length, PanelSizeProperties panelSizeProperties, final boolean drawUpsideDown)
	{
		List<NodeRenderPosition> result = new ArrayList<NodeRenderPosition>();
		
		for(int i = 0 ; i < length; i++)
		{
			result.add(createNodeRenderPosition(panelSizeProperties, drawUpsideDown));
		}
		
		return result;
	}
		
	public int getXLeft(String label)
	{
		return this.xMiddle - ((panelSizeProperties.stringWidth(label)/2));
	}
	public void setXMiddle(int xMiddle)
	{
		this.xMiddle = xMiddle;
	}
	
	public int getXMiddle()
	{
		return xMiddle;
	}
	
		
	public void setY(int y)
	{
		this.y = y;
	}
	
	public void setNumLabels(int numLabels)
	{
		this.numLabels = numLabels;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getNodeLinkTopBoundPosition()
	{
		if(drawUpsideDown)
		{
			return this.getY() + this.panelSizeProperties.fontHeight();
		}
		else
		{
			return this.getY() - this.panelSizeProperties.fontHeight();
		}
	}
	
	
	public int getLeafNodeBottomY()
	{
		if(drawUpsideDown)
		{
			return this.getLabelYTop(this.numLabels - 1);
		}
		else
		{
			return this.getLabelYBottom(this.numLabels - 1);
		}
	}
	
	
	public int getLabelY(int labelNum)
	{
		if(drawUpsideDown)
		{
			return getY() + panelSizeProperties.fontAscent() - getExtraLabelHeight(labelNum);
		}
		
		return getY() + getExtraLabelHeight(labelNum);
		
	}
	
	private int getExtraLabelHeight(int labelNum)
	{
		int extraSizeFactor = (labelNum + (panelSizeProperties.getMaxNumLabelsTree() - this.numLabels));
		return extraSizeFactor * panelSizeProperties.fontHeight();
	}
	
	
	
	public int getLabelYTop(int labelNum)
	{
		return getLabelY(labelNum) -  panelSizeProperties.fontAscent() - panelSizeProperties.textTopMargin();
	}

	public int getLabelYBottom(int labelNum)
	{
		return getLabelY(labelNum) + panelSizeProperties.fontDescendent() + panelSizeProperties.textTopMargin();
	}
	
	
	public int getBulletYTop()
	{
		return (int)(getBulletYMiddle() - (0.5 * getSymbolBulletDiameter()) -  panelSizeProperties.textTopMargin());
	}

	public int getBulletYBottom()
	{
		return (int)(getBulletYMiddle() + (0.5 * getSymbolBulletDiameter()) + panelSizeProperties.textTopMargin());
	}
	
	public int getBulletYMiddle()
	{
		if(drawUpsideDown)
		{
			return (int)(getY() - this.panelSizeProperties.fontAscent() + (0.5 * getSymbolBulletDiameter())) - + getExtraLabelHeight(this.numLabels - 1);
		}
		return (int) (getY()  + this.panelSizeProperties.fontDescendent() +  (0.5 * getSymbolBulletDiameter())) +  getExtraLabelHeight(this.numLabels -1);
	}
	
	
	public int getBulletDownLineBoundPositionY()
	{
		if(drawUpsideDown)
		{
			return getBulletYTop();
		}
		return getBulletYBottom();
	}

	public int getBulletUpLineBoundPositionY()
	{
		if(drawUpsideDown)
		{
			return getBulletYBottom();
		}
		return getBulletYTop();
	}
	
	
	public int getLabelUpLineBoundPositionY(int labelNum)
	{
		if(drawUpsideDown)
		{
			return getLabelYBottom(labelNum);
		}
		return getLabelYTop(labelNum);		
	}
	
	
	public int getLabelLineBoundPosition(int labelNum)
	{
		if(drawUpsideDown)
		{
			return getLabelYBottom(labelNum);
		}
		return getLabelYTop(labelNum);
	}
	
	
	
	public int getNamePlateX()
	{
		return this.getXMiddle() + ((int) getSymbolBulletDiameter()); 
	}
	
	
	public double getSymbolBulletDiameter()
	{
		int height = this.panelSizeProperties.fontHeight();
		return (int)(height * 0.8);
	}
	
	public void setLabelLength(int labelLength)
	{
		this.maxLabelLength = labelLength;
	}
	
	public int getLabelLength()
	{
		return maxLabelLength;
	}
	
	
	public static int computeXMiddlePosition(NodeRenderPosition leftMostPosition,NodeRenderPosition rightMostPosition)
	{
		/*
		// Deprecated: these old values give a somewhat skewed rendering, as the compute the 
		// middle taking the length of the node labels in account, rather than working
		// directly with the centers of the two node render positions
		int leftMostX = leftMostPosition.getXLeft();
		int rightMostX = rightMostPosition.getXLeft() + rightMostPosition.getLabelLength();
		*/
		int leftMostX = leftMostPosition.getXMiddle();
		int rightMostX = rightMostPosition.getXMiddle();
		
		
		return (leftMostX + rightMostX) / 2;
	}
	
}
