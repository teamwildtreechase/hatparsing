/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JPanel;

public class PanelSizeProperties 
{
	private int fontSize = 15;
    private Font font, smallFont, ultraSmallFont;    
    private FontMetrics metrics;
    
    private int topMargin = 60;
    private int bottomMargin = 60;	
    private int leftMargin = 20; 
    private int rightMargin = 30;
    private int wordSpace = 10;	
    private int textTopMargin = 4;
    private int levelSizeFactor = 5;
    private int levelSize = 2;
    
    private final String fontName = "Arial Unicode MS"; //"GNU Unifont" "unifont";// "Gentium";
	private final String ARIAL_UNICODE_FONT_FILE_NAME = "ARIALUNI.TTF";  // Not open source, but otherwise the best font
	private final String CODE2000_FONT_FILE_NAME = "CODE2000.TTF";  // Not open source, but otherwise the best font
	//private final String fontFileName = "CODE2000.TTF";
	private final String DEFAULT_FONT_FILE_NAME = "DejaVuSans.ttf";  // We settle for this as the best "free" TTF font, although limited in scope
	//private final String fontFileName = "LucidaSansRegular.ttf";

    
    private final Font basicUnicodeFont = createBasicUnicodeFontFromStream();
    private static final boolean UseSourceFolderToGetUnicodeFont = true;
    
    public int fontSize()
    {
    	return fontSize;
    }
    
    
    public int smallFontSize()
	{
		int result = fontSize - 4;
		
		if(result > 1)
		{
			return result;
		}
		return 1;
	}
	

	public int mediumFontSize()
	{
		int result = fontSize - 3;
		
		if(result > 1)
		{
			return result;
		}
		return 1;
	}
    
    
    public int ultraSmallFontSize()
	{
		int result = fontSize - 5;
		
		if(result > 1)
		{
			return result;
		}
		return 1;
	}
	
    public int fontDescendent()
    {
    	return metrics.getDescent();
    }
    
    public int fontAscent()
    {
    	return metrics.getAscent();
    }
    
    public Font font()
    {
    	return font;
    }
    
    public Font smallFont()
    {
    	return smallFont;
    }
    
    public Font ultraSmallFont()
    {
    	return ultraSmallFont;
    }
    
    public int topMargin()
    {
    	return topMargin;
    }
    
    
    public int bottomMargin()
    {
    	return bottomMargin;
    }
    
    public int leftMargin()
    {
    	return leftMargin;
    }
    
    public int rightMargin()
    {
    	return rightMargin;
    }
    
    public int textTopMargin()
    {
    	return textTopMargin;
    }
    
    public void setLevelSize(int levelSize)
    {
    	this.levelSize = levelSize;
    }

    public void setLevelSizeFactor(int levelSizeFactor)
    {
    	this.levelSizeFactor = levelSizeFactor;
    }
    
    public int fontHeight()
    {
    	return metrics.getHeight(); 
    }
    
	public int doubleFontHeight()
	{
		return 2 * fontHeight();
	}
	
	public int tripleFontHeight()
	{
		return 3 * fontHeight();
	}
    
	public int levelSize()
	{
		return levelSize;
	}
	
	public int wordSpace()
	{
		return wordSpace;
	}
	
	public void setWordSpace(int wordSpace)
	{
		this.wordSpace = wordSpace;
	}
	
	public void setTopMargin(int topMargin)
	{
		this.topMargin = topMargin;
	}
	
	public int increaseFontSize() {
		return ++fontSize;
	}
	
	public int decreaseFontSize() {
		if (fontSize==1) return 1;
		return --fontSize;
	}
	
	
	public int stringWidth(String word)
	{
		return this.metrics.stringWidth(word);
	}
	
	public static int computeStringRenderLength(JPanel panel, String word, Font font)
	{
		FontMetrics metrics = panel.getFontMetrics(font);
		return metrics.stringWidth(word);		
	}
	
	public int getMaxNumLabelsTree()
	{
		return this.levelSizeFactor - 2;
	}
	
	
	private InputStream getArialUnicodeFontIfAvailable()
	{
		InputStream fontStream = PanelSizeProperties.class.getResourceAsStream("/" + ARIAL_UNICODE_FONT_FILE_NAME);
		return fontStream;
	}
	
	private InputStream getCode2000FontIfAvailable()
	{
		InputStream fontStream = PanelSizeProperties.class.getResourceAsStream("/" + CODE2000_FONT_FILE_NAME);
		return fontStream;
	}
	
	private InputStream getDefaultFont()
	{
		InputStream fontStream = PanelSizeProperties.class.getResourceAsStream("/" + DEFAULT_FONT_FILE_NAME);
		return fontStream;
	}	
	
	private InputStream getBestAvailableFont()
	{
		// First try to get the ARIAL Unicode font if it is available
		InputStream fontStream = getArialUnicodeFontIfAvailable();
	
		if(fontStream == null) 
		{
			// Otherwise use Code2000 Font if available
			fontStream = getCode2000FontIfAvailable();
		}
		
		if(fontStream == null) 
		{
			// Otherwise use the default font
			fontStream = getDefaultFont();
		}
		
		if(fontStream == null)
		{	
			throw new RuntimeException("Font \"" + DEFAULT_FONT_FILE_NAME +   "\"  not found... make sure it is in the right place");
		}
		return fontStream;
	}
	
	private Font createBasicUnicodeFontFromStream()
	{
		InputStream fontStream = getBestAvailableFont();
		
		try 
		{
			Font font = Font.createFont(Font.TRUETYPE_FONT, fontStream);
			return font;
		} 
		catch (FontFormatException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	private Font createUnicodeFont(int style, int fontSize)
	{
		return basicUnicodeFont.deriveFont(style, (float)fontSize);
	}
	
	
	private void createFontsFromStream()
	{
		font = createUnicodeFont(Font.PLAIN, fontSize);
		smallFont = createUnicodeFont(Font.PLAIN, smallFontSize());
		ultraSmallFont = createUnicodeFont(Font.PLAIN, ultraSmallFontSize());
	}
	
	private void createFontsFromJaveJreFonts()
	{
		font = new Font(fontName, Font.PLAIN, fontSize);
		smallFont = new Font(fontName, Font.PLAIN, smallFontSize());
		ultraSmallFont = new Font(fontName, Font.PLAIN, ultraSmallFontSize());
	}
	
	
	public void loadFont(JPanel panel)
	{
		if(UseSourceFolderToGetUnicodeFont)
		{	
			createFontsFromStream();
		}
		else
		{
			createFontsFromJaveJreFonts();
		}
    	metrics = panel.getFontMetrics(font);
    	levelSize = fontHeight() * levelSizeFactor;	
	}
	
}
