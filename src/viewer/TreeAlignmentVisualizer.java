/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import alignment.Alignment;
import alignment.TreeAlignmentStruct;
import tsg.TSNodeLabel;

// ./pmd.sh /home/gemaille/atv-workingdirectory/trunk/src xml rulesets/unusedcode.xml

/**
 * This class stores the tree part of the Alignment graph and is responsible for
 * visualizing it. One important reason for using a separate class, is that we
 * work with two versions of the Alignment graph: the original one, and the
 * reordered one. This duplication is facilitated by the separate class.
 * Furthermore, the main rendering method drawTree(Graphics2D g2, int yOffset)
 * allows the specification of an yOffset, in order to draw the tree at the
 * right position within a panel.
 */

public class TreeAlignmentVisualizer extends TreeVisualizer<AlignmentTreePanel> {
	// private AlignmentTable alignmentTable;

	private TreeAlignmentStruct treeAlignmentStruct;
	AlignmentTreePanel thePanel;

	protected TreeAlignmentVisualizer(AlignmentTreePanel thePanel, boolean drawTreeUpsideDown, NodeLabelsExtracter nodeLabelsExtracter,
			ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer) {
		super(thePanel, drawTreeUpsideDown, nodeLabelsExtracter, extraTreeNodePropertiesVisualizer);
		this.thePanel = thePanel;
		this.treeAlignmentStruct = new TreeAlignmentStruct(this.getTheTreeStruct());
	}

	public static TreeAlignmentVisualizer createTreeAlignmentVisualizer(AlignmentTreePanel thePanel, boolean drawTreeUpsideDown) {
		NodeLabelsExtracter nodeLabelsExtracter = new SourceHatNodeLabelExtracter(null,null,null);
		return new TreeAlignmentVisualizer(thePanel, drawTreeUpsideDown, nodeLabelsExtracter,
				EmptyExtraTreeNodePropertiesVisualizer.createEmptyExtraTreeNodePropertiesVisualizer());
	}

	public void setAlignmentsAndComputeConsistency(Alignment sentenceAlignments) {
		// Generate new TreeAlignmentStruct based on new value theTreeStruct
		treeAlignmentStruct = new TreeAlignmentStruct(getTheTreeStruct());
		treeAlignmentStruct.setAlignmentsAndComputeConsistency(sentenceAlignments);
	}

	public void drawNodeLabel(Graphics2D g2, TSNodeLabel n, int i, int yOffset, boolean drawAlignmentViolations) {

		NodeRenderPosition nodePosition = getNodePosition(n);

		if (n.violatesSourceSubtreeAlignment && drawAlignmentViolations) {
			// set the appropriate alignment violations color
			g2.setColor(thePanel.alignmentViolationColor());

			java.awt.Font italicFont = new Font("Serif", Font.ITALIC, thePanel.getPanelSizeProperties().fontSize());
			g2.setFont(italicFont);
			g2.drawString(getTheTreeStruct().getLabel(i), nodePosition.getXLeft(n.label()), nodePosition.getY() + yOffset);
			g2.setFont(thePanel.getPanelSizeProperties().font());
		} else if ((n.alignmentViolaters != null) && (n.alignmentViolaters.size() > 0) && drawAlignmentViolations) {

			// set the appropriate alignment violations color
			g2.setColor(thePanel.alignmentViolationColor());

			String violatorsString = "  V.W.: ";
			for (TSNodeLabel violator : n.alignmentViolaters) {
				violatorsString += " |" + violator.label() + "|";
			}

			// overwrite the violation color with order change color if this
			// node has been moved
			// to another position relative to its parent (i.e. it is permuted
			// with its siblings)
			// if(n.ownParentRelativePositionChanged)
			// g2.setColor(thePanel.relativelyReorderdColor());

			g2.drawString(getTheTreeStruct().getLabel(i) + violatorsString, nodePosition.getXLeft(n.label()), nodePosition.getY() + yOffset);

		} else {

			// overwrite the color with order change color if this node has been
			// moved
			// to another position relative to its parent (i.e. it is permuted
			// with its siblings)
			// if(n.ownParentRelativePositionChanged)
			// g2.setColor(thePanel.relativelyReorderdColor());

			assert (getTheTreeStruct() != null);
			assert (getTheTreeStruct().getLabel(i) != null);
			g2.drawString(getTheTreeStruct().getLabel(i), nodePosition.getXLeft(n.label()), nodePosition.getY() + yOffset);
		}

		// set paint color back to black
		g2.setColor(Color.black);
	}

	public void drawNodeLinks(Graphics2D g2, TSNodeLabel n, int i, int yOffset, boolean drawAlignmentViolations) {

		TSNodeLabel p = n.parent;
		if (!(p == null)) {

			// if the subtree rooted at the node's parent is violated, draw link
			// to parent with the alignment violation color
			if ((p.alignmentViolaters != null) && (p.alignmentViolaters.size() > 0) && drawAlignmentViolations)
				g2.setColor(thePanel.alignmentViolationColor());

			// if the subtree rooted at this node is violated, draw appropriate
			// marker
			if ((n.alignmentViolaters != null) && (n.alignmentViolaters.size() > 0) && drawAlignmentViolations) {
				int markerSize = 15;
				Color tempColor = g2.getColor();
				g2.setColor(thePanel.alignmentViolationColor());
				BasicDrawing.drawCrossingMarker(getNodePosition(n).getXMiddle(), getNodePosition(n).getY() + yOffset
						+ thePanel.getPanelSizeProperties().textTopMargin() + ((int) (0.5 * markerSize)), markerSize, g2);
				// set back to previous color
				g2.setColor(tempColor);
			}

			// if the nodes position is changed relative to its parent, indicate
			// by appropriate color
			if (n.ownParentRelativePositionChanged) {
				BasicDrawing.drawReorderingMarker(getNodePosition(n).getXMiddle(), getNodePosition(n).getY() - thePanel.getPanelSizeProperties().fontSize()
						- thePanel.getPanelSizeProperties().textTopMargin() + yOffset, g2);
			}

			/*
			 * if(p.ownParentRelativePositionChanged) {
			 * g2.setColor(thePanel.relativelyReorderdColor()); }
			 */

			// if the nodes under this root are reordered, color the connecting
			// bar with the indicating color
			if (p.childOrderChanged) {
				g2.setStroke(new BasicStroke((float) 3));
				// g2.setColor(thePanel.rootsReorderingColor());
			}

			boolean skewed = thePanel.getCheckBoxTable().useSkewedLines();
			BasicDrawing.drawLine(getNodePosition(n).getXMiddle(), getNodePosition(n).getY() - thePanel.getPanelSizeProperties().fontSize()
					- thePanel.getPanelSizeProperties().textTopMargin() + yOffset, getNodePosition(p).getXMiddle(), getNodePosition(p).getY()
					+ thePanel.getPanelSizeProperties().textTopMargin() + yOffset, g2, skewed);
		}

		// Finally set back the stroke and color to the defaults
		g2.setStroke(new BasicStroke(1));
		g2.setColor(Color.black);

	}

	public void drawTree(Graphics2D g2, int yOffset) {
		for (TSNodeLabel n : getTheTreeStruct().getNodesArray()) {
			boolean isHead = thePanel.getCheckBoxTable().showHeads() && n.isHeadMarked();

			int i = getTheTreeStruct().getIndexTable().get(n);
			if (isHead)
				g2.setColor(Color.RED);

			// first draw the node label
			drawNodeLabel(g2, n, i, yOffset, thePanel.getCheckBoxTable().showTreeAlignmentViolations());
			// then draw the node links (lines)
			drawNodeLinks(g2, n, i, yOffset, thePanel.getCheckBoxTable().showTreeAlignmentViolations());
		}

	}

}
