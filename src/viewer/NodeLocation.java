/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.Point;

import tsg.TSNodeLabel;

/** Convenience class for storing the drawing location of a tree node in the GUI and computing 
 * distances between points*/
 
public class NodeLocation 
{
	private TSNodeLabel node;
	private Point location;
	
	public NodeLocation(TSNodeLabel node, Point location)
	{
		this.node = node;
		this.location = location;
	}

	public TSNodeLabel getNode()
	{
		return this.node;
	}
	
	public Point getLocation()
	{
		return this.location;
	}
	
	public static double betweenPointDistance(Point p1, Point p2)
	{
		 return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) +  (p1.y - p2.y) * (p1.y - p2.y));
	}
	
	public double computeDistance(Point p)
	{
		// compute and return the Eucledian distance between the two points
		return Math.sqrt((p.x - this.location.x) * (p.x - this.location.x) +  (p.y - this.location.y) * (p.y - this.location.y));
	}
	
}
