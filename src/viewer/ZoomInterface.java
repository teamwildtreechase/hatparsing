/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

public interface ZoomInterface 
{
	public int increaseFontSize();
	public int decreaseFontSize();
}
