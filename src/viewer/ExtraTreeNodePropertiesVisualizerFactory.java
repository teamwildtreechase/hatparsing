/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

public interface ExtraTreeNodePropertiesVisualizerFactory {
	public ExtraTreeNodePropertiesVisualizer createExtraTreeNodePropertiesVisualizer(TreePairPanel thePanel);
}
