/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import tsg.TSNodeLabel;
import util.Pair;
import viewer_io.CheckBoxTable;

@SuppressWarnings("serial")
public class TreePairPanel extends TreePanel<Pair<TSNodeLabel>> {

	protected TreeVisualizer<TreePairPanel> tree1Visualizer;
	protected TreeVisualizer<TreePairPanel> tree2Visualizer;

	UniqueColors uniqueColors;
	public HashMap<String, Color> variableColorLookupTable;
	public HashMap<String, Color> variableGrayValueLookupTable;
	public HashMap<String, Integer> variableVarNumLookupTable; // hasmap that
																// maps the
																// variable
																// numbers of
																// nodes to
																// integers

	public final int BETWEEN_TREE_DISTANCE = 60;

	protected TreePairPanel(CheckBoxTable checkBoxTable, PanelSizeProperties panelSizeProperties) {
		super(checkBoxTable, panelSizeProperties);
		variableColorLookupTable = new HashMap<String, Color>();
		variableGrayValueLookupTable = new HashMap<String, Color>();
		variableVarNumLookupTable = new HashMap<String, Integer>();
	}

	public static TreePairPanel createTreePairPanel(CheckBoxTable checkBoxTable) {
		PanelSizeProperties panelSizeProperties = new PanelSizeProperties();
		TreePairPanel treePairPanel = new TreePairPanel(checkBoxTable, panelSizeProperties);
		ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer = EmptyExtraTreeNodePropertiesVisualizer
				.createEmptyExtraTreeNodePropertiesVisualizer();
		TreeVisualizer<TreePairPanel> tree1Visualizer = TreeVisualizer.createSourceTreeVisualizer(treePairPanel, extraTreeNodePropertiesVisualizer);
		TreeVisualizer<TreePairPanel> tree2Visualizer = TreeVisualizer.createTargetTreeVisualizer(treePairPanel, extraTreeNodePropertiesVisualizer);
		treePairPanel.tree1Visualizer = tree1Visualizer;
		treePairPanel.tree2Visualizer = tree2Visualizer;

		return treePairPanel;
	}

	public static TreePairPanel createTreePairPanel(ArrayList<Pair<TSNodeLabel>> treePairBank, CheckBoxTable checkBoxTable) {
		TreePairPanel treePairPanel = createTreePairPanel(checkBoxTable);
		treePairPanel.loadTreebank(treePairBank);
		return treePairPanel;
	}

	public void setFirstTreeLeafDescriptionWords(List<String> leafDescriptionWords1) {
		this.tree1Visualizer.leafDescriptionData = SentenceRenderData.createSentenceRenderData(leafDescriptionWords1, getPanelSizeProperties());
	}

	public void setSecondTreeLeafDescriptionWords(List<String> leafDescriptionWords2) {
		this.tree2Visualizer.leafDescriptionData = SentenceRenderData.createSentenceRenderData(leafDescriptionWords2, getPanelSizeProperties());
	}

	public void setTree1Visualizer(TreeVisualizer<TreePairPanel> tree1Visualizer) {
		this.tree1Visualizer = tree1Visualizer;
	}

	public void setTree2Visualizer(TreeVisualizer<TreePairPanel> tree2Visualizer) {
		this.tree2Visualizer = tree2Visualizer;
	}

	public void resetVariableColorLookupTable() {
		variableColorLookupTable = new HashMap<String, Color>();
		variableGrayValueLookupTable = new HashMap<String, Color>();
		variableVarNumLookupTable = new HashMap<String, Integer>();
	}

	public void extractVariablesFromTreeAndMapToColors(TreeVisualizer<TreePairPanel> treeVisualizer) {
		TreeSet<String> uniqueVarNames = new TreeSet<String>();

		// First collect a set of unique var names
		for (TSNodeLabel node : treeVisualizer.getTheTreeStruct().getNodesArray()) {
			String varName = TreeVisualizer.getVarName(node);
			if (varName != "") {
				uniqueVarNames.add(varName);
			}
		}

		// create UniquColors object with the number of colors equal to the
		// number of unique var names
		uniqueColors = new UniqueColors(false, uniqueVarNames.size());

		int varNum = 1;
		// add a color mapping entry to the hashtable for every unique varname
		for (String varName : uniqueVarNames) {
			Color color = uniqueColors.generateNextColor();
			float hue = 10;
			float saturation = 0;
			float brightness = (float) ((((float) varNum) / uniqueVarNames.size()) * 0.85);
			Color grayValue = Color.getHSBColor(hue, saturation, brightness);
			variableColorLookupTable.put(varName, color);
			variableGrayValueLookupTable.put(varName, grayValue);
			variableVarNumLookupTable.put(varName, varNum);
			varNum++;
		}

	}

	public void init() {
		System.out.println("TreePairPanel.init() called");

		this.getPanelSizeProperties().loadFont(this);
		loadSentence();
		setPreferredSize(getArea());
		this.setMinimumSize(getArea());
		System.out.println("this.getPreferredSize() : " + this.getPreferredSize());
		revalidate();
		repaint();
	}

	@Override
	public void render(Graphics2D g2) {
		g2.setFont(getPanelSizeProperties().font());
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke());

		System.out.println("TreePairPanel.render() called");

		// g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		// RenderingHints.VALUE_ANTIALIAS_ON);
		// g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		// RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		// g2.setRenderingHint(RenderingHints.KEY_RENDERING,
		// RenderingHints.VALUE_RENDER_QUALITY);
		// g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		// RenderingHints.VALUE_INTERPOLATION_BICUBIC);

		tree1Visualizer.drawTree(g2, 0, false);
		tree2Visualizer.drawTree(g2, tree1Visualizer.getTreeHeight() + BETWEEN_TREE_DISTANCE, true);

	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		render(g2);
	}

	public void loadSentence() {

		System.out.println("TreePairPanel.loadSentence()... numSentences:" + getTreebank().size());

		if (getSentenceNumber() < getTreebank().size()) {
			System.out.println("loading sentence " + getSentenceNumber());

			Pair<TSNodeLabel> treePair = getTreebank().get(getSentenceNumber());
			TSNodeLabel tree1 = treePair.getFirst();
			TSNodeLabel tree2 = treePair.getSecond();
			tree1Visualizer.setTree(tree1);
			tree2Visualizer.setTree(tree2);
		} else {
			try {
				Pair<TSNodeLabel> treePair = new Pair<TSNodeLabel>(new TSNodeLabel("(S tree-not-present)"), new TSNodeLabel("(S tree-not-present)"));
				TSNodeLabel tree1 = treePair.getFirst();
				TSNodeLabel tree2 = treePair.getSecond();
				tree1Visualizer.setTree(tree1);
				tree2Visualizer.setTree(tree2);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		resetVariableColorLookupTable();
		extractVariablesFromTreeAndMapToColors(tree1Visualizer);
		extractVariablesFromTreeAndMapToColors(tree2Visualizer);

		System.out.println("tree1Visualizer.treeWidth: " + tree1Visualizer.treeWidth);
		System.out.println("tree2Visualizer.treeWidth: " + tree2Visualizer.treeWidth);
		getArea().width = Math.max(tree1Visualizer.treeWidth, tree2Visualizer.treeWidth);
		getArea().height = tree1Visualizer.getTreeHeight() + tree2Visualizer.getTreeHeight() + BETWEEN_TREE_DISTANCE;

	}

	protected int getBottomTreeYOffset() {
		return this.tree1Visualizer.getTreeHeight() + this.BETWEEN_TREE_DISTANCE;
	}
	
	public Pair<TSNodeLabel> getSentence(int n) {
		return getTreebank().get(n);
	}

	public boolean firstTreeConsistentWithString(String s) {
		return this.tree1Visualizer.treeIsConsistenWithString(s);
	}

	public boolean secondTreeConsistentWithString(String s) {
		return this.tree2Visualizer.treeIsConsistenWithString(s);
	}

	@Override
	public HashMap<String, Integer> getVarNumTable() {
		return variableVarNumLookupTable;
	}

	public HashMap<String, Color> getColorTable() {
		HashMap<String, Color> result = null;
		if (!this.checkBoxTable.noColors()) {
			result = this.variableColorLookupTable;
		} else {
			result = this.variableGrayValueLookupTable;
		}
		return result;
	}

	public int getMaxVarNum() {
		int result = 0;
		HashMap<String, Color> colorTable = getColorTable();

		if (colorTable != null) {
			result = colorTable.keySet().size();
		}
		return result;
	}

}
