/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Graphics2D;

import tsg.TSNodeLabel;

public interface ExtraTreeNodePropertiesVisualizer {
	public void drawExtraTreeNodeProperties(Graphics2D g2, TSNodeLabel node, NodeRenderPosition nodePosition, int yOffset);
}
