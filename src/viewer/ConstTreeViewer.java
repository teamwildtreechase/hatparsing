/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import tsg.TSNodeLabel;
import tsg.corpora.Wsj;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.MenuInteraction;

@SuppressWarnings("serial")
public class ConstTreeViewer extends SingleTreeViewer
{
	
	private static TSNodeLabel initialSentence = initialSentence();    

	public static TSNodeLabel initialSentence() {
		try {
			return new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
    private ConstTreeViewer(ButtonIOComponentTable buttonIOComponentTable, ConstTreePanel constTreePanel, CheckBoxTable checkBoxTable) 
    {
        super(buttonIOComponentTable, constTreePanel, checkBoxTable);
        this.setupTreeViewer();
    }
    
    
    public static ConstTreeViewer createConstTreeViewer()
    {
    	//Set up the drawing area.
    	CheckBoxTable checkBoxTable = CheckBoxTable.createCheckBoxTable();
		ConstTreePanel constTreePanel = ConstTreePanel.createConstTreePanel(checkBoxTable);
		constTreePanel.setBackground(Color.white);
		constTreePanel.setFocusable(false);
    	
    	return new ConstTreeViewer(ButtonIOComponentTable.createButtonIOComponentTable(), constTreePanel, checkBoxTable);
    }
    
    
    public void setInitialSentence() {
    	ArrayList<TSNodeLabel> treebank = new ArrayList<TSNodeLabel>();
        treebank.add(initialSentence);
        loadTreebank(treebank, true);
    }
     
    
	protected void loadTreebank(File treebankFile, boolean resetSpinner) {                		
		corpusFile = treebankFile;
		ArrayList<TSNodeLabel> treebank = null;
		try {
			treebank = Wsj.getTreebank(treebankFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadTreebank(treebank, resetSpinner);
	}
	
	@Override
	protected void createInputFields() {};
	
	protected void initializeTreePanel() 
	{
		//((ConstTreePanel)treePanel).skewedLinesCheckBox = optionCheckBoxs[0];
		//((ConstTreePanel)treePanel).showHeads = optionCheckBoxs[1];	
	}
    
    
    public TreePanel<TSNodeLabel> getTreePanel()
    {
    	return this.treePanel;
    }
    
    
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static ConstTreeViewer createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Constituency Tree Viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final ConstTreeViewer treeViewer = ConstTreeViewer.createConstTreeViewer();
        Dimension area = treeViewer.treePanel.getArea();
        frame.setMinimumSize(new Dimension(area.width+5, area.height+5));
        treeViewer.setOpaque(true); //content panes must be opaque
        frame.setContentPane(treeViewer);
        
        frame.addWindowListener (new WindowAdapter () {
            public void windowClosing (WindowEvent e) {
            	treeViewer.doBeforeClosing();
            }
        });

        
      //Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");    
        //JMenuItem exportAllToXml = new JMenuItem("Export All to XML");
   
        MenuInteraction menuInteraction = MenuInteraction.createMenuInteraction(treeViewer.treePanel);
        menuInteraction.addExportTreeItems(treeViewer.treePanel,"Tree");
        menuInteraction.addLoadTreeItem(treeViewer, "Load TreeBank");
        menuInteraction.addItemsToMenu(menu);
        menuInteraction.addQuitter(treeViewer);

        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        return treeViewer;
    }
	
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.    	
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

}
