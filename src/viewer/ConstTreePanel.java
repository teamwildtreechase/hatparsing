/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.List;
import javax.swing.JCheckBox;
import tsg.TSNodeLabel;
import viewer_io.CheckBoxTable;

@SuppressWarnings("serial")
public class ConstTreePanel extends TreePanel<TSNodeLabel> 
{	
	
	TreeVisualizer<?> treeVisualizer;

	
	public JCheckBox skewedLinesCheckBox;
	public JCheckBox showHeads;
	
	
	
	private ConstTreePanel(CheckBoxTable checkBoxTable, PanelSizeProperties panelSizeProperties) 
	{
		super(checkBoxTable, panelSizeProperties);
		treeVisualizer = TreeVisualizer.createSourceTreeVisualizer(this, EmptyExtraTreeNodePropertiesVisualizer.createEmptyExtraTreeNodePropertiesVisualizer());
		
	}
	
	public static ConstTreePanel createConstTreePanel(CheckBoxTable checkBoxTable) 
	{
		PanelSizeProperties panelSizeProperties = new PanelSizeProperties();
		ConstTreePanel result = new  ConstTreePanel(checkBoxTable,panelSizeProperties);
		return result;
	}
	
	public static ConstTreePanel createConstTreePanel(CheckBoxTable checkBoxTable, List<TSNodeLabel> treebank)
	{
		ConstTreePanel result = createConstTreePanel(checkBoxTable);
		result.loadTreebank(treebank);
		return result;
	}
	
	
	@Override
	public void render(Graphics2D g2) {
		g2.setFont(getPanelSizeProperties().font()); 
        g2.setColor(Color.black);      
        g2.setStroke(new BasicStroke());
        //g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        //		RenderingHints.VALUE_ANTIALIAS_ON);
        //g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
        //  RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //g2.setRenderingHint(RenderingHints.KEY_RENDERING,
        //		RenderingHints.VALUE_RENDER_QUALITY);        
        //g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
        //        RenderingHints.VALUE_INTERPOLATION_BICUBIC);                                   
            		
        
        System.out.println("ConstTreePanel.render() called");
        
        treeVisualizer.drawTree(g2,0, false);
        //treeVisualizer.drawTreeUpsedeDown(g2,false,treeVisualizer.treeHeight);
        
	}
	
	protected void paintComponent(Graphics g) {
        super.paintComponent(g);        
        Graphics2D g2 = (Graphics2D) g;
        render(g2);
    }
	    	
    public void loadSentence() {
    	
      	if(getSentenceNumber() < getTreebank().size())
    	{ 		
	    	TSNodeLabel t = getTreebank().get(getSentenceNumber());    	
	 		treeVisualizer.setTree(t);
    	}	
    	
	
		getArea().width = treeVisualizer.treeWidth;
        getArea().height = treeVisualizer.getTreeHeight();
    }
    
    
	public TSNodeLabel getSentence(int n) 
	{
		return getTreebank().get(n);
	}

	@Override
	public HashMap<String, Integer> getVarNumTable() {
		return null;
	}
	
	


}
