/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */

package viewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import viewer_io.ButtonIOComponent;
import viewer_io.CheckBoxTable;
import viewer_io.SpinnerPanel;


/** Class responsible for rendering both the tree and the menu and buttons, in other words the 
 * whole application*/

public  abstract class AlignmentViewer extends JPanel implements ChangeListener, ActionListener, MouseListener {
	
	private static final long serialVersionUID = 1L;

	protected AlignmentPanel alignmentPanel;
	protected File corpusFile;	
	
	protected JPanel controlPanel;
	protected JButton plus;
	protected JButton minus;

	protected JLabel statusBar;
		
	protected CheckBoxTable chekBoxTable;
	protected ButtonIOComponent[] ioComponents;
	protected JTextArea outputField;
	
	protected SpinnerPanel alignmentTripleSpinnerPanel;
	
	protected AlignmentViewer(CheckBoxTable checkBoxTable, AlignmentPanel alignmentPanel) {
		super(new BorderLayout());
		this.chekBoxTable = checkBoxTable;
		this.alignmentPanel = alignmentPanel;
		
		System.out.println("Entered AlignmentViewer Constructor...");
		setViewerOptions();

		buildComponents();		
		
		//Put the drawing area in a scroll pane.
		
		//JScrollPane scroller = new JScrollPane(extraPanel);
        JScrollPane scroller = new JScrollPane(alignmentPanel);
        scroller.setPreferredSize(new Dimension(800,600));

        System.out.println("scroller.getBackground()"+ scroller.getBackground());
        
        System.out.println("alignmentPanel.getBackground()" + alignmentPanel.getBackground());
        
        //Lay out        
        statusBar = new JLabel("");
        add(controlPanel, BorderLayout.NORTH);
        add(scroller, BorderLayout.CENTER);
        add(statusBar, BorderLayout.SOUTH);
        
 
        //alignmentPanel.init();
	}
	
	
	
	protected  abstract void setViewerOptions();
	
	
	public AlignmentPanel getAlignmentPanel()
	{
		return this.alignmentPanel;
	}
	
	
    protected void buildComponents() {
    	
    	System.out.println("AlignmentViewer.buildComponents...");
    	
    	//Set up control panel  
    	alignmentTripleSpinnerPanel = SpinnerPanel.createSpinnerPanel(this, "Alignment triple: ");
        
        // main control panel
    	controlPanel = new JPanel(new BorderLayout());
        controlPanel.setFocusable(false);
        
        // control panel for upper part, containing everything except checkboxes
        JPanel upperPanel = new JPanel();        
        upperPanel.setLayout(new BoxLayout(upperPanel,BoxLayout.X_AXIS));
        
        JPanel leftControlPanel = new JPanel();
        leftControlPanel.setLayout(new BoxLayout(leftControlPanel,BoxLayout.Y_AXIS));
        JPanel centerControlPanel = new JPanel();
        JPanel zoomBrowseControlPanel = new JPanel(new BorderLayout());
        JPanel southControlPanel = new JPanel();
        JPanel rightControlPanel = new JPanel();
        ArrayList<JPanel> ioComponentPanels = new  ArrayList<JPanel>();
        upperPanel.add(leftControlPanel);
         
        zoomBrowseControlPanel.add(centerControlPanel, BorderLayout.CENTER);
        zoomBrowseControlPanel.add(rightControlPanel, BorderLayout.EAST);
        outputField = new JTextArea();
        outputField.setPreferredSize(new Dimension(100,35));
        zoomBrowseControlPanel.add(outputField, BorderLayout.SOUTH);
        upperPanel.add(zoomBrowseControlPanel);
        
        controlPanel.add(upperPanel, BorderLayout.NORTH);
        controlPanel.add(southControlPanel, BorderLayout.SOUTH);
        
        System.out.println("optionCheckBoxs " + chekBoxTable.getCheckBoxes());
        
        if (chekBoxTable!=null) 
        {
        	System.out.println("optionsCheckBox not null!");
        	
        	for(JCheckBox cb : chekBoxTable.getCheckBoxes()) {
        		System.out.println("checkbox added to panel");
                cb.addActionListener(this);
                cb.setFocusable(false);                
                southControlPanel.add(cb);                
        	}
        }        
        
        if(ioComponents != null)
        {	

        	      
        	for(ButtonIOComponent ioComponent : ioComponents) 
        	{    
        		if(!((ioComponent.getButtonLabel() !=  null) ))
        		{	
        			JPanel ioComponentPanel = new JPanel();
        			
        			ioComponent.addToPanel(ioComponentPanel);
        			
        			
	        		if(ioComponent.getTheButton() != null)
	        		{
	        			ioComponent.getTheButton().addActionListener(this); 
	        		}
	        		
	        	
	        		ioComponentPanels.add(ioComponentPanel);
	        		leftControlPanel.add(ioComponentPanel);
	        		
        		}
        
        		
		    
		        
        	}          
        }
        
        plus = new JButton("+");                
        minus = new JButton("-");
        plus.addActionListener(this);        
        minus.addActionListener(this);
        plus.setFocusable(false);
        minus.setFocusable(false);
        centerControlPanel.add(new JLabel("Zoom: "));
        centerControlPanel.add(plus);
        centerControlPanel.add(minus);        
        
        rightControlPanel.add(alignmentTripleSpinnerPanel);  
  
    }
    
    
	public void stateChanged(ChangeEvent e) {
		Integer s = alignmentTripleSpinnerPanel.getSpinnerValue();
		int index = s-1;
		alignmentPanel.goToSentence(index);		
		//statusBar.setText(treebankComments.get(index));
		alignmentPanel.init(); 
		statusBar.setText(alignmentPanel.flatSentence);
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if (chekBoxTable!=null) {
        	for(JCheckBox cb : chekBoxTable.getCheckBoxes()) {
        		if (source==cb) {
        			alignmentPanel.init();
        			//controlSentenceNumber.requestFocus();
        			return;
        		}                
        	}
        }

		int fs;
		if (source==plus) {
			fs = alignmentPanel.panelSizeProperties.increaseFontSize();
			System.out.println("Font size: " + fs);
		}
		else if(source == minus)
		{
			fs = alignmentPanel.panelSizeProperties.decreaseFontSize();
			System.out.println("Font size: " + fs);
		}
	
		
		alignmentPanel.init();
		//controlSentenceNumber.requestFocus();
	}
	
	
}
