/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
 
public class ColorScan {
    JPanel component;
    JLabel imageLabel;
    JLabel xLabel;
    JLabel yLabel;
    JLabel redLabel;
    JLabel greenLabel;
    JLabel blueLabel;
 
    private JPanel getContent() {
        JPanel panel = new JPanel(new GridLayout(1,0));
        panel.add(getComponent());
        imageLabel = new JLabel();
        imageLabel.addMouseMotionListener(mml);
        panel.add(imageLabel);
        return panel;
    }
 
    private JPanel getComponent() {
        component = new JPanel(new GridBagLayout()) {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D)g;
                int w = getWidth();
                int h = getHeight();
                g2.setPaint(new GradientPaint(0, 0, new Color(200,200,130),
                                              w, h, new Color(200,150,200)));
                g2.fillRect(0,0,w,h);
            }
        };
        component.setBorder(BorderFactory.createLineBorder(Color.red));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5,5,5,5);
        gbc.weighty = 1.0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        component.add(new JTextField(12), gbc);
        component.add(new JButton("Button"), gbc);
        JPanel child = new JPanel();
        child .setPreferredSize(new Dimension(160,50));
        child.setBackground(Color.cyan);
        component.add(child, gbc);
        return component;
    }
 
    private void setImage() {
        // Give the gui a chance to be displayed and to
        // settle down before we take a snapshot of it.
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(400);
                } catch(InterruptedException e) {
                    System.out.println("interrupted");
                }
                BufferedImage image = getImage();
                imageLabel.setIcon(new ImageIcon(image));
            }
        });
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
    }
 
    private BufferedImage getImage() {
        int w = component.getWidth();
        int h = component.getHeight();
        int type = BufferedImage.TYPE_INT_RGB;
        BufferedImage image = new BufferedImage(w, h, type);
        Graphics2D g2 = image.createGraphics();
        component.paint(g2);
        g2.dispose();
        return image;
    }
 
    private JPanel getLabelPanel() {
        JPanel panel = new JPanel(new GridLayout(1,0));
        panel.add(getPositionPanel());
        panel.add(getColorPanel());
        return panel;
    }
 
    private JPanel getPositionPanel() {
        xLabel = new JLabel(" ");
        yLabel = new JLabel(" ");
        Dimension d = new Dimension(45, 25);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2,2,2,2);
        gbc.weightx = 1.0;
        addComponents(new JLabel("x"), xLabel, panel, gbc, d);
        addComponents(new JLabel("y"), yLabel, panel, gbc, d);
        return panel;
    }
 
    private JPanel getColorPanel() {
        redLabel   = new JLabel(" ");
        greenLabel = new JLabel(" ");
        blueLabel  = new JLabel(" ");
        Dimension d = new Dimension(45, 25);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2,2,2,2);
        gbc.weightx = 1.0;
        addComponents(new JLabel("r"), redLabel,   panel, gbc, d);
        addComponents(new JLabel("g"), greenLabel, panel, gbc, d);
        addComponents(new JLabel("b"), blueLabel,  panel, gbc, d);
        return panel;
    }
 
    private void addComponents(JComponent c1, JComponent c2, Container c,
                               GridBagConstraints gbc, Dimension d) {
        gbc.anchor = GridBagConstraints.EAST;
        c.add(c1, gbc);
        c2.setPreferredSize(d);
        gbc.anchor = GridBagConstraints.WEST;
        c.add(c2, gbc);
    }
 
    public static void main(String[] args) {
        ColorScan test = new ColorScan();
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(test.getContent());
        f.getContentPane().add(test.getLabelPanel(), "Last");
        f.setSize(500,400);
        f.setLocation(100,200);
        f.setVisible(true);
        test.setImage();
    }
 
    private MouseMotionListener mml = new MouseMotionAdapter() {
        public void mouseMoved(MouseEvent e) {
            Point p = e.getPoint();
            xLabel.setText(String.valueOf(p.x));
            yLabel.setText(String.valueOf(p.y));
            int rgb = getColorValue(p);
            redLabel.setText(String.valueOf((rgb>>16)& 0xff));
            greenLabel.setText(String.valueOf((rgb>>8)& 0xff));
            blueLabel.setText(String.valueOf(rgb & 0xff));
        }
    };
 
    private int getColorValue(Point p) {
        if(imageLabel.getIcon() == null)
            return 0;
        BufferedImage image =
            (BufferedImage)((ImageIcon)imageLabel.getIcon()).getImage();
        if(p.x < image.getWidth() && p.y < image.getHeight()) {
            try {
                return image.getRGB(p.x, p.y);
            } catch(ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage() + " for p = [" +
                                   p.x + ", " + p.y + "]");
            }
        }
        return 0;
    }
}
