/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import alignment.Alignment;
import alignment.AlignmentTripleData;
import alignment.CorpusTreeReordering;
import alignment.TreeReordering;
import tsg.TSNodeLabel;
import viewer_io.ATPCheckBoxTable;
import viewer_io.SentencesAndAlignmentsLoader;


@SuppressWarnings("serial")
/** This is the main panel that generates the actual contents for the application
 * 
 */
public class AlignmentTreePanel extends TreePanel<TSNodeLabel> implements SentencesAndAlignmentsLoader 
{	
	
	AlignmentTripleData alignmentTripleData;
	
	TreeAlignmentVisualizer theTreeVisualizer;
	TreeAlignmentVisualizer theReorderedTreeVisualizer;

	private AlignmentTripleRenderer alignmentTripleRenderer;
	
	int wordsYPosition, alignmentsOnlyWordYPos;
	int alignmentGraphHeight;
	
	
	private ATPCheckBoxTable checkBoxTable;
	
	// static affay of colors that can be used to draw alignments
	public final static Color[] alignmentColors = {Color.blue, Color.cyan, Color.green, Color.magenta, Color.orange, Color.pink,Color.yellow};
	public final static Color[] alignmentGrayColors = {Color.black, Color.gray, Color.lightGray};
	private int currentAlignmentColorIndex;


	
	//CheckBox that allows to choose whether or not to ignore nulls in the reordering algorithm
	//public JCheckBox ignoreNullsInReorderingCheckBox; 

		
	int topMargin = 20;
	int yOffset = 0;
	
	// Names of the treebankFile and alignmentsFile used in oracle reordering
	String  oracleTreeFileName, oracleAlignmentsFileName;
	
	private AlignmentTreePanel(AlignmentTripleData alignmentTripleData, ATPCheckBoxTable checkBoxTable, PanelSizeProperties panelSizeProperties, AlignmentTripleRenderer alignmementTripleRenderer) 
	{
		super(checkBoxTable, panelSizeProperties);
		this.getPanelSizeProperties().setLevelSize(2);
		this.getPanelSizeProperties().setLevelSizeFactor(2);
		this.getPanelSizeProperties().setWordSpace(40);
		this.alignmentTripleData = alignmentTripleData;
		this.checkBoxTable = checkBoxTable;
		this.theTreeVisualizer = TreeAlignmentVisualizer.createTreeAlignmentVisualizer(this, false);
		this.theReorderedTreeVisualizer = TreeAlignmentVisualizer.createTreeAlignmentVisualizer(this, false);
		this.alignmentTripleRenderer = alignmementTripleRenderer;
		
		this.setBackground(Color.white);
	    this.setFocusable(false);
		System.out.println(" AlignmentTreePanel default constructor returns");
	}
	
	
	public static AlignmentTreePanel createAlignmentTreePanel(ATPCheckBoxTable checkBoxTable)
	{
		AlignmentTripleData alignmentTripleData = new AlignmentTripleData();
		PanelSizeProperties panelSizeProperties = new PanelSizeProperties();
		AlignmentTripleRenderer alignmentTripleRenderer = AlignmentTripleRenderer.createAlignmentTripleRenderer(checkBoxTable, panelSizeProperties);
		return new AlignmentTreePanel(alignmentTripleData, checkBoxTable, panelSizeProperties,alignmentTripleRenderer);
	}
	
	
	public ATPCheckBoxTable getCheckBoxTable()
	{
		return this.checkBoxTable;
	}
	
	

	/** 
	 * Method that finds the closest node, looking in both the theTreeVisualizer and the theReorderedTreeVisualizer
	 * @param p the point in the GUI to find the closes node for
	 * @return the closest node
	 */
	private TSNodeLabel findClosestNode(Point p)
	{
		NodeLocation closestNL1 = theTreeVisualizer.findClosestNodeLocation(p);
		
		Point offsetCompensatedPoint = (Point) p.clone();
		offsetCompensatedPoint.y = offsetCompensatedPoint.y - yOffset;
		NodeLocation closestNL2 = theReorderedTreeVisualizer.findClosestNodeLocation(offsetCompensatedPoint);
		
		
		double distanceCNL1 = NodeLocation.betweenPointDistance( closestNL1.getLocation(),p);
		double distanceCNL2 = NodeLocation.betweenPointDistance( closestNL2.getLocation(),offsetCompensatedPoint);		
		
		if(distanceCNL1 < distanceCNL2)
		{	
			TSNodeLabel closestNode = closestNL1.getNode();
			return closestNode;
		}
		else
		{
			TSNodeLabel closestNode = closestNL2.getNode();
			return closestNode;	
		}
		
	}
	
	/**
	 * Method that generates description of node for the GUI
	 * @param node the node to generate the description for
	 * @return description
	 */
	private String guiNodeDescription(TSNodeLabel node)
	{
		String s = node.toStringOneLevel();
		s += "\nAlignmentSpan: [" + node.spanMin + "," + node.spanMax + "]";
		s += " numUnalignedDescendants: " + node.getNumUnalignedGovernedTerminals();
		return s;
	}
	
	/**
	 * Method that generates description for closest node to a point in the GUI
	 * @param p
	 * @return description
	 */
	public String guiClosestNodeDescription(Point p)
	{
		TSNodeLabel closestNode = findClosestNode(p);
		String result = "Closest node:\t" + guiNodeDescription(closestNode);
		System.out.println(result);
		return result;
	}
	
	
	/** 
	 * Method that sets the target sentences for the panel
	 * @param targetSentences
	 */
	public void loadTargetSentences(List<List<String>>  targetSentences) 
	{
		this.alignmentTripleData.setTargetSentences(targetSentences);
		lastIndex = Math.max(lastIndex ,targetSentences.size() - 1);
		System.out.println(">>TargetSentences loaded");
		init(); //repaint everything
	}
	
	/** 
	 * Method that sets the source sentences for the panel
	 * @param sourceSentences
	 */
	public void loadSourceSentences(List<List<String>>  sourceSentences) 
	{
		this.alignmentTripleData.setSourceSentences(sourceSentences);
		System.out.println(">>SourceSentences loaded");
		init(); //repaint everything
	}


	
	public void init() {
		this.getPanelSizeProperties().loadFont(this);
		loadSentence();
        setPreferredSize(getArea());
        revalidate();
        repaint();
	}
	
	@Override
	public void render(Graphics2D g2) {
		g2.setFont(this.getPanelSizeProperties().font()); 
        g2.setColor(Color.black);      
        g2.setStroke(new BasicStroke());
        
        
    	// reset index of the current alignment color, so that the first word will 
		// be painted the same color every time, and no funny repainting bugs can occur
		currentAlignmentColorIndex = 0;
		
		// output is well defined if the tree is available or not requested
		boolean treeExists =  (getSentenceNumber() < getTreebank().size());	
        
		// Show only alignments if we selected that option
		if(checkBoxTable.showOnlyAlignments())
		{
			this.alignmentTripleRenderer.render(g2, true);
		}
		// else show also the tree, provided it exists, else show nothing at all
		else if (treeExists && (this.theTreeVisualizer.getTheTreeStruct() != null))
		{	
			renderTreeAndAlignments(g2);
		}	

		
	}
	
	
	public void renderTreeAndAlignments(Graphics2D g2)
	{
	
		yOffset = 0;
		renderTreeAlignmentVisualizer(g2,this.theTreeVisualizer, yOffset);
					
		// invert the order of the children of the root
		//theReorderedTreeVisualizer.invertChildrenOrder(theReorderedTreeVisualizer.theTree);
		// recompute the visualization after the reordering was done
		if(checkBoxTable.showTreeReordering())
		{	
			// We take as offset for the second tree
			//System.out.println(">>> alignmentGraphHeight:" + alignmentGraphHeight  + "topMargin:" + topMargin);
			yOffset = alignmentGraphHeight - 2 *  (topMargin);
			getArea().height = 2 * alignmentGraphHeight;		
			renderTreeAlignmentVisualizer(g2,this.theReorderedTreeVisualizer,  yOffset);
		}
	}	
	
	/**
	 *  Method that renders the Alignment graph
	 * @param g2 the graphics object
	 * @param tav the tree alignment visualizer
	 * @param yOffset  An offset, to be used to visualize multiple alignment graphs below each other
	 */
	public void renderTreeAlignmentVisualizer(Graphics2D g2, TreeAlignmentVisualizer tav, int  yOffset)
	{
	
	
		// Draw the source words
		BasicDrawing.drawWords(tav.leafDescriptionData,topMargin + yOffset, g2);
		
		// draw the tree
		tav.drawTree(g2,  yOffset);
	
		if (this.alignmentTripleRenderer.getTargetRenderData() != null) 
		{
			// determine the target words yPosition depending on whether we are drawing the tree or only the alignments
			int yPosition = wordsYPosition;
			
			BasicDrawing.drawWords(this.alignmentTripleRenderer.getTargetRenderData(),yPosition + yOffset,  g2);
		}
	
		
		if((this.alignmentTripleRenderer.sentenceAlignments != null) && (!checkBoxTable.showOnlyAlignments()) && (getSentenceNumber() < getTreebank().size()))
		{	
			
			//System.out.println(">>> AlignmentTreePanel entered alignments drawing block...");
			
			// Now retrieve the words and then the positions in the source 
			// were this word is aligned to and draw a line between the two
			for(int i = 0; i < tav.getTheTreeStruct().getNumLexicalItems(); i++)
			{
				try
				{
					TSNodeLabel sourceNode = tav.getTheTreeStruct().getLexicalItem(i);
					//System.out.println("sourceNode != null: " + (sourceNode != null));
					
					//TSNodeLabel sourceNode = theTreeVisualizer.getSourceNodeAtIndex(a.getSourcePos());
					int[] sourceNodePos = tav.getNodeBottomPosition(sourceNode, false);
					int sourceXPos = sourceNodePos[0]; 
					int sourceYPos = sourceNodePos[1] + 3;
					
					//System.out.println("Source Draw position: " +  sourceXPos +" ," +  sourceYPos);
					//System.out.println("TargetAlignment: " + sourceNode.targetAlignment);
					
					if(sourceNode.targetAlignments !=  null)												
					{
						for(int targetAlignment : sourceNode.targetAlignments)															
						{								
							//System.out.println(">>> targetAlignment: " + targetAlignment);
							
							int targetXPos = this.alignmentTripleRenderer.getTargetRenderData().getXMiddlePos(targetAlignment);
							//int targetXPos = wordTargetXMiddleArray[a.getTargetPos()];
							int targetYPos = wordsYPosition - 10;
							
							// Draw the line of the alignment between the position of the source leaf word 
							// and the corresponding target word										
							
							// if the source node violates the alignment of another subtree, draw it striped
							boolean striped = (sourceNode.violatesSourceSubtreeAlignment && checkBoxTable.showTreeAlignmentViolations());
							this.drawAlignmentLine(sourceXPos, sourceYPos + yOffset, targetXPos, targetYPos + yOffset, g2, striped);
						}
					}
					else
					{
						//System.out.println("sourcenode.TargetAlignments is null!!!");
					}
			
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Error finding one of the nodes for the alignment, possibly mismatch between tree and alignment: " + e);
				}							
			}
				
		}
	}
	
	
	/**
	 *  Method that increments the index of the current alignment color, which loops through a 
	 *  round robing list of possible colors
	 */	
	public void incrementAlignmentColorIndex()
	{
		if(!this.checkBoxTable.noColors())
		{
			currentAlignmentColorIndex = (currentAlignmentColorIndex + 1) % alignmentColors.length;
		}
		else
		{	
			currentAlignmentColorIndex = (currentAlignmentColorIndex + 1) % alignmentGrayColors.length;
		}	
	}
	
	/**
	 * Get the current alignment color
	 */
	public Color getCurrentAlignmentColor()
	{
		if(!this.checkBoxTable.noColors())
		{	
			// extract the color for this alignment
			return alignmentColors[currentAlignmentColorIndex];
		}
		else
		{	
			return alignmentGrayColors[currentAlignmentColorIndex];
		}	
	}
	
	
	protected void paintComponent(Graphics g) {     
		super.paintComponent(g);   // This must be called first otherwise nothing is drawn!!!
		Graphics2D g2 = (Graphics2D) g;
        render(g2);
    }
	    	
	
	public void setOracleReorderingFileNames(String oracleTreeFileName, String oracleAlignmentsName)
	{
		this.oracleAlignmentsFileName = oracleAlignmentsName;
		this.oracleTreeFileName = oracleTreeFileName;
	}
	
	/** Function that writes out a file with the source sentences in reordered form
	 * 
	 * @param fileName the name of the file to write the output to
	 */
	public void produceReorderedSourceFile(String stringOutputFileName, String treeOutputFileName, String logFileName, boolean outputPerSentence, boolean writeReorderingLabels)
	{
		CorpusTreeReordering corpusTreeReordering = CorpusTreeReordering.createCorpusTreeReordering( this.checkBoxTable.ignoreNullsInReordering(), outputPerSentence, writeReorderingLabels);
		corpusTreeReordering.produceReorderedSourceFile(stringOutputFileName,treeOutputFileName,logFileName, this.oracleTreeFileName, this.oracleAlignmentsFileName, 0);
	}
	
	
	public void loadSentence()
	{
    	loadSentence(this.alignmentTripleData.getAlignment(getSentenceNumber()),getTreebank().get(getSentenceNumber()));
	}
	
	/**
	 * Method that loads the next sentence, loading the source sentence, target sentence,
	 * tree and alignments, each one optionally and only if it is available,  
	 * and initializing their associated visualization components
	 * 
	 *  This method was made to allow external specification of an alignment and tree 
	 *  to be processed, rather than being restricted to a treebank that has to be then 
	 *  pre-loaded as a whole, leading to problems when it gets to big to fit into memory.
	 */
    public void loadSentence(Alignment alignment, TSNodeLabel tArg) 
    {
    	System.out.println("AlignmentTreePanel.loadSentence() called");
    	
    	
    	int maxTargetXPos = 0;
    			
		List<String> targetWords = this.alignmentTripleData.getTargetSentence(getSentenceNumber());
	    List<String> sourceWords = this.alignmentTripleData.getSourceSentence(getSentenceNumber());
		
		this.alignmentTripleRenderer.loadSentence(sourceWords, targetWords, alignment,true);
		

		
    	if(tArg != null)
    	{ 		
	    	TSNodeLabel t = tArg;    	
	 		theTreeVisualizer.setTree(t);
	 		
	 		// make a recursive copy of the entire tree and give this to the 
	 		// reordered tree visualizer
	 		theReorderedTreeVisualizer.setTree(t.clone());
	 		
	 	
	 		// Perform a second consistency check as well, making sure the alignments correspond with the trees - or at least are likely to do so
			if(alignment != null && (this.alignmentTripleData.getNumAlignments()  == getTreebank().size()))
			{
				setTreeAlignments();
				System.out.println("setTreeAlignments() called");
			}
			
			//System.out.println("sentenceAlignments" + sentenceAlignments + "this.alignmentTripleData.getNumAlignments()" + this.alignmentTripleData.getNumAlignments() + " getTreebank().size() " + getTreebank().size());  
			
			theTreeVisualizer.leafDescriptionData = SentenceRenderData.createSentenceRenderData(theTreeVisualizer.getTheTreeStruct().getSourceWordArray(), getPanelSizeProperties());
			
			// repeat for reordered tree	    	
			theReorderedTreeVisualizer.leafDescriptionData = SentenceRenderData.createSentenceRenderData(theReorderedTreeVisualizer.getTheTreeStruct().getSourceWordArray(), getPanelSizeProperties());
    	}
    
		alignmentGraphHeight = theTreeVisualizer.getTreeHeight() + 2 * this.getPanelSizeProperties().levelSize();
		wordsYPosition =  alignmentGraphHeight; 
		
		alignmentGraphHeight += getPanelSizeProperties().bottomMargin();
		// Take for the panel width the maximimum of the tree width and the 
		// width of the target (Dutch) sentence
		getArea().width = Math.max(theTreeVisualizer.treeWidth, maxTargetXPos);
        getArea().height = alignmentGraphHeight; 
        
    }
       

    public void setTreeAlignments()
    {
    	
		// set the alignments in the TreeAlignmentVisualizer
		theTreeVisualizer.setAlignmentsAndComputeConsistency(this.alignmentTripleRenderer.sentenceAlignments);
		
		// Repeat the previous step for the reordered tree
		theReorderedTreeVisualizer.setAlignmentsAndComputeConsistency(this.alignmentTripleRenderer.sentenceAlignments);
		
		// Reorder the tree to match the target order as well as possible,
 		// under the tree child permutation reordering constraint
		TreeReordering tr = new TreeReordering(theReorderedTreeVisualizer.getTheTreeStruct());
 		boolean orderChanged = tr.performReordering(this.checkBoxTable.ignoreNullsInReordering());
 		System.out.print("orderChanged: " + orderChanged);
 		theReorderedTreeVisualizer.recomputeTreeVisualization();
    }	
    
    
	public TSNodeLabel getSentence(int n) 
	{
		return getTreebank().get(n);
	}
	
	/** Wrapper method that draws an alignment line using the BasicDrawing class
	 *  
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param g2
	 * @param striped
	 */
	public void drawAlignmentLine(int x1, int y1, int x2, int y2, Graphics2D g2, boolean striped) 
	{
			// extract the color for this alignment
			g2.setColor(this. getCurrentAlignmentColor());
			// increment the currentAlignmentColorIndex. Once the maximum index is reached, start over from 0
			this.incrementAlignmentColorIndex();
				
			BasicDrawing.drawAlignmentLine(x1, y1, x2, y2, g2, striped);
			
			g2.setColor(Color.black);
	}


	/** 
	 * Method that sets the alignments for the panel
	 * @param alignments An Arraylist of Arraylists of Alignment pairs, one ArrayList
	 * with alignment pairs for every sentence
	 */
	@Override
	public void loadAlignments(List<Alignment> alignments) 
	{
		this.alignmentTripleData.setAlignment(alignments);
		System.out.println(">>Alignments loaded");
		init(); //repaint everything
		
	}


	@Override
	public HashMap<String, Integer> getVarNumTable() {
		return null;
	}

	
	/** Wrapper methods that draws a line using the BasicDrawing Class
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param g2
	 */
	/*
	public void drawLine(int x1, int y1, int x2, int y2, Graphics2D g2) 
	{
		BasicDrawing.drawLine(x1,y1,x2, y2, g2, checkBoxTable.useSkewedLines()); 
	}
	*/




}
