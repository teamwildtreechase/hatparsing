/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ZoomPanel extends JPanel implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel zoomLabel;
	private JButton plus;
	private JButton minus;
	private ZoomInterface zoomInterface;

	
	private ZoomPanel(ZoomInterface zoomInterface, JButton plus, JButton minus, JLabel zoomLabel)
	{
		super();
		this.zoomInterface = zoomInterface;
		this.plus = plus;
		this.minus = minus;
		this.zoomLabel = zoomLabel;
		
        this.add(this.zoomLabel);
        this.add(this.plus);
        this.add(this.minus);
        this.addActionListeners();
	}
	
	public static ZoomPanel createZoomPanel(ZoomInterface zoomInterface)
	{
		 JButton plus = new JButton("+");                
		 JButton minus = new JButton("-");
		 JLabel zoomLabel = new JLabel("Zoom: ");
	     
	     plus.setFocusable(false);
	     minus.setFocusable(false);

	     return new ZoomPanel(zoomInterface, plus, minus, zoomLabel);
	}
	
	
	private void addActionListeners()
	{
		plus.addActionListener(this);        
	    minus.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		int fs;
		if ( e.getSource() == plus) {
			fs = zoomInterface.increaseFontSize();
			System.out.println("Font size: " + fs);
		}
		else if(e.getSource() == minus)
		{
			fs = zoomInterface.decreaseFontSize();
			System.out.println("Font size: " + fs);
		}
	}
	
	
}
