/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Image;
import javax.swing.*;
import java.awt.*;


public class MinimalisticPanel  extends JPanel 
{
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g) 
	    {
	        super.paintComponent(g); //paint background
	    
	        String imageFile = "/home/gemaille/public_html/reordering_visualization.png";

	        Image image = Toolkit.getDefaultToolkit().getImage(imageFile);

	        //Draw image at its natural size first.
	        
	        g.drawImage(image, 0,0, this);
	    }
}