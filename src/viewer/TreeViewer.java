/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */

package viewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.QuitInterface;
import viewer_io.TreebankLoaderInterface;
import viewer_io.SpinnerPanel;


/** Class responsible for rendering both the tree and the menu and buttons, in other words the 
 * whole application*/

public abstract class TreeViewer<T> extends JPanel implements ChangeListener, ActionListener, TreebankLoaderInterface<T>, QuitInterface{
	
	private static final long serialVersionUID = 1L;
    private static int MAX_CHECKBOXES_PER_ROW = 5;

	
	// We use a masterPanel to contain everything, so that space is available to add stuff later on
	protected JPanel masterPanel;
	
	// The component panel will contain the actual components or sreens, i.e. the trees or the alignments
	protected JPanel componentsPanel;
	
	protected TreePanel<T> treePanel;
	protected File corpusFile;	
	
	protected JScrollPane treeScroller;
	protected JPanel controlPanel;
	protected SpinnerPanel treeSpinnerPanel;
	protected JPanel rightControlPanel;
	protected ZoomPanel zoomPanel;

	protected JLabel statusBar;
	
	protected CheckBoxTable checkBoxTable;
	private ButtonIOComponentTable ioComponentsTable;
	private JTextArea outputField;
	
	protected final String SpinnerLabel = "Tree :";
	
	
	protected TreeViewer(ButtonIOComponentTable ioComponentsTable,TreePanel<T> treePanel, CheckBoxTable checkBoxTable)
	{
		super(new BorderLayout());
		this.checkBoxTable = checkBoxTable;
		this.checkBoxTable.addCheckBoxActionListeners((ActionListener) this);
		this.treePanel = treePanel;
		this.setIoComponentsTable(ioComponentsTable); 
	}
		
	
	public void setupTreeViewer()
	{
		
		masterPanel = new JPanel(new BorderLayout());
		
		//componentsPanel = new JPanel(new BorderLayout());
		componentsPanel =  new JPanel(new BorderLayout());
		
		System.out.println("0 : treePanel" + treePanel);
		createInputFields();
		System.out.println("1 : treePanel" + treePanel);
		zoomPanel = ZoomPanel.createZoomPanel(this.treePanel);
		buildComponents();	
		System.out.println("3 : treePanel" + treePanel);
		
		treePanel.setPreferredSize(new Dimension(800,600));
		treePanel.setVisible(true);
		
		System.out.println("treePanel" + treePanel);
		
	
		treeScroller = TreeViewer.createTreeScroller(this.treePanel);
                
        // componentsPanel for now just contains the trees, but has place for more panels
        componentsPanel.add(treeScroller,BorderLayout.SOUTH);
        
       
        //Lay out        
        statusBar = new JLabel("");
        masterPanel.add(controlPanel, BorderLayout.NORTH);
        masterPanel.add(componentsPanel, BorderLayout.CENTER);
       // masterPanel.add(statusBar, BorderLayout.SOUTH);

        
    	// Get the default toolkit
		Toolkit toolkit = Toolkit.getDefaultToolkit();

		// Get the current screen size
		Dimension screenSize = toolkit.getScreenSize();
		this.setPreferredSize(new Dimension( screenSize.width - 10, screenSize.height - 120));

        this.add(masterPanel,BorderLayout.NORTH);	

        setInitialSentence();
	}
	
	
	public  static <T> JScrollPane createTreeScroller(TreePanel<T> treePanel)
	{
		//Put the drawing area in a scroll pane.
        JScrollPane result = new JScrollPane(treePanel,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//JScrollPane scroller = new JScrollPane(treePanel,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		result.setPreferredSize(new Dimension(800,520));
        return result;
	}
	
	
	
	protected abstract void createInputFields();
	
	protected abstract void setInitialSentence();
		
	
	public TreePanel<T> getTreePanel()
	{
		return treePanel;
	}
	
	
	
	public void loadTreebank(List<T> treebank, boolean resetSpinner) {
		
		System.out.println("TreeViewer.loadTreeBank called...");
		int size = treebank.size();
		treePanel.loadTreebank(treebank);
		
		if(resetSpinner)
		{	
			this.treeSpinnerPanel.setSpinnerSize(size); 
		}
		
		treePanel.init();
		statusBar.setText(treePanel.flatSentence);
	 
	}
	
	protected String getSpinnerLabel()
	{
		return SpinnerLabel;
	}

	
    protected void buildComponents() 
    {
        // main control panel
    	controlPanel = new JPanel(new BorderLayout());
        controlPanel.setFocusable(false);
        
        // control panel for upper part, containing everything except checkboxes
        JPanel upperPanel = new JPanel();        
        upperPanel.setLayout(new BoxLayout(upperPanel,BoxLayout.X_AXIS));
        
        JPanel leftControlPanel =  createButtonIOComponentPanel(getIoComponentsTable(), this);
        
        JPanel checkBoxesPanel = createCheckBoxesPanel(checkBoxTable, this);
        
        treeSpinnerPanel = SpinnerPanel.createSpinnerPanel(this, getSpinnerLabel());
        rightControlPanel = new JPanel();

        rightControlPanel.setLayout(new BoxLayout(rightControlPanel,BoxLayout.X_AXIS));
        rightControlPanel.add(treeSpinnerPanel);
        rightControlPanel.setPreferredSize(new Dimension(400,70));

        
        upperPanel.add(leftControlPanel);
         

        JPanel zoomBrowseControlPanel = new JPanel(new BorderLayout());
        zoomBrowseControlPanel.add(rightControlPanel, BorderLayout.EAST);
        outputField = new JTextArea();
        outputField.setPreferredSize(new Dimension(10,70));
        zoomBrowseControlPanel.add(outputField, BorderLayout.SOUTH);
        upperPanel.add(zoomBrowseControlPanel);
        zoomBrowseControlPanel.add(zoomPanel, BorderLayout.CENTER);
        
        controlPanel.add(upperPanel, BorderLayout.NORTH);
        controlPanel.add(checkBoxesPanel, BorderLayout.SOUTH);
        
 
       
        
 
        
		System.out.println("1.5 : treePanel" + treePanel);
	
    }
    
protected void addCheckboxesToPanel(JPanel leftControlPanel ){
      
      JPanel southControlPanel = new JPanel();
      southControlPanel.setLayout(new BoxLayout(leftControlPanel,BoxLayout.X_AXIS));
      controlPanel.add(southControlPanel, BorderLayout.SOUTH);
      
      
    }   
    
    
    public static JPanel createCheckBoxesPanel(CheckBoxTable checkBoxTable, ActionListener containingPanel)
    {
    	JPanel result = new JPanel();
    	result.setLayout(new BoxLayout(result,BoxLayout.Y_AXIS));
    	
        if (checkBoxTable !=null) 
        {
          
          for(int i = 0; i < checkBoxTable.numCheckBoxes(); i+= MAX_CHECKBOXES_PER_ROW){     
            
            JPanel checkboxRowPanel = new JPanel();
            checkboxRowPanel.setLayout(new BoxLayout(checkboxRowPanel,BoxLayout.X_AXIS));
            result.add(checkboxRowPanel);
            System.out.println("optionCheckBoxs " + checkBoxTable.getCheckBoxes());
          
            for(int j = i; j < Math.min(i + MAX_CHECKBOXES_PER_ROW,checkBoxTable.numCheckBoxes()); j++){
              if (checkBoxTable!=null) 
              {
                System.out.println("optionsCheckBox not null!");            
                JCheckBox cb = checkBoxTable.getCheckBoxes().get(j);
                System.out.println("checkbox added to panel");
                cb.addActionListener(containingPanel);
                cb.setFocusable(false);                
                checkboxRowPanel.add(cb);                            
              }    
            }        
          }
          
          /*
          for(JCheckBox cb : checkBoxTable.getCheckBoxes()) {        		 
                cb.addActionListener(containingPanel);
                cb.setFocusable(false);                
                result.add(cb);                
        	}*/
        }        
        
        return result;
    }
    
    
    public static  JPanel createButtonIOComponentPanel(ButtonIOComponentTable ioComponentsTable, ActionListener containingPanel)
    {
    	
    	JPanel result = new JPanel();
    	result.setLayout(new BoxLayout(result,BoxLayout.Y_AXIS));
    	
    	 if(ioComponentsTable != null)
         {	
         	      
         	for(ButtonIOComponent ioComponent : ioComponentsTable.getComponents()) 
         	{    
         		if(!((ioComponent.getButtonLabel() !=  null) && ioComponent.hasTextField() ))
         		{	
         			JPanel ioComponentPanel = new JPanel();
         		  
         			ioComponent.addToPanel(ioComponentPanel);
         			
         			
 	        		if(ioComponent.getTheButton() != null)
 	        		{
 	        		
 	        			ioComponent.getTheButton().addActionListener(containingPanel); 
 	        		}
 	        		
 	        		 result.add(ioComponentPanel);
         		}
         	}          
         }
         
                
    	
    	return result;
    }
    
    
	public void stateChanged(ChangeEvent e) {
		Integer s = this.treeSpinnerPanel.getSpinnerValue();
		int index = s-1;
		treePanel.goToSentence(index);		
		//statusBar.setText(treebankComments.get(index));
		treePanel.init(); 
		statusBar.setText(treePanel.flatSentence);
	}

	public void actionPerformed(ActionEvent e) {
		
		System.out.println("TreeViewer: actionPerformed called...");
		
		Object source = e.getSource();
		
		if (checkBoxTable!=null) {
        	for(JCheckBox cb : checkBoxTable.getCheckBoxes()) {
        		if (source==cb) {
        			System.out.println("TreeViewer checkbox clicked ...");
        			treePanel.init();
        			//controlTreeNumber.requestFocus();
        			return;
        		}                
        	}
        }

	

	}
	
	public void setOutputFieldText(String text)
	{
		this.outputField.setText(text);
	}
	
	public void quit()
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
		    public void run()
		    {
		    	System.exit(0);
		    }
		});	
	}
	
	public void doBeforeClosing()
	{
		
	}


	public ButtonIOComponentTable getIoComponentsTable() {
		return ioComponentsTable;
	}


	public void setIoComponentsTable(ButtonIOComponentTable ioComponentsTable) {
		this.ioComponentsTable = ioComponentsTable;
	}
		
}
