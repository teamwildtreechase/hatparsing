/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import javax.swing.JCheckBox;

import tsg.TSNodeLabel;
import ccgLabeling.CCGLabel;
import extended_bitg.TreeRepresentation;

public class SourceHatNodeLabelExtracter extends NodeLabelsExtracter
{

	protected SourceHatNodeLabelExtracter(JCheckBox showHATReorderingLabelsCheckBox, JCheckBox showParentRelativeReorderingLabelsCheckBox,
	    JCheckBox showPreterminalLabelsCheckbox) {
		super(showHATReorderingLabelsCheckBox, showParentRelativeReorderingLabelsCheckBox,showPreterminalLabelsCheckbox);
	}

	@Override
	protected String getCCGLabel(TSNodeLabel n) 
	{
		return getSourceCCGLabel(n);
	}

	private String getSourceCCGLabel(TSNodeLabel n)
	{
		return n.getExtraLabel(CCGLabel.getCCGLabelProperty(TreeRepresentation.SourceCategory));
	}
	
}
