/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.io.File;
import java.util.List;
import tsg.TSNodeLabel;
import tsg.corpora.Wsj;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;


public abstract class SingleTreeViewer extends TreeViewer<TSNodeLabel>
{

	protected SingleTreeViewer(ButtonIOComponentTable ioComponentsTable, TreePanel<TSNodeLabel> treePanel, CheckBoxTable checkBoxTable) 
	{
		super(ioComponentsTable, treePanel, checkBoxTable);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public boolean loadTreebankAndSetSpinner(File file, boolean resetSpinner) 
	{
		List<TSNodeLabel> treeBank  = null;
		try 
		{
			treeBank = Wsj.getTreebank(file);
			super.loadTreebank(treeBank, resetSpinner);
			return true;
		} catch (Exception e) {
				
			e.printStackTrace();
			return false;
		}
		
	}

}
