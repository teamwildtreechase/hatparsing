/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JCheckBox;
import extended_bitg.HATPairPanel;
import extended_bitg.TreeRepresentation;
import tsg.TSNodeLabel;
import tsg.TreeStringConsistencyTester;
import viewer_io.EbitgCheckBoxTable;

/**
 * The class TreeVisualizer is responsible for rendering a tree. This includes computing the positions of all node markers, edges and labels. A tree can be
 * drawn upside down, which is implemented by allowing passing a boolean drawUpsideDown to the main rendering method drawTree.
 */
public class TreeVisualizer<PanelType> {
	private final boolean drawTreeUpsideDown;
	private TreeStruct theTreeStruct;

	ArrayList<NodeLocation> nodeLocations;

	ArrayList<TSNodeLabel> wrongNodes;
	List<NodeRenderPosition> nodeRenderPositions;

	SentenceRenderData leafDescriptionData;

	int treeWidth = 0;
	int sourceLanguageWordsYPos;

	public static int wrongNodesBorder = 2;
	TreePanel<PanelType> thePanel;

	private final NodeLabelsExtracter nodeLabelExtracter;
	private final ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected TreeVisualizer(TreePanel thePanel, boolean drawTreeUpsideDown, NodeLabelsExtracter nodeLabelExtracter, ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer) {
		this.thePanel = thePanel;
		this.thePanel.getPanelSizeProperties().setTopMargin(20);
		this.drawTreeUpsideDown = drawTreeUpsideDown;
		this.nodeLabelExtracter = nodeLabelExtracter;
		this.extraTreeNodePropertiesVisualizer = extraTreeNodePropertiesVisualizer;
	}

	public static <T, PanelType extends TreePanel<T>> TreeVisualizer<PanelType> createSourceTreeVisualizer(PanelType thePanel, ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizerl) {
		NodeLabelsExtracter nodeLabelExtracter = new SourceHatNodeLabelExtracter(new JCheckBox(),new JCheckBox(),new JCheckBox());
		return new TreeVisualizer<PanelType>(thePanel, false, nodeLabelExtracter, extraTreeNodePropertiesVisualizerl);
	}

	public static <T, PanelType extends TreePanel<T>> TreeVisualizer<PanelType> createTargetTreeVisualizer(PanelType thePanel, ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer) {
		NodeLabelsExtracter nodeLabelExtracter = new TargetHatNodeLabelExtracter(new JCheckBox(),new JCheckBox(),new JCheckBox());
		return new TreeVisualizer<PanelType>(thePanel, true, nodeLabelExtracter, extraTreeNodePropertiesVisualizer);
	}

	public static <T, PanelType extends TreePanel<T>> TreeVisualizer<PanelType> createSourceTreeVisualizer(PanelType thePanel, JCheckBox hatReorderingCheckBox, JCheckBox sourceRelativeReorderingCheckBox,
	    JCheckBox preterminalLabelsCheckbox, ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer) {
		NodeLabelsExtracter nodeLabelExtracter = new SourceHatNodeLabelExtracter(hatReorderingCheckBox, sourceRelativeReorderingCheckBox,preterminalLabelsCheckbox);
		return new TreeVisualizer<PanelType>(thePanel, false, nodeLabelExtracter, extraTreeNodePropertiesVisualizer);
	}

	public static <T, PanelType extends TreePanel<T>> TreeVisualizer<PanelType> createTargetTreeVisualizer(PanelType thePanel, JCheckBox hatReorderingCheckBox, JCheckBox sourceRelativeReorderingCheckBox,
	    JCheckBox showPreterminalLabelsCheckbox,ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer) {
		NodeLabelsExtracter nodeLabelExtracter = new TargetHatNodeLabelExtracter(hatReorderingCheckBox, sourceRelativeReorderingCheckBox,showPreterminalLabelsCheckbox);
		return new TreeVisualizer<PanelType>(thePanel, true, nodeLabelExtracter, extraTreeNodePropertiesVisualizer);
	}

	public NodeLocation findClosestNodeLocation(Point p) {
		NodeLocation closest = null;
		double minDistance = Integer.MAX_VALUE;

		for (NodeLocation l : nodeLocations) {
			double distance = l.computeDistance(p);

			// found a close node location
			if (distance < minDistance) {
				closest = l;
				minDistance = distance;
			}
		}

		return closest;
	}

	public void setLeafDescriptionWords(List<String> leafDescriptionWords1, PanelSizeProperties panelSizeProperties) {
		this.leafDescriptionData = SentenceRenderData.createSentenceRenderData(leafDescriptionWords1, panelSizeProperties);
	}

	void setTree(TSNodeLabel t) {
		this.setTheTreeStruct(new TreeStruct(t));
		recomputeTreeVisualization();
	}

	public void recomputeTreeVisualization() {

		initializeRenderingPositionArrays();

		this.computeAllNodesRenderPositions();

		this.generateNodeLocationsTable();

		sourceLanguageWordsYPos = ((int) thePanel.getPanelSizeProperties().fontHeight());

		treeWidth += thePanel.getPanelSizeProperties().rightMargin();

		// take the maximum of the sourecLanguageWords end the width of the tree
		treeWidth = Math.max(treeWidth, this.leafDescriptionLength());

	}

	public int leafDescriptionLength() {
		if (this.leafDescriptionData != null) {
			return this.leafDescriptionData.getSentenceRenderLength() + thePanel.getPanelSizeProperties().rightMargin();
		}
		return 0;
	}

	private void computeAllNodesRenderPositions() {
		computeLeafNodeRenderPositions();

		for (TSNodeLabel n : getTheTreeStruct().getNodesArray()) {
			if (n.isLexical)
				continue;
			int i = getTheTreeStruct().getIndexTable().get(n);
			updateValues(n, i);
		}
	}

	private void computeLeafNodeRenderPositions() {
		int accumulatedHorizontalLength = thePanel.getPanelSizeProperties().leftMargin();

		for (TSNodeLabel lexicalNode : getTheTreeStruct().getLexicals()) {
			int lexicalIndex = getTheTreeStruct().getIndexTable().get(lexicalNode);

			int wordLength = computeWordLength(lexicalNode);
			int labelLengthColumn = getTreeLeafColumnLabelWidth(lexicalNode);

			NodeRenderPosition position = nodeRenderPositions.get(lexicalIndex);
			position.setLabelLength(wordLength);
			position.setXMiddle(accumulatedHorizontalLength + labelLengthColumn / 2 + thePanel.getPanelSizeProperties().wordSpace());
			position.setY(computeRenderHeight(lexicalNode));
			position.setNumLabels(numLabels(lexicalNode));
			accumulatedHorizontalLength += labelLengthColumn + thePanel.getPanelSizeProperties().wordSpace();
		}

		treeWidth = accumulatedHorizontalLength + thePanel.getPanelSizeProperties().wordSpace();
	}

	private int computeRenderHeight(TSNodeLabel node) {
		int extraHeightForLeafDescriptionWords =  leafDescriptionHeight();
		int height = thePanel.getPanelSizeProperties().topMargin() + node.hight()  * thePanel.getPanelSizeProperties().levelSize() + extraHeightForLeafDescriptionWords;

		if (this.drawTreeUpsideDown) {
			//return getTreeHeight() - height;
			return getTreeHeight() - height- thePanel.getPanelSizeProperties().fontHeight();
		}
		return height;
	}

	private int leafDescriptionHeight() {
		return  thePanel.getPanelSizeProperties().fontHeight();
	}

	private void updateValues(TSNodeLabel n, int index) {
		NodeRenderPosition node = nodeRenderPositions.get(index);

		if (node.getXMiddle() != -1) {
			return;
		}
		getTheTreeStruct().setLabel(index, n.label());
		node.setY(this.computeRenderHeight(n));
		int wordLength = computeWordLength(getTheTreeStruct().getNode(index));
		node.setLabelLength(wordLength);
		node.setNumLabels(numLabels(n));
		TSNodeLabel[] daughters = n.daughters;

		updateValuesIfNotLexical(n.firstDaughter());

		int firstDaughterIndex = getTheTreeStruct().getIndexTable().get(n.firstDaughter());
		int lastDaughterIndex = getTheTreeStruct().getIndexTable().get(n.lastDaughter());
		NodeRenderPosition firstDaughterPosition = nodeRenderPositions.get(firstDaughterIndex);
		NodeRenderPosition lastDaughterPosition = nodeRenderPositions.get(lastDaughterIndex);

		if (daughters.length == 1) {
			node.setXMiddle(firstDaughterPosition.getXMiddle());
		} else {
			updateValuesIfNotLexical(n.lastDaughter());
			node.setXMiddle(NodeRenderPosition.computeXMiddlePosition(firstDaughterPosition, lastDaughterPosition));
		}
	}

	public void updateValuesIfNotLexical(TSNodeLabel node) {
		if (!node.isLexical) {
			updateValues(node, getNodeIndex(node));
		}
	}

	public int getNodeIndex(TSNodeLabel node) {
		return getTheTreeStruct().getIndexTable().get(node);
	}

	public void initializeRenderingPositionArrays() {
		this.nodeRenderPositions = NodeRenderPosition.createNodeRenderPositionList(this.theTreeStruct.getNodesCount(), this.thePanel.getPanelSizeProperties(), this.drawTreeUpsideDown);
	}

	public int getNoSourceWords() {
		return getTheTreeStruct().getSentenceLength();
	}

	public void generateNodeLocationsTable() {
		nodeLocations = new ArrayList<NodeLocation>();

		// Generate NodeLocation table
		for (TSNodeLabel n : getTheTreeStruct().getNodesArray()) {
			int index = getTheTreeStruct().getIndexTable().get(n);
			NodeRenderPosition position = nodeRenderPositions.get(index);
			nodeLocations.add(new NodeLocation(n, new Point(position.getXMiddle(), position.getY())));
		}
	}

	public void drawNodeLinks(Graphics2D g2, TSNodeLabel n, int i, int yOffset) {
		TSNodeLabel p = n.parent;
		if (!(p == null)) {

			int pIndex = getTheTreeStruct().getIndexTable().get(p);
			NodeRenderPosition childPosition = this.nodeRenderPositions.get(i);
			NodeRenderPosition parentPosition = this.nodeRenderPositions.get(pIndex);

			// draw the trees with straight lines
			boolean skewed = false;

			int childY = childPosition.getNodeLinkTopBoundPosition() + yOffset;
			int parentY = parentPosition.getBulletDownLineBoundPositionY() + yOffset;

			BasicDrawing.drawLine(childPosition.getXMiddle(), childY, parentPosition.getXMiddle(), parentY, g2, skewed);

			BasicDrawing.drawLine(childPosition.getXMiddle(), childPosition.getNodeLinkTopBoundPosition() + yOffset, childPosition.getXMiddle(), childPosition.getLabelUpLineBoundPositionY(0)
					+ yOffset, g2, skewed);

		}
	}

	public static int getVarNum(String varName, HashMap<String, Integer> varNumTable) {
		int varNum = -1;
		if ((varNumTable != null) && (varName != null) && varNumTable.containsKey(varName)) {

			varNum = varNumTable.get(varName);
		}

		return varNum;
	}

	public static Color getColorForVarNum(String varName, HashMap<String, Color> colorTable) {
		if (colorTable != null) {
			// We have to check that the key is present, since for unlabeled
			// node the field
			// n.varname is the empty string, and no associated color and number
			// will be present in that case
			if (colorTable.containsKey(varName)) {
				return colorTable.get(varName);
			} else {
				return Color.black;
			}
		} else {
			return Color.black;
		}
	}

	private List<String> getLabelStringListToRender(TSNodeLabel n) {
		return this.nodeLabelExtracter.getLabelStringListToRender(n);
	}

	private boolean showLabel(TSNodeLabel n) {
		return (getLabelStringListToRender(n).size() > 0);
	}

	/**
	 * Method that renders the tree using the graphics object g2 and the locations of nodes, edges and labels, which are assumed to have been computed by the
	 * constructor or the method recomputeTreeVisualization before this method is called.
	 * 
	 * Rendering is performed by looping over the array with nodes of the tree, and for every node rendering the node (as a labeled circle) as well as a line
	 * connecting it to its parent (if any) and any labels associated with the node.
	 * 
	 * The tree is drawn starting from the yPosition yOffSet and going up from there in case of normal drawing, or down from there in case of upside down
	 * drawing
	 * 
	 * @param g2
	 *            : the graphics object
	 * @param yOffset
	 *            : yOffSet - Rendering offset for tree in y direction from the perspective of the trees root node.
	 * @param drawUpsideDown
	 *            : Boolean indicating whether tree should be drawn upside down
	 */
	public void drawTree(Graphics2D g2, int yOffset, boolean drawUpsideDown) {

		for (TSNodeLabel n : getTheTreeStruct().getNodesArray()) {

			// don't display the label if it is empty

			// First draw the node links (lines)
			int i = getTheTreeStruct().getIndexTable().get(n);

			drawNodeLinks(g2, n, i, yOffset);

			// Then draw the node label
			drawNodeLabels(g2, n, i, yOffset, showNamePlates(), showLabel(n), drawUpsideDown);

		}
		if (wrongNodes != null) {
			markWrongNodes(g2);
		}

		if (this.leafDescriptionData != null) {
			BasicDrawing.drawWords(this.leafDescriptionData, this.getWordsYPos(drawUpsideDown, yOffset), g2);
		}

	}

	public int getMaxVarNum() {
		int result = 0;
		if (thePanel instanceof TreePairPanel) {
			TreePairPanel p = (TreePairPanel) thePanel;
			result = p.getMaxVarNum();
		}
		return result;
	}

	private boolean showNamePlates() {
		if ((thePanel instanceof HATPairPanel)) {
			return ((EbitgCheckBoxTable) ((HATPairPanel) thePanel).checkBoxTable).showNamePlates();
		}
		return false;
	}

	private int getWordsYPos(boolean drawUpsideDown, int yOffset) {
		if (drawUpsideDown) {
			return getTreeHeight() - sourceLanguageWordsYPos + yOffset;
		} else {
			return sourceLanguageWordsYPos;
		}

	}

	private HashMap<String, Color> getColorTable() {
		HashMap<String, Color> result = null;
		if (thePanel instanceof TreePairPanel) {
			TreePairPanel p = (TreePairPanel) thePanel;
			return p.getColorTable();
		}
		return result;
	}

	private HashMap<String, Integer> getVarNumTable() {
		if (thePanel instanceof TreePairPanel) {
			return ((TreePairPanel) thePanel).variableVarNumLookupTable;
		}

		return null;
	}

	private void drawStackedLabels(Graphics2D g2, TSNodeLabel n, NodeRenderPosition nodePosition, int yOffset) {
		int i = 0;
		for (String label : getLabelStringListToRender(n)) {
			BasicDrawing.setAntiAliasingOn(g2);
			g2.drawString(label, nodePosition.getXLeft(label), nodePosition.getLabelY(i) + yOffset);
			i++;
		}
	}

	public void drawNodeLabels(Graphics2D g2, TSNodeLabel node, int i, int yOffset, boolean showNamePlate, boolean showLabel, boolean drawUpsideDown) {
		NodeRenderPosition nodePosition = this.nodeRenderPositions.get(i);

		if (showLabel) {
			drawStackedLabels(g2, node, nodePosition, yOffset);
		}

		g2.setColor(getColorForVarNum(getVarName(node), getColorTable()));

		if (getVarName(node) != "") {
			int varNum = getVarNum(getVarName(node), getVarNumTable());
			String varString = "@" + varNum + "@";

			if (showNamePlate) {
				BasicDrawing.drawNamePlateX(varString, thePanel, nodePosition.getNamePlateX(), nodePosition.getBulletYMiddle() + yOffset, g2);
			}
			BasicDrawing.drawNodeSymbolBullet(((int) nodePosition.getSymbolBulletDiameter()), nodePosition.getXMiddle(), nodePosition.getBulletYMiddle() + yOffset, varNum, getMaxVarNum(), g2);
			// drawCoreContextLabeIfPresent(g2, node, nodePosition, yOffset);
			extraTreeNodePropertiesVisualizer.drawExtraTreeNodeProperties(g2, node, nodePosition, yOffset);
		} else if (node.label().contains("NULLBINDING")) {
			BasicDrawing.drawNullBindingSymbol(nodePosition.getXMiddle(), nodePosition.getBulletYMiddle() + yOffset, ((int) nodePosition.getSymbolBulletDiameter()), g2);
		}
		g2.setColor(Color.BLACK);
	}

	private int computeWordLength(TSNodeLabel n) {
		List<String> words = getLabelStringListToRender(n);

		int result = 0;

		for (String word : words) {
			result = Math.max(thePanel.getPanelSizeProperties().stringWidth(word), result);
		}

		return result;
	}

	private int numLabels(TSNodeLabel n) {
		return getLabelStringListToRender(n).size();
	}

	private int getTreeLeafColumnLabelWidth(TSNodeLabel n) {

		int result = computeWordLength(n);

		while (n.parent != null && n.isUniqueDaughter()) {
			n = n.parent;
			int length = computeWordLength(n);
			if (length > result) {
				result = length;
			}
		}
		return result;
	}

	void markWrongNodes(Graphics2D g2) {
		for (TSNodeLabel n : getTheTreeStruct().getNodesArray()) {

			if (wrongNodes.contains(n)) {
				NodeRenderPosition nodePosition = getNodePosition(n);
				g2.setColor(n.isPreLexical() ? Color.green : Color.red);
				g2.drawRect(nodePosition.getXLeft(n.label()) - wrongNodesBorder, nodePosition.getY() - thePanel.getPanelSizeProperties().fontHeight() + wrongNodesBorder, nodePosition.getLabelLength()
						+ 2 * wrongNodesBorder, thePanel.getPanelSizeProperties().fontHeight() + wrongNodesBorder);
				g2.setColor(Color.black);
			}
		}
	}

	public void setWrongNodes(ArrayList<TSNodeLabel> wrongNodes) {
		this.wrongNodes = wrongNodes;
	}

	public NodeRenderPosition getNodePosition(TSNodeLabel n) {
		return nodeRenderPositions.get(getTheTreeStruct().getIndexTable().get(n));
	}

	// Method finds and returns the draw position of the node at the (string)
	// position sourceIndex
	public TSNodeLabel getSourceNodeAtIndex(int sourceIndex) {
		return getTheTreeStruct().getLexicalItem(sourceIndex); // get the node
																// at the
																// position of
																// the alignment
	}

	public int[] getNodeBottomPosition(TSNodeLabel node, boolean drawTreeUpsideDown) {
		if (getTheTreeStruct().getIndexTable().containsKey(node)) {
			NodeRenderPosition nodePosition = getNodePosition(node);

			int[] result = new int[2];
			result[0] = nodePosition.getXMiddle();

			result[1] = nodePosition.getLeafNodeBottomY();

			return result;
		} else {
			System.out.println("node unknown");
			return null;
		}
	}

	public void printNodesArray() {
		for (int i = 0; i < getTheTreeStruct().getNodesArray().length; i++) {
			TSNodeLabel node = getTheTreeStruct().getNodesArray()[i];
			System.out.println("node[" + i + "] " + node + "isLexical:" + node.isLexical + " isTerminal: " + node.isTerminal());
		}
	}

	public void setTheTreeStruct(TreeStruct theTreeStruct) {
		this.theTreeStruct = theTreeStruct;
	}

	public TreeStruct getTheTreeStruct() {
		return theTreeStruct;
	}

	public int getTreeHeight() {
		int maxDepth = getTheTreeStruct().getTree().maxDepth();
		return ((int) thePanel.getPanelSizeProperties().topMargin() + 1* this.leafDescriptionHeight() + (maxDepth) * thePanel.getPanelSizeProperties().levelSize()) + 2*thePanel.getPanelSizeProperties().fontHeight();
	}

	public int getMaxLeafYPosition(boolean drawTreeUpsideDown) {

		if (drawTreeUpsideDown) {
			int minY = Integer.MAX_VALUE;
			for (NodeRenderPosition position : this.nodeRenderPositions) {
				minY = Math.min(position.getLabelYTop(0), minY);
			}

			return minY;

		}

		else {

			int maxY = Integer.MIN_VALUE;
			for (NodeRenderPosition position : this.nodeRenderPositions) {
				maxY = Math.max(position.getLabelYBottom(0), maxY);
			}

			return maxY;
		}
	}

	public static String getVarName(TSNodeLabel node) {

		String result = node.getExtraLabel(TreeRepresentation.VarNameProperty);

		if (result == null) {
			result = "";
		}
		return result;
	}

	/**
	 * Method that checks whether the tree and the string are consistent, that is the tree has the same number of terminal nodes as the String has words
	 * 
	 * @param s
	 * @return Whether the tree and the String are consistent
	 */
	public boolean treeIsConsistenWithString(String s) {
		return TreeStringConsistencyTester.treeIsConsistenWithString(s, this.theTreeStruct.getTree());
	}

}
