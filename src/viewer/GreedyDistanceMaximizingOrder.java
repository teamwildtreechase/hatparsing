/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.util.LinkedList;

import util.Pair;

/**
 * This class produces an order of integers, such that starting from the left each time the next element n is added 
 * is added such that the maximal between element distance over the first n elements remains minimum.
 * The approach is greedy in the sense, that for a pre-specified number of elements a better devision 
 * of the space is possible. However, if the devision is incrementally and greedily made with no possibility
 * of coming back on previous choices, this way is optimal.
 * For the one-dimensional case this Class implements, the optimal order boils down to an incremental binary
 * partition int ranges and sub-ranges.
 * One could image extensions to the general multi-dimensional case, however this more complex.
 * 
 *  The class has several applications. The motivating one, is to find an set of x colors where x is not 
 *  known beforehand and may grow online, such that these colors are maximally distinguishable. This
 *  can be cast into the problem of incrementally optimally choosing number from a range, so that at 
 *  each time for the set of first n numbers the minimal distance between any pair of numbers in that 
 *  set is kept maximal.	
 * @author Gideon Maillette de Buy Wenniger
 *
 */

public class GreedyDistanceMaximizingOrder 
{
	private int minNumber;
	private int maxNumber;
	private int[] order;


	public GreedyDistanceMaximizingOrder(int minNumber, int maxNumber)
	{
		System.out.println("GreedyDistanceMaximizingOrder( " + minNumber + "," + maxNumber + ") called...");
		this.minNumber = minNumber;
		this.maxNumber = maxNumber;
		order = new int[(maxNumber - minNumber) + 1];
		
		this.generateOrder();
	}
	
	public int get(int index)
	{
		return order[index];
	}
	
	
	public void generateOrder()
	{
		// If the length is just 1, there is not really an order to compute
		if(order.length == 1)
		{
			order[0] = 0;
		}
		// if the length is bigger than 1 we have something to do
		else if(order.length > 1)
		{	
			order[0] = minNumber;
			order[1] = maxNumber;
			
			LinkedList<Pair<Integer>> queue = new LinkedList<Pair<Integer>>();
			
			if((maxNumber - minNumber) > 1)
			{				
				queue.add(new Pair<Integer>(minNumber,maxNumber));
			}	
			
			//int curMinDistance = maxNumber - minNumber;
			
			int i = 2;
			while(! queue.isEmpty())
			{
				//System.out.println(this);
				// Retrieve the fist element in the queue
				Pair<Integer> pair = queue.pollFirst();
				//System.out.println("Pair: " + pair);
				
				int min = pair.getFirst();
				int max = pair.getSecond();
				int middle = (min + max) / 2;
				order[i] = middle;
				
				
				//int minDistance = Math.min((max - middle), (middle - min));
				// check that the order indeed is consistent
				//assert(minDistance <= curMinDistance);
				//curMinDistance = minDistance;
				
				//System.out.println("Added " + middle + " minDistance is : " +  minDistance);
				 
				// Add pair for lower range if this can be further split
				if((middle - min) > 1)
				{				
					Pair<Integer> p = new Pair<Integer>(min,middle);
					//System.out.println("Add Pair: " + p);
					queue.add(p);
				}
				
				// Add pair for upper range if this can be further split
				if((max - middle) > 1)
				{	
					Pair<Integer> p = new Pair<Integer>(middle,max);
					//System.out.println("Add Pair: " + p);
					queue.add(p);
				}
				i++;
			}
		}	
	}
		
	public String toString()
	{
		String result = "\nGreedyDistanceMaximizingOrder: \n[";
		
		for(int i = 0; i < order.length - 1; i++)
		{
			result += order[i] + ",";
		}
		result += order[order.length - 1] + "]";
		
		return result;
	}
	
	public static void main(String [] args)
	{
		GreedyDistanceMaximizingOrder gdmo = new GreedyDistanceMaximizingOrder(1,50);
		System.out.println(gdmo);
	}
	

}
