/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import viewer_io.CheckBoxTable;

import alignment.Alignment;
import alignment.AlignmentPair;

public class AlignmentTripleRenderer 
{
	private PanelSizeProperties panelSizeProperties;
	private SentenceRenderData sourceRenderData;
	private SentenceRenderData targetRenderData;
	private List<String> sourceAlignmentSetStrings;
	private List<String> targetPosNumberStrings;
	// the alignments stored as pairs extracted from the input
	Alignment sentenceAlignments;
	
	int wordsYPosition;
	int sourceWordsYPosition;
	int targetWordsYPos;
	public final AlignmentColorKeeper alignmentColorKeeper;
	
	
	private AlignmentTripleRenderer(AlignmentColorKeeper alignmentColorKeeper, PanelSizeProperties panelSizeProperties)
	{
		this.alignmentColorKeeper = alignmentColorKeeper;
		this.panelSizeProperties = panelSizeProperties;
	}
	
	public static AlignmentTripleRenderer createAlignmentTripleRenderer(CheckBoxTable checkBoxTable, PanelSizeProperties panelSizeProperties)
	{
		return new AlignmentTripleRenderer(new AlignmentColorKeeper(checkBoxTable), panelSizeProperties);
	}
	

	public int getSentenceHorizontalLength()
	{
		int sourceLength, targetLength;
		sourceLength = targetLength = 0;
		
		if(this.sourceRenderData != null)
		{
			sourceLength = this.sourceRenderData.getSentenceRenderLength();
		}
		if(this.getTargetRenderData() != null)
		{
			targetLength = this.getTargetRenderData().getSentenceRenderLength();
		}
		return Math.max(sourceLength, targetLength);
	}
	
	
	
	private boolean sourceWordsLoaded()
	{
		return (this.sourceRenderData != null);
	}
	
	
	private boolean targetWordsLoaded()
	{
		return (this.targetRenderData != null);
	}
	
	private boolean alignmentsLoaded()
	{
		return (this.sentenceAlignments != null);
	}
	
	
	private void renderWords(Graphics2D g2)
	{
		System.out.println("AlignmentTripleRenderer.renderWords callled");
		
		// draw the source words if available
		if(sourceWordsLoaded())
		{	
			 BasicDrawing.drawWords(this.sourceRenderData, sourceWordsYPosition, g2);			
		}
		
		targetWordsYPos = (int)(sourceWordsYPosition + 2 * panelSizeProperties.levelSize());
		
		// draw the second language words if available
		if (targetWordsLoaded()) 
		{			
			BasicDrawing.drawWords(getTargetRenderData(), targetWordsYPos, g2);			
		}		
	}
	
	
	
	private<T> String getSetString(Set<T> set)
	{
		String result = "{";
		
		Iterator<T> setIterator = set.iterator();
		while(setIterator.hasNext())
		{
			result += setIterator.next();
			
			if(setIterator.hasNext())
			{
				result += ",";
			}
		}
		
		result += "}";
		return result;
	}
	
	
	private void createSourceAlignmentsSetsStringList(List<String> sourceWords)
	{
		List<Set<Integer>> sourceAlignmentSets = getSourceAlignmentSets(sourceWords.size());
		this.sourceAlignmentSetStrings = createSetStringList(sourceAlignmentSets);
	}
	
	private void createTargetPosNumberStrings(List<String> targetWords)
	{
		this.targetPosNumberStrings = createNaturalNumberStringList(targetWords.size());
	}

	private static List<String> createNaturalNumberStringList(int length)
	{
		List<String> result = new ArrayList<String>();
		
		for(int i = 1; i <= length; i++)
		{
			String numberString = "" + i; 
			result.add(numberString);
		}
		return result;
	}
	
	private<T> List<String> createSetStringList(List<Set<T>> setList)
	{
		List<String> result = new ArrayList<String>();
		
		for(Set<T> set : setList)
		{
			result.add(getSetString(set));
		}
		return result;
	}
	
	private void renderLabels(Graphics2D g2, List<String> labelStrings, SentenceRenderData sentenceRenderData, int yRenderPos)
	{
		for(int i = 0; i < labelStrings.size(); i++)
		{
			String labelString = labelStrings.get(i);
			int labelRenderLengt = panelSizeProperties.stringWidth(labelString);
			int xRenderPos = sentenceRenderData.getXMiddlePos(i) - ((int)(0.5 * labelRenderLengt));
			g2.drawString(labelString, xRenderPos, yRenderPos);
		}
	}

	private void renderSourceAlignmentSets(Graphics2D g2)
	{
		
		
		int yRenderPos = this.sourceWordsYPosition - panelSizeProperties.fontHeight();
		renderLabels(g2, this.sourceAlignmentSetStrings, this.sourceRenderData, yRenderPos);
			
	}
	
	@SuppressWarnings("unused")
	private void renderTargetPosNums(Graphics2D g2)
	{
	
		int yRenderPos = this.targetWordsYPos + panelSizeProperties.fontHeight();
		renderLabels(g2, this.targetPosNumberStrings, this.targetRenderData,yRenderPos);
			
	}
	
	private void renderAlignments(Graphics2D g2)
	{
		// draw the alignments if available
		if(this.getTargetRenderData() != null  && this.sourceRenderData != null && sentenceAlignments != null)
		{
			int i = 0;
			
			for(AlignmentPair a : sentenceAlignments.getAlignmentPairs())
			{
				System.out.println("alignment pair " + i + a.getSourcePos() + ", " + a.getTargetPos());
				
				try
				{
					int sourceXPos = this.sourceRenderData.getXMiddlePos(a.getSourcePos());
					int sourceYPos = sourceWordsYPosition + panelSizeProperties.fontDescendent();
					int targetXPos = this.targetRenderData.getXMiddlePos(a.getTargetPos());
					int targetYPos = targetWordsYPos - panelSizeProperties.fontAscent();
					
					// Draw the line of the alignment between the position of the source leaf word 
					// and the corresponding target word
					
					System.out.println("Draw alignment line");
					this.drawAlignmentLine(sourceXPos, sourceYPos, targetXPos, targetYPos, g2, false);
				}		
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Error finding the alignment positions, possibly mismatch between loaded sentences and loaded alignments" + e);
				}	
				
				i++;
			}
			
		}
	}
	
	
	public void render(Graphics2D g2, boolean showSourceAlignmentSets)
	{
		
		renderWords(g2);
		renderAlignments(g2);
		
		if(showSourceAlignmentSets &&  sourceWordsLoaded() && targetWordsLoaded()  && alignmentsLoaded())
		{	
			renderSourceAlignmentSets(g2);
			//renderTargetPosNums(g2);
		}	
	}
	
	

	
	public List<Set<Integer>> getSourceAlignmentSets(int length)
	{
		List<Set<Integer>> result = new ArrayList<Set<Integer>>();
	
		// Create a list of sets, one for every source position
		for(int i = 0; i < length; i++)
		{
			result.add(new TreeSet<Integer>());
		}
				
		// Add every bound target position to the source positions it is bound from
		for(AlignmentPair alignmentPair : this.sentenceAlignments.getAlignmentPairs())
		{
			result.get(alignmentPair.getSourcePos()).add(alignmentPair.getTargetPos() + 1);
		}
		return result;
	}

    public void loadSentence(List<String> sourceWords, List<String> targetWords, Alignment alignment, boolean showSourceAlignmentSets) 
    {
		if(alignment != null)
		{
			sentenceAlignments = alignment;
			createSourceAlignmentsSetsStringList(sourceWords);
			createTargetPosNumberStrings(targetWords);
			this.sourceRenderData = SentenceRenderData.createSentenceRenderData(sourceWords,this.sourceAlignmentSetStrings, panelSizeProperties,showSourceAlignmentSets);
			this.setTargetRenderData(SentenceRenderData.createSentenceRenderData(targetWords,targetPosNumberStrings, panelSizeProperties,showSourceAlignmentSets));
		}
		else
		{
			this.sourceRenderData = SentenceRenderData.createSentenceRenderData(sourceWords, panelSizeProperties);
			this.setTargetRenderData(SentenceRenderData.createSentenceRenderData(targetWords, panelSizeProperties));
			
		}
		
		if(showSourceAlignmentSets)
		{	
			sourceWordsYPosition = panelSizeProperties.topMargin() + 2 * panelSizeProperties.fontHeight();
		}
		else
		{
			sourceWordsYPosition = panelSizeProperties.topMargin() +  panelSizeProperties.fontHeight();
		}
    }
       
	
	/** Wrapper method that draws an alignment line using the BasicDrawing class
	 *  
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param g2
	 * @param striped
	 */
	public void drawAlignmentLine(int x1, int y1, int x2, int y2, Graphics2D g2, boolean striped) 
	{
			// extract the color for this alignment
			g2.setColor(this.alignmentColorKeeper.getCurrentAlignmentColor());
			// increment the currentAlignmentColorIndex. Once the maximum index is reached, start over from 0
			this.alignmentColorKeeper.incrementAlignmentColorIndex();
				
			BasicDrawing.drawAlignmentLine(x1, y1, x2, y2, g2, striped);
			
			g2.setColor(Color.black);
	}
	
	public int getSourceLength()
	{
		if(this.sourceRenderData != null)
		{	
			return this.sourceRenderData.getSentenceLength();
		}
		else
		{
			return -1;
		}
	}
	
	public int getTargetLength()
	{
		if(this.getTargetRenderData() != null)
		{
			return this.getTargetRenderData().getSentenceLength();	
		}
		else
		{
			return -1;
		}
	}

	public void setTargetRenderData(SentenceRenderData targetRenderData) {
		this.targetRenderData = targetRenderData;
	}

	public SentenceRenderData getTargetRenderData() {
		return targetRenderData;
	}
	
}
