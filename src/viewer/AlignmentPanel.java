/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.junit.Assert;

import util.InputReading;
import viewer_io.CheckBoxTable;
import viewer_io.EbitgCheckBoxTable;
import viewer_io.ExportableCorpusViewPanel;
import viewer_io.SentencesAndAlignmentsLoader;
import alignment.Alignment;
import alignment.AlignmentPair;
import alignment.AlignmentTripleData;

public class AlignmentPanel extends BasicPanel implements SentencesAndAlignmentsLoader,  ChangeListener, ActionListener, ExportableCorpusViewPanel 
{	
	private static final long serialVersionUID = 1L;

	AlignmentTripleData alignmentTripleData;
	AlignmentTripleRenderer alignmentTripleRenderer;
		
	public AlignmentPanel(CheckBoxTable checkBoxTable)
	{
		super(checkBoxTable);
		
		// Set the topMargin to a lower value
		panelSizeProperties.setLevelSize(2);
		panelSizeProperties.setLevelSizeFactor(2);
		panelSizeProperties.setTopMargin(5);
		panelSizeProperties.loadFont(this);
		
		this.alignmentTripleData = new AlignmentTripleData();
		this.alignmentTripleRenderer = AlignmentTripleRenderer.createAlignmentTripleRenderer(checkBoxTable, panelSizeProperties);
		this.setBackground(Color.white);
	}
	

	
	public void loadFromStrings(String sourceSentence, String targetSentence, String alignmentString)
	{
		Alignment alignment = Alignment.createAlignment(alignmentString);
			
		List<String> sourceWords = InputReading.getWordsFromString(sourceSentence);	
		List<String> targetWords = InputReading.getWordsFromString(targetSentence);
		this.alignmentTripleRenderer.loadSentence(sourceWords, targetWords, alignment, showAlignmentSets());
	}

	public void initializePanel()
	{	
		//System.out.println("alignmentPanel.noColorCheckBox " + alignmentPanel.noColorCheckBox );
		this.setBackground(Color.white);
		this.setFocusable(false);
		this.setVisible(true);
		
		init();
	}
	
	
	
	/** 
	 * Method that sets the target sentences for the panel
	 * @param targetSentences
	 */
	public void loadTargetSentences(List<List<String>>  targetSentences) 
	{
		this.alignmentTripleData.setTargetSentences(targetSentences);
		lastIndex = Math.max(lastIndex ,targetSentences.size() - 1);
		System.out.println(">>TargetSentences loaded");
		init(); //repaint everything
	}
	
	/** 
	 * Method that sets the source sentences for the panel
	 * @param sourceSentences
	 */
	public void loadSourceSentences(List<List<String>>  sourceSentences) 
	{
		this.alignmentTripleData.setSourceSentences(sourceSentences);
		System.out.println(">>SourceSentences loaded");
		init(); //repaint everything
	}
	
	
	/** 
	 * Method that sets the alignments for the panel
	 * @param alignments An Arraylist of Arraylists of Alignment pairs, one ArrayList
	 * with alignment pairs for every sentence
	 */
	public void loadAlignments(List<Alignment>  alignments) 
	{
		this.alignmentTripleData.setAlignment(alignments);
		System.out.println(">>Alignments loaded");
		init(); //repaint everything
	}
	
	
	public void init() {
		System.out.println("AlignmentPanel.init called");
		panelSizeProperties.loadFont(this);
        setPreferredSize(getArea());
        revalidate();
        repaint();
	}
	
	public void initWithoutSentenceLouding()
	{
		System.out.println("AlignmentPanel.init called");
		//loadSentence();
		setPreferredSize(getArea());
        revalidate();
        repaint();
	}
	
	
	public void setPanelSize(boolean showAlignmentSets)
	{
		// Reset the width of the panel to match the  max length of the sentences
		this.getArea().width = this.alignmentTripleRenderer.getSentenceHorizontalLength() + 20;
		
		if(showAlignmentSets)
		{	
			this.getArea().height = this.alignmentTripleRenderer.targetWordsYPos + panelSizeProperties.topMargin() + panelSizeProperties.fontHeight();
		}
		else
		{	
			this.getArea().height = this.alignmentTripleRenderer.targetWordsYPos + panelSizeProperties.topMargin();
		}	
	}
	
	
	private boolean showAlignmentSets()
	{
		boolean showAlignmentSets = true;
		if(checkBoxTable instanceof EbitgCheckBoxTable)
		{
			showAlignmentSets = ((EbitgCheckBoxTable) checkBoxTable).showAlignmentSets();
		}
		return showAlignmentSets;
	}	
	
	
	public void render(Graphics2D g2) {
		
		System.out.println("AlignmentPanel.render called");
		
		
		g2.setFont(panelSizeProperties.font()); 
        g2.setColor(Color.black);      
        g2.setStroke(new BasicStroke());
        
    	// reset index of the current alignment color, so that the first word will 
		// be painted the same color every time, and no funny repainting bugs can occur
		this.alignmentTripleRenderer.alignmentColorKeeper.setAlignmentColorIndex(0);
		setPanelSize(showAlignmentSets());
		this.alignmentTripleRenderer.render(g2, showAlignmentSets());
	}
	
	protected void paintComponent(Graphics g) 
	{     
		super.paintComponent(g);  // Without this the background will not be updated and things will be drawn over each other
		System.out.println("AlignmentPanel.paintComponent called");
        Graphics2D g2 = (Graphics2D) g;
        render(g2);
    }
	  
	
	public void loadSentence()
	{
		System.out.println("AlignmentPanel.loadSentence called");
		
		Assert.assertTrue(sentenceNumber <= this.alignmentTripleData.getNumAlignments());
		// Sanity check: if the sentenceNumber is bigger then the maximum over sourcesentences length, target
		// sentences length and alignments length, this is certainly wrong
		Assert.assertTrue(sentenceNumber <= this.alignmentTripleData.getMaxOfSizes());
		
		Alignment alignment = null;
		alignment = this.alignmentTripleData.getAlignment(sentenceNumber);
		List<String>  sourceWords = this.alignmentTripleData.getSourceSentence(sentenceNumber);
		List<String>  targetWords =this.alignmentTripleData.getTargetSentence(sentenceNumber);
		
		//assert(sourceWords != null);
		
    	this.alignmentTripleRenderer.loadSentence(sourceWords, targetWords, alignment, showAlignmentSets());
	}
	
	public int goToSentence(int n) 
	{
		if (n<0)
		{
			sentenceNumber=0;
		}	
		else if (n>lastIndex) 
		{	
			sentenceNumber=lastIndex;	
		}
		else
		{
			sentenceNumber = n;
		}
		
		loadSentence();
		return sentenceNumber;
		
	}
    
	
	public String getSourceSentence()
	{
		List<String> sourceSentence = this.alignmentTripleData.getSourceSentence(sentenceNumber); 
		return wordListAsString(sourceSentence);
	}
	
	public String getTargetSentence()
	{
		List<String> targetSentence = this.alignmentTripleData.getTargetSentence(sentenceNumber); 
		return wordListAsString(targetSentence);
	}
	
	public String getAlignment()
	{
		List<AlignmentPair> alignments = (this.alignmentTripleData.getAlignment(sentenceNumber)).getAlignmentPairs();
		
		String result = "";
		
		for(AlignmentPair pair : alignments)
		{
			result += " " + pair.toString(); 
		}
		
		result = result.trim();
		
		return result;
	}
	
	public static String wordListAsString(List<String> sourceSentence)
	{
		String result = "";
		
		for(String word : sourceSentence)
		{
			result += " " + word;
		}
		
		result = result.trim();
		
		return result;
	}
	
	
    
	
	public int determineWordLenght(String[] words )
	{
		int length = 0; 
	
		for(int i = 0; i < words.length; i++) 
		{    			
			String w = words[i];
			int wordLength = panelSizeProperties.stringWidth(w);
			length += wordLength + panelSizeProperties.wordSpace();
		} 	
		return length;
	}
	    
	

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		init();
	}

	@Override
	public void stateChanged(ChangeEvent e) 
	{
		init();
	}

	@Override
	public int getCorpusSize() 
	{
		return this.alignmentTripleData.getCorpusSize();
	}
	
	
}



