/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import static util.ConfigCacher.createConfigCacher;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import extended_bitg.SafeGUIAffectingTaskRunner;
import alignment.Alignment;
import tsg.TSNodeLabel;
import util.ConfigCacher;
import util.ConfigFile;
import viewer_io.ATPCheckBoxTable;
import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.MenuInteraction;
import viewer_io.SentencesAndAlignmentsLoader;
import viewer_io.ViewerInputLoading;


@SuppressWarnings("serial")
/**
 * This is the main class that contains the main frame of the AlignmentVisualizer and 
 * that sets up everything for the application.
 */
public class AlignmentVisualizer extends SingleTreeViewer implements MouseListener, SentencesAndAlignmentsLoader {
	
	public static final String ConfigFileLocation = "configFileLocation";
	public static final String ReorderedOutputFileName = "reorderedOutputFile";
	
	private static TSNodeLabel initialSentence = initialSentence();    
	

	public static TSNodeLabel initialSentence() {
		try {
			return new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static AlignmentVisualizer createAlignmentVisualizer()
	{
		ATPCheckBoxTable checkBoxTable = ATPCheckBoxTable.createATPCheckBoxTable();
		AlignmentTreePanel treePanel = AlignmentTreePanel.createAlignmentTreePanel(checkBoxTable);
		ConfigCacher configCacher = createConfigCacher("ebitgViewerCachedConfig");
		AlignmentVisualizer alignmentVisualizer = new AlignmentVisualizer(configCacher, ButtonIOComponentTable.createButtonIOComponentTable(), treePanel, checkBoxTable);
		 // Add a mouse listener to the panel
		alignmentVisualizer.treePanel.addMouseListener(alignmentVisualizer);
		// Set preferred size of treeScroller to a different value
		// Make sure the window is still smaller than the bottom of the total frame
		// to assure a scroller can be properly generated below it if nescessary
		alignmentVisualizer.treeScroller.setPreferredSize(new Dimension(800,730));
        
		configCacher.registerEntry(AlignmentVisualizer.ConfigFileLocation,  "!!!./configFile.txt",alignmentVisualizer.getIoComponentsTable().getComponentByName(AlignmentVisualizer.ConfigFileLocation).getTextField());
		alignmentVisualizer.treePanel.init();
		alignmentVisualizer.treePanel.setVisible(true);
			
		return alignmentVisualizer;
	}
	
	
    private AlignmentVisualizer(ConfigCacher theConfigCacher, ButtonIOComponentTable buttonIOComponentTable, AlignmentTreePanel alignmentTreePanel, ATPCheckBoxTable checkBoxTable)
    {
    	super(buttonIOComponentTable, alignmentTreePanel, checkBoxTable);
    	this.checkBoxTable = checkBoxTable;
        this.setupTreeViewer();
      
    }
    
    public void setInitialSentence() {
    	ArrayList<TSNodeLabel> treebank = new ArrayList<TSNodeLabel>();
        treebank.add(initialSentence);
        loadTreebank(treebank, true);
    }
    
    

	@Override
	protected void createInputFields() 
	{		
		ConfigFile guiCachedConfig = null;
		try 
		{
			guiCachedConfig = new ConfigFile("guiCachedConfig");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("No cached config values found for gui, use defaults");
		}
		
		String reorderOutputFileName = null;
		String configFileLocation = null;
		
		// Load cached values if available
		if(guiCachedConfig != null && guiCachedConfig.hasValue("reorderOutputFileName") && guiCachedConfig.hasValue(AlignmentVisualizer.ConfigFileLocation) )
		{
			reorderOutputFileName = guiCachedConfig.getValue("reorderOutputFileName");
			configFileLocation =  guiCachedConfig.getValue(AlignmentVisualizer.ConfigFileLocation);
		}
		else // no values cached, use defaults
		{
			reorderOutputFileName = "/scratch/wenniger/CorpusData/EnglishReordered.txt";
			String startupLocation = "";
			try 
			{
				//TODO : Remove the jar this generates
				startupLocation = new File(AlignmentVisualizer.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			configFileLocation = startupLocation +"/configfile.txt";
			//configFileLocation = "String /home/gemaille/DataPreprocessing/AlignmentVisualizer/src/viewer/configfile.txt";
		}
		
		
		
		setIoComponentsTable(ButtonIOComponentTable.createButtonIOComponentTable());
		getIoComponentsTable().addComponent("reorderOutputFileName",new ButtonIOComponent(new JButton("Write reorder output"), new JTextField(reorderOutputFileName ),null, new JLabel("Output file:")));
		getIoComponentsTable().addComponent(AlignmentVisualizer.ConfigFileLocation,new ButtonIOComponent(new JButton("Load everything from configile"), new JTextField(configFileLocation ),null, new JLabel("config file:")));
		
		
		                            
		
	}
	
	
	protected static void saveGuiConfig(AlignmentVisualizer treeViewer)
	{
		System.out.println("Saving gui configuration...");
		
		try
		{
			BufferedWriter outWriter = new BufferedWriter(new FileWriter("guiCachedConfig"));
			
			outWriter.write("% This is an automatically generated configuration file \n");
			outWriter.write("% The file contains the values of parameters changed in and automatically stored by the GUI \n");
			String configFileLocationString = "configFileLocation =" + treeViewer.getIoComponentsTable().getComponentTextFieldString(AlignmentVisualizer.ConfigFileLocation);;
			
			outWriter.write(configFileLocationString + "\n");
			
			String reorderOutputFileName = "reorderOutputFileName =" + treeViewer.getIoComponentsTable().getComponentTextFieldString("reorderOutputFileName");
			outWriter.write(reorderOutputFileName+ "\n");
			
			outWriter.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	protected void initializeTreePanel() 
	{
        
     
	}
				
    public void doBeforeClosing() 
    {	    	
    	saveGuiConfig(this);
    }    
	
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static AlignmentVisualizer createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Tree Alignment Visualizer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final AlignmentVisualizer treeViewer = createAlignmentVisualizer();
        Dimension area = treeViewer.treePanel.getArea();
        System.out.println("Area: " + area.width + " " + area.height);
        frame.setMinimumSize(new Dimension(area.width+5, area.height+5));
        treeViewer.setOpaque(true); //content panes must be opaque
        frame.setContentPane(treeViewer);
        
        frame.addWindowListener (new WindowAdapter () {
            public void windowClosing (WindowEvent e) {
            	treeViewer.doBeforeClosing();
            }
        });

        
      //Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        
        
        MenuInteraction menuInteraction = MenuInteraction.createMenuInteraction(treeViewer.treePanel);
        menuInteraction.addAlignmentTripleLoadItems(treeViewer.treePanel, treeViewer.treeSpinnerPanel);
        menuInteraction.addLoadTreeItem(treeViewer,"Load TreeBank");
        menuInteraction.addExportTreeItems(treeViewer.treePanel,"Tree");
        menuInteraction.addQuitter(treeViewer);
        menuInteraction.addItemsToMenu(menu);
        
        
        
    treeViewer.getIoComponentsTable().getButtonByComponentIndex(0).addActionListener(new ActionListener()
    {
    	public void actionPerformed(ActionEvent e)
    	{
    	    			
    		AlignmentVisualizer.produceOracleReorderingOutput(treeViewer);
    		
    		
    	}
    });
    
 
    treeViewer.getIoComponentsTable().getButtonByComponentIndex(1).addActionListener(new ActionListener()
    {
    	public void actionPerformed(ActionEvent e)
    	{
    		treeViewer.loadEverythingFromConfigFile();
    	
    	}
    });
    
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
             
        ((AlignmentTreePanel)treeViewer.treePanel).theTreeVisualizer.printNodesArray();
        
        return treeViewer;
    }
    
    
    public void loadEverythingFromConfigFile()
    {
		String configfFilePath =  this.getIoComponentsTable().getComponentTextFieldString(AlignmentVisualizer.ConfigFileLocation);
		ViewerInputLoading.loadAlignmentTriplesAndTreesFromConfig( configfFilePath, (AlignmentTreePanel)this.treePanel,this, this.treeSpinnerPanel);
    }
	
    public boolean loadEverythingFromConfigFileSafe()
    {
    	SafeGUIAffectingTaskRunner<AlignmentVisualizer,Object, Boolean> taskRunner = SafeGUIAffectingTaskRunner.createAlignmentVisualizerCorpusLoader(this);
    	return SafeGUIAffectingTaskRunner.executeThreadSafe(taskRunner);
    }
    
    
    public static void produceOracleReorderingOutput(AlignmentVisualizer treeViewer)
    {

		System.out.println(">>> Producing reordered source file...");    	
		System.out.println("..");   
		
		String configFilePath =  treeViewer.getIoComponentsTable().get(1).getTextfieldContents();
		System.out.println("Reading configfile from " + configFilePath);
		
		try 
		{
			ConfigFile theConfig = new ConfigFile(configFilePath);
			
			String oracleCorpusPath = theConfig.getValue("oracleCorpusLocation");
			String oracleTreeFileName = oracleCorpusPath + theConfig.getValue("oracleParseFileName");
			String oracleAlignmentsName =  oracleCorpusPath + theConfig.getValue("oracleAlignmentsName");
			String logFileName = oracleCorpusPath + theConfig.getValue("oracleReorderingLogFileName");  
			((AlignmentTreePanel)treeViewer.treePanel).setOracleReorderingFileNames(oracleTreeFileName, oracleAlignmentsName);

			String outputPerSentenceString = theConfig.getStringIfPresentOrReturnDefault("outputPerSentence", "true").toLowerCase();
			boolean outputPerSentence =  outputPerSentenceString.equals("true");
			String writeReorderingLabelsString = theConfig.getStringIfPresentOrReturnDefault("writeReorderingLabels", "true").toLowerCase();
			boolean writeReorderingLabels =  writeReorderingLabelsString.equals("true");
			
			//String outputFilePath =  treeViewer.ioComponents[0].getTheTextField().getText();
			String stringOutputFileName =  oracleCorpusPath + theConfig.getValue("oracleReorderingStringOutputName");
			String treeOutputFileName =  oracleCorpusPath + theConfig.getValue("oracleReorderingTreeOutputName");
			((AlignmentTreePanel)treeViewer.treePanel).produceReorderedSourceFile(stringOutputFileName, treeOutputFileName,logFileName, outputPerSentence, writeReorderingLabels);

			
		} catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
		}
		
		
	
    }
        
	
    public AlignmentTreePanel getAlignmentTreePanel()
    {
    	return (AlignmentTreePanel)this.treePanel;
    }
    
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.    	
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    
    // Mouse interface methods
	public void mouseClicked(MouseEvent e) 
	{
		System.out.println("Mouse clicked!!!");
		Point mousePos = e.getPoint();
		String description = ((AlignmentTreePanel)treePanel).guiClosestNodeDescription(mousePos);
		this.setOutputFieldText(description);
	}

	public void mouseEntered(MouseEvent arg0) 
	{	
	}

	public void mouseExited(MouseEvent arg0) 
	{	
	}

	public void mousePressed(MouseEvent arg0) 
	{
	}

	public void mouseReleased(MouseEvent arg0) 
	{
	}







	@Override
	public void loadSourceSentences(List<List<String>> sourceSentences) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void loadTargetSentences(List<List<String>> targetSentences) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void loadAlignments(List<Alignment> alignments) 
	{
		// TODO Auto-generated method stub
	}





}
