/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import javax.swing.JCheckBox;

import ccgLabeling.CCGLabel;
import extended_bitg.TreeRepresentation;
import tsg.TSNodeLabel;

public class TargetHatNodeLabelExtracter extends NodeLabelsExtracter
{

	protected TargetHatNodeLabelExtracter(JCheckBox showHATReorderingLabelsCheckBox, JCheckBox showParentRelativeReorderingLabelsCheckBox,
		JCheckBox showPreterminalLabelsCheckbox) {
		super(showHATReorderingLabelsCheckBox,showParentRelativeReorderingLabelsCheckBox,showPreterminalLabelsCheckbox);
	}

	@Override
	protected String getCCGLabel(TSNodeLabel n) 
	{
		return getTargetCCGLabel(n);
	}

	private String getTargetCCGLabel(TSNodeLabel n)
	{
		return n.getExtraLabel(CCGLabel.getCCGLabelProperty(TreeRepresentation.TargetCategory));
	}
}
