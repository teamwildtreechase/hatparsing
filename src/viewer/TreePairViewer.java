/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import tsg.TSNodeLabel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import tsg.corpora.Wsj;
import util.ConfigFile;				
import util.Pair;
import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.MenuInteraction;
import viewer_io.TreePairBankLoader;
import viewer_io.TreebankLoaderInterface;
import static viewer.TreePairPanel.createTreePairPanel;



@SuppressWarnings("serial")
public class TreePairViewer  extends TreeViewer<Pair<TSNodeLabel>> implements TreebankLoaderInterface<Pair<TSNodeLabel>> {
	
	private static Pair<TSNodeLabel> initialTreePair = initialTreePair();    

	public static Pair<TSNodeLabel> initialTreePair() {
		try 
		{
			TSNodeLabel firstTree = new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");
			TSNodeLabel secondTree = new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");
			
			return new Pair<TSNodeLabel>(firstTree,secondTree);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static TreePairViewer createTreePairViewer()
	{
		CheckBoxTable checkBoxTable = CheckBoxTable.createCheckBoxTable();
		TreePairPanel treePairPanel =  createTreePairPanel(checkBoxTable);
        treePairPanel.setBackground(Color.white);
        treePairPanel.setFocusable(false);
		
		ButtonIOComponentTable buttonIOComponentTable = ButtonIOComponentTable.createButtonIOComponentTable();
		return new TreePairViewer(buttonIOComponentTable, treePairPanel, checkBoxTable);
	}
	
	
    public TreePairViewer(ButtonIOComponentTable buttonIOComponentTable, TreePairPanel treePairPanel, CheckBoxTable checkBoxTable) 
    {
    	super(buttonIOComponentTable, treePairPanel, checkBoxTable);
        this.setupTreeViewer();
    }
    
    
    
    public void setInitialSentence() {
    	ArrayList<Pair<TSNodeLabel>> treebank = new ArrayList<Pair<TSNodeLabel>>();
        treebank.add(initialTreePair);
        loadTreebank(treebank, true);
    }
     
    	
	
	@Override
	protected void createInputFields() 
	{
		ConfigFile guiCachedConfig = null;
		try 
		{
			guiCachedConfig = new ConfigFile("guiCachedConfig");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("No cached config values found for gui, use defaults");
		}
		
		String configFileLocation = null;
		
		// Load cached values if available
		if(guiCachedConfig != null  && guiCachedConfig.hasValue("configFileLocation") )
		{
			configFileLocation =  guiCachedConfig.getValue("configFileLocation");
		}
		
		else // no values cached, use defaults
		{
			String startupLocation = "";
			try 
			{
				//TODO : Remove the jar this generates
				startupLocation = new File(AlignmentVisualizer.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).toString();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			configFileLocation = startupLocation +"/configfile.txt";
			//configFileLocation = "String /home/gemaille/DataPreprocessing/AlignmentVisualizer/src/viewer/configfile.txt";
		}
		
		JTextField permField = new JTextField("1,2,3");
		
		java.awt.Font f = new java.awt.Font("Arial",Font.BOLD, 18);
		permField.setFont(f);
		
		setIoComponentsTable(ButtonIOComponentTable.createButtonIOComponentTable());
		getIoComponentsTable().addComponent("configFile", new ButtonIOComponent(new JButton("Load everything from configile"), new JTextField(configFileLocation ),null, new JLabel("config file:")));
		
	}
	
    public static void doBeforeClosing(TreePairViewer treeViewer) 
    {	    	
     	saveGuiConfig(treeViewer);
    }    
	
    
    protected static void saveGuiConfig(TreePairViewer treeViewer)
	{
		System.out.println("Saving gui configuration...");
		
		try
		{
			BufferedWriter outWriter = new BufferedWriter(new FileWriter("guiCachedConfig"));
			
			outWriter.write("% This is an automatically generated configuration file \n");
			outWriter.write("% The file contains the values of parameters changed in and automatically stored by the GUI \n");
			String configFileLocationString = "configFileLocation =" + treeViewer.getIoComponentsTable().getComponentTextFieldString("configFile");
			
			outWriter.write(configFileLocationString + "\n");
			
			outWriter.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
    
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Tree Pair Viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final TreePairViewer treeViewer = createTreePairViewer();
        Dimension area = treeViewer.treePanel.getArea();
        frame.setMinimumSize(new Dimension(area.width+5, area.height+5));
        treeViewer.setOpaque(true); //content panes must be opaque
        frame.setContentPane(treeViewer);
        
        frame.addWindowListener (new WindowAdapter () {
            public void windowClosing (WindowEvent e) {
            	doBeforeClosing(treeViewer);
            }
        });

        
      //Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");     
        
        MenuInteraction menuInteraction = MenuInteraction.createMenuInteraction(treeViewer.treePanel);
        menuInteraction.addExportTreeItems(treeViewer.treePanel,"TreePair");
        menuInteraction.addLoadTreeItem(treeViewer, "Load Tree Pair Bank");
        menuInteraction.addItemsToMenu(menu);
        menuInteraction.addQuitter(treeViewer);
         
        
        treeViewer.getIoComponentsTable().get(0).getTheButton().addActionListener(new ActionListener()
        {
        	public void actionPerformed(ActionEvent e)
        	{
        		loadEverythingFromConfig(treeViewer);
        	}
        });
        
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
	
    public static void loadEverythingFromConfig(TreePairViewer treeViewer)
    {
    	System.out.println(">>> Load everything...");    			
		String configFilePath =  treeViewer.getIoComponentsTable().get(0).getTextfieldContents();
		System.out.println("Reading configfile from " + configFilePath);
		try{
			ConfigFile theConfig = new ConfigFile(configFilePath);
			
			
			// file name with full path
			String treeBankName = theConfig.getValue("treePairFileName");			
			
			
			File treeBankFile = new File(treeBankName);
			if(! TreePairBankLoader.loadTreePairBank(treeBankFile,treeViewer, true))
			{
				System.out.println("Please check that the path to the treebank is set correctly in the configfile");
			}	
		}
		catch(Exception except)
		{
			System.out.println("Error handling configfile: " + except);
		}

    }
    
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.    	
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }


	@Override
	public boolean loadTreebankAndSetSpinner(File file, boolean resetSpinner) 
	{
		List<Pair<TSNodeLabel>> treePairBank  = null;
		try 
		{
			treePairBank = Wsj.getTreePairbank(file);
			super.loadTreebank(treePairBank, resetSpinner);
			return true;
		} catch (Exception e) {
				
			e.printStackTrace();
			return false;
		}
		
	}


}
