/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;


import ccgLabeling.CCGLabeler;
import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import tsg.TSNodeLabel;
import viewer_io.CheckBoxTable;

public class LabeledAlignmentPanel extends AlignmentPanel
{
	private static final long serialVersionUID = 1L;
	
	private final ParsesTable sourceParses;
	private final ParsesTable targetParses;
	private static final UnaryCategoryHandlerType PanelUnaryCategoryHandlerType = UnaryCategoryHandlerType.ALL;

	private LabeledAlignmentPanel(CheckBoxTable checkBoxTable, ParsesTable sourceParses, ParsesTable targetParses) 
	{
		super(checkBoxTable);
		this.sourceParses = sourceParses;
		this.targetParses = targetParses;
	}

	public static LabeledAlignmentPanel createLabeledAlignmentPanel(CheckBoxTable checkBoxTable)
	{
		return new LabeledAlignmentPanel(checkBoxTable, ParsesTable.createParseTable(), ParsesTable.createParseTable());
	}
	
	public TSNodeLabel getSourceParse()
	{
		return sourceParses.getParse(this.sentenceNumber);
	}
	
	public TSNodeLabel getTargetParse()
	{
		return targetParses.getParse(this.sentenceNumber);
	}
	
	public ParsesTable getSourceParsesTable()
	{
		return sourceParses;
	}
	
	public ParsesTable getTargetParsesTable()
	{
		return targetParses;
	}
	
	private boolean hasSourceParses()
	{
		return this.sourceParses.hasParses();
	}
	
	private boolean hasTargetParses()
	{
		return this.targetParses.hasParses();
	}
	
	public boolean hasParses()
	{
		return hasSourceParses() || hasTargetParses();
	}
	
	public CCGLabeler getCCGLabeler()
	{
		if(this.hasSourceParses() && this.hasTargetParses())
		{
			return CCGLabeler.createSourceAndTargetCCGLabeler(getSourceParse(), getTargetParse(),PanelUnaryCategoryHandlerType,true,true);
		}
		else if(this.hasSourceParses())
		{
			return CCGLabeler.createSourceCCGLabeler(getSourceParse(),PanelUnaryCategoryHandlerType,true,true);
		}
		else if (this.hasTargetParses())
		{
			return CCGLabeler.createTargetCCGLabeler(getTargetParse(),PanelUnaryCategoryHandlerType,true,true);
		}
		else
		{
			return CCGLabeler.createEmptyCCGLabeler();
		}
	}
	
	public void setLevelSize(int levelSize)
	{
		this.panelSizeProperties.setLevelSize(levelSize);
	}
}