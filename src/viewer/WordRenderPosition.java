/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

public class WordRenderPosition 
{
	private int xLeftPos;
	private int xMiddlePos;
	
	public WordRenderPosition(int xLeftPos, int xMiddlePos)
	{
		this.xLeftPos = xLeftPos;
		this.xMiddlePos = xMiddlePos;
	}

	public int getXLeftPos()
	{
		return xLeftPos;
	}
	
	public int getXMiddlePos()
	{
		return xMiddlePos;	
	}
}
