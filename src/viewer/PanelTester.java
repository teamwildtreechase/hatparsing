/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class PanelTester 
{
	private JPanel panel;
	private BufferedImage image;
	
	private PanelTester(JPanel panel)
	{
		this.panel = panel;
	}
	
	public static PanelTester createPanelTester(JPanel panel)
	{
		return new PanelTester(panel);
	}
	
	private void produceImage()
	{
		image = (BufferedImage) panel.createImage(panel.getWidth(), panel.getHeight());
		
		paintPanelContentsToImage();
		
		assert(image.getWidth() > 0);
		assert(image.getHeight() > 0);
	}
	
	private void paintPanelContentsToImage() 
	{
		Graphics2D g2 = image.createGraphics();
		panel.paint(g2);
		g2.dispose();
	}
	
	
	public boolean panelContainsNonBackgroundColor()
	{
		return this.panelContainsPixelWithDifferentColor(this.panelBackGroundColor());
	}
	
	public int countNonBackgroundPixels()
	{	
		
		return  this.countNumPixelsWithDifferentColor(this.panelBackGroundColor());
	}
	
	
	private Color panelBackGroundColor()
	{
		return panel.getBackground();
	}
	
	
	public boolean panelContainsPixelWithColor(Color color)
	{
		System.out.println("PanelTester.panelHasNonBackgroundPixels called...");
		
		produceImage();
			
		for(Color pixelColor : getImagePixelList())
		{
			if(pixelColor.equals(color))
			{
				return true;
			}
		}
		return false;	
	}

	private boolean panelContainsPixelWithDifferentColor(Color referenceColor)
	{
		produceImage();
		
		for(Color pixelColor : getImagePixelList())
		{
			if(!pixelColor.equals(referenceColor))
			{
				return true;
			}
		}
		return false;	
	}
	
	
	public int countNumPixelsWithColor(Color color)
	{
		int result = 0;
		produceImage();
			
		for(Color pixelColor : getImagePixelList())
		{
			if(pixelColor.equals(color))
			{
				result++;
			}
		}
		return result;	
	}

	
	private int countNumPixelsWithDifferentColor(Color referenceColor)
	{
		int result = 0;
		produceImage();
			
		for(Color pixelColor : getImagePixelList())
		{
			if(!pixelColor.equals(referenceColor))
			{
				result++;
			}
		}
		return result;	
	}
	
	private List<Color> getImagePixelList()
	{
		System.out.println("Entered getImagePixelList...");
		List<Color> result = new ArrayList<Color>();
		
		for(int i = 0; i < image.getWidth(); i++)
		{
			for(int j = 0; j  < image.getHeight(); j++)
			{
				result.add(getColorForRGBValue(image.getRGB(i,j)));
				
			}
		}
		
		System.out.println("Generated list of " + result.size() + " pixels");
		
		return result;
	}
	
	
	private static Color getColorForRGBValue(int rgbaColorValue)
	{
		return new Color(rgbaColorValue,true);
	}
	
}
