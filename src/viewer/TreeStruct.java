/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.List;

import tsg.TSNodeLabel;

/**
 * This class will store and generate all the basic tables 
 * such as a table of lexicals that are associated with a tree
 * @author gemaille
 *
 */

public class TreeStruct 
{
	private TSNodeLabel theTree;
	
	private IdentityHashMap<TSNodeLabel, Integer> indexTable;
	private IdentityHashMap<TSNodeLabel, Integer> lexicalsIndexTable;
	private TSNodeLabel[] nodesArray, lexicalsArray;
	private int  nodesCount;
	private int sentenceLength;
	private String[] labelArray;
	private ArrayList<String> sourceWordArray;
	
	/** Constructor
	 *  
	 * @param theTree
	 */
	public TreeStruct(TSNodeLabel theTree)
	{
		this.theTree = theTree;
		generateAccessTables();
	}
	
	// Getters and Setters
	
	/**
	 *  
	 * @return The tree (in the form of a TSNodeLabel with associated links)
	 */
	public TSNodeLabel getTree()
	{
		return theTree;
	}
	
	public  IdentityHashMap<TSNodeLabel, Integer> getIndexTable()
	{
		return indexTable;
	}
	
	public IdentityHashMap<TSNodeLabel, Integer> getLexicalsIndexTable()
	{
		return lexicalsIndexTable;
	}	
	
	public int getNumLexicalItems()
	{
		return this.lexicalsArray.length;
	}
	
	public TSNodeLabel[] getNodesArray()
	{
		return nodesArray;
	}
	
	public List<TSNodeLabel> getLexicals()
	{
		return new ArrayList<TSNodeLabel>(Arrays.asList(lexicalsArray));
	}
	
	
	public TSNodeLabel getLexicalItem(int index)
	{
		return lexicalsArray[index];
	}
	
	
	public  void setLexicalsArray(TSNodeLabel[] lexicalsArray)
	{
		this.lexicalsArray = lexicalsArray;
	}
	
	public int getNodesCount()
	{
		return nodesCount;
	}
	
	public int getSentenceLength()
	{
		return sentenceLength;
	}
	
	
	public void setLabel(int index, String label)
	{
		this.labelArray[index] = label;
	}
	
	
	public String getLabel(int index)
	{
		return this.labelArray[index];
	}
	
	
	public TSNodeLabel getNode(int index)
	{
		return this.nodesArray[index];
	}
	
	public ArrayList<String> getSourceWordArray()
	{
		return sourceWordArray;
	}
	
	public void generateAccessTables()
	{
		
    	indexTable = new IdentityHashMap<TSNodeLabel, Integer>();
    	lexicalsIndexTable = new IdentityHashMap<TSNodeLabel, Integer>();
    	nodesCount = theTree.countAllNodes();
    	sentenceLength = theTree.countLexicalNodes();
    	lexicalsArray = theTree.collectLexicalItems().toArray(new TSNodeLabel[sentenceLength]);
    	nodesArray = theTree.collectAllNodes().toArray(new TSNodeLabel[nodesCount]);        	

    	for(int i=0; i<nodesCount; i++) 
    	{
    		TSNodeLabel n = nodesArray[i];
    		indexTable.put(n, i);
    	}
    	
    	for(int i = 0; i < lexicalsArray.length; i++)
    	{
    		TSNodeLabel n = lexicalsArray[i];
    		lexicalsIndexTable.put(n,i);
    	}
    	
		labelArray = new String[nodesCount];

		sourceWordArray = new ArrayList<String>(sentenceLength);
		

		for(int j=0; j<sentenceLength; j++) 
		{
			TSNodeLabel n = lexicalsArray[j];
			int i = indexTable.get(n);
			labelArray[i] = n.label();
			sourceWordArray.add(n.label.toString());
		
		}   
	}
}
