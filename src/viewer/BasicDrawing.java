/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.Arc2D;
import java.util.List;


import util.Utility;

/** Class that contains all the basic drawing methods
 * 
 * @author Gideon Maillette de Buy Wenniger
 *
 */

public class BasicDrawing 
{
	
	public static void drawCrossingSymbol(int x, int y, int width, int height, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int xPoints[] = new int[3];
		int yPoints[] = new int[3];
		
		// Draw lower left triangle (= left arrowhead)
		xPoints[0] = (int) (x);
		xPoints[1] = (int) (x + (0.40 * width));
		xPoints[2] = (int) (x );		
		yPoints[0] = (int)(y + (0.60 * height));
		yPoints[1] = y + height;
		yPoints[2] = y + height;
		g2.fillPolygon(xPoints,yPoints,3);
		
		
		// Draw lower right triangle (= right arrowhead)
		xPoints[0] = (int) (x + width);
		xPoints[1] = (int) (x + width);
		xPoints[2] = (int) (x + 0.60 * width);		
		yPoints[0] = (int)(y + (0.60 * height));
		yPoints[1] = y + height;
		yPoints[2] = y + height;
		g2.fillPolygon(xPoints,yPoints,3);
		
		
		Stroke s = g2.getStroke();
		// go to bigger pensize
		g2.setStroke(new BasicStroke(2));
		// Draw two diagonal crossing lines, forming a cross
		g2.drawLine(x, y, x+width, y + height);
		g2.drawLine(x+ width, y, x, y + height);
		
		// restore original stroke
		g2.setStroke(s);
	}
	
	
	
	public static void drawDiamond(int x, int y, int width, int height,int extraMargin,
			Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int xPoints[] = new int[4];
		int yPoints[] = new int[4];
		
		int xNew = x + extraMargin;
		int yNew = y + extraMargin;
		int heightNew = height - (2 * extraMargin);
		int widthNew = width - (2 * extraMargin);
		
		// xpoints: start in upper middle, and go around clockwise
		xPoints[0] = (int) (xNew + (0.5 * widthNew));
		xPoints[1] = (int) (xNew + (widthNew));
		xPoints[2] = (int) (xNew + (0.5 * widthNew));
		xPoints[3] = xNew;
		
		yPoints[0] = yNew;
		yPoints[1] = (int) (yNew + (0.5 * heightNew));
		yPoints[2] = (int) (yNew + (heightNew));
		yPoints[3] = (int) (yNew + (0.5 * heightNew));
		
		g2.fillPolygon(xPoints,yPoints,4);
	}
	
	public static void drawTriangle1(int x, int y, int width, int height,int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int xPoints[] = new int[3];
		int yPoints[] = new int[3];
		
		int xNew = x + extraMargin;
		int yNew = y + extraMargin;
		int heightNew = height - (2 * extraMargin);
		int widthNew = width - (2 * extraMargin);
		
		// xpoints: start in upper middle, and go around clockwise
		xPoints[0] = (int) (xNew + (0.5 * widthNew));
		xPoints[1] = xNew + widthNew;
		xPoints[2] = xNew;
	
		
		yPoints[0] = yNew;
		yPoints[1] = (int) (yNew + (heightNew));
		yPoints[2] = (int) (yNew + (heightNew));
		
		g2.fillPolygon(xPoints,yPoints,3);
	}

	public static void drawTriangle2(int x, int y, int width, int height,int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int xPoints[] = new int[3];
		int yPoints[] = new int[3];
		
		int xNew = x + extraMargin;
		int yNew = y + extraMargin;
		int heightNew = height - (2 * extraMargin);
		int widthNew = width - (2 * extraMargin);
		
		// xpoints: start in upper middle, and go around clockwise
		xPoints[0] = xNew;
		xPoints[1] = xNew + widthNew;
		xPoints[2] = (int) (xNew + (0.5 * widthNew));
	
		
		yPoints[0] = yNew;
		yPoints[1] = yNew;
		yPoints[2] = (int) (yNew + (heightNew));
		
		g2.fillPolygon(xPoints,yPoints,3);
	}
	
	
	private static void drawSymbolBullet(int x, int y, int diameter, int number,int maxVarNum, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		List<Integer> binaryRepresentation = Utility.convertToDifferntNumericalSystem(number, 2);
		
		
		//System.out.println("\nnumber: " + number + "binary representation: ");
		
		int maxNoBits = (int)Math.ceil(log(2,maxVarNum));
		//System.out.println("}}}maxVarNum: " + maxVarNum + " maxNoBits :" + maxNoBits );
		
		double delta = 0;
		
		if(maxNoBits > 0)
		{	
			delta = (360 / maxNoBits);
		}
		
		
		int lastDrawnPieIndex = -10;
		
		for(int i = 0; i < binaryRepresentation.size(); i++)
		{
			int index = binaryRepresentation.size() - 1 - i;
			int bit = binaryRepresentation.get(index);
			//System.out.print(bit);
			
			// The start of the pie in degrees, we will draw up to 8 pies
			double start =  90 - delta*i;
			
			if(bit == 1)
			{
				boolean pieAdjacentToLastDrawnPie = (i  == (lastDrawnPieIndex + 1)); 
				
				
				if(pieAdjacentToLastDrawnPie)
				{
					//System.out.println(">>>Ajacent Pie!!!");
					// Draw a slightly overlapping pie
					int overlap = 4;
					drawPie(x,y,diameter,diameter,start + overlap,-(delta + overlap),g2);
				}
				else
				{
					drawPie(x,y,diameter,diameter,start,- delta,g2);
				}
				lastDrawnPieIndex = i;
			}
		}
		//System.out.println("");
	}
	
	
	public static double log(int base, int number)
	{
		return Math.log(number) / Math.log(base);
	}
	
	public static void drawPie(int x, int y, int width, int height, double start, double delta, Graphics2D g2)
	{
		Arc2D.Double pie = new Arc2D.Double(x, y, width, height, start, delta, Arc2D.PIE);
		g2.fill(pie);
	}
	
	public static void drawClosedCircle(int x, int y, int width, int height,int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int widthOval = width - (2 * extraMargin);
		int heightOval = (int)height - (2 * extraMargin); 
		int xOval = x + extraMargin ;
		int yOval = y + extraMargin;
		g2.fillOval(xOval, yOval, widthOval, heightOval);
	}
	
	
	public static void drawOpenCircle(int x, int y, int width, int height,int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int widthOval = width - (2 * extraMargin);
		int heightOval = (int)height - (2 * extraMargin); 
		int xOval = x + extraMargin ;
		int yOval = y + extraMargin;
		g2.drawOval(xOval, yOval, widthOval, heightOval);
	}
	
	
	public static void drawHorizontalRectangle(int x, int y, int width, int height, int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int widthOval = width - (2 * extraMargin);
		int heightOval = (int)(height * 0.4); 
		int xOval = x + extraMargin ;
		int yOval = (int)(y + (height /4) + extraMargin);
	
		g2.fillRect(xOval, yOval, widthOval, heightOval);
	}
	
	public static void drawVerticalRectangle(int x, int y, int width, int height,int extraMargin, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int xOval = (int)(x + (width/4));;
		int yOval = y + extraMargin;
		int widthOval =(int) (width * 0.4) ;
		int heightOval = height - (2 * extraMargin); 
		g2.fillRect(xOval, yOval, widthOval, heightOval);
	}
	
	public static void drawNullBindingSymbol(int x, int y, int diameter,Graphics2D g2)
	{
		g2.fillRect((int)(x - diameter/6),(int)(y - diameter/2), diameter/3, diameter);
	}
	
	@SuppressWarnings("rawtypes")
	public static void drawNamePlateX(String name,TreePanel panel, int x, int y, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		Font normalFont = g2.getFont();
		Color normalColor = g2.getColor();
	
		FontMetrics metrics = panel.getFontMetrics(panel.getPanelSizeProperties().smallFont());
	
		int height = metrics.getHeight();
		
		int extraMargin = 1; // extra margin so the texts fits in "comfortably"
		
		int symbolPlateHeight = (int)(height * 1.1) ;
		int symbolPlateWidt = symbolPlateHeight * 2 +  extraMargin;
		int symbolPlateLeftTopX =  x -extraMargin;
		int symbolPlateLeftTopY =  y -extraMargin;
		
		g2.fillRect(symbolPlateLeftTopX,symbolPlateLeftTopY,  symbolPlateWidt, symbolPlateHeight);
		
		String symbolNumString = name.replaceAll("@","");
		int symbolNumber = Integer.parseInt(symbolNumString) - 1;
		
		System.out.println("sybmolNumber: " + symbolNumber);

		
		
		g2.setColor(Color.WHITE);
		g2.drawString("" + symbolNumber,x ,y + height - 3 * extraMargin);
		
		
		// restore the normal font and color
		g2.setFont(normalFont);
		g2.setColor(normalColor);
	}
	
	@SuppressWarnings("rawtypes")
	public static void drawSymbolNamePlate(String name,TreePanel panel,int x, int y, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		Font normalFont = g2.getFont();
		Color normalColor = g2.getColor();
		
		FontMetrics metrics = panel.getFontMetrics(panel.getPanelSizeProperties().smallFont());
		//int width = metrics.stringWidth(name);
		int height = metrics.getHeight();
		
		int extraMargin = 1; // extra margin so the texts fits in "comfortably"
		
		int symbolPlateHeight = (int)(height * 1.1) ;
		int symbolPlateWidt = symbolPlateHeight * 4 +  extraMargin;
		int symbolPlateLeftTopX =  x -extraMargin;
		int symbolPlateLeftTopY =  y -extraMargin;
		
		g2.fillRect(symbolPlateLeftTopX,symbolPlateLeftTopY,  symbolPlateWidt, symbolPlateHeight);
		
		int symbolMargin = 2;
		int subSymbolY = symbolPlateLeftTopY;
		int symbolWidth = (symbolPlateWidt / 5);
		int symbolHeight = (symbolPlateWidt / 5);
		
		String symbolNumString = name.replaceAll("@","");
		int symbolNumber = Integer.parseInt(symbolNumString) - 1;
		
		System.out.println("sybmolNumber: " + symbolNumber);

		
		
		g2.setColor(Color.WHITE);
		List<Integer> pentalRepresentation = Utility.convertToDifferntNumericalSystem(symbolNumber, 4);
		
		System.out.println(" pentalRepresentation : " +  pentalRepresentation);
		
		for(int i = 0; i < 4; i++)
		{
			int subSymbolX = symbolPlateLeftTopX + ((1 + i) * symbolWidth) ;
			
			int symbolIndex = -1;
			int pentalRepresentationIndex = i + (pentalRepresentation.size() -4); 
			if(pentalRepresentationIndex >= 0)
			{
				symbolIndex = pentalRepresentation.get(pentalRepresentationIndex);
			}
			
			
			if(symbolIndex == 0)
			{
//				drawCircle(subSymbolX, subSymbolY, symbolWidth, symbolHeight, g2);
				drawTriangle2(subSymbolX, subSymbolY, symbolWidth, symbolHeight,symbolMargin, g2);
			}
			else if(symbolIndex == 1)
			{	
				drawDiamond(subSymbolX, subSymbolY, symbolWidth, symbolHeight,symbolMargin, g2);
			}
			else if (symbolIndex == 2)
			{	
				drawVerticalRectangle(subSymbolX, subSymbolY, symbolWidth, symbolHeight,symbolMargin, g2);
			}
			else if(symbolIndex == 3) 
			{	
				drawHorizontalRectangle(subSymbolX, subSymbolY, symbolWidth, symbolHeight,symbolMargin, g2);
			}
			else if(symbolIndex == 4)
			{
				drawClosedCircle(subSymbolX, subSymbolY, symbolWidth, symbolHeight,symbolMargin / 2, g2);
			}
		}
		
		
		g2.setColor(Color.WHITE);
		g2.drawString("" + symbolNumber,x ,y + height - 3 * extraMargin);
		
		
		// restore the normal font and color
		g2.setFont(normalFont);
		g2.setColor(normalColor);
	}
	
	
	@SuppressWarnings("rawtypes")
	public static void drawCircleNodeSymbol(TreePanel panel,int x, int y, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		FontMetrics metrics = panel.getFontMetrics(panel.getPanelSizeProperties().smallFont());
		int height = metrics.getHeight();
		int diameter = (int)(height * 0.9);
		int circleX = (int)(x - 0.5* diameter);
		int circleY = y;
		
		drawClosedCircle(circleX,circleY,diameter,diameter,0,g2);
	}
	

	public static void drawNodeSymbolBullet(int diameter,int x, int y,int number,int maxVarNum, Graphics2D g2)
	{

		//setAntiAliasingOn(g2);
		
		int circleX = (int)(x - 0.5* diameter);
		int circleY = (int)(y - 0.5* diameter);
		
		Color normalColor = g2.getColor();
		drawClosedCircle(circleX,circleY,diameter,diameter,0,g2);
		g2.setColor(Color.WHITE);
		// Draw the Symbol bullet in white over circle
		drawSymbolBullet( circleX,  circleY, diameter, number - 1,maxVarNum, g2);
		g2.setColor(normalColor);
		// draw open circle to form the border
		drawOpenCircle(circleX,circleY,diameter,diameter,0,g2);
	}
	
	
	

	
	// Draws the crossing marker symbol, the center will be at (x,y)
	public static void drawCrossingMarker(int x, int y, int size, Graphics2D g2)
	 {
		 setAntiAliasingOn(g2);
		 int width, height;
		 width = height = size;
		drawCrossingSymbol((int)(x - (0.5 * width)) , (int)(y - 0.5 * height ) , width, height, g2); 
	 }
	
	
	public static void drawReorderingMarker(int x, int y, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		int width = 14;
		int height = 14;
		//g2.fillOval((int)(x - (0.5 * width)) , (int)(y - (0.5 * height)) - 1, width, height);
		drawDiamond((int)(x - (0.5 * width)) , (int)(y - (0.5 * height)) , width, height,0, g2);		
	}
	
	// This method is used to prevent very funny visualization bugs that happen when 
	// g2.drawLine(x1,y1,x1,y1) is called ( a line between two same points)
	// in combination with a preceding call to 	g2.setStroke(s);
	// Thes bugs are really a problem of Java, as it seems from what people write about it on the internet
	public static void drawLineSafe(int x1, int y1, int x2, int y2, Graphics2D g2) 
	{
		if((x1 == x2) && (y1 == y2))
		{
			System.out.println("Warning: drawing a line between two same points - skipped");
			return;
		}
		else
		{
			g2.drawLine(x1,y1,x2,y2);
		}
		
	}
	
	
	public static void drawLine(int x1, int y1, int x2, int y2, Graphics2D g2, boolean skewed) 
	{
		setAntiAliasingOn(g2);
		if((x1 == x2) && (y1 == y2))
		{
			System.out.println("Warning: drawing a line between two same points");
			return;
		}

		if (!skewed) 
		{
			if(x1 != x2)
			{
				int yM = y1 + ((int)((y2-y1) * 0.3));
				drawLineSafe(x1,y1,x1,yM,g2);
				drawLineSafe(x1,yM,x2,yM,g2);
				drawLineSafe(x2,yM,x2,y2,g2);
			}
			else
			{
				drawLineSafe(x1,y1,x2,y2,g2);
			}
		}	
		else 
		{
			drawLineSafe(x1,y1,x2,y2,g2);
			//g2.drawLine(x1,y1,x2/2,y2);
		}
	}

	
	public static void drawStripedLine(int x1, int y1, int x2, int y2, Graphics2D g2)
	{		
		setAntiAliasingOn(g2);
		int stepLength = 5; // small stripes have length 5
		double lineLength = Math.sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
		
		double xStep = (((x2-x1) * stepLength) / lineLength) ;
		double yStep = (((y2-y1)* stepLength) / lineLength );
		
		double xCur = x1;
		double yCur = y1;
		
		if(stripedLineDrawStepSizeBiggerThanZero(xStep,yStep)) // prevent problems for very short lines leading to infinite looping
		{
			// draw half line, half no line until the endpoint is reached
			while ( (Math.abs(xCur - x2) > (2 * Math.abs(xStep))) || ( Math.abs(y2 - yCur) > (2 * Math.abs(yStep))))
			{
				drawLineSafe((int)xCur,(int)yCur,(int)(xCur + xStep),(int)(yCur + yStep),g2);
				xCur += 2 * xStep;
				yCur += 2 * yStep;
			}
		}
	}
	
	private static boolean stripedLineDrawStepSizeBiggerThanZero(double xStep,double yStep)
	{
		return (Math.abs(xStep) > 0) || (Math.abs(yStep) > 0); // prevent problems for very short lines leading to infinite looping
	}
	
	
	public static void drawAlignmentLine(int x1, int y1, int x2, int y2, Graphics2D g2, boolean striped) 
	{			
		setAntiAliasingOn(g2);
		if(striped)
		{	
			drawStripedLine(x1,y1,x2,y2,g2);
		}
		else
		{	
			g2.drawLine(x1,y1,x2,y2);
		}	
	}
	
	
	public static void drawWords(SentenceRenderData sentenceRenderData, int yPosition, Graphics2D g2)
	{
		setAntiAliasingOn(g2);
		for(int i = 0; i < sentenceRenderData.getSentenceLength(); i++)
		{
			g2.drawString(sentenceRenderData.getWord(i), sentenceRenderData.getXLeftPos(i), yPosition);
		}
	}
	
	public static void setAntiAliasingOn(Graphics2D g2)
	{
		((Graphics2D)g2).setRenderingHint
		 (RenderingHints.KEY_ANTIALIASING,
		  RenderingHints.VALUE_ANTIALIAS_ON);
	}
	
	
}
