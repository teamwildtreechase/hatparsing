/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import tsg.corpora.ConstCorpus;
import util.Utility;

/**
 * This class implements an Iterator for files containing trees (with a tree possibly 
 * spread out over multiple lines). The Iterator collects lines until everything belonging 
 * to the next tree has been collected, and then returns the next tree.
 * The purpose of this class is to facilitate batch processing of very large Tree files,
 * were it is not a good idea to try to load everything into an ArrayList, but rather on 
 * demand reading of the next tree is required to keep things fitting into memory.
 * @author gemaille
 *
 */
public class TreeFileIterator 
{
	BufferedReader fileReader;
	private int treeNumber;
	private int initialSkip;
	
	public TreeFileIterator(String corpusLocation)
	{
		try 
		{
			fileReader = new BufferedReader(new FileReader(corpusLocation));
			this.treeNumber = 0;
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	public  TreeFileIterator(String corpusLocation, int initialSkip)
	{
		this(corpusLocation);
		
		this.initialSkip = initialSkip;
		
		// Skip the first initilSkip trees
		while(this.treeNumber < this.initialSkip)
		{
			if((this.treeNumber % 10000) == 0)
			{
				System.out.println("Skipped " + this.treeNumber + " trees");
			}
			this.getNextTree();
		}
	}
	
	
	/**
	 *  Close the file used by this tree iterator 
	 */
	public void closeFile()
	{
		System.out.println("Close iterators file");
		try {
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getNextTree()
	{
		int parenthesis = 0;
		String sentence = "";
		String line = null;
		try 
		{
			
			while( (line = fileReader.readLine()) != null)
			{
				if (line.length()==0) continue;
				if(line.charAt(0) == '%') continue;
				if (line.indexOf('(')==-1 && line.indexOf(')')==-1) continue;
				int parethesisInLine = Utility.countParenthesis(line);			
				parenthesis += parethesisInLine;
				sentence += line;
				
				//System.out.println("parenthesis :" + parenthesis);
				
				if (parenthesis==0)					
				{
					
					
					if (sentence.length()==0) continue;  //Fixed bug here (first: 	if (line.length()==0) continue;)  
					sentence = sentence.trim();				
					// Replace backslashes with empty String
					sentence = sentence.replaceAll("\\\\", "");
					// Remove empty lines : replace them with the empty String
					sentence = sentence.replaceAll("\n", "");
					// Replace long sequence of whitespace with just one whitespace character
					sentence = sentence.replaceAll("\\s+", " ");
					sentence = ConstCorpus.adjustParenthesisation(sentence);
					
					this.treeNumber++;
					//System.out.println("Sentence: " + sentence);
					return sentence;
					
				}				
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		System.out.println("No more lines");
		//System.out.println("sentence: " + sentence);
		return null;
	}
	
	public static boolean produceNormalizedTreeFile(String corpusLocation,String normalizedCorpusLocation)
	{
		try 
		{
			BufferedWriter normalizedCorpusWriter = new BufferedWriter(new FileWriter(normalizedCorpusLocation));
			TreeFileIterator tfr = new TreeFileIterator(corpusLocation,1000000);
			
			String normalizedTree = null;
			while((normalizedTree = tfr.getNextTree()) != null)
			{
				 normalizedCorpusWriter.write( normalizedTree + "\n"); 
			}
			
			normalizedCorpusWriter.close();
			tfr.closeFile();
			
			return true;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static void main(String [] args)
	{
		String corpusLocation, normalizedCorpusLocation;
		
		
		if(args.length ==  2)
		{
			System.out.println("args[0]: " + args[0] + "  args[1]: " + args[1]);
			corpusLocation = args[0];
			normalizedCorpusLocation = args[1];
			
			// Produce a normalized tree file
			TreeFileIterator.produceNormalizedTreeFile(corpusLocation, normalizedCorpusLocation);
			
		
		}
		else
		{
			System.out.println("Wrong usage.\nUsage: " + "java -jar produceNormalizedTreeFile.jar CorpusFileName OutputNormalizedCorpusFileName ");
		}	
	}

	

}
