/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class TranslationEquivalentsCoverageNormalizer extends ScoreNormalizer
{


	protected TranslationEquivalentsCoverageNormalizer(
			EbitgTreeStatisticsComputation treeStatisticsComputation) {
		super(treeStatisticsComputation);
	}

	@Override
	public int computeScoreDenominator(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg) 
	{
		//System.out.println("TranslationEquivalentsCoverageNormalizer.computeScoreDenominator");
		int result = treeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart(tg.getChart());
		assert(result >= 1);
		return  result;
	}

}
