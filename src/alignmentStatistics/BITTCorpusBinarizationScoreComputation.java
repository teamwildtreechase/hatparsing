/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

import util.DoubleMatrixCreater;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class BITTCorpusBinarizationScoreComputation extends CorpusBinarizationScoreComputation implements ScoreComputer 
{
	
	protected BITTCorpusBinarizationScoreComputation(List<List<Double>> summedPossibleInternalNodes, ScoreNormalizer scoreNormalizer,ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		super(summedPossibleInternalNodes, scoreNormalizer, scoreDerivativeComputation, treeStatisticsComputation);
	}
	
	
	protected BITTCorpusBinarizationScoreComputation(BITTCorpusBinarizationScoreComputation toCopy)
	{
		super(toCopy);
	}
	
	
	public static  BITTCorpusBinarizationScoreComputation createBITTCorpusBinarizationScoreComputation(int maxMaxSourceLengthAtomicFragment, ScoreNormalizer scoreNormalizer, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> summedInternalNodes = doubleMatrixCreator.createNumberMatrix(1, maxMaxSourceLengthAtomicFragment);
		return new BITTCorpusBinarizationScoreComputation(summedInternalNodes, scoreNormalizer, new BasicNormalization(), treeStatisticsComputation);
	}
	
	
	
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		assert(maxBranchingFactor == -1);
		double result = computeNumberInternalNodes(tg, maxSourceLengthAtomicFragment);
		//System.out.println("NumInternalnodesCompuation. computeIntermediateScore: " + result);
		return result;
	}
	
	
	private  int computeNumberInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment)
	{
		if(this.isBITT(tg, maxSourceLengthAtomicFragment))
		{	
			return this.getNumInternalNodes(tg,2);
		}
		
		// the sentence-pair alignment triple could not be parsed with this combination of 
		// maxBranchingFactor and maxSourceLengthAtomicFragment
		// It will be considered as being entirely flat, having no internal nodes and 
		// a binarization score of 0
		else 
		{
			return 0;
		}
	}
	
	@Override
	public double computeScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxBranchingFactor) 
	{
		return computeFinalScore(tg, maxSourceLengthAtomicFragment, -1, this);
	}
	
}
