/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTransductionOperation;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.NumberedObject;
import extended_bitg.EbitgInference.InferenceType;

public class TransductionOperationCountComputation 
{

	protected final HashMap<Integer,HashMap< EbitgTransductionOperation,Integer>> transductionOperationCounts;
	
	private TransductionOperationCountComputation( HashMap<Integer,HashMap< EbitgTransductionOperation,Integer>> transductionOperationCounts)
	{
		this.transductionOperationCounts = transductionOperationCounts;
	}
	
	public static TransductionOperationCountComputation createTransductionOperationCountComputation()
	{
		return new TransductionOperationCountComputation(new HashMap<Integer,HashMap< EbitgTransductionOperation,Integer>>());
	}
	
	public void  updateTransductionOperationCountsWithTree(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> tg)
	{
		//System.out.println("\n>>> updateTransductionOperationCountsWithTree");
		updateTransductionOperationCountsWithTreeState(tg.getRootState());
	}
	
	public void updateTransductionOperationCountsWithTreeState(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state)
	{
		InferenceType type = state.getInferenceType();
		
		// If the Inference at this state is of type Atomic or SourceNullsBinding, 
		// meaning it is a leaf rather than internal node, we return 0 for this subtree
		if((type == InferenceType.Atomic) || (type == InferenceType.SourceNullsBinding))
		{	
			return;
		}	
		else
		{
			incrementOperationCount(state);
		}

		for(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			updateTransductionOperationCountsWithTreeState(child);
		}
		
		return;
	}
	
	
	public void incrementOperationCount(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> treeState)
	{
		//System.out.println("Increment operation count...");
		
		EbitgTransductionOperation operation = treeState.createEbitgTransductionOperation();
		
		int branchingFactor = operation.getBranchingFactor();
		//System.out.println(" operation.getBranchingFactor():" +  operation.getBranchingFactor());
		
		HashMap<EbitgTransductionOperation, Integer> map;
		
		if(this.transductionOperationCounts.containsKey(branchingFactor))
		{
			map = this.transductionOperationCounts.get(branchingFactor);
		}
		else
		{
			// Generate new Hashmap that will contain the EbitgTransductionOperations with this 
			// branching factor
			map = new HashMap<EbitgTransductionOperation, Integer> ();
			// add the new Hashhmap to the containing Hashmap that is indexed on the Branchingfactors
			this.transductionOperationCounts.put(branchingFactor, map);
		}
		
		// Check if an entry already exists for operation
		if(map.containsKey(operation))
		{
			// entry exists, increment its count
			int newCount = map.get(operation) + 1;
			map.put(operation, newCount);
			
			//System.out.println("operation repetitively observed!");
			//System.exit(0);
			
		}
		else // generate new entry with count one
		{
			map.put(operation,1);
		}
	}
	
	
	public int getObservedTypesCount(int branchingFactor)
	{
		if(this.transductionOperationCounts.containsKey(branchingFactor))
		{
			HashMap<EbitgTransductionOperation, Integer> map = this.transductionOperationCounts.get(branchingFactor);
			return map.keySet().size();
		}
		else
		{
			System.out.println("branchingFactor" + branchingFactor + " not present in transductionOperationCounts");
			return 0;
		}
	}

	public int getObservedTokensCount(int branchingFactor)
	{
		if(this.transductionOperationCounts.containsKey(branchingFactor))
		{
			HashMap<EbitgTransductionOperation, Integer> map = this.transductionOperationCounts.get(branchingFactor);
			
			int numTokens = 0;
			
			for(int count : map.values())
			{
				numTokens += count;
			}
			
			return numTokens;
			
		}
		else
		{
			System.out.println("branchingFactor" + branchingFactor + " not present in transductionOperationCounts");
			return 0;
		}
	}
	
	public void addTransductionOperationCounts(TransductionOperationCountComputation computation2)
	{
		for(Integer key : computation2.transductionOperationCounts.keySet())
		{
			HashMap< EbitgTransductionOperation,Integer> map2 = computation2.transductionOperationCounts.get(key);
			
			if(this.transductionOperationCounts.containsKey(key))
			{
				HashMap< EbitgTransductionOperation,Integer> map1 = this.transductionOperationCounts.get(key);
				
				for(EbitgTransductionOperation operation : map2.keySet())
				{
					int summedCount;
					
					if(map1.containsKey(operation))
					{
						summedCount = map1.get(operation) + map2.get(operation);
					}
					else
					{
						summedCount = map2.get(operation);
					}
					
					map1.put(operation, summedCount);
				}
			}
			else
			{
				this.transductionOperationCounts.put(key, map2);
			}
		}
	}
	
	public ArrayList<NumberedObject<EbitgTransductionOperation>>  getFrequencySortedOperations(int branchingFactor)
	{
		
		ArrayList<NumberedObject<EbitgTransductionOperation>>  frequencySortedOperations = new ArrayList<NumberedObject<EbitgTransductionOperation>>();
		
		HashMap<EbitgTransductionOperation, Integer> map = transductionOperationCounts.get(branchingFactor);
		
		if(map != null)
		{	
			for(EbitgTransductionOperation operation : map.keySet())
			{
				int count = map.get(operation);
				 frequencySortedOperations.add(new NumberedObject<EbitgTransductionOperation>(operation,count));
			}
			
			 Collections.sort(frequencySortedOperations, Collections.reverseOrder());
		}	 
		
		return frequencySortedOperations;
	}
	
}
