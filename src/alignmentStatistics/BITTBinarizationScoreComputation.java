/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

import util.DoubleMatrixCreater;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class BITTBinarizationScoreComputation extends BinarizationScoreComputation implements ScoreComputer
{
	
	protected BITTBinarizationScoreComputation(List<List<Double>> meanBinarizationScores,  ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		super(meanBinarizationScores, scoreNormalizer, scoreDerivativeComputation, treeStatisticsComputation);
	}
	
	public BITTBinarizationScoreComputation(BITTBinarizationScoreComputation toCopy)
	{
		super(toCopy);
	}
	
	public static BITTBinarizationScoreComputation createBITTBinarizationScoreComputation(int maxMaxSourceLengthAtomicFragment, ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> scores = doubleMatrixCreator.createNumberMatrix(1, maxMaxSourceLengthAtomicFragment);
		return new BITTBinarizationScoreComputation(scores,  scoreNormalizer,scoreDerivativeComputation, treeStatisticsComputation);
	}
	
	
	public double computeBinarizationScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment)
	{
		//System.out.println(" BITTBinarizationScoreComputation: maxSourceLengthAtomicFragment: " + maxSourceLengthAtomicFragment);
		if(this.isBITT(tg,maxSourceLengthAtomicFragment))
		{	
			//System.out.println(" BITTBinarizationScoreComputation : getNumInternalNodes(tg,2) : " + ScoreComputation.getNumInternalNodes(tg,2));
			//System.out.println(" BITTBinarizationScoreComputation : tg.getSourceLength() : " + tg.getSourceLength());
			
			return  binarizationScore(this.getNumInternalNodes(tg,2),  scoreNormalizer.computeScoreDenominator(tg));
		}
		else
		{
			return  0;
		}
	}

	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		return computeBinarizationScore(tg, maxSourceLengthAtomicFragment);
	}
	
	
	@Override
	public double computeScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxBranchingFactor) 
	{
		//assert(maxBranchingFactor ==  -1);
		return (Double)computeFinalScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor, this);
	}
	
	
}
