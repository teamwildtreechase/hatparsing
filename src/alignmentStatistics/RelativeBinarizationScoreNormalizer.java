/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class RelativeBinarizationScoreNormalizer  extends ScoreNormalizer
{

	protected RelativeBinarizationScoreNormalizer(
			EbitgTreeStatisticsComputation treeStatisticsComputation) 
	{
		super(treeStatisticsComputation);
	}

	public static int determineMaxPossibleInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		// We want the number of internal nodes if any branching factor is allowed,
		// which will certainly be the case by choosing Integer.MAX_VALUE for it
		return EbitgTreeStatisticsComputation.determineMaxPossibleInternalNodes(tg);
	}
	
	public int computeScoreDenominator(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		if(treeStatisticsComputation.alignmentLength(tg) > 1)
		{	
			return determineMaxPossibleInternalNodes(tg);
		}
		
		return 1;
	}
}
