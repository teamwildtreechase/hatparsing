/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.concurrent.Callable;
import alignment.SourceString;
import util.Pair;

public abstract class EbitgCorpusComputationMultiThread<E extends EbitgCorpusComputationMultiThread<E, T>, T extends SourceString> implements Callable<E> {
	private final int NumSentencePairsUntilMessage = 100;
	protected final int maxAllowedInferencesPerNode;
	protected int minSentencePairNum, maxSentencePairNum;
	// protected String sourceLanguageFileName, targetLanguageFileName, alignmentsFileName;
	protected int outputLevel;
	protected final int maxSentenceLength;
	protected final InputStringsEnumeration<T> inputStringEnumeration;

	protected EbitgCorpusComputationMultiThread() {
		this.maxAllowedInferencesPerNode = -1;
		this.maxSentenceLength = -1;
		this.inputStringEnumeration = null;
	}

	protected EbitgCorpusComputationMultiThread(int maxAllowedInferencesPerNode, Pair<Integer> minMaxSentencePairNums, InputStringsEnumeration<T> inputStringEnumeration, int maxSentenceLength,
			int outputLevel) {
		assert (maxAllowedInferencesPerNode > 10000);
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.minSentencePairNum = minMaxSentencePairNums.getFirst();
		this.maxSentencePairNum = minMaxSentencePairNums.getSecond();
		// this.sourceLanguageFileName = sourceLanguageFileName;
		// this.targetLanguageFileName = targetLanguageFileName;
		// this.alignmentsFileName = alignmentsFileName;
		this.maxSentenceLength = maxSentenceLength;
		this.outputLevel = outputLevel;
		this.inputStringEnumeration = inputStringEnumeration;
	}

	public void performLoopCorpusComputation() {
		try {
			// int numSentencePairs = FileStatistics.countLines(alignmentsFileName);
			int numSentencePairs = (this.maxSentencePairNum - this.minSentencePairNum) + 1;

			long timeStart = System.currentTimeMillis();

			// Get a Runtime object

			doBeforeLooping();

			int sentencePairNum = 1;
			// While we manage to read a next source and target sentence and an alignment
			while (this.inputStringEnumeration.hasMoreElements()) {
				T nextElement = this.inputStringEnumeration.nextElement();
				String sourceSentence = nextElement.getSourceString();
				// String targetSentence = nextElement.getTargetString();
				// String alignment = nextElement.getAlignmentString();

				// fast stop for testing

				if (sentencePairNum < minSentencePairNum) {
					sentencePairNum++;
					continue;
				}
				if (sentencePairNum > maxSentencePairNum) {
					break;
				}
				// FIXME we want to identify the task not the thread id
				showOutputForSelectedSentencePair(sentencePairNum, "Thread ID: " + Thread.currentThread().getId());
				showOutputForSelectedSentencePair(sentencePairNum, "}}}Processing sentence pair " + sentencePairNum);
				showOutputForSelectedSentencePair(sentencePairNum, "sourceSentence: " + sourceSentence);

				// System.out.println("EbitgCorpusComputationMultiThread<E extends EbitgCorpusComputationMultiThread.performLoopCorpusComputation() - semtemcePairNum: "
				// + sentencePairNum);

				// System.out.println("sourceString: " + sourceSentence + "targetString: " + targetSentence +" alignmentString: " + alignment);
				performCorpusComputation(nextElement, sentencePairNum);

				int processedSentencePairNum = sentencePairNum - this.minSentencePairNum + 1;
				showOutputForSelectedSentencePair(processedSentencePairNum, progressTimeString(timeStart, processedSentencePairNum, numSentencePairs));

				sentencePairNum++;

				showOutputForSelectedSentencePair(sentencePairNum, "Heapsize end iteration:");
				showOutputForSelectedSentencePair(sentencePairNum, heapSize());

			}

			// writeResults(outputWriter);

			// close all the readers and writers

			doAfterLooping();

			showOutputForSelectedSentencePair(sentencePairNum, "Heapsize at termination");
			showOutputForSelectedSentencePair(sentencePairNum, heapSize());

			Thread.currentThread();
			// Allow other threads to execute
			// yield();
			Thread.sleep((long) 1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void showOutputForSelectedSentencePair(int sentencePairNum, String toShow) {
		boolean show = ((sentencePairNum % NumSentencePairsUntilMessage) == 0);

		if (show) {
			System.out.println(toShow);
		}
	}

	protected abstract void doBeforeLooping() throws IOException;

	protected abstract void performCorpusComputation(T input, int sentencePairNum);

	protected abstract void doAfterLooping() throws IOException;

	public static String heapSize() {
		long reservedSize = Runtime.getRuntime().totalMemory();
		long freeSize = Runtime.getRuntime().freeMemory();
		double heapSize = reservedSize - freeSize;

		// System.out.println("Heap size:" + heapSize + "bytes");
		return "Heap size:" + (heapSize / 1000000) + "mb";
	}

	public static String progressTimeString(long timeStart, int processedSentencePairNum, int numSentencePairs) {
		NumberFormat nf = NumberFormat.getInstance();
		double percentageCompleted = (processedSentencePairNum * 100.0) / numSentencePairs;
		long time = System.currentTimeMillis();
		long timePassed = time - timeStart;
		double fractionCompleted = ((double) processedSentencePairNum) / numSentencePairs;
		double fractionLeft = 1 - fractionCompleted;
		long timePerSentencePair = timePassed / processedSentencePairNum;

		long expectedTimeRemaining = (long) (((fractionLeft / fractionCompleted) * timePassed) / 1000);

		int hoursRemaining = (int) (expectedTimeRemaining / (60 * 60));
		expectedTimeRemaining = (expectedTimeRemaining % (60 * 60));
		int minutesRemaining = (int) (expectedTimeRemaining / 60);
		expectedTimeRemaining = (expectedTimeRemaining % (60));

		String result = "Average time per sentence pair: " + timePerSentencePair + "  miliseconds";
		result += "\n" + "Finished " + nf.format(percentageCompleted) + "%";
		result += "Expected: " + hoursRemaining + "hours, " + minutesRemaining + "minutes and " + expectedTimeRemaining + " seconds remaining";
		return result;
	}

	public boolean sentenceIsLongerThanMaxSentenceLength(String sentence) {
		int sentenceLength = sentence.split(" ").length;
		// System.out.println("maxSentenceLength: " + maxSentenceLength);
		return (sentenceLength > maxSentenceLength);
	}

	public void printTooLongSentencesSkippingMessage(int sentencePairNum, boolean sourceSentenceTooLong, boolean targetSentenceTooLong) {
		if (sourceSentenceTooLong && targetSentenceTooLong) {
			System.out.println("Skipping sentence pair " + sentencePairNum + " because both the source and target sentence are longer then the max allowed");
		} else if (sourceSentenceTooLong) {
			System.out.println("Skipping sentence pair " + sentencePairNum + " because both the source is longer then the max allowed");
		} else {
			System.out.println("Skipping sentence pair " + sentencePairNum + " because both the target is longer then the max allowed");
		}
	}

	public int getMaxAllowedInferencesPerNode() {
		return maxAllowedInferencesPerNode;
	}

	public int getMinSentencePairNum() {
		return this.minSentencePairNum;
	}

	public int getMaxSentencePairNum() {
		return this.maxSentencePairNum;
	}

	public void printMinMaxSentencePairNums() {
		System.out.println("Min/Max Sentence pair numbers" + this.minSentencePairNum + "," + this.maxSentencePairNum);
	}

}
