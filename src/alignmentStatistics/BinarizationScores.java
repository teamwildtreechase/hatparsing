/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import org.junit.Assert;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class BinarizationScores 
{
	public final int maxMaxBranchingFactor;
	public int maxMaxSourceLengthAtomicFragment;
	
	public final BinarizationScoreComputation binarizationScoreComputation;
	private final BinarizationScoreComputation binarizationScoreSTDComputation;
	private final BITTBinarizationScoreComputation bittBinarizationScoreComputation;
	protected final BITTBinarizationScoreComputation bittBinarizationScoreSTDComputation;
	protected final CorpusBinarizationScoreComputation corpusBinarizationScoreComputation;
	protected final BITTCorpusBinarizationScoreComputation bittCorpusBinarizationScoreComputation;
	protected final TranslationEquivalentsCoverageComputation translationEquivalentsCoverageComputation; 
	protected final CoverageWithExtraAlignmentsComputation coverageWithExtraAlignmentsComputation;
	protected final TranslationEquivalentsCoverageWithExtraAlignmentsComputation translationEquivalentsCoverageWithExtraAlignmentsComputation; 
	
	private BinarizationScores
	(int maxMaxBranchingFactor, 
	 int maxMaxSourceLengthAtomicFragment,
	 BinarizationScoreComputation binarizationScoreComputation,
	 BinarizationScoreComputation binarizationScoreSTDComputation,
	 BITTBinarizationScoreComputation bittBinarizationScoreComputation,
	 BITTBinarizationScoreComputation bittBinarizationScoreSTDComputation,
	 CorpusBinarizationScoreComputation numInternalNodesComputation,
	 BITTCorpusBinarizationScoreComputation bittCorpusBinarizationScoreComputation,
	 TranslationEquivalentsCoverageComputation translationEquivalentsCoverageComputation,
	 CoverageWithExtraAlignmentsComputation coverageWithExtraAlignmentsComputation,
	 TranslationEquivalentsCoverageWithExtraAlignmentsComputation translationEquivalentsCoverageWithExtraAlignmentsComputation
	 )
	{
		this.maxMaxBranchingFactor = maxMaxBranchingFactor;
		this.maxMaxSourceLengthAtomicFragment = maxMaxSourceLengthAtomicFragment;
		this.binarizationScoreComputation = binarizationScoreComputation;
		this.binarizationScoreSTDComputation = binarizationScoreSTDComputation;
		this.bittBinarizationScoreComputation = bittBinarizationScoreComputation;
		this.bittBinarizationScoreSTDComputation = bittBinarizationScoreSTDComputation;
		this.corpusBinarizationScoreComputation = numInternalNodesComputation;
		this.bittCorpusBinarizationScoreComputation = bittCorpusBinarizationScoreComputation;
		this.translationEquivalentsCoverageComputation = translationEquivalentsCoverageComputation;
		this.coverageWithExtraAlignmentsComputation = coverageWithExtraAlignmentsComputation;
		this.translationEquivalentsCoverageWithExtraAlignmentsComputation = translationEquivalentsCoverageWithExtraAlignmentsComputation;
		this.checkSizes();
		this.checkTypes();
	}
	
	

	private static BinarizationScores createBinarizationScores(int maxMaxBranchingFactor, int maxMaxSourceLengthAtomicFragment, ScoreNormalizer scoreNormalizer, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		BasicNormalization basicNormalization = new BasicNormalization();
		BinarizationScoreComputation binarizationScoreComputation = BinarizationScoreComputation.createBinarizationScoreComputation(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, scoreNormalizer,basicNormalization, treeStatisticsComputation);
		BinarizationScoreComputation binarizationScoreSTDComputation = BinarizationScoreComputation.createBinarizationScoreComputation(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, scoreNormalizer,null, treeStatisticsComputation);
		BITTBinarizationScoreComputation bittBinarizationScoreComputation =BITTBinarizationScoreComputation.createBITTBinarizationScoreComputation(maxMaxSourceLengthAtomicFragment, scoreNormalizer, basicNormalization, treeStatisticsComputation);
		BITTBinarizationScoreComputation bittBinarizationScoreSTDComputation =BITTBinarizationScoreComputation.createBITTBinarizationScoreComputation(maxMaxSourceLengthAtomicFragment, scoreNormalizer, null, treeStatisticsComputation);
		CorpusBinarizationScoreComputation corpusBinarizationScoreComputation = CorpusBinarizationScoreComputation.createCorpusBinarizationScoreComputation(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, scoreNormalizer, treeStatisticsComputation);
		BITTCorpusBinarizationScoreComputation bittCorpusBinarizationScoreComputation = BITTCorpusBinarizationScoreComputation.createBITTCorpusBinarizationScoreComputation(maxMaxSourceLengthAtomicFragment, scoreNormalizer, treeStatisticsComputation);
	
		
		// Create a translationEquivalents coverage normalizer, which normalizes by the number of 
		// translation equivalent nodes in the chart
		
		
		TranslationEquivalentsCoverageComputation translationEquivalentsCoverageComputation = TranslationEquivalentsCoverageComputation.createTranslationEquivalentsCoverageComputation(maxMaxBranchingFactor, treeStatisticsComputation);
		int maxMaxNumberExtraAlignments = CoverageWithExtraAlignmentsComputation.estimateMaximumNumberOfExtraAlignments(maxMaxSourceLengthAtomicFragment); 
		CoverageWithExtraAlignmentsComputation coverageWithExtraAlignmentsComputation = CoverageWithExtraAlignmentsComputation.createCoverageWithExtraAlignmentsComputation(maxMaxNumberExtraAlignments, treeStatisticsComputation);
		TranslationEquivalentsCoverageWithExtraAlignmentsComputation translationEquivalentsCoverageWithExtraAlignmentsComputation = TranslationEquivalentsCoverageWithExtraAlignmentsComputation.createTranslationEquivalentsCoverageWithExtraAlignmentsComputation(maxMaxNumberExtraAlignments, treeStatisticsComputation);
		return new BinarizationScores(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, 
				binarizationScoreComputation, binarizationScoreSTDComputation, 
				bittBinarizationScoreComputation,bittBinarizationScoreSTDComputation, 
				corpusBinarizationScoreComputation, 
				bittCorpusBinarizationScoreComputation,
				translationEquivalentsCoverageComputation,
				coverageWithExtraAlignmentsComputation,
				translationEquivalentsCoverageWithExtraAlignmentsComputation);	
	}


	public static BinarizationScores createBinarizationScores(int maxMaxBranchingFactor, int maxMaxSourceLengthAtomicFragment, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		ScoreNormalizer scoreNormalizer = new AbsoluteBinarizationScoreNormalizer(treeStatisticsComputation);
		return createBinarizationScores(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, scoreNormalizer, treeStatisticsComputation);
	}
	
	
	/**
	 * Copy factory method
	 * @param toCopy
	 * @return
	 */
	public static BinarizationScores createBinarizationScores(BinarizationScores toCopy)
	{
		BinarizationScoreComputation binarizationScoreComputation = new BinarizationScoreComputation(toCopy.binarizationScoreComputation);
		BinarizationScoreComputation binarizationScoreSTDComputation = new  BinarizationScoreComputation(toCopy.getBinarizationScoreSTDComputation());
		
		BITTBinarizationScoreComputation bittBinarizationScoreComputation = new BITTBinarizationScoreComputation(toCopy.getBittBinarizationScoreComputation());
		BITTBinarizationScoreComputation bittBinarizationScoreSTDComputation =new BITTBinarizationScoreComputation(toCopy.bittBinarizationScoreSTDComputation);
		
		CorpusBinarizationScoreComputation corpusBinarizationScoreComputation = new CorpusBinarizationScoreComputation(toCopy.corpusBinarizationScoreComputation);
		BITTCorpusBinarizationScoreComputation bittCorpusBinarizationScoreComputation = new BITTCorpusBinarizationScoreComputation(toCopy.bittCorpusBinarizationScoreComputation);
		TranslationEquivalentsCoverageComputation translationEquivalentsCoverageComputation = new TranslationEquivalentsCoverageComputation(toCopy.translationEquivalentsCoverageComputation);
		CoverageWithExtraAlignmentsComputation coverageWithExtraAlignmentsComputation = new CoverageWithExtraAlignmentsComputation(toCopy.coverageWithExtraAlignmentsComputation);
		TranslationEquivalentsCoverageWithExtraAlignmentsComputation translationEquivalentsCoverageWithExtraAlignmentsComputation = new TranslationEquivalentsCoverageWithExtraAlignmentsComputation(toCopy.translationEquivalentsCoverageWithExtraAlignmentsComputation); 
		return new BinarizationScores(toCopy.maxMaxBranchingFactor, toCopy.maxMaxSourceLengthAtomicFragment,
				binarizationScoreComputation, binarizationScoreSTDComputation, 
				bittBinarizationScoreComputation,bittBinarizationScoreSTDComputation, 
				corpusBinarizationScoreComputation, 
				bittCorpusBinarizationScoreComputation,
				translationEquivalentsCoverageComputation,
				coverageWithExtraAlignmentsComputation,
				translationEquivalentsCoverageWithExtraAlignmentsComputation);	
	}
	
	
	public static BinarizationScores createNodeExtractionScores(int maxMaxBranchingFactor, int maxMaxSourceLengthAtomicFragment, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		ScoreNormalizer scoreNormalizer = new RelativeBinarizationScoreNormalizer(treeStatisticsComputation);
		return createBinarizationScores(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment, scoreNormalizer, treeStatisticsComputation);
	}
	
	
	public void prepareForSTDComputation()
	{
		this.getBinarizationScoreSTDComputation().createSTDsComputation(this.binarizationScoreComputation);
		this.bittBinarizationScoreSTDComputation.createSTDsComputation(this.getBittBinarizationScoreComputation());
	}
	
	
	public void addBinarizationScores(BinarizationScores binarizationScores)
	{
		addMeans(binarizationScores);
		addSTDs(binarizationScores);
		addNumInternalNodes(binarizationScores);
		addNumTranslationEquivalentNodes(binarizationScores);
		addCoverageScores(binarizationScores);
		this.binarizationScoreSTDComputation.checkSize();
	}
	
	public void checkTypes()
	{
		Assert.assertFalse("BinarizationScores: binarizationScoreCompuation is of type BITTBinarizationScoreCompuation - wrong!", this.binarizationScoreComputation instanceof BITTBinarizationScoreComputation);
		Assert.assertFalse("BinarizationScores: binarizationScoreSTDCompuation is of type BITTBinarizationScoreCompuation - wrong!", this.binarizationScoreSTDComputation instanceof BITTBinarizationScoreComputation);
		Assert.assertTrue(this.bittBinarizationScoreComputation instanceof BITTBinarizationScoreComputation);
		Assert.assertTrue(this.bittBinarizationScoreSTDComputation instanceof BITTBinarizationScoreComputation);
		Assert.assertTrue(this.bittCorpusBinarizationScoreComputation instanceof BITTCorpusBinarizationScoreComputation);
		Assert.assertFalse(this.corpusBinarizationScoreComputation instanceof BITTCorpusBinarizationScoreComputation);
	}
	
	public void checkSizes()
	{
		this.binarizationScoreComputation.checkSize();
		this.binarizationScoreSTDComputation.checkSize();
	}
	
	
	private void addCoverageScores(BinarizationScores binarizationScores)
	{
		this.coverageWithExtraAlignmentsComputation.addScores(binarizationScores.coverageWithExtraAlignmentsComputation);
		this.translationEquivalentsCoverageWithExtraAlignmentsComputation.addScores(binarizationScores.translationEquivalentsCoverageWithExtraAlignmentsComputation);
	}
	
	private void addNumTranslationEquivalentNodes(BinarizationScores binarizationScores)
	{
		this.translationEquivalentsCoverageComputation.addScores(binarizationScores.translationEquivalentsCoverageComputation);
	}
	
	private void addNumInternalNodes(BinarizationScores binarizationScores)
	{
		this.corpusBinarizationScoreComputation.addScores(binarizationScores.corpusBinarizationScoreComputation);
		this.bittCorpusBinarizationScoreComputation.addScores(binarizationScores.bittCorpusBinarizationScoreComputation);
	}
	
	
	private void addMeans(BinarizationScores binarizationScores)
	{
		this.binarizationScoreComputation.addScores(binarizationScores.binarizationScoreComputation);
		this.bittBinarizationScoreComputation.addScores(binarizationScores.bittBinarizationScoreComputation);
	}
	
	void addSTDs(BinarizationScores binarizationScores)
	{
		this.binarizationScoreSTDComputation.addScores(binarizationScores.binarizationScoreSTDComputation);
		this.bittBinarizationScoreSTDComputation.addScores(binarizationScores.bittBinarizationScoreSTDComputation);
	}
	
	
	/**
	 * This method updates the binarization scores with the scores for a particular 
	 * treegrower (=> a certain <sourceSentence,targetAlignment,Alignment Triple>)
	 * The binarization scores describe qualitatively the fraction given by the 
	 * number of internal nodes present in the tree divided by the maximum number 
	 * internal nodes possible (i.e. if we were able to find a fully binary derivation).
	 * The score is maximally 100 (fully binary derivation) and minimally 0 (completely flat derivation)
	 * The score depends on what branching factor we allow, subtrees that start with a nod having 
	 * more children then the maximal branching factor allowed, will be counted as having 0 internal nodes.
	 * @param tg
	 * @param sourceLength
	 */
	public void updateScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,  boolean computeSTDs)
	{
		//System.out.println("BinarizationScores.updateScores called - tg.getSourceLength():" + tg.getSourceLength());
	
		if(computeSTDs)
		{	
			this.getBinarizationScoreSTDComputation().updateScores(tg);
			this.bittBinarizationScoreSTDComputation.updateScores(tg);
		}
		else
		{	
			this.binarizationScoreComputation.updateScores(tg);
			this.getBittBinarizationScoreComputation().updateScores(tg);
			this.corpusBinarizationScoreComputation.updateScores(tg);
			this.bittCorpusBinarizationScoreComputation.updateScores(tg);
			this.translationEquivalentsCoverageComputation.updateScores(tg);
			this.coverageWithExtraAlignmentsComputation.updateScores(tg);
			this.translationEquivalentsCoverageWithExtraAlignmentsComputation.updateScores(tg);
		}	
	}
	
	public void computeMeanBinarizationScores(int numParsedSentences)
	{
		//System.out.println("\t ||||| computeMeanBinarizationScores, numParsedSentences: " + numParsedSentences);
		
		this.binarizationScoreComputation.computeMeanBinarizationScores(numParsedSentences);
		this.getBittBinarizationScoreComputation().computeMeanBinarizationScores(numParsedSentences);
	}
	
	public void computeSTDBinarizationScores(int numParsedSentences)
	{
		//System.out.println("\t ||||| computeSTDBinarizationScores, numParsedSentences: " + numParsedSentences);
		
		this.getBinarizationScoreSTDComputation().computeSTDBinarizationScores(numParsedSentences);
		this.bittBinarizationScoreSTDComputation.computeSTDBinarizationScores(numParsedSentences);
	}

	public void computeCorpusScores()
	{
		this.corpusBinarizationScoreComputation.computeCorpusScores();
		this.bittCorpusBinarizationScoreComputation.computeCorpusScores();
		this.translationEquivalentsCoverageComputation.computeCorpusScores();
		this.coverageWithExtraAlignmentsComputation.computeCorpusScores();
		this.translationEquivalentsCoverageWithExtraAlignmentsComputation.computeCorpusScores();
	}

	public BinarizationScoreComputation getBinarizationScoreComputation()
	{
		return this.binarizationScoreComputation;
	}
	
	public BITTBinarizationScoreComputation getBITTBinarizationScoreComputation()
	{
		return this.getBittBinarizationScoreComputation();
	}

	public CorpusBinarizationScoreComputation getCorpusBinarizationScoreComputation()
	{
		return this.corpusBinarizationScoreComputation;
	}
	
	public BITTCorpusBinarizationScoreComputation getBITTCorpusBinarizationScoreComputation()
	{
		return this.bittCorpusBinarizationScoreComputation;
	}



	public BITTBinarizationScoreComputation getBittBinarizationScoreComputation() {
		return bittBinarizationScoreComputation;
	}

	public BinarizationScoreComputation getBinarizationScoreSTDComputation() {
		return binarizationScoreSTDComputation;
	}
	
	
	public TranslationEquivalentsCoverageComputation getTranslationEquivalentsCoverageComputation()
	{
		return translationEquivalentsCoverageComputation;
	}
	
	public CoverageWithExtraAlignmentsComputation getCoverageWithExtraAlignmentsComputation()
	{
		return coverageWithExtraAlignmentsComputation;
	}
	
}
