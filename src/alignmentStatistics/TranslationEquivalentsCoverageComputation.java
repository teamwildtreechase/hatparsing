/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import util.DoubleMatrixCreater;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class TranslationEquivalentsCoverageComputation extends CorpusScoreComputation implements ScoreComputer 
{
	
	protected TranslationEquivalentsCoverageComputation(
			List<List<Double>> basicScores, ScoreNormalizer scoreNormalizer,
			ScoreDerivativeComputation scoreDerivativeComputation,
			EbitgTreeStatisticsComputation treeStatisticsComputation) 
	{
		super(basicScores, scoreNormalizer, scoreDerivativeComputation,
				treeStatisticsComputation);
	}

	protected TranslationEquivalentsCoverageComputation(TranslationEquivalentsCoverageComputation toCopy)
	{
		super(toCopy);
	}
	
	public static TranslationEquivalentsCoverageComputation  createTranslationEquivalentsCoverageComputation(int maxMaxBranchingFactor, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		TranslationEquivalentsCoverageNormalizer translationEquivalentsCoverageNormalizer = new TranslationEquivalentsCoverageNormalizer(treeStatisticsComputation);
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> scores = doubleMatrixCreator.createNumberMatrix(1,maxMaxBranchingFactor);
		return new TranslationEquivalentsCoverageComputation (scores,translationEquivalentsCoverageNormalizer,new BasicNormalization(), treeStatisticsComputation);
	}
	
	
	public List<List<Double>>  computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		List<List<Double>> result = new ArrayList<List<Double>>();
		
		List<Double> scoreList = new ArrayList<Double>();
		for(int maxBranchingFactor = 1;maxBranchingFactor <= this.maxMaxBranchingFactor(); maxBranchingFactor++)
		{
					
			double score = computeScore(tg,1,maxBranchingFactor);
			assert(score >= 0);
			scoreList.add(score);
		}
		  // Add to Matrix containing all scores for this sentence 
		 result.add(scoreList);
		
		//System.out.println("result.size(): " + result.size() + "," + result.get(0).size());
		
		 Assert.assertEquals(basicScores.size(), result.size());
		 Assert.assertEquals(basicScores.get(0).size(), result.get(0).size());
		 
		return result;
	}
	
	private  double computeNumberExtractableTranslationEquivalentNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxBranchingFactor)
	{
		if(this.acceptableMaximalBranching(tg, 1, maxBranchingFactor))
		{
			return this.treeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart(tg.getChart());
		}
		return 0;
	}
	

	@Override
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxBranchingFactor) 
	{
		return this.computeNumberExtractableTranslationEquivalentNodes(tg, maxBranchingFactor);
	}

	@Override
	public double lengthOneScore() 
	{
		return 1;
	}
	
	@Override
	/**
	 *  Beware that the maxMaxBranchingFactor function is overwritten from the 
	 *  original in the superclass ScoreComputation.In this computation class
	 *  there is no AtomicLengt, so the branching factor takes its place on the 
	 *  horizontal axis. 
	 */
	public final int maxMaxBranchingFactor()
	{
		return this.basicScores.get(0).size();
	}

}
