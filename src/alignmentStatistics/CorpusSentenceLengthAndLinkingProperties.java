/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeState;

import junit.framework.Assert;

import util.MeanStdComputation;

public class CorpusSentenceLengthAndLinkingProperties extends CorpusSentenceLengthProperties
{

	private final List<Integer> sentencesNumberOfLinks;
	
	public CorpusSentenceLengthAndLinkingProperties(List<Integer> sourceSentenceLengths,  List<Integer> targetSentenceLengths, List<Double> sourceTargetLengthRatios,List<Integer> sentencesNumberOfLinks)
	{
		super(sourceSentenceLengths, targetSentenceLengths, sourceTargetLengthRatios);
		this.sentencesNumberOfLinks = sentencesNumberOfLinks;
	}
	
	public static CorpusSentenceLengthAndLinkingProperties createCorpusSentenceLengthAndLinkingProperties()
	{
		return new CorpusSentenceLengthAndLinkingProperties(new ArrayList<Integer>(), new ArrayList<Integer>(), new ArrayList<Double>(),new ArrayList<Integer>());
	}
	
	
	protected void addCorpusLengthProperties(CorpusSentenceLengthAndLinkingProperties corpusLengtProperties)
	{
		super.addCorpusLengthProperties(corpusLengtProperties);
		this.sentencesNumberOfLinks.addAll(corpusLengtProperties.sentencesNumberOfLinks);
	}
	
	public void updateSentenceLengthAndLinkingStatistics(EbitgChartBuilder<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> chartBuilder)
	{
		int sourceLength = chartBuilder.getSourceLength();
		int targetLength = chartBuilder.getTargetLength();
		int numLinks = chartBuilder.getAlignmentChunking().getNumLinks();
		super.updateSentenceLengthStatistics(sourceLength, targetLength);
		this.sentencesNumberOfLinks.add(numLinks);
		//System.out.println("SourceLength: " + sourceLength + " Target Length: " + targetLength + " Num links: " + numLinks);
		
	}
	
	public double getMeanNumLinks()
	{
		return MeanStdComputation.computeIntsMean(sentencesNumberOfLinks);
	}
	
	public double getStdNumLinks()
	{
		return MeanStdComputation.computeIntsStd(sentencesNumberOfLinks);
	}
	
	public double getMeamNumLinksMinAlignmentLengthRatio()
	{
		return MeanStdComputation.computeDoublesMean(getNumLinksPairwiseDividedByMinSourceTargetLengthList());
	}
	
	public double getStdNumLinksMinAlignmentLengthRatio()
	{
		return MeanStdComputation.computeDoublesStd(getNumLinksPairwiseDividedByMinSourceTargetLengthList());
	}

	public double getMeamNumLinksSourceLengthRatio()
	{
		return   MeanStdComputation.computeDoublesMean(getNumLinksPairwiseDividedBySourceLengthList());
	}

	public double getStdNumLinksSourceLengthRatio()
	{
		return MeanStdComputation.computeDoublesStd(getNumLinksPairwiseDividedBySourceLengthList());
	}
	
	public double getMeamNumLinksTargetLengthRatio()
	{
		return MeanStdComputation.computeDoublesMean(getNumLinksPairwiseDividedByTargetLengthList());
	}

	public double getStdNumLinksTargetLengthRatio()
	{
		return MeanStdComputation.computeDoublesStd(getNumLinksPairwiseDividedByTargetLengthList());
	}
	
	private List<Double> getNumLinksPairwiseDividedByMinSourceTargetLengthList()
	{
		return (pairWiseDivision(this.sentencesNumberOfLinks, getMinSourceTargetLengthList()));
	}

	private List<Double> getNumLinksPairwiseDividedByTargetLengthList()
	{
		return (pairWiseDivision(this.sentencesNumberOfLinks,this.targetSentenceLengths));
	}
	
	
	private List<Double> getNumLinksPairwiseDividedBySourceLengthList()
	{
		return (pairWiseDivision(this.sentencesNumberOfLinks,this.sourceSentenceLengths));
	}
	
	private List<Integer> getMinSourceTargetLengthList()
	{
		return pairWiseMinList(this.sourceSentenceLengths, this.targetSentenceLengths);
	}
	
	
	private static <T extends Number>  List<Double> pairWiseDivision(List<T> numeratorsList, List<T> denominatorsList)
	{
		List<Double> result = new ArrayList<Double>();
		
		Assert.assertEquals(numeratorsList.size(), denominatorsList.size());
		
		for(int i = 0; i < numeratorsList.size(); i++)
		{
			Number number1 = numeratorsList.get(i);
			Number number2 = denominatorsList.get(i);
			Double n = number1.doubleValue() / number2.doubleValue();
			result.add(n);
		}
		return result;
	}
	
	
	private static <T extends Comparable<T>>  List<T> pairWiseMinList(List<T> list1, List<T> list2)
	{
		List<T> result = new ArrayList<T>();
		
		Assert.assertEquals(list1.size(), list2.size());
		
		for(int i = 0; i < list1.size(); i++)
		{
			T element1 = list1.get(i);
			T element2 = list2.get(i);
			
			boolean firstElementSmallest = (element1.compareTo(element2) < 0); 
			
			if(firstElementSmallest)
			{
				result.add(element1);
			}
			else
			{
				result.add(element2);
			}
		}
		return result;
	}
	
	public int minNumLinks()
	{
		return Collections.min(this.sentencesNumberOfLinks);
	}
	
	public int maxNumLinks()
	{
		return Collections.max(this.sentencesNumberOfLinks);
	}
	
	protected String getLinkingPropertiesString()
	{
		String result = "";
		result += "\n Min Number Of Links: " + nf.format(this.minNumLinks());
		result += "\n Max Number Of Links: " + nf.format(this.maxNumLinks());
		result += "\n Mean Number Of Links: " + nf.format(this.getMeanNumLinks()) + " Standard deviation: " + nf.format(getStdNumLinks());
		result += "\n Mean (Number Of Links / Source Length) ratio : " + nf.format(this.getMeamNumLinksSourceLengthRatio()) + " Standard deviation: " + nf.format(getStdNumLinksSourceLengthRatio());
		result += "\n Mean (Number Of Links / Target Length) ratio : " + nf.format(this.getMeamNumLinksTargetLengthRatio()) + " Standard deviation: " + nf.format(getStdNumLinksTargetLengthRatio());
		result += "\n Mean (Number Of Links / min(Source Length, Target Length)) ratio : " + nf.format(this.getMeamNumLinksMinAlignmentLengthRatio()) + " Standard deviation: " + nf.format(getStdNumLinksMinAlignmentLengthRatio());
		return result;
	}
	
}
