/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;
import util.DoubleMatrixCreater;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeState.TreeType;

public abstract class ScoreComputation 
{
	protected final List<List<Double>> basicScores;
	protected final EbitgTreeStatisticsComputation treeStatisticsComputation;
	final ScoreNormalizer scoreNormalizer;
	ScoreDerivativeComputation scoreDerivativeComputation;
	
	
	protected ScoreComputation(List<List<Double>> basicScores, ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		this.basicScores = basicScores;
		this.scoreNormalizer = scoreNormalizer;
		this.scoreDerivativeComputation = scoreDerivativeComputation;
		this.treeStatisticsComputation = treeStatisticsComputation;
	}
	
	/**
	 * Copy constructor
	 * @param toCopy
	 */
	protected ScoreComputation(ScoreComputation toCopy)
	{
		this.scoreNormalizer = toCopy.scoreNormalizer;
		this.scoreDerivativeComputation = toCopy.scoreDerivativeComputation;
		this.treeStatisticsComputation = toCopy.treeStatisticsComputation;
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		this.basicScores = doubleMatrixCreator.cloneNumberMatrix(toCopy.basicScores);
		//System.out.println(">>>>>>>ScoreComputation>>Size of Copied matrix: " + this.basicScores.size() + "," + this.basicScores.get(0).size());
		
		//System.out.println(">>>>>>>ScoreComputation>>Size of Copied matrix: " + this.basicScores.size() + "," + this.basicScores.get(0).size());
	}

	public boolean isBITT(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment)
	{
		//System.out.println("EbitgTreeStatisticsComputation.determineTreeType(tg, maxSourceLengthAtomicFragment)" + EbitgTreeStatisticsComputation.determineTreeType(tg, maxSourceLengthAtomicFragment));
		//System.out.println(tg.getSourceLength());
		//System.out.println(tg.getTreeString());
		return (treeStatisticsComputation.determineTreeType(tg, maxSourceLengthAtomicFragment).equals(TreeType.BITT));
	}
	
	public boolean acceptableMaximalBranching(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		return (this.getMaxOccuringBranching(tg, maxSourceLengthAtomicFragment) <= maxBranchingFactor);
	}
	
	public List<Double> computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int  maxBranchingFactor, ScoreComputer scoreComputer)
	{
		List<Double> result = new ArrayList<Double>();
		
		for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <= maxMaxSourceLengthAtomicFragment(); maxSourceLengthAtomicFragment++)
		{			
			double score = scoreComputer.computeScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor);
	
			//System.out.println(">>>ScoreComputation.computeSentenceScores  original score:" + score);
			score = scoreDerivativeComputation.computeDerivedScore(score, maxBranchingFactor - 1, maxSourceLengthAtomicFragment -1);	
		
			//System.out.println(">>>ScoreComputation.computeSentenceScores  score:" + score);
			
			
			assert(score >= 0);
			result.add(score);
		}
		
		return result;
	}	
	
	public int maxMaxBranchingFactor()
	{
		int result = this.basicScores.size();
		assert(result > 0);
		return result;
	}
	
	public final int maxMaxSourceLengthAtomicFragment()
	{
		return this.basicScores.get(0).size();
	}
	
	public abstract void updateScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg);
	
	public abstract List<Double> getDataRowToRender(int index);
	
	public void addScores(ScoreComputation scoreComputation)
	{
		EbitgTreeStatisticsData.addArrays(this.basicScores, scoreComputation.basicScores);
	}
	
	public final double getScore(int maxBranchingFactor, int maxSourceLength)
	{
		//System.out.println("BinarizationScoreComputation: this.basicScores.size: " + this.basicScores.size() + ","  + this.basicScores.get(0).size());
		return this.basicScores.get(maxBranchingFactor -1).get(maxSourceLength - 1);
	}
	
	
	public int getNumInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxBranchingFactor)
	{
		// Compute double of internal nodes for sentence given the max branching factor
		return  treeStatisticsComputation.determineNumberInternalNodes(tg, maxBranchingFactor);
	}
	
	public double computeFinalScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor,ScoreComputer scoreComputer)
	{
		double result = 0;
		
		if(treeStatisticsComputation.alignmentLength(tg) > 1)
		{	
			result = scoreComputer.computeIntermediateScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor);
		}
		else
		{
			result =  scoreComputer.lengthOneScore();
		}
		
		//System.out.println("ScoreComputation.finalScore: " + result);
		
		assert(result >= 0);
		
		return result;
	}
	
	public int getMaxOccuringBranching(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>>tg, int maxSourceLengthAtomicFragment)
	{
		return treeStatisticsComputation.determineMaximalOccuringBranching(tg, maxSourceLengthAtomicFragment);
	}
}