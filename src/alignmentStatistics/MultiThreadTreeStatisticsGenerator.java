/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import alignment.AlignmentStringTriple;
import util.FilePartMerger;
import util.FileUtil;
import util.Pair;
import static alignmentStatistics.EbitgTreeStatisticsData.createEbitgTreeStatisticsData;

public class MultiThreadTreeStatisticsGenerator extends MultiThreadCorpusDataGenerator {
	private final int maxSourceLength;
	private final static String AtomicPartsMustAllShareSameAlignmentChunkLabel = "atomicPartsMustAllShareSameAlignmentChunk";
	private final boolean atomicPartsMustAllShareSameAlignmentChunk;
	private final String rootOutputFolderPath;
	private final String hatPropertiesFileName;

	private MultiThreadTreeStatisticsGenerator(MultiThreadComputationParameters parameters, int maxSourceLength, String rootOutputFolderPath, String hatPropertiesFileName,
			boolean atomicPartsMustAllShareSameAlignmentChunk) {
		super(parameters);
		this.maxSourceLength = maxSourceLength;
		this.rootOutputFolderPath = rootOutputFolderPath;
		this.hatPropertiesFileName = hatPropertiesFileName;
		this.atomicPartsMustAllShareSameAlignmentChunk = atomicPartsMustAllShareSameAlignmentChunk;
	}

	public static MultiThreadTreeStatisticsGenerator createMultiThreadTreeStatisticsGenerator(String configFileName, int numThreads) {
		MultiThreadComputationParameters parameters = MultiThreadComputationParameters.createMultiThreadComputationParameters(configFileName, numThreads);
		int maxSourceLength = parameters.theConfig.getIntIfPresentOrReturnDefault("maxSourceLength", 40);
		String hatPropertiesFileName = parameters.theConfig.getStringIfPresentOrReturnDefault("hatPropertiesFileName", "alignmentTripleProperties");
		String rootOutputFolderPath = parameters.theConfig.getStringIfPresentOrReturnDefault("rootOutputFolderPath", "");
		String atomicPartsMustAllShareSameAlignmentChunkString = parameters.theConfig.getStringIfPresentOrReturnDefault(AtomicPartsMustAllShareSameAlignmentChunkLabel, "false");
		boolean atomicPartsMustAllShareSameAlignmentChunk = Boolean.parseBoolean(atomicPartsMustAllShareSameAlignmentChunkString);
		return new MultiThreadTreeStatisticsGenerator(parameters, maxSourceLength, rootOutputFolderPath, hatPropertiesFileName, atomicPartsMustAllShareSameAlignmentChunk);
	}

	public EbitgTreeStatisticsData createMergedMeansData(List<EbitgTreeStatisticsMultiThread> registeredThreads, boolean atomicPartsMustAllShareSameAlignmentChunk) {

		EbitgTreeStatisticsData mergedData = createEbitgTreeStatisticsData(parameters.getMaxAllowedInferencesPerNode(), maxSourceLength, atomicPartsMustAllShareSameAlignmentChunk);

		// Merge the data collected by the different threads
		for (EbitgTreeStatisticsMultiThread thread : registeredThreads) {
			mergedData.addTreeStatisticsData(thread.statisticsData);
		}

		// Compute the mean of the binarization scores
		// and corpus binarization scores for the merged data
		mergedData.computeMeanBinarizationScores();
		mergedData.computeCorpusBinarizationScores();

		System.out.println(mergedData.getSentenceLengthPropertiesString());

		return mergedData;
	}

	public EbitgTreeStatisticsData computeMeansDataMulitThread() {
		List<Pair<Integer>> minMaxSentencePairNumList = this.createMinMaxSentencePairNumsList();
		List<Future<EbitgTreeStatisticsMultiThread>> registeredTasks = new ArrayList<Future<EbitgTreeStatisticsMultiThread>>();
		int taskNum = 1;

		ExecutorService executorService = Executors.newFixedThreadPool(this.parameters.getNumThreads());

		for (Pair<Integer> minMaxSentencePairNum : minMaxSentencePairNumList) {
			// Name of the temporary file that will store the properties of the alignment triples processed by the thread
			String threadHATPropertiesFileName = rootOutputFolderPath + hatPropertiesFileName + "Part" + taskNum + ".txt";

			InputStringsEnumeration<AlignmentStringTriple> inputStringEnumeration = AlignmentTriplesEnumeration.createAlignmentTriplesEnumeration(parameters.getSourceFileName(),
					parameters.targetFileName, parameters.alignmentsFileName);
			EbitgTreeStatisticsMultiThread ets = new EbitgTreeStatisticsMultiThread(parameters.getMaxAllowedInferencesPerNode(), maxSourceLength, minMaxSentencePairNum, inputStringEnumeration,
					threadHATPropertiesFileName, parameters.getMaxSentenceLength(), parameters.getOutputLevel(), this.atomicPartsMustAllShareSameAlignmentChunk);
			Future<EbitgTreeStatisticsMultiThread> executingTask = executorService.submit(ets);
			registeredTasks.add(executingTask);
			taskNum++;
		}

		// Wait until all threads finish
		List<EbitgTreeStatisticsMultiThread> results = MultiThreadCorpusDataGenerator.joinThreads(registeredTasks);

		System.out.println("First round of computing statistics with multiple threads completed");
		System.out.println("Merging data...");
		EbitgTreeStatisticsData result = createMergedMeansData(results, atomicPartsMustAllShareSameAlignmentChunk);
		System.out.println("Merging data completed...");
		executorService.shutdown();
		return result;
	}

	public void computeMergedSTDScores(List<EbitgTreeStatisticsMultiThread> registeredThreads, EbitgTreeStatisticsData mergedData) {
		// Merge the standard deviation data collected by the different threads
		for (EbitgTreeStatisticsMultiThread thread : registeredThreads) {
			mergedData.binarizationScores.addSTDs(thread.statisticsData.binarizationScores);
			mergedData.nodeExtractionScores.addSTDs(thread.statisticsData.nodeExtractionScores);
		}

		mergedData.prepareForSTDComputation();
		// Compute the standard derivations of the binarization scores for the merged data
		mergedData.computeSTDBinarizationScores();
	}

	public void computeSTDDataMultiThread(EbitgTreeStatisticsData mergedData, boolean atomicPartsMustAllShareSameAlignmentChunk) {
		List<Pair<Integer>> minMaxSentencePairNumList = this.createMinMaxSentencePairNumsList();

		List<Future<EbitgTreeStatisticsMultiThread>> registeredTasks = new ArrayList<Future<EbitgTreeStatisticsMultiThread>>();
		ExecutorService executorService = Executors.newFixedThreadPool(this.parameters.getNumThreads());

		for (Pair<Integer> minMaxSentencePairNum : minMaxSentencePairNumList) {
			InputStringsEnumeration<AlignmentStringTriple> inputStringEnumeration = AlignmentTriplesEnumeration.createAlignmentTriplesEnumeration(parameters.getSourceFileName(),
					parameters.targetFileName, parameters.alignmentsFileName);
			EbitgTreeStatisticsMultiThread ets = new EbitgTreeStatisticsMultiThread(parameters.getMaxAllowedInferencesPerNode(), maxSourceLength, minMaxSentencePairNum, inputStringEnumeration, null,
					parameters.getMaxSentenceLength(), parameters.getOutputLevel(), mergedData.binarizationScores, mergedData.nodeExtractionScores, atomicPartsMustAllShareSameAlignmentChunk);
			Future<EbitgTreeStatisticsMultiThread> executingTask = executorService.submit(ets);
			registeredTasks.add(executingTask);
		}

		// Wait until all threads finish
		List<EbitgTreeStatisticsMultiThread> results = MultiThreadCorpusDataGenerator.joinThreads(registeredTasks);

		System.out.println("Second round of computing statistics with multiple threads completed");
		System.out.println("Merging data...");
		computeMergedSTDScores(results, mergedData);
		System.out.println("Merging data completed...");
		executorService.shutdown();
	}

	public EbitgTreeStatisticsData produceStatistics(boolean writeStatisticsToFiles) {
		System.out.println("Started produceTreeStatistics...");
		System.out.println("\n totalSentencePairs: " + parameters.totalSentencePairs);
		
		// Make new folder for storing the results if it does not exist yet
		FileUtil.createFolderIfNotExisting(rootOutputFolderPath);
		
		EbitgTreeStatisticsData mergedData = computeMeansDataMulitThread();

		// Compute the STDs using the merged data containing the means as input
		computeSTDDataMultiThread(mergedData, atomicPartsMustAllShareSameAlignmentChunk);

		if (writeStatisticsToFiles) {
			writeResultsToFiles(mergedData);
		}

		mergedData.getBinarizationScores().checkSizes();
		return mergedData;
	}

	public void writeResultsToFiles(EbitgTreeStatisticsData mergedData) {
		try {
			// All threads have finished updating the statistics, all sentence pairs
			// have been processed. Write the final results
			EbitgTreeStatisticsDataWriter resultsWriter = EbitgTreeStatisticsDataWriter.createEbitgTreeStatisticsDataWriter(mergedData);
			resultsWriter.writeResults(parameters.theConfig,rootOutputFolderPath);

			// Produce merged file with properties of all the alignment triples
			produceMergedHATPropertiesFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getHatProptertiesFilePath(){
	  return rootOutputFolderPath + hatPropertiesFileName;
	}
	
	public void produceMergedHATPropertiesFile() {
		FilePartMerger.produceMergedPartsFileHatProperties(getHatProptertiesFilePath(), parameters.getNumThreads());
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			int i = 0;
			for (String arg : args) {
				System.out.println("arg[" + i + "] " + arg);
				i++;
			}

			System.out.println("Wrong usage, usage: java -jar produceTreeStatistics configFileName numThreads");
			return;
		}

		String configFileName = args[0];
		int numThreads = Integer.parseInt(args[1]);
		MultiThreadTreeStatisticsGenerator treeStatisticsGenerator = MultiThreadTreeStatisticsGenerator.createMultiThreadTreeStatisticsGenerator(configFileName, numThreads);
		treeStatisticsGenerator.produceStatistics(true);
		System.out.println("Finished");
	}
}
