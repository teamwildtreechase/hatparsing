/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public class TimeObject 
{
	private int days;
	private int hours;
	private int minutes;
	private int seconds;
	private int millis;
	
	
	private TimeObject(int days, int hours,int minutes, int seconds, int millis)
	{
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.millis = millis;
	}
	
	public int getDays()
	{
		return days;
	}
	
	public int getHours()
	{
		return hours;
	}
	
	public int getMinutes()
	{
		return minutes;
	}
	
	public int getSeconds()
	{
		return seconds;
	}
	
	public int getMillis()
	{
		return millis;
	}
	
	private static long millisPerDay()
	{
		return 24 * millisPerHour();
	}
	
	private static long millisPerHour()
	{
		return 60 * millisPerMinute();
	}
	
	private static long millisPerMinute()
	{
		return 60 * millisPerSecond();
	}
	
	private static long millisPerSecond()
	{
		return 1000;
	}
	
	public static TimeObject createTimeObject(long timeInMillis)
	{
		System.out.println("timeInmillis: " + timeInMillis);
		int days = (int) (timeInMillis / millisPerDay());
		long millisRemaining = timeInMillis % millisPerDay();
		int hours = (int) (millisRemaining / millisPerHour());
		millisRemaining = millisRemaining % millisPerHour();
		int minutes = (int) (millisRemaining / millisPerMinute());
		millisRemaining = millisRemaining % millisPerMinute();
		int seconds = (int) (millisRemaining / millisPerSecond());
		millisRemaining = millisRemaining % millisPerSecond();
		int millis = (int) millisRemaining;
		
		return new TimeObject(days,hours,minutes,seconds, millis);
	}	
	
	public static String createTimeString(long timeInMillis)
	{
		return createTimeObject(timeInMillis).toString();
	}
	
	public String toString()
	{
		String result = "";
		if(days > 0)
		{	
			result += "" + days + " days ";
		}
		
		result += hours + " hours " + minutes + " minutes " + seconds + " seconds ";
		return result;
	}
	
	public static void main(String[] args)
	{
		int timeInMillis = 129123;
		System.out.println(createTimeObject(timeInMillis));
		
	}
	
}
