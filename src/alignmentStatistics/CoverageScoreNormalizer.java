/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class CoverageScoreNormalizer extends ScoreNormalizer
{

	protected CoverageScoreNormalizer(
			EbitgTreeStatisticsComputation treeStatisticsComputation) 
	{
		super(treeStatisticsComputation);
	}

	@Override
	public int computeScoreDenominator(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg) 
	{
		return 1;
	}

}
