/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public class IntegerDataProperty implements DataProperty
{
	private final String propertyName;
	private final int propertyValue;
	
	public IntegerDataProperty(String propertyName,int propertyValue)
	{
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}	


	@Override
	public String getPropertyName() 
	{
		return this.propertyName;
	}

	@Override
	public String getPropertyValue() {
		String result = "" + this.propertyValue;
		return result;
	}
	
	

}
	

