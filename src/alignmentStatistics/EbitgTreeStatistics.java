/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import util.ConfigFile;
import util.FileStatistics;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartBuilderProperties;
import static alignmentStatistics.EbitgTreeStatisticsData.createEbitgTreeStatisticsData;

public class EbitgTreeStatistics 
{

	
	EbitgTreeStatisticsData statisticsData;
	private final int maxAllowedInferencesPerNode;
	private final static boolean IgnoreNonReducableTranslationEquivalents = true;

	/**
	 * Default constructor
	 */
	
	public  EbitgTreeStatistics(int maxAllowedInferencesPerNode, EbitgTreeStatisticsData statisticsData)
	{
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.statisticsData = statisticsData;
	}
	
	public static EbitgTreeStatistics createEbitgTreeStatistics(int maxAllowedInferencesPerNode,int  maxMaxSourceLengthAtomicFragment) 
	{
		EbitgTreeStatisticsData statisticsData = createEbitgTreeStatisticsData(maxAllowedInferencesPerNode,maxMaxSourceLengthAtomicFragment,IgnoreNonReducableTranslationEquivalents);
		EbitgTreeStatistics statistics = new  EbitgTreeStatistics(maxAllowedInferencesPerNode,statisticsData);
		return statistics;
	}
	
	
	public void produceCorpuStatistics(String sourceLanguageFileName, String targetLanguageFileName, String alignmentsFileName, String outputFileName, String hatPropertiesFileName, int outputLevel, ConfigFile theConfig)
	{
		BufferedReader sourceLanguageReader, targetLanguageReader, alignmentsReader;
		BufferedWriter outputWriter;
		BufferedWriter hatPropertiesWriter;
		
		try 
		{
			int numSentencePairs = FileStatistics.countLines(alignmentsFileName);
		
			
			sourceLanguageReader = new BufferedReader(new FileReader(sourceLanguageFileName));
			targetLanguageReader = new BufferedReader(new FileReader(targetLanguageFileName));
			alignmentsReader = new BufferedReader(new FileReader(alignmentsFileName));
			outputWriter = new BufferedWriter(new FileWriter(outputFileName));
		
			hatPropertiesWriter = new BufferedWriter(new FileWriter(hatPropertiesFileName));
			
			
			String sourceSentence;
			String  targetSentence;
			String alignment;
			
			long timeStart = System.currentTimeMillis();
			
			  // Get a Runtime object
			
			int sentencePairNum = 1;
			// While we manage to read a next source and target sentence and an alignment
			while( ((sourceSentence = sourceLanguageReader.readLine()) != null) && ((targetSentence = targetLanguageReader.readLine()) != null) 
					&& ((alignment = alignmentsReader.readLine()) != null))
			{
			
				System.out.println("}}}Processing sentence pair " + sentencePairNum);
				
				if(outputLevel > 0)
				{	
					System.out.println("sourceSentence: " + sourceSentence);
					System.out.println("targetSentence: " + targetSentence);
					System.out.println("alignment: " + alignment);
				}
				
				EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder( sourceSentence, targetSentence,alignment,maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,EbitgChartBuilderProperties.USE_CACHING);
				boolean success = chartBuilder.findDerivationsAlignment();
				
				// update the statistics for all the statistics entries
				this.statisticsData.updateStatistics(chartBuilder,hatPropertiesWriter,sentencePairNum,  success);
				
			
				if((sentencePairNum % 1) == 0)
				{
					 printProgressTime(timeStart, sentencePairNum, numSentencePairs);
					
				}
				
				
				this.statisticsData.incrementNumSentencePairs();
				
				sentencePairNum++;
				
				System.out.println("Heapsize end iteration:");
				printHeapSize();
				
			}
			
			
			 EbitgTreeStatisticsDataWriter resultsWriter = EbitgTreeStatisticsDataWriter.createEbitgTreeStatisticsDataWriter(this.statisticsData); 	
			 resultsWriter.writeResults(theConfig,"");

			
			// close all the readers and writers
			outputWriter.close();
			sourceLanguageReader.close();
			targetLanguageReader.close();
			alignmentsReader.close();
			
			System.out.println("Heapsize at termination");
			printHeapSize();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void printHeapSize()
	{
		long reservedSize = Runtime.getRuntime().totalMemory();
		long freeSize = Runtime.getRuntime().freeMemory();
		long heapSize = reservedSize -freeSize;
		
		//System.out.println("Heap size:" + heapSize + "bytes");
		System.out.println("Heap size:" + (heapSize/1000000) + "mb");
	}
	

	
	public static void printProgressTime( long timeStart, int sentencePairNum, int numSentencePairs)
	{
		NumberFormat nf = NumberFormat.getInstance();
		double percentageCompleted = (sentencePairNum * 100.0) / numSentencePairs;
		long time = System.currentTimeMillis();
		long timePassed = time - timeStart;
		double fractionCompleted = ((double)sentencePairNum) / numSentencePairs;
		double fractionLeft = 1 - fractionCompleted;
		long timePerSentencePair = timePassed / sentencePairNum;
		
		long expectedTimeRemaining = (long) (((fractionLeft / fractionCompleted) * timePassed) / 1000);
		
		int hoursRemaining = (int) (expectedTimeRemaining / (60*60));
		expectedTimeRemaining = (expectedTimeRemaining %  (60*60));
		int minutesRemaining = (int) (expectedTimeRemaining  / 60);
		expectedTimeRemaining =  (expectedTimeRemaining %  (60));
			
		System.out.println("Average time per sentence pair: " + timePerSentencePair + "  miliseconds");
		System.out.println("Finished " + nf.format(percentageCompleted) + "%");
		System.out.println("Expected: " + hoursRemaining + "hours, " + minutesRemaining + "minutes and " + expectedTimeRemaining + " seconds remaining");
	}
	
	

	
	
	public static void main(String[] args)
	{
		
		System.out.println("Started produceTreeStatistics...");
		
		
		if(args.length != 1)
		{
			System.out.println("Wrong usage, uage: java -jar produceTreeStatistics configFileName");
			return;
		}
		
		try 
		{
	
	
			String sourceFileName, targetFileName, alignmentsFileName, outputFileName;
			ConfigFile theConfig;
		
			System.out.println("args[0]: " + args[0]);
			theConfig = new ConfigFile(args[0]);
			
		
			int maxAllowedInferencesPerNode = theConfig.getIntIfPresentOrReturnDefault("maxAllowedInferencesPerNode", 100000);
			
			EbitgTreeStatistics ets = createEbitgTreeStatistics(maxAllowedInferencesPerNode,300);
			
			// Input locations
			String corpusLocation =  theConfig.getValue("corpusLocation");
			System.out.println("corpusLocation: " + corpusLocation);
			sourceFileName  = corpusLocation  +  theConfig.getValue("sourceFileName");
			targetFileName  =  corpusLocation  + theConfig.getValue("targetFileName");
			alignmentsFileName = corpusLocation  + theConfig.getValue("alignmentsFileName");
			// Output locations
			 outputFileName =   theConfig.getValue("treeStatisticsFileName");
		   String hatPropertiesFileName = theConfig.getValue("hatPropertiesFileName");
			
			int outputLevel = theConfig.getIntIfPresentOrReturnDefault("outputLevel", 0);
			
			
			ets.produceCorpuStatistics(sourceFileName, targetFileName, alignmentsFileName, outputFileName, hatPropertiesFileName, outputLevel, theConfig);
			
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println("File not readable");
		}		
		
	}
	
}
