/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import util.Pair;

public class MultiThreadCorpusDataGenerator 
{

	public final MultiThreadComputationParameters parameters;
	
	protected  MultiThreadCorpusDataGenerator(MultiThreadComputationParameters parameters)
	{
		this.parameters = parameters;
	}
	
	public MultiThreadComputationParameters getParameters()
	{
		return this.parameters;
	}

	
	protected List<Pair<Integer>>  createMinMaxLineNumsList(int totalNumLines)
	{
		List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();
		
		Pair<Integer> sentencePairsPerThreadAndNewNumThreads = MultiThreadComputationParameters.computeSentencePairsPerThreadAndNewNumThreads(totalNumLines, parameters.numThreads); 
		int sentencePairsPerThread = sentencePairsPerThreadAndNewNumThreads.getFirst();
		int newNumThreads = sentencePairsPerThreadAndNewNumThreads.getSecond();
		
		
		for(int minSentencePairNum = 1; minSentencePairNum <= totalNumLines; minSentencePairNum +=  sentencePairsPerThread)
		 {	 
			 // compute the maximum sentence pair num, take the totalSentencePairs if the number left is 
			 // less than sentencePairsPerThread, which can be the case for the last thread
			 int maxSentencePairNum = Math.min(minSentencePairNum + sentencePairsPerThread - 1, totalNumLines);
			 
			 result.add(new Pair<Integer>(minSentencePairNum,maxSentencePairNum));
		 }
		
		//System.out.println("result.size(): " + result.size() + " parameters.numThreads" + parameters.getNumThreads());
		assert(result.size() == newNumThreads);
		return result;
	}
	
	
	protected List<Pair<Integer>> createMinMaxSentencePairNumsList()
	{
		return createMinMaxLineNumsList(parameters.totalSentencePairs);
	}
	
	public static <T> List<T> joinThreads(List<Future<T>> executingTasks)
	{
		
		System.out.println("joinThreads: noThreads to be joined " + executingTasks.size());
		
		List<T> results = new ArrayList<T>();
		 // Wait until all threads finish
		 for(Future<T> waitForCompletion : executingTasks)
		 {
			 try 
			 {
				 T aResult = waitForCompletion.get();
				 results.add(aResult);
				 //FIXME need custom executor that can generate custom wrappers of futures
				 //System.out.println("Thread " + thread.getId() + " finished");
			 } catch (InterruptedException e) 
			 {
				 System.out.println(e);
				 e.printStackTrace();
				 System.exit(1);
			 } catch (ExecutionException e) 
			 {
				System.out.println(e);
				e.printStackTrace();
				System.exit(1);
			}
		 }
		 return results;
		 
	}
	
}