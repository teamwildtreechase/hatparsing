/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultiFolderFilePathListCreater 
{
	public final static String CorpusPropertiesFileStart = "EbitgTreeStatistics"; 
	public final static String LanguagePairPatternString = "-\\p{Alpha}{2}-\\p{Alpha}{2}-";
	public final static String CumulativeRelativeFrequenciesAllHATsFileStart = "cumulativeRelativeFrequenciesAllHATs";
	
	private final String fileOfInterestNamePrefix;
	private final String languagePairPatternString;
	
	
	private MultiFolderFilePathListCreater(String fileOfInterestNamePrefix, String languagePairPatternString)
	{
		this.fileOfInterestNamePrefix = fileOfInterestNamePrefix;
		this.languagePairPatternString = languagePairPatternString;
	}
	
	public static MultiFolderFilePathListCreater createCorpusPropertiesPathListCreater()
	{
		return new MultiFolderFilePathListCreater(CorpusPropertiesFileStart, LanguagePairPatternString);
	}
	
	 public static MultiFolderFilePathListCreater createCorpusPropertiesPathListCreater(String corpusPropertiesFileStartPrefix)
	  {
	    return new MultiFolderFilePathListCreater(corpusPropertiesFileStartPrefix, LanguagePairPatternString);
	  }
	
	public static MultiFolderFilePathListCreater createCumulativeAbsoluteFrequenciesAllHATsPathListCreater()
	{
		return new MultiFolderFilePathListCreater(CumulativeRelativeFrequenciesAllHATsFileStart, LanguagePairPatternString);
	}
	
	private static List<String> listSubFolderNamesSortedAlphanumerically(String baseFolder)
	{
		List<String> result = new ArrayList<String>();
		File mainFolder = new File(baseFolder);
		
		if(!mainFolder.isDirectory())
		{
			throw new RuntimeException("The specified mainfolder is not a directory");
		}
		
		for(File subFolder : mainFolder.listFiles())
		{
			if(subFolder.isDirectory())
			{
				result.add(subFolder.getAbsolutePath());
			}
		}
		
		Collections.sort(result);
		
		return result;
	}
	
	public String getLanguagePairStringFromSubFolderPath(String subFolderPath)
	{
		String result;
		
		Pattern pattern = Pattern.compile(languagePairPatternString);
		Matcher matcher = pattern.matcher(subFolderPath);
		
		if(matcher.find())
		{
			result = matcher.group();
			// Remove the "-" at the start and end
			result = result.substring(1, result.length() - 1);
		}
		else
		{
			throw new RuntimeException("getLanguagePairStringFromSubFolderPath : Error : Found no matching ");
		}
		
		if(matcher.find())
		{
			throw new RuntimeException("getLanguagePairStringFromSubFolderPath: Error : Found more than one matching ");
		}
		
		return result;
	}
	
	public List<String> getLanguagePairStingsFromBaseFolderPath(String baseFolder)
	{
		List<String> result = new ArrayList<String>();
		
		for(String subFolderPath : listSubFolderNamesSortedAlphanumerically(baseFolder))
		{
			result.add(getLanguagePairStringFromSubFolderPath(subFolderPath));
		}
		return result;
	}

	
	private String getPropertyFilePathFromSubFolderPath(String subFolderPath)
	{
		File subFolder = new File(subFolderPath);
		
		if(!subFolder.isDirectory())
		{
			throw new RuntimeException(" getPropertyFileNameFromSubFolderName: folder is not a directory");
		}
		
		for(File file : subFolder.listFiles())
		{
			if(file.isFile())
			{
				if(file.getName().contains(fileOfInterestNamePrefix))
				{
					return file.getAbsolutePath();
				}
			}
		}
		throw new RuntimeException("getPropertyFileNameFromSubFolderName Error : unable to find properties file starting with" + fileOfInterestNamePrefix + " in directory");
	}
	
	public List<String> getPropertiesFilePathsFromBaseFolderPath(String baseFolder)
	{
		List<String> result = new ArrayList<String>();
		
		for(String subFolderPath : listSubFolderNamesSortedAlphanumerically(baseFolder))
		{
			result.add(getPropertyFilePathFromSubFolderPath(subFolderPath));
			
		}
		return result;
	}
	
}
