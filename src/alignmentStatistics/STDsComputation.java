/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

public class STDsComputation implements MatrixComputationPerformer, ScoreDerivativeComputation
{
	final List<List<Double>> meanValues;
	
	
	public STDsComputation( List<List<Double>> meanValues)
	{
		this.meanValues = meanValues;
	}
	
	
	public void computeRowComputation(List<Double> row, int numParsedSentences)
	{
		computeSTDBinarizationScoresRow(row, numParsedSentences);
	}
	
	public void computeSTDBinarizationScoresRow(List<Double> stdsRow,int numParsedSentences)
	{
		for(int i = 0; i < stdsRow.size(); i++ )
		{	
			double oldValue = stdsRow.get(i);
			// Be ware that we compute the sample standard deviation, which is normalized by n-1 rather than n
			double newValue = Math.sqrt((oldValue / (numParsedSentences-1)));
			
			//System.out.println("STD: oldValue: " + oldValue + " newValue: " + newValue);
			stdsRow .set(i, newValue);
		}
	}	
	
	private double computeSquaredDifference(double val1, double val2)
	{
		return (val1 - val2) * (val1 - val2);
	}
	
	
	public double computeDerivedScore(double score, int rowIndex, int columnIndex)
	{
		double meanValue =  meanValues.get(rowIndex).get(columnIndex); 
		//System.out.println("computeDerivedScore: meanValue" + meanValue);
		double result = computeSquaredDifference(score, meanValue);
		return result;
	}
	
}


