/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;
import java.util.*;

public class SetMethods<Element>
{


		public  Set<Element> union (Set<Element> x, Set<Element> y)
		{
			Set<Element>  t = new HashSet<Element> (x);
			t.addAll(y);
			return t;
		}

		public  Set<Element>  intersection (Set<Element>  x, Set<Element>  y)
		{
			Set<Element>  t = new HashSet<Element> (x);
			t.retainAll(y);
			return t;
		}
                //the elements in x but not in y: "set-theoretic difference"
		public Set<Element>  minus (Set<Element>  x, Set<Element>  y)
		{
			Set<Element>  t = new HashSet<Element> ();
			for (Element o:x)
			if (!y.contains(o)) t.add(o);
			return t;
	}
		
	public static Set <Character> stringToSet(String s)
	{
		Set <Character> s1= new HashSet<Character>();
		for (int i=0;i<s.length();i++)
		{
		s1.add(s.charAt(i));
		}
		return s1;

	}

	public  Boolean hasNonEmptyIntersection( Set<Element> set1, Set<Element> set2)
	{
		Set<Element> set1Copy = new HashSet<Element> (set1);
		
		return (set1Copy.removeAll(set2));
	}

	public  Boolean hasEmptyIntersection(Set<Element> set1, Set<Element> set2)
	{
		Set<Element> set1Copy = new HashSet<Element> (set1);
		
		return !(set1Copy.removeAll(set2));
	}
	
	
	public static void main(String [] args)
	{
		Set <Character> a1 = stringToSet("hello");
		Set <Character> a2 = stringToSet("computer");

		SetMethods<Character> sm1 = new  SetMethods<Character>();;
		
		System.out.println("The union is"+ sm1.union(a1,a2));
		System.out.println("The intersection is"+sm1.intersection(a1,a2));
		System.out.println("The minus is"+ sm1.minus(a1, a2));
	}
}



