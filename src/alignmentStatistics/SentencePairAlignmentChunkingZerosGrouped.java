/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import util.InputReading;

import alignment.Alignment;

public class SentencePairAlignmentChunkingZerosGrouped extends SentencePairAlignmentChunking
{
	protected SentencePairAlignmentChunkingZerosGrouped(
			Alignment alignment,
			int sourceLength, int targetLength) 
	{
		super(alignment, sourceLength,targetLength);
	}

	public static SentencePairAlignmentChunkingZerosGrouped createSentencePairAlignmentChunkingZerosGrouped(String alignmentString, String sourceString, String targetString) {
		
		List<String> sourceWordArray = InputReading.constructSentenceWordsArrayList(sourceString);
		List<String> targetWordArray = InputReading.constructSentenceWordsArrayList(targetString);
		return createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceWordArray.size(), targetWordArray.size());
	}

	public static SentencePairAlignmentChunkingZerosGrouped createSentencePairAlignmentChunkingZerosGrouped(String alignmentString, int sourceLength, int targetLength) {
		
		Alignment alignment = Alignment.createAlignment(alignmentString);
		SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking = new SentencePairAlignmentChunkingZerosGrouped(alignment, sourceLength,targetLength);
		sentencePairAlignmentChunking.doTheWork();
		return sentencePairAlignmentChunking;
	}
	
	private void addSourceZeroAlignments()
	{
		Set<Integer> nonZeroSourcePositions = getNonZeroSourcePositions();
		
		
		// initialize the properties of having null alignments to false, change when null alignments 
		// are found
		initializeHasNullAlignmentPropertiesToFalse();
	
		Set<Integer> zerosBlock = new HashSet<Integer>();
		
		// Add Chunks for un-aligned source positions
		for(int i = 0; i < getSourceLength(); i++)
		{
			// source position has no alignment to any target position
			if(! nonZeroSourcePositions.contains(i))
			{
				zerosBlock.add(i);
				this.hasSourceNullAlignments = true;
			}
			else if(!zerosBlock.isEmpty())
			{
				addSourceZerosChunk(zerosBlock);
				zerosBlock = new HashSet<Integer>();
			}
		}
		
		if(!zerosBlock.isEmpty())
		{
			addSourceZerosChunk(zerosBlock);
		}
	
	}
	
	private void addTargetZeroAlignments()
	{
		Set<Integer> nonZeroTargetPositions = getNonZeroTargetPositions();	
		Set<Integer> zerosBlock = new HashSet<Integer>();
		// Add Chunks for un-aligned target positions
		for(int i = 0; i < getTargetLength(); i++)
		{
			// target position has no alignment to any source position
			if(! nonZeroTargetPositions.contains(i))
			{
				zerosBlock.add(i);
				this.hasTargetNullAlignments = true;
			}
			else if(!zerosBlock.isEmpty())
			{
				addTargetZerosChunk(zerosBlock);
				zerosBlock = new HashSet<Integer>();
			}
		}
		
		if(!zerosBlock.isEmpty())
		{
			addTargetZerosChunk(zerosBlock);
		}

	}
	
	public void addZeroAlignments()
	{
		addSourceZeroAlignments();
		addTargetZeroAlignments();
	}


	private void addSourceZerosChunk(Set<Integer> zerosBlock)
	{
		AlignmentChunk sourceZeroChunk = AlignmentChunk.createSourceNullsGroupAlignmentChunk(zerosBlock);
		alignmentChunks.add(sourceZeroChunk);	
	}

	private void addTargetZerosChunk(Set<Integer> zerosBlock)
	{
		AlignmentChunk targetZeroChunk = AlignmentChunk.createTargetNullsGroupAlignmentChunk(zerosBlock);
		alignmentChunks.add(targetZeroChunk);
	}
}