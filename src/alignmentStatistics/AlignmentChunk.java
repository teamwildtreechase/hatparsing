/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import util.Sorting;

import bitg.IntegerSetMethods;

import alignment.AlignmentPair;



public class AlignmentChunk 
{
	private Set<AlignmentPair> alignmentPairs;
	private Set<Integer>  sourcePositions;
	private Set<Integer> targetPositions;
	
	public static final SetMethods<Integer> integerSetMethods = new SetMethods<Integer>();
	public static final SetMethods<AlignmentPair> apSetMethods = new SetMethods<AlignmentPair>();
	
	public static enum AlignmentType{OneToOne,OneToMany,ManyToOne,OneToZero,ZeroToOne,ManyToManyFullConnected,ManyToManyPartialConnected};
	
	public AlignmentChunk()
	{
		alignmentPairs = new HashSet<AlignmentPair>();
		this.sourcePositions = new HashSet<Integer>();
		this.targetPositions = new HashSet<Integer>();
	}
	
	public AlignmentChunk(AlignmentChunk toCopy)
	{
		// Carefull that this only makes a shallow copy, so no deep copy of the elements
		// themselves is made, but for our purposes this is ok
		this.alignmentPairs = new HashSet<AlignmentPair>(toCopy.alignmentPairs);
		this.sourcePositions = new HashSet<Integer>(toCopy.sourcePositions);
		this.targetPositions = new HashSet<Integer>(toCopy.targetPositions);
		
	}
	
	/**
	 * Constructor that generates an AlignmentChunk containing just one AlignmentPair given as input
	 * @param p : the AlignmentPair to generate the chunk for. 
	 * Use an alignmentpair with -1 for source or target position, to indicate a zero alignment in the source 
	 * or target respectively 
	 * 
	 */
	public AlignmentChunk(AlignmentPair p)
	{
		this();
	
		int sourcePos = p.getSourcePos();
		int targetPos = p.getTargetPos();
		
		if(sourcePos >= 0)
		{	
			this.sourcePositions.add(sourcePos);
		}
		if(targetPos >= 0)
		{	
			this.targetPositions.add(targetPos);
		}
		if((sourcePos >= 0)  && (targetPos >= 0))
		{	
			this.alignmentPairs.add(p);
		}	
	}
	
	
	private AlignmentChunk(Set<Integer>  sourcePositions,  Set<Integer> targetPositions)
	{
		this();
		this.sourcePositions.addAll(sourcePositions);
		this.targetPositions.addAll(targetPositions);
	}
	
	public static AlignmentChunk createSourceNullsGroupAlignmentChunk(Set<Integer>  sourcePositions)
	{
		return new AlignmentChunk(sourcePositions, new HashSet<Integer>());
	}
	
	public static AlignmentChunk createTargetNullsGroupAlignmentChunk(Set<Integer>  targetPositions)
	{
		return new AlignmentChunk(new HashSet<Integer>(),targetPositions);
	}
	
	public Set<Integer> getTargetPositions()
	{
		return targetPositions;
	}
	
	
	public Set<Integer> getSourcePositions()
	{
		return sourcePositions;
	}
	
	/** In case we want to actually know where a specific source position 
	 * that belongs to an alignment chunk maps to, then this method finds it out.
	 * The motivation is that, while we can always work with chunks as one unit, if 
	 * we do so we do lose information, and it is desirable to introduce
	 * as little extra ambiguity as possible.
	 * @param sourcePos
	 * @return The set of target positions to which the source position maps
	 */
	public HashSet<Integer> getSourcePosMappingPositions(int sourcePos)
	{
		HashSet<Integer> mappedTargetPositions = new HashSet<Integer>();
		
		for(AlignmentPair pair : this.alignmentPairs)
		{
			if(pair.getSourcePos() == sourcePos)
			{
				mappedTargetPositions.add(pair.getTargetPos());
			}
		}
		
		return mappedTargetPositions;
	}
	
	public Set<AlignmentPair> getAlignmentPairs()
	{
		return alignmentPairs;
	}
	
	public boolean canBeMergedWith(AlignmentChunk chunk2)
	{
		boolean emptySourceIntersection = integerSetMethods.intersection(this.getSourcePositions(), chunk2.getSourcePositions()).isEmpty();
		boolean emptyTargetIntersection = integerSetMethods.intersection(this.getTargetPositions(), chunk2.getTargetPositions()).isEmpty();
		
		return (! (emptySourceIntersection && emptyTargetIntersection));
		
	}
	
	
	// Merge method that adapts the AlignmentChunk it is called from by merging the 
	// items belonging to the argument with it
	public void mergeWithChunk( AlignmentChunk chunk2)
	{
		assert(this.canBeMergedWith(chunk2));
		
		// Now add the elements of the thee Hashsets of the second chunk in the appropriate
		// Hashset of the (copy of) the first chunk
		this.getSourcePositions().addAll(chunk2.getSourcePositions());
		this.getTargetPositions().addAll(chunk2.getTargetPositions());
		this.getAlignmentPairs().addAll(chunk2.getAlignmentPairs());		
	}
	
	
	// Static merge method that leaves both originals intact
	public static AlignmentChunk mergeChunks(AlignmentChunk chunk1, AlignmentChunk chunk2)
	{
		// first copy chunk1
		AlignmentChunk mergeResult = new AlignmentChunk(chunk1);
		
		// then merge it with the second chunk
		mergeResult.mergeWithChunk(chunk2);

		return mergeResult;
	}
	
	public boolean isUnalignedChunk()
	{
		return (this.isUnalignedSourceChunk() || this.isUnalignedTargetChunk());
				
	}
	
	public boolean isUnalignedSourceChunk()
	{
		return (this.getNumTargetPositions() == 0);
	}
	
	public boolean isUnalignedTargetChunk()
	{
		return (this.getNumSourcePositions() == 0);
	}
	
	/**
	 * This method returns true when all source positions are connected to 
	 * all target positions in the chunk
	 * @return
	 */
	public boolean isCompleteChunk()
	{
		return (this.alignmentPairs.size() == (this.getNumSourcePositions() * this.getNumTargetPositions()));
	}
	
	
	public boolean isContinuousChunk()
	{	
		return (IntegerSetMethods.formsConsecutiveRange(targetPositions) && IntegerSetMethods.formsConsecutiveRange(sourcePositions));
	}
	
	public boolean hasContinuousSide()
	{
		return (IntegerSetMethods.formsConsecutiveRange(targetPositions) || IntegerSetMethods.formsConsecutiveRange(sourcePositions));
	}
	
	
	public int getMinSourcePos()
	{
		if(this.sourcePositions.isEmpty())
		{
			return -1;
		}
		return Collections.min(this.sourcePositions);
	}
	
	public int getMaxSourcePos()
	{
		if(this.sourcePositions.isEmpty())
		{
			return -1;
		}
		return Collections.max(this.sourcePositions);
	}

	public int getMinTargetPos()
	{
		if(this.targetPositions.isEmpty())
		{
			return -1;
		}
		return Collections.min(this.targetPositions);
	}
	
	public int getMaxTargetPos()
	{
		if(this.targetPositions.isEmpty())
		{
			return -1;
		}
		return Collections.max(this.targetPositions);
	}
	
	
	public List<Integer> getSourcePositionsAsSortedListAscending()
	{
		return Sorting.getSetAsSortedListAscending(this.sourcePositions);
	}
	
	public List<Integer> getSourcePositionsAsSortedListDescending()
	{
		return Sorting.getSetAsSortedListDescending(this.sourcePositions);
	}
	
	public List<Integer> getTargetPositionsAsSortedListAscending()
	{
		return Sorting.getSetAsSortedListAscending(this.targetPositions);
	}
	
	public List<Integer> getTargetPositionsAsSortedListDescending()
	{
		return Sorting.getSetAsSortedListDescending(this.targetPositions);
	}
	
	public int getNumSourcePositions()
	{
		return this.getSourcePositions().size();
	}
	
	public int getNumTargetPositions()
	{
		return this.getTargetPositions().size();
	}
	
	
	public AlignmentType determineAlignmentType()
	{
		
		// Simple one to one alignments
		if((getNumSourcePositions() == 1) && (getNumTargetPositions() == 1))
		{	
			return AlignmentType.OneToOne;
		}
		
		// Zero Alignments
		else if(getNumSourcePositions() == 0)
		{
			assert(getNumTargetPositions() > 0);
			
		
			return AlignmentType.ZeroToOne;		
		}
		else if(getNumTargetPositions() == 0)
		{
			assert(getNumSourcePositions() > 0);
			return AlignmentType.OneToZero;		
		}
		
		// one-to-many
		else if((getNumSourcePositions() == 1) && (getNumTargetPositions() > 1))
		{
			return AlignmentType.OneToMany;		
		}
		
		// many-to-one
		else if((getNumSourcePositions() > 1) && (getNumTargetPositions() == 1))
		{
			return AlignmentType.ManyToOne;		
		}
		
		else
		{
			assert((getNumSourcePositions() > 1) && (getNumTargetPositions() > 1));
			
			int numAlignmentPairs = this.alignmentPairs.size();
			
			// An efficient way to check if everything is connected is to check whether in the Set of AlignmentPairs, which 
			// by definition contains no duplicates, there are as much pairs as the number of source positions times the number
			// of target positions that are involved in the alignment of this chunk
			if(numAlignmentPairs == (getNumSourcePositions() * getNumTargetPositions()))
			{
				return AlignmentType.ManyToManyFullConnected;	
			}
			else
			{
				return AlignmentType.ManyToManyPartialConnected;
			}
		}
	}
	
	
	public String toString()
	{
		String result = "<Chunk> : ";
		
		result += " SourcePositions: {";
		
		for(int sourcePos : this.getSourcePositions())
		{
			result += sourcePos + ",";
		}
		result += "}";
	
		
		result += " TargetPositions: {";
		
		for(int targetPos : this.getTargetPositions())
		{
			result += targetPos + ",";
		}
		result += "}";
		
		
		result += " Alignment Pairs: ";
		for(AlignmentPair ap :this.getAlignmentPairs())
		{
			result += "(" + ap.getSourcePos() + "," + ap.getTargetPos() + ") ";
		}
		
		result += " </Chunk>";
		
		return result;
		
	}
	
}
