/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.Set;

import alignment.Alignment;
import alignment.AlignmentPair;

public class SentencePairAlignmentChunkingZerosSeparate extends SentencePairAlignmentChunking
{

	protected SentencePairAlignmentChunkingZerosSeparate(
			Alignment alignment,
			int sourceLength, int targetLength) 
	{
		super(alignment, sourceLength,targetLength);
	}

	public void addZeroAlignments()
	{
		Set<Integer> nonZeroSourcePositions = getNonZeroSourcePositions();
		Set<Integer> nonZeroTargetPositions = getNonZeroTargetPositions();
		
		// initialize the properties of having null alignments to false, change when null alignments 
		// are found
		initializeHasNullAlignmentPropertiesToFalse();

		// Generate the sets of non-zero source and target positions
		for(AlignmentPair p : sentenceAlignments.getAlignmentPairs())
		{
			nonZeroSourcePositions.add(p.getSourcePos());
			nonZeroTargetPositions.add(p.getTargetPos());
		}
		
		// Add Chunks for un-aligned source positions
		for(int i = 0; i < getSourceLength(); i++)
		{
			// source position has no alignment to any target position
			if(! nonZeroSourcePositions.contains(i))
			{
				AlignmentPair nullSourcePair = new AlignmentPair(i,-1);
				AlignmentChunk sourceZeroChunk = new AlignmentChunk(nullSourcePair);
				alignmentChunks.add(sourceZeroChunk);
				
				this.hasSourceNullAlignments = true;
			}
		}
		
		// Add Chunks for un-aligned target positions
		for(int i = 0; i < getTargetLength(); i++)
		{
			// target position has no alignment to any source position
			if(! nonZeroTargetPositions.contains(i))
			{
				AlignmentPair nullTargetPair = new AlignmentPair(-1,i);
				AlignmentChunk targetZeroChunk = new AlignmentChunk(nullTargetPair);
				alignmentChunks.add(targetZeroChunk);
				
				this.hasTargetNullAlignments = true;
			}
		}
	}
	
}
