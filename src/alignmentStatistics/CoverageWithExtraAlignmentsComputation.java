/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;

import util.DoubleMatrixCreater;

import junit.framework.Assert;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class CoverageWithExtraAlignmentsComputation extends CorpusScoreComputation 
{
	
	protected CoverageWithExtraAlignmentsComputation(
			List<List<Double>> basicScores, ScoreNormalizer scoreNormalizer,
			ScoreDerivativeComputation scoreDerivativeComputation,
			EbitgTreeStatisticsComputation treeStatisticsComputation) {
		super(basicScores, scoreNormalizer, scoreDerivativeComputation,
				treeStatisticsComputation);
	}
	
	protected CoverageWithExtraAlignmentsComputation(CoverageWithExtraAlignmentsComputation toCopy)
	{
		super(toCopy);
	}
	
	public static CoverageWithExtraAlignmentsComputation  createCoverageWithExtraAlignmentsComputation(int maxMaxNumberExtraAlignments,EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> scores = doubleMatrixCreator.createNumberMatrix(1,maxMaxNumberExtraAlignments + 1);
		return new CoverageWithExtraAlignmentsComputation(scores,new CoverageScoreNormalizer(treeStatisticsComputation),new BasicNormalization(), treeStatisticsComputation);
	}
	
	/**
	 * To compute our scores we actually need to know the maximum occurring number of 
	 * extra alignments in the corpus. However, we do not know this beforehand. 
	 * Simply taking the maximum sentence length has proven to be insufficient in certain 
	 * cases. In the maximum everything could be aligned with everything. However, it is not 
	 * desirable to compute the scores for sentenceLengt * sentenceLength parameter values.
	 * We therefore make a pessimistic guess, larger than the sentencelengt, but smaller 
	 * than the square of it (For europarl the sentence length was sufficient, only for Hansards it was not)
	 * @param maxSourceLength
	 * @return An estimate of the maximum number of extra alignments occurring in the corpus
	 */
	public static int estimateMaximumNumberOfExtraAlignments(int maxSourceLength)
	{
		return maxSourceLength * 3;
	}
	
	@Override
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxNumberExtraAlignments) 
	{
		int result;
		if(treeHasNoMoreThanMaxNumberExtraAlignments(tg, maxNumberExtraAlignments))
		{
			result = 1;
		}
		else
		{	
			result = 0;
		}
		//System.out.println("CoverageWithExtraAlignmentsComputation.computeIntermediateScore : " + result + " maxNumberExtraAlignments " + maxNumberExtraAlignments);
		return result;
	}


	protected boolean treeHasNoMoreThanMaxNumberExtraAlignments(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,int maxNumberExtraAlignments)
	{
		if(this.treeStatisticsComputation.determineMaxNumberOfExtraAlignmentsInTree(tg) <= maxNumberExtraAlignments)
		{	
			return true;
		}
		
		else 
		{
			return false;
		}
	}

	@Override
	public double lengthOneScore() 
	{
		return 1;
	}


	public List<List<Double>>  computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		List<List<Double>> result = new ArrayList<List<Double>>();
		
		List<Double> scoreList = new ArrayList<Double>();
		for(int maxExtraAlignments = 0;maxExtraAlignments <= this.maxMaxExtraAlignments(); maxExtraAlignments++)
		{
					
			double score = computeScore(tg,1,maxExtraAlignments);
			assert(score >= 0);
			scoreList.add(score);
			
			if(maxExtraAlignments == this.maxMaxExtraAlignments())
			{
				if(!(score > 0))
				{
					System.out.println("Number of extra Alignments: " + this.treeStatisticsComputation.determineMaxNumberOfExtraAlignmentsInTree(tg));
					System.exit(0);
					throw new RuntimeException("CoverageWithExtraAlignmentsComputation.computeSentenceScores: something went wrong, score for maximam parameter value must always be greater than 0 but is not");
		
				}
			}
			
		}
		  // Add to Matrix containing all scores for this sentence 
		 result.add(scoreList);
		
		//System.out.println("result.size(): " + result.size() + "," + result.get(0).size());
		
		 Assert.assertEquals(basicScores.size(), result.size());
		 Assert.assertEquals(basicScores.get(0).size(), result.get(0).size());
		 
		return result;
	}
	
	public final int maxMaxExtraAlignments()
	{
		return this.basicScores.get(0).size() - 1;
	}
	
}
