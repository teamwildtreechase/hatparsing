/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;

import util.DoubleMatrixCreater;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class CorpusBinarizationScoreComputation extends CorpusScoreComputation implements ScoreComputer
{
	
	protected int summedPossibleInternalNodes;
	
	protected CorpusBinarizationScoreComputation(List<List<Double>> summedInternalNodes, ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		super(summedInternalNodes, scoreNormalizer,scoreDerivativeComputation, treeStatisticsComputation);
	}
	
	/**
	 * Copy constructor
	 * @param toCopy
	 */
	protected  CorpusBinarizationScoreComputation(CorpusBinarizationScoreComputation toCopy)
	{
		super(toCopy);
	}
	
	
	public static  CorpusBinarizationScoreComputation createCorpusBinarizationScoreComputation(int maxMaxBranchingFactor, int maxMaxSourceLengthAtomicFragment, ScoreNormalizer scoreNormalizer, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> summedInternalNodes = doubleMatrixCreator.createNumberMatrix(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment);
		return new CorpusBinarizationScoreComputation(summedInternalNodes,scoreNormalizer,new BasicNormalization(), treeStatisticsComputation);
	}
	
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		assert(maxBranchingFactor > 0);
		double result = computeNumberInternalNodes(tg, maxSourceLengthAtomicFragment, maxBranchingFactor);
		//System.out.println("NumInternalnodesCompuation. computeIntermediateScore( " + maxSourceLengthAtomicFragment + "," + maxBranchingFactor + "): " + result);
		return result;
	}
	
	private int computeNumberInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		if(this.acceptableMaximalBranching(tg, maxSourceLengthAtomicFragment, maxBranchingFactor))
		{	
			return this.getNumInternalNodes(tg,maxBranchingFactor);
		}
		
		// the sentence-pair alignment triple could not be parsed with this combination of 
		// maxBranchingFactor and maxSourceLengthAtomicFragment
		// It will be considered as being entirely flat, having no internal nodes and 
		// a binarization score of 0
		else 
		{
			return 0;
		}
	}
	
	/**
	 * The lengthOneScore for the relative binarization corpus score. This score is returned 
	 * when the alignment length is one (either the source length - source only
	 * or the minimum of source and target length for the symmetric EbitgTreeStatisticsComputation case.
	 */
	public double lengthOneScore()
	{
		return 1;
	}

	public List<List<Double>>  computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		List<List<Double>> result = new ArrayList<List<Double>>();
		
		for(int maxBranchingFactor = 1;maxBranchingFactor <= maxMaxBranchingFactor(); maxBranchingFactor++)
		{
			  // Add to Matrix containing all scores for this sentence 
			 result.add(computeSentenceScores(tg, maxBranchingFactor,this));
		}
		
		//System.out.println("result.size(): " + result.size() + "," + result.get(0).size());
		
		return result;
	}
	

	
}
