/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

public class FinalScoresComputation
{
	public static final BasicNormalization BasicNormalization = new BasicNormalization();
	public static final STDsComputation STDsComputation = new STDsComputation(null);

	
	public static void performComputation(MatrixComputationPerformer computationPerformer, List<List<Double>> matrix, int numParsedSentences)
	{
		for(int i = 0; i < matrix.size(); i++)
		{
			List<Double> row = matrix.get(i);
			computationPerformer.computeRowComputation(row, numParsedSentences);
		}	
	}

	
	public static void computeSTDBinarizationScores(List<List<Double>> matrix, int numParsedSentences)
	{
		performComputation(STDsComputation, matrix, numParsedSentences);
	}
	
	public static void computeNormalizedScores(List<List<Double>> matrix, int normalizer)
	{
		performComputation(BasicNormalization, matrix, normalizer);
	}
	
	
	public static void computeCorpusBinarizationScores(List<List<Double>> matrix, int normalizer)
	{
		performComputation(BasicNormalization, matrix, normalizer);
		scaleMatrix(matrix, 100);
	}
	
	
	public static void scaleMatrix(List<List<Double>> matrix, double factor)
	{
		for(int i = 0; i < matrix.size(); i++)
		{
			for(int j = 0; j < matrix.get(0).size(); j++)
			{
				double scaledValue = matrix.get(i).get(j) * factor;
				matrix.get(i).set(j, scaledValue);
			}
		}
	}

}
