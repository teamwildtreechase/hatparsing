/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import alignment.SourceString;

public abstract class InputStringsEnumeration<T extends SourceString> implements Enumeration<T>
{

	private final List<BufferedReader> inputReaders;
	private List<String> nextInputStrings;
	
	protected InputStringsEnumeration( List<BufferedReader> inputReaders)
	{
		this.inputReaders = inputReaders;
		getNextInputStrings();
	}
	
	protected void getNextInputStrings() 
	{
		List<String> result = new ArrayList<String>();
		
		for(BufferedReader inputReader : inputReaders)
		{
			assert(inputReader != null);
			String input = null;
			try 
			{
				input = inputReader.readLine();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			if(input == null)
			{
				this.nextInputStrings = null;
				closeInputReaders();
				return;
				
			}
			result.add(input);
		}
		
		this.nextInputStrings = result;
		
		if(inputStringsAreAcceptable(this.nextInputStrings))
		{	
			return;
		}
		else
		{
			// continue until acceptable input Strings are found or the end of file is reached
			getNextInputStrings();
		}
	}
	
	private void closeInputReaders() 
	{
		for(BufferedReader inputReader : this.inputReaders)
		{
			try 
			{
				inputReader.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	protected abstract T getNextT(List<String> nextInputStrings);
	
	protected abstract boolean inputStringsAreAcceptable(List<String> nextInputStrings);
	
	@Override
	public boolean hasMoreElements() 
	{
		return (nextInputStrings != null);
	}

	@Override
	public T nextElement() 
	{
		if(hasMoreElements())
		{
			T result = getNextT(nextInputStrings);
			getNextInputStrings();
			return result;
		}
		
		throw new RuntimeException("Error : Has no more elements!");
	}

}
