/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SentenceLengthStatisticsComputer {

	private final BufferedReader inputReader;
	private final List<Integer> sentenceLengths;

	private SentenceLengthStatisticsComputer(BufferedReader inputReader, List<Integer> sentenceLengths) {
		this.sentenceLengths = sentenceLengths;
		this.inputReader = inputReader;
	}

	public static SentenceLengthStatisticsComputer createSentenceLengthStatisticsComputer(String inputFileName) {
		try {
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
			return new SentenceLengthStatisticsComputer(inputReader, new ArrayList<Integer>());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void collectSentenceLengthsList() {
		String line = null;
		try {
			while ((line = inputReader.readLine()) != null) {
				String[] words = line.split(" ");
				sentenceLengths.add(words.length);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	
	}
	private double computeMeanSentenceLength() {
		double total = 0;
		for (Integer sentenceLength : this.sentenceLengths) {
			total += sentenceLength;
		}
		return total / this.sentenceLengths.size();
	}
	
	private double computeStandardDeviationSentenceLength() {
		double meanSentenceLength = computeMeanSentenceLength();
		double sumSquaredDifferences = 0;
		for (Integer sentenceLength : this.sentenceLengths) {
			double difference = sentenceLength - meanSentenceLength;
			double squarredDifference = difference * difference;
			sumSquaredDifferences += squarredDifference;
		}
		return Math.sqrt(sumSquaredDifferences * (1.0 / (this.sentenceLengths.size() -1)));
	}

	public void computeSentenceLengthStatistics() {
		collectSentenceLengthsList();
		//System.out.println("Sentence lengths list:" + this.sentenceLengths);
		
		double meanSentenceLength = computeMeanSentenceLength();
		double standardDeviationSentenceLength = computeStandardDeviationSentenceLength();
		System.out.println("Mean sentence length: " + meanSentenceLength);
		System.out.println("Standard deviation sentence length: " + standardDeviationSentenceLength);
	}
	
	public static void main (String[] args)
	{
		if(args.length != 1)
		{
			throw new RuntimeException("Error : usage sentenceLengthStatisticsComputer InputFilePath");
		}
		createSentenceLengthStatisticsComputer(args[0]).computeSentenceLengthStatistics();
	}

}
