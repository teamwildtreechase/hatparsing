/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

public interface MatrixComputationPerformer 
{
	public void computeRowComputation(List<Double> row, int normalizer);
	
}
