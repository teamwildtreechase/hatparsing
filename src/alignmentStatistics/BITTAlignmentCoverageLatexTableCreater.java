/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;
import util.DoubleMatrixCreater;

public class BITTAlignmentCoverageLatexTableCreater 
{
	final List<String> languagePairNames;
	final List<List<List<Double>>> basicScoresMatrices;
	final List<List<List<Double>>> strictScoresMatrices;
	final LanguageAbbreviationLookupTable languageAbbreviationLookupTable;
	final static String BasicScoreName = "AC Continuous TEs";
	final static String StrictScoreName = "AC Disontinuous TEs";
	
	
	private BITTAlignmentCoverageLatexTableCreater (List<String> languagePairNames, List<List<List<Double>>> basicScoresMatrices, List<List<List<Double>>> strictScoresMatrices, LanguageAbbreviationLookupTable languageAbbreviationLookupTable)
	{
		this.languagePairNames = languagePairNames;
		this.basicScoresMatrices = basicScoresMatrices;
		this.strictScoresMatrices = strictScoresMatrices;
		this.languageAbbreviationLookupTable = languageAbbreviationLookupTable;
	}
	
	public static BITTAlignmentCoverageLatexTableCreater  createBITTAlignmentCoverageLatexTableCreater(String basicScoresBaseFolderName, String strictScoresBaseFolderName, String languageAbbreviationTableName)
	{
		List<String> languagePairNames = createLanguagePairNames(basicScoresBaseFolderName); 
		List<List<List<Double>>>  basicScoresMatrices = createDoubleMatricesListFromBasicFolder(basicScoresBaseFolderName);
		List<List<List<Double>>>  strictScoresMatrices = createDoubleMatricesListFromBasicFolder(strictScoresBaseFolderName);
		LanguageAbbreviationLookupTable languageAbbreviationLookupTable = LanguageAbbreviationLookupTable.createLanguageAbbreviationLookupTable(languageAbbreviationTableName);
		return new BITTAlignmentCoverageLatexTableCreater(languagePairNames,basicScoresMatrices,strictScoresMatrices, languageAbbreviationLookupTable);
	}
	
	private static 	List<String> createLanguagePairNames(String baseFolderName)
	{
		MultiFolderFilePathListCreater multiFolderFilePathListCreater = MultiFolderFilePathListCreater.createCumulativeAbsoluteFrequenciesAllHATsPathListCreater();
		List<String> languagePairNames = multiFolderFilePathListCreater.getLanguagePairStingsFromBaseFolderPath(baseFolderName);
		return languagePairNames;
	}
	
	private static List<List<List<Double>>> createDoubleMatricesListFromBasicFolder(String baseFolderPath)
	{
		MultiFolderFilePathListCreater multiFolderFilePathListCreater = MultiFolderFilePathListCreater.createCumulativeAbsoluteFrequenciesAllHATsPathListCreater();
		List<String> fullFilePaths =  multiFolderFilePathListCreater.getPropertiesFilePathsFromBaseFolderPath(baseFolderPath);
		List<List<List<Double>>> result = new ArrayList<List<List<Double>>>();
		
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		
		for(String filePath : fullFilePaths)
		{
			List<List<Double>> doubleMatrix = doubleMatrixCreator.readNumberMatrix(filePath, true,true);
			assert(doubleMatrix.size() > 0);
			assert(doubleMatrix.get(0).size() > 0);
			result.add(doubleMatrix);
		}
		return result;
	}
	
	
	private String createTableHeaderString()
	{
		String result = "\\begin{center}";
		result += "\n \t \\begin{tabular}{ | l |";
		
		for(int i = 0; i < 3 ; i++)
		{
			result += " l |";
		}
		result += "}\n\\hline";
	
		result += "\nLanguage pair & " + BasicScoreName + " & " + StrictScoreName;
		result += " \\\\ \\hline";
		return result;
	}
	
	private String createTableFooterString()
	{
		String result = "\t \\hline \n \t \\end{tabular}\n\\end{center}";
		return result;
	}
	
	
	
	private String createPropertiesLine(int index)
	{
		String result = "";
		// We only want a limited number of language pairs per table
		String languageAbbreviationPair = languagePairNames.get(index);;
		result += this.languageAbbreviationLookupTable.getLanguagePairForAbbreviationPair(languageAbbreviationPair);
		result +=  " & ";
		result += getBITTAlignmentCoverage(this.basicScoresMatrices.get(index));
		result +=  " & ";
		result += getBITTAlignmentCoverage(this.strictScoresMatrices.get(index));
		
		result += "\\\\ \\hline";
		return result;
	}

	
	private double getBITTAlignmentCoverage(List<List<Double>> scoreMatrix)
	{
		double result;
		// The BITT score (ALL_BITs) is stored in the second column of the first row
		result = scoreMatrix.get(0).get(1);
		return result;
	}
	
	
	private void writePropertiesTable()
	{
		System.out.println(createTableHeaderString());
		for(int i = 0; i < this.basicScoresMatrices.size(); i++)
		{
			System.out.println("\t" + createPropertiesLine(i));
		}
		System.out.println(createTableFooterString());
	}
	
	
	public static void main(String[] args)
	{
		if(args.length != 3)
		{
			int i = 0;
			for(String arg : args)
			{
				System.out.println("arg[" + i + "]" + arg);
				i++;
			}
			
			System.out.println("Usage: java - jar bittAlignmentCoverageTableCreator  BasicScoresBaseFolder StrictScoresBaseFolder LanguageAbbreviationTableName");
			System.exit(0);
		}
		
		String basicScoresBaseFolder = args[0];
		String strictScoresBaseFolder = args[1];
		String languageAbbreviationTableName = args[2];
		BITTAlignmentCoverageLatexTableCreater bittAlignmentCoverageLatexTableCreater = BITTAlignmentCoverageLatexTableCreater.createBITTAlignmentCoverageLatexTableCreater(basicScoresBaseFolder,strictScoresBaseFolder, languageAbbreviationTableName);
		bittAlignmentCoverageLatexTableCreater.writePropertiesTable();
	}
	

}
