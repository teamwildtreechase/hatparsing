/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public class TestClassChild extends TestClassParent 
{

	protected void innerMethod()
	{
		System.out.println("TestClassChild Inner Method called");
	}
	
	
	public static void main(String[] args)
	{
		TestClassParent tcp = new TestClassParent();
		tcp.outerMethod();
		
		TestClassChild tcc = new TestClassChild();
		tcc.outerMethod();
		
		TestClassParent tcp2 = new TestClassChild();
		tcp2.outerMethod();
	}
	
}
