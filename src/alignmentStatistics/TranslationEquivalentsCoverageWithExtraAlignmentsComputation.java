/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

import util.DoubleMatrixCreater;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class TranslationEquivalentsCoverageWithExtraAlignmentsComputation extends CoverageWithExtraAlignmentsComputation
{

	protected TranslationEquivalentsCoverageWithExtraAlignmentsComputation(
			List<List<Double>> basicScores, ScoreNormalizer scoreNormalizer,
			ScoreDerivativeComputation scoreDerivativeComputation,
			EbitgTreeStatisticsComputation treeStatisticsComputation) {
		super(basicScores, scoreNormalizer, scoreDerivativeComputation,
				treeStatisticsComputation);
	}

	protected TranslationEquivalentsCoverageWithExtraAlignmentsComputation(
			CoverageWithExtraAlignmentsComputation toCopy) 
	{
		super(toCopy);
	}	
	
	public static TranslationEquivalentsCoverageWithExtraAlignmentsComputation createTranslationEquivalentsCoverageWithExtraAlignmentsComputation(int maxMaxNumberExtraAlignments,EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		TranslationEquivalentsCoverageNormalizer translationEquivalentsCoverageNormalizer = new TranslationEquivalentsCoverageNormalizer(treeStatisticsComputation);
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> scores = doubleMatrixCreator.createNumberMatrix(1,maxMaxNumberExtraAlignments + 1);
		return new TranslationEquivalentsCoverageWithExtraAlignmentsComputation(scores,translationEquivalentsCoverageNormalizer,new BasicNormalization(), treeStatisticsComputation);
	}
	
	private  double computeNumberExtractableTranslationEquivalentNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxExtraAlignments)
	{
		double result;
		if(treeHasNoMoreThanMaxNumberExtraAlignments(tg, maxExtraAlignments))
		{
			result =  this.treeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart(tg.getChart());
		}
		else
		{	
			result =  0;
		}
		//System.out.println("TranslationEquivalentsCoverageWithExtraAlignmentsComputation.computeIntermediateScore : " + result + " maxNumberExtraAlignments " + maxExtraAlignments);
		return result;
	}
	
	@Override
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxExtraAlignments) 
	{
		//System.out.println(" TranslationEquivalentsCoverageWithExtraAlignmentsComputation.computeIntermediateScore()... ");
		return this.computeNumberExtractableTranslationEquivalentNodes(tg, maxExtraAlignments);
	}
}
