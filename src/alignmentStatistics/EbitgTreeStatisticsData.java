/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeStateBasic;
import static extended_bitg.EbitgTreeState.TreeType;

public class EbitgTreeStatisticsData 
{
	
	public final int maxMaxBranchingFactor;
	protected int maxMaxSourceLengthAtomicFragment;
	int numSentencePairs;
	protected int numNullContainingSentences;
	protected int numNoNullsSentences;
	
	protected int numSkipped;
	
	
	public ArrayList<EbitgTreeStatisticsEntry> statisticsEntries; 
	protected BinarizationScores binarizationScores;
	protected BinarizationScores nodeExtractionScores;
	protected TransductionOperationCountComputation transductionOperationCountsComputation;
	
	private final CorpusSentenceLengthAndLinkingProperties corpusSentenceLengthAndLinkingProperties;
	
	//public double meanSourceLength, meanTargetLength, meanSourceTargetRatio, stdSourceLength, stdTargetLength, stdSourceTargetRatio; 
	
	
	EbitgTreeStatisticsComputation treeStatisticsComputation;
	private boolean computeSTDs, writeHATProperties;
	
	protected final int maxAllowedInferencesPerNode;
	
	
	
	public  EbitgTreeStatisticsData(int maxAllowedInferencesPerNode, int maxSourceLength,
			 EbitgTreeStatisticsComputation treeStatisticsComputation, 
			 ArrayList<EbitgTreeStatisticsEntry> statisticsEntries, 
			 BinarizationScores binarizationScores,
			 BinarizationScores nodeExtractionScores, 
			 TransductionOperationCountComputation transductionOperationCountsComputation, CorpusSentenceLengthAndLinkingProperties corpusSentenceLengthAndLinkingProperties)	{
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.maxMaxSourceLengthAtomicFragment =  maxSourceLength;
		this.maxMaxBranchingFactor  =  maxSourceLength;
			
		//sourceSentenceLengths = new ArrayList<Integer>();
		//targetSentenceLengths = new ArrayList<Integer>();
		//sourceTargetLengthRatios = new ArrayList<Double>();
		this.statisticsEntries = statisticsEntries;
		
		this.transductionOperationCountsComputation = transductionOperationCountsComputation;
		this.treeStatisticsComputation = treeStatisticsComputation;
		this.computeSTDs = false;
		this.binarizationScores = binarizationScores;
		this.nodeExtractionScores = nodeExtractionScores;
		this.corpusSentenceLengthAndLinkingProperties = corpusSentenceLengthAndLinkingProperties;
	}
	

	/**
	 * Factory method for EbitgTreeStatisticsData
	 * @param maxAllowedInferencesPerNode
	 * @param maxMaxSourceLengthAtomicFragment
	 * @return The generated EbitgTreeStatisticsData object
	 */
	public static EbitgTreeStatisticsData createEbitgTreeStatisticsData(int maxAllowedInferencesPerNode,int maxMaxSourceLengthAtomicFragment, boolean considerNonReducableTranslationEquivalents)
	{
		// Set the number format used by the class
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(4);
		nf.setMinimumFractionDigits(4);
		
		TransductionOperationCountComputation transductionOperationCountComputation = TransductionOperationCountComputation.createTransductionOperationCountComputation(); 
		
		EbitgTreeStatisticsComputation treeStatisticsComputation = new  SymmetricTreeStatisticsComputation(considerNonReducableTranslationEquivalents);
		//EbitgTreeStatisticsComputation treeStatisticsComputation = new  SymmetricTreeStatisticsComputation();
		
		ArrayList<EbitgTreeStatisticsEntry> statisticsEntries = new  ArrayList<EbitgTreeStatisticsEntry>();
		
		BinarizationScores binarizationScores = BinarizationScores.createBinarizationScores(maxMaxSourceLengthAtomicFragment,maxMaxSourceLengthAtomicFragment,treeStatisticsComputation);
		BinarizationScores nodeExtractionScores = BinarizationScores.createNodeExtractionScores(maxMaxSourceLengthAtomicFragment, maxMaxSourceLengthAtomicFragment, treeStatisticsComputation);
		CorpusSentenceLengthAndLinkingProperties corpusSentenceLengthAndLinkingProperties = CorpusSentenceLengthAndLinkingProperties.createCorpusSentenceLengthAndLinkingProperties();
		EbitgTreeStatisticsData  ebitgTreeStatisticsData = new EbitgTreeStatisticsData(maxAllowedInferencesPerNode,maxMaxSourceLengthAtomicFragment,treeStatisticsComputation, statisticsEntries, binarizationScores, nodeExtractionScores, transductionOperationCountComputation,corpusSentenceLengthAndLinkingProperties);
		ebitgTreeStatisticsData.addStatisticEntries();
		
		return  ebitgTreeStatisticsData;
	}

	
	public void addStatisticEntries()
	{
		 // add a EbitgTreeStatisticsEntry for every maxSourceLengthAtomicFragment in the range 
		 // 1 ...maxMaxSourceLengthAtomicFragment. This will facilitate computing statistics 
		 // for atomic fragments of different sizes in this range
		 for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <= maxMaxSourceLengthAtomicFragment; maxSourceLengthAtomicFragment++)
		 {
			 statisticsEntries.add(new EbitgTreeStatisticsEntry(maxSourceLengthAtomicFragment));
		 }
		 
	}
	
	
	public void setComputeSTDs(boolean value)
	{
		this.computeSTDs = value;
	}
	
	public void setWriteHATProperties(boolean writeHATProperties)
	{
		this.writeHATProperties = writeHATProperties;
	}
	
	/**
	 *  Method that adds the data of another EbitgTreeStatisticsData instance to the 
	 *  data of this EbitgTreeStatisticsData data instance
	 * @param data
	 */
	public void addTreeStatisticsData( EbitgTreeStatisticsData data)
	{
		this.numSentencePairs += data.numSentencePairs;
		this.numNullContainingSentences += data.numNullContainingSentences;
		this.numNoNullsSentences += data.numNoNullsSentences;
		
		this.numSkipped += data.numSkipped;
		
		 for(int i = 0; i < this.statisticsEntries.size(); i++)
		 {
			 EbitgTreeStatisticsEntry localEntry = this.statisticsEntries.get(i);
			 EbitgTreeStatisticsEntry addedEntry = data.statisticsEntries.get(i);
			 
			 // Add the statistics of added entry to the statistics of localEntry
			 localEntry.addTreeStatisticsEntryData(addedEntry);
			 
		 }
		 
		 this.binarizationScores.addBinarizationScores(data.binarizationScores);
		 this.nodeExtractionScores.addBinarizationScores(data.nodeExtractionScores);
		 
		 //this.binarizationScoresForParsedSentences.addAll(data.binarizationScoresForParsedSentences);
	
		 
		 this.corpusSentenceLengthAndLinkingProperties.addCorpusLengthProperties(data.corpusSentenceLengthAndLinkingProperties);
		 this.transductionOperationCountsComputation.addTransductionOperationCounts(data.transductionOperationCountsComputation);
		 
	}
	
	

	
	private void performParseSuccessIndependentStatisticsUpdates(EbitgChartBuilder<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> chartBuilder)
	{
		// We always update the sentence length and linking statistics, 
		// independent of the success of parsing
		updateSentenceLengthAndLinkingStatistics(chartBuilder);
		// We update the sentence type count also independent of whether parsing is a success
		this.incrementSentenceTypeCount(chartBuilder.hasNullAlignments());
	}
	
	private void performParseSuccesDependentStatisticsUpdates(EbitgChartBuilderBasic chartBuilder, HATProperties hatProperties, boolean success)
	{
		if(!success)
		{
			this.updateStatisticsWithFailure();
			hatProperties.setIsParsable(false);
		}
		
		else
		{	
		
			hatProperties.setIsParsable(true);
			
			// Build a tree for the first Inference in the chart
			EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
			
			// update the binarization scores with the scores for this chart
			this.binarizationScores.updateScores(tg, this.computeSTDs);
			this.nodeExtractionScores.updateScores(tg, this.computeSTDs);

			// update the counts of transduction operations
			this.transductionOperationCountsComputation.updateTransductionOperationCountsWithTree(tg);
			
			for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <= this.maxMaxSourceLengthAtomicFragment; maxSourceLengthAtomicFragment++)
			 {
				// Get the statistics entry
				EbitgTreeStatisticsEntry statisticsEntry = this.statisticsEntries.get(maxSourceLengthAtomicFragment - 1);
				// check we have indeed the right entry
				assert(maxSourceLengthAtomicFragment == statisticsEntry.getMaxSourceLengthAtomicFragment());
	
				// Compute the tree type
				TreeType treeType = this.treeStatisticsComputation.determineTreeType(tg,  maxSourceLengthAtomicFragment);
				//System.out.println("treeType: " + treeType);	
			
				statisticsEntry.updateStatistics(treeType, chartBuilder.hasNullAlignments());
				
				// Determine the maximal occurring branching in the tree
				int maxBranching =  this.treeStatisticsComputation.determineMaximalOccuringBranching(tg,  maxSourceLengthAtomicFragment);
				
				
				// increment the count of this maximal occurring branching
				statisticsEntry.increaseCountTreeWithMaxBranching(maxBranching);
				
				if(maxSourceLengthAtomicFragment == 1)
				{
					hatProperties.setTreeTypeAtomLengthOne(treeType);
					
					int[] nodeTypeCounts = this.treeStatisticsComputation.determineNodeTypeCounts(tg);
					hatProperties.setNodeTypeCounts(nodeTypeCounts);
				}
				else if(maxSourceLengthAtomicFragment == 5)
				{
					hatProperties.setTreeTypeAtomLengthFive(treeType);
				}
				else if(maxSourceLengthAtomicFragment == 10)
				{
					hatProperties.setTreeTypeAtomLengthTen(treeType);
				}
				else if(maxSourceLengthAtomicFragment == 20)
				{
					hatProperties.setTreeTypeAtomLengthTwenty(treeType);
				}
				
			 }
		}	
	}
	
	
	/**
	 * Method that updates the statistics with the chartBuilder (=> chart) found for 
	 * a certain <sourceSentence,targetAlignment,Alignment Triple>
	 * @param chartBuilder : the chartBuilder, containing (possibly) the chart found for the triple
	 * @param success : Whether a chart could be constructed
	 */
	public void updateStatistics(EbitgChartBuilderBasic chartBuilder,BufferedWriter threadHATPropertiesWriter, int alignmentTripleNumber, boolean success)
	{
		int sourceLength = chartBuilder.getAlignmentChunking().getSourceLength();
		int targetLength = chartBuilder.getAlignmentChunking().getTargetLength();
		
		// Object that collects the property of the (first) HAT
		HATProperties hatProperties = new HATProperties(alignmentTripleNumber, sourceLength, targetLength);
		
		// Check if the alignmentchunking used for building the chart contains null alignments
		hatProperties.setHasNullAlignment(chartBuilder.hasNullAlignments());
		
		this.performParseSuccessIndependentStatisticsUpdates(chartBuilder);
		
		this.performParseSuccesDependentStatisticsUpdates(chartBuilder, hatProperties, success);
		
		// Write the collected HAT properties to the properties file
		if(this.writeHATProperties)
		{
			try 
			{
				//System.out.println("WRITING HAT Properties Line");
				threadHATPropertiesWriter.write(hatProperties.toCSVString() + "\n");
			} catch (IOException e) 
			{
				
				e.printStackTrace();
			}
		}
	
	}
	
	private void updateSentenceLengthAndLinkingStatistics(EbitgChartBuilder<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> chartBuilder)
	{
		this.corpusSentenceLengthAndLinkingProperties.updateSentenceLengthAndLinkingStatistics(chartBuilder);
	}
	
	public void updateStatisticsWithFailure()
	{
		this.numSkipped++;
	}

	public void incrementNumSentencePairs()
	{
		this.numSentencePairs++;
	}
	
	
	public  void incrementSentenceTypeCount(boolean hasNullAlignments)
	{
		if(hasNullAlignments)
		{
			this.numNullContainingSentences++;
		}
		else
		{
			this.numNoNullsSentences++;
		}
	}
	
	/**
	 *  Method that takes two arrays and adds the values of the second Array to these of the first
	 * 
	 * @param array1
	 * @param array2
	 */
	public static void addArrays(List<List<Double>> array1, List<List<Double>> array2)
	{
		for(int i = 0; i < array1.size(); i++)
		{
			addLists(array1.get(i), array2.get(i));
		}
	}
	
	
	public static void addLists(List<Double> list1, List<Double> list2)
	{
		for(int j = 0; j < list1.size(); j++)
		{
			double val1 = list1.get(j);
			double val2 = list2.get(j);
			double sum = val1 + val2;
			list1.set(j, sum);
		}
	}
	

	public int getNumParsedSentences()
	{
		return this.numSentencePairs - this.numSkipped;
	}
	
	public void prepareForSTDComputation()
	{
		this.binarizationScores.prepareForSTDComputation();
		this.nodeExtractionScores.prepareForSTDComputation();
	}
	
	
	public void computeMeanBinarizationScores()
	{
		this.binarizationScores.computeMeanBinarizationScores(this.getNumParsedSentences());
		this.nodeExtractionScores.computeMeanBinarizationScores(this.getNumParsedSentences());
	}
	
	
	public void computeSTDBinarizationScores()
	{
		this.binarizationScores.computeSTDBinarizationScores(this.getNumParsedSentences());
		this.nodeExtractionScores.computeSTDBinarizationScores(this.getNumParsedSentences());
	}
	
	public void computeCorpusBinarizationScores()
	{
		this.binarizationScores.computeCorpusScores();
		this.nodeExtractionScores.computeCorpusScores();
	}
	
	public BinarizationScores getBinarizationScores()
	{
		return this.binarizationScores;
	}
	
	public BinarizationScores getNodeExtractionScores()
	{
		return this.nodeExtractionScores;
	}
	
	public int getObservedTokensCount(int branchingFactor)
	{
		return this.transductionOperationCountsComputation.getObservedTokensCount(branchingFactor);
	}
	
	public int getObservedTypesCount(int branchingFactor)
	{
		return this.transductionOperationCountsComputation.getObservedTypesCount(branchingFactor);
	}
	
	public int getMaxSourceLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMaxSourceLengt();
	}
	
	public int getMaxTargetLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMaxTargetLength();
	}
	
	public int getMinSourceLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMinSourceLength();
	}
	
	public int getMinTargetLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMinTargetLength();
	}
	
	public double getMeanSourceLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMeanSourceLength();
	}
	
	public double getMeanTargetLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMeanTargetLength();
	}
	
	public double getStdSourceLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getStdSourceLength();
	}
	
	public double getStdTargetLength()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getStdTargetLength();
	}
	
	public double getMeanSourceTargetRatio()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getMeanSourceTargetRatio();
	}
	
	public double getStdSourceTargetRatio()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getStdSourceTargetRatio();
	}
	
	protected String getLinkingPropertiesString()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getLinkingPropertiesString();
	}
	
	public String getSentenceLengthPropertiesString()
	{
		return this.corpusSentenceLengthAndLinkingProperties.getSentenceLengthPropertiesString();
	}
	
}
