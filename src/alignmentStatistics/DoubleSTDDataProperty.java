/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.text.NumberFormat;

public class DoubleSTDDataProperty implements DataProperty
{
	private final String propertyName;
	private final double propertyValue;
	private final double standardDeviation;
	private static final String LatexPlusMinSign = "$\\pm$";
	private final NumberFormat nf;
	
	public DoubleSTDDataProperty(String propertyName,double propertyValue, double standardDeviation)
	{
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.standardDeviation = standardDeviation;
		this.nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
	}	

	

	@Override
	public String getPropertyName() 
	{
		return this.propertyName;
	}

	@Override
	public String getPropertyValue() {
		String result = nf.format(this.propertyValue) + LatexPlusMinSign + nf.format(this.standardDeviation);
		return result;
	}
	


}
	