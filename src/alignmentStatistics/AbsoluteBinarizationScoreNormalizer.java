/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class AbsoluteBinarizationScoreNormalizer extends ScoreNormalizer
{
	
	public AbsoluteBinarizationScoreNormalizer
	(
			EbitgTreeStatisticsComputation treeStatisticsComputation) {
		super(treeStatisticsComputation);
	}
	
	public int computeScoreDenominator(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		//System.out.println("<<<<<<<BinarizationScoreComputation.computeScoreDenominator() called");
		//return tg.getSourceLength() - 1;
		return treeStatisticsComputation.absoluteBinarizationScoreNormalizer(tg);
	}
	

	
}
