/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;
import java.util.Hashtable;

import extended_bitg.EbitgTreeState.TreeType;

public class HATProperties 
{
	
	
	private int alignmentTripleNumber; // The (line) number of the alignment triple 
	
	
	private Hashtable<String,Object> properties;
	
	/*
	private int sourceLength; // The length of the source sentence
	private int targetLength; // The length of the target sentence
	private boolean isParsable; // Whether the alignment triple could be parsed to produce the HATs
	private boolean hasNullAlignments; //Whether null aligned words are present
	private int numAtomicNodes;
	private int numSourceNullBindingNodes;
	private int numBITTNodes;// The number of BITT nodes appearing in the first HAT
	private int numPETNodes;  // The number of PET nodes appearing in the first HAT
	private int numHATNodes;  // The number of HAT nodes appearing in the first HAT  
	private TreeType treeTypeAtomLengthOne; // The "normal" tree type
	private TreeType treeTypeAtomLengthFive;
	private TreeType treeTypeAtomLengthTen;
	private TreeType treeTypeAtomLengthTwenty;
	*/
	
	/*
	 *  Constructor that sets the the most basic properties of the HAT
	 */
	public HATProperties(int alignmentTripleNumber, int sourceLength, int targetLength)
	{
		this.alignmentTripleNumber = alignmentTripleNumber;
		
		 properties = new Hashtable<String, Object>();
		
		 this.setValue("sourceLength", new Integer(sourceLength));
		 this.setValue("targetLength", new Integer(targetLength));
		 
	}
	
	
	public HATProperties(String propertiesString) throws Exception
	{
		try 
		{
			 properties = new Hashtable<String, Object>();
				
			
			//System.out.println("propertiesString: " + propertiesString);
			
			String[] parts = propertiesString.split(",");
			
			if(parts.length != 14)
			{
				throw new Exception("Exception: HATProperties from string could not determine all 14 fields, possibly misformed data line");
			}
			
			this.alignmentTripleNumber = Integer.parseInt(parts[0].trim());
			this.setValue("sourceLength", new Integer(Integer.parseInt(parts[1].trim())));
			this.setValue("targetLength", new Integer(Integer.parseInt(parts[2].trim())));
			this.setValue("isParsable", new Boolean(Boolean.parseBoolean(parts[3].trim())));
			this.setValue("hasNullAlignments", new Boolean(Boolean.parseBoolean(parts[4].trim())));
			this.setValue("numAtomicNodes", new Integer(Integer.parseInt(parts[5].trim())));
			this.setValue("numSourceNullBindingNodes", new Integer(Integer.parseInt(parts[6].trim())));
			this.setValue("numBITTNodes", new Integer(Integer.parseInt(parts[7].trim())));
			this.setValue("numPETNodes", new Integer(Integer.parseInt(parts[8].trim())));
			this.setValue("numHATNodes", new Integer(Integer.parseInt(parts[9].trim())));
			this.setValue("treeTypeAtomLengthOne", parseTreeType(parts[10].trim()));
			this.setValue("treeTypeAtomLengthFive", parseTreeType(parts[11].trim()));
			this.setValue("treeTypeAtomLengthTen", parseTreeType(parts[12].trim()));
			this.setValue("treeTypeAtomLengthTwenty", parseTreeType(parts[13].trim()));
			
		}
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		
	}
	
	public int getAlignmentTripleNumber()
	{
		return this.alignmentTripleNumber;
	}
	
	
	private void setValue(String name, Object value)
	{
		properties.put(name, value);
	}
	
	public Object getValue(String name)
	{
		return properties.get(name);
	}
	
	public static TreeType parseTreeType(String treeType) throws Exception
	{
		
		if(treeType.equals("BITT"))
		{
			return TreeType.BITT;
		}
		else if(treeType.equals("PET"))
		{
			return TreeType.PET;
		}
		else if(treeType.equals("HAT"))
		{
			return TreeType.HAT;
		}
		else 
		{
			throw new Exception("HATProperties.parseTreeType : Error: Unknown tree type!");
			
		}
	}

	public static String treeTypeString(TreeType treeType) 
	{
		
		if(treeType != null)
		{	
		
			if(treeType.equals(TreeType.BITT))
			{
				return "binarizable";
			}
			else if(treeType.equals(TreeType.PET))
			{
				return "bijection";
			}
			else if(treeType.equals(TreeType.HAT))
			{
				return "many";
			}
			else 
			{
				return "null";
				
			}
		}
		else 
		{
			return "null";
			
		}
		
	}
	
	
	public void setIsParsable(boolean isParsable)
	{
		this.setValue("isParsable", new Boolean(isParsable));
	}
	
	public void setHasNullAlignment(boolean hasNullAlignments)
	{
		this.setValue("hasNullAlignments", new Boolean(hasNullAlignments));
	}
	
	public void setNumAtomicNodes(int numAtomicNodes)
	{
		this.setValue("numAtomicNodes", new Integer(numAtomicNodes));
	}
	
	public void setNumSourceNullBindingNodes(int numSourceNullBindingNodes)
	{
		this.setValue("numSourceNullBindingNodes", new Integer(numSourceNullBindingNodes));
	}
	
	public void setNumBITTNodes(int numBITTNodes)
	{
		this.setValue("numBITTNodes", new Integer(numBITTNodes));
	}
	
	public void setNumPETNodes(int numPETNodes)
	{
		this.setValue("numPETNodes", new Integer(numPETNodes));
	}
	
	public void setNumHATNodes(int numHatNodes)
	{
		this.setValue("numHATNodes", new Integer(numHatNodes));
	}
	
	
	/**
	 * Sets the node type counts from an ordered array with the node counts for the five different types
	 * occuring in HATs
	 * @param nodeTypeCounts
	 */
	public void setNodeTypeCounts(int[] nodeTypeCounts)
	{
		this.setNumAtomicNodes(nodeTypeCounts[0]);
		this.setNumSourceNullBindingNodes(nodeTypeCounts[1]);
		this.setNumBITTNodes(nodeTypeCounts[2]);
		this.setNumPETNodes(nodeTypeCounts[3]);
		this.setNumHATNodes(nodeTypeCounts[4]);
	}
	
	public void setTreeTypeAtomLengthOne(TreeType treeType)
	{
		
		this.setValue("treeTypeAtomLengthOne", treeType);
	}
	
	public void setTreeTypeAtomLengthFive(TreeType treeType)
	{
		this.setValue("treeTypeAtomLengthFive", treeType);
	}
	
	public void setTreeTypeAtomLengthTen(TreeType treeType)
	{
		this.setValue("treeTypeAtomLengthTen", treeType);
	}
	
	public void setTreeTypeAtomLengthTwenty(TreeType treeType)
	{
		this.setValue("treeTypeAtomLengthTwenty", treeType);
	}
	
	
	
	public static String propertiesHeader()
	{
		String result = "";
		
		result += "alignmentTripleNumber" + " , ";
		result += "sourceLength" + " , ";
		result += "targetLength" + " , ";
		result += "isParsable" + " , ";
		result += "hasNullAlignments" + " , ";
		result += "numAtomicNodes" + " , ";
		result += "numSourceNullBindingNodes" + " , ";
		result += "numBITTNodes" + " , ";
		result += "numPETNodes" + " , ";
		result += "numHATNodes" + " , ";
		result += "treeTypeAtomLengthOne" + " , ";;
		result += "treeTypeAtomLengthFive" + " , ";
		result += "treeTypeAtomLengthTen" + " , ";
		result += "treeTypeAtomLengthTwenty";
		result += "\n";
		
		return result;
	}
	
	public String toCSVString()
	{
		String result = "";
		
		result += alignmentTripleNumber + " , ";
		result +=  properties.get("sourceLength") + " , ";
		result +=  properties.get("targetLength") + " , ";
		result +=  properties.get("isParsable") + " , ";
		result +=  properties.get("hasNullAlignments") + " , ";
		result +=  properties.get("numAtomicNodes") + " , ";
		result +=  properties.get("numSourceNullBindingNodes") + " , ";
		result +=  properties.get("numBITTNodes") + " , ";
		result +=  properties.get("numPETNodes") + " , ";
		result +=  properties.get("numHATNodes") + " , ";
		result +=  properties.get("treeTypeAtomLengthOne") + " , ";;
		result +=  properties.get("treeTypeAtomLengthFive") + " , ";
		result +=  properties.get("treeTypeAtomLengthTen") + " , ";
		result +=  properties.get("treeTypeAtomLengthTwenty");
	
		
		
		return result;
	}
	
}