/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;
import util.DoubleMatrixCreater;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public class BinarizationScoreComputation extends ScoreComputation implements ScoreComputer 
{

	protected BinarizationScoreComputation(List<List<Double>> meanBinarizationScores,  ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivationComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		super(meanBinarizationScores, scoreNormalizer, scoreDerivationComputation, treeStatisticsComputation);
		checkSize();
	}
	
	public static BinarizationScoreComputation createBinarizationScoreComputation(int maxMaxBranchingFactor, int maxMaxSourceLengthAtomicFragment, ScoreNormalizer scoreNormalizer, ScoreDerivativeComputation scoreDerivativeComputation, EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		assert(maxMaxBranchingFactor > 1);
		DoubleMatrixCreater doubleMatrixCreator = new DoubleMatrixCreater();
		List<List<Double>> scores = doubleMatrixCreator.createNumberMatrix(maxMaxBranchingFactor, maxMaxSourceLengthAtomicFragment);
		BinarizationScoreComputation result = new BinarizationScoreComputation(scores,  scoreNormalizer, scoreDerivativeComputation, treeStatisticsComputation);
		return result;
	}
	
	
	public BinarizationScoreComputation (BinarizationScoreComputation toCopy)
	{
		super(toCopy);
		checkSize();
	}
	
	public void checkSize()
	{
		//System.out.println("@@@@@@@@@@@BinarizationSCoreComputation .. Checking size");
		if(this instanceof BITTBinarizationScoreComputation)
		{	
			//System.out.println(this.basicScores.size() + "= 1" + (this.basicScores.size() == 1));
			assert(this.basicScores.size() == 1);
			if(!(this.basicScores.size() == 1))
			{
				throw new RuntimeException("BinarizationScoreComputation.checkSizes : unexpected size");
			}
		}
		else
		{
			//System.out.println(this.basicScores.size() + "> 1" + (this.basicScores.size() > 1));
			assert(this.basicScores.size() > 1);
			if(!(this.basicScores.size() > 1))
			{
				throw new RuntimeException("BinarizationScoreComputation.checkSizes : unexpected size");
			}
		}
	}
	
	
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		return computeBinarizationScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor);
	}
	
	
	protected static double binarizationScore(int numInternalNodes, int normalizer)
	{
		
		//System.out.println("BinarizationScoreComputation.binarizationscore >> numInternalNodes:" + numInternalNodes);
		
		// The normalizer may become 0, in case of the RelativeBinarizationScoreNormalizer
		// when no internal nodes are possible (i.e. for the alignment 1-0 ( with source position 0 un-aligned))
		// In this case we should not divide by zero but rather just return the maximum score of 100
		if(normalizer == 0)
		{	
			return 100;
		}

		return ((double)numInternalNodes / normalizer) * 100;
	}
	
	
	public void createSTDsComputation(BinarizationScoreComputation meanComputation)
	{
		this.scoreDerivativeComputation = new STDsComputation(meanComputation.basicScores);
	}
	
	
	
	public double computeBinarizationScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		
		double result = 0;
		//System.out.println("ScoreComputation.acceptableMaximalBranching(tg," + maxSourceLengthAtomicFragment +"," + maxBranchingFactor + "))");
		if(this.acceptableMaximalBranching(tg,maxSourceLengthAtomicFragment,maxBranchingFactor))
		{	
			result =  binarizationScore(this.getNumInternalNodes(tg,maxBranchingFactor),scoreNormalizer.computeScoreDenominator(tg) );
		}
		
		// the sentence-pair alignment triple could not be parsed with this combination of 
		// maxBranchingFactor and maxSourceLengthAtomicFragment
		// It will be considered as being entirely flat, having no internal nodes and 
		// a binarization score of 0
		else 
		{
			result = 0;
		}
		
		//System.out.println("QQQQ : BinarizationScore :" + result);
		
		return result;
	}
	
	public double lengthOneScore()
	{
		//System.out.println("BinarizationScoreComputation.lengthOneScore() called");
		return 100;
	}

	@Override
	public double computeScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxBranchingFactor) 
	{
		assert(maxBranchingFactor > 0);
		return (Double)computeFinalScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor, this);
	}
	
	
	public List<Double> computeSentenceBinarizationScoresList(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> tg, int maxBranchingFactor)
	{
		return computeSentenceScores(tg,  maxBranchingFactor,this);
	}
	
	
	public List<List<Double>>  computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> tg)
	{
		List<List<Double>> result = new ArrayList<List<Double>>();
		
		for(int maxBranchingFactor = 1;maxBranchingFactor <= this.maxMaxBranchingFactor(); maxBranchingFactor++)
		{

			  // Add to Matrix containing all scores for this sentence 
			 result.add(computeSentenceScores(tg,  maxBranchingFactor, this));
		}
		
		//System.out.println("result.size(): " + result.size() + "," + result.get(0).size());
		
		return result;
	}
	
	public void updateScores(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> tg)
	{
		EbitgTreeStatisticsData.addArrays(this.basicScores, computeSentenceScores(tg));
		
	}
	
	public void computeMeanBinarizationScores(int numParsedSentences)
	{
		if(this.scoreDerivativeComputation instanceof BasicNormalization)
		{
			FinalScoresComputation.computeNormalizedScores(this.basicScores, numParsedSentences);
		}
		else
		{
			throw new RuntimeException();
		}
		
	}
	
	public void computeSTDBinarizationScores(int numParsedSentences)
	{
		if(this.scoreDerivativeComputation instanceof STDsComputation)
		{	
			FinalScoresComputation.computeSTDBinarizationScores(this.basicScores, numParsedSentences);
		}
		else
		{
			System.out.println("this.scoreDerivativeComputation" + this.scoreDerivativeComputation);
			throw new RuntimeException();
		}
	}
	
	
	public List<Double> getMeanBinarizationsScoresRow(int index)
	{
		return this.basicScores.get(index);
	}
	

	@Override
	public List<Double> getDataRowToRender(int index) 
	{
		return this.basicScores.get(index);	
	}
	
}
