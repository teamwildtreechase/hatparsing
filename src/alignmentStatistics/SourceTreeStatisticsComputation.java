/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgChart;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.InferenceLengthProperties;

public class SourceTreeStatisticsComputation extends EbitgTreeStatisticsComputation
{

	public SourceTreeStatisticsComputation(boolean atomicPartsMustAllShareSameAlignmentChunk)
	{
		super(atomicPartsMustAllShareSameAlignmentChunk);
	}
	
	/**
	 * This method returns the normalizer for the absolute binarization 
	 * score. In this case the source length minus one. Note that this 
	 * normalizer is problematic in the sense that in fact both the source 
	 * and target length play a role in what level of binarization score
	 * is achievable, which was the motivation for the Symmetric variant 
	 * of TreeStatisticsComputation.
	 */
	public int absoluteBinarizationScoreNormalizer(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg)
	{
		if(tg.getSourceLength() > 1)
		{	
			return tg.getSourceLength() -1;
		}

		// If the length is one, we consider the binarization 
		// score to be always (trivially) maximal and 
		// normalize by one. For the corpus scores we count one node 
		// for length one alignments, even though strictly these have 
		// no internal nodes
		return 1;	
	}
	


	/**
	 *  @param state : The state for which the number of children is computed
	 *  @return :the asymmetric null ignoring branching factor for the state, looking only at the not-null children on the source side
	 */
	@Override
	public int nullIgnoringBranchingFactorState(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> state)
	{
		int result = state.getNumNotNullSourceChildren();
		//System.out.println("SourceTreeStatisticsComputation.numberOfChildrenState : " + result);
		return result;
	}


	@Override
	public boolean subTreePairMayBeTreatedAtomic(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> state,
			int maxSourceLengthAtomicFragment, EbitgChart chart)
	{
		InferenceLengthProperties inferenceLengthProperties = state.getInferenceLengthProperties();
		return (inferenceLengthProperties.getSourceSpanLengthAlignedWordsOnly(chart) <= maxSourceLengthAtomicFragment);
	}

	@Override
	public int alignmentLength(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg) {
		return tg.getSourceLength();
	}
	
	
}
