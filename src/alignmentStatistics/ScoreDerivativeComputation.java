/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public interface ScoreDerivativeComputation 
{
	public double computeDerivedScore(double score, int rowIndex, int columnIndex);
}
