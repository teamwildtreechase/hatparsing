/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public interface ScoreComputer 
{
	public double computeIntermediateScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor);
	public double computeScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg, int maxSourceLengthAtomicFragment, int maxBranchingFactor);
	public double lengthOneScore();
	
}
