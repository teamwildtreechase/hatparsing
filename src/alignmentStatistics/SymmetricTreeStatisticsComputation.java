/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgChart;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.InferenceLengthProperties;

public class SymmetricTreeStatisticsComputation extends EbitgTreeStatisticsComputation
{
	public SymmetricTreeStatisticsComputation(boolean atomicPartsMustAllShareSameAlignmentChunk)
	{
		super(atomicPartsMustAllShareSameAlignmentChunk);
	}
	
	
	
	public boolean subTreePairMayBeTreatedAtomic(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> state, int maxSourceLengthAtomicFragment, EbitgChart chart)
	{
		//final int sourceSpanLength = state.getChosenInference().getSourceSpanLength();
		//final int targetSpanLength = state.getChosenInference().getTargetSpanLength();
		InferenceLengthProperties inferenceLengthProperties = state.getInferenceLengthProperties();
		final int sourceSpanLength = inferenceLengthProperties.getSourceSpanLengthAlignedWordsOnly(chart);
		final int targetSpanLength = inferenceLengthProperties.getTargetSpanLengthAlignedWordsOnly();
		final int maxLength = Math.max(sourceSpanLength, targetSpanLength);
		
		assert(sourceSpanLength > 0);
		assert(targetSpanLength > 0);
		
		//System.out.println("sourceSpanLengt: " + sourceSpanLength + " targetSpanLength: " + targetSpanLength);
		
		return (maxLength <= maxSourceLengthAtomicFragment);
	}

	/**
	 *  @param state : The state for which the number of children is computed
	 *  @return :the symmetric null ignoring branching factor for the state, taking the maximum over the source and target side
	 */
	@Override
	public int nullIgnoringBranchingFactorState(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> state) 
	{
		int result = Math.max(state.getNumNotNullSourceChildren(), state.getNumNotNullTargetChildren());
		//System.out.println("SymmertricTreeStatisticsComputation.numberOfChildrenState : " + result);
		return result;

	}



	/**
	 * This method returns the absolute binarization score normalizer 
	 * for the alignment triple belonging with the treegrower tg. 
	 * This normalizer is the minimum of sourcelength and target length minus one.
	 * The intuition is that the best amount of binarization we can hope to achieve,
	 * is that when for the shortes of the source and target string, every word 
	 * is aligned in a binary way with the target. Since there are some extra words 
	 * on the other, longer side, these will be grouped to form minimal Translation Equivalents
	 * with other words. In this optimal case, the number of internal nodes will then be
	 * min(sourcelength, targetlength) - 1 , which corresponds to full binarization.
	 */
	@Override
	public int absoluteBinarizationScoreNormalizer(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> tg) 
	{
		int minLength = Math.min(tg.getSourceLength(), tg.getTargetLength());
		if(minLength > 1)
		{
			return minLength - 1;
		}
		// If the length is one, we consider the binarization 
		// score to be always (trivially) maximal and 
		// normalize by one. For the corpus scores we count one node 
		// for length one alignments, even though strictly these have 
		// no internal nodes
		return 1;
	}
	
	public int alignmentLength(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg) 
	{
		//System.out.println("SymmetricTreeStatisticsComputation.alignmentLength() => tg.getSourceLength()" + tg.getSourceLength() + " tg.getTargetLength() " + tg.getTargetLength());
		int result = Math.min(tg.getSourceLength(), tg.getTargetLength());	
		return result;
	}
}
