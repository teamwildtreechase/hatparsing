/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import extended_bitg.EbitgTransductionOperation;
import extended_bitg.NumberedObject;
import util.ConfigFile;
import util.FileUtil;
import alignment.CorpusTreeReordering;

/**
 * Class that writes the Tree Statistics to files
 * @author gemaille
 *
 */
public class EbitgTreeStatisticsDataWriter 
{
    private static String DEFAULT_COMBINED_RESULTS_FILE_NAME = "ResultsCombined.txt"; 
  
  
	private EbitgTreeStatisticsData ebitgTreeStatisticsData;
	private NumberFormat nf;
	
	private EbitgTreeStatisticsDataWriter(EbitgTreeStatisticsData ebitgTreeStatisticsData, NumberFormat nf)
	{
		this.ebitgTreeStatisticsData = ebitgTreeStatisticsData;
		this.nf = nf;
	}
	
	public static  EbitgTreeStatisticsDataWriter createEbitgTreeStatisticsDataWriter(EbitgTreeStatisticsData ebitgTreeStatisticsData)
	{
		return new EbitgTreeStatisticsDataWriter(ebitgTreeStatisticsData,NumberFormat.getInstance());
	}
	
	

	public void writeGeneralStatistics(BufferedWriter outputWriter) throws IOException
	{
		outputWriter.write("\n Total number of sentence pairs: " + ebitgTreeStatisticsData.numSentencePairs);
		outputWriter.write("\n number of skipped sentence pairs: "  + ebitgTreeStatisticsData.numSkipped); 
		outputWriter.write("\n number of sentence pairs containing nulls: "  + ebitgTreeStatisticsData.numNullContainingSentences);
		outputWriter.write("\n number of sentence pairs not containing nulls: "  + ebitgTreeStatisticsData.numNoNullsSentences);
		outputWriter.write("\n mean source length: " + nf.format(ebitgTreeStatisticsData.getMeanSourceLength()) + " standard deviation: " + nf.format(ebitgTreeStatisticsData.getStdSourceLength()));
		outputWriter.write("\n minimum source length: " +ebitgTreeStatisticsData.getMinSourceLength());
		outputWriter.write("\n maximum source length: " +ebitgTreeStatisticsData.getMaxSourceLength());
		outputWriter.write("\n mean target length: " + nf.format(ebitgTreeStatisticsData.getMeanTargetLength()) + " standard deviation: " + nf.format(ebitgTreeStatisticsData.getStdTargetLength()));
		outputWriter.write("\n minimum target length: " + ebitgTreeStatisticsData.getMinTargetLength());
		outputWriter.write("\n maximum target length: " + ebitgTreeStatisticsData.getMaxTargetLength());
		outputWriter.write("\n mean (source length : target length) ratio: " + nf.format(ebitgTreeStatisticsData.getMeanSourceTargetRatio()) + " standard deviation: " + nf.format(ebitgTreeStatisticsData.getStdSourceTargetRatio()));
		outputWriter.write("\n maximum branching factor observed (for maxSourceLengthAtomicFragment = 1) : " + ebitgTreeStatisticsData.statisticsEntries.get(0).getMaxMaxBranching());
		outputWriter.write(ebitgTreeStatisticsData.getLinkingPropertiesString());
		outputWriter.write("\n\n maxmimun allowed inferences per node : " +  ebitgTreeStatisticsData.maxAllowedInferencesPerNode + "  (used to prevent out of memory errors)");
	}
	
	
	/**s
	 * Write the labels for both Pure and Null hats statistics
	 * @param outputWriter
	 * @throws IOException
	 */
	public void writeNullsAndPuresLabels(BufferedWriter outputWriter) throws IOException
	{
		outputWriter.write("maxSourceLengthAtomicFragment,");
		outputWriter.write("Pure_BITTs,");
		outputWriter.write("Null_BITTs,");
		outputWriter.write("Pure_PETs,");
		outputWriter.write("Null_PETs,");
		outputWriter.write("Pure_HATs,");
		outputWriter.write("Null_HATs");		
	}
	
	public void writeBasicLabels(BufferedWriter outputWriter) throws IOException
	{
		outputWriter.write("maxSourceLengthAtomicFragment,");
		outputWriter.write("BITTs,");
		outputWriter.write("PETs,");
		outputWriter.write("HATs");
	}	
	
	

	public void writeCumulativeNullsAndPuresLabels(BufferedWriter outputWriter) throws IOException
	{
		
		outputWriter.write("maxSourceLengthAtomicFragment,");
		outputWriter.write("Pure_BITTs,");
		outputWriter.write("All_BITTs,");
		outputWriter.write("All_BITTs_AND_Pure_PETs,");
		outputWriter.write("All_BITTs_AND_All_PETs,");
		outputWriter.write("All_BITTs_AND_All_PETs_AND_Pure_HATs,");
		outputWriter.write("All_BITTs_AND_All_PETs_AND_All_HATs");
	}	
	
	
	public void writeCumulativeBasicLabels(BufferedWriter outputWriter) throws IOException
	{	
		outputWriter.write("maxSourceLengthAtomicFragment,");
		outputWriter.write("BITTs,");
		outputWriter.write("BITTs_AND_PETs,");
		outputWriter.write("BITTs_AND_PETs_AND_HATs");
	}
		
	
	// Wrapper functions writing statistics for with/without nulls and frequencies, and cumulative/non-cmulative

	public void writeParsedWithoutNullsRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeParsedNullsStatistics(outputWriter, false, true, false);
	}
	
	public void writeParsedWithoutNullsCumulativeRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{		
		writeParsedNullsStatistics(outputWriter, false, true, true);	
	}

	public void writeParsedWithNullsRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeParsedNullsStatistics(outputWriter,true, true, false);
	}
	
	public void writeParsedWithNullsCumulativeRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeParsedNullsStatistics(outputWriter, true, true, true);		
	}
	
	public void writeParsedNullsStatistics(BufferedWriter outputWriter,boolean withNulls,  boolean computeRelativeFreq, boolean cumulative) throws IOException
	{
		if(cumulative)
		{	
			writeCumulativeBasicLabels(outputWriter);
		}	
		else
		{	
			writeBasicLabels(outputWriter);
		}
			
		double normalizer;
		
		if(withNulls)
		{
			normalizer = ebitgTreeStatisticsData.numNullContainingSentences;
		}
		else
		{
			normalizer = ebitgTreeStatisticsData.numNoNullsSentences;
		}
		
		boolean isNonInteger = computeRelativeFreq;  // if we compute relative freqs these are displayed as non-integers
		
		int maxMaxSourceLength = Math.min(ebitgTreeStatisticsData.getMaxSourceLength(), ebitgTreeStatisticsData.maxMaxSourceLengthAtomicFragment);
		
		
		for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <= maxMaxSourceLength; maxSourceLengthAtomicFragment++)
		{	
			EbitgTreeStatisticsEntry statisticsEntry = ebitgTreeStatisticsData.statisticsEntries.get( maxSourceLengthAtomicFragment - 1);
			
			outputWriter.write("\n");		
			outputWriter.write(maxSourceLengthAtomicFragment + ",");	
			
			double value = 0;
		
			double newValue;
			
			if(withNulls)
			{	
				newValue = statisticsEntry.numNullBITTs;
			}
			else
			{
				newValue = statisticsEntry.numPureBITTs;
			}	
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			if(withNulls)
			{	
				newValue = statisticsEntry.numNullPETs;
			}
			else
			{
				newValue = statisticsEntry.numPurePETs;
			}	
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			
			if(withNulls)
			{	
				newValue = statisticsEntry.numNullHATs;
			}
			else
			{
				newValue = statisticsEntry.numPureHATs;
			}	
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, false);
			
		}
		
	}
	

	public double computeNewValue(double oldValue,double newValue, double normalizer, boolean computeRelativeFreq, boolean cumulative)
	{
		double addedValue;
		double returnValue;
		
		if(computeRelativeFreq)
		{
			addedValue = (newValue / normalizer) * 100;
		}
		else
		{
			addedValue = newValue;
		}
		
		if(cumulative)
		{
			returnValue = oldValue + addedValue;
		}
		else
		{
			returnValue = addedValue;
		}
		
		return returnValue;
	}
	

	public void writeAllAbsoluteFrequencies(BufferedWriter outputWriter) throws IOException
	{

		writeAllParsed(outputWriter,false, false);
	}
	
	public void writeAllCumulativeAbsoluteFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeAllParsed(outputWriter,false, true);
	}
	
	public void writeAllParsedRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeAllParsed(outputWriter,true, false);		
	}
	
	public void writeAllCumulativeParsedRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		writeAllParsed(outputWriter,true, true);
	}
	
	
	public void writeNumber(BufferedWriter outputWriter, double number, boolean isNonInteger, boolean writeComma) throws IOException
	{
		if(isNonInteger)
		{
			outputWriter.write("" + nf.format(number));
		}
		else
		{
			outputWriter.write("" + ((int)number));
		}
		
		if(writeComma)
		{
			outputWriter.write(",");
		}
	}
	
	public void writeAllParsed(BufferedWriter outputWriter,boolean computeRelativeFreq, boolean cumulative) throws IOException
	{
		if(cumulative)
		{	
			writeCumulativeNullsAndPuresLabels(outputWriter);
		}
		else
		{
			writeNullsAndPuresLabels(outputWriter);
		}
			
		int numParsedSentencePairs = ebitgTreeStatisticsData.numSentencePairs - ebitgTreeStatisticsData.numSkipped;
		
		int maxMaxSourceLength = Math.min(ebitgTreeStatisticsData.getMaxSourceLength(), ebitgTreeStatisticsData.maxMaxSourceLengthAtomicFragment);
		
		for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <=  maxMaxSourceLength; maxSourceLengthAtomicFragment++)
		{	
			EbitgTreeStatisticsEntry statisticsEntry = ebitgTreeStatisticsData.statisticsEntries.get( maxSourceLengthAtomicFragment - 1);
			
			outputWriter.write("\n");
		
			outputWriter.write(maxSourceLengthAtomicFragment + ",");	
			
			double value = 0;
			double normalizer = numParsedSentencePairs;
			boolean isNonInteger = computeRelativeFreq;  // if we compute relative freqs these are displayed as non-integers
			
			double newValue = statisticsEntry.numPureBITTs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			newValue = statisticsEntry.numNullBITTs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			newValue = statisticsEntry.numPurePETs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			newValue = statisticsEntry.numNullPETs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			newValue = statisticsEntry.numPureHATs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, true);
			
			newValue = statisticsEntry.numNullHATs;
			value = computeNewValue(value, newValue, normalizer, computeRelativeFreq, cumulative);
			writeNumber(outputWriter, value, isNonInteger, false);
			
			
		}
		
	}
	
	
	public void writeMaxSourceLengthLabels(BufferedWriter outputWriter) throws IOException
	{
		for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment < ebitgTreeStatisticsData.getMaxSourceLength(); maxSourceLengthAtomicFragment++)
		{
			outputWriter.write("atomFLenght=" + maxSourceLengthAtomicFragment + ",");
		}
		outputWriter.write("atomFLenght=" + ebitgTreeStatisticsData.getMaxSourceLength());
	}

	public void writeMaxSourceLengthLabelsFull(BufferedWriter outputWriter) throws IOException
	{
		for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment < ebitgTreeStatisticsData.maxMaxSourceLengthAtomicFragment; maxSourceLengthAtomicFragment++)
		{
			outputWriter.write("atomFLenght=" + maxSourceLengthAtomicFragment + ",");
		}
		outputWriter.write("atomFLenght=" + ebitgTreeStatisticsData.maxMaxSourceLengthAtomicFragment);
	}
	
	public int writeMaxBranchingLabelsHorizontal(BufferedWriter outputWriter, ScoreComputation scoreComputation) throws IOException
	{
		//int maxMaxBranchingFactor = Math.min(ebitgTreeStatisticsData.maxMaxBranchingFactor, ebitgTreeStatisticsData.maxSourceLength);
		int maxMaxBranchingFactor = scoreComputation.maxMaxBranchingFactor();
		
		int labelsWritten = 0;
		for(int maxBranchingFactor = 1; maxBranchingFactor < maxMaxBranchingFactor; maxBranchingFactor++)
		{
			outputWriter.write("maxBranchingFactor=" + maxBranchingFactor + ",");
			labelsWritten++;
		}
		outputWriter.write("maxBranchingFactor=" + maxMaxBranchingFactor);
		labelsWritten++;
		return labelsWritten;
	}

	public int writeMaxExtraAlignmentsHorizontal(BufferedWriter outputWriter, CoverageWithExtraAlignmentsComputation scoreComputation) throws IOException
	{
		int elementsWritten = 0;
		for(int maxExtraAlignments = 0; maxExtraAlignments < scoreComputation.maxMaxExtraAlignments(); maxExtraAlignments++)
		{
			outputWriter.write("maxExtraAlignments=" + maxExtraAlignments + ",");
			elementsWritten++;
		}
		outputWriter.write("maxExtraAlignments=" + scoreComputation.maxMaxExtraAlignments());
		elementsWritten++;
		return elementsWritten;
	}
	
	public void writeMaxBranchingLabels(BufferedWriter outputWriter) throws IOException
	{
		outputWriter.write("maxBranching,");
		writeMaxSourceLengthLabels(outputWriter);
	}

	public void writeMaxBranchingLabelsFull(BufferedWriter outputWriter) throws IOException
	{
		outputWriter.write("maxBranching,");
		writeMaxSourceLengthLabelsFull(outputWriter);
	}
	
	public void writeMaxBranchingCounts(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing max branching counts...");
		writeMaxBranchingStatistics(outputWriter, false, false);
	}
	
	public void writeMaxBranchingRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing max branching relative frequencies...");	
		writeMaxBranchingStatistics(outputWriter, true, false);
	}
	
	
	public void writeCumulativeMaxBranchingCounts(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing cumulative max branching counts...");
		writeMaxBranchingStatistics(outputWriter, false, true);	
	}
	
	public void writeCumulativeMaxBranchingRelativeFrequencies(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing max branching cumulative relative frequencies...");
		writeMaxBranchingStatistics(outputWriter, true, true);
	}

	
	/**
	 * A generic function that writes max branching statistics for:
	 * 	- absolute counts
	 *  - cumulative absolute counts
	 *  - relative frequencies
	 *  - cumulative relative 
	 * @param outputWriter The outputWriter to write the results to
	 * @param computeRelativeFreq  Whether or not to compute relative frequencies
	 * @param cumulative  Whether or not to compute cumulative
	 * @throws IOException
	 */
	public void writeMaxBranchingStatistics(BufferedWriter outputWriter,boolean computeRelativeFreq, boolean cumulative) throws IOException
	{
		
		EbitgTreeStatisticsEntry firstEntry = ebitgTreeStatisticsData.statisticsEntries.get(0);
		int numParsedSentencePairs = ebitgTreeStatisticsData.numSentencePairs - ebitgTreeStatisticsData.numSkipped;
		writeMaxBranchingLabels(outputWriter);
		
		double[] values = new double[ebitgTreeStatisticsData.getMaxSourceLength()];
		
		for(int maxBranching = 1; maxBranching <= firstEntry.getMaxMaxBranching(); maxBranching++)
		{
			outputWriter.write("\n" + maxBranching + ",");
			
			for(int maxSourceLengthAtomicFragment = 1; maxSourceLengthAtomicFragment <= ebitgTreeStatisticsData.getMaxSourceLength(); maxSourceLengthAtomicFragment++)
			{	
				// check first the entry exists for ebitgTreeStatisticsData maxSourceLengthAtomicFragment
				if( maxSourceLengthAtomicFragment <= ebitgTreeStatisticsData.statisticsEntries.size() )
				{
					EbitgTreeStatisticsEntry entry = ebitgTreeStatisticsData.statisticsEntries.get(maxSourceLengthAtomicFragment - 1);
					double oldValue = values[maxSourceLengthAtomicFragment-1];
					double newValue = entry.getCountTreeWithMaxBracnching(maxBranching); 
					
					values[maxSourceLengthAtomicFragment-1] = computeNewValue(oldValue,newValue, numParsedSentencePairs, computeRelativeFreq, cumulative);	
				}
				
				writeNumber(outputWriter, values[maxSourceLengthAtomicFragment-1], computeRelativeFreq, false);
				
				writeCommaExceptForLastRowItem(outputWriter,  maxSourceLengthAtomicFragment, ebitgTreeStatisticsData.getMaxSourceLength());
			}
		}	
	}
	
	
	
	public void writeCommaExceptForLastRowItem(BufferedWriter outputWriter, int item, int lastItem) throws IOException
	{
		if(item != lastItem)
		{
			outputWriter.write(",");
		}
	}
	
	
	
	private int writeGeneralDataRow(BufferedWriter outputWriter, List<Double> dataRow) throws IOException
	{
		int itemNum = 0;
		for(double score : dataRow)
		{	
			writeNumber(outputWriter,score , true, false);
			writeCommaExceptForLastRowItem(outputWriter,itemNum , dataRow.size() - 1);
			itemNum++;
		}
		return itemNum;
	}
	
	private int writeBinarizationStatisticsRow(BufferedWriter outputWriter, List<Double> dataRow) throws IOException
	{
		int maxSourceLengthAtomicFragment = 1;
		int dataPointsWritten = 0;
		for(double score : dataRow)
		{	
			//double score = dataRow.get(maxSourceLengthAtomicFragment - 1);
		
			writeNumber(outputWriter,score , true, false);
			writeCommaExceptForLastRowItem(outputWriter,  maxSourceLengthAtomicFragment, dataRow.size());
			maxSourceLengthAtomicFragment++;
			dataPointsWritten++;
		}
		return dataPointsWritten;
	}
	
	
	public void writeBinarizationStatistics(BufferedWriter outputWriter,ScoreComputation scoresComputation) throws IOException
	{
		System.out.println("Writing  Binarization Statistics...");
		
		writeMaxBranchingLabelsFull(outputWriter);
		
		//EbitgTreeStatisticsEntry firstEntry = ebitgTreeStatisticsData.statisticsEntries.get(0);
		//int maxMaxBranching = Math.min( firstEntry.getMaxMaxBranching(), ebitgTreeStatisticsData.maxMaxBranchingFactor);
		int maxMaxBranching  = scoresComputation.maxMaxBranchingFactor();
		
		
		for(int maxBranching = 1; maxBranching <= maxMaxBranching; maxBranching++)
		{
			outputWriter.write("\n" + maxBranching + ",");
			
			writeBinarizationDataRow(outputWriter, scoresComputation, maxBranching - 1);
		}	
	}

	public void writeBinarizationStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		this.writeBinarizationStatistics(outputWriter,binarizationScores.binarizationScoreComputation);
	}
	
	public void writeBinarizationSTDStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		this.writeBinarizationStatistics(outputWriter,binarizationScores.getBinarizationScoreSTDComputation());
	}
	
	public void writeCorpusBinarizationStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		this.writeBinarizationStatistics(outputWriter,binarizationScores.corpusBinarizationScoreComputation);
	}
	
	public void writeTranslationEquivalentsCoverageStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		int noLabelsWritten = writeMaxBranchingLabelsHorizontal(outputWriter, binarizationScores.translationEquivalentsCoverageComputation);
		outputWriter.write("\n");
		int noDataItemsWritten = writeBinarizationDataRow(outputWriter, binarizationScores.translationEquivalentsCoverageComputation, 0);
		outputWriter.write("\n");
		System.out.println(">> writeTranslationEquivalentsCoverageStatistics  noLabelsWritten :" + noLabelsWritten + " noDataItemsWritten:" +  noDataItemsWritten);
		Assert.assertEquals(noLabelsWritten,noDataItemsWritten);
	}

	public void writeCoverageWithExtraAlignmentsStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		writeMaxExtraAlignmentsHorizontal(outputWriter, binarizationScores.coverageWithExtraAlignmentsComputation);
		outputWriter.write("\n");
		writeGeneralDataRow(outputWriter, binarizationScores.coverageWithExtraAlignmentsComputation, 0);
		outputWriter.write("\n");
	}

	public void writeTranslationEquivalentsCoverageWithExtraAlignmentsStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		int noLabelsWritten = writeMaxExtraAlignmentsHorizontal(outputWriter, binarizationScores.translationEquivalentsCoverageWithExtraAlignmentsComputation);
		outputWriter.write("\n");
		int noDataItemsWritten =  writeGeneralDataRow(outputWriter, binarizationScores.translationEquivalentsCoverageWithExtraAlignmentsComputation, 0);
		outputWriter.write("\n");
		Assert.assertEquals(noLabelsWritten,noDataItemsWritten);
	}
	
	
	public void writeCorpusBITTBinarizationStatistics(BufferedWriter outputWriter, BinarizationScores binarizationScores) throws IOException
	{
		this.writeMaxSourceLengthLabelsFull(outputWriter);
		outputWriter.write("\n");
		writeBinarizationDataRow(outputWriter, binarizationScores.bittCorpusBinarizationScoreComputation, 0);
		outputWriter.write("\n");
	}
	
	
	public int writeBinarizationDataRow(BufferedWriter outputWriter, ScoreComputation scoreComputation, int index) throws IOException
	{
		List<Double> dataRow = scoreComputation.getDataRowToRender(index);
		return writeBinarizationStatisticsRow(outputWriter,dataRow);
		
	}

	public int writeGeneralDataRow(BufferedWriter outputWriter, ScoreComputation scoreComputation, int index) throws IOException
	{
		List<Double> dataRow = scoreComputation.getDataRowToRender(index);
		int elementsWritten = writeGeneralDataRow(outputWriter,dataRow);
		return elementsWritten;
	}
	
	
	public void writeBITTBinarizationStatistics(BufferedWriter outputWriter,BinarizationScores binarizationScores) throws IOException
	{
		System.out.println("Writing  BITT Binarization Statistics...");
		
		this.writeMaxSourceLengthLabelsFull(outputWriter);
		outputWriter.write("\n");
		writeBinarizationDataRow(outputWriter, binarizationScores.getBittBinarizationScoreComputation(), 0);
		outputWriter.write("\n");
	}
	
	public void writeBITTBinarizationSTDStatistics(BufferedWriter outputWriter,BinarizationScores binarizationScores) throws IOException
	{
		System.out.println("Writing  BITT Binarization Statistics...");
		
		this.writeMaxSourceLengthLabelsFull(outputWriter);
		outputWriter.write("\n");
		writeBinarizationDataRow(outputWriter, binarizationScores.bittBinarizationScoreSTDComputation, 0);
		outputWriter.write("\n");
	}
	

	
	
	
	public void writeCoarseTransductionOperationStatistics(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing  Transduction Operation Statistics...");
		outputWriter.write("branchingFactor, observed_Types, num_Tokens");
		
		for(int branchingFactor = 1 ; branchingFactor <= ebitgTreeStatisticsData.maxMaxBranchingFactor; branchingFactor++)
		{	
			int observedTypes = ebitgTreeStatisticsData.getObservedTypesCount(branchingFactor);
			int branchingFactorNumTokens = ebitgTreeStatisticsData.getObservedTokensCount(branchingFactor);
			outputWriter.write("\n" + branchingFactor + "," + observedTypes + "," + branchingFactorNumTokens);
		}	
	}
	

	public void writeDetailedTransductionOperationStatistics(BufferedWriter outputWriter) throws IOException
	{
		System.out.println("Writing Detailed Transduction Operation Statistics...");
		outputWriter.write("branchingFactor;  count; transduction_operation");
		for(int branchingFactor = 1 ; branchingFactor <= ebitgTreeStatisticsData.maxMaxBranchingFactor; branchingFactor++)
		{	
			ArrayList<NumberedObject<EbitgTransductionOperation>> frequencySortedOperations =  ebitgTreeStatisticsData.transductionOperationCountsComputation.getFrequencySortedOperations(branchingFactor);
			
			if(frequencySortedOperations.size() > 0)
			{
				 for(NumberedObject<EbitgTransductionOperation> no : frequencySortedOperations)
				 {
					outputWriter.write("\n" + branchingFactor + ";" + no.getNumber() +";" + no.getObject() ); 
				 }
			}
		}

	}
	

	
	
	
	public BufferedWriter getWriterForConfigOrDefaultName(String nameInConfig, ConfigFile theConfig, String defaultName, String outputFolderName) throws IOException
	{
		String fileName;
		if(theConfig.hasValue(nameInConfig))
		{	
			fileName= outputFolderName + "/" + theConfig.getValue(nameInConfig);
		}
		else
		{
			fileName = outputFolderName + "/" + defaultName;
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		return writer;
	}
	

	
	
	public void writeBinarizationScores(BinarizationScores binarizationScores, ConfigFile theConfig, String outputFolderName, String suffix) throws IOException
	{
	
		BufferedWriter outputWriter2 = getWriterForConfigOrDefaultName( "binarizationScores" + suffix,theConfig,  "binarizationScores" + suffix +".csv", outputFolderName);
		writeBinarizationStatistics(outputWriter2, binarizationScores);
		outputWriter2.close();	
			
		outputWriter2 =  getWriterForConfigOrDefaultName( "binarizationScoresSTDs"+ suffix,theConfig,  "binarizationScoresSTDs" + suffix +".csv", outputFolderName);
		writeBinarizationSTDStatistics(outputWriter2, binarizationScores);
		outputWriter2.close();	
		
		
		outputWriter2 =  getWriterForConfigOrDefaultName( "binarizationCorpusScores"+ suffix,theConfig,  "binarizationCorpusScores" + suffix +".csv", outputFolderName);
		writeCorpusBinarizationStatistics(outputWriter2, binarizationScores);
		outputWriter2.close();	
	
		outputWriter2 = getWriterForConfigOrDefaultName( "bittBinarizationScores"+ suffix,theConfig,  "bittBinarizationScores" + suffix +".csv", outputFolderName);
		writeBITTBinarizationStatistics(outputWriter2, binarizationScores);
		outputWriter2.close();	
	
		
		outputWriter2 =  getWriterForConfigOrDefaultName( "bittBinarizationScoresSTDs"+ suffix,theConfig,  "bittBinarizationScoresSTDs" + suffix +".csv", outputFolderName);
		writeBITTBinarizationSTDStatistics(outputWriter2,binarizationScores);
		outputWriter2.close();	
		
		
		outputWriter2 =  getWriterForConfigOrDefaultName( "bittBinarizationCorpusScores"+ suffix,theConfig,  "bittBinarizationCorpusScores" + suffix +".csv", outputFolderName);
		writeCorpusBITTBinarizationStatistics(outputWriter2,binarizationScores);
		outputWriter2.close();	
		
	
	}
	
	
	public void writeDetailedCoverageScores(BinarizationScores binarizationScores, ConfigFile theConfig, String outputFolderName) throws IOException
	{	
		BufferedWriter outputWriter2;
		outputWriter2 = getWriterForConfigOrDefaultName( "translationEquivalentsCoverageGivenMaxBranchingFactorScores",theConfig,  "translationEquivalentsCoverageGivenMaxBranchingFactorScores" +".csv", outputFolderName);
		writeTranslationEquivalentsCoverageStatistics(outputWriter2, ebitgTreeStatisticsData.binarizationScores);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "coverageWithExtraAlignmentsScores",theConfig,  "coverageWithExtraAlignmentsScores" +".csv", outputFolderName);
		writeCoverageWithExtraAlignmentsStatistics(outputWriter2, ebitgTreeStatisticsData.binarizationScores);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "translationEquivalentsCoverageGivenhExtraAlignmentsScores",theConfig,  "translationEquivalentsCoverageGivenExtraAlignmentsScores" + ".csv", outputFolderName);
		writeTranslationEquivalentsCoverageWithExtraAlignmentsStatistics(outputWriter2, ebitgTreeStatisticsData.binarizationScores);
		outputWriter2.close();
	}	
	
	public void writeResults(ConfigFile theConfig, String rootOutputFolderPath) throws IOException
	{
	

		// Get the output folder name from the configfile
		String outputFolderPath;
		if(theConfig.hasValue("treeStatisticsOutputFolder"))
		{	
			outputFolderPath = rootOutputFolderPath + theConfig.getValue("treeStatisticsOutputFolder")  + CorpusTreeReordering.getDateIimeWithoutWhiteSpace();
		}
		else
		{
			outputFolderPath = rootOutputFolderPath + "treeStatisticsResults" + CorpusTreeReordering.getDateIimeWithoutWhiteSpace();
		}
		// Create ouput directory if non-existant
		FileUtil.createFolderIfNotExisting(outputFolderPath);
		
		// Output locations
		 String outputFileName =  outputFolderPath + "/" +  theConfig.getStringIfPresentOrReturnDefault("treeStatisticsFileName",DEFAULT_COMBINED_RESULTS_FILE_NAME);
		 BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
		
		
		
		// Write the collected statistics to the output file
		outputWriter.write("\n" + CorpusTreeReordering.getDateTime()); // write a date/time when the reordering was performed
		outputWriter.write("\n\n");
		outputWriter.write("\n<EbitgTreeStatistics>");
		
		// Write the normal statistics
	
		// Write general statistics about processed corpus, maximum sentence lengths etc
		 int tableNum = 1;
		
		outputWriter.write("\n<table " + tableNum +">");
		writeGeneralStatistics(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;

		// Write normal statistics
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nAbsolute Frequencies of different tree types:\n\n");
		this.writeAllAbsoluteFrequencies(outputWriter); // write results to file containing all results
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nRelative Frequencies of different tree types:\n\n");
		this.writeAllParsedRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nRelative frequencies over null containing sentence pairs:\n\n");
		this.writeParsedWithNullsRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nRelative frequencies over null free sentence pairs:\n\n");	
		this.writeParsedWithoutNullsRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		// Write the cumulative statistics
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative absolute frequencies:\n\n");
		this.writeAllCumulativeAbsoluteFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative Relative Frequencies:\n\n");
		this.writeAllCumulativeParsedRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative Relative frequencies over null containing sentence pairs:\n\n");
		this.writeParsedWithNullsCumulativeRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative Relative frequencies over null free sentence pairs:\n\n");
		this.writeParsedWithoutNullsCumulativeRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCounts of trees with certain maximal branching:\n\n");
		writeMaxBranchingCounts(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative Counts of trees with certain maximal branching upto max_maxBranching:\n\n");
		writeCumulativeMaxBranchingCounts(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nRelative frequencies of trees with certain maximal branching upto max_maxBranching:\n\n");
		writeMaxBranchingRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nCumulative relative frequencies of trees with certain maximal branching upto max_maxBranching:\n\n");
		writeCumulativeMaxBranchingRelativeFrequencies(outputWriter);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBinarization scores means for differnt maxBranchingFactors:\n\n");
		writeBinarizationStatistics(outputWriter,ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBinarization scores stds for differnt maxBranchingFactors:\n\n");
		writeBinarizationSTDStatistics(outputWriter, ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;

		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBinarization Corpus scores for differnt maxBranchingFactors:\n\n");
		writeCorpusBinarizationStatistics(outputWriter, ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
		

		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBITT Binarization scores means for differnt maxBranchingFactors:\n\n");
		writeBITTBinarizationStatistics(outputWriter,ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBITT Binarization scores stds for differnt maxBranchingFactors:\n\n");
		writeBITTBinarizationSTDStatistics(outputWriter,ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
		
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nBITT Corpus Binarization scores for differnt maxBranchingFactors:\n\n");
		writeCorpusBITTBinarizationStatistics(outputWriter, ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
	
		outputWriter.write("\n<table " + tableNum +">");
		outputWriter.write("\n\nTranslation Equivalents coverage scores for differnt maxBranchingFactors:\n\n");
		writeTranslationEquivalentsCoverageStatistics(outputWriter, ebitgTreeStatisticsData.binarizationScores);
		outputWriter.write("\n</table>\n");
		tableNum++;
	
		
		// Write the results to separate files as well
		writeResultsInSeparatefiles(outputFolderPath, theConfig);
		
		outputWriter.write("\n</EbitgTreeStatistics>");
		
		outputWriter.close();
	}
	
	
	public void writeResultsInSeparatefiles(String outputFolderName, ConfigFile theConfig) throws IOException
	{
		
		// Write normal statistics
		BufferedWriter outputWriter2 = getWriterForConfigOrDefaultName( "absoluteFrequenciesAllFileName",theConfig,  "absoluteFrequenciesAllHATs.csv", outputFolderName);
		this.writeAllAbsoluteFrequencies(outputWriter2); // write results to separate file
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "relativeFrequenciesAllFileName",theConfig,  "relativeFrequenciesAllHATs.csv", outputFolderName);
		this.writeAllParsedRelativeFrequencies(outputWriter2);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "relativeFrequenciesNullsFileName",theConfig,  "relativeFrequenciesNullHATs.csv", outputFolderName);
		this.writeParsedWithNullsRelativeFrequencies(outputWriter2);
		outputWriter2.close();	
		
		outputWriter2 = getWriterForConfigOrDefaultName( "relativeFrequenciesPuresFileName",theConfig,  "relativeFrequenciesPureHATs.csv", outputFolderName);
		this.writeParsedWithoutNullsRelativeFrequencies(outputWriter2);
		outputWriter2.close();
		
		// Write the cumulative statistics
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeAbsoluteFrequenciesAllFileName",theConfig,  "cumulativeAbsoluteFrequenciesAllHATs.csv", outputFolderName);
		this.writeAllCumulativeAbsoluteFrequencies(outputWriter2);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeRelativeFrequenciesAllFileName.csv",theConfig,  "cumulativeRelativeFrequenciesAllHATs.csv", outputFolderName);
		this.writeAllCumulativeParsedRelativeFrequencies(outputWriter2);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeRelativeFrequenciesNullsFileName.csv",theConfig,  "cumulativeRelativeFrequenciesNullHATs.csv", outputFolderName);
		this.writeParsedWithNullsCumulativeRelativeFrequencies(outputWriter2);
		outputWriter2.close();
		
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeRelativeFrequenciesPuresFileName.csv",theConfig,  "cumulativeRelativeFrequenciesPureHATs.csv", outputFolderName);
		this.writeParsedWithoutNullsCumulativeRelativeFrequencies(outputWriter2);
		outputWriter2.close();	
		
		
		
		// Branching factor statistics
		
		outputWriter2 = getWriterForConfigOrDefaultName( "maxBranchingCounts",theConfig,  "maxBranchingCounts.csv", outputFolderName);
		writeMaxBranchingCounts(outputWriter2);
		outputWriter2.close();	
		
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeMaxBranchingCounts",theConfig,  "cumulativeMaxBranchingCounts.csv", outputFolderName);
		this.writeCumulativeMaxBranchingCounts(outputWriter2);
		outputWriter2.close();	
		
		outputWriter2 = getWriterForConfigOrDefaultName( "maxBranchingRelativeFrequencies",theConfig,  "maxBranchingRelativeFrequencies.csv", outputFolderName);
		writeMaxBranchingRelativeFrequencies(outputWriter2);
		outputWriter2.close();	
		
		outputWriter2 = getWriterForConfigOrDefaultName( "cumulativeMaxBranchingRelativeFrequencies",theConfig,  "cumulativeMaxBranchingRelativeFrequencies.csv", outputFolderName);
		writeCumulativeMaxBranchingRelativeFrequencies(outputWriter2);
		outputWriter2.close();	
	
		writeBinarizationScores(ebitgTreeStatisticsData.binarizationScores, theConfig, outputFolderName, "");
		writeBinarizationScores(ebitgTreeStatisticsData.nodeExtractionScores, theConfig, outputFolderName, "RELATIVE");
	
		writeDetailedCoverageScores(ebitgTreeStatisticsData.binarizationScores, theConfig, outputFolderName);
		
		outputWriter2 = getWriterForConfigOrDefaultName( "coarseTransductionOperationStatistics",theConfig,  "coarseTransductionOperationStatistics.csv", outputFolderName);
		writeCoarseTransductionOperationStatistics(outputWriter2); 
		outputWriter2.close();	
		
		outputWriter2 = getWriterForConfigOrDefaultName( "detailedTransductionOperationStatistics",theConfig,  "detailedTransductionOperationStatistics.csv", outputFolderName);
		writeDetailedTransductionOperationStatistics(outputWriter2); 
		outputWriter2.close();	
	}
	
}
