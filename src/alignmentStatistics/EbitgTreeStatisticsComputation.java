/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInference.InferenceComplexityType;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgNode;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgInference.InferenceType;
import extended_bitg.EbitgTreeState.TreeType;
import extended_bitg.EbitgTreeStateBasic;

public abstract  class EbitgTreeStatisticsComputation 
{
	
	private final boolean atomicPartsMustAllShareSameAlignmentChunk;
	
	public EbitgTreeStatisticsComputation(boolean atomicPartsMustAllShareSameAlignmentChunk)
	{
		this.atomicPartsMustAllShareSameAlignmentChunk = atomicPartsMustAllShareSameAlignmentChunk;
	}	
	
	
	public abstract boolean subTreePairMayBeTreatedAtomic(EbitgTreeState<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?,?>> state, int maxSourceLengthAtomicFragment, EbitgChart chart);
	public abstract int nullIgnoringBranchingFactorState(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state);
	public abstract int absoluteBinarizationScoreNormalizer(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg);
	public abstract int alignmentLength(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg);
	
	public int determineNumberInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg , int  maxBranchingFactor)
	{
		return determineNumberInternalNodes(tg.getRootState(), maxBranchingFactor);
	}
	
	public TreeType determineTreeType(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg , int  maxSourceLengthAtomicFragment)
	{
		return determineTreeType(tg.getRootState(),  maxSourceLengthAtomicFragment,tg.getChart());
	}

	public int determineMaximalOccuringBranching(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg , int  maxSourceLengthAtomicFragment)
	{
		return determineMaximalOccurringNullIgnoringBranching(tg.getRootState(),  maxSourceLengthAtomicFragment, tg.getChart());
	}
	
	public int[] determineNodeTypeCounts(EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg)
	{
		return  determineNodeTypeCounts(tg.getRootState());
	}
	
	public int determineMaxNumberOfExtraAlignmentsInTree(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		return determineMaximalNumberExtraAlignmentsAcrossNodesInTree(tg.getRootState());
	}
	
	
	
	public int determineNumberTranslationEquivalentNodesInChart(EbitgChart chart)
	{
		int result = 0;
		
		//System.out.println("EbitgTreeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart" + " chart: " + chart);
		
		
		for(EbitgChartEntry chartEntry : chart.getChartEntriesAsList())
		{
			for(EbitgNode node : chartEntry.getNodes())
			{
				EbitgInference firstNodeInference = chartEntry.getInference(node, 0);
				
				if(firstNodeInference.isComplete() && node.isPartOfAlignmentDerivation())
				{
					result++;
				}
			}
		}
		
		return result;
	}
	
	
	public boolean subtreeMustBeIgnored(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state, int maxSourceLengthAtomicFragment, EbitgChart chart)
	{
		if(atomicPartsMustAllShareSameAlignmentChunk)
		{
			//return state.mapsToOnePositionOnEitherSide() || this.subTreePairMayBeTreatedAtomic(state, maxSourceLengthAtomicFragment, chart);
			return stateConsistsOfOnePart(state) || this.subTreePairMayBeTreatedAtomic(state, maxSourceLengthAtomicFragment, chart);	
			
		}
		else
		{
			return state.subTreeContainsNoContiguousTranslationEquivalents() || this.subTreePairMayBeTreatedAtomic(state, maxSourceLengthAtomicFragment, chart);
		}
	}
	
	private boolean stateConsistsOfOnePart(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state)
	{
		return state.consistsOfOneAlignmentChunk();
	}
	
	
	/**
	 * Method that determines the type of a EbitgTreeState.
	 * The type is BITT by default, and becomes :
	 *  - HAT if the state or any of its children is HAT
	 * 	- PET if the state or any of its children is PET (and none is HAT)	
	 * If the source span of the state is smaller then what we allow to be treated as an 
	 * atomic fragment, we treat the state as an atomic fragment, and consider its type 
	 * BITT. That means the subtree of the state will then not cause the entire tree 
	 * to be non-BITT (it will only become non-BITT if some subtree spanning a source 
	 * that has a source length > maxSourceLengthAtomicFragment is contained in the tree).
	 * 
	 * @param state
	 * @param maxSourceLengthAtomicFragment
	 * @return The tree type
	 */
	public TreeType determineTreeType(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state, int maxSourceLengthAtomicFragment, EbitgChart chart)
	{

		if(subtreeMustBeIgnored(state, maxSourceLengthAtomicFragment,chart))
		{
			return TreeType.BITT;
		}
		
		// type is ITG unless proven otherwise
		TreeType treeType = TreeType.BITT;
		
		if(state.getInferenceComplexityType() == InferenceComplexityType.HAT)
		{
			return TreeType.HAT;
		}
		
		// If the state's chosenInference is of type PET, the three is of type PET unless 
		// even a HAT node is found amongst the subtrees of the children
		else if(state.getInferenceComplexityType() == InferenceComplexityType.PET)
		{
	
			treeType = TreeType.PET;
		}
		
		for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			TreeType childType = determineTreeType(child,  maxSourceLengthAtomicFragment,chart);
			
			// if just one of the child subtrees is HAT, the entire tree is HAT
			if(childType == TreeType.HAT)
			{
				return childType;
			}
			
			// If one of the child subtrees is permutation, then the entire subtree is at least 
			// permutation, but possibly even HAT, so we have to continue checking
			else if(childType == TreeType.PET)
			{
				treeType = TreeType.PET;
			}
		}
	
		return treeType;
		
		
	}

	/**
	 *  Method that determines the maximal occurring branching in the Tree, that 
	 *  is the maximal occurring number of children over all the nodes in the Tree
	 *  In this computation null-aligned words are ignored, in order to be more robust 
	 *  and arrive at the same branching factor for all HATs for a certain alignment.
	 *  The refinement of EbitgTreeStatisticsComputation will determine whether this branching factor depends only on the source
	 *  size or also on the target side.
	 * @param state The EbitgTreeState for which to compute the maximal branching factor
	 * @param maxSourceLengthAtomicFragment The maximum (source) length of subtrees that may be ignored in determining the result
	 * @return The maximum occurring branching factor 
	 */
	public int determineMaximalOccurringNullIgnoringBranching(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state, int maxSourceLengthAtomicFragment,EbitgChart chart)
	{
		
		int maxBranching = 1;

		if(subtreeMustBeIgnored(state, maxSourceLengthAtomicFragment,chart))
		{
			return 1;
		}
		
		int nodeBranching = this.nullIgnoringBranchingFactorState(state);
		
		maxBranching = Math.max(nodeBranching, maxBranching);
		
		// Determine the maximal branching over the children nodes, and update
		// the maximum value if one of the children node subtrees has a higher max branching
		for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			int childMaxBranching = determineMaximalOccurringNullIgnoringBranching(child,  maxSourceLengthAtomicFragment,chart);
			
			maxBranching = Math.max(childMaxBranching, maxBranching);
			
		}
		
		return maxBranching;
	}
	
	/**
	 * This method recursively finds the maximum number of extra alignments across a tree state and its children 
	 * tree states. 
	 * @param state
	 * @return
	 */
	private int determineMaximalNumberExtraAlignmentsAcrossNodesInTree(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state)
	{
		int result = approximateMaxNumberOfExtraAlignments(state);
		
		// Determine the maximal result over the children nodes, and update
		// the maximum value if one of the children node subtrees has a higher value
		for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			int childResult = determineMaximalNumberExtraAlignmentsAcrossNodesInTree(child);
			result = Math.max(result, childResult);
		}
		return result;
	}
	
	private static int determineNumberOfAlignmentsCountingCompleteChildrenAsOne(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state){
	  int numberOfAlignments = 0;
      
      for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
      {
          if(child.chosenInferenceIsComplete())
          {
              //System.out.println(">>>EbitgTreeStatisticsComputation. determineMaxNumberOfExtraAlignments : child is complete");
              //System.out.println("State: " + child);
              // The state corresponds to a translation equivalent : a variable/gap in the inference
              numberOfAlignments++;
          }
          else
          {
              int childNonZeroAlignments = child.getNumberAlignedTargetPositions(); 
              assert(childNonZeroAlignments >= 1);
              numberOfAlignments += childNonZeroAlignments;
              
              //System.out.println(">>>EbitgTreeStatistics. determineMaxNumberOfExtraAlignments ..");
              //System.out.println("State: " + child + " childNonZeroAlignments " + childNonZeroAlignments);
          }
      }
      return numberOfAlignments;
	}
	
	/**
	 * This method gives an approximate estimate of the number of extra alignments for a tree state (non-recursively).
	 * This number is defined by first counting the total number of alignment links in the state
	 * (whereby complete child states are counted as one alignment link) and then subtracting 
	 * from this number the minimal number of children (across source and target side) for this state
	 * ("the minimum null ignoring branching factor"). Unfortunately, this method exact in the sense 
	 * of really yielding the minimum number of alignment links that needs to be deleted to yield 
	 * a bijective alignments. As it turns out, computing this actual number of extra alignments is much 
	 * more complicated, as it corresponds to a more complex optimization algorithm. This problem probably 
	 * requires at least something like the Dijkstra shortest path / Viterbi algorithm to be correctly solved.  
	 * 
	 * @param state
	 * @return
	 */
	private static int approximateMaxNumberOfExtraAlignments(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state)
	{
		
		if(state.subTreeContainsNoContiguousTranslationEquivalents())
		{
			return 0;
		}	
		
		int minNullIgnoringBranchingFactor = state.computeMinNullIgnoringBranchingFactor();
		//System.out.println("numberOfAlignments: " + numberOfAlignments);
		//System.out.println("minNullIgnoringBranchingFactor: " + minNullIgnoringBranchingFactor);
		
		int result = determineNumberOfAlignmentsCountingCompleteChildrenAsOne(state) - minNullIgnoringBranchingFactor;
		
		assert (result >= 0);
		
		return result;
		
	}
	
	
	
	
	
	
	
	
	
	
	public static int determineMaxPossibleInternalNodes(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		return determineMaxPossibleInternalNodes(tg.getRootState());
	}
	
	private static int determineMaxPossibleInternalNodes(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state)
	{
		
		InferenceType type = state.getInferenceType();
		int numInternalNodes = 0;
		
		// If the Inference at this state is of type Atomic or SourceNullsBinding, 
		// meaning it is a leaf rather than internal node, we return 0 for this subtree
		if(state.subTreeContainsNoContiguousTranslationEquivalents() || (type == InferenceType.Atomic) || (type == InferenceType.SourceNullsBinding))
		{	
			return 0;
		}	
		
		else
		{
			numInternalNodes += 1;
		}
		
		
		for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			int childNumInternalNodes = determineMaxPossibleInternalNodes(child);
			
			numInternalNodes += childNumInternalNodes;
			
		}
		
		return numInternalNodes;
		
	}
	
	
	public int determineNumberInternalNodes(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> state, int maxBranchingFactor)
	{
		InferenceType type = state.getInferenceType();
		int numInternalNodes = 0;
		
		// If the Inference at this state is of type Atomic or SourceNullsBinding, 
		// meaning it is a leaf rather than internal node, we return 0 for this subtree
		if(state.subTreeContainsNoContiguousTranslationEquivalents()  || (type == InferenceType.Atomic) || (type == InferenceType.SourceNullsBinding))
		{	
			return 0;
		}	
		
		// If the chosen inference (=>the state) has more children than allowed, we
		// treat the whole subtree as one big chunk since that is the only way a grammar 
		// with the chosen maxBranchingFactor could deal with it. That means it has 0 internal nodes
		else if(nullIgnoringBranchingFactorState(state) > maxBranchingFactor)
		{
			return 0;
		}
		else
		{
			numInternalNodes += 1;
		}
		
		
		for(EbitgTreeState<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> child : state.getChildren())
		{
			int childNumInternalNodes = determineNumberInternalNodes(child, maxBranchingFactor);
			
			numInternalNodes += childNumInternalNodes;
			
		}
		
		return numInternalNodes;

	}
	
	public  int[] determineNodeTypeCounts(EbitgTreeState<EbitgLexicalizedInference,EbitgTreeStateBasic> state)
	{
		InferenceType type = state.getInferenceType();
		int[] nodeTypeCounts = new int[5];
		
		// If the Inference at this state is of type Atomic or SourceNullsBinding, 
		// meaning it is a leaf rather than internal node, we return 0 for this subtree
		if(type == InferenceType.Atomic) 
		{	
			nodeTypeCounts[0]++;
			return nodeTypeCounts;
		}
		
		if(type == InferenceType.SourceNullsBinding)
		{
			nodeTypeCounts[1]++;
			 // A source null binding node always has one atomic node below it that completes the 
			// subtree it roots. Rather then recursing into that node we just update the count here and finish.
			nodeTypeCounts[0]++;
			return nodeTypeCounts;
		}
		
	
		if(type == InferenceType.BITT)
		{
			nodeTypeCounts[2]++;
		}
		else if(type == InferenceType.PET)
		{
			nodeTypeCounts[3]++;
		}
		else if(type == InferenceType.HAT)
		{
			nodeTypeCounts[4]++;
		}
		else
		{
			System.out.println("Error: An unknown node type has been encountered. Exiting");
			System.exit(0);
		}
		
		
		// Add the node type counts of the children
		for(EbitgTreeStateBasic child : state.getChildren())
		{
			int[] childNodeTypeCounts =  determineNodeTypeCounts(child);
			
			for(int i = 0 ; i < 5; i++)
			{
				nodeTypeCounts[i] += childNodeTypeCounts[i];	
			}
		}
		
		return nodeTypeCounts;

	}
	

}
