/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import alignment.Alignment;
import alignment.AlignmentPair;
import static alignmentStatistics.AlignmentStatistics.createAlignmentStatistics;


import util.InputReading;
import util.Pair;

public abstract class SentencePairAlignmentChunking 
{	
	// the alignments stored as pairs extracted from the input
	
	final int sourceLength, targetLength;
	protected Alignment sentenceAlignments;
	
	private HashMap<Integer,AlignmentChunk> sourcePositionChunkLookupTable;
	private HashMap<Integer,AlignmentChunk> targetPositionChunkLookupTable;
	
	protected List<AlignmentChunk> alignmentChunks;
	
	protected boolean hasSourceNullAlignments, hasTargetNullAlignments;
	
	public boolean hasNullAlignments()
	{
		return (hasSourceNullAlignments || hasTargetNullAlignments);
	}
	
	public boolean hasSourceNullAlignments()
	{
		return hasSourceNullAlignments;
	}
	
	public boolean hasTargetNullAlignments()
	{
		return hasTargetNullAlignments;
	}
	
	public List<AlignmentChunk> getAlignmentChunks()
	{
		return alignmentChunks;
	}
	
	
	
	
	// Constructor for already extracted ArrayLists with Alignment Pairs and source/target words
	protected SentencePairAlignmentChunking(Alignment alignment, int sourceLength, int targetLength) 
	{
		this.sentenceAlignments = alignment;
		this.sourceLength = sourceLength;
		this.targetLength = targetLength;
	}
	
	public static SentencePairAlignmentChunking createSentencePairAlignmentChunkingZerosSeparate(String alignmentString, String sourceString, String targetString) {
		
		Alignment alignment = Alignment.createAlignment(alignmentString);
		List<String> sourceWordArray = InputReading.constructSentenceWordsArrayList(sourceString);
		List<String> targetWordArray = InputReading.constructSentenceWordsArrayList(targetString);
		
		SentencePairAlignmentChunking sentencePairAlignmentChunking = new SentencePairAlignmentChunkingZerosSeparate(alignment, sourceWordArray.size(), targetWordArray.size());
		sentencePairAlignmentChunking.doTheWork();
		return sentencePairAlignmentChunking;
	}
	
	public void doTheWork()
	{
		sourcePositionChunkLookupTable = new HashMap<Integer,AlignmentChunk>(); 
		targetPositionChunkLookupTable = new  HashMap<Integer,AlignmentChunk>(); 
		
		performChunking();
		setUpPositionToChunkMappingTables();
	}
	
	
	
	public int getSourceLength()
	{
		return sourceLength;
	}
	
	public int getTargetLength()
	{
		return targetLength;
	}
		
	public void performChunking()
	{
		//System.out.println(">>>Extract alignment chunks...");
		extractAlignmentChunks();
		//System.out.println(">>>Merge alignment chunks...");
		mergeAlignmentChunks();
		//System.out.println(">>>Add zero alignments...");
		addZeroAlignments();
		//System.out.println("Finished perforomChunking");
	}
	
	
	public void extractAlignmentChunks()
	{
		this.alignmentChunks = new ArrayList<AlignmentChunk>(sentenceAlignments.size());
		
		// Add an AlignmentChunk for every AlignmentPair
		for(AlignmentPair p : sentenceAlignments.getAlignmentPairs())
		{
			this.alignmentChunks.add(new  AlignmentChunk(p));
		}	
	}
	
	/**
	 * Method sets up tables that map source and target positions to the alignment chunks containing them
	 */
	public void setUpPositionToChunkMappingTables()
	{
		for(AlignmentChunk chunk: this.alignmentChunks)
		{
			for(Integer sourcePos : chunk.getSourcePositions())
			{
				sourcePositionChunkLookupTable.put(sourcePos,chunk);		
			}
			
			for(Integer targetPos : chunk.getTargetPositions())
			{
				targetPositionChunkLookupTable.put(targetPos,chunk);		
			}
		}
	}
	
	/**
	 * Method that returns the Alignment Chunk containing the provided source position
	 * @param sourcePosition
	 * @return the AlignmentChunk containing the source position
	 */
	public AlignmentChunk getAlignmentChunkForSourcePosition(int sourcePosition)
	{
		return sourcePositionChunkLookupTable.get(sourcePosition);
	}
	
	/**
	 * Method that returns the Alignment Chunk containing the provided target position
	 * @param targetPosition
	 * @return the AlignmentChunk containing the target position
	 */
	public AlignmentChunk getAlignmentChunkForTargetPosition(int targetPosition)
	{
		return targetPositionChunkLookupTable.get(targetPosition);
	}
	
	
	public void mergeAlignmentChunks()
	{
		// copy all initial AlignmentChunk to a list of mergable chunks
		ArrayList<AlignmentChunk> mergeableChunks = new ArrayList<AlignmentChunk>(alignmentChunks);
		ArrayList<AlignmentChunk> finalChunks = new ArrayList<AlignmentChunk>();
		
		
		assert(mergeableChunks.size() == alignmentChunks.size());
		
		//System.out.println("mergeableChunks.size() : " + mergeableChunks.size());

		
		while(! mergeableChunks.isEmpty())
		{

			//System.out.println("mergeableChunks.size() : " + mergeableChunks.size());
			
			// get the last chunk in the list (For reasons of efficiency we work right to left with the ArrayList, 
			// since then removing elments does require shifting back the other elements to a lesser extent)
			AlignmentChunk c = mergeableChunks.remove(mergeableChunks.size() - 1);
			
			
			boolean suitableMergePartnerFound = false;
			int i = 0;
			
			while(i < mergeableChunks.size() && (!suitableMergePartnerFound))
			{
				AlignmentChunk otherChunk = mergeableChunks.get(i);
				
				if(c.canBeMergedWith(otherChunk))
				{
					//System.out.println("Perform merging...");
					
					c.mergeWithChunk(otherChunk);
					suitableMergePartnerFound = true;
					
					// remove the otherChunk that is used in the merging
					mergeableChunks.remove(i);
					
					// add the merge result
					mergeableChunks.add(c);
				}
				
				i++;
			}
			
			// the chunk c could be merged with none of the other chunks
			if(!suitableMergePartnerFound)
			{
				// Chunk c could not be merged futher, and will not be in the future:
				// It is a final chunk and added as such to the final chunks list
				finalChunks.add(c);
			}
			
		}
		
	
		alignmentChunks = new ArrayList<AlignmentChunk>();
		for(int i = 0; i < finalChunks.size(); i++)
		{
			// Swap the order while putting finalChunks in alignmentChunks
			alignmentChunks.add(finalChunks.get(finalChunks.size() - i - 1));
		}
		
	}
	
	public abstract void addZeroAlignments();

	/**
	 *  Method that checks if there are alignments at all
	 * @return boolean indicating whether alignments are present
	 */
	public boolean hasAlignments()
	{
		return this.sentenceAlignments.size() > 0;
	}
	
	public String toString()
	{
		String result = "noSourceWords: " + getSourceLength();
		result += "noTargetWords: " + getTargetLength();
		result += "\n<AlignmentChunks> :";
		
		int i = 1; 
		for(AlignmentChunk chunk : alignmentChunks)
		{
			result += "\n " + i + "\t" + chunk;
			i++;
		}
		result += "\n</AlignmentChunks>";
			
		return result;
	}
	
	public int getNumLinks()
	{
		return this.sentenceAlignments.size();
	}
	
	
	public List<AlignmentPair> getSentenceAlignments()
	{
		return Collections.unmodifiableList(this.sentenceAlignments.getAlignmentPairs());
	}
	
	public String getAlignmentsAsString()
	{
		return this.sentenceAlignments.getAlignmentAsString();
	}
	
	public List<AlignmentPair> getSourceSpanCoveredAlignments(Pair<Integer> sourceSpan)
	{
	
		List<AlignmentPair> result = new ArrayList<AlignmentPair>();
		
		for(AlignmentPair alignmentPair : this.sentenceAlignments.getAlignmentPairs())
		{
			if((alignmentPair.getSourcePos() >= sourceSpan.getFirst()) && (alignmentPair.getSourcePos() <= sourceSpan.getSecond()))
			{
				result.add(new AlignmentPair(alignmentPair));
			}
		}
		return result;
	}
	
	
	public Set<Integer> getNonZeroSourcePositions()
	{
		HashSet<Integer> nonZeroSourcePositions = new HashSet<Integer>();
		
		for(AlignmentPair p : sentenceAlignments.getAlignmentPairs())
		{
			nonZeroSourcePositions.add(p.getSourcePos());
		}
		
		return nonZeroSourcePositions;
	}
	
	
	public Set<Integer> getNonZeroTargetPositions()
	{
		HashSet<Integer> nonZeroTargetPositions = new HashSet<Integer>();
	
		for(AlignmentPair p : sentenceAlignments.getAlignmentPairs())
		{
			nonZeroTargetPositions.add(p.getTargetPos());
		}
		
		return nonZeroTargetPositions;
	}
	
	
	public Set<Integer> getTargetPositions()
	{
		Set<Integer> result = new HashSet<Integer>();
		
		for(AlignmentChunk chunk : this.alignmentChunks)
		{
			result.addAll(chunk.getTargetPositions());
		}
		return result;
	}
	
	
	protected void initializeHasNullAlignmentPropertiesToFalse()
	{
		this.hasSourceNullAlignments = false;
		this.hasTargetNullAlignments = false;
	}
	
	public static void main(String[] args)
	{
		String dutchString2 = "Ik verklaar de zitting van het Europees Parlement , die op vrijdag 17 december werd onderbroken , te zijn hervat . Ik wens u allen een gelukkig nieuwjaar en hoop dat u een goede vakantie heeft gehad .";
		String englishString2 = "I declare resumed the session of the European Parliament adjourned on Friday 17 December 1999 , and I would like once again to wish you a happy new year in the hope that you enjoyed a pleasant festive period .";
		String alignmentString2 = "0-0 1-1 2-1 3-2 4-3 5-4 6-5 7-6 8-7 9-8 9-9 10-10 11-11 12-12 13-13 9-14 9-15 15-16 9-17 9-18 9-19 16-20 17-21 18-22 19-22 23-22 24-23 24-24 25-25 26-26 14-27 20-27 21-27 26-27 27-27 28-27 38-27 29-28 31-29 32-30 33-31 35-32 36-33 37-34 34-35 34-36 39-37";
		
		SentencePairAlignmentChunking s2 = SentencePairAlignmentChunking.createSentencePairAlignmentChunkingZerosSeparate(alignmentString2, englishString2, dutchString2);
		s2.performChunking();
		AlignmentStatistics as2 = createAlignmentStatistics();
		as2.addAlignmentTypeCounting(s2);
		
		System.out.println(">>>Resulting Alignment Chunks:" + s2);
		System.out.println(">>>Alignment Statistics:" + as2);
		
		/*
		String englishString0 = "That destroyed them . Quite";
		String dutchString0 = "Dat richte hen ten gronde .";
		String alignmentString0 = "0-0 1-1 1-3 1-4 2-2 3-5"; 
		*/
		
		String englishString0 = "s1 s2 s3";
		String dutchString0 = "t1 t2 t3";
		String alignmentString0 = "0-0 2-2";
		SentencePairAlignmentChunking s0 =  SentencePairAlignmentChunking.createSentencePairAlignmentChunkingZerosSeparate(alignmentString0, englishString0, dutchString0);
		s0.performChunking();
		
		AlignmentStatistics as0 = createAlignmentStatistics();
		as0.addAlignmentTypeCounting(s0);
		
		System.out.println(">>>Resulting Alignment Chunks:" + s0);
		//System.out.println(">>>Alignment Statistics:" + as0);
		
	
		System.out.println("numString(  9,6))" + AlignmentStatistics.numString(9,6));
		System.out.println("numString( 10,6))" + AlignmentStatistics.numString(10,6));
		System.out.println("numString( 11,6))" + AlignmentStatistics.numString(11,6));
		System.out.println("numString( 50,6))" + AlignmentStatistics.numString(50,6));
		System.out.println("numString( 99,6))" + AlignmentStatistics.numString(99,6));
		System.out.println("numString(-99,6))" + AlignmentStatistics.numString(-99,6));
		System.out.println("numString( -9,6))" + AlignmentStatistics.numString(-9,6));
		
	}
}
