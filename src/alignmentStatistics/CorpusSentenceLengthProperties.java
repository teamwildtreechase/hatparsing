/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import util.MeanStdComputation;

public class CorpusSentenceLengthProperties 
{
	protected NumberFormat nf = NumberFormat.getInstance();
	private  int maxSourceLength, maxTargetLength, minSourceLength, minTargetLength;
	protected final List<Integer> sourceSentenceLengths;
	protected final List<Integer> targetSentenceLengths;
	private final List<Double> sourceTargetLengthRatios;

	protected CorpusSentenceLengthProperties( List<Integer> sourceSentenceLengths,  List<Integer> targetSentenceLengths, List<Double> sourceTargetLengthRatios)
	{
		this.sourceSentenceLengths = sourceSentenceLengths;
		this.targetSentenceLengths = targetSentenceLengths;
		this.sourceTargetLengthRatios = sourceTargetLengthRatios;
		this.minSourceLength = Integer.MAX_VALUE;
		this.minTargetLength = Integer.MAX_VALUE;
	
	}
	
	public static CorpusSentenceLengthProperties createCorpusSentenceLengthProperties()
	{
		return new CorpusSentenceLengthProperties(new ArrayList<Integer>(), new ArrayList<Integer>(), new ArrayList<Double>());
	}
	
	protected void addCorpusLengthProperties(CorpusSentenceLengthProperties corpusLengtProperties)
	{
		this.maxSourceLength = Math.max(this.maxSourceLength, corpusLengtProperties.maxSourceLength);
		this.maxTargetLength = Math.max(this.maxTargetLength, corpusLengtProperties.maxTargetLength);
		this.minSourceLength = Math.min(this.minSourceLength, corpusLengtProperties.minSourceLength);
		this.minTargetLength = Math.min(this.minTargetLength, corpusLengtProperties.minTargetLength);
		
		
		this.sourceSentenceLengths.addAll(corpusLengtProperties.sourceSentenceLengths);
		this.targetSentenceLengths.addAll(corpusLengtProperties.targetSentenceLengths);
		this.sourceTargetLengthRatios.addAll(corpusLengtProperties.sourceTargetLengthRatios);
	}
	
	
	public int getMaxSourceLengt()
	{
		return maxSourceLength;
	}
	
	public int getMaxTargetLength()
	{
		return maxTargetLength;
	}
	
	public int getMinSourceLength()
	{
		return minSourceLength;
	}
	
	public int getMinTargetLength()
	{
		return minTargetLength;
	}
	
	public double getMeanSourceLength()
	{
		return MeanStdComputation.computeIntsMean(this.sourceSentenceLengths);
	}
	
	public double getMeanTargetLength()
	{
		return MeanStdComputation.computeIntsMean(this.targetSentenceLengths);
	}
	
	public double getStdSourceLength()
	{
		return MeanStdComputation.computeIntsStd(this.sourceSentenceLengths);
	}
	
	public double getStdTargetLength()
	{
		return MeanStdComputation.computeIntsStd(this.targetSentenceLengths);
	}
	
	public double getMeanSourceTargetRatio()
	{
		return MeanStdComputation.computeDoublesMean(this.sourceTargetLengthRatios);
	}
	
	public double getStdSourceTargetRatio()
	{
		return MeanStdComputation.computeDoublesStd(this.sourceTargetLengthRatios);
	}
	
	public List<Integer> getSourceLengths()
	{
		return Collections.unmodifiableList(sourceSentenceLengths);
	}
	
	public List<Integer> getTargetLengths()
	{
		return Collections.unmodifiableList(targetSentenceLengths);
	}
	
	public List<Double> getSourceTargetLengthRatios()
	{
		return Collections.unmodifiableList(sourceTargetLengthRatios);
	}
	
	protected void updateSentenceLengthStatistics(int sourceLength, int targetLength)
	{
		double sourceTargetLengthRatio = ((double) sourceLength) / targetLength; 
		this.sourceSentenceLengths.add(sourceLength);
		this.targetSentenceLengths.add(targetLength);
		this.sourceTargetLengthRatios.add(sourceTargetLengthRatio);
		updateMaxAndMinLengths(sourceLength, targetLength);
	}
	
	private void updateMaxAndMinLengths(int sourceLength, int targetLength)
	{
		maxSourceLength = Math.max(maxSourceLength, sourceLength);
		maxTargetLength = Math.max(maxTargetLength, targetLength);
		minSourceLength = Math.min(minSourceLength,sourceLength);
		minTargetLength = Math.min(minTargetLength,targetLength);
	}
	
	public String getSentenceLengthPropertiesString()
	{
		String result = "";
		result += "SourceSentenceLengths:  " + this.sourceSentenceLengths.size() + " entries: \n" + this.sourceSentenceLengths;
		result += "\n mean source length:" + getMeanSourceLength();
		result += "\n\nTargetSentenceLengths: " + this.targetSentenceLengths.size() + " entries: \n" + this.targetSentenceLengths;
		result += "\n mean target length:" + getMeanTargetLength();
		
		return result;
	}
	
}
