/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import alignment.AlignmentStringTriple;

public class AlignmentTriplesEnumeration extends InputStringsEnumeration<AlignmentStringTriple>
{

	protected AlignmentTriplesEnumeration(List<BufferedReader> inputReaders)
	{
		super(inputReaders);
	}
	
	public static AlignmentTriplesEnumeration createAlignmentTriplesEnumeration(String sourceFileName, String targetFileName, String alignmentsFileName)
	{
		try 
		{
			BufferedReader sourceReader = new BufferedReader(new FileReader(sourceFileName));
			BufferedReader targetReader = new BufferedReader(new FileReader(targetFileName));
			BufferedReader alignmentsReader = new BufferedReader(new FileReader(alignmentsFileName));
			List<BufferedReader> inputReaders = Arrays.asList(sourceReader,targetReader,alignmentsReader);
			return new AlignmentTriplesEnumeration(inputReaders);
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	protected AlignmentStringTriple getNextT(List<String> nextInputStrings) 
	{
		assert(nextInputStrings.size() == 3);
		return AlignmentStringTriple.createTrimmedAlignmentStringTriple(nextInputStrings.get(0), nextInputStrings.get(1), nextInputStrings.get(2));
	}

	@Override
	protected boolean inputStringsAreAcceptable(List<String> nextInputStrings) {
		return true;
	}

}
