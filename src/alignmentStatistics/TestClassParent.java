/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public class TestClassParent 
{

	protected void innerMethod()
	{
		System.out.println("TestClassParent Inner Method called");
	}
	
	
	public void outerMethod()
	{
		System.out.println("TestClassParent Outer Method called");
		
		innerMethod();
	}
	
}
