/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

import junit.framework.Assert;

public class BasicNormalization implements MatrixComputationPerformer, ScoreDerivativeComputation

{
	
	public void computeRowComputation(List<Double> row, int normalizer)
	{
		computeNormalizedScoresRow(row, normalizer);
	}
	
	public void computeNormalizedScoresRow(List<Double> meansRow, int normalizer)
	{
		if(normalizer > 0 )
		{	
			for(int i = 0; i < meansRow.size(); i++)
			{	
				double oldValue = meansRow.get(i);
				double newValue = oldValue / normalizer;
				
				//System.out.println("MEAN : oldValue: " + oldValue + " newValue: " + newValue + " normalizer: " + normalizer);
				meansRow.set(i, newValue);
			}
		}
		else // The normalizer is 0, meaning all scores should be maximal
		{	
			for(int i = 0; i < meansRow.size(); i++)
			{	
				double newValue = 1;
				meansRow.set(i, newValue);
			}
		}	
	}

	@Override
	public double computeDerivedScore(double score, int rowIndex, int columnIndex)
	{
		Assert.assertTrue(score <= 100);
		return score;
	}
	
}
