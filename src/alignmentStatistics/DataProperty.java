/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

public interface DataProperty 
{
	public String getPropertyName();
	public String getPropertyValue();
}
