/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */

package alignmentStatistics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import alignment.AlignmentStringTriple;

import util.Pair;
import static alignmentStatistics.EbitgTreeStatisticsData.createEbitgTreeStatisticsData;
import extended_bitg.EbitgChartBuilderBasic;


public class EbitgTreeStatisticsMultiThread extends EbitgCorpusComputationMultiThread<EbitgTreeStatisticsMultiThread,AlignmentStringTriple>
{
	private static final boolean USE_GREEDY_NULL_BINDING = true;
	private static final boolean USE_CACHING = true;
	
	EbitgTreeStatisticsData statisticsData;
	// minimal and maximal sentence pair numbers
	
	private String hatPropertiesFileName;
	BufferedWriter hatPropertiesWriter;
	private boolean writheHATProperties;

	/**
	 * Default constructor
	 */

	public EbitgTreeStatisticsMultiThread(int maxAllowedInferencesPerNode,int maxSourceLength, Pair<Integer> minMaxSentencePairNums, InputStringsEnumeration<AlignmentStringTriple> inputStringEnumeration, String hatPropertiesFileName, int maxSentenceLength, int outputLevel, boolean ignoreNonReducableTranslationEquivalents) 
	{	
		super(maxAllowedInferencesPerNode, minMaxSentencePairNums, inputStringEnumeration,maxSentenceLength, outputLevel);
		this.statisticsData =  createEbitgTreeStatisticsData(maxAllowedInferencesPerNode,maxSourceLength, ignoreNonReducableTranslationEquivalents);
		this.hatPropertiesFileName = hatPropertiesFileName;
		this.statisticsData.setWriteHATProperties(true);
		this.writheHATProperties = true;

	}
	
	public EbitgTreeStatisticsMultiThread(int maxAllowedInferencesPerNode,int maxSourceLength,Pair<Integer> minMaxSentencePairNums,InputStringsEnumeration<AlignmentStringTriple> inputStringEnumeration, String hatPropertiesFileName, int maxSentenceLength, int outputLevel,BinarizationScores binarizationScoresToCopy, BinarizationScores nodeExtractionScoresToCopy, boolean ignoreNonReducableTranslationEquivalents) 
	{	
		this(maxAllowedInferencesPerNode,maxSourceLength, minMaxSentencePairNums, inputStringEnumeration,hatPropertiesFileName,maxSentenceLength,  outputLevel, ignoreNonReducableTranslationEquivalents);
		this.statisticsData.binarizationScores = BinarizationScores.createBinarizationScores(binarizationScoresToCopy);
		this.statisticsData.nodeExtractionScores = BinarizationScores.createBinarizationScores(nodeExtractionScoresToCopy);
		this.statisticsData.prepareForSTDComputation();
		this.statisticsData.setComputeSTDs(true);
		this.statisticsData.setWriteHATProperties(false);
		this.writheHATProperties = false;
	}	
	
	protected void doBeforeLooping() throws IOException
	{
		hatPropertiesWriter = null;
		// Write the header with labels of the properties to the csv file
		if(this.writheHATProperties)
		{	
			hatPropertiesWriter = new BufferedWriter(new FileWriter(hatPropertiesFileName));
			hatPropertiesWriter.write(HATProperties.propertiesHeader());
		}
	}
	
	protected void performCorpusComputation(AlignmentStringTriple input, int sentencePairNum)
	{
		EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(input.getSourceString(),input.getTargetString(),input.getAlignmentString(),getMaxAllowedInferencesPerNode(),USE_GREEDY_NULL_BINDING,USE_CACHING);
		boolean success = chartBuilder.findDerivationsAlignment();
		this.statisticsData.incrementNumSentencePairs();
		
		// update the statistics for all the statistics entries
		this.statisticsData.updateStatistics(chartBuilder,hatPropertiesWriter,sentencePairNum,  success);
		
		chartBuilder = null;

	}
	
	protected void doAfterLooping() throws IOException
	{
		if(hatPropertiesWriter != null)
		{	
				hatPropertiesWriter.close();
		}	
	}
	
	@Override
	public EbitgTreeStatisticsMultiThread  call() 
	{
		performLoopCorpusComputation();
		return this;
	}
		
}