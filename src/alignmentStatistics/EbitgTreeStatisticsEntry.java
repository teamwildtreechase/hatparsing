/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;

import extended_bitg.EbitgTreeState.TreeType;

public class EbitgTreeStatisticsEntry 
{
	public int numPureBITTs, numNullBITTs, numPurePETs, numNullPETs, numPureHATs, numNullHATs;
	private int maxSourceLengthAtomicFragment;
	private ArrayList<Integer> treeCountsForMaxBranchingFactors;

	/**
	 * Default constructor, initializes all the counts to 0, set maxSourceLenthAtomicFragment to 1
	 */
	public EbitgTreeStatisticsEntry()
	{
		this(1);
	}
	
	public EbitgTreeStatisticsEntry(int maxSourceLengthAtomicFragment)
	{
		this. maxSourceLengthAtomicFragment =  maxSourceLengthAtomicFragment;
		this.numPureBITTs = this.numNullBITTs = this.numPurePETs = this.numNullPETs = this.numPureHATs = this.numNullHATs =  0;
		this.treeCountsForMaxBranchingFactors = new ArrayList<Integer>();		
	}
	
	/**
	 *  Method that adds the statistics from another entry to the statistics of this entry.
	 *  To be used in mutlithreading, when the workload is divided over multiple independent threads.
	 * @param entry
	 */
	public void addTreeStatisticsEntryData(EbitgTreeStatisticsEntry entry)
	{
		this.numNullBITTs += entry.numNullBITTs;
		this.numNullHATs += entry.numNullHATs;
		this.numNullPETs += entry.numNullPETs;
		this.numPureBITTs += entry.numPureBITTs;
		this.numPureHATs += entry.numPureHATs;
		this.numPurePETs += entry.numPurePETs;
		this.maxSourceLengthAtomicFragment = Math.max(this.maxSourceLengthAtomicFragment, entry.maxSourceLengthAtomicFragment);
	
		// If necessary, increase the size of reeCountsForMaxBranchingFactors by adding extra elements 
		// to match the size of this in entry
		growArrayListToContainElementAtIndex(this.treeCountsForMaxBranchingFactors, entry.treeCountsForMaxBranchingFactors.size() - 1);
		
		// Add the counts in entry to the counts in this
		for(int i = 1; i <= entry.getMaxMaxBranching(); i++)
		{
			int newCount = this.getCountTreeWithMaxBracnching(i) + entry.getCountTreeWithMaxBracnching(i);
			this.treeCountsForMaxBranchingFactors.set(i-1, newCount);
		}
		
	}
	

	public int getMaxSourceLengthAtomicFragment()
	{
		return  maxSourceLengthAtomicFragment;
	}
	
	public synchronized  void updateStatistics(TreeType treeType, boolean hasNullAlignments)
	{
		if(treeType == TreeType.HAT)
		{
			if(hasNullAlignments)
			{
				this.numNullHATs++;
			}
			else
			{
				this.numPureHATs++;
			}
		
		}
		else if(treeType == TreeType.PET)
		{
			if(hasNullAlignments)
			{
				this.numNullPETs++;
			}
			else
			{
				this.numPurePETs++;
			}
			
		}
		
		else if(treeType == TreeType.BITT)
		{
			if(hasNullAlignments)
			{
				this.numNullBITTs++;
			}
			else
			{
				this.numPureBITTs++;
			}
			
		}
		
	}
	
	
	public static void growArrayListToContainElementAtIndex(ArrayList<Integer> list, int index)
	{
		if(list.size() > index)
		{
			return;
		}
		else
		{
			int extraElementsRequired = (index -list.size()) + 1;
						
			for(int i = 0; i < extraElementsRequired; i++)
			{
				list.add(0);
			}
		}
		
	}
	
	
	
	public void increaseCountTreeWithMaxBranching(int maxBranching)
	{
		
		int index =  maxBranching - 1; // We are not interested in a maxBranching of 0, so we start at 1
		
		// make sure the ArrayList is big enough, and increase its size if necessary
		growArrayListToContainElementAtIndex(this.treeCountsForMaxBranchingFactors, index);
		
		int newCount = this.treeCountsForMaxBranchingFactors.get(index) + 1;
		// set the new incremented count for trees with a maximal branching of maxBranching
		this.treeCountsForMaxBranchingFactors.set(index,newCount); 
	}
	
	public int getCountTreeWithMaxBracnching(int maxBranching)
	{
		int index =  maxBranching - 1; // We are not interested in a maxBranching of 0, so we start at 1
		
		if(index < this.treeCountsForMaxBranchingFactors.size())
		{
			return this.treeCountsForMaxBranchingFactors.get(index);
		}
		else
		{
			return 0;
		}	
	}
	
	public void printMaxBranchingCounts()
	{
		for(int i = 0; i < this.treeCountsForMaxBranchingFactors.size(); i++)
		{
			System.out.println("branching: " +  (i + 1) + " count: " + this.treeCountsForMaxBranchingFactors.get(i));  
		}
	}
	
	
	public int getMaxMaxBranching()
	{
		return this.treeCountsForMaxBranchingFactors.size();
	}
		
	
	
}
