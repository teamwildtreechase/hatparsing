/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

public abstract class ScoreNormalizer 
{
	protected final EbitgTreeStatisticsComputation treeStatisticsComputation;
	public abstract int computeScoreDenominator(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg);
	
	protected ScoreNormalizer(EbitgTreeStatisticsComputation treeStatisticsComputation)
	{
		this.treeStatisticsComputation = treeStatisticsComputation;
	}

}
