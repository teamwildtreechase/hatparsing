/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.FileNotFoundException;
import java.io.IOException;
import extended_bitg.EbitgChartBuilderProperties;

import util.ConfigFile;
import util.FileStatistics;
import util.Pair;

public class MultiThreadComputationParameters {
	private static final String TOTAL_SENTENCE_PAIRS_PROPERTY = "totalSentencePairs";
	private static final String TARGET_PARSE_FILE_NAME_PROPERTY = "targetParseFileName";
	private static final String SOURCE_FILE_NAME_PROPERTY = "sourceFileName";
	private static final String TARGET_FILE_NAME_PROPERTY = "targetFileName";
	private static final String ALIGNMENTS_FILE_NAME_PROPERTY = "alignmentsFileName";
	public static final String SOURCE_TAG_FILE_NAME_PROPERTY = "sourceTagFileName";
	public static final String TARGET_TAG_FILE_NAME_PROPERTY = "targetTagFileName";
	private final String sourceFileName;
	public final String targetFileName;
	final String alignmentsFileName;
	final String targetParseFileName;
	final ConfigFile theConfig;
	final int totalSentencePairs;
	protected final int numThreads;
	// final int sentencePairsPerThread;
	private final int maxAllowedInferencesPerNode;
	private final boolean useCaching;
	private final int maxSentenceLength;
	private int outputLevel;

	protected MultiThreadComputationParameters(String sourceFileName, String targetFileName, String alignmentsFileName, String targetParseFileName, ConfigFile theConfig, int totalSentencePairs,
	// int sentencePairsPerThread,
			int maxAllowedInferencesPerNode, boolean useCaching, int numThreads, int maxSentenceLength, int outputLevel) {
		this.sourceFileName = sourceFileName;
		this.targetFileName = targetFileName;
		this.alignmentsFileName = alignmentsFileName;
		this.targetParseFileName = targetParseFileName;
		this.theConfig = theConfig;
		this.totalSentencePairs = totalSentencePairs;
		// this.sentencePairsPerThread = sentencePairsPerThread;
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.maxSentenceLength = maxSentenceLength;
		this.setOutputLevel(outputLevel);
		this.numThreads = numThreads;
		this.useCaching = useCaching;
	}

	public static MultiThreadComputationParameters createMultiThreadComputationParameters(ConfigFile theConfig, int numThreads) {
		try {

			// Input locations
			String sourceFileName = extractSourceFileName(theConfig);
			String targetFileName = extractTargetFileName(theConfig);
			String alignmentsFileName = extractAlignmentsFileName(theConfig);
			String targetParseFileName = extractTargetParseFileName(theConfig);
			int outputLevel = extractOutputLevel(theConfig);
			int totalSentencePairs = extractTotalSentencePairs(theConfig, alignmentsFileName);

			int maxAllowedInferencesPerNode = extractMaxAllowedInferencesPerNode(theConfig);
			int maxSentenceLength = theConfig.getIntIfPresentOrReturnDefault("maxSentenceLength", Integer.MAX_VALUE);

			String useCachingString = theConfig.getStringIfPresentOrReturnDefault(EbitgChartBuilderProperties.UseCachingProperty, "true");
			boolean useCaching = useCachingString.equals("true");

			return new MultiThreadComputationParameters(sourceFileName, targetFileName, alignmentsFileName, targetParseFileName, theConfig, totalSentencePairs, maxAllowedInferencesPerNode,
					useCaching, numThreads, maxSentenceLength, outputLevel);

		} catch (IOException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Something went wrong in reading the configfile or getting values from it");
	}

	public static MultiThreadComputationParameters createMultiThreadComputationParameters(String configFileName, int numThreads) {
		try {
			ConfigFile theConfig = new ConfigFile(configFileName);
			return createMultiThreadComputationParameters(theConfig, numThreads);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		throw new RuntimeException("Something went wrong in reading the configfile or getting values from it");
	}

	protected static String extractCorpusLocation(ConfigFile theConfig) {
		String result = theConfig.getValueWithPresenceCheck("corpusLocation");
		System.out.println("corpusLocation: " + result);
		return result;
	}

	protected static int extractMaxAllowedInferencesPerNode(ConfigFile theConfig) {
		return theConfig.getIntIfPresentOrReturnDefault("maxAllowedInferencesPerNode", 100000);
	}

	protected static String extractSourceFileName(ConfigFile theConfig) {
		return extractCorpusLocation(theConfig) + theConfig.getValueWithPresenceCheck(SOURCE_FILE_NAME_PROPERTY);
	}

	protected static String extractTargetFileName(ConfigFile theConfig) {
		return extractCorpusLocation(theConfig) + theConfig.getValueWithPresenceCheck(TARGET_FILE_NAME_PROPERTY);
	}

	protected static String extractAlignmentsFileName(ConfigFile theConfig) {
		return extractCorpusLocation(theConfig) + theConfig.getValueWithPresenceCheck(ALIGNMENTS_FILE_NAME_PROPERTY);
	}

	protected static String extractTargetParseFileName(ConfigFile theConfig) {
		return extractCorpusLocation(theConfig) + theConfig.getValue(TARGET_PARSE_FILE_NAME_PROPERTY);
	}

	public String getSourceTagFileName() {
		return extractCorpusLocation(this.theConfig) + this.theConfig.getValueWithPresenceCheck(SOURCE_TAG_FILE_NAME_PROPERTY);
	}

	public String getTargetTagFileName() {
		return extractCorpusLocation(this.theConfig) + this.theConfig.getValueWithPresenceCheck(TARGET_TAG_FILE_NAME_PROPERTY);
	}

	protected static int extractTotalSentencePairs(ConfigFile theConfig, String alignmentsFileName) throws IOException {
		int result = 0;

		if (theConfig.hasValue(TOTAL_SENTENCE_PAIRS_PROPERTY)) {
			String totalSentencePairsString = theConfig.getValue(TOTAL_SENTENCE_PAIRS_PROPERTY);
			result = Integer.parseInt(totalSentencePairsString);
		} else {
			result = FileStatistics.countLines(alignmentsFileName);
		}
		return result;
	}

	protected static int extractOutputLevel(ConfigFile theConfig) {
		int result = 0;
		String outputLevelString = theConfig.getValue("outputLevel");
		if (outputLevelString != null) {
			result = Integer.parseInt(outputLevelString);
		}
		return result;
	}

	protected static Pair<Integer> computeSentencePairsPerThreadAndNewNumThreads(int totalSentencePairs, int numThreads) {
		// Compute number sentence pair per threads
		int newNumThreads = numThreads;
		int sentencePairsPerThread = totalSentencePairs / numThreads;
		// Add one if an exact devision over the threads is not possible, and
		// some
		// would be left otherwise
		if ((totalSentencePairs % newNumThreads) > 0) {
			sentencePairsPerThread += 1;

			// Recompute the new number of threads after increasing the number
			// of
			// sentence pairs per thread
			newNumThreads = (totalSentencePairs / sentencePairsPerThread);

			// As we increased the sentence pairs per thread, the
			// totalSentencePairs may
			// no longer have fit in the number of threads. We then need to add
			// an extra
			// thread to assure all sentence pairs remain covered.
			if ((newNumThreads * sentencePairsPerThread) < totalSentencePairs) {
				newNumThreads++;
			}
		}
		// System.out.println("totalSentencePairs : " + totalSentencePairs );
		// System.out.println("sentencePairsPerThread : " +
		// sentencePairsPerThread);
		// System.out.println("newNumTrheads: " + newNumThreads);
		return new Pair<Integer>(sentencePairsPerThread, newNumThreads);
	}

	public int getMaxAllowedInferencesPerNode() {
		return maxAllowedInferencesPerNode;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public String getTargetFileName() {
		return targetFileName;
	}

	public String getAlignmentsFileName() {
		return alignmentsFileName;
	}

	public void setOutputLevel(int outputLevel) {
		this.outputLevel = outputLevel;
	}

	public int getMaxSentenceLength() {
		return maxSentenceLength;
	}

	public int getOutputLevel() {
		return outputLevel;
	}

	public int getNumThreads() {
		return this.numThreads;
	}

	public boolean getUseCaching() {
		return useCaching;
	}

	public String getTargetParseFileNameChecked() {
		if (targetParseFileName.endsWith("null")) {
			throw new RuntimeException("Error : \"targetParseFileName\" not well defined in parameters. Please make sure this value is properly defined in the config file");
		}
		return targetParseFileName;
	}

	public String getTargetParseFileName() {
		return targetParseFileName;
	}

}
