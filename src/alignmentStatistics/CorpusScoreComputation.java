/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.List;

import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;

/**
 * Abstract lass that computes the scores defined over the entire corpus.
 * The concrete implementing classes have in common that they use an 
 * aggregated  "summedPossibleTotal" which is used to normalize the scores
 * that are computed at the end of computation 
 * @author gemaille
 *
 */
public abstract class CorpusScoreComputation extends ScoreComputation implements ScoreComputer
{
	protected int summedPossibleTotal;

	protected CorpusScoreComputation(List<List<Double>> basicScores,
			ScoreNormalizer scoreNormalizer,
			ScoreDerivativeComputation scoreDerivativeComputation,
			EbitgTreeStatisticsComputation treeStatisticsComputation) {
		super(basicScores, scoreNormalizer, scoreDerivativeComputation,
				treeStatisticsComputation);
	}
	
	/**
	 * Copy constructor
	 * @param toCopy
	 */
	protected  CorpusScoreComputation(CorpusScoreComputation toCopy)
	{
		super(toCopy);
	}
	
	
	public final void addScores(CorpusScoreComputation scoreComputation)
	{
		super.addScores(scoreComputation);
		this.summedPossibleTotal += scoreComputation.summedPossibleTotal;
	}
	
	@Override
	public double computeScore(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg,
			int maxSourceLengthAtomicFragment, int maxBranchingFactor) 
	{
		return computeFinalScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor, this);
	}

	protected final void updateSummedPossibleTotal(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg)
	{
		this.summedPossibleTotal += this.scoreNormalizer.computeScoreDenominator(tg);
	}
	
	public final void computeCorpusScores()
	{
		System.out.println(">>>CorpusScoreComputation.computeCorpusScores - this.summedPossibleTotal :  " + this.summedPossibleTotal);
		FinalScoresComputation.computeCorpusBinarizationScores(this.basicScores,this.summedPossibleTotal);
	}

	@Override
	public final List<Double> getDataRowToRender(int index) 
	{
		List<Double> result = this.basicScores.get(index);
		return result;
	}
	
	protected abstract  List<List<Double>>  computeSentenceScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg);
	
	@Override
	public void updateScores(EbitgTreeGrower<? extends EbitgLexicalizedInference,? extends EbitgTreeState<?,?>> tg) 
	{
		EbitgTreeStatisticsData.addArrays(this.basicScores, computeSentenceScores(tg));
		updateSummedPossibleTotal(tg);
	}
}
