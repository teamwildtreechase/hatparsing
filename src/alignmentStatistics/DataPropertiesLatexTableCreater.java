/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataPropertiesLatexTableCreater {

	final List<String> languagePairNames;
	final List<DataProperties> languagePairProperties;
	final int languagePairPerTable;

	private DataPropertiesLatexTableCreater(List<String> languagePairNames, List<DataProperties> languagePairProperties, int languagePairsPerTable) {
		this.languagePairNames = languagePairNames;
		this.languagePairProperties = languagePairProperties;
		this.languagePairPerTable = languagePairsPerTable;
	}

	public static DataPropertiesLatexTableCreater createDataPropertiesLatexTableCreater(List<String> languagePairNames, List<String> fileNames, int languagePairsPerTable) {
		return new DataPropertiesLatexTableCreater(languagePairNames, createLanguagePairProperties(fileNames), languagePairsPerTable);
	}

	private static List<DataProperties> createLanguagePairProperties(List<String> fileNames) {
		List<DataProperties> result = new ArrayList<DataProperties>();

		for (String fileName : fileNames) {
			result.add(getPropertiesFromFile(fileName));
		}

		return result;
	}

	private static DataProperties getPropertiesFromFile(String fileName) {
		DataProperties result = new DataProperties();

		try {

			BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

			boolean informationLinesStarted = false;
			String line;
			while ((line = fileReader.readLine()) != null) {
				// end of the relevant information reached
				if (line.contains("</table")) {
					break;
				}

				if (informationLinesStarted) {
					result.addDataProperty(line);
				} else if (line.contains("<table")) {
					informationLinesStarted = true;
				} else {
					continue;
				}
			}
			fileReader.close();
			return result;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not find the file");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not read from the file");
		}
	}

	private void writePropertiesTable(int languagePairStartIndex) {
		System.out.println(createTableHeaderString());
		System.out.println(createLanguagePairNamesLine(languagePairStartIndex));
		for (int i = 0; i < this.languagePairProperties.get(0).getNumProperties(); i++) {
			System.out.println("\t" + createPropertiesLine(i, languagePairStartIndex));
		}
		System.out.println(createTableFooterString());
	}

	public void writePropertyLines() {

		for (int languagePairStartIndex = 0; languagePairStartIndex < this.languagePairProperties.size(); languagePairStartIndex += this.languagePairPerTable) {
			writePropertiesTable(languagePairStartIndex);
		}
	}

	private String createLanguagePairNamesLine(int languagePairStartIndex) {
		String result = "{\\bf Language Pairs}";

		for (int i = languagePairStartIndex; i < Math.min(languagePairStartIndex + this.languagePairPerTable, this.languagePairNames.size()); i++) {
			String languagePair = this.languagePairNames.get(i);
			result += " & " + "{\\bf " + languagePair + "}";
		}
		result += " \\\\ \\hline";
		return result;
	}

	private String createTableHeaderString() {
		String result = "\\begin{center}";
		result += "\n \t \\begin{tabular}{ | p{5cm} |";

		for (int i = 0; i < this.languagePairProperties.get(0).getNumProperties(); i++) {
			result += " l |";
		}
		result += "}\n\\hline";

		return result;
	}

	private String createTableFooterString() {
		String result = "\t \\hline \n \t \\end{tabular}\n\\end{center}";
		return result;
	}

	private String createPropertiesLine(int index, int languagePairStartIndex) {
		String result = "";
		String propertyName = this.languagePairProperties.get(0).getDataProperty(index).getPropertyName();
		result += propertyName;
		// We only want a limited number of language pairs per table
		for (int i = languagePairStartIndex; i < Math.min(languagePairStartIndex + this.languagePairPerTable, this.languagePairNames.size()); i++) {
			DataProperties dataProperties = this.languagePairProperties.get(i);
			String valueString = dataProperties.getDataProperty(index).getPropertyValue();
			result += " & " + valueString;
		}
		result += "\\\\ \\hline";
		return result;
	}

	public static void main(String[] args) {
		List<String> languagePairNames = new ArrayList<String>();
		List<String> fullFilePaths = new ArrayList<String>();

		if (args.length != 3) {
			for (int i = 0; i < args.length; i++) {
				System.out.println("arg[" + i + "]" + args[i]);
			}

			System.out.println("Usage: java - jar dataPropertiesLatexTableCreator  BaseFolder LanguagePairsPerTable CorpusPropertiesFileStartPrefix");
			System.exit(0);
		}

		String baseFolder = args[0];
		int languagePairPerTable = Integer.parseInt(args[1]);
		MultiFolderFilePathListCreater multiFolderFilePathListCreater = MultiFolderFilePathListCreater.createCorpusPropertiesPathListCreater(args[2]);
		languagePairNames = multiFolderFilePathListCreater.getLanguagePairStingsFromBaseFolderPath(baseFolder);
		fullFilePaths = multiFolderFilePathListCreater.getPropertiesFilePathsFromBaseFolderPath(baseFolder);

		DataPropertiesLatexTableCreater propertiesLatexTableCreater = DataPropertiesLatexTableCreater.createDataPropertiesLatexTableCreater(languagePairNames, fullFilePaths, languagePairPerTable);
		propertiesLatexTableCreater.writePropertyLines();
	}

}
