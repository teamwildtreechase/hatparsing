/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import util.ConfigFile;
import util.FileLineIterator;
import alignmentStatistics.AlignmentChunk.AlignmentType;

import static alignmentStatistics.SentencePairAlignmentChunking.createSentencePairAlignmentChunkingZerosSeparate;

public class AlignmentStatistics {
	public NumberFormat nf;
	public NumberFormat percentageFormat;
	public final static int maxAlignmentFertility = 40;

	private int numSentences;
	private int totalSourceWords;
	private int totalTargetWords;
	private ArrayList<Integer> zeroToZeroContainingSentences;

	private int[][] alignmentCountsTable;
	private int[][] mToNFullyConnectedAlignmentCountTable;
	private int[][] mToNPartiallyConnectedAlignmentCountTable;

	public enum NormalizationType {
		None, SourceSide, TargetSide
	}

	public AlignmentStatistics(NumberFormat nf, NumberFormat percentageFormat,
			ArrayList<Integer> zeroToZeroContainingSentences,
			int[][] alignmentCountsTable,
			int[][] mToNFullyConnectedAlignmentCountTable,
			int[][] mToNPartiallyConnectedAlignmentCountTable) {
		this.nf = nf;
		this.percentageFormat = percentageFormat;
		this.zeroToZeroContainingSentences = zeroToZeroContainingSentences;
		this.alignmentCountsTable = alignmentCountsTable;
		this.mToNFullyConnectedAlignmentCountTable = mToNFullyConnectedAlignmentCountTable;
		this.mToNPartiallyConnectedAlignmentCountTable = mToNPartiallyConnectedAlignmentCountTable;
	}

	public static AlignmentStatistics createAlignmentStatistics() {
		ArrayList<Integer> zeroToZeroContainingSentences = new ArrayList<Integer>();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumIntegerDigits(1);
		nf.setMaximumFractionDigits(6);
		nf.setMinimumFractionDigits(6);

		NumberFormat percentageFormat = NumberFormat.getPercentInstance();
		percentageFormat.setMinimumIntegerDigits(2);
		percentageFormat.setMaximumIntegerDigits(2);
		percentageFormat.setMaximumFractionDigits(4);
		percentageFormat.setMinimumFractionDigits(4);

		int[][] alignmentCountsTable = new int[maxAlignmentFertility + 1][maxAlignmentFertility + 1];
		int[][] mToNFullyConnectedAlignmentCountTable = new int[maxAlignmentFertility - 1][maxAlignmentFertility - 1];
		int[][] mToNPartiallyConnectedAlignmentCountTable = new int[maxAlignmentFertility - 1][maxAlignmentFertility - 1];

		AlignmentStatistics alignmentStatistics = new AlignmentStatistics(nf,
				percentageFormat, zeroToZeroContainingSentences,
				alignmentCountsTable, mToNFullyConnectedAlignmentCountTable,
				mToNPartiallyConnectedAlignmentCountTable);
		return alignmentStatistics;
	}

	public void addAlignmentTypeCounting(SentencePairAlignmentChunking chunking) {
		numSentences++;
		totalSourceWords += chunking.getSourceLength();
		totalTargetWords += chunking.getTargetLength();

		boolean hasZeroToOneAlignments, hasOneToZeroAlignments;
		hasZeroToOneAlignments = hasOneToZeroAlignments = false;

		List<AlignmentChunk> alignmentChunks = chunking.getAlignmentChunks();
	
		for (AlignmentChunk c : alignmentChunks) {
			// System.out.println("Processing chunk " + i);
			AlignmentType type = c.determineAlignmentType();

			updateAlignmentCountTable(c);

			switch (type) {

			case OneToZero:
				hasOneToZeroAlignments = true;
				break;
			case ZeroToOne:
				hasZeroToOneAlignments = true;
				break;

			/*
			 * case OneToOne: this.oneToOneAlignments++; break;
			 * 
			 * case OneToMany: this.oneToManyAlignments++; break; case
			 * ManyToOne: this.manyToOneAlignments++; break;
			 */
			case ManyToManyFullConnected:
				updateAlignmentCountMtoNTable(c,
						mToNFullyConnectedAlignmentCountTable);
				break;
			case ManyToManyPartialConnected:
				updateAlignmentCountMtoNTable(c,
						mToNPartiallyConnectedAlignmentCountTable);
				break;
			default:
				// something wrong if default case is reached
				assert (false);

			}
		}

		if (hasZeroToOneAlignments && hasOneToZeroAlignments) {
			zeroToZeroContainingSentences.add(numSentences);
		}

	}

	public void updateAlignmentCountTable(AlignmentChunk c) {
		int m = c.getSourcePositions().size();
		int n = c.getTargetPositions().size();

		alignmentCountsTable[m][n]++;
	}

	public void updateAlignmentCountMtoNTable(AlignmentChunk c, int[][] table) {

		int m = c.getSourcePositions().size();
		int n = c.getTargetPositions().size();

		assert (m > 1);
		assert (n > 1);
		table[m - 2][n - 2]++;
	}

	public String oneToManyStatisticsString() {
		String result = "";

		// System.out.println(" alignmentCountsTable.length :" +
		// alignmentCountsTable.length);
		// System.out.println(" alignmentCountsTable[i].length" +
		// alignmentCountsTable[0].length);

		for (int i = 2; i < alignmentCountsTable[0].length; i++) {
			result += "| 1-" + i + " : " + alignmentCountsTable[1][i];
		}

		return result;
	}

	public String manyToOneStatisticsString() {
		String result = "";

		for (int i = 2; i < alignmentCountsTable.length; i++) {
			result += "| " + i + "-1" + " : " + alignmentCountsTable[i][1];
		}

		return result;
	}

	private String manyToManyStatisticsString(int[][] alignmentCounts,
			NormalizationType normalizationType) {
		return tableStatisticsString(alignmentCounts, normalizationType, 2, 2);
	}

	private String tableStatisticsString(int[][] alignmentCounts,
			NormalizationType normalizationType, int mStart, int nStart) {
		String result = "\n\t";

		for (int n = nStart; n < alignmentCounts.length + nStart; n++) {
			result += "  n = " + numString(n, 2) + "  ";
		}
		result += "\n";

		for (int m = mStart; m < alignmentCounts.length + mStart; m++) {
			result += "\n" + "m = " + numString(m, 2) + "  ";
			for (int n = nStart; n < alignmentCounts.length + nStart; n++) {
				String numString;
				double alignmentChance;

				switch (normalizationType) {
				case None:
					numString = numString(alignmentCounts[n - nStart][m
							- mStart], 8);
					break;
				case SourceSide:
					alignmentChance = (((double) alignmentCounts[n - nStart][m
							- mStart] * m) / totalSourceWords);
					numString = percentageFormat.format(alignmentChance);
					break;
				case TargetSide:
					alignmentChance = (((double) alignmentCounts[n - nStart][m
							- mStart] * n) / totalTargetWords);
					numString = percentageFormat.format(alignmentChance);
					break;
				default:
					numString = numString(alignmentCounts[n - nStart][m
							- mStart], 8);
					break;

				}

				result += numString + "  ";
			}
		}
		return result;
	}

	private String overviewStatisticsString(NormalizationType normalizationType) {
		return tableStatisticsString(alignmentCountsTable, normalizationType,
				0, 0);
	}

	/**
	 * Method that generates a string for a integer number with a specified
	 * length
	 * 
	 * @param number
	 *            The number for which the String is generated
	 * @param length
	 *            The desired length of the String generated
	 * @return A String for the number preceded by zeros if nescessary
	 */
	public static String numString(int number, int length) {

		String numString = "" + number;

		// deal with zero case
		int numCopy = number;

		// Deal with minus case: extra whitespace to add decreased by one
		if (numCopy < 0) {
			numCopy *= 10;
		}

		// Deal with zero case: if zero then work with 1 instead, if negative
		// work with the absolute of the negative value
		numCopy = Math.max(1, Math.abs(numCopy));

		int compareNum = (int) Math.pow(10, length - 1);

		// While smaller than the comparision number, add whitespace and
		// multiply by 10
		while (numCopy < compareNum) {
			// System.out.println("numCopy: " + numCopy);
			numString = " " + numString;
			numCopy *= 10;
		}
		return numString;
	}

	public double oneToZeroCount(NormalizationType normalizationType) {
		double totalCount = alignmentCountsTable[1][0];

		if (normalizationType == NormalizationType.TargetSide) {
			System.out
					.println("WARNING: Normalizing the 1-0 alignments, which are inherently"
							+ " a source phenomenon with the target counts. This will not give a meaningfull probability!!!");
		}

		totalCount /= normalizationDevider(normalizationType);
		return totalCount;
	}

	public double zeroToOneCount(NormalizationType normalizationType) {
		double totalCount = alignmentCountsTable[0][1];

		if (normalizationType == NormalizationType.SourceSide) {
			System.out
					.println("WARNING: Normalizing the 0-1 alignments, which are inherently"
							+ " a target phenomenon with the source counts. This will not give a meaningfull probability!!!");
		}

		totalCount /= normalizationDevider(normalizationType);
		return totalCount;
	}

	public double oneToOneCount(NormalizationType normalizationType) {
		double totalCount = alignmentCountsTable[1][1];
		totalCount /= normalizationDevider(normalizationType);
		return totalCount;
	}

	public double mToOneCount(NormalizationType normalizationType) {
		return normalizedTableCount(alignmentCountsTable, normalizationType, 2,
				alignmentCountsTable.length - 1, 1, 1, 0, 0);
	}

	public double oneToNCount(NormalizationType normalizationType) {
		return normalizedTableCount(alignmentCountsTable, normalizationType, 1,
				1, 2, alignmentCountsTable.length - 1, 0, 0);
	}

	public double mToNCount(NormalizationType normalizationType) {
		return normalizedTableCount(alignmentCountsTable, normalizationType, 2,
				alignmentCountsTable.length - 1, 2,
				alignmentCountsTable.length - 1, 0, 0);
	}

	public double mToNFullyConnectedCount(NormalizationType normalizationType) {
		return normalizedTableCount(mToNFullyConnectedAlignmentCountTable,
				normalizationType, 2, alignmentCountsTable.length - 1, 2,
				alignmentCountsTable.length - 1, 2, 2);
	}

	public double mToNPartiallyConnectedCount(
			NormalizationType normalizationType) {
		return normalizedTableCount(mToNPartiallyConnectedAlignmentCountTable,
				normalizationType, 2, alignmentCountsTable.length - 1, 2,
				alignmentCountsTable.length - 1, 2, 2);
	}

	/**
	 * General function that computes a weighted count of a block of entries
	 * from alignmentsCountTable
	 * 
	 * @param normalizationType
	 * @param mMin
	 *            : the min value of m to start from
	 * @param mMax
	 *            : the max value of m to visit (inclusive)
	 * @param nMin
	 *            : the min value of n to start with
	 * @param nMax
	 *            : the max value of n to visit (inclusive)
	 * @param mOffset
	 *            : the value of m the table starts with (typically 0, but can
	 *            be otherwise)
	 * @param nOffset
	 *            : the value of n the table starts with typically 0, but can
	 * @return : the normalized sum of counts for the chosen block
	 */

	public double normalizedTableCount(int[][] countsTable,
			NormalizationType normalizationType, int mMin, int mMax, int nMin,
			int nMax, int mOffset, int nOffset) {
		double totalCount = 0;

		for (int m = mMin; m <= mMax; m++) {
			for (int n = nMin; n <= nMax; n++) {
				totalCount += countsTable[m - mOffset][n - nOffset]
						* normalizationMultiplier(m, n, normalizationType);
			}
		}

		totalCount /= normalizationDevider(normalizationType);
		return totalCount;
	}

	private int normalizationMultiplier(int m, int n,
			NormalizationType normalizationType) {
		switch (normalizationType) {
		case None:
			return 1;
		case SourceSide:
			return m;
		case TargetSide:
			return n;
		default:
			return 1;
		}
	}

	private int normalizationDevider(NormalizationType normalizationType) {
		switch (normalizationType) {
		case None:
			return 1;
		case SourceSide:
			return totalSourceWords;
		case TargetSide:
			return totalTargetWords;
		default:
			return 1;
		}
	}

	public String toString() {
		double summedFractions = oneToZeroCount(NormalizationType.SourceSide)
				+ oneToOneCount(NormalizationType.SourceSide)
				+ oneToNCount(NormalizationType.SourceSide)
				+ mToOneCount(NormalizationType.SourceSide)
				+ mToNCount(NormalizationType.SourceSide);

		double summedCounts = oneToZeroCount(NormalizationType.None)
				+ oneToOneCount(NormalizationType.None)
				+ oneToNCount(NormalizationType.None)
				+ mToOneCount(NormalizationType.None)
				+ mToNCount(NormalizationType.None);

		String result = "";

		result += "\n----------------------------------------------------------";
		result += "\nNumber of sentences: " + numSentences;
		result += "\nTotal source words: " + totalSourceWords;
		result += "\nTotal target words: " + totalTargetWords;
		result += "\nZero to Zero Alignment containing sentences (zero alignments in both source and target): "
				+ zeroToZeroContainingSentences.size();
		result += "\n 1-null: "
				+ (int) oneToZeroCount(NormalizationType.None)
				+ " \\ (Source side conditional chance) "
				+ percentageFormat
						.format(oneToZeroCount(NormalizationType.SourceSide));
		result += "\n null-1: "
				+ (int) zeroToOneCount(NormalizationType.None)
				+ " \\ (Target side conditional chance) "
				+ percentageFormat
						.format(zeroToOneCount(NormalizationType.TargetSide))
				+ "(Not part of other statistics for source words!)";
		result += "\n----------------------------------------------------------";
		result += "\n 1-1: "
				+ oneToOneCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(oneToOneCount(NormalizationType.SourceSide));
		result += "\n----------------------------------------------------------";
		result += "\n 1-n: "
				+ (int) oneToNCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(oneToNCount(NormalizationType.SourceSide));
		result += "\n m-1: "
				+ (int) mToOneCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(mToOneCount(NormalizationType.SourceSide));
		result += "\n----------------------------------------------------------";
		result += "\n m-n:  "
				+ (int) mToNCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(mToNCount(NormalizationType.SourceSide));
		result += "\n m-n Fully Connected : "
				+ (int) mToNFullyConnectedCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(mToNFullyConnectedCount(NormalizationType.SourceSide))
				+ "\t m-n Partial Connected : "
				+ (int) mToNPartiallyConnectedCount(NormalizationType.None)
				+ " \\ "
				+ percentageFormat
						.format(mToNPartiallyConnectedCount(NormalizationType.SourceSide));
		result += "\n (Sanity Check) Summed fractions : " + summedFractions;
		result += "\n (Sanity Check) Summed counts : " + summedCounts;
		result += "\n----------------------------------------------------------";
		result += "\n m-1 and 1-n counts:";
		result += "\n" + oneToManyStatisticsString();
		result += "\n" + manyToOneStatisticsString();
		result += "\n----------------------------------------------------------";
		result += "\n----------------------------------------------------------";
		result += "\n----------------------------------------------------------";
		result += "\n------Absolute Counts Tables-----";
		result += "\n" + overviewStatisticsString(NormalizationType.None);
		result += "\n----------------------------------------------------------";
		result += "\n Fully connected m-n alignments: "
				+ manyToManyStatisticsString(
						mToNFullyConnectedAlignmentCountTable,
						NormalizationType.None);
		result += "\n----------------------------------------------------------";
		result += "\n Partially connected m-n alignments: "
				+ manyToManyStatisticsString(
						mToNPartiallyConnectedAlignmentCountTable,
						NormalizationType.None);
		result += "\n----------------------------------------------------------";
		result += "\n----------------------------------------------------------";
		result += "\n----------------------------------------------------------";
		result += "\n----------------------------------------------------------";
		result += "\n------Source Relative Probabilities Tables-----";
		result += "\n" + overviewStatisticsString(NormalizationType.SourceSide);
		result += "\n----------------------------------------------------------";
		result += "\n Fully connected m-n alignments: "
				+ manyToManyStatisticsString(
						mToNFullyConnectedAlignmentCountTable,
						NormalizationType.SourceSide);
		result += "\n----------------------------------------------------------";
		result += "\n Partially connected m-n alignments: "
				+ manyToManyStatisticsString(
						mToNPartiallyConnectedAlignmentCountTable,
						NormalizationType.SourceSide);

		result += "\n\n\n" + zeroToZeroContainingSentencesListString();
		return result;
	}

	public String zeroToZeroContainingSentencesListString() {
		String result = "Zero to Zero Containing Sentence Pairs :\n";

		for (int sentenceNumber : zeroToZeroContainingSentences) {
			result += sentenceNumber + ",";
		}
		result += "\n";
		return result;
	}

	public static AlignmentStatistics generateCorpusAlignmentStatistics(
			String sourceFileName, String targetFileName,
			String alignmentFileName, int maxSentenceNum) {
		AlignmentStatistics as = createAlignmentStatistics();

		FileLineIterator sourceIterator = new FileLineIterator(sourceFileName);
		FileLineIterator targetIterator = new FileLineIterator(targetFileName);
		FileLineIterator alignmentIterator = new FileLineIterator(
				alignmentFileName);

		int i = 1;

		while (sourceIterator.hasNextLine() && targetIterator.hasNextLine()
				&& alignmentIterator.hasNextLine()
				&& ((maxSentenceNum < 0) || (i < (maxSentenceNum + 1)))) {
			System.out.println(">>Processing sentence " + i);

			String sourceString = sourceIterator.nextLine();
			String targetString = targetIterator.nextLine();
			String alignmentString = alignmentIterator.nextLine();

			SentencePairAlignmentChunking sac = createSentencePairAlignmentChunkingZerosSeparate(
					alignmentString, sourceString, targetString);
			// Perform the chunking of the sentence alignment pair
			sac.performChunking();
			// add the alignment counts for the chunked sentence alignment pair
			as.addAlignmentTypeCounting(sac);

			i++;
		}

		return as;
	}

	public static void main(String[] args) 
	{
		if(args.length != 2)
		{
			System.out.println("Usage: AlignmentStatistics ConfigFileLocation OutputFileLocation");
			System.exit(0);
		}
		
		try 
		{
			ConfigFile configFile = new ConfigFile(args[0]);
			String resultsFileName = args[1];

			AlignmentStatistics as = AlignmentStatistics
					.generateCorpusAlignmentStatistics(configFile.getSourceFilePath(),
							configFile.getTargetFilePath(),configFile.getAlignmentsFilePath(), 5000);

			System.out.println(">>>> Corpus Statistics:\n\n" + as);
			// Write the generated statistics to a file
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					resultsFileName));
			bw.write(">>>> Corpus Statistics:\n\n" + as);
			bw.close();
		
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}
}
