/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.io.FileNotFoundException;

import util.ConfigFile;

public class LanguageAbbreviationLookupTable 
{
	ConfigFile mappingTable;
	private String languageAbbreviation;
	
	private LanguageAbbreviationLookupTable(ConfigFile mappingTable)
	{
		this.mappingTable = mappingTable;
	}
	
	public static LanguageAbbreviationLookupTable createLanguageAbbreviationLookupTable(String mappingFileName)
	{
		ConfigFile mappingTable;
		try 
		{
			mappingTable = new ConfigFile(mappingFileName);
			return new LanguageAbbreviationLookupTable(mappingTable);
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}	  
	}
	
	public String getLanguageForAbbreviation(String languageAbbreviaiton)
	{
		return mappingTable.getStringIfPresentOrReturnDefault(languageAbbreviation, languageAbbreviation);
	}
	
	public String getLanguagePairForAbbreviationPair(String abbreviationPair)
	{
		String[] parts = abbreviationPair.split("-");
		
		String result =  mappingTable.getStringIfPresentOrReturnDefault(parts[0],parts[0]);
		result += "-" + mappingTable.getStringIfPresentOrReturnDefault(parts[1],parts[1]);
		return result;
	}
	
}
