/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentStatistics;

import java.util.ArrayList;
import java.util.List;

import bitg.Pair;


public class DataProperties 
{
	private final List<DataProperty> dataProperties;

	public DataProperties()
	{
		this.dataProperties = new ArrayList<DataProperty>();
	}
	
	public void addDataProperty(String line)
	{
		if(!(line.trim()).equals(""))
		{
			dataProperties.add(createDataProperty(line));
		}
	}
	
	private DataProperty createDataProperty(String line)
	{
		List<Pair<String,Double>>  nameValuePairs = getNameValuePairs(line);
		
		if(nameValuePairs.size() == 1)
		{
			return createIntegerDataProperty(nameValuePairs);
		}
		else
		{
			return createDoubleSTDDataProperty(nameValuePairs);
		}
	}
	
	private DataProperty createIntegerDataProperty(List<Pair<String,Double>>  nameValuePairs)
	{
		Pair<String,Double> propertyPair = nameValuePairs.get(0);
		IntegerDataProperty dataProperty = new IntegerDataProperty(propertyPair.first, propertyPair.last.intValue());
		return dataProperty;
	}
	
	private DoubleSTDDataProperty createDoubleSTDDataProperty(List<Pair<String,Double>>  nameValuePairs)
	{
		Pair<String,Double> propertyPair1 = nameValuePairs.get(0);
		Pair<String,Double> propertyPair2 = nameValuePairs.get(1);
		DoubleSTDDataProperty dataProperty = new DoubleSTDDataProperty(propertyPair1.first, propertyPair1.last, propertyPair2.last);
		return dataProperty;
	}
	
	private List<Pair<String,Double>> getNameValuePairs(String line)
	{
		List<Pair<String,Double>> result = new ArrayList<Pair<String,Double>>();
		
		String propertyName = "";
		// Split on whitespace
		String[] parts = line.split("\\s+");
		
		//int i = 0;
		for(String part : parts)
		{
			//System.out.println("part " + i +" : " + part);
			
			if(isParsableToNumber(part))
			{
				propertyName = propertyName.trim();
				propertyName = propertyName.substring(0, propertyName.length() - 1);
				Pair<String,Double> nameValuePair = new Pair<String, Double>(propertyName, Double.parseDouble(part));
				result.add(nameValuePair);
				propertyName = "";
			}
			else
			{
				propertyName += part + " ";
			}
			//i++;
		}
		
		return result;
	}
	
	public DataProperty getDataProperty(int index)
	{
		return this.dataProperties.get(index);
	}
	
	public int getNumProperties()
	{
		return this.dataProperties.size();
	}
	
	public boolean isParsableToNumber(String string)
	{
		try
		{
			Double.parseDouble(string);
			return true;
		}
		catch(NumberFormatException nfe)
		{
			return false;
		}
	}
	
}
