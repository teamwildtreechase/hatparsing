/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;

public class TreebankLoader<T> extends  FileActionPerformer
{	
	
	private final String menuString;
	private static final char MenuAccellerator = 'O';
	private TreebankLoaderInterface<T> treebankLoaderInterface;
	
	private TreebankLoader(FileChoosingHandler fileChoosingHandler, TreebankLoaderInterface<T>  TreebankLoaderInterface, String menuString)
	{
		super(fileChoosingHandler);
		this. treebankLoaderInterface =  TreebankLoaderInterface;
		this.menuString = menuString;
	}
	
	public static <T> TreebankLoader<T> createTreebankLoader(FileChoosingHandler fileChoosingHandler, TreebankLoaderInterface<T>  treebankLoaderInterface, String menuString)
	{
		return new TreebankLoader<T>(fileChoosingHandler, treebankLoaderInterface, menuString);
	}
	
	public void loadInput(File TreebankSentencesFile) 
	{
		loadTreebank(TreebankSentencesFile,treebankLoaderInterface, true);
		
	}

	public  static <T> boolean loadTreebank(File treebankFile, TreebankLoaderInterface<T>  treebankLoaderInterface, boolean resetSpinner)
	{
		System.out.println("\n Entered loadTreebank...");

		try 
		{
			treebankLoaderInterface.loadTreebankAndSetSpinner(treebankFile,resetSpinner);
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			System.out.println("loadTreebank failed");
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	@Override
	public String getLabel() 
	{
		return menuString;
	}

	@Override
	public char getAccellerator() 
	{
		return TreebankLoader.MenuAccellerator;
	}

	@Override
	public void performFileAction(File file) 
	{
		loadTreebank(file,treebankLoaderInterface, true);
		
	}


		
}