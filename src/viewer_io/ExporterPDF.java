/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.Graphics2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.DefaultFontMapper;
//import com.itextpdf.text.pdf.DefaultFontMapper.BaseFontParameters;
import com.itextpdf.text.pdf.BaseFont;
//import com.itextpdf.text.pdf.FontMapper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;



public class ExporterPDF  extends Exporter
{

	private ExporterPDF(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel, String label, char accellerator)
	{
		super(fileChoosingHandler, panel, label, accellerator);
	}
	
	
	public static ExporterPDF createExporterPDF(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel, String label, char accellerator)
	{
		return new ExporterPDF(fileChoosingHandler, panel, label, accellerator);
	}
	
	public void performFileAction(File file)
	{
		 exportToPDF(file, panel); 
	}
	
	
	public static  DefaultFontMapper createFontMapper(String fontDirectoryName)
	{
		 DefaultFontMapper mapper = new DefaultFontMapper();
		    mapper.insertDirectory(fontDirectoryName);
		    String name;
		    Map<?, ?> map = mapper.getMapper();
		    for (Iterator<?> i = map.keySet().iterator(); i.hasNext();) 
		    {
		      name = (String) i.next();
		      System.out.println(name + ": "
		          + ((DefaultFontMapper.BaseFontParameters) map.get(name)).fontName);
		    }
		    
		    // This is a very essential part and costed me half a day to find out !!!
		    // See o.a. http://ignum.dl.sourceforge.net/project/jfreechart/2.%20Documentation/Version%200.8.1/jfreechart2pdf-v2.pdf
		    // and also http://itext-general.2136553.n4.nabble.com/About-defaultFontMapper-awtToPdf-td2156354.html
		    setTheBaseFontIdentityParametersWhitoutWhichUnicodeWillNotWorkAndYouWillGoScreaming(mapper);
		 
		  return mapper;  
	}
	
	private static void setTheBaseFontIdentityParametersWhitoutWhichUnicodeWillNotWorkAndYouWillGoScreaming(DefaultFontMapper mapper)
	{
		   DefaultFontMapper.BaseFontParameters pp = mapper.getBaseFontParameters("Arial Unicode MS");
		   if (pp!=null) 
		   {
			   System.out.println("BaseFontParameters are null => Set encoding to BaseFont.IDENTITY_H");
			   pp.encoding = BaseFont.IDENTITY_H;
		   }
	}
	
	
	public static void exportToPDF(File selectedFile, ExportablePanel panel) 
	{
		System.out.println("Export file to PDF : " + selectedFile.getAbsolutePath());
		
		// step 1: creation of a document-object
		if (selectedFile.exists()) selectedFile.delete();
		Document document = new Document(new Rectangle((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight()));
		try {
			// step 2:
			// we create a writer
			PdfWriter writer = PdfWriter.getInstance(
			// that listens to the document
					document,
					// and directs a PDF-stream to a file
					new FileOutputStream(selectedFile));
			// step 3: we open the document
			document.open();
			// step 4:
			// we create a template and a Graphics2D object that corresponds
			// with it
			PdfContentByte cb = writer.getDirectContent();
			//String fontsDirectory = "/usr/share/fonts/truetype";
			//String fontsDirectory = "/home/gmaillet/Desktop/Fonts";
			//String fontsDirectory = "./libraries/Fonts";
			//String fontsDirectory = "/home/gemaille/atv-bitg-workingdirectory/trunk/libraries/Fonts";
			//String fontsDirectory = "/usr/share/fonts/default/TrueType";

			
			// A fontmapper is required to map the Unicode Font Name of java to a font that actually exists 
			// on the system and that fully implements all required characters. The fontmapper maps the java 
			// fontname to a system font that is used in the pdfwriter
			//FontMapper fm = createFontMapper(fontsDirectory);
			//DefaultFontMapper fm = new DefaultFontMapper();
			//BaseFont unicode = BaseFont.createFont("/usr/share/fonts/truetype/unifont/unifont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			//fm.putName("Arial Unicode MS",  new BaseFontParameters("/usr/share/fonts/truetype/ARIALUNI.TTF"));
			//fm.putName("Arial Unicode MS",  new BaseFontParameters(unicode.)));
			
			PdfTemplate tp = cb.createTemplate((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight());
			// Gets a Graphics2D to print on. The graphics are translated to PDF commands as shapes. No PDF fonts will appear. 
		    Graphics2D g2 = tp.createGraphicsShapes((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight());
			
			//Graphics2D g2 = cb.createGraphics((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight(),fm );

			addUnicodeSupportingFont(g2);
			panel.render(g2);
			g2.dispose();
			cb.addTemplate(tp, 0, 0);
			
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
 
		// step 5: we close the document
		document.close();
	}

	
	  private static void addUnicodeSupportingFont(Graphics2D g2)
      {
               java.awt.Font font = new java.awt.Font("Arial Unicode MS", java.awt.Font.PLAIN, 12);
               g2.setFont(font);
      }


	@Override
	public String getLabel() 
	{
		return label;
	}


	@Override
	public char getAccellerator() 
	{
		return accellerator;
	}
	
}
