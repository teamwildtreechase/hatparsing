/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.Dimension;
import java.awt.Graphics2D;

public interface ExportablePanel 
{

	public Dimension getArea();
	public abstract void render(Graphics2D g2);
	
}
