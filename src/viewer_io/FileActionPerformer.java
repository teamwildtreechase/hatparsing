/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;


public abstract class FileActionPerformer extends ActionPerformer
{
	
	protected FileChoosingHandler fileChoosingHandler;
	
	protected  FileActionPerformer(FileChoosingHandler fileChoosingHandler)
	{
		this.fileChoosingHandler = fileChoosingHandler;
	}
	
	public void performAction()
	{			
		performFileAction(fileChoosingHandler.getSelectedFile("open"));
	}
	

	public abstract void performFileAction(File file);
	
}
