/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;


public abstract class ActionPerformer 
{

	public abstract String getLabel();
	public abstract char getAccellerator();
	public abstract void performAction() throws Exception;
	
}
