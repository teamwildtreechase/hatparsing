/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SpinnerPanelTestPanel extends JPanel implements ChangeListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void stateChanged(ChangeEvent e) 
	{
		System.out.println("State Changed");
	}

}
