/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;

import viewer_io.SpinnerPanelTestPanel;

public class SpinnerPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel leftLabel;
	private JSpinner spinner;
	private JLabel rightLabel;
	private int maxSpinnerValue = 0;
	
	public SpinnerPanel(JLabel leftLabel, JSpinner spinner, JLabel rightLabel, ChangeListener containingPanel)
	{
		super(new FlowLayout());
		this.leftLabel = leftLabel;
		this.spinner = spinner;
		this.rightLabel = rightLabel;
		this.rightLabel.setFocusable(false);
		this.add(this.leftLabel);
		this.add(this.spinner);
		this.add(this.rightLabel);
	}
	
	public static SpinnerPanel createSpinnerPanel(ChangeListener containingPanel, String spinnerLabel)
	{
		JSpinner spinner = createNumberSpinner();
		spinner.setPreferredSize(new Dimension(50,30));
		spinner.addChangeListener(containingPanel);
		spinner.setFocusable(true);  
		JLabel leftLabel = new JLabel(spinnerLabel);
		JLabel rightLabel = new JLabel("of : 0");
		
		return new SpinnerPanel(leftLabel, spinner, rightLabel, containingPanel);
	}
	
	private static JSpinner createNumberSpinner()
	{
		JSpinner result =  new JSpinner();
		return result;
	}
	

	
	public void setSpinnerSize(int size)
	{
		System.out.println("\n\n @@@@@@@@@@ " +
				"setSpinnerSize(" + size +") called");
		
		// always have a size of 1 at least to prevent errors
		int newSize = Math.max(size, 1);
		
		SpinnerNumberModel spinnerModel = new SpinnerNumberModel(1,1,newSize,1);
		
		this.spinner.setModel(spinnerModel);   		
		
		this.rightLabel.setText("of: " + size);    

		// Only set focus to this spinner if there is actually something to spin
		// i.e. more than one alignment triple or tree
		if(size > 1)
		{	
			this.spinner.requestFocusInWindow();
		}	
		System.out.println("spinner: " + this.spinner);
		this.maxSpinnerValue = size;
	}
	
	public int getSpinnerValue()
	{
		return (Integer) this.spinner.getValue();
	}
	
	public int getMaxSpinnerValue()
	{
		return this.maxSpinnerValue;
	}
	
	public JSpinner getSpinner()
	{
		return spinner;
	}
	
	
	
	
	public void init()
	{
		this.revalidate();
		this.repaint();
	}
	
	public String toString()
	{
		String result = "";
		
		result += "<< SpinnerPanel: " 
			+ "\nleftLabel: " + this.leftLabel.getText() 
			+ "\nrightLabel: " + this.rightLabel.getText();
		
		int i =1;
		for(Component component: this.getComponents())
		{
			result += "\nComponent " + i + " " + component;
			i++;
		}
		
		result += "\n>>";
		return result;
	}
	
	public static void main(String[] args)
	{
		JFrame mainFrame = new JFrame();
		SpinnerPanelTestPanel mainPanel = new SpinnerPanelTestPanel();
		
		SpinnerPanel spinnerPanel = SpinnerPanel.createSpinnerPanel(mainPanel, "Item :");
		mainPanel.add(spinnerPanel);
		
		mainFrame.add(mainPanel);
		
	    mainFrame.pack();
	    mainFrame.setVisible(true);
	    
	    spinnerPanel.setSpinnerSize(3);
	}
	
}
