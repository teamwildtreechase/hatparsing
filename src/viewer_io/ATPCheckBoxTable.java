/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;

public class ATPCheckBoxTable extends CheckBoxTable
{

	private ATPCheckBoxTable(List<JCheckBox> checkBoxes)
	{
		super(checkBoxes);
	}
	
	
	public static ATPCheckBoxTable createATPCheckBoxTable()
	{
		return new ATPCheckBoxTable(createATPCheckBoxOptions());
	}
		
    public static List<JCheckBox> createATPCheckBoxOptions()
    {
    	List<JCheckBox> result = new ArrayList<JCheckBox>();
    	result.add(new JCheckBox("Skewed Lines", false));
    	result.add(new JCheckBox("Show Heads", false));
    	result.add(new JCheckBox("No Colors", false));
    	result.add(new JCheckBox("Show only alignments", false));
    	result.add(new JCheckBox("Show Tree Alignment Violations", true));
    	result.add(new JCheckBox("Show Tree Reordering", true));
    	result.add(new JCheckBox("Ignore nulls in Reordering", true));
    	return result;
    }
	

    
    public boolean useSkewedLines()
    {
    	return this.checkBoxes.get(0).isSelected();
    }
    
    public boolean showHeads()
    {
    	return this.checkBoxes.get(1).isSelected();
    }
    
    public boolean noColors()
    {
    	return this.checkBoxes.get(2).isSelected();
    }
    
    public boolean showOnlyAlignments()
    {
    	return this.checkBoxes.get(3).isSelected();
    }
    
    public boolean showTreeAlignmentViolations()
    {
    	return this.checkBoxes.get(4).isSelected();
    }
    
    public boolean showTreeReordering()
    {
    	return this.checkBoxes.get(5).isSelected();
    }
    
    public boolean ignoreNullsInReordering()
    {
    	return this.checkBoxes.get(6).isSelected();
    }
}
