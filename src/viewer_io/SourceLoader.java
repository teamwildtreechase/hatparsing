/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import java.util.List;

import util.InputReading;

public class SourceLoader extends InputLoader
{	
	private static final String MenuString = "Open Source Sentences";
	private static final char MenuAccellerator = 'S';
	
	private SourceLoader(FileChoosingHandler fileChoosingHandler, SentencesAndAlignmentsLoader loader)
	{
		super(fileChoosingHandler,loader);
	}
	
	public static SourceLoader createSourceLoader(FileChoosingHandler fileChoosingHandler,SentencesAndAlignmentsLoader loader)
	{
		return new SourceLoader(fileChoosingHandler,loader);
	}
	
	@Override
	public void loadInput(File sourceSentencesFile) 
	{
		loadSourceSentences(sourceSentencesFile,loader);
		
	}

	public static boolean loadSourceSentences(File sourceSentencesFile,  SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader) 
	{                		
		System.out.println("\n Entered loadSourceSentences...");
		
		List<List<String>> sourceSentences = null;
		try 
		{
			sourceSentences = InputReading.getSentences(sourceSentencesFile);
			sentencesAndAlignmentsLoader.loadSourceSentences(sourceSentences);
			return true;
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			System.out.println(e);
			return false;
		}
				
	}

	@Override
	public String getLabel() 
	{
		return SourceLoader.MenuString;
	}

	@Override
	public char getAccellerator() 
	{
		return SourceLoader.MenuAccellerator;
	}

	@Override
	public void performFileAction(File file) 
	{
		
		loadSourceSentences(file,loader);
		
	}

}
