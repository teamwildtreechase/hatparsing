/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;


public interface TreebankLoaderInterface<T> 
{
	public boolean  loadTreebankAndSetSpinner(File file, boolean resetSpinner);
}
