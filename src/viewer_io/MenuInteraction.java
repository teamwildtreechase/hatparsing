/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import viewer_io.SpinnerPanel;

public class MenuInteraction 
{	
	private List<JMenuItem> menuItems;
	private FileChoosingHandler fileChooserHandler;
	
	private MenuInteraction(List<JMenuItem> menuItems, FileChoosingHandler fileChoosingHandler)
	{
		this.fileChooserHandler = fileChoosingHandler;
		this.menuItems = menuItems;
	}
	
	public static MenuInteraction createMenuInteraction(JPanel panel)
	{
		
		
		List<JMenuItem> menuItems = new ArrayList<JMenuItem>();
		
		return new MenuInteraction(menuItems,FileChoosingHandler.createFileChoosingHandler(panel));
	}
	
	public List<JMenuItem> getMenuItems()
	{
		return menuItems;
	}
	
	
	public FileChoosingHandler getFileChoosingHandler()
	{
		return this.fileChooserHandler;
	}
	
	public void addAlignmentTripleLoadItems(JPanel panel, SpinnerPanel spinnerPanel)
	{
		try {
			menuItems.addAll(MenuInteraction.createAlignmentTripeLoadItems(this.getFileChoosingHandler(), panel, spinnerPanel));
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void addExportTreeItems(JPanel panel, String treeName)
	{
		List<Exporter> exporters = new ArrayList<Exporter>();
		exporters.add(ExporterPDF.createExporterPDF(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export " + treeName + " to PDF", 'P'));
		exporters.add(ExporterEPS.createExporterEPS(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export "+ treeName + " to EPS", (char)0));
		exporters.add(ExporterAllPDF.createExporterAllPDF(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export all "+ treeName + "s to PDF", 'L'));
		
		this.addExportItems(panel,exporters);
		
 }
	

	public void addExportAlignmentstems(JPanel panel)
	{
		List<Exporter> exporters = new ArrayList<Exporter>();
		exporters.add(ExporterPDF.createExporterPDF(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export Alignment to PDF", 'Z'));
		exporters.add(ExporterEPS.createExporterEPS(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export Alignment to EPS", (char)0));
		exporters.add(ExporterAllPDF.createExporterAllPDF(this.getFileChoosingHandler(),(ExportableCorpusViewPanel)panel, "Export all Alignments to PDF", 'C'));
		
		this.addExportItems(panel,exporters);
	    
	}
	
	public void addQuitter(QuitInterface quitInterface)
	{
		this.menuItems.add(createItem(Quitter.createQuitter(quitInterface)));
	}
	
	private static List<JMenuItem> createAlignmentTripeLoadItems(FileChoosingHandler fileChoosingHandler, JPanel panel, SpinnerPanel spinnerPanel) throws Exception
	{
		List<JMenuItem> result = new ArrayList<JMenuItem>();  		
		
		if(panel instanceof SentencesAndAlignmentsLoader )
		{
			result.add(createItem(SourceLoader.createSourceLoader(fileChoosingHandler, (SentencesAndAlignmentsLoader) panel)));
			result.add(createItem(TargetLoader.createTargetLoader(fileChoosingHandler, (SentencesAndAlignmentsLoader) panel, spinnerPanel)));
			result.add(createItem(AlignmentsLoader.createAlignmentsLoader(fileChoosingHandler, (SentencesAndAlignmentsLoader) panel)));
			
			return result;
		}	
		throw new Exception("The panel is not an instance of SentencesAndAlignmentsLoader");
	}
	
	
	public <T> void addLoadTreeItem(TreebankLoaderInterface<T> treeViewer, String menuString)
	{
		this.menuItems.add(createItem(TreebankLoader.createTreebankLoader(this.getFileChoosingHandler(),  (TreebankLoaderInterface<T>) treeViewer, menuString)));
		return;
	}
	
	public static  JMenuItem createAccelleratedMenuItem(String name, char accellerator)
	{
		JMenuItem item  = new JMenuItem(name);
		if(accellerator > 0)
		{	
			item.setAccelerator(KeyStroke.getKeyStroke(accellerator, java.awt.event.InputEvent.ALT_MASK));
		}	
		return item;
	}
	

	
	public JMenuItem createExportItem(final JPanel panel, final Exporter exporter) throws Exception
	{		
		if(panel instanceof ExportableCorpusViewPanel)
		{
			
			 return createItem(exporter);
		}	
		
		throw new Exception("The panel is not an instance of ExportableCorpusViewPane");
	}
	
	
	
	public static JMenuItem createItem(final  ActionPerformer fileActionPerformer)  
	{
		JMenuItem item = createAccelleratedMenuItem(fileActionPerformer.getLabel(),fileActionPerformer.getAccellerator() );
		
	
		item.addActionListener(new ActionListener() 
		   {
	        	public void actionPerformed(ActionEvent e) 
	        	{
	        		try 
	        		{
						fileActionPerformer.performAction();
					} catch (Exception e1) 
					{
						e1.printStackTrace();
					}
	            }
	        });
		
		  item.setMnemonic(fileActionPerformer.getAccellerator());	
		 return item;  
	}
	
	private void addExportItems(final JPanel panel,List<Exporter> exporters)
	{
		
		for(Exporter exporter : exporters)
		{
			try 
			{
				JMenuItem exportItem =  createExportItem(panel,exporter);
				this.menuItems.add(exportItem);
			} catch (Exception e) 
			{
				
				e.printStackTrace();
			}
		}
	}
	
	public void addItemsToMenu(JMenu menu)
	{
		  for(JMenuItem item : this.getMenuItems())
	        {
	        	System.out.println("item :" + item);
	        	menu.add(item);
	        }
	}
}
