/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;


public abstract class InputLoader extends FileActionPerformer 
{
	protected SentencesAndAlignmentsLoader loader;
		
	public InputLoader(FileChoosingHandler fileChoosingHandler, SentencesAndAlignmentsLoader loader)
	{
		super(fileChoosingHandler);
		this.loader = loader;
	}

	public  abstract void loadInput(File inputFile);

	

}
