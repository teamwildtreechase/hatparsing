/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

public class Quitter  extends ActionPerformer 
{

	private static final String MenuString = "Quit";
	private static final char MenuAccellerator = 'Q';
	QuitInterface quitInterface;
	
	
	private Quitter(QuitInterface quitInterface)
	{
		this.quitInterface = quitInterface;
	}
	
	public static Quitter createQuitter(QuitInterface quitInterface)
	{
		return new Quitter(quitInterface);
	}
	
	@Override
	public String getLabel() 
	{
		return  Quitter.MenuString;
	}

	@Override
	public char getAccellerator() 
	{
		return  Quitter.MenuAccellerator;
	}


	@Override
	public void performAction() 
	{
		quitInterface.doBeforeClosing();
		quitInterface.quit();
	}

	
}
