/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;


public abstract class Exporter extends   FileActionPerformer
{
	protected ExportableCorpusViewPanel panel;
	protected String label;
	protected char accellerator;
	

	public Exporter(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel, String label, char accellerator)
	{
		super(fileChoosingHandler);
		this.panel = panel;
		this.label = label;
		this.accellerator = accellerator;
	}
	
}
