/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import java.util.List;

import alignment.Alignment;
import util.InputReading;

public class AlignmentsLoader extends InputLoader
{	
	
	private static final String MenuString = "Open Alignments";
	private static final char MenuAccellerator = 'A';
	
	private AlignmentsLoader(FileChoosingHandler fileChoosingHandler, SentencesAndAlignmentsLoader loader)
	{
		super(fileChoosingHandler, loader);
	}
	
	public static AlignmentsLoader createAlignmentsLoader(FileChoosingHandler fileChoosingHandler, SentencesAndAlignmentsLoader loader)
	{
		return new AlignmentsLoader(fileChoosingHandler, loader);
	}
	
	@Override
	public void loadInput(File AlignmentsSentencesFile) 
	{
		loadAlignments(AlignmentsSentencesFile,loader);
		
	}

	public static boolean loadAlignments(File alignmentsFile, SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader)
	{
		System.out.println("\n Entered loadAlignments...");
		
		List<Alignment> alignments = null;
		try 
		{
			alignments = InputReading.getAlignments(alignmentsFile);
			assert(alignments != null);
			sentencesAndAlignmentsLoader.loadAlignments(alignments);
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			System.out.println("loadAlignments failed");
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	@Override
	public String getLabel() 
	{
		return AlignmentsLoader.MenuString;
	}

	@Override
	public char getAccellerator() 
	{
		return AlignmentsLoader.MenuAccellerator;
	}

	@Override
	public void performFileAction(File file) 
	{
		loadAlignments(file,loader);
		
	}


		
}