/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;
import util.FileUtil;


public class ExporterEPS  extends Exporter
{

	private ExporterEPS(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel,String label, char accellerator)
	{
		super(fileChoosingHandler, panel,label, accellerator);
	}
	
	
	public static ExporterEPS createExporterEPS(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel,String label, char accellerator)
	{
		return new ExporterEPS(fileChoosingHandler, panel,label, accellerator);
	}
	
	public void performFileAction(File file)
	{
		exportToEPS(file,  panel); 
	}
	
	
	 /**
     * Exports the current graph to EPS.
     *
     * @param file the eps file to export to.
     * @throws IOException if IO goes wrong.
     */
    public static void exportToEPS(File selectedFile, ExportablePanel panel) 
    {
    	
    	if (selectedFile.exists()) selectedFile.delete();
    	try 
    	{
    	
    		System.out.println("Started writing eps file");
    		/*
    		EpsGraphics dummy = new EpsGraphics("Title", new ByteArrayOutputStream(),
    	            0, 0, 1, 1, ColorMode.BLACK_AND_WHITE);

    	   // panel.render(dummy);
    	    //dummy.flush();
    	    //dummy.close();
    	    //dummy.dispose();
    		*/
    	    
    		EpsGraphics g2 = new EpsGraphics("Title", new FileOutputStream(selectedFile), 0, 0,
    				(int) panel.getArea().getWidth() + 2, (int) panel.getArea().getHeight(), ColorMode.COLOR_RGB);
    		panel.render(g2);

            g2.flush();
            g2.close();
            g2.dispose();
    	    
    	    System.out.println("Finished writing eps file");
    	   
    	} 
    	
    	catch(IOException e) 
    	{
    		e.printStackTrace();
    		FileUtil.handleExceptions(e);
    	} 
    	
    	
    }


	@Override
	public String getLabel() 
	{
		return label;
	}


	@Override
	public char getAccellerator() 
	{
		return accellerator;
	}
	
}
