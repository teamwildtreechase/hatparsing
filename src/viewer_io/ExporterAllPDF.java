/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.Graphics2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;


public class ExporterAllPDF  extends Exporter
{

	private ExporterAllPDF(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel,String label, char accellerator)
	{
		super(fileChoosingHandler, panel, label, accellerator);
	}
	
	
	public static ExporterAllPDF createExporterAllPDF(FileChoosingHandler fileChoosingHandler, ExportableCorpusViewPanel panel,String label, char accellerator)
	{
		return new ExporterAllPDF(fileChoosingHandler, panel, label, accellerator);
	}
	
	public void performFileAction(File file)
	{
		exportAllToPDF(file,panel);
	}
	
	public static void exportAllToPDF(File outputFile,  ExportableCorpusViewPanel panel) {
		if (outputFile.exists()) outputFile.delete();
		int currentSentence = panel.getSentenceNumber();		
		BaseColor bgColor = new BaseColor(0xFF, 0xDE, 0xAD);
		try {
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(outputFile));
			document.open();
			PdfContentByte cbL = writer.getDirectContent();
			//Chunk intro = new Chunk("Conversion of sec-00 of the WSJ in TDS");
			//document.add(intro);
			for(int sentenceNumber = 0; sentenceNumber < panel.getCorpusSize() ; sentenceNumber++) {
				//System.out.println(sentenceNumber);
				panel.goToSentence(sentenceNumber);
				panel.loadSentence();						
				Rectangle r = new Rectangle((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight());
				Chunk sentenceLabel = new Chunk("Sentence # " + (sentenceNumber+1));
				sentenceLabel.setBackground(bgColor);				
				document.setPageSize(r);
				document.newPage();
				document.add(sentenceLabel);
				Graphics2D g2 = cbL.createGraphics((int)panel.getArea().getWidth(), (int)panel.getArea().getHeight());
				panel.render(g2);
				g2.dispose();
			}
			document.close();
		}		
		catch (DocumentException de) {System.err.println(de.getMessage());} 
		catch (IOException ioe) {System.err.println(ioe.getMessage());}					
		
		// Go back to the original sentence
		panel.goToSentence(currentSentence);
	}
	


	@Override
	public String getLabel() 
	{
		return label;
	}


	@Override
	public char getAccellerator() 
	{
		return accellerator;
	}
	
}
