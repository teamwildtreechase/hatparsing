/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import tsg.TSNodeLabel;
import util.Pair;

public class TreePairBankLoader extends  FileActionPerformer
{	
	
	private static final String MenuString = "Open TreePairBank";
	private static final char MenuAccellerator = 0;
	private TreebankLoaderInterface<Pair<TSNodeLabel>> treebankLoaderInterface;
	
	private TreePairBankLoader(FileChoosingHandler fileChoosingHandler, TreebankLoaderInterface<Pair<TSNodeLabel>>  treebankLoaderInterface)
	{
		super(fileChoosingHandler);
		this. treebankLoaderInterface =  treebankLoaderInterface;
	}
	
	public static TreePairBankLoader createTreePairBankLoader(FileChoosingHandler fileChoosingHandler, TreebankLoaderInterface<Pair<TSNodeLabel>>   treebankLoaderInterface)
	{
		return new TreePairBankLoader(fileChoosingHandler, treebankLoaderInterface);
	}
	
	public void loadInput(File TreePairBankSentencesFile) 
	{
		loadTreePairBank(TreePairBankSentencesFile,treebankLoaderInterface, true);
		
	}

	public static boolean loadTreePairBank(File treePairBankFile, TreebankLoaderInterface<Pair<TSNodeLabel>>  treebankLoaderInterface, boolean resetSpinner)
	{
		System.out.println("\n Entered loadTreePairBank...");
		
		try 
		{
			 treebankLoaderInterface.loadTreebankAndSetSpinner(treePairBankFile,resetSpinner);
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			System.out.println("loadTreePairBank failed");
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	@Override
	public String getLabel() 
	{
		return TreePairBankLoader.MenuString;
	}

	@Override
	public char getAccellerator() 
	{
		return TreePairBankLoader.MenuAccellerator;
	}

	@Override
	public void performFileAction(File file) 
	{
		loadTreePairBank(file,treebankLoaderInterface,true);
		
	}


		
}