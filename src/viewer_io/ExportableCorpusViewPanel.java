/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;


public interface ExportableCorpusViewPanel extends ExportablePanel
{
	public int goToSentence(int sentenceNumber);
	public void loadSentence();
	public int getCorpusSize();
	public int getSentenceNumber();
}
