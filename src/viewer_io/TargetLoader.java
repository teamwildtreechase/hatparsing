/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import java.util.List;
import util.InputReading;
import viewer_io.SpinnerPanel;

public class TargetLoader extends InputLoader
{	

	private static final String MenuString = "Open Target Sentences";
	private static final char MenuAccellerator = 'T';		
	protected SpinnerPanel spinnerPanel;
	
	private TargetLoader(FileChoosingHandler fileChoosingHandler,SentencesAndAlignmentsLoader loader, SpinnerPanel spinnerPanel)
	{
		super(fileChoosingHandler,loader);
		this.spinnerPanel = spinnerPanel;
	}
	
	public static TargetLoader createTargetLoader(FileChoosingHandler fileChoosingHandler, SentencesAndAlignmentsLoader loader, SpinnerPanel spinnerPanel)
	{
		return new TargetLoader(fileChoosingHandler, loader,spinnerPanel);
	}
	
	@Override
	public void loadInput(File TargetSentencesFile) 
	{
		loadTargetSentences(TargetSentencesFile,loader, spinnerPanel);
		
	}

	public static boolean loadTargetSentences(List<List<String>> targetSentences, SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader,
			SpinnerPanel spinnerPanel) 
	{                		
		int size = targetSentences .size();
		System.out.println("spinnerPanel.setSpinnerSize("+size+")");
		spinnerPanel.setSpinnerSize(size);
		sentencesAndAlignmentsLoader.loadTargetSentences(targetSentences);
		
		return true;
	
		
	}
	
	public static boolean loadTargetSentences(File targetSentencesFile, SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader,
			SpinnerPanel spinnerPanel) 
	{                		
		System.out.println("\n Entered loadTargetSentences...");
		
		List<List<String>> targetSentences = null;
		try 
		{
			targetSentences = InputReading.getSentences(targetSentencesFile);
		
			return loadTargetSentences(targetSentences, sentencesAndAlignmentsLoader, spinnerPanel);
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public String getLabel() 
	{
		return TargetLoader.MenuString;
	}

	@Override
	public char getAccellerator() 
	{
		return TargetLoader.MenuAccellerator;
	}

	@Override
	public void performFileAction(File file) 
	{
		loadTargetSentences(file,loader, spinnerPanel);
	}



}
