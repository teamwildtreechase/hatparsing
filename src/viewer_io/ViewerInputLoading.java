/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;
import extended_bitg.HATViewer;
import util.ConfigFile;
import viewer_io.SpinnerPanel;

public class ViewerInputLoading 
{
	private static final String SourceParseFileNameProperty = "sourceParseFileName";
	private static final String TargetParseFileNameProperty = "targetParseFileName";
	
	public static <T> boolean loadTreebank(File treebankFile, boolean resetSpinner, TreebankLoaderInterface<T>  treebankLoader)
	{                		
		return treebankLoader.loadTreebankAndSetSpinner(treebankFile,resetSpinner);
	}
	
	
	public static void loadEverythingExceptTreesFromConfig(String configFilePath ,SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader, SpinnerPanel spinnerPanel)
	{
		System.out.println(">>> Load everything except trees...");    			
		System.out.println("Reading configfile from " + configFilePath);
		try{
			ConfigFile theConfig = new ConfigFile(configFilePath);
			
			String corpusLocation = theConfig.getValue("corpusLocation");
			String targetFileName = corpusLocation + theConfig.getValue("targetFileName");
			String sourceFileName = corpusLocation + theConfig.getValue("sourceFileName");
			String alignmentsFileName = corpusLocation + theConfig.getValue("alignmentsFileName");
		
			System.out.println("targetFileName:" + targetFileName);
			
			File targetSentencesFile = new File(targetFileName);
			if(! TargetLoader.loadTargetSentences(targetSentencesFile ,sentencesAndAlignmentsLoader, spinnerPanel))
			{
				System.out.println("Please check that the path to the target sentences is set correctly in the configfile");
			}
			File alignmentFile = new File(alignmentsFileName);
			
			assert(alignmentFile != null);
			
			File sourceFile = new File(sourceFileName);
			if(! SourceLoader.loadSourceSentences(sourceFile,sentencesAndAlignmentsLoader))
			{
				System.out.println("Please check that the path to the source sentences is set correctly in the configfile");
			}
			
			if(! AlignmentsLoader.loadAlignments(alignmentFile, sentencesAndAlignmentsLoader))
			{
				System.out.println("Please check that the path to the alignmentfile is set correctly in the configfile - current complete path: " + alignmentsFileName);
			}
		}
		catch(Exception except)
		{
			System.out.println("Error handling configfile: " + except);
		}

	}
	
	
	public static <T> void loadAlignmentTriplesAndTreesFromConfig(String configFilePath ,SentencesAndAlignmentsLoader  sentencesAndAlignmentsLoader,TreebankLoaderInterface<T> treeBankLoader, SpinnerPanel spinnerPanel)
	{		
		System.out.println(">>> Load trees...");    			
		ConfigFile theConfig = loadConfig(configFilePath);
		loadTreeBank(treeBankLoader, theConfig, "parseFileName");
    	loadEverythingExceptTreesFromConfig(configFilePath ,  sentencesAndAlignmentsLoader, spinnerPanel);
	}

	public static <T> void loadAlignmentTriplesAndParsesFromConfig(String configFilePath , HATViewer hatViewer,SpinnerPanel spinnerPanel)
	{		
    	System.out.println(">>> loadAlignmentTriplesAndParsesFromConfig - Load trees... , configFilePath: " + configFilePath);    			
		ConfigFile theConfig = loadConfig(configFilePath);
		
		loadTreeBank(hatViewer.getAlignmentPanel().getSourceParsesTable(), theConfig, SourceParseFileNameProperty);
		loadTreeBank(hatViewer.getAlignmentPanel().getTargetParsesTable(), theConfig, TargetParseFileNameProperty);
		//assert(labeledAlignmentPanel.getTargetParsesTable().hasParses());
		loadEverythingExceptTreesFromConfig(configFilePath ,hatViewer, spinnerPanel);
	}
	
	
	private static <T> boolean loadTreeBank(TreebankLoaderInterface<T> treeBankLoader, ConfigFile theConfig, String parsesName)
	{
		String corpusLocation = theConfig.getValue("corpusLocation");
		
		if(corpusLocation == null)
		{
			System.out.println("Please check that the path to the corpus is set correctly in the configfile");
			return false;
		}
		else if(theConfig.getValue(parsesName) == null)
		{
			System.out.println("Please check that the property "  + parsesName + " is set correctly in the configfile");
			return false;
		}
		else 
		{
			String treeBankName = corpusLocation + theConfig.getValue(parsesName);
			File treeBankFile = new File(treeBankName);
			System.out.println("ViewerInputLoading -Load treebank at: " + treeBankFile);
			ViewerInputLoading.loadTreebank(treeBankFile, true,treeBankLoader);
			return true;
		}
	}
	

	private static ConfigFile loadConfig(String configFilePath)
	{
		try
		{
			System.out.println("Reading configfile from " + configFilePath);
			ConfigFile theConfig = new ConfigFile(configFilePath);
			return theConfig;
		}
		catch(Exception except)
		{
			System.out.println("Error handling configfile: " + except);
			throw new RuntimeException(except);
		}
	}
	
	




}