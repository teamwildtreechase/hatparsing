/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import viewer.PanelSizeProperties;

/** Convenience class used to group a set of associated button, textfield and corresponding labels
  * each of the fields is optional and may be set to null
  * public class ButtonIOComponent
  */ 
public class ButtonIOComponent
{
	private JButton theButton;
	private JTextField theTextField;
	private JLabel buttonLabel;
	JLabel textfieldLabel;
	
	private static final int ButtonPanelWidth = 270;
	// Number that is used in determining how wide the buttoncomponent may be  made, depending on the screen size
	private static final int RightOfButtonComponentRequiredWidth = 600; //TODO: Fix this magic number, which should be really dependent on were the ButtonIOComponent is used
	
	public ButtonIOComponent(JButton theButton, JTextField theTextField, JLabel buttonLabel, JLabel textfieldLabel)
	{
		this.setTheButton(theButton);
		this.theTextField = theTextField;
		this.setButtonLabel(buttonLabel);
		this.textfieldLabel = textfieldLabel;
		
	}

	public void setTheButton(JButton theButton) {
		this.theButton = theButton;
	}

	public JButton getTheButton() {
		return theButton;
	}

	
	public String getTextfieldContents()
	{
		if(theTextField != null)
		{
			return theTextField.getText();
		}
		else
		{
			return null;
		}
	}
	
	public boolean hasTextField()
	{
		return (this.theTextField != null);
	}
	
	public void setTextFieldSize(Dimension d)
	{
		if(theTextField != null)
		{	
			theTextField.setPreferredSize(d);
		}	
	}
	
	public void addToPanel(JPanel ioComponentPanel)
	{

		ioComponentPanel.setLayout(new FlowLayout(FlowLayout.LEFT));  
		
		int totalWidth = getScreenSize().width - RightOfButtonComponentRequiredWidth ;
		int textfieldPanelWidth = totalWidth - ButtonPanelWidth;
		
		
		JPanel buttonPanel = new JPanel();
		JPanel textFieldPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(ButtonPanelWidth,30));
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		textFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		textFieldPanel.setPreferredSize(new Dimension(textfieldPanelWidth,25));
	
		
		
		if(this.getTheButton() != null)
		{
			this.getTheButton().setPreferredSize(new Dimension(ButtonPanelWidth - 10,25)); 
			
			// add the corresponding button label if available
			if(this.getButtonLabel() !=  null)
			{
				
				buttonPanel.add(this.getButtonLabel());
			}
			
			buttonPanel.add(Box.createHorizontalGlue());
			
			this.getTheButton().setFocusable(false);
			buttonPanel.add(this.getTheButton());
		}
		
		if(this.hasTextField())        			        		
		{
		
			// add the corresponding label if available
			if(this.textfieldLabel !=  null)
			{
				if(this.getTextfieldLabelRenderLength() < 100)
				{	
					this.textfieldLabel.setPreferredSize(new Dimension(100,20)); 
					this.setTextFieldSize(new Dimension(textfieldPanelWidth - 120,20));
					
				}
				else
				{
					this.textfieldLabel.setPreferredSize(new Dimension(this.getTextfieldLabelRenderLength(),20));
					this.setTextFieldSize(new Dimension(textfieldPanelWidth - this.getTextfieldLabelRenderLength() - 20,20)); 
					
				}
				textFieldPanel.add(this.textfieldLabel);
			}
			
			textFieldPanel.add(Box.createHorizontalGlue());
			textFieldPanel.add(this.theTextField);
		}
		
		
		ioComponentPanel.add(buttonPanel);
		ioComponentPanel.add(Box.createHorizontalGlue());
		ioComponentPanel.add(textFieldPanel);

	}
	
	private int getTextfieldLabelRenderLength()
	{
		return PanelSizeProperties.computeStringRenderLength(new JPanel(), this.textfieldLabel.getText(), this.textfieldLabel.getFont());
	}
	
	private static Dimension getScreenSize()
	{
		// Get the default toolkit
		Toolkit toolkit = Toolkit.getDefaultToolkit();

				// Get the current screen size
		Dimension scrnsize = toolkit.getScreenSize();
		return scrnsize;
	}
	

	public void setButtonLabel(JLabel buttonLabel) {
		this.buttonLabel = buttonLabel;
	}

	public JLabel getButtonLabel() {
		return buttonLabel;
	}
	
	public JTextField getTextField()
	{
		return this.theTextField;
	}
	
	
	/*
	public void setTheTextField(JTextField theTextField) {
		this.theTextField = theTextField;
	}

	public JTextField getTheTextField() {
		return theTextField;
	}
	*/
}
