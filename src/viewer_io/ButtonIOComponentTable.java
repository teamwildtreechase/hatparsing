/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;

public class ButtonIOComponentTable 
{

	List<ButtonIOComponent> buttonIOComponents;
	Map<String,ButtonIOComponent> lookupTable; 
	
	private ButtonIOComponentTable(List<ButtonIOComponent> buttonIOComponents, Map<String,ButtonIOComponent> lookupTable) 
	{
		this.buttonIOComponents = buttonIOComponents;
		this.lookupTable = lookupTable;
	}
	
	public static ButtonIOComponentTable createButtonIOComponentTable()
	{
		List<ButtonIOComponent> buttonIOComponents = new ArrayList<ButtonIOComponent>();
		Map<String,ButtonIOComponent> lookupTable = new Hashtable<String,ButtonIOComponent>();
		return new ButtonIOComponentTable(buttonIOComponents, lookupTable);
	}
	
	public void addComponent(String componentName, ButtonIOComponent component)
	{
		buttonIOComponents.add(component);
		lookupTable.put(componentName, component);
	}
	
	public ButtonIOComponent getComponentByName(String name)
	{
	
		return lookupTable.get(name);
		
	}
	
	public ButtonIOComponent get(int index)
	{
		return buttonIOComponents.get(index);
	}

	public List<ButtonIOComponent> getComponents()
	{
		return buttonIOComponents;
	}
	
	public String getComponentTextFieldString(String componentName)
	{
		return getComponentByName(componentName).getTextfieldContents();
	}

	public JButton getButtonByComponentIndex(int componentIndex)
	{
		
		return this.get(componentIndex).getTheButton();
	}
	
}