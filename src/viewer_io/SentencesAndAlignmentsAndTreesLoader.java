/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

public interface SentencesAndAlignmentsAndTreesLoader<T> extends SentencesAndAlignmentsLoader, TreebankLoaderInterface<T> 
{
	
}