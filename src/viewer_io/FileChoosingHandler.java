/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

public class FileChoosingHandler 
{
	private JFileChooser fileChooser;
	private JPanel hostingPanel;
	
	
	private FileChoosingHandler(JFileChooser fileChooser, JPanel hostingPanel)
	{
		this.fileChooser = fileChooser;
		this.hostingPanel = hostingPanel;
	}
	
	public static FileChoosingHandler createFileChoosingHandler(JPanel hostingPanel)
	{
		return new FileChoosingHandler(new JFileChooser(), hostingPanel);
	}
	
	
	public JFileChooser getFileChooser()
	{
		return fileChooser;
	}
	
	public JPanel getHostingPanel()
	{
		return hostingPanel;
	}
	

	
	public File getSelectedFile(String dialogTitle)
	{
		File result = null;
		
		// save original dialog title
		String oldTitle = fileChooser.getDialogTitle();
		
		fileChooser.setDialogTitle(dialogTitle);
		int returnVal = fileChooser.showOpenDialog(hostingPanel);	                
        if (returnVal == JFileChooser.APPROVE_OPTION) 
        {
        	result = fileChooser.getSelectedFile();
        }	
		
        fileChooser.setDialogTitle(oldTitle);
        return result;		 
		
		
		
	}
	
}
