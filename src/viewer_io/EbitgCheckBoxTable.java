/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;

public class EbitgCheckBoxTable extends CheckBoxTable
{

	protected EbitgCheckBoxTable(List<JCheckBox> checkBoxes) 
	{
		super(checkBoxes);
	}

	public static EbitgCheckBoxTable createEbitgCheckBoxTable()
	{
		return new EbitgCheckBoxTable(createBitgCheckBoxOptions());
	}
		
    public static List<JCheckBox> createBitgCheckBoxOptions()
    {
    	List<JCheckBox> result = new ArrayList<JCheckBox>();
    	result.add(new JCheckBox("Compute All induced HATs", false));
    	result.add(new JCheckBox("Use Symbols for Words", false));
    	result.add( new JCheckBox("No Colors", false));
    	result.add(new JCheckBox("Show Name Plates", false));
      	result.add(new JCheckBox("Show extra labels", false));
      	result.add(new JCheckBox("Show alignment sets", true));
      	result.add(new JCheckBox("Greedy Null Binding", true));
      	result.add(new JCheckBox("Show HAT Reordering Labels", true));
      	result.add(new JCheckBox("Show Parent-Relative Reordering Labels", true));
      	result.add(new JCheckBox("Show Pre-terminal set-permutation Labels", false));
    	return result;
    }
	
    public boolean computeAllInducedHATs()
    {
    	return this.checkBoxes.get(0).isSelected();
    }
    
    public boolean useSymbolsForWords()
    {
    	return this.checkBoxes.get(1).isSelected();
    }
    
    public boolean noColors()
    {
    	return this.checkBoxes.get(2).isSelected();
    }
    
    public boolean showNamePlates()
    {
    	return this.getShowNamePlateCheckBox().isSelected();
    }
    
    public boolean showExtraLabels()
    {
    	return this.getShowExtraLabelsCheckBox().isSelected();
    }
   
    public boolean showAlignmentSets()
    {
    	return this.getShowAlignmentSetCheckBox().isSelected();
    }
    
    public boolean useGreedyNullBinding()
    {
    	return this.getUseGreedyNullBindingCheckBox().isSelected();
    }
    
    public boolean showHATReorderingLabels()
    {
    	return this.getShowHATReorderingLabelsCheckBox().isSelected();
    }
    
    public boolean showParentRelativeReorderingLabels()
    {
    	return this.getShowParentRelativeReorderingLabelsCheckBox().isSelected();
    }
    
    public boolean showPreTerminalLabels()
    {
        return this.getShowPreterminalLabelsCheckBox().isSelected();
    }

    public JCheckBox getUseSymbolsCheckBox()
    {
    	return this.checkBoxes.get(1);
    }

    public JCheckBox getNoColorsCheckBox()
    {
    	return this.checkBoxes.get(2);
    }

    public JCheckBox getShowNamePlateCheckBox()
    {
    	return this.checkBoxes.get(3);
    }

    public JCheckBox getShowExtraLabelsCheckBox()
    {
    	return this.checkBoxes.get(4);
    }
   
    public JCheckBox getShowAlignmentSetCheckBox()
    {
    	return this.checkBoxes.get(5);
    }
    
    public JCheckBox getUseGreedyNullBindingCheckBox()
    {
    	return this.checkBoxes.get(6);
    }
    
    public JCheckBox getShowHATReorderingLabelsCheckBox()
    {
    	return this.checkBoxes.get(7);
    }
    
    public JCheckBox getShowParentRelativeReorderingLabelsCheckBox()
    {
    	return this.checkBoxes.get(8);
    }
        
    public JCheckBox getShowPreterminalLabelsCheckBox() 
    {      
      return this.checkBoxes.get(9);
    }
}
