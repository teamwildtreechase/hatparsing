/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;

public class CheckBoxTable 
{
	protected List<JCheckBox> checkBoxes; 
	
	protected CheckBoxTable(List<JCheckBox> checkBoxes)
	{
		this.checkBoxes = checkBoxes;
	}


	public static CheckBoxTable createCheckBoxTable()
	{
		return new CheckBoxTable(createCheckBoxOptions());
	}
		
    public static List<JCheckBox> createCheckBoxOptions()
    {
    	List<JCheckBox> result = new ArrayList<JCheckBox>();
    	result.add(new JCheckBox("No Colors", false));
    	return result;
    }
	
	
	public List<JCheckBox> getCheckBoxes()
	{
		return checkBoxes;
	}
	
	public int numCheckBoxes(){
	  return checkBoxes.size();
	}

	public  boolean noColors()
	{
		return this.checkBoxes.get(0).isSelected();
	}
	
	public void addCheckBoxActionListeners(final ActionListener actionListener)
	{	
		for(JCheckBox checkBox : this.getCheckBoxes())
		{
			checkBox.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent e) 
				{
					actionListener.actionPerformed(e);
				}
		
			});
		}
	}
	
}

