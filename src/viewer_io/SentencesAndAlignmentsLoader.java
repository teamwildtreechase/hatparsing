/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

import java.util.List;
import alignment.Alignment;

public interface SentencesAndAlignmentsLoader 

{
	public void loadAlignments(List<Alignment> alignments);
	
	public void loadSourceSentences(List < List< String >> sourceSentences);

	public void loadTargetSentences(List < List< String >> targetSentences);
	
	
}
