/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package viewer_io;

public interface QuitInterface 
{
	public void doBeforeClosing();
	public void quit();
}
