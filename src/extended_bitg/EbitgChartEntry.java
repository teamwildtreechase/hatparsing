/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import reorderingLabeling.FineParentRelativeReorderingLabel;
import hat_lexicalization.Lexicalizer;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import alignmentStatistics.AlignmentChunk;
import alignmentStatistics.SentencePairAlignmentChunking;
import bitg.ChartEntry;
import bitg.IntegerSetMethods;

public class EbitgChartEntry extends ChartEntry<EbitgNode, EbitgInference> {

	// the entry types are mutually exclusive: i.e. if we have a binary inference in a chart entry
	// other non-binary inferences will not be considered/allowed and the type of the whole entry
	// may be considered Binary.
	public enum EntryType {
		SourceNullsBinding, Atomic, BITT, Complex
	};

	/*
	 * Remark: The problem is that in the extended world view a derivation is not simply a matter of combining chart entries. In the worst case it is a matter
	 * of combining derivations, since a chart entry may or may not match extra un-aligned words in the target. Thus we should indicate in the chart entry
	 * whether it is ambiguous.
	 */

	private boolean isAligned;

	private boolean isComplete;

	// the set of target positions that can be bound: this consists of the target
	// positions that strictly must be bound supplemented by those null positions that
	// are also bindable
	private HashSet<Integer> bindableTargetPositions;

	private HashSet<Integer> dependentSourcePositions;

	// reference to the used Lexicalizer,used for getting the associated source words
	public Lexicalizer lexicalizer;;

	private EntryType entryType;
	private FineParentRelativeReorderingLabel parentRelativeReorderingLabel;

	private boolean containsMonontoneBittInferences;
	
	
	/**
	 *	The HATComplexityType is the type of HAT we are dealing with as opposed to the
	 *	type of local Inference Complexity type. The HATComplexityType is computed 
	 *	recursively from the local InferenceComplexityType and the HATComplexityType
	 *	of the children. Hereby the principle is that the HAT complexity type is the parent 
	 *	is the HAT complexity type of the most complex child or if the local operation of 
	 *	the Inference is even more complex it is that.
	 *
	 */
	public enum HATComplexityType {INCOMPLETE,ATOMIC,BITT_MONO,BITT_INVERTED,PET,HAT};
	
	
	private HATComplexityType hatComplexityType = null;
	

	public EbitgChartEntry(int i, int j, Lexicalizer lexicalizer) {
		super(i, j);
		isComplete = false;

		// initialize the tables with covered source and target positions
		bindableTargetPositions = new HashSet<Integer>();
		dependentSourcePositions = new HashSet<Integer>();

		this.lexicalizer = lexicalizer;

	}

	public EbitgChartEntry(int i, Lexicalizer lexicalizer) {
		this(i, i, lexicalizer);

		// Get the alignment chunk that covers source position i
		assert (lexicalizer.getChartInternalAlignmentChunking() != null);
		AlignmentChunk chunk = lexicalizer.getChartInternalAlignmentChunking().getAlignmentChunkForSourcePosition(i);
		assert (chunk != null);
		// copy the target positions from the aligmentchunk as the covered target positions for this entry
		bindableTargetPositions = new HashSet<Integer>(chunk.getTargetPositions());
		// copy the source positions from the alignmentchunk as the dependent source positions
		dependentSourcePositions = new HashSet<Integer>(chunk.getSourcePositions());

		// Compute the completeness from the combination of the bindable target positions,
		// the dependentSourcePositions and the actual span of this chart entry
		computeCompleteness();

		if (bindableTargetPositions.isEmpty()) {
			isAligned = false;
		} else {
			isAligned = true;
		}

		// System.out.println(" ExtendedBITGChartEntry(" + i + ",chunking) returns");
	}

	// Getters and Setters

	public void setEntryType(EntryType type) {
		this.entryType = type;
	}

	
	public void setHATComplexityType(HATComplexityType type) {
		this.hatComplexityType = type;
	}
	
	public HATComplexityType getHATComplexityType()
	{
		return this.hatComplexityType;
	}
	
	public void setContainsMonontoneBittInferences(boolean containsMonontoneBittInferences) {
		this.containsMonontoneBittInferences = containsMonontoneBittInferences;
	}

	public boolean containsMonontoneBittInferences() {
		return this.containsMonontoneBittInferences;
	}
	
	public EntryType getEntryType() {
		return this.entryType;
	}

	/**
	 * Method that (re)computes completeness. If the chart entry
	 */
	public void computeCompleteness() {
		// A chart entry is only complete if both the souce and target
		// positions it depends upon are completed
		this.setIsComplete(chartEntryCoversAllDepentSourcePositions() && targetPositionsAreComplete(bindableTargetPositions));
	}

	public boolean chartEntryCoversAllDepentSourcePositions() {
		// If there are multiple dependent source positions
		if (dependentSourcePositions.size() > 1) {
			// the min and maximum dependent source position should be within the range [i...j]
			Boolean allDependentSourcePositionsCovered = (IntegerSetMethods.findMinInteger(dependentSourcePositions) >= i) && (IntegerSetMethods.findMaxInteger(dependentSourcePositions) <= j);

			if (allDependentSourcePositionsCovered) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}

	}

	public boolean targetPositionsAreComplete(HashSet<Integer> targetPositions) {
		// The target positions have to be able to form a consecutive range
		// (They may not for all derivations, but should be able to do so for at least
		// one derivation)
		if (IntegerSetMethods.formsConsecutiveRange(bindableTargetPositions)) {
			return true;
		} else {
			return false;
		}
	}

	public void setIsComplete(Boolean value) {
		this.isComplete = value;
	}

	public Boolean isComplete() {
		return this.isComplete;
	}

	public void setIsAligned(Boolean value) {
		this.isAligned = value;
	}

	public boolean isAligned() {
		return isAligned;
	}

	public void copyValuesFromEntry(EbitgChartEntry entry) {
		// copy the bindable target positions and dependent source positions
		this.bindableTargetPositions = entry.bindableTargetPositions;
		this.dependentSourcePositions = entry.dependentSourcePositions;

		// Copy the booleans
		this.isComplete = entry.isComplete;
		this.isAligned = entry.isAligned;

	}

	/**
	 * Add an inference to the nodeInference HashMap
	 * 
	 * @param inference
	 *            : the inference to add
	 */
	public void addInference(EbitgInference inference, EbitgChart chart) {
		EbitgNode node = new EbitgNode(inference);
		// lexicalizer.testInferenceConsistency(inference);
		this.addInference(inference, node);

		// Update the bindable target positions
		this.bindableTargetPositions.addAll(inference.getBoundTargetPositions());

	}

	public boolean isIndependent() {
		return dependentSourcePositions.isEmpty();
	}

	public HashSet<Integer> getDependentSourcePositions() {
		return dependentSourcePositions;
	}

	public HashSet<Integer> getBindableTargetPositions() {
		return bindableTargetPositions;
	}

	public int findMaxCoveredTargetPosition() {
		int max = 0;

		for (int targetPos : bindableTargetPositions) {
			if (targetPos > max) {
				max = targetPos;
			}
		}
		return max;
	}

	public int findMinCoveredTargetPosition() {
		int min = Integer.MAX_VALUE;

		for (int targetPos : bindableTargetPositions) {
			if (targetPos < min) {
				min = targetPos;
			}
		}
		return min;
	}

	/**
	 * Function that returns true if the chart entry has no bindable target positions, that is by itself it binds no target positions at all in the target
	 * 
	 * @return Boolean indicating if the entry is unaligned
	 */
	public boolean isUnalignedEntry() {
		return this.bindableTargetPositions.isEmpty();
	}

	public List<String> getSentenceSourceWords() {
		return Collections.unmodifiableList(lexicalizer.getSourceWords());
	}

	public List<String> getSentenceTargetsWords() {
		return Collections.unmodifiableList(lexicalizer.getTargetWords());
	}

	public String toString() {
		String s = "\n";
		s += "Extended BITG ChartEntry : <" + i + "," + j + ">";

		s += "\n";
		s += "\tisAligned: " + this.isAligned + "  ||  ";
		s += "isComplete : " + this.isComplete + "  ||  ";

		s += "\n dependent source positions: " + this.dependentSourcePositions;
		s += "\n bindable target positions: " + this.bindableTargetPositions;
		// loop over the derivations

		s += "\n number of nodes: " + nodeInferences.size();
		s += "\nDerivations:\n";
		for (EbitgNode key : nodeInferences.keySet()) {
			s += "\nNode: " + key + " number derivations: " + (nodeInferences.get(key).size());
			// loop over the nodeInferences for each of the nodes
			for (EbitgInference d : nodeInferences.get(key)) {
				s += "\n\t" + d.toString();
			}
		}
		return s;
	}

	public SentencePairAlignmentChunking getAlignmentChunking() {
		return this.lexicalizer.getChartInternalAlignmentChunking();
	}

	/**
	 * Method that finds the set of inferences that binds the minimum number of target positions if any inferences exist. Otherwise returns null.
	 * 
	 * @return
	 */
	private List<EbitgInference> getMinimumTargetPositionsBindingInferences() {
		if (this.hasInferences()) {
			// System.out.println("EbitgChartEntry.getMinimumTargetPositionsBindingInferences - this.hasInferences() = true");
			EbitgNode bestNode = null;
			int minExtraBoundPositions = Integer.MAX_VALUE;

			for (EbitgNode node : this.nodeInferences.keySet()) {
				int noExtraBoundPositions = node.getExtraBoundTargetPositions().size();
				if (noExtraBoundPositions < minExtraBoundPositions) {
					minExtraBoundPositions = noExtraBoundPositions;
					bestNode = node;
				}
			}

			// System.out.println("bestNode: " + bestNode);
			List<EbitgInference> result = this.getInferences(bestNode);
			// System.out.println("result: " + result);
			return result;
		}
		return null;
	}

	public EbitgInference getFirstCompleteMinimumTargetBindingInference() {

		List<EbitgInference> minimumTargetBindingInferences = getMinimumTargetPositionsBindingInferences();

		if (minimumTargetBindingInferences != null) {
			EbitgInference minimumTargetPositionsBindingInference = minimumTargetBindingInferences.get(0);

			if (this.isComplete()) {
				return minimumTargetPositionsBindingInference;
			}

		}
		/*
		 * else { System.out.println("EbitgChartEntry: minimumTargetBindingInferences is null"); }
		 */
		return null;
	}

	public int sourceSpanLength() {
		return this.getJ() - this.getI() + 1;
	}

	public FineParentRelativeReorderingLabel getParentRelativeReorderingLabel() {
		return this.parentRelativeReorderingLabel;
	}

	public void setReorderingLabel(FineParentRelativeReorderingLabel parentRelativeReorderingLabel) {
		this.parentRelativeReorderingLabel = parentRelativeReorderingLabel;
	}
}
