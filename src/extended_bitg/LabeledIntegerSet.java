/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.HashSet;

import bitg.IntegerSetMethods;

/**
 * Class that wraps an Integerset and combines it with an integer label, to be used 
 * in combination with sorting to remember the original position
 * @author gemaille
 *
 */
public class LabeledIntegerSet implements Comparable<LabeledIntegerSet>
{
	private HashSet<Integer> set;
	private int label;
	
	
	public LabeledIntegerSet(HashSet<Integer> set, int label)
	{
		this.set = set;
		this.label = label;
	}
	
	public void setLabel(int label)
	{
		this.label = label;
	}

	public int getLabel()
	{
		return this.label;
	}
	
	public HashSet<Integer> getSet()
	{
		return this.set;
	}
	
	@Override
	public int compareTo(LabeledIntegerSet set2) 
	{
		// Find minimum and maximum values of the two sets
		int set1Min = IntegerSetMethods.findMinInteger(this.getSet());
		int set1Max = IntegerSetMethods.findMaxInteger(this.getSet());
		int set2Min = IntegerSetMethods.findMinInteger(set2.getSet());
		int set2Max = IntegerSetMethods.findMaxInteger(set2.getSet());
		
		// All elements of set1 are strictly smaller than those of set2
		// implying set1 strictly follows set2
		if(set1Max < set2Min)
		{
			return  -1;
		}
		// All elements of set2 are strictly smaller than those of set1
		// implying set2 strictly follows set1
		else if(set2Max < set1Min)
		{
			return 1;
		}
		// Neither set1 nor set2 strictly precedes the other: the two sets are incomparable,
		// their ordering is arbitrary
		else
		{
			return 0;
		}
	}
}
