package extended_bitg;

import java.util.Arrays;
import java.util.List;

import extended_bitg.EbitgChartEntry.EntryType;
import extended_bitg.EbitgChartEntry.HATComplexityType;
import extended_bitg.EbitgInference.InferenceType;

public class HATComplexityTypeComputer {

	private EbitgChart theChart;

	private HATComplexityTypeComputer(EbitgChart theChart) {
		this.theChart = theChart;
	}

	public static HATComplexityTypeComputer createHAComplexityTypeComputer(
			EbitgChart theChart) {
		return new HATComplexityTypeComputer(theChart);
	}

	/**
	 * This method computes the HATComplexityType based on the representative
	 * Inference and the smaller ChartEntries.
	 * 
	 * This method uses the dynamic programming fact that for any entry in the
	 * table, the label can be computed by taking the labels of the two entries
	 * it covers and the representative Inference for the current Chart Entry.
	 * Keep in mind with this that entries may contain null values as well, this
	 * is no problem, and also these entries do not correspond directly to chart
	 * entries that have inferences (they may but do not have to). This makes
	 * that the algorithm works, by taking the maximum label over the two
	 * (overlapping) left and right sub entries, without requiring a third loop
	 * to go over split points or anything.
	 * 
	 * Crucially, we do not need to loop over different splits. Instead this
	 * algorithm works based on the fact that for every entry in the label
	 * chart, there are two sub entries that are 1 smaller ( a left and a right
	 * one). Now based on the labels of these sub entries (which may contain
	 * nulls) we can directly compute the label for the bigger chart entry. You
	 * can actually proof that this is correct by "copmlete induction" - it is a
	 * dynamic programming algorithm in the end.
	 */
	private HATComplexityType computeHATComplexityType(EbitgChartEntry entry,
			EbitgInference representativeInference) {
		HATComplexityType result = null;
		int i = entry.getI();
		int j = entry.getJ();

		EbitgChartEntry leftSmallerChartEntry = this.theChart.getChartEntry(i,
				j - 1);
		EbitgChartEntry rightSmallerChartEntry = this.theChart.getChartEntry(
				i + 1, j);

		HATComplexityType inferenceHATComplexityType = getHATComplexityTypeForLocalEbitInferenceOperator(representativeInference);
		List<HATComplexityType> hatComplexityTypesList = Arrays.asList(
				leftSmallerChartEntry.getHATComplexityType(),
				rightSmallerChartEntry.getHATComplexityType(),
				inferenceHATComplexityType);
		result = getMostComplexHATComplexityType(hatComplexityTypesList);

		// System.out.println("HATComplexityType computed for entry: " +
		// entry.getI() + ","
		// + entry.getJ() + " : " + result);
		return result;
	}

	private HATComplexityType getHATComplexityTypeForLocalEbitInferenceOperator(
			EbitgInference ebitgInference) {

		if (ebitgInference == null) {
			return HATComplexityType.INCOMPLETE;
		}

		InferenceType inferenceType = ebitgInference.getInferenceType();

		if (inferenceType.equals(InferenceType.HAT)) {
			return HATComplexityType.HAT;
		} else if (inferenceType.equals(InferenceType.PET)) {
			return HATComplexityType.PET;
		} else if (inferenceType.equals(InferenceType.BITT)) {
			if (ebitgInference.isInverted()) {
				return HATComplexityType.BITT_INVERTED;
			} else {
				return HATComplexityType.BITT_MONO;
			}
		} else if (inferenceType.equals(InferenceType.Atomic)
				|| inferenceType.equals(InferenceType.SourceNullsBinding)) {
			return HATComplexityType.ATOMIC;
		} else {
			throw new RuntimeException(
					"Error : type from outside of enumeration");
		}
	}

	private HATComplexityType getMostComplexHATComplexityType(
			List<HATComplexityType> hatComplecityTypeList) {
		if (hatComplecityTypeList.contains(HATComplexityType.HAT)) {
			return HATComplexityType.HAT;
		} else if (hatComplecityTypeList.contains(HATComplexityType.PET)) {
			return HATComplexityType.PET;
		} else if (hatComplecityTypeList
				.contains(HATComplexityType.BITT_INVERTED)) {
			return HATComplexityType.BITT_INVERTED;
		} else if (hatComplecityTypeList.contains(HATComplexityType.BITT_MONO)) {
			return HATComplexityType.BITT_MONO;
		} else if (hatComplecityTypeList.contains(HATComplexityType.ATOMIC)) {
			return HATComplexityType.ATOMIC;
		} else if (hatComplecityTypeList.contains(HATComplexityType.INCOMPLETE)) {
			return HATComplexityType.INCOMPLETE;
		} else {
			throw new RuntimeException(
					"Error : type from outside of enumeration");
		}
	}

	private boolean isAtomicOrSourceNullBindingEntry(EbitgChartEntry entry) {
		return (entry.getEntryType() == EntryType.Atomic)
				|| (entry.getEntryType() == EntryType.SourceNullsBinding);
	}

	private void addHATComplexityTypeToAtomicOrSourceNullBindingEntry(
			EbitgChartEntry entry) {
		if (!(isAtomicOrSourceNullBindingEntry(entry))) {
			throw new RuntimeException(
					"Error: trying to use addHATComplexityTypeToSpanOneChartEntry"
							+ "for chart entry that contains EntryType different than Atomic or SourceNullBinding - entry: "
							+ entry);
		}

		EbitgInference firstInference = entry
				.getFirstCompleteMinimumTargetBindingInference();
		if (firstInference == null) {
			entry.setHATComplexityType(HATComplexityType.INCOMPLETE);
		} else {
			if (firstInference.isComplete()) {
				entry.setHATComplexityType(HATComplexityType.ATOMIC);
			} else {
				entry.setHATComplexityType(HATComplexityType.INCOMPLETE);
			}
		}
	}

	public void complyteHATComplexityTypesEntireChart() {
		int chartLength = this.theChart.getSourceLength();
		for (int spanLength = 1; spanLength <= chartLength; spanLength++) {
			for (int spanBegin = 0; spanBegin <= chartLength - spanLength; spanBegin++) {
				EbitgChartEntry chartEntry = theChart.getChartEntry(spanBegin,
						spanBegin + spanLength - 1);

				if (isAtomicOrSourceNullBindingEntry(chartEntry)) {
					addHATComplexityTypeToAtomicOrSourceNullBindingEntry(chartEntry);
				} else {
					chartEntry
							.setHATComplexityType(computeHATComplexityType(
									chartEntry,
									chartEntry
											.getFirstCompleteMinimumTargetBindingInference()));
				}
			}
		}
	}
}
