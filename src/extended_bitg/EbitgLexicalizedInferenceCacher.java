/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.HashMap;
import java.util.Map;

public class EbitgLexicalizedInferenceCacher<T extends EbitgLexicalizedInference>  
{
	final Map<EbitgInference,T> tightLexicalizedInferencesCache;
	
	private EbitgLexicalizedInferenceCacher( Map<EbitgInference,T> tightLexicalizedInferencesCache)
	{
		this.tightLexicalizedInferencesCache = tightLexicalizedInferencesCache;
	}
	
	public static <T extends EbitgLexicalizedInference> EbitgLexicalizedInferenceCacher<T>    createEbitgLexicalizedInferenceCacher()
	{
		return new EbitgLexicalizedInferenceCacher<T>(new HashMap<EbitgInference, T>());
	}
	
	public void addTightEbitgLexicalizedInferenceToCache(EbitgInference rawInference, T lexicalizedInference)
	{
		this.tightLexicalizedInferencesCache.put(rawInference, lexicalizedInference);
	}
	
	private T getTightLexicalizedInferenceFromCache(EbitgInference rawInference)
	{
		return this.tightLexicalizedInferencesCache.get(rawInference);
	}
	
	public boolean cacheContainsTightEbitgLexicalizedInference(EbitgInference rawInferrence)
	{
		return this.tightLexicalizedInferencesCache.containsKey(rawInferrence);
	}
	
	public T getTightLexicalizedInference(EbitgInference rawInference)
	{
		if(cacheContainsTightEbitgLexicalizedInference(rawInference))
		{
			return getTightLexicalizedInferenceFromCache(rawInference);
		}
		
		throw new RuntimeException("EbitgLexicalizedInferenceCaher: Error: trying to get tight lexicalized inference that is not yet cached");
	}
	
}
