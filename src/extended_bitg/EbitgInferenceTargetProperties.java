/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;


import util.Span;
import java.util.HashSet;
import java.util.Set;

public class EbitgInferenceTargetProperties 
{
	// the lowest word index covered by the target range of this derivation
	protected int minTargetPos;
	// the highest word index covered by the target range of this derivation
	protected int maxTargetPos;
	
	// Aligned target positions that are  in this inference
	protected Set<Integer> basicBoundTargetPositions;
	// The extra aligned null positions
	protected Set<Integer> extraBoundTargetPositions;
	
	private  EbitgInferenceTargetProperties(int minTargetPos, int maxTargetPos, Set<Integer> basicBoundTargetPositions, Set<Integer> extraBoundTargetPositions)
	{
		this.minTargetPos = minTargetPos;
		this.maxTargetPos = maxTargetPos;
		this.basicBoundTargetPositions = basicBoundTargetPositions;
		this.extraBoundTargetPositions = extraBoundTargetPositions;
	}
	
	public static EbitgInferenceTargetProperties createEbitgInferenceTargetProperties(int minTargetPos, int maxTargetPos, Set<Integer> basicBoundTargetPositions, Set<Integer> extraBoundTargetPositions)
	{
		return new EbitgInferenceTargetProperties(minTargetPos, maxTargetPos,  basicBoundTargetPositions,extraBoundTargetPositions);
	}
	
	
	public int getMinTargetPos()
	{
		return this.minTargetPos;
	}
	
	public int getMaxTargetPos()
	{
		return this.maxTargetPos;
	}
	
	public Span getTargetSpan()
	{
		return new Span(minTargetPos,maxTargetPos);
	}
	
	public  HashSet<Integer> getBoundTargetPositions()
	{
		HashSet<Integer> boundTargetPositions = new HashSet<Integer>();
		boundTargetPositions.addAll(basicBoundTargetPositions);
		boundTargetPositions.addAll(extraBoundTargetPositions);
		return boundTargetPositions;
	}
	
	public void setBoundTargetPositions( HashSet<Integer> basicBoundTargetPositions,HashSet<Integer> extraBoundTargetPositions)
	{
		this.basicBoundTargetPositions = basicBoundTargetPositions;
		this.extraBoundTargetPositions = extraBoundTargetPositions;
	}
	
	public Set<Integer> getBasicBoundTargetPositions()
	{
		return basicBoundTargetPositions;
	}
	
	public Set<Integer> getExtraBoundTargetPositions()
	{
		return extraBoundTargetPositions;
	}
	

	
}
