/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import util.Span;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.FinePhraseCentricReorderingLabel;
import reorderingLabeling.FinePhraseCentricReorderingLabelBasic;
import reorderingLabeling.ReorderingLabel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import extended_bitg.EbitgChartEntry.HATComplexityType;
import util.Pair;
import util.ParseAndTagBracketAndLabelConverter;
import util.SpecialCharacters;
import util.Utility;
import alignment.AlignmentPair;
import alignmentStatistics.AlignmentChunk;
import alignmentStatistics.SentencePairAlignmentChunking;
import bitg.IntegerSetMethods;

/**
 * This class models the Extended Binary ITG (EBitg) inference, which is the
 * building block of HATs. During parsing, a packed chart of EbitgInference
 * objects is created, that forms a forest of HATs induced by a certain
 * alignment triple.
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class EbitgInference {
	// We compute and keep track of the relative target positions of the child
	// nodes
	// For child nodes that are complex/binary we treat them as having one
	// position, defined
	// by the covering target span. For Atomic/SourceNullBinding nodes, we
	// consider them as
	// having possibly multiple target positions. We have to do sorting to find
	// these relative
	// positions, and we can use the EntryType to determine with what kind of
	// nodes we're dealing.

	// The InferenceType is close to the implementation and is used for
	// distinguishing types from the computational point of view
	public enum InferenceType {
		SourceNullsBinding, BITT, PET, HAT, Atomic
	};

	// The InferenceComplexityType abstracts over the InfernceType, by
	// Mapping the SourceNullBinding and Atomic classes to BITT,PET or HAT
	// This type distinguishes what an Inference is from a complexity point of
	// view
	public enum InferenceComplexityType {
		BITT, PET, HAT
	};

	
	private boolean inverted;
	private InferenceType inferenceType;

	private final EbitgInferenceTargetProperties targetProperties;

	private List<EbitgNode> nodes;

	// List of pointers to the chart entries were the involved nodes are stored
	private List<EbitgChartEntry> nodeChartEntries;

	// the chart entry that contains the inference
	private final EbitgChartEntry containingChartEntry;

	EbitgInferencePermutationProperties permutationProperties;

	private final int numNotNullTargetChildren;

	public List<EbitgChartEntry> getNodeChartEntries() {
		return Collections.unmodifiableList(this.nodeChartEntries);
	}

	public List<EbitgNode> getNodes() {
		return Collections.unmodifiableList(nodes);
	}

	public InferenceType getInferenceType() {
		return inferenceType;
	}
	
	/**
	 * Get the HATComplexityType, which is the complexity of not the inference 
	 * but the entire HAT subtree rooted at this inference 
	 * @return
	 */
	public HATComplexityType getHATComplexityType(){
		return this.getContainingChartEntry().getHATComplexityType();
	}

	/**
	 * Getter for source (set)permutation
	 * The list contains unsorted sets. If you want to  get sorted sets, 
	 * which may be important for canonical labeling, use the method 
	 * 
	 * Remark that this method may not include unaligned word positions 
	 * when called from EbitgInference directly, while it will contain these 
	 * positions when called from EbitgLexicalizedInference, which re-introduces 
	 * the unaligned word positions in a process called "Lexicalization".
	 * @return The unsorted source (set) permutation
	 */
	public List<Set<Integer>> getUnsortedSourceSetPermutation() {
		return this.getPermutationProperties().getSourcePermutation();
	}

	
	/**
	 * This method returns the sorted (Set) Permutation for the EbitgInference
	 * 
	 * Remark that this method may not include unaligned word positions 
	 * when called from EbitgInference directly, while it will contain these 
	 * positions when called from EbitgLexicalizedInference, which re-introduces 
	 * the unaligned word positions in a process called "Lexicalization".
	 * 
	 * @return The sorted source (set) permutation
	 */
	public List<SortedSet<Integer>> getSortedSourceSetPermuation() {
		// Convert the permutation from a ArrayList of HashSet<Integer>
		// to an ArrayList of TreeSet<Integer>, which is sorted
		ArrayList<SortedSet<Integer>> sortedPermutation = new ArrayList<SortedSet<Integer>>();
		for (Set<Integer> set : this.getUnsortedSourceSetPermutation()) {
			sortedPermutation.add(new TreeSet<Integer>(set));
		}
		return sortedPermutation;
	}
	
	public boolean isInverted() {
		return inverted;
	}

	/**
	 * Getter for the chart entry containing the inference
	 * 
	 * @return The containing chart entry
	 */
	public EbitgChartEntry getContainingChartEntry() {
		return containingChartEntry;
	}

	/**
	 * Constructor following rules of dependency injection framework
	 * 
	 * @param nodes
	 * @param basicBoundTargetPositions
	 * @param extraBoundTargetPositions
	 * @param nodeChartEntries
	 * @param inverted
	 * @param inferenceType
	 * @param generatedTargetWords
	 */
	public EbitgInference(EbitgInferenceTargetProperties targetProperties,
			List<EbitgNode> nodes, List<EbitgChartEntry> nodeChartEntries,
			boolean inverted, InferenceType inferenceType,
			EbitgChartEntry containingChartEntry) {
		this.targetProperties = targetProperties;
		this.nodes = nodes;
		this.nodeChartEntries = nodeChartEntries;
		this.inverted = inverted;
		this.inferenceType = inferenceType;
		this.numNotNullTargetChildren = this.computeNumNotNullTargetChildren();
		this.containingChartEntry = containingChartEntry;
	}

	/**
	 * Constructor for Binary ITG derivation type
	 * 
	 * @param leftPart
	 *            The left part in the derivation (either containing chart entry
	 *            or derivation)
	 * @param rightPart
	 *            The right part in the derivation (either containing chart
	 *            entry or derivation)
	 * @param inverted
	 *            Whether or not inversion is required
	 * @return The created Binary EbitgIncerence
	 */
	public static EbitgInference createEbitgInference(EbitgNode leftPart,
			EbitgNode rightPart, List<EbitgChartEntry> nodeChartEntries,
			boolean inverted, EbitgChartEntry containingChartEntry) {
		List<EbitgNode> nodes = new ArrayList<EbitgNode>();
		nodes.add(leftPart);
		nodes.add(rightPart);
		Set<Integer> basicBoundTargetPositions = new HashSet<Integer>();
		Set<Integer> extraBoundTargetPositions = new HashSet<Integer>();

		// The bound target positions are all positions bound by the left part
		// combined with all those
		// bound by the right part
		basicBoundTargetPositions.addAll(leftPart
				.getBasicBoundTargetPositions());
		basicBoundTargetPositions.addAll(rightPart
				.getBasicBoundTargetPositions());
		extraBoundTargetPositions.addAll(leftPart
				.getExtraBoundTargetPositions());
		extraBoundTargetPositions.addAll(rightPart
				.getExtraBoundTargetPositions());

		// the new minimal target position will be the minimum of the two parts
		int maxTargetPos = Math.max(leftPart.getMaxTargetPos(),
				rightPart.getMaxTargetPos());
		// the new maximal target position will be the maximum of the two parts
		int minTargetPos = Math.min(leftPart.getMinTargetPos(),
				rightPart.getMinTargetPos());

		EbitgInferenceTargetProperties targetProperties = EbitgInferenceTargetProperties
				.createEbitgInferenceTargetProperties(minTargetPos,
						maxTargetPos, basicBoundTargetPositions,
						extraBoundTargetPositions);

		InferenceType inferenceType = InferenceType.BITT;
		EbitgInference inference = new EbitgInference(targetProperties, nodes,
				nodeChartEntries, inverted, inferenceType, containingChartEntry);

		inference.setPermutationProperties(EbitgInferencePermutationProperties
				.createPermutationProperties(inference));

		return inference;
	}

	private static int getMinTargetPosFromInvolvedParts(
			List<EbitgNode> involvedParts) {
		int minTargetPos = involvedParts.get(0).getMinTargetPos();
		for (EbitgNode part : involvedParts) {
			if (part.getMinTargetPos() < minTargetPos) {
				minTargetPos = part.getMinTargetPos();
			}
		}
		return minTargetPos;
	}

	private static int getMaxTargetPosFromInvolvedParts(
			List<EbitgNode> involvedParts) {
		int maxTargetPos = involvedParts.get(0).getMinTargetPos();
		for (EbitgNode part : involvedParts) {
			if (part.getMaxTargetPos() > maxTargetPos) {
				maxTargetPos = part.getMaxTargetPos();
			}
		}
		return maxTargetPos;
	}

	private void determineInferenceTypeForComplexInference() {
		// Determine if the permutation is a HAT (at least one source position
		// maps to multiple
		// relative target positions) of just a normal permutation (PET)
		if (permutationIsHAT(getPermutationProperties().getSourcePermutation())) {
			inferenceType = InferenceType.HAT;
		} else {
			inferenceType = InferenceType.PET;
		}
	}

	public static EbitgInference createEbitgInference(
			List<EbitgNode> involvedParts,
			List<EbitgChartEntry> nodeChartEntries,
			EbitgChartEntry containingChartEntry) {
		Set<Integer> basicBoundTargetPositions = new HashSet<Integer>();
		Set<Integer> extraBoundTargetPositions = new HashSet<Integer>();

		List<EbitgNode> nodes = new ArrayList<EbitgNode>(involvedParts);

		assert (involvedParts.size() > 0);

		// Find the minimum and maximum target position over all involved parts
		int minTargetPos = getMinTargetPosFromInvolvedParts(involvedParts);
		int maxTargetPos = getMaxTargetPosFromInvolvedParts(involvedParts);

		for (EbitgNode part : involvedParts) {
			// add the bound target positions of the part
			basicBoundTargetPositions.addAll(part
					.getBasicBoundTargetPositions());
			extraBoundTargetPositions.addAll(part
					.getExtraBoundTargetPositions());
		}

		EbitgInferenceTargetProperties targetProperties = EbitgInferenceTargetProperties
				.createEbitgInferenceTargetProperties(minTargetPos,
						maxTargetPos, basicBoundTargetPositions,
						extraBoundTargetPositions);
		EbitgInference inference = new EbitgInference(targetProperties, nodes,
				nodeChartEntries, false, null, containingChartEntry);

		inference.setPermutationProperties(EbitgInferencePermutationProperties
				.createPermutationProperties(inference));
		inference.determineInferenceTypeForComplexInference();

		return inference;
	}

	/**
	 * Function that determines if an Inference is a HAT.
	 * 
	 * Firstly checks if any source position maps to multiple target position,
	 * which is the case if any of the permutation sublists has more then 1
	 * element. In this case we have a HAT. Secondly checks if the amount of
	 * indices in the permutation of the node exceeds the amount of different
	 * indices. If this is the case it means at least two source positions must
	 * be mapping to a same target position and thus we also have a HAT in our
	 * hands.
	 * 
	 * @param permutation
	 * @return Whether the permutation is a HAT
	 */
	public static boolean permutationIsHAT(List<Set<Integer>> permutation) {

		Set<Integer> occuringIndicesSet = new HashSet<Integer>();
		List<Integer> occuringIndicesCollection = new ArrayList<Integer>();

		for (Set<Integer> targetSet : permutation) {
			if (someSomethingToManyMappingOccurs(targetSet)) {
				return true;
			}

			occuringIndicesSet.addAll(targetSet);
			occuringIndicesCollection.addAll(targetSet);
		}

		return someManyToSomethingMappingOccurs(occuringIndicesSet,
				occuringIndicesCollection);

	}

	private static boolean someSomethingToManyMappingOccurs(
			Set<Integer> targetSet) {
		// If any of the sublists of the permutation has size greater then 1,
		// meaning a mapping to more then 1 target node, we have a HAT
		if (targetSet.size() > 1) {
			return true;
		}
		return false;
	}

	private static boolean someManyToSomethingMappingOccurs(
			Set<Integer> occuringIndicesSet,
			List<Integer> occuringIndicesCollection) {
		// If there set of occurring indices has a smaller size as
		// the multiset of occurring indices, it means some target indices
		// are mapped to multiple times and this implies a HAT inference
		if (occuringIndicesSet.size() < occuringIndicesCollection.size()) {
			return true;
		}
		// We have no HAT node
		else {
			return false;
		}
	}

	/**
	 * Constructor for atomic case: length one entries
	 * 
	 * @param basicBoundTargetPositions
	 *            The basic (necessarily) bound target positions bound
	 * @param extraBoundTargetPositions
	 *            The extra (optionally) bound target positions
	 * @param containingChartEntry
	 *            : The length one chart entry that will contain the inference
	 * @return The generated atomic EbitgInference
	 */
	public static EbitgInference createEbitgInference(
			HashSet<Integer> basicBoundTargetPositions,
			HashSet<Integer> extraBoundTargetPositions,
			EbitgChartEntry containingChartEntry) {

		// assert(basicBoundTargetPositions.size() > 0);
		// Map<Integer,String> generatedTargetWords = new
		// HashMap<Integer,String>();
		List<EbitgNode> nodes = new ArrayList<EbitgNode>();
		List<EbitgChartEntry> nodeChartEntries = new ArrayList<EbitgChartEntry>();

		nodeChartEntries.add(containingChartEntry);
		InferenceType inferenceType = InferenceType.Atomic;

		Pair<Integer> minMaxTargetPos = EbitgInference
				.findMinMaxTargetPosition(basicBoundTargetPositions,
						extraBoundTargetPositions);

		EbitgInferenceTargetProperties targetProperties = EbitgInferenceTargetProperties
				.createEbitgInferenceTargetProperties(
						minMaxTargetPos.getFirst(),
						minMaxTargetPos.getSecond(), basicBoundTargetPositions,
						extraBoundTargetPositions);
		EbitgInference inference = new EbitgInference(targetProperties, nodes,
				nodeChartEntries, false, inferenceType, containingChartEntry);

		// inference.computePermutationOrder(); // Gideon : replaced by atomic
		// case creator
		inference.setPermutationProperties(EbitgInferencePermutationProperties
				.createAtomicPermutationProperties(inference
						.getBoundTargetPositions()));
		// System.out.println("EbitgInference. Atomic Permutation order: " +
		// EbitgInference.permutationString(inference.sourcePermutation,true));

		return inference;
	}

	public static Pair<Integer> findMinMaxTargetPosition(
			HashSet<Integer> basicBoundTargetPositions,
			HashSet<Integer> extraBoundTargetPositions) {

		Set<Integer> boundTargetPositions = new HashSet<Integer>();
		boundTargetPositions.addAll(basicBoundTargetPositions);
		boundTargetPositions.addAll(extraBoundTargetPositions);

		// Determine the minimum and maximum target positions and set them
		int minTargetPos = IntegerSetMethods
				.findMinInteger(boundTargetPositions);
		int maxTargetPos = IntegerSetMethods
				.findMaxInteger(boundTargetPositions);

		return new Pair<Integer>(minTargetPos, maxTargetPos);
	}

	/**
	 * Constructor for source null binding entries
	 * 
	 * @param sourceBinding
	 *            The containing chart entry for the Inference to be generated
	 * @param alignedEntry
	 *            The sub Chart entry that is aligned to a target position
	 *            within the containing chart entry
	 * @param node
	 *            : The node that is aligned in the source null-binding chart
	 *            entry. This corresponds to a choice of a set of aligned target
	 *            words, some fixed ands some extra bound resulting from the set
	 *            of un-bound target words
	 * @return The generated Source Null binding EbitgInference
	 */
	public static EbitgInference createEbitgInference(
			EbitgChartEntry sourceBinding, EbitgChartEntry alignedEntry,
			EbitgNode node) {
		List<EbitgNode> nodes = new ArrayList<EbitgNode>();
		List<EbitgChartEntry> nodeChartEntries = new ArrayList<EbitgChartEntry>();
		InferenceType inferenceType = InferenceType.SourceNullsBinding;
		Set<Integer> basicBoundTargetPositions = new HashSet<Integer>(
				node.getBasicBoundTargetPositions());
		Set<Integer> extraBoundTargetPositions = new HashSet<Integer>(
				node.getExtraBoundTargetPositions());
		nodeChartEntries.add(sourceBinding);

		// Chart entries of size 1 should have only one inference per node,
		// otherwise something
		// is wrong and the code after this assertion will fail
		assert (alignedEntry.getInferences(node).size() == 1);

		// Since there are no extra target entries bound, the min and max target
		// index
		// are simply copied from the bound entry
		EbitgInferenceTargetProperties targetProperties = EbitgInferenceTargetProperties
				.createEbitgInferenceTargetProperties(node.getMinTargetPos(),
						node.getMaxTargetPos(), basicBoundTargetPositions,
						extraBoundTargetPositions);

		EbitgInference inference = new EbitgInference(targetProperties, nodes,
				nodeChartEntries, false, inferenceType, sourceBinding);

		inference.setPermutationProperties(EbitgInferencePermutationProperties
				.createAtomicPermutationProperties(inference
						.getBoundTargetPositions()));
		// inference.computePermutationOrderAnNumberOfTargetPositionsForPermutationIndices();

		return inference;
	}

	public Set<Integer> getBoundTargetPositions() {
		return Collections.unmodifiableSet(this.targetProperties
				.getBoundTargetPositions());
	}

	public Set<Integer> getBasicBoundTargetPositions() {
		return Collections.unmodifiableSet(this.targetProperties
				.getBasicBoundTargetPositions());
	}

	public Set<Integer> getExtraBoundTargetPositions() {
		return Collections.unmodifiableSet(this.targetProperties
				.getExtraBoundTargetPositions());
	}

	public static void addSelection(Map<Integer, String> goalMap,
			List<String> sourceArray, Set<Integer> selection) {
		// HashMap entries that link the selected positions to their
		// corresponding words
		for (int pos : selection) {
			goalMap.put(pos, sourceArray.get(pos));
		}
	}

	public static String treeString(List<String> targetWords,
			InferenceType type, List<SortedSet<Integer>> permutation) {
		String result = "";

		result += sortedPermutationString(permutation);

		if ((type == InferenceType.SourceNullsBinding)
				|| (type == InferenceType.Atomic)) {
			result += "=>"
					+ Utility
							.stringListString(ParseAndTagBracketAndLabelConverter
									.getStringsWithReplacedBrackets(targetWords));
		}
		return result;
	}

	public static String addEnclosingBrackets(String withoutBrackets) {
		String result = withoutBrackets;
		result = SpecialCharacters.LeftSquareBracket + result + SpecialCharacters.RightSquareBracket;
		return result;
	}

	public List<Set<Integer>> getChildrenBoundTargetPositions() {
		List<Set<Integer>> childBoundTargetPositions = new ArrayList<Set<Integer>>();

		for (EbitgNode node : this.getNodes()) {
			childBoundTargetPositions.add(node.getBoundTargetPositions());
		}

		return childBoundTargetPositions;
	}

	public List<Set<Integer>> getChildrenBasicBoundTargetPositions() {
		List<Set<Integer>> childrenBasicBoundTargetPositions = new ArrayList<Set<Integer>>();

		for (EbitgNode node : this.getNodes()) {
			childrenBasicBoundTargetPositions.add(node
					.getBasicBoundTargetPositions());
		}

		return childrenBasicBoundTargetPositions;
	}

	public List<Set<Integer>> getChildrenExtraBoundTargetPositions() {
		List<Set<Integer>> childrenExtraBoundTargetPositions = new ArrayList<Set<Integer>>();

		for (EbitgNode node : this.getNodes()) {
			childrenExtraBoundTargetPositions.add(node
					.getExtraBoundTargetPositions());
		}
		return childrenExtraBoundTargetPositions;
	}

	/**
	 * This method checks whether an inference is complete. The primary
	 * completeness information is computed in the chart entries containing the
	 * inferences, these also store the required source positions and bindable
	 * target positions. However, it may be necessary for visualization purposes
	 * to know whether a certain Inference is complete by checking whether the
	 * inference is contained in a complete chart entry and also has a
	 * consecutive range of bound target positions
	 * 
	 * Remark that this method should only be called for Inferences that have
	 * already been properly generated. I.e. it is not meant to use for checking
	 * whether an inference is legal or not
	 * 
	 * @return
	 */
	public boolean isComplete() {
		// The completeness of atomic and source null binding entries
		if ((this.inferenceType == InferenceType.Atomic)
				|| (this.inferenceType == InferenceType.SourceNullsBinding)) {
			EbitgChartEntry containingChartEntry = this.nodeChartEntries.get(0);
			boolean targetPositionsAreComplete = IntegerSetMethods
					.formsConsecutiveRange(this.getBoundTargetPositions());
			return (targetPositionsAreComplete && containingChartEntry
					.isComplete());
		}
		{
			// An inference that is not atomic or source building must be
			// complete, otherwise it could not have
			// been legally generated
			return true;
		}
	}

	public boolean isComplex() {
		return ((this.inferenceType == InferenceType.HAT) || (this.inferenceType == InferenceType.PET));
	}

	public int getNumNotNullSourceChildren() {
		return this.nodes.size();
	}

	public int getNumNotNullTargetChildren() {
		return this.numNotNullTargetChildren;
	}

	private int computeNumNotNullTargetChildren() {
		int result = 0;
		Set<Integer> outsideGapBasicBoundTargetPositions = new HashSet<Integer>();

		if (this.nodes.size() > 0) {
			for (int i = 0; i < this.nodes.size(); i++) {
				EbitgNode node = this.nodes.get(i);
				EbitgChartEntry chartEntry = this.nodeChartEntries.get(i);

				if (chartEntry.isComplete()) {
					result += 1;
				} else {
					outsideGapBasicBoundTargetPositions.addAll(node
							.getBasicBoundTargetPositions());
				}
			}
		}
		// If the Inference is itself Atomic or SourceNullBinding it has no
		// nodes, but could
		// still have more than one child on the target, hence then we count
		// those directly
		else {
			outsideGapBasicBoundTargetPositions.addAll(this
					.getBasicBoundTargetPositions());

		}
		// The final result is the number of complete gap entries (=
		// variables/gaps)
		// plus the number of basic bound target positions outside these gaps
		result += outsideGapBasicBoundTargetPositions.size();
		return result;
	}

	public int numTargetChildren() {
		int maxTargetIndex = -1;

		for (Set<Integer> sourceToTargetMapping : this
				.getPermutationProperties().getSourcePermutation()) {
			for (int relativeTargetPos : sourceToTargetMapping) {
				maxTargetIndex = Math.max(maxTargetIndex, relativeTargetPos);
			}
		}
		return maxTargetIndex + 1;
	}

	private SentencePairAlignmentChunking getAlignmentChunking() {
		return this.containingChartEntry.getAlignmentChunking();
	}

	private int getFirstSourcePosition() {
		return this.containingChartEntry.getI();
	}

	private int getLastSourcePosition() {
		return this.containingChartEntry.getJ();
	}

	/*
	 * 
	 * private AlignmentChunk getFirstNonZeroAlignmentChunk() { for(int
	 * sourcePos = getFirstSourcePosition(); sourcePos <=
	 * getLastSourcePosition(); sourcePos++) { AlignmentChunk alignmentChunk =
	 * this
	 * .getAlignmentChunking().getAlignmentChunkForSourcePosition(sourcePos);
	 * if(!alignmentChunk.isUnalignedChunk()) { return alignmentChunk; } } throw
	 * new RuntimeException("No nonzero alignment chunk was found"); }
	 * 
	 * 
	 * public boolean allContainedPositionsAreInSameContinousAlignmentChunk() {
	 * //System.out.println(
	 * ">>>EbitgInference.allContainedPositionsAreInSameCompleteAlignmentChunk called"
	 * );
	 * 
	 * boolean allContainedPositionsAreInSameAlingmentChunk =
	 * this.allContainedAlignedPositionsAreInSameAlignmentChunk();
	 * 
	 * if(allContainedPositionsAreInSameAlingmentChunk) { AlignmentChunk chunk =
	 * this.getFirstNonZeroAlignmentChunk();
	 * 
	 * return chunk.isContinuousChunk(); //return chunk.hasContinuousSide();
	 * //return (chunk.isCompleteChunk() && chunk.isContinuousChunk()); } return
	 * false; }
	 */

	public boolean allContainedAlignedPositionsAreInSameAlignmentChunk() {
		return (getNonZeroAlignmentChunks().size() == 1);
	}

	private Set<AlignmentChunk> getNonZeroAlignmentChunks() {
		Set<AlignmentChunk> result = new HashSet<AlignmentChunk>();

		for (int sourcePos = getFirstSourcePosition(); sourcePos <= getLastSourcePosition(); sourcePos++) {
			AlignmentChunk alignmentChunk = this.getAlignmentChunking()
					.getAlignmentChunkForSourcePosition(sourcePos);

			if (!alignmentChunk.isUnalignedChunk()) {
				result.add(alignmentChunk);
			}
		}
		return result;
	}

	public EbitgInferenceTargetProperties getTargetProperties() {
		return targetProperties;
	}

	public static String sortedPermutationString(
			List<SortedSet<Integer>> permutation) {
		String result = SpecialCharacters.LeftSquareBracket;

		Iterator<SortedSet<Integer>> iterator = permutation.iterator();

		SortedSet<Integer> elements;
		while (iterator.hasNext()) {
			elements = iterator.next();
			result += getPermutationSetStringStartingFromOne(elements);

			if (iterator.hasNext()) {
				result += ",";
			}
		}

		result += SpecialCharacters.RightSquareBracket;
		return result;
	}
		

	private static String getPermutationSetStringStartingFromOne(
			Set<Integer> elements) {
		boolean addBrackets = (elements.size() > 1);
		return IntegerSetMethods.setString(elements, -1, addBrackets);
	}

	public static String permutationString(List<Set<Integer>> permutation) {
		String result = "";

		Iterator<Set<Integer>> iterator = permutation.iterator();

		Set<Integer> elements;
		while (iterator.hasNext()) {
			elements = iterator.next();
			result += getPermutationSetStringStartingFromOne(elements);

			if (iterator.hasNext()) {
				result += ",";
			}
		}

		return result;
	}

	public int getMinTargetPos() {
		return this.targetProperties.getMinTargetPos();
	}

	public int getMaxTargetPos() {
		return this.targetProperties.getMaxTargetPos();
	}

	public Span getTargetSpan() {
		return new Span(this.getMinTargetPos(), this.getMaxTargetPos());
	}

	public List<SortedSet<Integer>> computeSortedInvertedPermutation() {
		return this.getPermutationProperties()
				.computeSortedInvertedPermutation();
	}

	public List<AlignmentPair> getCoveredAlignments() {
		return this.getAlignmentChunking().getSourceSpanCoveredAlignments(
				new Pair<Integer>(this.getFirstSourcePosition(), this
						.getLastSourcePosition()));
	}

	public String toString() {
		String result = "";
		result += "\n<ExtendedBITGDerivation>";

		result += "\nminTargetPos: " + this.getMinTargetPos()
				+ " maxTargetPos: " + this.getMaxTargetPos();
		result += "\nDerivation type: " + this.inferenceType;
		result += "\nBasic Bound target positions: "
				+ this.getBasicBoundTargetPositions();
		result += "\nExtra Bound target positions: "
				+ this.getExtraBoundTargetPositions();
		result += "\nBound target positions: " + this.getBoundTargetPositions();

		result += "\n node source ranges: ";
		for (EbitgChartEntry c : nodeChartEntries) {
			result += "\t(" + c.getI() + "," + c.getJ() + ")";
		}

		result += "\n Derivation parts:";
		int i = 1;
		for (EbitgNode part : nodes) {
			result += "part " + i + ": " + part + "\t";
			i++;
		}

		result += "\n</ExtendedBITGDerivation>>";
		return result;
	}

	public boolean containsCompleteSubPhrase() {
		boolean result = false;

		if (this.getNodeChartEntries().size() > 1) {
			for (EbitgChartEntry childChartEntry : this.getNodeChartEntries()) {
				if (childChartEntry.isComplete()) {
					result = true;
					break; // one complete sub-phrase is sufficient
				}
			}
		}
		return result;
	}

	public boolean isMinimalPhrasePairInference() {
		return !containsCompleteSubPhrase() && isComplete();
	}

	public boolean isOfInferenceType(InferenceType inferenceType) {
		return getInferenceType().equals(inferenceType);
	}

	public EbitgInferencePermutationProperties getPermutationProperties() {
		return permutationProperties;
	}

	public void setPermutationProperties(
			EbitgInferencePermutationProperties permutationProperties) {
		this.permutationProperties = permutationProperties;
	}

	public FineParentRelativeReorderingLabel getParentRelativeReorderingLabel() {
		return this.getContainingChartEntry()
				.getParentRelativeReorderingLabel();
	}

	public FinePhraseCentricReorderingLabelBasic getFinePhraseCentricReorderingLabel() {
		return (FinePhraseCentricReorderingLabelBasic) FinePhraseCentricReorderingLabelBasic.createFinePhraseCentricReorderingLabelBasic()
				.getReorderingLabelForInference(this);
	}
}