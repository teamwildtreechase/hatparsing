/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_database.HATSelectionPanel;

import java.util.Arrays;
import java.util.List;

public class EbitgViewerExampleExporterBot implements Runnable
{
	private HATViewer ebitgViewer;
	private String monochromeOutputFolder, colorOutputFolder;
	private String languagePairTableName;
	List<Integer> selectedExampleNumbersList;
	private final boolean closeApplicationAfterRunning;
	
	private EbitgViewerExampleExporterBot (HATViewer ebitgViewer, String monochromeOutputFolder, String colorOutputFolder, String languagePairTableName, List<Integer> selectedExampleNumbersList, boolean closeApplicationAfterRunning)
	{
		this.ebitgViewer = ebitgViewer;
		this.monochromeOutputFolder = monochromeOutputFolder;
		this.colorOutputFolder = colorOutputFolder;
		this.languagePairTableName = languagePairTableName;
		this.selectedExampleNumbersList = selectedExampleNumbersList;
		this.closeApplicationAfterRunning = closeApplicationAfterRunning;
	}
	
	public static EbitgViewerExampleExporterBot createEbitgViewerExampleExporterBot(String monochromeOutputFolder, String colorOutputFolder, String languagePairTableName, List<Integer> selectedExampleNumbersList, boolean closeApplicationAfterRunning)
	{
		return new EbitgViewerExampleExporterBot(HATViewer.createAndShowGUI(), monochromeOutputFolder, colorOutputFolder, languagePairTableName, selectedExampleNumbersList, closeApplicationAfterRunning);
	}
	
	
	public void exportExamplesMonochromeAndColor()
	{
		ebitgViewer.performSelectionOnAlignmentTripleNumbers(this.languagePairTableName, this.selectedExampleNumbersList);
		ebitgViewer.setNoColors(true);
		exportExamples(monochromeOutputFolder);
		 
		ebitgViewer.setNoColors(false);
		exportExamples(colorOutputFolder);
	}
	
	
	
	public String getLanguageString()
	{
		if(this.languagePairTableName.equals(HATSelectionPanel.TableNameEnglishDutch))
		{
			return "en-ne";
		}
		else if(this.languagePairTableName.equals(HATSelectionPanel.TableNameEnglishGerman))
		{
			return "en-de";
		}
		else if (this.languagePairTableName.equals(HATSelectionPanel.TableNameEnglishFrench))
		{
			return "en-fr";
		}
		else
		{
			throw new RuntimeException("Unexpected and unrecognized languagePairTableName");
		}
	}
	
	private String generateBasicOuputName(int exampleNumber)
	{
		return "example-" +  getLanguageString() + "-" + exampleNumber; 
	}
	
	public String generateOutputName(String typeString, int exampleNumber, String outputFolder)
	{
		if(typeString != null && typeString.length() > 0 )
		{	
			return outputFolder + generateBasicOuputName(exampleNumber) + "-" + typeString + ".pdf";
		}
		return outputFolder + generateBasicOuputName(exampleNumber) +  ".pdf";
	}
	
	public void exportExamples(String outputFolder)
	{
		ebitgViewer.loadEveryThingFromConfigFile();
		
		for(int i = 1; i <= ebitgViewer.getMaxAlignmentSpinnerValue(); i++)
		{	
			ebitgViewer.loadAlignmentTriple(i);
			ebitgViewer.computeHATs();
			int alignmentTripleNumber = ebitgViewer.getCurrentAlignmentTripleNumber();
			System.out.println(">>>>>>>>>>>>>Alignment Triple number: " + alignmentTripleNumber);
			
			
			ebitgViewer.setShowAlignmentSets(false);
			ebitgViewer.exportAlignmentToFile(generateOutputName("alignments", alignmentTripleNumber,outputFolder ) );
			
			ebitgViewer.setShowAlignmentSets(true);
			ebitgViewer.exportAlignmentToFile(generateOutputName("alignments-extra", alignmentTripleNumber,outputFolder ));
			
			ebitgViewer.exportHATToFile(generateOutputName("", alignmentTripleNumber,outputFolder ));
		}	
	}
	
	public void quit()
	{
		ebitgViewer.quit();
	}
	
	public static void exportSelectedExamples( String monochromeOutputFolder, String colorOutputFolder, String languagePairTableName, List<Integer> selectedExampleNumberssList, boolean closeApplicationAfterRunning)
	{
		EbitgViewerExampleExporterBot exporterBot = EbitgViewerExampleExporterBot.createEbitgViewerExampleExporterBot(monochromeOutputFolder, colorOutputFolder, languagePairTableName, selectedExampleNumberssList, closeApplicationAfterRunning);
		
		// This is necessary since repaint is not thread safe,
		// see http://stevensrmiller.com/wordpress/?p=567
		// IF this is not done, pending repainting tasks will be executed in a non-thread safe way 
		// with the rest of the code executed in this class, leading to errors
		exporterBot.ebitgViewer.startRunnableThreadSafe(exporterBot);
	}
	
	public static void main(String[] args)
	{
		String examplesFolder = "/home/gemaille/Desktop/alignmentExamples/";
		String monochromeOutputFolder = examplesFolder + "monochrome/";
		String colorOutputFolder = examplesFolder + "color/"; 

		String languagePairTableName = HATSelectionPanel.TableNameEnglishDutch;
		Integer[] selectedExamples = new Integer[]{5309,6213,9455};
		List<Integer> selectedExampleNumbersList = Arrays.asList(selectedExamples);
		exportSelectedExamples(monochromeOutputFolder, colorOutputFolder, languagePairTableName, selectedExampleNumbersList,false);
		
		languagePairTableName = HATSelectionPanel.TableNameEnglishFrench;
		selectedExamples = new Integer[]{192,294,204723,221315,754908};
		selectedExampleNumbersList = Arrays.asList(selectedExamples);
		exportSelectedExamples(monochromeOutputFolder, colorOutputFolder, languagePairTableName, selectedExampleNumbersList,false);
		
		languagePairTableName = HATSelectionPanel.TableNameEnglishGerman;
		selectedExamples = new Integer[]{13491,25145,4131};
		selectedExampleNumbersList = Arrays.asList(selectedExamples);
		exportSelectedExamples(monochromeOutputFolder, colorOutputFolder, languagePairTableName, selectedExampleNumbersList,true);

	}

	@Override
	public void run() 
	{
		this.exportExamplesMonochromeAndColor();
		if(closeApplicationAfterRunning)
		{	
			this.quit();
		}	
		
	}
}
