/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_lexicalization.Lexicalizer;

import java.util.HashSet;
import java.util.List;

public class EbitgChartInitializerAllNullBindings extends EbitgChartInitializer
{

	public EbitgChartInitializerAllNullBindings(EbitgChart theChart,
			Lexicalizer lexicalizer) 
	{
		super(theChart, lexicalizer);
	}

	
	protected  HashSet<HashSet<Integer>> findLeftFreePositionsSets(int targetPosition)
	{
		List<Integer> leftFreeTargetPositions = findDirectLeftFreeTargetPositionsRightToLeft(targetPosition);
		return findAllLeftStartingConsectutiveSubLists(leftFreeTargetPositions); 
	}
	
	protected  HashSet<HashSet<Integer>> findRightFreePositionsSets(int targetPosition)
	{
		List<Integer> rightFreeTargetPositions = findDirectRightFreeTargetPositionsLeftToRight(targetPosition);
		return findAllLeftStartingConsectutiveSubLists(rightFreeTargetPositions); 
	}
	
	public static HashSet<HashSet<Integer>> findAllLeftStartingConsectutiveSubLists(List<Integer> list)
	{
		HashSet<HashSet<Integer>> result = new HashSet<HashSet<Integer>>();
		
		HashSet<Integer> sublist = new HashSet<Integer>();
		
		// First add a copy of the empty sublist as the first element
		result.add(new HashSet<Integer>(sublist));
		
		for(int i = 0; i <list.size(); i++)
		{
			// Add one more element to the growing sublist
			sublist.add(list.get(i));
			// add a copy of sublist
			result.add(new HashSet<Integer>(sublist));
		}		
		return result;
	}


	@Override
	protected boolean isAcceptableTypeOfSourceNullBinding(int i, int j) 
	{
		// All valid source null bindings are acceptable
		return true;
	}
	
}
