/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

// Class that grows a binary tree from the chart
public class EbitgTreeGrower<T extends EbitgLexicalizedInference,T2 extends EbitgTreeState<T, T2>> 
{
	private T2 rootState;
	private final EbitgTreeStateChartBacking<T,T2> treeStateChartBacking;
	
	
	public  EbitgTreeGrower(T2 rootState, EbitgTreeStateChartBacking<T,T2> treeStateChartBacking)
	{
		this.rootState = rootState;
		this.treeStateChartBacking = treeStateChartBacking;
	}
	
	
	
	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T,T2>>  EbitgChartEntry getRootChartEntry(EbitgTreeStateChartBacking<T,T2> treeChartBacking)
	{
		return treeChartBacking.getChart().getChartEntry(0,treeChartBacking.getChart().getSourceLength() -1);
	}
	
	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T,T2>>  EbitgNode getRootNode(EbitgTreeStateChartBacking<T,T2> treeChartBacking)
	{
		EbitgNode  rootNode = getRootChartEntry(treeChartBacking).getNodes().get(0);
		return rootNode;
	}
	
	public static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T,T2>> EbitgTreeGrower<T,T2> createEbitgTreeGrower(EbitgTreeStateChartBacking<T,T2> treeChartBacking, T2 rootState)
	{
		//System.out.println("Entered EbitgTreeGrowerConstructor");
	
		// create the root EbitgTreeState
		
		// Make the tree complete, that is generate recursively the first tree
		rootState.makeTreeComplete();
		
		EbitgTreeGrower<T,T2> treeGrower = new EbitgTreeGrower<T,T2>(rootState, treeChartBacking); 
		
		return treeGrower;
	}
	
	public static  EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> createEbitgTreeGrower(EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder)
	{
		EbitgTreeStateChartBacking<EbitgLexicalizedInference,EbitgTreeStateBasic> treeChartBacking = EbitgTreeStateChartBackingBasic.createEbitgTreeStateChartBackingBasic(chartBuilder); 
		EbitgTreeStateBasic rootState = EbitgTreeStateBasic.createEbitgTreeStateBasic(treeChartBacking,getRootNode(treeChartBacking),getRootChartEntry(treeChartBacking).getInferences(getRootNode(treeChartBacking)),null,0,0);
		return createEbitgTreeGrower(treeChartBacking,rootState);
	}
	
	
	 public boolean hasNullAlignments()
	 {
		 return this.treeStateChartBacking.hasNullAlignments();
	 }
	 
	 public int getSourceLength()
	 {
		 return this.treeStateChartBacking.getSourceLength();
	 }
	 
	 public int getTargetLength()
	 {
		 return this.treeStateChartBacking.getTargetLength();
	 }
	
	public String getTreeString()
	{
		return getRootState().treeRepresentation.getTreeString();
	}
	
	public String getTwinTreeString()
	{
		return getRootState().treeRepresentation.getTwinTreeString();
	}
		
	
	public boolean generateNextTree()
	{
		boolean result = getRootState().generateNextInference();
		getRootState().makeTreeComplete();
		return result;
	}
	
	
	public void writeAllPossibleTreesToFile(String fileName)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
			writer.write("\n" + getTreeString());	
			
			// while next trees could be generated
			while(this.generateNextTree())
			{
				writer.write("\n" + getTreeString());	
			}
			
			writer.close();
		
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void writeAllPossibleTreePairsToFile(String fileName)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
			
			do
			{	
				// First tree already generated upon initialization
				writer.write("\n" + getTreeString());	
				
				writer.write("\n" + getTwinTreeString());		
			}	
			// while next trees could be generated
			while(this.generateNextTree());
			
			
			writer.close();
		
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void writeFirstPossibleTreePairToFile(String fileName)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
			
			// First tree already generated upon initialization
			writer.write("\n" + getTreeString());	
			
			writer.write("\n" + getTwinTreeString());		
			
			writer.close();
		
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	public static void writeEmptyFile(String fileName)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			writer.write("");	
			writer.close();
		
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}

	

	public void setRootState(T2 rootState) {
		this.rootState = rootState;
	}

	public T2 getRootState() {
		return rootState;
	}
	
	
	public EbitgChart getChart()
	{
		//System.out.println("EbitgTreeGrower.getChart: " + chartBuilder.getChart());
		return treeStateChartBacking.getChart();
	}
	
}
