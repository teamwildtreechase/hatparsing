/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;
import java.util.Set;

import hat_lexicalization.Lexicalizer;

public abstract class EbitgTreeStateChartBacking<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>>  
{
	private final EbitgChartBuilder<T,T2> chartBuilder;
	private final Lexicalizer lexicalizer;
	
	protected EbitgTreeStateChartBacking(EbitgChartBuilder<T,T2> chartBuilder, Lexicalizer lexicalizer)
	{
		this.chartBuilder = chartBuilder;
		this.lexicalizer = lexicalizer;
	}
	

	private EbitgLexicalizedInference createLexicalizedInferenceFromRawInference(EbitgInference rawInference)
	{
		return lexicalizer.createLexicalizedInference(rawInference);
	}
	
	
	public T createLabelEnrichedInferenceFromLexicalizedInference(EbitgInference rawInference)
	{
		return chartBuilder.createLabelEnrichedInferenceFromLexicalizedInference(createLexicalizedInferenceFromRawInference(rawInference));
	}
	
	protected String getGeneratedTargetWord(int absoluteTargetPosition)
	{
		return this.lexicalizer.getTargetWord(absoluteTargetPosition);
	}
	
	public EbitgChart getChart()
	{
		return this.chartBuilder.getChart();
	}
	
	 public boolean hasNullAlignments()
	 {
		 return this.chartBuilder.hasNullAlignments();
	 }
	 
	 public int getSourceLength()
	 {
		 return this.chartBuilder.getSourceLength();
	 }
	 
	 public int getTargetLength()
	 {
		 return this.chartBuilder.getTargetLength();
	 }
	 
	 public Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos)
	 {
			return this.lexicalizer.getOriginalPlusAssociatedExtraBoundTargetPositions(compactedTargetPos);
	 }
}
