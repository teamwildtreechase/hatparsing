/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import util.ConfigFile;

/**
 * This class serves to keep track of the number of an source-target-alignment triple
 * 
 * @author gemaille
 * 
 */
public class NumberKeeper {
	private int currentIndex;
	private ArrayList<Integer> numbers;

	private NumberKeeper(ArrayList<Integer> numbers, int currentIndex) {
		this.numbers = numbers;
		this.currentIndex = currentIndex;
	}

	private void addNumbersFromFile(String numbersFile) {
		BufferedReader numbersReader = null;
		try {
			numbersReader = new BufferedReader(new FileReader(numbersFile));

			// skip the first line containing the labels
			String numberLine = numbersReader.readLine();

			while ((numberLine = numbersReader.readLine()) != null) {
				String[] parts = numberLine.split(",");

				int number = Integer.parseInt(parts[0].trim());
				numbers.add(number);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				numbersReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static NumberKeeper createNumberKeeper() {
		return new NumberKeeper(new ArrayList<Integer>(), 0);
	}

	public static NumberKeeper getNumberKeeperFromConfig(String configFilePath) {
		NumberKeeper numberKeeper = createNumberKeeper();

		try {
			ConfigFile theConfig = new ConfigFile(configFilePath);
			String selectedSubCorpusLocation = theConfig.getStringIfPresentOrReturnDefault("selectedSubCorpusLocation", "selected-corpus/");
			String numberFileName = theConfig.getStringIfPresentOrReturnDefault("selectedHATsFileName", null);

			if (numberFileName == null) {
				return numberKeeper;
			}

			String numberFileLocation = selectedSubCorpusLocation + numberFileName;
			numberKeeper.addNumbersFromFile(numberFileLocation);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return numberKeeper;
	}

	public int getCurrentNumber() {
		if (currentIndex > (numbers.size() - 1)) {
			System.out.println("NumberKeeper: Warning - no number in table for this index");
			return -1;
		}

		return numbers.get(currentIndex);
	}

	public void nextNumber() {
		if (this.currentIndex < numbers.size() - 1) {
			this.currentIndex++;
		}
	}

	public void previousNumber() {
		if (this.currentIndex > 0) {
			this.currentIndex--;
		}
	}

	public void goToIndex(int index) {
		if (index < numbers.size()) {
			this.currentIndex = index;
		}
	}

	public boolean isEmpty() {
		return this.numbers.isEmpty();
	}
}
