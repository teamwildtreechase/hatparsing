/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_database.DatabaseInterface;
import hat_database.HATSelectionPanel;
import hat_database.HATsDatabaseBuilderPanel;
import hat_database.HatsDatabaseAutomaticConfigfileCreater;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import alignment.Alignment;
import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SymmetricTreeStatisticsComputation;
import tsg.TSNodeLabel;
import util.ConfigCacher;
import util.InputReading;
import util.Pair2;
import viewer.EmptyExtraTreeNodePropertiesVisualizer;
import viewer.LabeledAlignmentPanel;
import viewer_io.SpinnerPanel;
import viewer.TreePairPanel;
import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.EbitgCheckBoxTable;
import viewer_io.ExporterPDF;
import viewer_io.MenuInteraction;
import viewer_io.SentencesAndAlignmentsLoader;
import viewer_io.TreePairBankLoader;
import viewer_io.ViewerInputLoading;
import viewer.TreePairViewer;
import static util.ConfigCacher.createConfigCacher;
import static extended_bitg.HATPairPanel.createHATPairPanel;
import extended_bitg.EbitgTreeState.TreeType;

// SCROLLBARS INFO: 
//http://www.javakb.com/Uwe/Forum.aspx/java-gui/4544/Added-JTree-to-JScrollPane-but-no-Hori-Scrollbars

@SuppressWarnings("serial")
public class HATViewer extends TreePairViewer implements SentencesAndAlignmentsLoader {
	public static final int MAX_ALLOWED_INFERENCES_PER_NODE = 100000;
	// private static final boolean USE_GREEDY_NULL_BINDING = true;

	public static final String SourceKeyString = "SourcetComponent";
	public static final String TargetKeyString = "TargetComponent";
	public static final String AlignmentKeyString = "AlignmentComponent";
	public static final String ConfigKeyString = "ConfigComponent";

	/*
	 * public static final String sourceInitial = "don't want it"; public static final String targetInitial = "ne le veux pas";Che public static final String
	 * alignmentInitial = "0-0 0-3 1-2 2-1";
	 */
	/*
	 * public static final String sourceInitial = "s1 s2 s3 s4"; public static final String targetInitial = "t1 t2 t3 t4 t5"; public static final String
	 * alignmentInitial = "0-0 1-1 1-2 1-3 2-4 3-4";
	 */

	public static final String sourceInitial = "the man walks fast to his car";
	public static final String targetInitial = "de man loopt snel naar zijn auto";
	public static final String alignmentInitial = "0-0 1-1 2-2 3-3 4-4 5-5 6-6";

	protected final String HatSpinnerLabel = "HAT :";

	public static final boolean atomicPartsMustAllShareSameAlignmentChunk = true;
	public final EbitgTreeStatisticsComputation treeStatisticsComputation = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);

	private final LabeledAlignmentPanel alignmentPanel;
	final ConfigCacher theConfigCacher;

	JTabbedPane tabbedPane;
	private HATSelectionPanel hatSelectionPanel;
	protected HATsDatabaseBuilderPanel hatsDatabaseBuilderPanel;

	// A NumberKeeper attribute: keeps the number of the current source-target-alignment triple,
	// being its position in the original corpus from which it was extracted
	NumberKeeper numberKeeper = NumberKeeper.createNumberKeeper();
	MenuInteraction menuInteraction;

	protected final SpinnerPanel alignmentTripleSpinnerPanel;

	// We keep a variable that locks the value of the alignments for the spinner, while
	// computation of a set of HATs is taking place, to avoid inconsistency between computed
	// trees and the alignments that are used to draw lines between those trees
	private boolean alignmentsLocked = false;

	public static TSNodeLabel initialSentence() {
		try {
			return new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static HATViewer createEbitgViewer() {

		EbitgCheckBoxTable checkBoxTable = EbitgCheckBoxTable.createEbitgCheckBoxTable();

		ConfigCacher configCacher = createConfigCacher("ebitgViewerCachedConfig");

		// Set up the drawing area.
		HATPairPanel treePairPanel = createHATPairPanel(null, checkBoxTable, EmptyExtraTreeNodePropertiesVisualizer.createEmptyExtraTreeNodePropertiesVisualizer());
		treePairPanel.setBackground(Color.white);
		treePairPanel.setFocusable(false);

		LabeledAlignmentPanel alignmentPanel = LabeledAlignmentPanel.createLabeledAlignmentPanel(checkBoxTable);
		alignmentPanel.initializePanel();
		HATViewer ebitgViewer = new HATViewer(ButtonIOComponentTable.createButtonIOComponentTable(), alignmentPanel, configCacher, treePairPanel, checkBoxTable);

		configCacher.registerEntry("configFileLocation", "!!!./configFile.txt", ebitgViewer.getConfigFileField());
		configCacher.updateRegisteredTextFields();

		// alignmentPanel.setPreferredSize(new Dimension(800,600));

		JScrollPane scroller = new JScrollPane(alignmentPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setPreferredSize(new Dimension(1800, 170));

		JLabel corpusSetSizeReport = new JLabel("of: 0");
		corpusSetSizeReport.setFocusable(false);

		ebitgViewer.rightControlPanel.add(ebitgViewer.alignmentTripleSpinnerPanel);

		ebitgViewer.componentsPanel.add(scroller, BorderLayout.NORTH);

		// Get the default toolkit
		// Toolkit toolkit = Toolkit.getDefaultToolkit();
		// Get the current screen size
		// Dimension screenSize = toolkit.getScreenSize();

		Dimension mainPanelPrefferdSize = ebitgViewer.getPreferredSize();
		ebitgViewer.treeScroller.setPreferredSize(new Dimension(mainPanelPrefferdSize.width, mainPanelPrefferdSize.height - 340));

		ebitgViewer.tabbedPane = new JTabbedPane();
		ebitgViewer.tabbedPane.addTab("HATs visualization", ebitgViewer);
		addHATSelectionPanel(ebitgViewer);

		System.out.println("Ebitgviewer creator returns");

		return ebitgViewer;
	}

	public JTextField getSourceField() {
		return getIoComponentsTable().getComponentByName(SourceKeyString).getTextField();
	}

	public JTextField getTargetField() {
		return getIoComponentsTable().getComponentByName(TargetKeyString).getTextField();
	}

	public JTextField getAlignmentsField() {
		return getIoComponentsTable().getComponentByName(AlignmentKeyString).getTextField();
	}

	public JTextField getConfigFileField() {
		return getIoComponentsTable().getComponentByName(ConfigKeyString).getTextField();
	}

	public static void addHATSelectionPanel(HATViewer ebitgViewer) {
		ebitgViewer.hatSelectionPanel = new HATSelectionPanel(ebitgViewer.getConfigFileField(), ebitgViewer.getConfigCacher());
		ebitgViewer.hatsDatabaseBuilderPanel = new HATsDatabaseBuilderPanel(ebitgViewer.getConfigCacher());
		ebitgViewer.tabbedPane.addTab("HAT Selection", ebitgViewer.hatSelectionPanel);
		ebitgViewer.tabbedPane.addTab("HATs Database Building", ebitgViewer.hatsDatabaseBuilderPanel);
	}

	public HATViewer(ButtonIOComponentTable buttonIOComponentTable, LabeledAlignmentPanel alignmentPanel, ConfigCacher configCacher, TreePairPanel treePairPanel, CheckBoxTable checkBoxTable) {
		super(buttonIOComponentTable, treePairPanel, checkBoxTable);
		this.alignmentPanel = alignmentPanel;
		this.theConfigCacher = configCacher;
		this.alignmentTripleSpinnerPanel = SpinnerPanel.createSpinnerPanel(this, "Alignment Triple: ");
		this.menuInteraction = createMenuInteraction(this);
	}

	protected String getSpinnerLabel() {
		return HatSpinnerLabel;
	}

	@Override
	protected void createInputFields() {
		java.awt.Font f = new java.awt.Font("Arial", Font.BOLD, 18);
		JTextField alignmentField = new JTextField(alignmentInitial);
		alignmentField.setFont(f);

		JTextField sourceField = new JTextField(sourceInitial);
		sourceField.setFont(f);

		JTextField targetField = new JTextField(targetInitial);
		targetField.setFont(f);

		JTextField configFileField = new JTextField("configFile.txt");
		targetField.setFont(f);

		JButton button1 = new JButton("Compute HATs");
		button1.setMnemonic('E');

		JButton configFileCreateButton = new JButton("Make Configfile for Database");
		button1.setMnemonic('E');

		JButton button2 = new JButton("Load Everything from ConfigFile");
		getIoComponentsTable().addComponent(HATViewer.SourceKeyString, new ButtonIOComponent(button1, sourceField, null, new JLabel("SourceString:")));
		getIoComponentsTable().addComponent(HATViewer.TargetKeyString, new ButtonIOComponent(null, targetField, null, new JLabel("TargetString:")));
		getIoComponentsTable().addComponent(HATViewer.AlignmentKeyString, new ButtonIOComponent(configFileCreateButton, alignmentField, null, new JLabel("Alignment:")));
		getIoComponentsTable().addComponent(HATViewer.ConfigKeyString, new ButtonIOComponent(button2, configFileField, null, new JLabel("ConfigFile location:")));
	}

	public void doBeforeClosing() {
		theConfigCacher.saveGuiConfig();
	}

	public static MenuInteraction createMenuInteraction(HATViewer ebitgViewer) {
		MenuInteraction menuInteraction = MenuInteraction.createMenuInteraction(ebitgViewer.getAlignmentPanel());
		menuInteraction.addAlignmentTripleLoadItems(ebitgViewer, ebitgViewer.alignmentTripleSpinnerPanel);
		menuInteraction.addLoadTreeItem(ebitgViewer, "Load TreeBank");
		menuInteraction.addExportTreeItems(ebitgViewer.treePanel, "Tree");
		menuInteraction.addExportAlignmentstems(ebitgViewer.getAlignmentPanel());
		menuInteraction.addLoadTreeItem(ebitgViewer.getAlignmentPanel().getSourceParsesTable(), "Load Source Parses");
		menuInteraction.addLoadTreeItem(ebitgViewer.getAlignmentPanel().getTargetParsesTable(), "Load Target Parses");
		menuInteraction.addQuitter(ebitgViewer);
		return menuInteraction;
	}

	private static void createButtonActionListeners(final HATViewer treeViewer) {
		treeViewer.getIoComponentsTable().get(0).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((HATViewer) treeViewer).actionPerformed(e);
			}
		});

		treeViewer.getIoComponentsTable().get(2).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeViewer.createConfigFileForDB();
			}
		});

		treeViewer.getIoComponentsTable().get(3).getTheButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeViewer.loadEveryThingFromConfigFile();
			}
		});
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	public static HATViewer createAndShowGUI() {
		// Create and set up the window.
		JFrame mainFrame = new JFrame("HAT Viewer");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final HATViewer treeViewer = createEbitgViewer();
		treeViewer.hatsDatabaseBuilderPanel.setContainingFrame(mainFrame);
		Dimension area = treeViewer.treePanel.getArea();
		System.out.println("Area: " + area.width + " " + area.height);
		mainFrame.setMinimumSize(new Dimension(area.width + 5, area.height + 5));
		treeViewer.setOpaque(true); // content panes must be opaque
		mainFrame.setContentPane(treeViewer.tabbedPane);
		// frame.setContentPane(treeViewer);

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				treeViewer.doBeforeClosing();
			}
		});

		// Menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		treeViewer.menuInteraction.addItemsToMenu(menu);

		createButtonActionListeners(treeViewer);

		for (JMenuItem item : treeViewer.menuInteraction.getMenuItems()) {
			System.out.println("item :" + item);
			menu.add(item);
		}

		menuBar.add(menu);
		mainFrame.setJMenuBar(menuBar);

		// Display the window.
		mainFrame.pack();
		mainFrame.setVisible(true);

		return treeViewer;
	}

	private String getConfigFilePath() {
		return this.getIoComponentsTable().get(3).getTextfieldContents();
	}

	private void loadSentencesAndAlignmentsAndSourceParses() {
		ViewerInputLoading.loadAlignmentTriplesAndParsesFromConfig(getConfigFilePath(), this, this.alignmentTripleSpinnerPanel);
	}

	public void loadEveryThingFromConfigFile() {
		System.out.println(">>> Load everything from configFile...");
		loadSentencesAndAlignmentsAndSourceParses();
		this.numberKeeper = NumberKeeper.getNumberKeeperFromConfig(getConfigFilePath());
	}

	private String getSourceString() {
		return this.getSourceField().getText();
	}

	private String getTargetString() {
		return this.getTargetField().getText();
	}

	public String getAlignmentString() {
		return this.getAlignmentsField().getText();
	}

	/**
	 * 
	 * @param hatInputPreparation
	 * @param tg
	 * @return True if all went well and false if an error occurred in checking the consistency of the generated trees
	 */
	public boolean displayComputedHATs(HATInputPreparation hatInputPreparation, EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg) {
		File f = new File("tempITGTrees.txt");

		this.loadTreebank(f, true, hatInputPreparation.getSourceLeafDescriptions(), hatInputPreparation.getTargetLeafDescriptions(), getAlignmentString());

		int numTrees = this.treePanel.getNumTrees();
		this.treeSpinnerPanel.setSpinnerSize(numTrees);

		return this.determineAndPrintProperties(tg, hatInputPreparation.getSourceString(), hatInputPreparation.getTargetString());

	}

	private void setMaxNumLabels() {
		if ((!showExtraLabels()) && (this.getAlignmentPanel().getSourceParse() == null) && (this.getAlignmentPanel().getTargetParse() == null)) {
			this.treePanel.setMaxNumLabels(1);
		} else {
			this.treePanel.setMaxNumLabels(3);
		}
	}

	public void setShowAlignmentSets(boolean value) {
		((EbitgCheckBoxTable) this.checkBoxTable).getShowAlignmentSetCheckBox().setSelected(value);
	}

	public void setNoColors(boolean value) {
		((EbitgCheckBoxTable) this.checkBoxTable).getNoColorsCheckBox().setSelected(value);
	}

	private boolean showReorderingLabels(){
		return ((EbitgCheckBoxTable) this.checkBoxTable).showHATReorderingLabels() ||
				((EbitgCheckBoxTable) this.checkBoxTable).showParentRelativeReorderingLabels();
	}
	
	private boolean showExtraLabels() {
		return ((EbitgCheckBoxTable) this.checkBoxTable).showExtraLabels() ||
				showReorderingLabels();
	}
	

	public HATInputPreparation createHatInputPreparationFromInputFields() {
		return HATInputPreparation.createHATInputPreparation(this.getSourceString(), this.getTargetString(), this.getAlignmentString(), ((EbitgCheckBoxTable) this.checkBoxTable).useSymbolsForWords());
	}

	private EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> createTreeGrower(HATInputPreparation hatInputPreparation, TSNodeLabel sourceParse) {
		// assert(alignmentPanel.hasParses());
		// assert(alignmentPanel.getCCGLabeler().hasLabelChart());
		return EbitgChartBuilderBasic.buildAndWriteTreePairsForAlignment(MAX_ALLOWED_INFERENCES_PER_NODE, hatInputPreparation.getSourceString(), hatInputPreparation.getTargetString(),
				hatInputPreparation.getAlignmentString(), getAlignmentPanel().getCCGLabeler(), ((EbitgCheckBoxTable) this.checkBoxTable).computeAllInducedHATs(),
				((EbitgCheckBoxTable) this.checkBoxTable).useGreedyNullBinding());

	}

	public boolean computeHATsThreadSafe() {
		SafeGUIAffectingTaskRunner<HATViewer, Object, Boolean> taskRunner = SafeGUIAffectingTaskRunner.createEbitgViewerHatsComputer(this);
		return SafeGUIAffectingTaskRunner.executeThreadSafe(taskRunner);
	}

	public boolean computeHATs() {
		alignmentsLocked = true;
		System.out.println(">>> Produce ITG trees for permutation...");

		System.out.println(" this.checkBoxTable : " + this.checkBoxTable);
		HATInputPreparation hatInputPreparation = createHatInputPreparationFromInputFields();

		this.getAlignmentPanel().loadFromStrings(hatInputPreparation.getSourceString(), hatInputPreparation.getTargetString(), hatInputPreparation.getAlignmentString());

		this.getAlignmentPanel().initWithoutSentenceLouding();

		setMaxNumLabels();

		TSNodeLabel sourceParse = null;

		setMaxNumLabels();

		Alignment alignments = Alignment.createAlignment(hatInputPreparation.getAlignmentString());
		((HATPairPanel) this.treePanel).setAlignments(alignments.getAlignmentPairs());

		System.out.println(">>> Prepared input:");
		System.out.println("sourceString: " + hatInputPreparation.getSourceString());
		System.out.println("targetString: " + hatInputPreparation.getTargetString());
		System.out.println("alignment: " + hatInputPreparation.getAlignmentString());

		EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg = createTreeGrower(hatInputPreparation, sourceParse);

		if (tg != null) {

			// Print the set of minimal phrase pairs
			// MinimalPhrasePairExtractor mppe = new MinimalPhrasePairExtractor(tg.getChart());
			// mppe.printMinimalPhrasePairInferences();

			alignmentsLocked = false;
			return displayComputedHATs(hatInputPreparation, tg);
		}

		alignmentsLocked = false;

		// If the parsing failed we do not consider it as an error of the visualization
		return true;

	}

	/**
	 * Method that loads the HAT treebank, sets the leaf description words and the alignments that belong with the set of HATs
	 * 
	 * @param treebankFile
	 * @param resetSpinner
	 * @param leafDescriptionWords1
	 * @param leafDescriptionWords2
	 * @param alignmentString
	 * @return
	 */
	protected void loadTreebank(File treebankFile, boolean resetSpinner, List<String> leafDescriptionWords1, List<String> leafDescriptionWords2, String alignmentString) {
		TreePairBankLoader.loadTreePairBank(treebankFile, this, resetSpinner);
		setTreeLeafDescriptions(leafDescriptionWords1, leafDescriptionWords2);

		System.out.println("EbitgViewer.loadTreebank....");
		// FIXME this should not be done here
		// List<AlignmentPair> alignments = InputReading.constructAlignmentPairs(alignmentString);
		// ((HATPairPanel)this.treePanel).setAlignments(alignments);
		return;
	}

	protected void setTreeLeafDescriptions(List<String> leafDescriptionWords1, List<String> leafDescriptionWords2) {
		((TreePairPanel) this.treePanel).setFirstTreeLeafDescriptionWords(leafDescriptionWords1);
		((TreePairPanel) this.treePanel).setSecondTreeLeafDescriptionWords(leafDescriptionWords2);
	}

	/**
	 * Method that determines the consistency of the source and target string with both the original and the twin tree
	 * 
	 * @param sourceString
	 * @param targetString
	 * @return A pair of a boolean and a String. The boolean is true provided that both trees are consistent, the String is a String that contains the
	 *         consistency properties as a description"
	 */
	public Pair2<Boolean, String> determineConsistency(String sourceString, String targetString) {

		boolean firstTreeConsistent = ((HATPairPanel) this.treePanel).firstTreeConsistentWithString(sourceString);
		boolean secondTreeConsistent = ((HATPairPanel) this.treePanel).secondTreeConsistentWithString(targetString);

		return new Pair2<Boolean, String>((firstTreeConsistent && secondTreeConsistent), "\nOriginalTreeConsistent: " + firstTreeConsistent + " twinTreeConsistent: " + secondTreeConsistent);
	}

	public boolean determineAndPrintProperties(EbitgTreeGrower<? extends EbitgLexicalizedInference, ? extends EbitgTreeState<?, ?>> tg, String sourceString, String targetString) {

		System.out.println("EbitgViewer: DeterminAndPrintProperties");
		int sourceLength = InputReading.determineNumWordsString(sourceString);
		int targetLength = InputReading.determineNumWordsString(targetString);

		System.out.println("EbitViewer : targetString" + targetString);

		int maxSourceLengthAtomicFragment = 1;

		int numberTranslationEquivalentNodesInChart = treeStatisticsComputation.determineNumberTranslationEquivalentNodesInChart(tg.getChart());
		int maxBranchingFactor = treeStatisticsComputation.determineMaximalOccuringBranching(tg, maxSourceLengthAtomicFragment);
		int maxExtraAlignments = treeStatisticsComputation.determineMaxNumberOfExtraAlignmentsInTree(tg);

		// tg.getChart().printChartContents();

		TreeType treeType = treeStatisticsComputation.determineTreeType(tg, maxSourceLengthAtomicFragment);

		Pair2<Boolean, String> consistencyInformation = this.determineConsistency(sourceString, targetString);

		// Test: Check that the trees are consistent

		if (!consistencyInformation.getFirst()) {
			System.out.println("EbitgViewer : Tree" + getCurrentAlignmentTripleNumber() + " is inconsistent");
			// System.exit(0);
		}

		String outputString = "";

		if (this.numberKeeper != null && (!this.numberKeeper.isEmpty())) {
			outputString = "Alignment Triple number:\t" + getCurrentAlignmentTripleNumber();
		}
		outputString += "  sourceLength: " + sourceLength + "   targetLength: " + targetLength + "\nmaxBranchingFacor: " + maxBranchingFactor + "   maxExtraAlignments: " + maxExtraAlignments
				+ "\n#TranslationEquivalentNodes: " + numberTranslationEquivalentNodesInChart + " treeType: " + treeType.toString() + consistencyInformation.getSecond();

		if (!consistencyInformation.getFirst()) {
			System.out.println("Inconsistency detected in trees: ");
			System.out.println(outputString);
		}

		this.setOutputFieldText(outputString);

		return consistencyInformation.getFirst();
	}

	public int getCurrentAlignmentTripleNumber() {
		return this.numberKeeper.getCurrentNumber();
	}

	// Deal with scrolling trough the sentences
	public void stateChanged(ChangeEvent e) {
		System.out.println("EbitgViewer.stateChanged() called ...");

		System.out.println("(this.alignmentTripleSpinnerPanel.getSpinner()" + this.alignmentTripleSpinnerPanel.getSpinner());
		System.out.println("(this.treeSpinnerPanel.getSpinner()" + this.treeSpinnerPanel.getSpinner());
		System.out.println("e.getSource()" + e.getSource());

		if (e.getSource().equals(this.alignmentTripleSpinnerPanel.getSpinner()) && (!this.alignmentsLocked)) {
			loadCurrentAlingmentTripleSpinnerValue();
		} else if (e.getSource().equals(this.treeSpinnerPanel.getSpinner())) {
			System.out.println("EbitgViewer ... treeSpinnerPanel block entered");
			Integer n = treeSpinnerPanel.getSpinnerValue() - 1;
			System.out.println("Tree number: " + n);
			int treeToLoad = this.treePanel.goToSentence(n);
			System.out.println("Tree to be loaded: " + treeToLoad);
			this.treePanel.init();

		}

	}

	public void loadCurrentAlingmentTripleSpinnerValue() {
		Integer number = this.alignmentTripleSpinnerPanel.getSpinnerValue();
		loadAlignmentTriple(number);
		this.numberKeeper.goToIndex(number - 1);

		System.out.println("numbeKeeper goes to index: " + (number - 1));
		// System.out.println("this.numberKeeper.getCurrentNumber() : " + this.numberKeeper.getCurrentNumber());
	}

	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);

		if (useSymbolsChanged(e) || showNamePlatesChanged(e) || computeHATsButtonPressed(e) || showEtraLabelsChanged(e)) {
			// Displaying words/symbols changed, recompute the HATs
			System.out.println("Recompute the HATs ");
			this.computeHATs();
		}

	}

	private boolean useSymbolsChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getUseSymbolsCheckBox());
	}

	private boolean showNamePlatesChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getShowNamePlateCheckBox());
	}

	private boolean computeHATsButtonPressed(ActionEvent e) {
		return e.getSource().equals(this.getIoComponentsTable().get(0).getTheButton());
	}

	private boolean showEtraLabelsChanged(ActionEvent e) {
		return e.getSource().equals(((EbitgCheckBoxTable) this.checkBoxTable).getShowExtraLabelsCheckBox());
	}

	public void loadAlignmentTriple(int number) {
		int index = number - 1;

		this.numberKeeper.goToIndex(number - 1);
		getAlignmentPanel().goToSentence(index);
		// statusBar.setText(treebankComments.get(index));
		getAlignmentPanel().init();

		String sourceSentence = getAlignmentPanel().getSourceSentence();
		String targetSentence = getAlignmentPanel().getTargetSentence();
		String alignment = getAlignmentPanel().getAlignment();

		this.getSourceField().setText(sourceSentence);
		this.getTargetField().setText(targetSentence);
		this.getAlignmentsField().setText(alignment);

	}

	public void loadAlignmentTripleThreadSafe(int number) {
		SafeGUIAffectingTaskRunner<HATViewer, Integer, Boolean> taskRunner = SafeGUIAffectingTaskRunner.createEbitgViewerAlignmentTripleLoader(this, number);
		SafeGUIAffectingTaskRunner.executeThreadSafe(taskRunner);
	}

	public void createConfigFileForDB() {
		File hatsDatabaseFile = this.menuInteraction.getFileChoosingHandler().getSelectedFile("Select HATs database file to create configfile from");
		HatsDatabaseAutomaticConfigfileCreater hdbacfc = HatsDatabaseAutomaticConfigfileCreater.createHatsDatabaseAutomaticConfigFileCreater(hatsDatabaseFile);
		hdbacfc.writeConfigFile();
		this.getConfigFileField().setText(hdbacfc.getConfigFilePath());
		
		List<String> tableNames;
		try {
			tableNames = DatabaseInterface.getTableNamesFromDatabaseFilterOutIndexTables(hatsDatabaseFile.getCanonicalPath());
			this.hatSelectionPanel.setFromField(tableNames);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public int getMaxAlignmentSpinnerValue() {
		return this.alignmentTripleSpinnerPanel.getMaxSpinnerValue();
	}

	public void startRunnableThreadSafe(Runnable r) {
		// This is necessary since repaint is not thread safe,
		// see http://stevensrmiller.com/wordpress/?p=567
		javax.swing.SwingUtilities.invokeLater(r);
	}

	public void exportAlignmentToFile(String fileName) {
		ExporterPDF.exportToPDF(new File(fileName), this.getAlignmentPanel());
	}

	public void exportHATToFile(String fileName) {
		ExporterPDF.exportToPDF(new File(fileName), this.treePanel);
	}

	protected ConfigCacher getConfigCacher() {
		return this.theConfigCacher;
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public LabeledAlignmentPanel getAlignmentPanel() {
		return alignmentPanel;
	}

	@Override
	/**
	 * We implement the  SentencesAndAlignmentsLoader interface in this class as well, 
	 * while mainly delegating to the alignmentPanel field in this class, to do the actual work
	 * for the three methods of this interface. However, this allows is to do some extra actions 
	 * specific to this GUI, in this case loading the current alignment triple after the
	 * alignments are loaded
	 * @param alignments
	 */
	public void loadAlignments(List<Alignment> alignments) {
		this.alignmentPanel.loadAlignments(alignments);
		this.loadCurrentAlingmentTripleSpinnerValue();
	}

	@Override
	public void loadSourceSentences(List<List<String>> sourceSentences) {
		this.alignmentPanel.loadSourceSentences(sourceSentences);
	}

	@Override
	public void loadTargetSentences(List<List<String>> targetSentences) {
		this.alignmentPanel.loadTargetSentences(targetSentences);
	}

	public void performSelectionOnAlignmentTripleNumbers(String languagePair, List<Integer> selection)
	{
		this.hatSelectionPanel.performSelectionOnAlignmentTripleNumbers(languagePair, selection);
	}
	
}
