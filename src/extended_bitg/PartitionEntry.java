/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;

/**
 * The PartitionEntry class is used to store the minimal cost for partitioning the
 * relative (source positions)  starting from the left of the chart entry, and going 
 * up to index -1. Importantly, an index of 0 means nothing has been covered yet,
 * while an index of n-1 means that the full chart entry spanning from 0 - n-1 has 
 * been covered.
 * This means that in the list of partition entries there are n+1 entries.  
 * @author gemaille
 *
 */
public class PartitionEntry 
{
	private int minimalCost;
	private ArrayList<PartitionEntry> viterbiParents;
	private int index;
	
	public PartitionEntry()
	{
		//Take only half the maxvalue to prevent overflow errors
		minimalCost = Integer.MAX_VALUE/2; 
		viterbiParents = new ArrayList<PartitionEntry> ();
	}
	
	public void setIndex(int index)
	{
		this.index  = index;
	}
	
	public int getIndex()
	{
		return this.index;
	}
	
	public int getMinimalCost()
	{
		return minimalCost;
	}

	public void setMinimalCost(int minimalCost)
	{
		this.minimalCost = minimalCost;
	}
	
	public void addViterbiParent(PartitionEntry parent)
	{
		this.viterbiParents.add(parent);
	}
	
	public void replaceViterbiParents(PartitionEntry newParent)
	{
		this.viterbiParents = new ArrayList<PartitionEntry>();
		this.viterbiParents.add(newParent);
	}
	
	public ArrayList<PartitionEntry> getViterbiParents()
	{
		return this.viterbiParents;
	}
	
	public String toString()
	{
		String result = "";
		
		result += "<PartitionEntry> minCost: " + minimalCost ;
		result += " index: " + index + " </PartitionEntry>";
		return result;
	}
}
