/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

public class InferenceLengthProperties 
{

	private final EbitgLexicalizedInference inference;
	
	private InferenceLengthProperties(EbitgLexicalizedInference inference)
	{
		this.inference = inference;
	}

	public static InferenceLengthProperties createInferenceLengthProperties(EbitgLexicalizedInference inference)
	{
		return new InferenceLengthProperties(inference);
	}
	
	
	
	public int getTargetSpanLengthAlignedWordsOnly()
	{
		return this.inference.getBasicBoundTargetPositions().size();
	}
	
	/**
	 * @return : the length of the span in the source of the inference
	 */
	public int getSourceSpanLength()
	{
		return inference.getSourceSpanLength();
	}
	
	public int getTargetSpanLength()
	{
		return (this.inference.getMaxTargetPos() - this.inference.getMinTargetPos()) + 1;
	}
	
	public int getSourceSpanLengthAlignedWordsOnly(EbitgChart chart)
	{
		int result = computeSourceSpanLengthAlignedWordsOnly(chart);
		
		if(result >= 0)
		{
			return result;
		}
		throw new RuntimeException("EbitgInference: Field SourceSpanLengthAlignedWordsOnly has not been computed yet properly");
	}
	
	protected int computeSourceSpanLengthAlignedWordsOnly(EbitgChart chart)
	{
		EbitgChartEntry containingEntry = this.inference.getContainingChartEntry();
		
		int result = 0;
		
		for(int i = containingEntry.getI(); i <= containingEntry.getJ(); i++)
		{
			if(chart.getChartEntry(i, i).hasInferences())
			{
				result++;
			}
		}
		return result;
	}
	
	
}
