/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import util.Span;
import hat_lexicalization.Lexicalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class EbitgInferenceLexicalization 
{
	final EbitgInferenceTargetProperties targetProperties;
	private final Span sourceSpan;
	final private Lexicalizer lexicalizer;

	private EbitgInferenceLexicalization(EbitgInferenceTargetProperties targetProperties, Lexicalizer lexicalizer, Span sourceSpan)
	{
		this.targetProperties = targetProperties;
		this.lexicalizer = lexicalizer;
		this.sourceSpan = sourceSpan;
	}
	
	public static EbitgInferenceLexicalization createEbitgInferenceLexicalization(EbitgInferenceTargetProperties targetProperties, Lexicalizer lexicalizer,Span sourceSpan)
	{
		return new EbitgInferenceLexicalization(targetProperties, lexicalizer,sourceSpan);
	}
	
	public Map<Integer,String>  getGeneratedTargetWordsMap()
	{
		Map<Integer,String> result = new HashMap<Integer, String>();
		List<String> targetWords = this.lexicalizer.getTargetWords();
		for(Integer targetPos : targetProperties.getBoundTargetPositions())
		{
			result.put(targetPos, targetWords.get(targetPos));
		}
		return result;
	}
	
	private String getGeneratedTargetWord(int absoluteTargetPosition)
	{
		return this.lexicalizer.getTargetWord(absoluteTargetPosition);
	}
	
	public List<String> getGeneratedSourceWords()
	{
		return this.lexicalizer.getSourceWords().subList(sourceSpan.getFirst(), sourceSpan.getSecond() + 1);
	}
	
	public List<String> getGeneratedTargetWords()
	{
		
		ArrayList<String> result = new ArrayList<String>();
		// Sort the bound target positions by putting them in a TreeSet, then put the result into an ArrayList
		ArrayList<Integer> sortedAbsoluteTargetPositions = new ArrayList<Integer>(new TreeSet<Integer>(targetProperties.getBoundTargetPositions()));
		
		for(int absoluteTargetPosition :  sortedAbsoluteTargetPositions)
		{
			// get the generated word for target position i
			result.add(this.getGeneratedTargetWord(absoluteTargetPosition));
		}
		return result;	
	}
	
	public String getGeneratedTargetWordAtRelativePosition(int relativePosition)
	{
		// Generate a TreeSet from the HashSet with the target positions for the child. Since it is a treeset, it is sorted
		TreeSet<Integer> absoluteTargetPositionsSorted =  new TreeSet<Integer>(targetProperties.getBoundTargetPositions());
		
		 
		Integer[] sortedAbsoluteTargetPositionsArray = absoluteTargetPositionsSorted.toArray(new Integer[absoluteTargetPositionsSorted.size()]);
		 
		int absoluteTargetPosition =  sortedAbsoluteTargetPositionsArray[relativePosition];
		return this.getGeneratedTargetWord(absoluteTargetPosition);
	}
	
	
	/**
	 * @return : the length of the span in the source of the inference
	 */
	public int getSourceSpanLength()
	{
		return sourceSpan.getSpanLength();
	}

	public Span getSourceSpan()
	{
		return this.sourceSpan;
	}
	
}
