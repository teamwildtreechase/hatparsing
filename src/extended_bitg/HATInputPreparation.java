/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.List;

import util.InputReading;

public class HATInputPreparation 
{

	private String sourceString, targetString, alignmnentString;
	private List<String> sourceLeafDescriptions, targetLeafDescriptions;
	
	public HATInputPreparation(String sourceString, String targetString, String alignmentString, List<String> sourceLeafDescriptions,List<String> targetLeafDescriptions)
	{
		this.sourceString = sourceString;
		this.targetString = targetString;
		this.alignmnentString = alignmentString;
		this.sourceLeafDescriptions = sourceLeafDescriptions;
		this.targetLeafDescriptions = targetLeafDescriptions;
	}
	
	
	public static HATInputPreparation createHATInputPreparation(String originalSourceString, String originalTargetString, String originalAlignmentString, boolean replacedWordsByVariables)
	{
		List<String> sourceLeafDescriptions = null;
		List<String> targetLeafDescriptions = null;
		
		if(replacedWordsByVariables)
		{
			sourceLeafDescriptions = getLeafWordDescriptionsList(originalSourceString);
			targetLeafDescriptions = getLeafWordDescriptionsList(originalTargetString);
		}
		
		return new HATInputPreparation(getAdaptedString(replacedWordsByVariables,originalSourceString),getAdaptedString(replacedWordsByVariables,originalTargetString),originalAlignmentString, sourceLeafDescriptions,targetLeafDescriptions);
	}
	
	
	public String getSourceString()
	{
		return sourceString;
	}
	
	public String getTargetString()
	{
		return targetString;
	}
    
	public String getAlignmentString()
	{
		return alignmnentString;
	}
	
	public List<String> getSourceLeafDescriptions()
	{
		return sourceLeafDescriptions;
	}
	
	public List<String> getTargetLeafDescriptions()
	{
		return targetLeafDescriptions;
	}
	
    private static List<String> getLeafWordDescriptionsList(String originalString)
    {
    	List<String> result =  new ArrayList<String>();
    	
    	int length = InputReading.determineNumWordsString(originalString);
    	
    	List<String> words = InputReading.getWordsFromString(originalString);
    	
    	for(int i = 0; i < length; i++)
    	{
    		result.add("s" + i + " = " +  "\"" + words.get(i) + "\""); 
    	}
    	
    	return result;
    }
    
 
    
    
    public static String getReplaceBracketsString(String string)
    {
    	string = string.replace('(', '{');
		string = string.replace(')', '}');
		return string;
    }
    
    private static String getAdaptedString(boolean replacedWordsByVariables, String originalString)
    {
    	
    	if(replacedWordsByVariables)
    	{
    		return getWordsReplacedByVariablesString(originalString);
    	}
    	
    	//return getReplaceBracketsString(originalString);
    	return originalString;		
    }
    
    /**
     * 
     * @param originalString The string from which the words must be replaced
     * @return String in which the words are replaced by variables
     */
     public static String getWordsReplacedByVariablesString(String originalString)
     {
     	String result = "";
     	int length = InputReading.determineNumWordsString(originalString);
     	
  
     	for(int i = 0; i < length; i++)
     	{
     		result += "s" + i + " ";

     	}
     	
     	result = result.trim();
     	
     	return result;
     }
	
}
