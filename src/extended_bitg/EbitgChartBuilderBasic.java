/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import alignment.AlignmentStringTriple;
import ccgLabeling.CCGLabeler;
import hat_lexicalization.Lexicalizer;

public class EbitgChartBuilderBasic extends EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic>
{

  private static int MAX_ALLOWED_INFERENCES_PER_NODE_DEFAULT = 200000;
  private static final boolean USE_GREEDY_NULL_BINDING_DEFAULT = true;
  private static final boolean USE_CACHING_DEFAULT = true;
  
  
	protected EbitgChartBuilderBasic(Lexicalizer lexicalizer,
			EbitgLexicalizedInferenceCacher<EbitgLexicalizedInference> lexicalizedInferenceCacher,
			EbitgChart theChart, EbitgChartInitializer chartInitializer,
			EbitgChartFiller chartFiller, boolean useCaching,HATComplexityTypeComputer hatComplexityTypeComputer) {
		super(lexicalizer, lexicalizedInferenceCacher, theChart, chartInitializer,
				chartFiller, useCaching, hatComplexityTypeComputer);
	}
	
	/**
	 * Factory method that creates the chart builder for an alignment triple < sourceString,  targetString, alignmentString>
	 * @param sourceString  The source string 
	 * @param targetString  The target string 
	 * @param alignmentString The alignment string e.g. '0-0 0-1 1-2 2-3'
	 * @param maxAllowedInferencesPerNode A Control parameter that specifies the maximum allowed number of inferences per node, 
	 * to stop chart building in cases there is too much 
	 * ambiguity which would lead the ChartBuilder to go out of memory 
	 * @param lexicalzer : The Lexicalizer, which determines if the HATs chart is build compactly or with an explict representation of the nulls
	 * @return The ChartBuilder that is constructed for the input alignment triple
	 */
	private static EbitgChartBuilderBasic  createEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,Lexicalizer lexicalizer, boolean useCaching)
	{ 
		EbitgChart theChart = createInitialChart(lexicalizer);
		EbitgChartInitializer chartInitializer =  EbitgChartInitializer.createChartInitializer(theChart, lexicalizer, useGreedyNullBinding);
		EbitgChartFiller chartFiller = new EbitgChartFiller(maxAllowedInferencesPerNode,theChart);
		chartInitializer.initializeChart();
			
		EbitgLexicalizedInferenceCacher<EbitgLexicalizedInference> lexicalizedInferenceCacher = EbitgLexicalizedInferenceCacher.createEbitgLexicalizedInferenceCacher();
		HATComplexityTypeComputer hatComplexityTypeComputer = HATComplexityTypeComputer.createHAComplexityTypeComputer(theChart);
		EbitgChartBuilderBasic  theBuilder = new EbitgChartBuilderBasic(lexicalizer,lexicalizedInferenceCacher, theChart, chartInitializer, chartFiller,useCaching,hatComplexityTypeComputer);
		
		theBuilder.checkChartSizeConsistency();
		
		//System.out.println("EbitgChartBuilderBasic - SourceString: " + sourceString);
		//System.out.println("EbitgChartBuilderBasic - TargetString: " + targetString);
		//System.out.println("EbitgChartBuilderBasic - AlignmentString: " + alignmentString);
		return theBuilder;
	}
	
	public static EbitgChartBuilderBasic  createEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,boolean useCaching)
	{
		if(UseCompactHATsRepresentation)
		{	
			return createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding,useCaching);
		}
		else
		{
			return createExplicitNullsHATsEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding,useCaching);
		}
	}
	
	public static EbitgChartBuilderBasic createEbitgChartBuilderWithDefaultSettings(String sourceString, String targetString, String alignmentString){
	  return EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, MAX_ALLOWED_INFERENCES_PER_NODE_DEFAULT,USE_GREEDY_NULL_BINDING_DEFAULT, USE_CACHING_DEFAULT);	  
	}
	
	public static EbitgChartBuilderBasic createCompactHATsEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,boolean useCaching, CCGLabeler ccgLabeler)
	{
		Lexicalizer lexicalizer =  Lexicalizer.createCompactLexicalizer(alignmentString,  sourceString,  targetString,ccgLabeler);
		return createEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding, lexicalizer,useCaching);
	}
	
	public static EbitgChartBuilderBasic createCompactHATsEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,boolean useCaching)
	{
		CCGLabeler ccgLabeler = CCGLabeler.createEmptyCCGLabeler();
		return createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding, useCaching,ccgLabeler);
	}
	
	private static EbitgChartBuilderBasic  createExplicitNullsHATsEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,boolean useCaching,CCGLabeler ccgLabeler)
	{
		Lexicalizer lexicalizer =  Lexicalizer.createExplicitNullsLexicalizer(alignmentString,  sourceString,  targetString,ccgLabeler);
		return createEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding, lexicalizer,useCaching);
	}

	public static EbitgChartBuilderBasic  createExplicitNullsHATsEbitgChartBuilder(String sourceString, String targetString, String alignmentString, int maxAllowedInferencesPerNode, boolean useGreedyNullBinding,boolean useCaching)
	{
		CCGLabeler ccgLabeler = CCGLabeler.createEmptyCCGLabeler();
		return createExplicitNullsHATsEbitgChartBuilder(sourceString, targetString, alignmentString, maxAllowedInferencesPerNode, useGreedyNullBinding, useCaching, ccgLabeler);
	}
	
	public static EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> buildAndWriteTreesForAlignment(int maxAllowedInferencesPerNode, String sourceString, String targetString, String alignmentString, boolean useGreedyNullBinding)
	{
			AlignmentStringTriple trimmedAlignmentTriple = AlignmentStringTriple.createTrimmedAlignmentStringTriple(sourceString, targetString, alignmentString);
			EbitgChartBuilderBasic builder = createEbitgChartBuilder(trimmedAlignmentTriple.getSourceString(),trimmedAlignmentTriple.getTargetString(),trimmedAlignmentTriple.getAlignmentString(),maxAllowedInferencesPerNode, useGreedyNullBinding,true);
			return buildAndWriteTreesForAlignment(builder, maxAllowedInferencesPerNode);
	}
	
	public static EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> buildAndWriteTreePairsForAlignment(int maxAllowedInferencesPerNode, String sourceString, String targetString, String alignmentString, CCGLabeler ccgLabeler, boolean writeAllTrees, boolean useGreedyNullBinding)
	{
	
			AlignmentStringTriple trimmedAlignmentTriple = AlignmentStringTriple.createTrimmedAlignmentStringTriple(sourceString, targetString, alignmentString);
			EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> builder = createCompactHATsEbitgChartBuilder(trimmedAlignmentTriple.getSourceString(),trimmedAlignmentTriple.getTargetString(),trimmedAlignmentTriple.getAlignmentString(), maxAllowedInferencesPerNode, useGreedyNullBinding, true, ccgLabeler);
			return buildAndWriteTreePairsForAlignment(builder, writeAllTrees);
	}

	@Override
	protected EbitgLexicalizedInference createLabelEnrichedInferenceFromLexicalizedInference(EbitgLexicalizedInference ebitgLexicalizedInference) 
	{
		return ebitgLexicalizedInference;
	}

	@Override
	public EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> createEbitgTreeGrower() 
	{
		return EbitgTreeGrower.createEbitgTreeGrower(this);
	}
	

}
