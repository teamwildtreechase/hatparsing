/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import util.Span;
import reorderingLabeling.FineParentRelativeReorderingLabel;
import reorderingLabeling.ParentRelativeReorderingLabel;
import reorderingLabeling.ReorderingLabeler;
import hat_lexicalization.Lexicalizer;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

import alignmentStatistics.SentencePairAlignmentChunking;
import bitg.BITGChartEntry;

public abstract class EbitgChartBuilder<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> {
	public static final boolean UseCompactHATsRepresentation = true;
	private final boolean useCaching;

	final Lexicalizer lexicalizer;
	final EbitgLexicalizedInferenceCacher<T> lexicalizedInferenceCacher;
	public final EbitgChartInitializer chartInitializer;
	final EbitgChartFiller chartFiller;
	private final EbitgChart theChart;
	private boolean derivationsAlignmentHaveBeenComputed = false;
	
	private final HATComplexityTypeComputer hatComplexityTypeComputer;

	public void checkChartSizeConsistency() {
		assert (this.lexicalizer.getChartInternalAlignmentChunking().getSourceLength() == this.getChart().getSourceLength());
	}

	/**
	 * Constructor following dependency injection framework rules: no business
	 * logic, only assignments
	 * 
	 * @param alignmentChunking
	 * @param theChart
	 * @param chartInitializer
	 * @param chartFiller
	 */
	protected EbitgChartBuilder(Lexicalizer lexicalizer, EbitgLexicalizedInferenceCacher<T> lexicalizedInferenceCacher, EbitgChart theChart,
			EbitgChartInitializer chartInitializer, EbitgChartFiller chartFiller, boolean useCaching,HATComplexityTypeComputer hatComplexityTypeComputer) {
		this.lexicalizer = lexicalizer;
		this.lexicalizedInferenceCacher = lexicalizedInferenceCacher;
		this.theChart = theChart;
		this.chartInitializer = chartInitializer;
		this.chartFiller = chartFiller;
		this.useCaching = useCaching;
		this.hatComplexityTypeComputer = hatComplexityTypeComputer;
	}

	public boolean hasNullAlignments() {
		return this.lexicalizer.getOriginalSentencePairAlignmentChunking().hasNullAlignments();
	}

	protected static EbitgChart createInitialChart(Lexicalizer lexicalizer) {
		return EbitgChart.createEbitgChart(lexicalizer);
	}

	public String chartEntryStringExtra(BITGChartEntry entry) {
		String s = "\n|| ";
		s += entry.toString();
		s += "\nPermutation coverage: ";

		return s;
	}

	public int getChartSourceLength() {
		return getChart().getSourceLength();
	}

	public int getChartTargetLength() {
		return getChart().getTargetLength();
	}

	public int getSourceLength() {
		return this.lexicalizer.getSourceWords().size();
	}

	public int getTargetLength() {
		return this.lexicalizer.getTargetWords().size();
	}

	// incrementally find all derivations bottomn up
	public boolean findDerivationsAlignment() {
		if (!this.lexicalizer.getChartInternalAlignmentChunking().hasAlignments()) {
			System.out.println("Error: There are no alignments present, so chart could not be built.");
			return false;
		}

		// System.out.println("Find derivations, sourceLengt: " + sourceLength);

		// loop over the span length from 0 to the length of the source
		// permutation
		for (int spanLength = 2; spanLength <= getChart().getSourceLength(); spanLength++) {
			// System.out.println("Find derivations for spanLength: "+
			// spanLength);

			for (int beginIndex = 0; beginIndex < (getChart().getSourceLength() - spanLength + 1); beginIndex++) {
				try {
					chartFiller.addInferncesToChartEntry(beginIndex, beginIndex + spanLength - 1);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e);
					System.out.println("Failed to build the chart");
					return false;
					// System.exit(1);
				}
			}

		}

		markNodesPartOfAlignmentDerivation();

		// this.printChartContents();

		// System.out.println("Chart build...");

		computeReorderingLabels();
		this.hatComplexityTypeComputer.complyteHATComplexityTypesEntireChart();
		this.derivationsAlignmentHaveBeenComputed = true;
		
		return true;
	}

	public boolean derivationsAlignmentHaveBeenComputed() {
		return this.derivationsAlignmentHaveBeenComputed;
	}

	public void computeReorderingLabels() {
		ReorderingLabeler reorderingLabeler = ReorderingLabeler.createReorderingLabeler(theChart);
		for (EbitgChartEntry chartEntry : this.getChart().getChartEntriesAsList()) {
		    FineParentRelativeReorderingLabel reorderingLabel = reorderingLabeler.getReorderingLabelForChartEntry(chartEntry);
			chartEntry.setReorderingLabel(reorderingLabel);
		}
	}

	public void markNodesPartOfAlignmentDerivation() {

		EbitgChartEntry rootEntry = this.getChart().getChartEntry(0, this.getChartSourceLength() - 1);

		for (EbitgNode rootNode : rootEntry.getNodes()) {
			rootNode.setIsPartOfAlignmentDerivation(true);
		}

		ArrayDeque<EbitgChartEntry> chartEntriesQueu = new ArrayDeque<EbitgChartEntry>();
		chartEntriesQueu.add(rootEntry);
		markNodesPartOfAlignmentDerivation(chartEntriesQueu);
	}

	private void markNodesPartOfAlignmentDerivation(ArrayDeque<EbitgChartEntry> chartEntriesQueu) {
		Set<EbitgChartEntry> chartEntriesVisited = new HashSet<EbitgChartEntry>();

		while (!chartEntriesQueu.isEmpty()) {
			// System.out.println("EbitgChartBuilder.markNodesPartOfAlignmentDerivation : queueSize = "
			// + chartEntriesQueu.size());

			EbitgChartEntry firstEntryInQueue = chartEntriesQueu.pollFirst();
			chartEntriesVisited.add(firstEntryInQueue);
			// System.out.println("firstEntryInQueue" + firstEntryInQueue);

			// Construct a set of chart entries occuring in the inferences so
			// that they can be added
			// to the chart entries queue, but each only once
			Set<EbitgChartEntry> chartEntriesForInferences = new HashSet<EbitgChartEntry>();

			for (EbitgNode node : firstEntryInQueue.getNodes()) {
				if (node.isPartOfAlignmentDerivation()) {
					for (EbitgInference inference : firstEntryInQueue.getInferences(node)) {
						for (int i = 0; i < inference.getNodes().size(); i++) {
							EbitgNode inferenceNode = inference.getNodes().get(i);
							EbitgChartEntry nodeChartEntry = inference.getNodeChartEntries().get(i);
							inferenceNode.setIsPartOfAlignmentDerivation(true);

							// Get the actual node in the chart entry (which is
							// the same but another instance)
							// and set the is-part-of-alignment-derivation
							// property for it
							nodeChartEntry.getNode(inferenceNode).setIsPartOfAlignmentDerivation(true);
						}

						chartEntriesForInferences.addAll(inference.getNodeChartEntries());
					}
				}
			}

			// Necessary to avoid visiting the same chart entry multiple times
			chartEntriesForInferences.removeAll(chartEntriesVisited);

			// System.out.println("EbitgChartBuilder. markNodesPartOfAlignmentDerivation : adding number of chart entries"
			// + chartEntriesForInferences.size() );

			// Add the chart entries part of derivations for the nodes processed
			// for the current chart entry
			// to the queue
			chartEntriesQueu.addAll(chartEntriesForInferences);

		}
	}

	// Print the contents of the chart
	public void printChartContents() {
		getChart().printChartContents();
	}

	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> EbitgTreeGrower<T, T2> buildAndWriteTreePairsForAlignment(
			EbitgChartBuilder<T, T2> builder, boolean writeAllTrees) {
		String tempTreeFileName = "tempITGTrees.txt";

		EbitgTreeGrower<T, T2> tg = null;

		if (builder.findDerivationsAlignment()) {

			tg = builder.createEbitgTreeGrower();

			if (writeAllTrees) {
				tg.writeAllPossibleTreePairsToFile(tempTreeFileName);
			} else {
				tg.writeFirstPossibleTreePairToFile(tempTreeFileName);
			}
		} else {
			EbitgTreeGrower.writeEmptyFile(tempTreeFileName);
			System.out.println("No trees could be generated");
		}

		return tg;
	}

	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> EbitgTreeGrower<T, T2> buildAndWriteTreesForAlignment(
			EbitgChartBuilder<T, T2> builder, int maxAllowedInferencesPerNode) {
		builder.findDerivationsAlignment();
		EbitgTreeGrower<T, T2> tg = builder.createEbitgTreeGrower();
		tg.writeAllPossibleTreesToFile("tempITGTrees.txt");

		return tg;
	}

	public boolean isTightSourceSpan(Span chartSpan) {
		if (lexicalizer.nullsAreIncludedDuringParsing()) {
			return theChart.isTightSourceSpan(chartSpan);
		}
		return true;
	}

	protected abstract T createLabelEnrichedInferenceFromLexicalizedInference(EbitgLexicalizedInference ebitgLexicalizedInference);

	private T getTightLexicalizedInferenceFromCacheOrCreateAndAddNew(EbitgInference rawInference) {
		if (this.lexicalizedInferenceCacher.cacheContainsTightEbitgLexicalizedInference(rawInference)) {
			return this.lexicalizedInferenceCacher.getTightLexicalizedInference(rawInference);
		} else {

			EbitgLexicalizedInference ebitgLexicalizedInference = this.lexicalizer.createTightLexicalizedInference(rawInference);
			T result = createLabelEnrichedInferenceFromLexicalizedInference(ebitgLexicalizedInference);
			this.lexicalizedInferenceCacher.addTightEbitgLexicalizedInferenceToCache(rawInference, result);
			return result;
		}
	}

	public EbitgLexicalizedInference getTightPhrasePairForChartSpan(Span chartSpan) {

		if ((chartSpan != null) && (isTightSourceSpan(chartSpan))) {
			EbitgChartEntry entry = theChart.getChartEntry(chartSpan.getFirst(), chartSpan.getSecond());

			EbitgInference rawInference = entry.getFirstCompleteMinimumTargetBindingInference();

			if (rawInference != null) {
				if (useCaching) {
					return getTightLexicalizedInferenceFromCacheOrCreateAndAddNew(rawInference);
				}

				return this.lexicalizer.createTightLexicalizedInference(rawInference);
			}
		}

		return null;
	}

	public T getTightPhrasePair(Span originalSourceSpan) {
		Span chartSpan = lexicalizer.getChartSpan(originalSourceSpan);
		//System.out.println("chartSpan: " + chartSpan);
		return createLabelEnrichedInferenceFromLexicalizedInference(getTightPhrasePairForChartSpan(chartSpan));
	}

	public SentencePairAlignmentChunking getAlignmentChunking() {
		return this.lexicalizer.getChartInternalAlignmentChunking();
	}

	public Lexicalizer getLexicalizer() {
		return this.lexicalizer;
	}

	public EbitgChart getChart() {
		return theChart;
	}

	public abstract EbitgTreeGrower<T, T2> createEbitgTreeGrower();

}
