/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_lexicalization.Lexicalizer;

import java.util.ArrayList;
import java.util.List;

import util.Pair;


/**
 * The EbitgChart class stores the Ebitg chart.
 * @author gmaillet
 *
 */
public class EbitgChart 
{

	/** A note on past mistakes and memory leaks:
	 * 
	 * Interestingly, having a separate class to store the chart is not only good from 
	 * a modeling point of view, but also sort of necessary if we want to have a class 
	 * EbitgChartInitializer that can initialize the chart. If, as we did in the past, we 
	 * would make the mistake to store a pointer to EbitgChartBuilder in EbitgChartInitializer,
	 * then we will create a huge memory leak. What happens then, is when an object EbigChartBuilder
	 * is no longer referenced, it would still be referenced by the EbitgChartInitializer it contains.
	 * Thus the EbitgChartBuilder could then not be freed from memory, and as we were building charts
	 * in a loop over sentence pairs, the memory would be rapidly filled with the contents of all 
	 * EbitgChartBuilder that all had to be kept in memory. After eliminating the back reference in 
	 * EbitgChartInitializer to the encapsulating object EbitgChartBuilder, and replacing this dependency
	 * by the dependency on this class EbitgChart that is created by EbitgChartBuilder but shared by 
	 * EbitgChartBuilder and EbitgChartInitializer, we eliminated the memory leak problems.
	 */
	
	
	//protected ArrayList<ArrayList<EbitgChartEntry>> chart;
	private final  EbitgChartEntry[][] chart;
	private final int sourceLength, targetLength;
	
	private EbitgChart(EbitgChartEntry[][] chart, int sourceLength,int targetLength)
	{
		this.chart = chart;
		this.sourceLength = sourceLength;
		this.targetLength = targetLength;
	}

	public static EbitgChart createEbitgChart(Lexicalizer lexicalizer)
	{
		
		int sourceLength = lexicalizer.getChartInternalAlignmentChunking().getSourceLength();
		int targetLength = lexicalizer.getChartInternalAlignmentChunking().getTargetLength();
		return new EbitgChart(createChartEntriesTable(sourceLength, lexicalizer),sourceLength,targetLength);
	}
	
	/**
	 *  Set up the chart, create special chart entries for the spanlenght=1
	 *  entries 
	 */
	private static  EbitgChartEntry[][] createChartEntriesTable(int sourceLength, Lexicalizer lexicalizer)
	{				
		
		//System.out.println("theChart.getSourceLength(): " + theChart.getSourceLength() + "chartBuilder.targetLength: "+ theChart.getTargetLength());
		// allocate the chart
		//theChart.chart = new ArrayList<ArrayList< EbitgChartEntry>>(); 
		 EbitgChartEntry[][] result = new EbitgChartEntry[sourceLength][sourceLength]; 
		
		// Set the proper i and j values for the entries in the chart
		for(int i = 0; i < sourceLength; i++)
		{
			for(int j = 0; j < sourceLength; j++)
			{
				// only initialize elements with j >= i
				if(j>= i)
				{	
					
					if(i == j)
					{					
						//theChart.chart.get(i).add(new  EbitgChartEntry(i,alignmentChunking));
						result[i][j] = new  EbitgChartEntry(i,lexicalizer);
					}
					else
					{
						//theChart.chart.get(i).add(new  EbitgChartEntry(i,j,  alignmentChunking));
						result[i][j] = new  EbitgChartEntry(i,j,  lexicalizer);
					}
				}
				
				else
				{
					// add null entry for 
					//theChart.chart.get(i).add(null);
				}
			}
		}
		
		//System.out.println("theChart.chart.length" + theChart.getChart().length);
		//System.out.println("theChart.chart[0].length" + theChart.getChart()[0].length);
		return result;
	}

	
	public int getSourceLength() 
	{
		return sourceLength;
	}

	public int getTargetLength() {
		return targetLength;
	}
	
	public EbitgChartEntry getChartEntry(int i, int j)
	{
		//return chart.get(i).get(j);
		//System.out.println("EbitgChart.getChartEntry(" + i + "," + j + ")");
		return getChart()[i][j];
	}
	
	public List<EbitgChartEntry> getChartEntriesAsList()
	{
		List<EbitgChartEntry> result = new ArrayList<EbitgChartEntry>();
		
		for(int i = 0; i < chart.length; i++)
		{
			for(int j = 0; j < chart[0].length; j++)
			{
				if(chart[i][j] != null)
				{	
					result.add(chart[i][j]);
				}	
			}
		}
		
		return result;
		
		
	}
	
	
	/** Method that checks whether an inference covers all the target positions in the chart
	 * 
	 * @param inference The inference to check it for
	 * @return Whether all target positions are covered
	 */
	public boolean allTargetPositionsCovered(EbitgInference inference)
	{
		int numBoundTargetPositions = inference.getBoundTargetPositions().size();
		if(numBoundTargetPositions == this.targetLength)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean isWholeSourceRangeCoveringEntry(EbitgChartEntry entry)
	{
		return ((entry.getI() == 0) && (entry.getJ() == sourceLength - 1));
	}

	public EbitgChartEntry[][] getChart() {
		return chart;
	}

	public void printChartContents()
	{
		
		System.out.println("Printing the contents of the chart:");
		
		// loop over the span length from 0 to the length of the source permutation
		for(int spanLength = 1; spanLength <= this.getSourceLength(); spanLength++)
		{
			System.out.println("\nEntries with spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (this.getSourceLength() - spanLength + 1); beginIndex++)
			{	
				 EbitgChartEntry entry = this.getChartEntry(beginIndex, beginIndex + spanLength - 1);
				if(entry != null)
				{	
					System.out.println(entry.toString());
				}	
			}	
		}
	}
	
	
	/**
	 * Function that determines whether a chart entry has no extra bound source positions on the edges.
	 * In order for this to be true, the two length one entries on left and right edges of the entry
	 * must themselves be atomic, i.e. have inferences.
	 * @param i
	 * @param j
	 * @return
	 */
	protected boolean isTightSourceSpan(Pair<Integer> sourceSpan)
	{
		//System.out.println("SourceSpan: "+  sourceSpan);
		boolean leftEdgeContainsInferrableEntry = getChartEntry(sourceSpan.getFirst(), sourceSpan.getFirst()).hasInferences();
		boolean rightEdgeContainsInferrableEntry = getChartEntry(sourceSpan.getSecond(), sourceSpan.getSecond()).hasInferences();
		
		//System.out.println(leftEdgeContainsInferrableEntry);
		//System.out.println(rightEdgeContainsInferrableEntry);
		boolean bothEdgesAreAligned = leftEdgeContainsInferrableEntry && rightEdgeContainsInferrableEntry; 
		return (bothEdgesAreAligned);
	}
	

	public EbitgChartEntry getTopChartEntry()
	{
		return this.getChartEntry(0, this.chart.length - 1);
	}
	
	
	
	
}
