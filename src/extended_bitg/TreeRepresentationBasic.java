/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.List;

public class TreeRepresentationBasic extends TreeRepresentation<EbitgLexicalizedInference,EbitgTreeStateBasic> 
{

	protected TreeRepresentationBasic(
			EbitgTreeStateBasic containingState,
			String treeString, String twinTreeString) {
		super(containingState, treeString, twinTreeString);
	}
	
	public static   TreeRepresentationBasic createEbitgTreeStateTreeRepresentation(EbitgTreeStateBasic containingState)
	{
		return new TreeRepresentationBasic(containingState, null,null);
	}


	@Override
	protected List<String> getAdditionalExtraLabels() 
	{
		return new ArrayList<String>();
	}

}
