/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;
import java.awt.Graphics2D;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;



public class TestUnicodePDF {
	

	  public static void main(String[] args) throws Exception {
	    Document document = new Document();
	    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("/home/gemaille/Desktop/2.pdf"));
	    document.open();
	
	    PdfContentByte cb = writer.getDirectContent();
	    PdfTemplate tp = cb.createTemplate(100, 50);
	    Graphics2D g2 = tp.createGraphicsShapes(100, 50);
	    java.awt.Font font = new java.awt.Font("Arial Unicode MS", java.awt.Font.PLAIN, 12);
	    g2.setFont(font);
	    String text = "\u5e73\u548C";
	    g2.drawString(text, 0, 40);
	    g2.drawRect(0, 0, 100, 100);
	    g2.dispose();
	    cb.addTemplate(tp, 36, 780);
	    document.close();
	  }


}
