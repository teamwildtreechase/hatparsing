/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import util.Span;
import hat_lexicalization.Lexicalizer;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import ccgLabeling.CCGLabels;
import ccgLabeling.CCGLabeler;

public class EbitgLexicalizedInference extends EbitgInference {

	private final EbitgInferenceLexicalization lexicalization;
	private final CCGLabels ccgLabels;

	public EbitgLexicalizedInference(
			EbitgInferenceTargetProperties targetProperties,
			List<EbitgNode> nodes, List<EbitgChartEntry> nodeChartEntries,
			boolean inverted, InferenceType inferenceType,
			EbitgChartEntry containingChartEntry,
			EbitgInferenceLexicalization lexicalization,
			EbitgInferencePermutationProperties permutationProperties,
			CCGLabels ccgLabels) {
		super(targetProperties, nodes, nodeChartEntries, inverted,
				inferenceType, containingChartEntry);
		this.lexicalization = lexicalization;
		this.setPermutationProperties(permutationProperties);
		this.ccgLabels = ccgLabels;
	}

	public static EbitgLexicalizedInference createEbitgLexicalizedInference(
			EbitgInference inference, Lexicalizer lexicalizer,
			CCGLabeler ccgLabeler) {
		// System.out.println(" createEbitgLexicalizedInferenceFromRawInference called");
		assert (inference != null);
		Span nullExtendedSourceSpan = lexicalizer
				.createLexicalizedNullExtendedSourceSpan(inference
						.getContainingChartEntry());
		EbitgInferenceTargetProperties nullExtendedTargetProperties = lexicalizer
				.createReLexicalizedTargetProperties(inference
						.getTargetProperties());
		EbitgInferenceLexicalization lexicalization = EbitgInferenceLexicalization
				.createEbitgInferenceLexicalization(
						nullExtendedTargetProperties, lexicalizer,
						nullExtendedSourceSpan);
		return new EbitgLexicalizedInference(nullExtendedTargetProperties,
				inference.getNodes(), inference.getNodeChartEntries(),
				inference.isInverted(), recomputeInferenceType(
						inference.getInferenceType(), lexicalization),
				inference.getContainingChartEntry(), lexicalization,
				inference.getPermutationProperties(),
				ccgLabeler.computeCCGLabels(nullExtendedSourceSpan,
						nullExtendedTargetProperties));
	}

	public static EbitgLexicalizedInference createEbitgTightLexicalizedInferenceFromRawInference(
			EbitgInference inference, Lexicalizer lexicalizer,
			CCGLabeler ccgLabeler) {
		// System.out.println(" createEbitgTightLexicalizedInferenceFromRawInference called");
		// lexicalizer.printOriginalAlignmentTriple();
		Span nullExtendedSourceSpan = lexicalizer
				.createLexicalizedTightSourceSpan(inference
						.getContainingChartEntry());
		EbitgInferenceTargetProperties nullExtendedTargetProperties = lexicalizer
				.createReLexicalizedTightTargetProperties(inference
						.getTargetProperties());
		EbitgInferenceLexicalization lexicalization = EbitgInferenceLexicalization
				.createEbitgInferenceLexicalization(
						nullExtendedTargetProperties, lexicalizer,
						nullExtendedSourceSpan);
		return new EbitgLexicalizedInference(nullExtendedTargetProperties,
				inference.getNodes(), inference.getNodeChartEntries(),
				inference.isInverted(), recomputeInferenceType(
						inference.getInferenceType(), lexicalization),
				inference.getContainingChartEntry(), lexicalization,
				inference.getPermutationProperties(),
				ccgLabeler.computeCCGLabels(nullExtendedSourceSpan,
						nullExtendedTargetProperties));
	}

	protected static InferenceType recomputeInferenceType(
			InferenceType originalType,
			EbitgInferenceLexicalization lexicalization) {
		if (originalType == EbitgInference.InferenceType.Atomic) {
			boolean bindsExtraNulls = lexicalization.getSourceSpan()
					.getSpanLength() > 1;
			if (bindsExtraNulls) {
				return InferenceType.SourceNullsBinding;
			}
		}
		return originalType;
	}

	public Map<Integer, String> getGeneratedTargetWordsMap() {
		return Collections.unmodifiableMap(lexicalization
				.getGeneratedTargetWordsMap());
	}

	public EbitgInferenceLexicalization getLexicalization() {
		return this.lexicalization;
	}

	public int getSourceSpanLength() {
		return this.lexicalization.getSourceSpanLength();
	}

	public List<String> getGeneratedTargetWords() {
		return this.lexicalization.getGeneratedTargetWords();
	}

	public List<String> generatedSourceWords() {
		return getLexicalization().getGeneratedSourceWords();
	}

	protected String toTreeString() {
		InferenceType type = this.getInferenceType();

		List<String> generatedTargetWords = this.lexicalization
				.getGeneratedTargetWords();
		return treeString(generatedTargetWords, type, getSortedSourceSetPermuation());
	}

	/*
	 * private String getExtraLabels() { if(this.ccgLabels != null &&
	 * this.ccgLabels.hasSourceLabel()) { return
	 * ccgLabels.getCCGSourceLabelString(); } return null; }
	 */

	List<String> getSourceWords() {
		return this.lexicalization.getGeneratedSourceWords();
	}

	protected boolean containsAbsoluteTargetPosition(int absoluteTargetPosition) {
		return this.getBoundTargetPositions().contains(absoluteTargetPosition);
	}

	public String toString() {
		String result = super.toString();

		result += "\n<Lexicalized extension> source span: "
				+ this.lexicalization.getSourceSpan();
		result += "\n</Lexicalized extension>";
		return result;
	}

	public InferenceLengthProperties getInferenceLengthProperties() {
		return InferenceLengthProperties.createInferenceLengthProperties(this);
	}

	public boolean isSourceNullBinding() {
		return isOfInferenceType(EbitgInference.InferenceType.SourceNullsBinding);
	}

	public Span getSourceSpan() {
		return this.lexicalization.getSourceSpan();
	}

	public boolean hasOneNodeCharEntry() {
		return getNodeChartEntries().size() == 1;
	}

	public CCGLabels getCCGLabels() {
		return this.ccgLabels;
	}

}