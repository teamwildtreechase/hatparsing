/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;

public class EbitgTreeStateBasic extends EbitgTreeState<EbitgLexicalizedInference, EbitgTreeStateBasic>
{

	protected EbitgTreeStateBasic(EbitgTreeStateChartBacking<EbitgLexicalizedInference, EbitgTreeStateBasic> treeStateChartBacking, EbitgNode node,
			ArrayList<EbitgInference> inferences, EbitgTreeStateBasic parent, int parentRelativeChildNumber, int parentRelativePermutationGroup, String nodeID,
			String nodeLabel, boolean hasMoreInferences) {
		super(treeStateChartBacking, node, inferences, parent, parentRelativeChildNumber, parentRelativePermutationGroup, nodeID, nodeLabel, hasMoreInferences);
	}


	@Override
	public EbitgTreeStateBasic createEbitgTreeState(EbitgTreeStateChartBacking<EbitgLexicalizedInference, EbitgTreeStateBasic> treeStateChartBacking,
			EbitgNode node, ArrayList<EbitgInference> inferences, 
			EbitgTreeStateBasic parent,
			int parentRelativeChildNumber, int parentRelativePermutationGroup) 
	{
		return createEbitgTreeStateBasic(treeStateChartBacking, node, inferences, parent, parentRelativeChildNumber, parentRelativePermutationGroup);
	}
	
	public static EbitgTreeStateBasic createEbitgTreeStateBasic(EbitgTreeStateChartBacking<EbitgLexicalizedInference, EbitgTreeStateBasic> treeStateChartBacking,
			EbitgNode node, ArrayList<EbitgInference> inferences, 
			EbitgTreeStateBasic parent,
			int parentRelativeChildNumber, int parentRelativePermutationGroup)
	{
		boolean hasMoreInferences = true;
		EbitgTreeStateBasic state = new EbitgTreeStateBasic(treeStateChartBacking,node, inferences,  parent,  parentRelativeChildNumber, parentRelativePermutationGroup, computeNodeIDFromParent(parent, parentRelativeChildNumber), computeNodeLabelFromParent(parent, parentRelativePermutationGroup),hasMoreInferences);
		return state;
		
	}


	@Override
	protected void collectInformationForExtraLabels() 
	{
	}
	
	@Override
	protected TreeRepresentation<EbitgLexicalizedInference,EbitgTreeStateBasic> createTreeRepresentation(EbitgTreeStateBasic containingState) 
	{
		return TreeRepresentationBasic.createEbitgTreeStateTreeRepresentation(containingState);
	}
	
}
