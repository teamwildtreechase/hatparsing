/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import reorderingLabeling.FinePhraseCentricReorderingLabel;
import ccgLabeling.CCGLabel;
import ccgLabeling.CCGLabels;
import util.ParseAndTagBracketAndLabelConverter;
import util.SpecialCharacters;
import util.Utility;
import util.XMLTagging;

public abstract class TreeRepresentation<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> {
	public static final String LEXICAL_PRODUCTIONS_INDICATOR = "=>"; 
	private static final String HAT_LABEL_FOR_TREE_VISUALIZATION = "COMPLEX";
  
    private T2 containingState;
	private String treeString; // the tree String for the (sub)tree rooted this
								// state
	private String twinTreeString; // The tree that results from executing the
									// Eitg operations on the original tree

	public static String HAT_REORDERING_LABEL_PROPERTY = "HATReorderingLabel";
	public static String PARENT_RELATIVE_REORDERING_LABEL_PROPERTY = "ParentRelativeReorderingLabel";
	private static final String NullBindingString = "NULLBINDING";
	public static final String SourceCategory = "Source";
	public static final String TargetCategory = "Target";

	public static final String ExtraLabelsProperty = "ExtraLabels";
	public static final String VarNameProperty = "VarName";

	protected TreeRepresentation(T2 containingState, String treeString,
			String twinTreeString) {
		this.containingState = containingState;
		this.treeString = treeString;
		this.twinTreeString = twinTreeString;
	}

	protected abstract List<String> getAdditionalExtraLabels();

	public String getTreeString() {
		return treeString;
	}

	public String getTwinTreeString() {
		return twinTreeString;
	}

	public void generateNonTerminalStateTreeStrings() {
		this.treeString = generateNonTerminalStateTreeString();
		this.twinTreeString = this.generateTwinNonTerminalTreeString();

		if (containingState.parent == null) {
			// add surrounding brackets and ending semicolon to finish the
			// constructed tree
			// this.treeString = "(" + this.treeString + ").";

			// System.out.println("Root EbitgTreeState is complete => whole tree completed!");
			// System.out.println("Generated treeString for root: \n\n" +
			// this.treeString);
		}
	}

	private String generateNonTerminalStateTreeString() {
		String treeString = "( "
				+ containingState.getTreeStringChosenInference();
		treeString += getExtraLabelsString();// Add node identifier for twin
												// node matching
		for (EbitgTreeState<T, T2> child : containingState.getChildren()) {
			treeString += child.treeRepresentation.treeString + " ";
		}
		treeString += " )";

		return treeString;
	}

	public static String getExtraLabelsLeftTag() {
		return XMLTagging.getLeftXMLTag(ExtraLabelsProperty);
	}

	public static String getExtraLabelsRightTag() {
		return XMLTagging.getRightXMLTag(ExtraLabelsProperty);
	}

	public static String getVarNameLeftTag() {
		return XMLTagging.getLeftXMLTag(VarNameProperty);
	}

	public static String getVarNameRightTag() {
		return XMLTagging.getRightXMLTag(VarNameProperty);
	}

	protected String getExtraLabelsString() {
		String result = getExtraLabelsLeftTag();

		result += getNodeLabelString();

		result += getCCGLabelsString();

		for (String extraLabel : getAdditionalExtraLabels()) {
			result += "&" + extraLabel;
		}

		result += "&" + getHATReorderingLabel();
		result += "&" + getParentRelativeReorderingLabel();

		result += getExtraLabelsRightTag();
		return result;
	}

	private static String getHATReorderingLabelLeftTag() {
		return XMLTagging
				.getLeftXMLTag(HAT_REORDERING_LABEL_PROPERTY);
	}

	private static String getHATReorderingLabelRightTag() {
		return XMLTagging
				.getRightXMLTag(HAT_REORDERING_LABEL_PROPERTY);
	}
	
	private static String getParentRelativeReorderingLabelLeftTag() {
		return XMLTagging
				.getLeftXMLTag(PARENT_RELATIVE_REORDERING_LABEL_PROPERTY);
	}

	private static String getParentRelativeReorderingLabelRightTag() {
		return XMLTagging
				.getRightXMLTag(PARENT_RELATIVE_REORDERING_LABEL_PROPERTY);
	}

	private String getFinePhraseCentricReorderingLabelString() {
		FinePhraseCentricReorderingLabel label = this.getContainingState().getChosenInfernce()
				.getFinePhraseCentricReorderingLabel();
    // In publications etc we use the label "COMPLEX" instead of "HAT" 
		// for labeling complex reordering nodes in the HAT. So we use this label 
		// here, substituting it for the original label "HAT"
		if(label.isHatLabel()){
		  return HAT_LABEL_FOR_TREE_VISUALIZATION;
		}
		return label.getLabel();
	}

	


	private String getHATReorderingLabel() {
		return getHATReorderingLabelLeftTag()
				+ getFinePhraseCentricReorderingLabelString()
				+ getHATReorderingLabelRightTag();
		//return getReorderingLabelLeftTag()
		//		+ getParentRelativeReorderingLabelString()
		//		+ getReorderingLabelRightTag();
	}
	
	private String getParentRelativeReorderingLabel() {
		return getParentRelativeReorderingLabelLeftTag()
				+ getParentRelativeReorderingLabelString()
				+ getParentRelativeReorderingLabelRightTag();
	}
	
	private String getParentRelativeReorderingLabelString() {
		return this.getContainingState().getChosenInfernce()
				.getParentRelativeReorderingLabel().getLabel();
	}

	private String getNodeLabelString() {
		return getVarNameLeftTag() + "@" + containingState.nodeLabel + "@"
				+ getVarNameRightTag();
	}

	private CCGLabels getCCGLabels() {
		return this.containingState.getChosenInfernce().getCCGLabels();
	}

	private static String getCCGLeftLabelTag(String category) {
		return XMLTagging.getLeftXMLTag(CCGLabel.getCCGLabelProperty(category));
	}

	private static String getCCGRightLabelTag(String category) {
		return XMLTagging
				.getRightXMLTag(CCGLabel.getCCGLabelProperty(category));
	}

	private static String getCCGXMLString(String ccgLabelString, String category) {
		return getCCGLeftLabelTag(category) + ccgLabelString
				+ getCCGRightLabelTag(category);
	}

	private String getCCGLabelsString() {
		String result = "";
		if (getCCGLabels().hasSourceLabel()) {
			result += "&"
					+ getCCGXMLString(getCCGLabels().getCCGSourceLabelString(),
							SourceCategory);
		}
		if (getCCGLabels().hasTargetLabel()) {
			result += "&"
					+ getCCGXMLString(getCCGLabels().getCCGTargetLabelString(),
							TargetCategory);
		}

		return result;
	}

	private EbitgTreeState<T, T2> getFirstInvolvedState(
			SortedSet<Integer> targetToSourceSet) {
		return containingState.getChildren().get(targetToSourceSet.first());
	}

	/**
	 * This method checks, based on the targetToSourceSet - which is obtained
	 * from the sortedInvertedPermutation - whether this targetToSourceSet (and
	 * the corresponding target side element) corresponds to a complete phrase
	 * pair. This is automatically the case if the inference of the first
	 * involved stated on the source side is complete, and automatically false
	 * if this is not the case.
	 * 
	 * @param targetToSourceSet
	 * @return
	 */
	private boolean correspondToNonCompletePhrasePair(
			SortedSet<Integer> targetToSourceSet) {
		return !getFirstInvolvedState(targetToSourceSet).getChosenInfernce()
				.isComplete();
	}

	private String generateTwinNonTerminalTreeString() {
		List<SortedSet<Integer>> sortedInvertedPermutation = containingState.chosenInference
				.computeSortedInvertedPermutation();
		// System.out.println("## Permutation: " +
		// this.chosenInference.getSourcePermutation());
		// System.out.println("###Inverted Permutation:" +
		// sortedInvertedPermutation);

		String treeString = "(";

		treeString += EbitgInference.treeString(null,
				containingState.chosenInference.getInferenceType(),
				sortedInvertedPermutation);
		treeString += getExtraLabelsString(); // Add node identifier for twin
												// node matching
		// System.out.println("treeString:  " + treeString);

		int relativeTargetPos = 0;

		// boolean everyTargetPositionHasOwnSet =
		// (containingState.chosenInference.getBoundTargetPositions().size() ==
		// sortedInvertedPermutation.size());

		for (SortedSet<Integer> targetToSourceSet : sortedInvertedPermutation) {

			EbitgTreeState<T, T2> firstInvolvedState = getFirstInvolvedState(targetToSourceSet);

			// System.out.println(" firstInvolvedState " + firstInvolvedState );

			// return (mapsToMultipleStates(targetToSourceSet) ||
			// (!getFirstInvolvedState(targetToSourceSet).getChosenInfernce().isComplete()));
			// if (mapsToMultipleStates(targetToSourceSet) ||
			// (!firstInvolvedState.isComplete())) {
			// New condition: seems to work
			if (correspondToNonCompletePhrasePair(targetToSourceSet)) {
				String bottomTreeString = collectInvolvedPrimitiveStatesAndComputeTreeString(
						targetToSourceSet, relativeTargetPos);
				// System.out.println(" bottomTreeString" + bottomTreeString);
				treeString += bottomTreeString + " ";
			} else {
				// take the only element from the set
				EbitgTreeState<T, T2> involvedState = firstInvolvedState;
				// Get the twinTreeString from the state
				treeString += involvedState.treeRepresentation.twinTreeString;

				//System.out.println("Gideon : involvedState.treeRepresentation.twinTreeString  "
				//				+ involvedState.treeRepresentation.twinTreeString);
			}

			relativeTargetPos++;
		}

		treeString += " )";

		// System.out.println("treeString returned : " + treeString);

		return treeString;

	}

	private List<String> findTargetWords(int relativeTargetPos) {
		// FIXME This method is not safe for the compact version of
		// EbitgChartBuilder
		// The extra bound target positions are then not properly recovered

		List<String> result = new ArrayList<String>();
		EbitgInferencePermutationProperties permutationProperties = this.containingState
				.getPermutationProperties();
		assert (permutationProperties
				.getNumberOfTargetPositionsForPermutationIndex(relativeTargetPos) > 0);
		TreeSet<Integer> relativeAssociatedTargetPositions = new TreeSet<Integer>(
				permutationProperties
						.getTargetPositionsForPermutationIndex(relativeTargetPos));

		assert (relativeAssociatedTargetPositions.size() > 0);
		for (int associatedRelativeTargetPos : relativeAssociatedTargetPositions) {
			Set<Integer> absoluteTargetPositions = this.containingState
					.getOriginalPlusAssociatedExtraBoundTargetPositions(associatedRelativeTargetPos);
			// assert(findTargetWord(absoluteTargetPos) != null);
			for (int absoluteTargetPos : absoluteTargetPositions) {
				result.add(findTargetWord(absoluteTargetPos));
			}
		}
		assert (result.size() > 0);
		return result;
	}

	public String collectInvolvedPrimitiveStatesAndComputeTreeString(
			SortedSet<Integer> sourcePositionsSet, int relativeTargetPos) {
		List<EbitgTreeState<T, T2>> involvedStates = new ArrayList<EbitgTreeState<T, T2>>();

		// collect a list of EbitgTreeStates that are needed to build the
		// TreeString for the target
		for (Integer sourcePos : sourcePositionsSet) {
			involvedStates.add(containingState.getChildTreeState(sourcePos));
		}

		// find the target words that belongs to the relative target position
		List<String> targetWords = ParseAndTagBracketAndLabelConverter
				.getStringsWithReplacedBrackets(this
						.findTargetWords(relativeTargetPos));

		// Generate the twin treestring from the set of involved states and the
		// target words list

		String bottomTreeString;
		bottomTreeString = generateTwinBottomTreeStringIncompleteStates(
				involvedStates, targetWords);

		return bottomTreeString;
	}

	private List<String> getSourceWordsForInvolvedStates(
			List<EbitgTreeState<T, T2>> involvedStates) {
		List<String> sourceWords = new ArrayList<String>();

		for (EbitgTreeState<T, T2> state : involvedStates) {
			// Collect the source words of the states that are merged
			sourceWords.addAll(state.getChosenInfernceSourceWords());
		}
		return ParseAndTagBracketAndLabelConverter
				.getStringsWithReplacedBrackets(sourceWords);
	}

	public void generateTerminalStateTreeStrings() {
		this.treeString = generateTerminalStateTreeString();
		this.twinTreeString = generateTwinTerminalStateTreeString();
	}

	private String generateTerminalStateTreeString() {

		String treeString = "";
		boolean extraBracketRequired = false;

		if (containingState.chosenInferenceIsComplete()) {
			treeString = "(" + containingState.getTreeStringChosenInference();
			treeString += getExtraLabelsString(); // Add node identifier for
													// twin node matching
			extraBracketRequired = true;
		}

		// GIDEON: This is to deal with null binding words that should be
		// grouped actually
		else if (containingState.isSourceNullBinding()) {
			treeString = "(" + NullBindingString
					+ this.containingState.getTreeStringChosenInference();
			extraBracketRequired = true;
		}

		sanityCheckStateIsTerminal();

		// System.out.println("this.gettwinTreeStringChosenInference().toTreeString()"
		// + this.getChosenInference().toTreeString());
		// System.out.println("entry.getSourceWords(): " +
		// entry.getSourceWords());
		// System.out.println("alignment chunking sourcewords:" +
		// entry.alignmentChunking.getSourceWords());

		treeString += listString(containingState.chosenInference
				.getSourceWords());

		if (extraBracketRequired) {
			treeString += ")";
		}

		return treeString;
	}

	private void sanityCheckStateIsTerminal() {
		if (containingState.hasChosenInferenceWithOneNodeCharEntry()) {
			return;
		}
		throw new RuntimeException("state is not terminal");
	}

	private ArrayList<SortedSet<Integer>> createTwinTerminalStateTargetPermutation(
			List<String> targetWords) {
		ArrayList<SortedSet<Integer>> targetPermutation = new ArrayList<SortedSet<Integer>>();

		for (int wordNum = 0; wordNum < targetWords.size(); wordNum++) {
			TreeSet<Integer> targetPermutationPositions = new TreeSet<Integer>();
			targetPermutationPositions.add(0);
			targetPermutation.add(targetPermutationPositions);
		}
		return targetPermutation;
	}

	/**
	 * This method generates a tree string for a terminal state This means this
	 * method should not be called for states that are themselves trees, for
	 * them the tree will be generated by generateTwinNonTerminalTreeString()
	 * 
	 * @return The generated twin tree String
	 */
	private String generateTwinTerminalStateTreeString() {
		List<String> targetWords = containingState
				.getGeneratedTargetWordsArray();

		// System.out.println("*\n***\n*\n generateTwinTerminalStateTreeString, targetWords :"
		// + targetWords);

		String result = "";

		result += "(";

		ArrayList<SortedSet<Integer>> targetPermutation = createTwinTerminalStateTargetPermutation(targetWords);

		result += EbitgInference.sortedPermutationString(targetPermutation);
		result += getExtraLabelsString(); // Add node identifier for twin node
											// matching

		List<String> replacedBracketTargetWords = ParseAndTagBracketAndLabelConverter
				.getStringsWithReplacedBrackets(targetWords);

		if (targetWords.size() > 1) {
			for (String word : replacedBracketTargetWords) {
				result += " " + word;
			}
		} else {
			result += LEXICAL_PRODUCTIONS_INDICATOR;
			List<EbitgTreeState<T, T2>> involvedStates = new ArrayList<EbitgTreeState<T, T2>>();
			involvedStates.add(containingState);
			result += Utility
					.stringListString(getSourceWordsForInvolvedStates(involvedStates));
			result += " " + replacedBracketTargetWords.get(0);
		}

		result += ")";

		return result;

	}

	String generateTargetNullBindingLabel(int noTargetWords,
			List<String> sourceWords) {
		// Make a list of target word positions that are combined
		String result = NullBindingString + SpecialCharacters.LeftSquareBracket
				+ "{";
		for (int i = 0; i < noTargetWords - 1; i++) {
			result += i + ",";
		}
		result += (noTargetWords - 1) + "}"
				+ SpecialCharacters.RightSquareBracket;
		result += LEXICAL_PRODUCTIONS_INDICATOR;
		// Add the source words generated
		result = result + "[";
		for (int i = 0; i < sourceWords.size() - 1; i++) {
			result += sourceWords.get(i) + ",";
		}
		result += sourceWords.get(sourceWords.size() - 1) + "]";
		return result;
	}

	/**
	 * This method generates a tree string for a list of involved incomplete
	 * states and target words This means this method should not be called for
	 * states that are themselves trees, for them the tree will be generated by
	 * generateTwinNonTerminalTreeString()
	 * 
	 * @param involvedStates
	 *            : an array lists of states that contribute
	 * @param targetWords
	 *            : a list of the target words that are involved
	 * @return The generated twin tree String
	 */
	public String generateTwinBottomTreeStringIncompleteStates(
			List<EbitgTreeState<T, T2>> involvedStates, List<String> targetWords) {
		String result = "";

		if (targetWords.size() > 1) {
			result += "( "
					+ generateTargetNullBindingLabel(targetWords.size(),
							getSourceWordsForInvolvedStates(involvedStates));
			for (String word : targetWords) {
				result += " " + word;
			}
			result += ") ";
		} else {
			result += " " + targetWords.get(0);
		}

		return result;
	}

	/**
	 * This function takes an absolute target position and finds for it the
	 * associated target word
	 * 
	 * @param absoluteTargetPosition
	 * @return The associated target word
	 */
	public String findTargetWord(int absoluteTargetPosition) {
		for (EbitgTreeState<T, T2> childState : containingState.getChildren()) {
			// If the set of (relative) target positions for this source
			// position contains the target
			// position that we are looking for
			if (childState
					.chosenInferenceContainsAbsoluteTargetPosition(absoluteTargetPosition)) {
				return childState
						.getGeneratedTargetWord(absoluteTargetPosition);
			}
		}

		return null;
	}

	/**
	 * Generate a String for an ArrayList of source words
	 * 
	 * @param list
	 * @return The list as a String of Words separated by a space
	 */
	private static String listString(List<String> list) {
		String result = "";
		for (String sourceWord : ParseAndTagBracketAndLabelConverter
				.getStringsWithReplacedBrackets(list)) {
			result += " " + sourceWord;
		}

		return result;
	}

	protected T2 getContainingState() {
		return this.containingState;
	}

}
