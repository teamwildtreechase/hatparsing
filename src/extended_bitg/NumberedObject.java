/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

/**
 * Generic NumberdObject class, used in sorting objects
 * @author gemaille
 *
 * @param <ObjectType>
 */
public class NumberedObject<ObjectType>  implements Comparable<NumberedObject<ObjectType>>
{
	private ObjectType object;
	private int number;
	
	public NumberedObject(ObjectType object, int number)
	{
		this.object = object;
		this.number = number;
	}
		
	public ObjectType getObject()
	{
		return object;
	}
	
	public int getNumber()
	{
		return number;
	}

	@Override
	public int compareTo(NumberedObject<ObjectType> object2) 
	{
		return (this.number - object2.number);
	}

	public String toString()
	{
		return "" + object + " " + number;
	}
}
