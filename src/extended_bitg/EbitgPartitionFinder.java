/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.List;

import util.Pair;

public class EbitgPartitionFinder 
{
	final EbitgChart theChart;
	final EbitgChartEntry chartEntry;
	final EbitgMinPartitionsChart minPartitionsChart;

	private EbitgPartitionFinder(EbitgMinPartitionsChart minPartitionsChart, EbitgChartEntry chartEntry, EbitgChart theChart)
	{
		this.chartEntry = chartEntry;
		this.minPartitionsChart = minPartitionsChart;
		this.theChart = theChart;
	}

 
	public static EbitgPartitionFinder createEbitgPartitionFinder(EbitgChartEntry chartEntry, EbitgChart theChart)
	{
		EbitgMinPartitionsChart minPartitionsChart = EbitgMinPartitionsChart.createMinPartitionsChart(chartEntry, theChart); 
		return new EbitgPartitionFinder(minPartitionsChart, chartEntry, theChart);
	}
	 
	private List<PartitionsList> findMinimalEntryPartitions()
	{
		    // Unpack the chart to enumerate all minimal partitions as lists of spans in the original entry
		List<PartitionsList> minEntryPartitions  = unpackBackPointersMinPartitions();
		
		// Return the list of PartitionLists, each PartitionList containing a minimal partition
			return minEntryPartitions;
		}
	
	
	public List<PartitionsList>  unpackBackPointersMinPartitions()
	{	
		List<PartitionsList> incompleteMinNumComponentsPartitions = new ArrayList<PartitionsList>();
		List<PartitionsList> finishedMinNumComponentsPartitions = new ArrayList<PartitionsList>();
		
		
		incompleteMinNumComponentsPartitions.add(new PartitionsList(this.minPartitionsChart.getLastPartitionEntry())); 
		
		
		while(! incompleteMinNumComponentsPartitions.isEmpty())
		{
			
			////System.out.println(" incompleteMinNumComponentsPartitions :"  +  incompleteMinNumComponentsPartitions);
		
		List<PartitionsList> newIncompleteMinNumComponentsPartitions = new ArrayList<PartitionsList>();
		
		for(PartitionsList list : incompleteMinNumComponentsPartitions)
		{
			// if the front of the PartitionsList has been reached, and thus the list is complete
			if(list.getHeadEntry().getIndex() == this.chartEntry.getI())
			{
				finishedMinNumComponentsPartitions.add(list); 
			}
			else
			{	
				// Add all possible viterbi extensions of list to newList
				for(PartitionEntry parent : list.getHeadEntry().getViterbiParents())
				{
					// make a copy of the old list
					PartitionsList newList = new PartitionsList(list);
					
					Pair<Integer> newPair = new Pair<Integer> (parent.getIndex(),list.getHeadEntry().getIndex() - 1);
					newList.getPartitions().addFirst(newPair);
					newList.setHeadEntry(parent);
				
					newIncompleteMinNumComponentsPartitions.add(newList);
				}							
			}
		}
		
		// replace incompleteMinNumComponentsPartitions with the new list
			incompleteMinNumComponentsPartitions = newIncompleteMinNumComponentsPartitions;
		}
		
		return finishedMinNumComponentsPartitions;
	}
	
	public List<List<EbitgChartEntry>> generatePartitionings()
	{
		List<PartitionsList> minComponentPartitionLists = this.findMinimalEntryPartitions();
		return this.generatePartitioningsFromPartitionLists(minComponentPartitionLists);
	}
	
	
	private List<List<EbitgChartEntry>> generatePartitioningsFromPartitionLists(List<PartitionsList> minComponentPartitionLists) 
	{	
		
		List<List<EbitgChartEntry>> partitionings = new  ArrayList<List<EbitgChartEntry>> ();
		
		for(PartitionsList partitionList : minComponentPartitionLists)
		{
			ArrayList<EbitgChartEntry> partitioning = new ArrayList<EbitgChartEntry>();
			for(int index = 0; index < partitionList.getPartitions().size(); index++)
			{
				Pair<Integer> span = partitionList.getPartitions().get(index);
				partitioning.add( theChart.getChartEntry(span.getFirst(), span.getSecond()));
			}
			
			partitionings.add(partitioning);
		}
		
		return partitionings;
	
	}
}
