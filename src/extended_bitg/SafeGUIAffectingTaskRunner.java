/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingUtilities;

import viewer.AlignmentVisualizer;

public abstract class SafeGUIAffectingTaskRunner<T0, T , T2> extends Thread
{
	
	protected final T0 executerObject;
	List<T> arguments;
	protected T2 result;
	
	protected SafeGUIAffectingTaskRunner(T0 executerObject, List<T> arguments)
	{
		this.executerObject = executerObject;;
		this.arguments = arguments;
	}

	public static EbitgViewerAlignmentTripleLoader createEbitgViewerAlignmentTripleLoader(HATViewer ebitgViewer, Integer index)
	{
		return new EbitgViewerAlignmentTripleLoader(ebitgViewer,Arrays.asList(index));
	}
	
	public static EbitgViewerHatsComputer createEbitgViewerHatsComputer(HATViewer ebitgViewer)
	{
		return new EbitgViewerHatsComputer(ebitgViewer,new ArrayList<Object>());
	}
	
	public static AlignmentVisualizerCorpusLoader createAlignmentVisualizerCorpusLoader(AlignmentVisualizer alignmentVisualizer)
	{
		return new AlignmentVisualizerCorpusLoader(alignmentVisualizer, new ArrayList<Object>());
	}
	
	
	public static <T,T2 extends Object> boolean  executeThreadSafe(SafeGUIAffectingTaskRunner<T,T2, Boolean> taskRunner)
	{
    	try 
    	{
    		SwingUtilities.invokeAndWait(taskRunner);
			taskRunner.join();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
    	return taskRunner.getResult();
	}
	
	
	
	public T2 getResult()
	{
		return result;
	}
	
}

class EbitgViewerAlignmentTripleLoader extends SafeGUIAffectingTaskRunner<HATViewer,Integer,Boolean>
{
	protected EbitgViewerAlignmentTripleLoader(HATViewer ebitgViewer, List<Integer> arguments) 
	{
		super(ebitgViewer, arguments);
	}

	@Override
	public void run() 
	{
		this.executerObject.loadAlignmentTriple(arguments.get(0));
		result = true;
	}
}

class EbitgViewerHatsComputer extends SafeGUIAffectingTaskRunner<HATViewer,Object,Boolean>
{
	protected EbitgViewerHatsComputer(HATViewer ebitgViewer, List<Object> arguments) 
	{
		super(ebitgViewer, arguments);
	}

	@Override
	public void run() 
	{
		result = this.executerObject.computeHATs();
	}
	
}

class AlignmentVisualizerCorpusLoader extends SafeGUIAffectingTaskRunner<AlignmentVisualizer,Object,Boolean>
{
	protected AlignmentVisualizerCorpusLoader(AlignmentVisualizer alignmentVisualizer, List<Object> arguments) 
	{
		super(alignmentVisualizer, arguments);
	}

	@Override
	public void run() 
	{
		this.executerObject.loadEverythingFromConfigFile();
		result = true;
	}
	
}
