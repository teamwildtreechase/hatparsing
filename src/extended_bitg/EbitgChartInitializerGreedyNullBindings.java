/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_lexicalization.Lexicalizer;
import java.util.HashSet;
import java.util.List;

public class EbitgChartInitializerGreedyNullBindings extends EbitgChartInitializer{

	public EbitgChartInitializerGreedyNullBindings(EbitgChart theChart,
			Lexicalizer lexicalizer) 
	{
		super(theChart, lexicalizer);
	}

	
	protected HashSet<HashSet<Integer>> createAllAndNoneFreePositionsSets(List<Integer> freeTargetPositions)
	{
		HashSet<HashSet<Integer>> result = new HashSet<HashSet<Integer>>();
		
		HashSet<Integer> noFreePositionsSet = new HashSet<Integer>();
		HashSet<Integer> allFreePositionsSet = new HashSet<Integer>(freeTargetPositions);
		
		result.add(noFreePositionsSet);
		result.add(allFreePositionsSet);
		return result;
	}
	
	
	@Override
	protected HashSet<HashSet<Integer>> findLeftFreePositionsSets(int targetPosition) 
	{
		List<Integer> leftFreeTargetPositions = findDirectLeftFreeTargetPositionsRightToLeft(targetPosition);
		return createAllAndNoneFreePositionsSets(leftFreeTargetPositions);
	}

	@Override
	protected HashSet<HashSet<Integer>> findRightFreePositionsSets(int targetPosition) 
	{
		List<Integer> rightFreeTargetPositions = findDirectRightFreeTargetPositionsLeftToRight(targetPosition);
		return createAllAndNoneFreePositionsSets(rightFreeTargetPositions);
	}

	
	private boolean sourcePosIsAligned(int sourcePos)
	{
		return theChart.getChartEntry(sourcePos,sourcePos).isAligned();
	}

	
	private boolean isLeftChartBorderPosition(int sourcePos)
	{
		return (sourcePos == 0); 
	}
	
	private boolean isRightChartBorderPosition(int sourcePos)
	{
		return (sourcePos == theChart.getSourceLength() -1 );
	}
	
	
	private boolean entryIsAlignedOrDirectRightEntryIsAligned(int sourcePos)
	{		
		if(isRightChartBorderPosition(sourcePos))
		{
			return true;
		}
		else if(sourcePosIsAligned(sourcePos) || sourcePosIsAligned(sourcePos + 1))
		{
			return true;
		}
		
		
		return false;
	}

	
	
	private boolean entryIsAlignedOrDirectLeftEntryIsAligned(int sourcePos)
	{
		if(isLeftChartBorderPosition(sourcePos))
		{
			return true;
		}
		
		else if(sourcePosIsAligned(sourcePos) || sourcePosIsAligned(sourcePos - 1))
		{
			return true;
		}
		
		return false;
	}
	

	@Override
	protected boolean isAcceptableTypeOfSourceNullBinding(int i, int j) 
	{
		//return true;
		return (entryIsAlignedOrDirectLeftEntryIsAligned(i) && entryIsAlignedOrDirectRightEntryIsAligned(j));
	}

}
