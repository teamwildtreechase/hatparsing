/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import extended_bitg.EbitgInference.InferenceComplexityType;
import extended_bitg.EbitgInference.InferenceType;
import alignmentStatistics.SetMethods;

public abstract class EbitgTreeState<T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> {
	public enum TreeType {
		BITT, PET, HAT
	};

	private final EbitgTreeStateChartBacking<T, T2> treeStateChartBacking;
	private EbitgNode node;
	private ArrayList<EbitgInference> inferences;
	protected T chosenInference;
	protected int chosenInferenceIndex;
	protected T2 parent;
	private List<T2> children;
	protected boolean isComplete; // boolean indicating whether the subtree starting from this state is completed
	protected boolean hasMoreInferences; // boolean indicating whether the subtree rooted at this node has more inferences
	protected String nodeID; // the ID of the node, unique for every node
	protected String nodeLabel; // the label of the node, unique for groups of nodes that match together (i.e. belong to the same alignment chunk)

	protected TreeRepresentation<T, T2> treeRepresentation;

	@SuppressWarnings("unchecked")
	protected EbitgTreeState(EbitgTreeStateChartBacking<T, T2> treeStateChartBacking, EbitgNode node, ArrayList<EbitgInference> inferences, T2 parent, int parentRelativeChildNumber,
			int parentRelativePermutationGroup, String nodeID, String nodeLabel, boolean hasMoreInferences) {
		this.treeStateChartBacking = treeStateChartBacking;
		this.node = node;
		this.inferences = inferences;
		this.chosenInferenceIndex = 0;
		this.chosenInference = getChosenLabelEnrichedInference(); // Assumption: there is always at least one inference
		this.setChildren(new ArrayList<T2>());
		this.isComplete = false;
		this.parent = parent;
		this.nodeID = nodeID;
		this.nodeLabel = nodeLabel;
		this.hasMoreInferences = hasMoreInferences;
		this.treeRepresentation = this.createTreeRepresentation((T2) this);
	}

	public abstract T2 createEbitgTreeState(EbitgTreeStateChartBacking<T, T2> treeStateChartBacking, EbitgNode node, ArrayList<EbitgInference> inferences, T2 parent, int parentRelativeChildNumber,
			int parentRelativePermutationGroup);

	protected abstract void collectInformationForExtraLabels();

	protected abstract TreeRepresentation<T, T2> createTreeRepresentation(T2 containingState);

	public boolean isTerminalState() {
		return chosenInference.getNodes().isEmpty();
	}

	/**
	 * Add the children and return true if children are added and false if terminal and nothing added
	 * 
	 * @return Whether children are added
	 */
	@SuppressWarnings("unchecked")
	public boolean addChildrenForChosenInference() {
		// System.out.println("addChildrenForChosenInference() called...");
		// System.out.println("choseninference: " + chosenInference);

		if (!isTerminalState()) {

			List<Integer> permutationGrouping = findPermutationGrouping(this.chosenInference.getUnsortedSourceSetPermutation());

			for (int i = 0; i < chosenInference.getNodes().size(); i++) {
				EbitgNode childNode = chosenInference.getNodes().get(i);
				ArrayList<EbitgInference> childInferences = chosenInference.getNodeChartEntries().get(i).getInferences(childNode);

				// Add a new child with its list of inferences and a link to this State as a parent
				this.getChildren().add(createEbitgTreeState(treeStateChartBacking, childNode, childInferences, (T2) this, i, permutationGrouping.get(i)));
				// System.out.println("added a child...");
			}
			return true;
		} else // A terminal node was reached
		{
			// System.out.println("Terminal node reached, no childeren added");
			return false;
		}
	}

	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> String computeNodeLabelFromParent(EbitgTreeState<T, T2> parent, int parentRelativePermutationGroup) {
		String nodeLabel;
		if (parent != null) {
			nodeLabel = parent.nodeID + "-" + parentRelativePermutationGroup;

		} else // this is the root node
		{
			nodeLabel = "0";
		}
		return nodeLabel;
	}

	protected static <T extends EbitgLexicalizedInference, T2 extends EbitgTreeState<T, T2>> String computeNodeIDFromParent(EbitgTreeState<T, T2> parent, int parentRelativeChildNumber) {
		String nodeID;
		if (parent != null) {
			nodeID = parent.nodeID + "-" + parentRelativeChildNumber;

		} else // this is the root node
		{
			nodeID = "0";
		}
		return nodeID;
	}

	private T2 getFirstChildTreeState() {
		return getChildren().get(0);
	}

	private void makeStateWithoutChildrenAddedComplete() {
		if (addChildrenForChosenInference()) {
			getFirstChildTreeState().makeTreeComplete();
		} else {
			this.isComplete = true;
			// the label of the node with extra brackets around it is the treeString of a terminal
			// this.treeString = "(" + this.node.toString() + " word)";

			collectInformationForExtraLabels();
			this.treeRepresentation.generateTerminalStateTreeStrings();

			// If the tree has only one node, we start from the incomplete root, and end up
			// adding no children and finishing after just one call
			if (this.isRoot()) {
				return;
			}
			// Else we are not at the root, and have to continue the completing process by
			// going to the node's parent and continuing from there
			else {
				parent.makeTreeComplete();
			}
		}
	}

	private void makeStateWithChildrenComplete() {
		// Find the next incomplete child and recursively make the tree complete starting from that child
		for (T2 child : this.getChildren()) {
			if (!child.isComplete()) {
				child.makeTreeComplete();
				return;
			}
		}

		this.isComplete = true;

		collectInformationForExtraLabels();
		this.treeRepresentation.generateNonTerminalStateTreeStrings();

		// System.out.println("\n$$$$ twinTreeSting:" + twinTreeString);

		if (parent != null) {
			parent.makeTreeComplete();
		}

		return;
	}

	public void makeTreeComplete() {
		// System.out.println(" makeTreeComplete() called from" + this);

		if (!this.isComplete) {
			if (this.getChildren().isEmpty()) {
				makeStateWithoutChildrenAddedComplete();
			} else {
				makeStateWithChildrenComplete();
			}
		} else {
			parent.makeTreeComplete();
		}
	}

	public boolean mapsToOnePositionOnEitherSide() {
		int numNotNullSourcChildren = this.chosenInference.getNumNotNullSourceChildren();
		int numNotNullTargetChildren = this.chosenInference.getNumNotNullTargetChildren();
		return ((numNotNullSourcChildren == 1) || (numNotNullTargetChildren == 1));
	}

	public boolean containsCompleteSubPhrase() {
		// Note that any inference might have been used here (If one contains a complete sub-phrase then all must)
		return this.chosenInference.containsCompleteSubPhrase();
	}

	public boolean subTreeContainsNoContiguousTranslationEquivalents() {
		return !(this.containsCompleteSubPhrase());
	}

	public String getConsecutiveIntegerString(int length, int offSet) {
		String result = "";
		for (int i = 0; i < length - 1; i++) {
			result += i + ",";
		}
		result += (length - 1);
		return result;
	}

	private T2 getNextStateToAdapt() {
		T2 result = null;

		for (T2 child : this.getChildren()) {
			if (child.hasMoreInferences) {
				result = child;
				break;
			}
		}
		return result;
	}

	private boolean generateNextInferenceAtParent() {
		// the subtree rooted at this node can be no further changed, we must backtrack to
		// generate the next change at its parent
		// System.out.println("Generate next inference at parent");
		this.isComplete = false;
		this.hasMoreInferences = false;
		if (this.parent != null) {
			return parent.generateNextInference();
		} else {
			return false;
		}
	}

	private boolean generateNextInferenceAtChild(T2 nextChildToAdapt) {
		// recursively generate the next inference in the child
		// System.out.println("Generate next inference at child" + nextChildToAdapt );
		this.isComplete = false;
		return nextChildToAdapt.generateNextInference();
	}

	public boolean generateNextInference() {
		T2 nextChildToAdapt = getNextStateToAdapt();

		// If a next child to adapt exists
		if (nextChildToAdapt != null) {
			return generateNextInferenceAtChild(nextChildToAdapt);
		} else if (this.chosenInferenceIndex < (this.inferences.size() - 1)) {
			// System.out.println("Generate next inference in state itself");
			// generate the next inference in this node
			nextInference();
			return true;
		} else {
			return generateNextInferenceAtParent();
		}
	}

	/**
	 * This methods groups a permutation - a list of HashSet<Integer> elements - into a set of groups, starting with group number 0 such that set elements of
	 * the list that share overlapping elements are put into the same group. The list is processed left to right (the process order influences the way group
	 * numbers are chosen, though the number of groups is independent of the processing order of the elements in the list)
	 * 
	 * @param permutation
	 *            : the permutation to perform the grouping for
	 * @return a list of integers that indicate group assignment
	 */
	public List<Integer> findPermutationGrouping(List<Set<Integer>> permutation) {
		SetMethods<Integer> setMethods = new SetMethods<Integer>();
		ArrayList<Integer> grouping = new ArrayList<Integer>();

		List<HashSet<Integer>> groupTargetPosSets = new ArrayList<HashSet<Integer>>();

		for (int i = 0; i < permutation.size(); i++) {
			// Make a copy of the i'th element, to avoid changing the original as a side-effect
			HashSet<Integer> setI = new HashSet<Integer>(permutation.get(i));

			boolean foundOverlappingSet = false;

			// Find the set (if present) from groupTargetPosSets that overlaps with setI
			for (int j = 0; j < groupTargetPosSets.size(); j++) {
				HashSet<Integer> setJ = groupTargetPosSets.get(j);

				if (setMethods.hasNonEmptyIntersection(setI, setJ)) {
					setJ.addAll(setI);
					foundOverlappingSet = true;
					grouping.add(j);
				}
			}

			// If no overlapping set has been found
			if (!foundOverlappingSet) {
				// Add setI as a new set to groupTargetPosSets
				groupTargetPosSets.add(setI);
				// The position of setI is the last position in the new groupTargetPosSets list
				grouping.add(groupTargetPosSets.size() - 1);
			}
		}

		return grouping;
	}

	public void nextInference() {
		System.out.println("{{EbitgTreeState.nextInference called at: " + this + "}}");

		this.chosenInferenceIndex++;
		this.chosenInference = getChosenLabelEnrichedInference();
		// Reset the list of children to a new empty ArrayList
		this.setChildren(new ArrayList<T2>());
		// Add the children for the chosen inference
		this.addChildrenForChosenInference(); // add a new set of children for the chosen inference
		this.isComplete = false; // isComplete must be set back to false
	}

	/**
	 * Gets the chosen raw inference
	 * 
	 * @return
	 */
	private EbitgInference getChosenRawInference() {
		return this.inferences.get(chosenInferenceIndex);
	}

	private T getChosenLabelEnrichedInference() {
		return treeStateChartBacking.createLabelEnrichedInferenceFromLexicalizedInference(getChosenRawInference());
	}

	public boolean isComplete() {
		return isComplete;
	}

	public boolean isRoot() {
		return (this.parent == null);
	}

	protected String getGeneratedTargetWord(int absoluteTargetPosition) {
		return this.treeStateChartBacking.getGeneratedTargetWord(absoluteTargetPosition);
	}

	protected boolean chosenInferenceContainsAbsoluteTargetPosition(int absoluteTargetPosition) {
		return this.chosenInference.containsAbsoluteTargetPosition(absoluteTargetPosition);
	}

	public String toString() {
		String result = "";
		result += "<EbitgTreeState" + this.node + ">";
		return result;
	}

	public void setChildren(List<T2> children) {
		this.children = children;
	}

	public List<T2> getChildren() {
		return children;
	}

	public int getNumberAlignedTargetPositions() {
		return chosenInference.getBasicBoundTargetPositions().size();
	}

	public InferenceType getInferenceType() {
		return this.chosenInference.getInferenceType();
	}

	public boolean consistsOfOneAlignmentChunk() {
		return chosenInference.allContainedAlignedPositionsAreInSameAlignmentChunk();
	}

	public InferenceComplexityType getInferenceComplexityType() {
		if (getInferenceType().equals(InferenceType.HAT)) {
			return InferenceComplexityType.HAT;
		} else if (getInferenceType().equals(InferenceType.PET)) {
			return InferenceComplexityType.PET;
		} else if (getInferenceType().equals(InferenceType.BITT)) {
			return InferenceComplexityType.BITT;
		} else {
			// An atomic node that hard-binds more than 1 target position
			// is still considered a HAT
			if (this.chosenInference.getBasicBoundTargetPositions().size() > 1) {
				return InferenceComplexityType.HAT;
			} else {
				return InferenceComplexityType.BITT;
			}
		}
	}

	public int computeMinNullIgnoringBranchingFactor() {
		return Math.min(chosenInference.getNumNotNullSourceChildren(), chosenInference.getNumNotNullTargetChildren());
	}

	public boolean chosenInferenceIsComplete() {
		return this.chosenInference.isComplete();
	}

	public String getTreeStringChosenInference() {
		return this.chosenInference.toTreeString();
	}

	public EbitgTransductionOperation createEbitgTransductionOperation() {
		return new EbitgTransductionOperation(chosenInference);
	}

	public List<String> getChosenInfernceSourceWords() {
		return this.chosenInference.getSourceWords();
	}

	public List<String> getGeneratedTargetWordsArray() {
		return this.chosenInference.getGeneratedTargetWords();
	}

	public InferenceLengthProperties getInferenceLengthProperties() {
		return InferenceLengthProperties.createInferenceLengthProperties(chosenInference);
	}

	public int getNumNotNullSourceChildren() {
		return this.chosenInference.getNumNotNullSourceChildren();
	}

	public int getNumNotNullTargetChildren() {
		return this.chosenInference.getNumNotNullTargetChildren();
	}

	public EbitgInferencePermutationProperties getPermutationProperties() {
		return this.chosenInference.permutationProperties;
	}

	public boolean isSourceNullBinding() {
		return chosenInference.isSourceNullBinding();// getInferenceType();
	}

	public boolean hasChosenInferenceWithOneNodeCharEntry() {
		return chosenInference.hasOneNodeCharEntry();

	}

	public boolean isMinimalPhrasePairState() {
		return this.chosenInference.isMinimalPhrasePairInference();
	}

	public String getNodeLabel() {
		return this.nodeLabel;
	}

	public T getChosenInfernce() {
		return this.chosenInference;
	}

	public List<T> getChildInferences() {
		List<T> result = new ArrayList<T>();

		for (T2 childState : this.getChildren()) {
			result.add(childState.chosenInference);
		}
		return result;
	}

	public T2 getChildTreeState(int childIndex) {
		return this.children.get(childIndex);
	}

	public Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos) {
		return this.treeStateChartBacking.getOriginalPlusAssociatedExtraBoundTargetPositions(compactedTargetPos);
	}

	public EbitgChart getChart() {
		return this.treeStateChartBacking.getChart();
	}
}
