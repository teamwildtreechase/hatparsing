/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_lexicalization.Lexicalizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import alignmentStatistics.AlignmentChunk;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;
import extended_bitg.EbitgChartEntry.EntryType;
import static extended_bitg.EbitgInference.createEbitgInference;



public abstract class EbitgChartInitializer 
{
	EbitgChart theChart;
	Lexicalizer lexicalizer;
	Set<Integer> alreadyUsedTargetPositions;
	
	// Flag that determines whether redundant derivations resulting from a target position
	// being shared by multiple source positions should be generated. Such redundant derivations
	// are only different in which of the source permutations in a chunk gets to bind the specific
	// target words, but since the whole construct is only translation equivalent as a whole
	// that in fact does not matter, and thus we would normally only get one of the set 
	// of redundant derivations
	public final static boolean DoNotProduceRedundantDerivations = true;
	
	public EbitgChartInitializer(EbitgChart theChart, Lexicalizer lexicalizer)
	{
		this.theChart = theChart;
		this.lexicalizer = lexicalizer;
		this.alreadyUsedTargetPositions = new HashSet<Integer>();
	}

	public static EbitgChartInitializer createChartInitializer(EbitgChart theChart, Lexicalizer lexicalizer, boolean useGreedyNullBinding)
	{
		assert(lexicalizer.getChartInternalAlignmentChunking() instanceof SentencePairAlignmentChunkingZerosGrouped);
		
		EbitgChartInitializer  result;
		if(useGreedyNullBinding)
		{
			result = new EbitgChartInitializerGreedyNullBindings(theChart,lexicalizer);	
		}
		else
		{
			result = new EbitgChartInitializerAllNullBindings(theChart,lexicalizer);
		}
		
		return result;
	}
	

	
	/**
	 *  Initialize the chart: set it up by creating the chart entries, then 
	 *  add length one inferences and null-binding inferences
	 */
	public void initializeChart()
	{
		//System.out.println(">>> Starting Chart initialization...");
		
		for(int i = 0; i < theChart.getSourceLength(); i++)
		{
			addSpanOneInferences(i);
		}
		
		 addNullBindingInferences();
		 
		 //System.out.println(">>> Chart initialization completed");
	}
	
	
		
	public void addSpanOneInferences(int i)
	{
		//System.out.println(">>Entered addSpanOneInferences");
		
		// First get the entry
		EbitgChartEntry entry = theChart.getChartEntry(i, i);
		 
		AlignmentChunk chunk = lexicalizer.getChartInternalAlignmentChunking().getAlignmentChunkForSourcePosition(i);
		HashSet<Integer> targetMappings = new HashSet<Integer> (chunk.getSourcePosMappingPositions(i));
		 
		HashSet<HashSet<Integer>> possibleExtraBoundTargetPositionSets = findPossibleExtendedAlignmentSets(i,DoNotProduceRedundantDerivations);
		 
		if(targetMappings.size() > 0)
		{	
			for(HashSet<Integer> extraBoundTargetPositions : possibleExtraBoundTargetPositionSets)
			{ 	
				assert(targetMappings.size() > 0);
				// create new inference for the extended alignment
				EbitgInference inference = createEbitgInference(targetMappings, extraBoundTargetPositions, entry);
				
				if(inferenceIsAdmissableForChartEntry(inference, entry))
				{	 
					 // add the inference
						entry.addInference(inference, theChart);
				}	
				
				assert(Collections.max(inference.getBasicBoundTargetPositions() ) <= Collections.max(lexicalizer.getChartInternalAlignmentChunking().getNonZeroTargetPositions())); 

			}
		}	
			 // Recompute the completeness of the entry based on the newly added inferences
		 entry.computeCompleteness();
		 entry.setEntryType(EntryType.Atomic);
	}
	
	private boolean inferenceIsAdmissableForChartEntry(EbitgInference inference, EbitgChartEntry entry)
	{
		 // To be admissible the inference must either cover the entire range of target positions
		 // or it must not be the entry that covers the entire source span
		 if(theChart.allTargetPositionsCovered(inference) || (!theChart.isWholeSourceRangeCoveringEntry(entry)))
		 {
			 // We do not add inferences for source positions that bind nothing
			 if(inference.getBoundTargetPositions().size() > 0)
			 {	 
				 return true;
			 } 	 
		 }
		 return false;
	}
	
	public HashSet<HashSet<Integer>>  findPossibleExtendedAlignmentSets(int sourcePos, boolean DoNotProduceRedundantDerivations)
	{
	
		//System.out.println("\n\n>>>>findPossibleExtraAlignmentSets...\n\n");
		
		AlignmentChunk chunk = lexicalizer.getChartInternalAlignmentChunking().getAlignmentChunkForSourcePosition(sourcePos);
		
		HashSet<Integer> availableTargetMappings = new HashSet<Integer>(chunk.getSourcePosMappingPositions(sourcePos));
		
		if(DoNotProduceRedundantDerivations)
		{
			availableTargetMappings.removeAll(this.alreadyUsedTargetPositions);
		}
		
		// Create empty list of set combinations
		HashSet<HashSet<Integer>> setCombinations = new HashSet<HashSet<Integer>>();
		
		// add one element containing just the original target mappings
		//setCombinations.add(new HashSet<Integer>(targetMappings));
		
		// add one element containing nothing
		setCombinations.add(new HashSet<Integer>());
		
		
		//System.out.println("setCombinations.size():" + setCombinations.size());
		//System.out.println("targetMappings: " + targetMappings);
		
		for(Integer targetPos : availableTargetMappings)
		{
			HashSet<HashSet<Integer>> leftFreePositionSets = findLeftFreePositionsSets(targetPos);
			HashSet<HashSet<Integer>> rightFreePositionSets = findRightFreePositionsSets(targetPos);
		
			 // combine the current combinations with the new left free position sets for this targetPos
			 setCombinations = findAllSetCombinations(setCombinations,leftFreePositionSets);
			 //combine the current combinations with the new right free position sets for this targetPos
			 setCombinations = findAllSetCombinations(setCombinations,rightFreePositionSets);
		}
		
		// update the already used target positions
		
		if(DoNotProduceRedundantDerivations)
		{	
			this.alreadyUsedTargetPositions.addAll(availableTargetMappings);
		}	
		
		return setCombinations;
		
	}
	
	public static HashSet<HashSet<Integer>> findAllSetCombinations(HashSet<HashSet<Integer>> sets1,HashSet<HashSet<Integer>> sets2 )
	{
		HashSet<HashSet<Integer>> result = new HashSet<HashSet<Integer>>();
		
		for(HashSet<Integer> set1 : sets1)
		{
			for(HashSet<Integer> set2 : sets2)
			{
				// first copy set 1
				HashSet<Integer> union = new HashSet<Integer>(set1);
				// next add all elements from the second set to form the union
				union.addAll(set2);
				
				//System.out.println("Union: "+ union);
				result.add(union);
			}	
		}		
		return result;
	}
	
	protected abstract  HashSet<HashSet<Integer>> findLeftFreePositionsSets(int targetPosition);
	protected abstract HashSet<HashSet<Integer>> findRightFreePositionsSets(int targetPosition);
	
	public List<Integer> findDirectLeftFreeTargetPositionsRightToLeft(int targetPosition)
	{
		ArrayList<Integer> leftOfTargetFreePositions = new ArrayList<Integer>();
		
		int leftOfTargetPosition = targetPosition - 1;
		
		if(leftOfTargetPosition >= 0)
		{
			AlignmentChunk chunk = lexicalizer.getChartInternalAlignmentChunking().getAlignmentChunkForTargetPosition(leftOfTargetPosition);
			
			if(chunk.isUnalignedChunk())
			{
				return chunk.getTargetPositionsAsSortedListDescending();
			}
		}
		return leftOfTargetFreePositions;
	}

	public List<Integer> findDirectRightFreeTargetPositionsLeftToRight(int targetPosition)
	{
		ArrayList<Integer> rightOfTargetFreePositions = new ArrayList<Integer>();
		
		int rightOfTargetPosition = targetPosition + 1;
		
		if(rightOfTargetPosition < theChart.getTargetLength())
		{
			AlignmentChunk chunk = lexicalizer.getChartInternalAlignmentChunking().getAlignmentChunkForTargetPosition(rightOfTargetPosition);
			
			if(chunk.isUnalignedChunk())
			{
				return chunk.getTargetPositionsAsSortedListAscending();
			}
		}
		return rightOfTargetFreePositions;
	}
	
	

	/** Add all null binding inferences
	 * These inferences consist of one aligned source entry 
	 * plus (possibly multiple) null-aligned source entries on its	
	 * left and/or right side. The length is minimally 2, meaning there 
	 * must be at least one empty source entry bound 
	 */
	public void addNullBindingInferences()
	{
		// loop over the span length from 2 to the length of the source 
		for(int spanLength = 2; spanLength <= theChart.getSourceLength(); spanLength++)
		{
			//System.out.println("Find inferences for spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (theChart.getSourceLength() - spanLength + 1); beginIndex++)
			{
				int i = beginIndex;
				int j = beginIndex + spanLength -1; 
				addNulBindingInferencesForChartEntry(i, j);
			}
		}
	}
	
	// What constitues an acceptable type of source null binding is specified by the implementing sub-classes
	protected abstract boolean isAcceptableTypeOfSourceNullBinding(int i, int j);
	
	private boolean hasExactlyOneNullBindingElement(int alignedPos)
	{
		return alignedPos >= 0;
	}
	
	private void addNulBindingInferencesForChartEntry(int i, int j)
	{
		int alignedPos = posAlignedWordNullBindingEntry(i,j);
		
		// if this entry is a valid null-binding entry
		if(hasExactlyOneNullBindingElement(alignedPos) && isAcceptableTypeOfSourceNullBinding(i,j))
		{
			EbitgChartEntry entry = theChart.getChartEntry(i,j);
			EbitgChartEntry alignedEntry = theChart.getChartEntry(alignedPos,alignedPos);
			
			addInferencesForAllNullBindingsOfAlignedEntry(entry, alignedEntry);
			
			// The null binding entry copies all its values from the aligned Entry
			// it encapsulates
			entry.copyValuesFromEntry(alignedEntry);
			entry.setEntryType(EntryType.SourceNullsBinding);
		}
	}
	
	private void addInferencesForAllNullBindingsOfAlignedEntry(	EbitgChartEntry entry ,	EbitgChartEntry alignedEntry )
	{

		// Add a inference for every inference (corresponding to a particular 
		// target binding of unaligned target positions) of the aligned entry
		for(EbitgNode node : alignedEntry.getNodes())		
		{
			
			//System.out.println("node: " + node + "entry: " + entry);
			
			// A null-binding inference is generated. The third argument is used to extract the 
			// bound target words from 
			EbitgInference inference = createEbitgInference(entry, alignedEntry,node);
									
			 // To be admissible the inference must either cover the entire range of target positions
			 // or it must not be the entry that covers the entire source span
			 if(theChart.allTargetPositionsCovered(inference) || (!theChart.isWholeSourceRangeCoveringEntry(entry)))
			 {
				 // add the inference
				 entry.addInference(inference, theChart);
			 }	
		}
		
	}
	
	
	/** This function does two things: it checks whether the entry 
	 * spanning range i..j in the chart is a nullBinding entry, 
	 * which is an entry that has exactly one aligned-entry, and 
	 * the rest of the entries un-aligned. Furthermore the span of
	 * such an entry must be at least 2
	 * 
	 * @param i The left most position of the span (inclusive)
	 * @param j The right most position of the span (inclusive)
	 * @return The position of the aligned entry, or -1 if the  conditions for being a null-binding entry are not met
	 */
	public int posAlignedWordNullBindingEntry(int i, int j)
	{
		
		// The length of then entry mustchartInitializer. be at least 2, otherwise
		// it can never be a null binding entry, for it is then an atomic entry
		if((j - i) < 1)
		{
			return -1;
		}

		int  alignedPos  = -1;
		
		for(int pos = i; pos <= j; pos++)
		{
			EbitgChartEntry entry = theChart.getChartEntry(pos,pos);
			
			if(entry.isAligned())
			{
				// we already have an aligned position and found another one
				// that is not acceptable for the entry to be a nullbinding entry
				if(alignedPos >= 0)
				{	
					return -1;
				}
				else
				{	
					alignedPos = pos;
				}	
			}
		}
		// We found exactly one aligned entry at alignedPos and return this
		return alignedPos;
	}
	
}
