/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.HashSet;
import java.util.Set;

import util.Utility;

/** Class that stores the node of the ExtendedBITG tree, and is used to group 
 * Inferences in the chart that behave the same way and should be combined on this 
 * group level
 * 
 * @author gemaille
 *
 */
public class EbitgNode 
{
	private Set<Integer> basicBoundTargetPositions;
	private Set<Integer> extraBoundTargetPositions;
	private int minTargetPos, maxTargetPos;
	private boolean isPartOfAlignmentDerivation;

	public EbitgNode(Set<Integer> basicBoundTargetPositions,Set<Integer> extraBoundTargetPositions,  int minTargetPos, int maxTargetPos)
	{
		this.basicBoundTargetPositions = new HashSet<Integer>(basicBoundTargetPositions);
		this.extraBoundTargetPositions = new HashSet<Integer>(extraBoundTargetPositions);
		this.minTargetPos = minTargetPos;
		this.maxTargetPos = maxTargetPos;
		this.isPartOfAlignmentDerivation = false;
	}
	
	public EbitgNode(EbitgInference d)
	{
		this(d.getBasicBoundTargetPositions(),d.getExtraBoundTargetPositions(), d.getMinTargetPos(), d.getMaxTargetPos());
	}
	

	/**
	 * Copy constructor
	 * @param node
	 */
	public EbitgNode(EbitgNode node)
	{
		this.basicBoundTargetPositions = new HashSet<Integer>(node.getBasicBoundTargetPositions());
		this.extraBoundTargetPositions = new HashSet<Integer>(node.getExtraBoundTargetPositions());
		this.minTargetPos = node.minTargetPos;
		this.maxTargetPos = node.maxTargetPos;
		this.isPartOfAlignmentDerivation = node.isPartOfAlignmentDerivation;
	}
	
	public void setMinTargetPos(int min)
	{
		this.minTargetPos = min;
	}
	
	public int getMinTargetPos()
	{
		return this.minTargetPos;
	}
	
	public void setMaxTargetPos(int max)
	{
		this.maxTargetPos = max;
	}
	
	public int getMaxTargetPos()
	{
		return this.maxTargetPos;
	}
	
	public Set<Integer> getBasicBoundTargetPositions()
	{
		return basicBoundTargetPositions;
	}
	
	public Set<Integer> getExtraBoundTargetPositions()
	{
		return extraBoundTargetPositions;
	}
	
	
	public boolean isPartOfAlignmentDerivation()
	{
		return this.isPartOfAlignmentDerivation;
	}
	
	public void setIsPartOfAlignmentDerivation(boolean value)
	{
		this.isPartOfAlignmentDerivation = value;
	}
	
	
	public  HashSet<Integer> getBoundTargetPositions()
	{
		HashSet<Integer> boundTargetPositions = new HashSet<Integer>();
		boundTargetPositions.addAll(basicBoundTargetPositions);
		boundTargetPositions.addAll(extraBoundTargetPositions);
		return boundTargetPositions;
	}
	
	/**
	 *  Method that specifies when two nodes are the same . This is now the case 
	 *  when they have the same sets of bound target positions. This can later be 
	 *  extended with labels or other information
	 * @param nodeObject
	 * @return Whether the node is the same as the node provided in the argument
	 */
	public boolean equals(Object nodeObject)
	{
		//System.out.println("Equals method called...");
		
		if(nodeObject instanceof EbitgNode)
		{	
			EbitgNode node = (EbitgNode) nodeObject;
			
			if((node.basicBoundTargetPositions.equals(this.basicBoundTargetPositions)) && (node.extraBoundTargetPositions.equals(this.extraBoundTargetPositions)))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public int hashCode()
	{
		// See http://macchiato.com/columns/Durable6.html for implementation guidelines
		
		// A big prime number used for hashing	
		int  prime = 1000003; 
		
		int result = 0;
		
		// We start with the hascode for the first integer Hashset
		result = basicBoundTargetPositions.hashCode();
		// we multiply the result with a big prime, and add the hascode for the second set to get our final result
		result = (result * prime) +  extraBoundTargetPositions.hashCode();
		
		return result;		
	}
	
	public String IntegerSetString(Set<Integer> set)
	{
		String result = "[" + Utility.objectSetStringWithoutBrackets(set) + "]";		
		return result;		
	}
	
	public String toString()
	{
		String result = "";
		result += "<Node:" + IntegerSetString(this.getBasicBoundTargetPositions()) +  "+"
		 +  IntegerSetString(this.getExtraBoundTargetPositions()) + ">";
		
		
		return result;
	}

}
