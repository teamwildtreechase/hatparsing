/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

public class EbitgChartBuilderProperties 
{

	public static final String UseCachingProperty = "useCaching";
	public static final boolean USE_GREEDY_NULL_BINDING = true;
	public static final boolean USE_CACHING = true;
	public static final int MaxAllowedInferencesPerNode = 100000;
	
}
