/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.LinkedList;
import util.Pair;

/**
 * This class implements a PartitionList:  a list of ranges partitioning a chart entry.
 * The objects in this class can be grown until the full chart entry has been covered. 
 * This is used to compute an exhasustive list of possible partitions of a minimal length
 * @author gemaille
 *
 */
public class PartitionsList 
{
	private LinkedList<Pair<Integer>> partitions;
	private PartitionEntry headEntry;

	public PartitionsList(PartitionEntry headEntry)
	{
		this.setHeadEntry(headEntry);
		this.setPartitions(new LinkedList<Pair<Integer>>());
	}
	
	/**
	 * Copy constructor
	 * @param toCopy
	 */
	public  PartitionsList( PartitionsList toCopy)
	{
		this.setHeadEntry(headEntry);
		this.setPartitions(new LinkedList<Pair<Integer>>(toCopy.getPartitions()));
	}

	public void setPartitions(LinkedList<Pair<Integer>> partitions) {
		this.partitions = partitions;
	}

	public LinkedList<Pair<Integer>> getPartitions() 
	{
		return partitions;
	}

	public void setHeadEntry(PartitionEntry headEntry) {
		this.headEntry = headEntry;
	}

	public PartitionEntry getHeadEntry() {
		return headEntry;
	}

	public String toString()
	{
		String result = "headEntry: " + headEntry;
		result += "\t partitions: ";
		
		result += partitions.toString();
		
		return result;
		
	}
}

