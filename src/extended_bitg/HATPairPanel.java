/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import alignment.AlignmentPair;
import tsg.TSNodeLabel;
import util.Pair;
import viewer.AlignmentColorKeeper;
import viewer.BasicDrawing;
import viewer.ExtraTreeNodePropertiesVisualizer;
import viewer.ExtraTreeNodePropertiesVisualizerFactory;
import viewer.PanelSizeProperties;
import viewer.TreePairPanel;
import viewer.TreeVisualizer;
import viewer_io.EbitgCheckBoxTable;

public class HATPairPanel extends viewer.TreePairPanel {
	private static final long serialVersionUID = 1L;

	private List<AlignmentPair> alignments;

	AlignmentColorKeeper alignmentColorKeeper;

	private HATPairPanel(ArrayList<AlignmentPair> alignments, EbitgCheckBoxTable checkBoxTable, AlignmentColorKeeper alignmentColorKeeper, PanelSizeProperties panelSizeProperties) {
		super(checkBoxTable, panelSizeProperties);
		this.alignments = alignments;
		this.alignmentColorKeeper = alignmentColorKeeper;
		this.getPanelSizeProperties().setWordSpace(20);
	}

	public static HATPairPanel createHATPairPanel(ArrayList<AlignmentPair> alignments, EbitgCheckBoxTable checkBoxTable,ExtraTreeNodePropertiesVisualizerFactory extraTreeNodePropertiesVisualizerFactory) {

		AlignmentColorKeeper alignmentColorKeeper = new AlignmentColorKeeper(checkBoxTable);
		PanelSizeProperties panelSizeProperties = new PanelSizeProperties();
		HATPairPanel hatPairPanel = new HATPairPanel(alignments, checkBoxTable, alignmentColorKeeper, panelSizeProperties);

		ExtraTreeNodePropertiesVisualizer extraTreeNodePropertiesVisualizer = extraTreeNodePropertiesVisualizerFactory.createExtraTreeNodePropertiesVisualizer(hatPairPanel);
		TreeVisualizer<TreePairPanel> tree1Visualizer = TreeVisualizer.createSourceTreeVisualizer((TreePairPanel) hatPairPanel, checkBoxTable.getShowHATReorderingLabelsCheckBox(),checkBoxTable.getShowParentRelativeReorderingLabelsCheckBox(),checkBoxTable.getShowPreterminalLabelsCheckBox(), extraTreeNodePropertiesVisualizer);
		TreeVisualizer<TreePairPanel> tree2Visualizer = TreeVisualizer.createTargetTreeVisualizer((TreePairPanel) hatPairPanel, checkBoxTable.getShowHATReorderingLabelsCheckBox(),checkBoxTable.getShowParentRelativeReorderingLabelsCheckBox(),checkBoxTable.getShowPreterminalLabelsCheckBox(), extraTreeNodePropertiesVisualizer);
		hatPairPanel.setTree1Visualizer(tree1Visualizer);
		hatPairPanel.setTree2Visualizer(tree2Visualizer);

		return hatPairPanel;
	}

	public static HATPairPanel createHATPairPanel(ArrayList<Pair<TSNodeLabel>> treePairBank, ArrayList<AlignmentPair> alignments, EbitgCheckBoxTable checkBoxTable,ExtraTreeNodePropertiesVisualizerFactory extraTreeNodePropertiesVisualizerFactory) {
		HATPairPanel hatPairPanel = createHATPairPanel(alignments, checkBoxTable,extraTreeNodePropertiesVisualizerFactory);
		hatPairPanel.loadTreebank(treePairBank);
		return hatPairPanel;
	}

	public void setAlignments(List<AlignmentPair> alignments2) {
		this.alignments = alignments2;
	}

	public void render(Graphics2D g2) {
		// Draw the two trees
		super.render(g2);

		// Draw alignments between the leaf nodes of the trees
		if (alignments != null) {
			drawAlignmentsBetweenTrees(g2);
		}

	}


	public void drawAlignmentsBetweenTrees(Graphics2D g2) {
		int sourxeMaxY = this.tree1Visualizer.getMaxLeafYPosition(false);
		int targetMaxY = this.tree2Visualizer.getMaxLeafYPosition(true);

		for (TSNodeLabel sourceNode : this.tree1Visualizer.getTheTreeStruct().getLexicals()) {
			int[] sourceNodeDrawPosition = this.tree1Visualizer.getNodeBottomPosition(sourceNode, false);

			int x1 = sourceNodeDrawPosition[0];
			int y1 = sourceNodeDrawPosition[1];
			int x2 = sourceNodeDrawPosition[0];
			int y2 = sourxeMaxY;

			BasicDrawing.drawAlignmentLine(x1, y1, x2, y2, g2, true);
		}
		
		
		for (TSNodeLabel targetNode : this.tree2Visualizer.getTheTreeStruct().getLexicals()) {
			int[] targetNodeDrawPosition = this.tree2Visualizer.getNodeBottomPosition(targetNode, true);

			int x1 = targetNodeDrawPosition[0];
			int y1 = targetNodeDrawPosition[1] + getBottomTreeYOffset();
			int x2 = targetNodeDrawPosition[0];
			int y2 = targetMaxY + getBottomTreeYOffset();

			BasicDrawing.drawAlignmentLine(x1, y1, x2, y2, g2, true);
		}

		this.alignmentColorKeeper.setAlignmentColorIndex(0);

		for (AlignmentPair alignment : alignments) {
			// Get the source and target position
			int sourcePos = alignment.getSourcePos();
			int targetPos = alignment.getTargetPos();

			// Get the associated source and target lexical nodes
			TSNodeLabel sourceNode = this.tree1Visualizer.getTheTreeStruct().getLexicalItem(sourcePos);
			TSNodeLabel targetNode = this.tree2Visualizer.getTheTreeStruct().getLexicalItem(targetPos);

			int[] sourceNodeDrawPosition = this.tree1Visualizer.getNodeBottomPosition(sourceNode, false);
			int[] targetNodeDrawPosition = this.tree2Visualizer.getNodeBottomPosition(targetNode, true);

			int x1 = sourceNodeDrawPosition[0];
			int y1 = sourxeMaxY;
			int x2 = targetNodeDrawPosition[0];
			int y2 = getBottomTreeYOffset() + targetMaxY;

			// extract the color for this alignment
			g2.setColor(this.alignmentColorKeeper.getCurrentAlignmentColor());
			// increment the currentAlignmentColorIndex. Once the maximum index is reached, start over from 0
			this.alignmentColorKeeper.incrementAlignmentColorIndex();

			BasicDrawing.drawAlignmentLine(x1, y1, x2, y2, g2, false);
		}

	}

}