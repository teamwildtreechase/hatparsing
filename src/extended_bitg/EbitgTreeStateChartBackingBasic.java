/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import hat_lexicalization.Lexicalizer;

public class EbitgTreeStateChartBackingBasic extends EbitgTreeStateChartBacking<EbitgLexicalizedInference,EbitgTreeStateBasic>
{

	protected EbitgTreeStateChartBackingBasic(EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder, Lexicalizer lexicalizer) {
		super(chartBuilder, lexicalizer);
	}

	public static EbitgTreeStateChartBackingBasic   createEbitgTreeStateChartBackingBasic(EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder)
	{
		return new EbitgTreeStateChartBackingBasic(chartBuilder, chartBuilder.getLexicalizer());
	}
}
