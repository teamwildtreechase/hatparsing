/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import bitg.IntegerSetMethods;

public class EbitgInferencePermutationProperties 
{
	// How the child nodes have to be permuted to get the target order
	private final List<Set<Integer>> sourcePermutation;	
	private Map<Integer,Set<Integer>> targetPositionsForPermutationIncices;

	private EbitgInferencePermutationProperties(List<Set<Integer>> sourcePermutation, Map<Integer,Set<Integer>> targetPositionsForPermutationIncices)
	{
		this.sourcePermutation = sourcePermutation;
		this.targetPositionsForPermutationIncices = targetPositionsForPermutationIncices;
	}

	public static EbitgInferencePermutationProperties createAtomicPermutationProperties(Set<Integer> positions)
	{
		return new EbitgInferencePermutationProperties(computeAtomicPermutationOrder(positions),null);
	}
	
	public static EbitgInferencePermutationProperties createPermutationProperties(EbitgInference inference)
	{
		Map<Set<Integer>,Integer> setsPositionMappingTable  = computeBoundTargetPositionsToRelativeTargetPositionMappingTable(inference);
		List<Set<Integer>> permutationOrder = EbitgInferencePermutationProperties.computeSourcePermutationOrder(inference, setsPositionMappingTable);
		Map<Integer,Set<Integer>> targetPositionsForPermutationIncices = computeTargetPositionsForPermutationIndices(inference, setsPositionMappingTable);
		return new EbitgInferencePermutationProperties(permutationOrder,targetPositionsForPermutationIncices);
	}
	
	
	/**
	 * Getter for souce permutation
	 * @return The source permutation
	 */
	public  List<Set<Integer>> getSourcePermutation()
	{
		return Collections.unmodifiableList(sourcePermutation);
	}
	
	private static List<Set<Integer>>  computeAtomicPermutationOrder(final Set<Integer> positions)
	{
		List<Set<Integer>> result =  new ArrayList<Set<Integer>>();
		Set<Integer> boundTargetPositionsCopyNormalized =  new HashSet<Integer>();
		
		if(!positions.isEmpty())
		{	
			int minimum = Collections.min(positions);	
			for(int pos : positions)
			{
				boundTargetPositionsCopyNormalized.add(pos - minimum);
			}
		}	
		result.add(boundTargetPositionsCopyNormalized);
		return result;
	}

	
	private static Set<Integer> getExtraGroupableTargetPositionsAndUpdateRemainingExtraGroupablePositions(EbitgInference inference,Set<Integer> remainingExtraBoundTargetPositions, Set<Integer> targetPositions)
	{
		int absoluteTargetPos = (Integer) targetPositions.toArray()[0];
		// Find the set of extra bound target positions that is groupable with this target position
		Set<Integer> extraGroupableTargetPositions = extraBoundTargetPositionsGroupableWithTargetPosition(inference, absoluteTargetPos);
		// Keep only positions not already used
		extraGroupableTargetPositions.retainAll(remainingExtraBoundTargetPositions);
		
//			//update the set of remaining positions by removing those that have just been used/assigned				
		remainingExtraBoundTargetPositions.removeAll(extraGroupableTargetPositions);
		
		// Add the absolute target position itself 
		extraGroupableTargetPositions.add(absoluteTargetPos);
		return extraGroupableTargetPositions;
	}
	
	
	private static Map<Integer,Set<Integer>>  computeTargetPositionsForPermutationIndices(EbitgInference inference, Map<Set<Integer>, Integer> setsPositionMappingTable)
	{
		// We keep the number of target positions associated with a source permutation index
		// for convencience during visualization
		Map<Integer,Set<Integer>> result = new HashMap<Integer,Set<Integer>>();
		
		// Keep a set of extra bound target positions that are remaining for groupoing with bound target positions
		Set<Integer> remainingExtraBoundTargetPositions = new HashSet<Integer>(inference.getExtraBoundTargetPositions());
		
		for(Set<Integer> targetPositions : setsPositionMappingTable.keySet())
		{
			int relativeTargetPosition = setsPositionMappingTable.get(targetPositions); 		
			
			if(targetPositions.size() == 1)
			{
				// Find the set of extra bound target positions that is groupable with this target position
				Set<Integer> extraGroupableTargetPositions = getExtraGroupableTargetPositionsAndUpdateRemainingExtraGroupablePositions(inference, remainingExtraBoundTargetPositions, targetPositions);
				result.put(relativeTargetPosition, extraGroupableTargetPositions);
			}
			else
			{	
				result.put(relativeTargetPosition, targetPositions);
			}	
			
		}
		return result;
	}
	
	
	private static Set<Integer> findLongestSpanLeftAdjacentPositions(List<Integer> associatedExtraBoundTargetPositionsList,  int boundTargetPosition, int boundTargetPositionIndex)
	{
		Set<Integer> result  = new HashSet<Integer>();
		
		int previousAdjacentPosition = boundTargetPosition;
		for(int index = boundTargetPositionIndex -1; index >= 0; index--)
		{
			int position = associatedExtraBoundTargetPositionsList.get(index);
		
			if(!positionsAreAdjacent(previousAdjacentPosition ,position))
			{
				break;
			}	
			result.add(position);
			previousAdjacentPosition = position;
		}
		return result;
	}
	
	private static Set<Integer> findLongestSpanRightAdjacentPositions(List<Integer> associatedExtraBoundTargetPositionsList,  int boundTargetPosition, int boundTargetPositionIndex)
	{
		Set<Integer> result  = new HashSet<Integer>();
		
		int previousAdjacentPosition = boundTargetPosition;
		for(int index = boundTargetPositionIndex  + 1; index < associatedExtraBoundTargetPositionsList.size(); index++)
		{
			int position = associatedExtraBoundTargetPositionsList.get(index);
		
			if(!positionsAreAdjacent(previousAdjacentPosition,position))
			{
				break;
			}	
			result.add(position);
			previousAdjacentPosition = position;
		}
		return result;
	}
	
	
	private static Set<Integer>  extraBoundTargetPositionsGroupableWithTargetPosition(EbitgInference inference, int boundTargetPosition)
	{
		Set<Integer> result  = new HashSet<Integer>();
		List<Integer> associatedExtraBoundTargetPositionsList =  createSoretedListWithBoundTargetPositionAndAssociatedExtraBoundTargetPositions(inference, boundTargetPosition);
		int boundTargetPositionIndex = Collections.binarySearch(associatedExtraBoundTargetPositionsList,boundTargetPosition);
		
		// Find the longest possible span of adjacent positions to the left
		result.addAll(findLongestSpanLeftAdjacentPositions(associatedExtraBoundTargetPositionsList, boundTargetPosition, boundTargetPositionIndex));	
		//Find the longest possible span of adjacent positions to the right
		result.addAll(findLongestSpanRightAdjacentPositions(associatedExtraBoundTargetPositionsList, boundTargetPosition, boundTargetPositionIndex));
		return result;
	}
	
	private static List<Integer> createSoretedListWithBoundTargetPositionAndAssociatedExtraBoundTargetPositions(EbitgInference inference, int boundTargetPosition)
	{
		 List<Integer> associatedExtraBoundTargetPositionsList = new ArrayList<Integer>(extraBoundTargetPositionAppearingWithBoundTargetPosition(inference, boundTargetPosition));
		 associatedExtraBoundTargetPositionsList.add(boundTargetPosition);
		 Collections.sort(associatedExtraBoundTargetPositionsList);
		 return associatedExtraBoundTargetPositionsList;
	}
	
	private static boolean positionsAreAdjacent(int pos1, int pos2)
	{
		return (Math.abs(pos1 - pos2) == 1);
	}

	/**
	 * Function that returns the number of target positions mapped to by a certain permutation Index,
	 * used primarily for visualization of the trees
	 * @param permutationIndex
	 * @return The number of target positions
	 */
	public int getNumberOfTargetPositionsForPermutationIndex(int permutationIndex)
	{
		if(this.targetPositionsForPermutationIncices.containsKey(permutationIndex))
		{
			return this.targetPositionsForPermutationIncices.get(permutationIndex).size();
		}
		else
		{
			throw new RuntimeException("EbitgInference. getTargetPositionsForPermutationIndex : permutation index not registered in table");
		}
	}
	
	public Set<Integer> getTargetPositionsForPermutationIndex(int permutationIndex)
	{
		if(this.targetPositionsForPermutationIncices.containsKey(permutationIndex))
		{
			return this.targetPositionsForPermutationIncices.get(permutationIndex);
		}
		else
		{
			throw new RuntimeException("EbitgInference. getTargetPositionsForPermutationIndex : permutation index not registered in table");
		}
	}

	
	private void addElementToIntegerToIntegerSetMap(Map<Integer, Set<Integer>> map, int key, int value)
	{
		if(map.containsKey(key))
		{
			Set<Integer> set = map.get(key);
			set.add(value);
		}
		else
		{
			Set<Integer> set = new HashSet<Integer>();
			set.add(value);
			map.put(key, set);
		}
	}

	
	private List<Set<Integer>> computeInvertedPermutationFromInvertedMap(Map<Integer, Set<Integer>>	invertedMap, int maxTargetPos)
	{
		List<Set<Integer>> invertedPermutation = new ArrayList<Set<Integer>>();
		for(int targetPos = 0; targetPos <= maxTargetPos; targetPos++)
		{
			//System.out.println("EbitgInference.computeInvertedPermutation targetPos: " + targetPos);
			assert(invertedMap.get(targetPos) != null);
			invertedPermutation.add(invertedMap.get(targetPos));
		}
		assert(invertedPermutation.size() > 0);
		return invertedPermutation;
	}
	
	
	private <T extends Object & Comparable<? super T>> T  getMaximumSetElementFromSetsList(List<Set<T>> setsList)
	{
		Set<T> allElements = new HashSet<T>();
		for(Set<T> set : setsList)
		{
			allElements.addAll(set);
		}
		return Collections.max(allElements);
	}
	
	
	private Map<Integer, Set<Integer>> computeTargetToSourceMap()
	{
		Map<Integer, Set<Integer>>	invertedMap = new HashMap<Integer, Set<Integer>>();	

		// Compute an inverted lookup table that maps target positions to HashSets of 
		// associated source positions
		for(int sourcePos = 0; sourcePos < this.sourcePermutation.size(); sourcePos++)
		{
			 Set<Integer> sourcePosTargetMapping = this.sourcePermutation.get(sourcePos);
			 
			 //System.out.println("EbitgInference.computeInvertedPermutation - sourcePosTargetMapping" + sourcePosTargetMapping);
			 
			 for(Integer targetPos : sourcePosTargetMapping)
			 {
				addElementToIntegerToIntegerSetMap(invertedMap,targetPos,sourcePos);
			 }
		}
		return invertedMap;
	}
	
	/**
	 * Method that computes an inverted permutation that maps from target to source
	 * @return The inverted permutation
	 */
	public List<Set<Integer>> computeInvertedPermutation()
	{
		Map<Integer, Set<Integer>> targetToSourceMap = computeTargetToSourceMap();
		int maxTargetPos = getMaximumSetElementFromSetsList(this.sourcePermutation);
		return computeInvertedPermutationFromInvertedMap(targetToSourceMap, maxTargetPos);
	}
	
	public List<SortedSet<Integer>> computeSortedInvertedPermutation()
	{
		 List<Set<Integer>> invertedPermutation = computeInvertedPermutation();
		 List<SortedSet<Integer>>  orderedInvertedPermutation = new  ArrayList<SortedSet<Integer>>();
		 
		/*
		 int i = 0;
		 for(Set<Integer> element : invertedPermutation)
		 {
			 //System.out.println("EbitgInference.computeSortedInvertedPermutation:");
			 //System.out.println("Element " + i + "  " + element);
			 i++;
		 }
		 */
		 
		 for(Set<Integer> set : invertedPermutation)
		 {
			 // Add the HashSet<Integer> to a TreeSet<Integer> thereby making the elements sorted
			 orderedInvertedPermutation.add(new TreeSet<Integer>(set));
		 }
		 
		 return orderedInvertedPermutation;
	}

	/**
	 * Method used for testing consistency of formed source permutation.
	 * Every source permutation should have elements forming an consecutive range
	 * @return
	 */
	private static boolean sourcePermutationFormsConsecutiveRange(List<Set<Integer>> sourcePermutation)
	{
		Set<Integer> sourcePermutationElements = new HashSet<Integer>();
	
		for(Set<Integer> targetPositions : sourcePermutation)
		{
			sourcePermutationElements.addAll(targetPositions);
		}
		
		return IntegerSetMethods.formsConsecutiveRange(sourcePermutationElements);
	}
	
	/**
	 * Method used for testing consistency of formed source permutation.
	 * Every source permutation should contain the element zero.
	 * @return boolean indicating whether the property holds
	 */
	private static boolean sourcePermutationContainsZero(List<Set<Integer>> sourcePermutation)
	{
		Set<Integer> sourcePermutationElements = new HashSet<Integer>();
		
		for(Set<Integer> targetPositions : sourcePermutation)
		{
			sourcePermutationElements.addAll(targetPositions);
		}
		
		return sourcePermutationElements.contains(0);	
	}
	
	private static List<Set<Integer>> computeSourcePermutationOrder(EbitgInference inference, Map<Set<Integer>, Integer> setsPositionMappingTable)
	{
		List<Set<Integer>> result = computeSourcePermutation(inference, setsPositionMappingTable);
		assert(sourcePermutationFormsConsecutiveRange(result));
		assert(sourcePermutationContainsZero(result));
		return result;
	}
	
	private static List<Set<Integer>> computeUnqueUnsortedTargetSetsList(EbitgInference inference)
	{
		List<Set<Integer>> childTargetPositions =  inference.getChildrenBasicBoundTargetPositions();
		Set<Set<Integer>> uniqueUnsortedSets =  new HashSet<Set<Integer>>();
		
		assert(childTargetPositions.size() > 0);
		
		// First we generate the HashSet just to filter out multiple occurrences of the 
		// same target position in different children and have one mapping entry that is 
		// shared by them
		for(int i = 0; i <  childTargetPositions.size(); i++)
		{
			// If the child node is complete (that is, its containing chart entry is complete),
			// we may contain its set of target mapping positions as one relative position from 
			// the point of view of the currrent node
			if(inference.getNodeChartEntries().get(i).isComplete())
			{
				 uniqueUnsortedSets.add(childTargetPositions.get(i)); 
			}
			else
			{
			// The child is incomplete and we must treat all mapped target positions separately	
				
				for(int pos : childTargetPositions.get(i))
				{
					HashSet<Integer> posSet = new HashSet<Integer>();
					posSet.add(pos);
					uniqueUnsortedSets.add(posSet); 
				}
			}
		}
		return  new ArrayList<Set<Integer>>(uniqueUnsortedSets);
	}
	
	
	private static Map<Set<Integer>,Integer> computeTargetSetToRelativePositionTableFromUniqueTargetSetsList(List<Set<Integer>> uniqueTargetSetsList)
	{	
		HashMap<Set<Integer>,Integer> targetSetToRelativeTargetPositionMappingTable = new HashMap<Set<Integer>,Integer>();
		IntegerSetComparator comp = new IntegerSetComparator();
		
		Collections.sort( uniqueTargetSetsList,comp);
		
		assert(uniqueTargetSetsList.size() > 0);
		
		for(int i = 0; i <  uniqueTargetSetsList.size(); i++)
		{
			// again put the IntegerSets in the HashMap, but now with there associated
			// relative target position which was found by sorting the array of 
			//LabeledIntegerSets
			 Set<Integer> key = new HashSet<Integer>( uniqueTargetSetsList.get(i));
			 int value = i;
			 targetSetToRelativeTargetPositionMappingTable.put(key,value);
			 assert(targetSetToRelativeTargetPositionMappingTable.values().contains(0));
		}
		
		assert(targetSetToRelativeTargetPositionMappingTable.size() > 0);
		assert(targetSetToRelativeTargetPositionMappingTable.values().contains(0));
		return targetSetToRelativeTargetPositionMappingTable;
	}
	
	private static Map<Set<Integer>,Integer> computeBoundTargetPositionsToRelativeTargetPositionMappingTable(EbitgInference inference)
	{
		// produce a list of unique target sets. Elements of permutation sets that are not complete are all added individually
		 List<Set<Integer>>  uniqueTargetSetsList=  computeUnqueUnsortedTargetSetsList(inference);
		// Take the set of unique target sets and use their absolute order to compute a target set to relative target position table
		return computeTargetSetToRelativePositionTableFromUniqueTargetSetsList(uniqueTargetSetsList);
	}
	
	
	private static List<Set<Integer>> computeSourcePermutation(EbitgInference inference, Map<Set<Integer>, Integer> setsPositionMappingTable)
	{
		List<Set<Integer>> childTargetPositions =  inference.getChildrenBasicBoundTargetPositions();
		List<Set<Integer>>  sourcePermutation = new ArrayList<Set<Integer>>();
		
		for(int i = 0; i <  childTargetPositions.size(); i++)
		{
			
			Set<Integer> childMappingSet;
			
			Set<Integer> targetPositions = childTargetPositions.get(i);
			
			// If the node is complete, we add just one mapping position for it
			if(inference.getNodeChartEntries().get(i).isComplete())
			{
				childMappingSet = computeChildMappingSetForCompleteChartEntry(targetPositions, setsPositionMappingTable);
			}
			else
			{
				childMappingSet = computeChildMappingSetForInCompleteChartEntry(targetPositions, setsPositionMappingTable);
			}
			
			sourcePermutation.add(childMappingSet);
		}
		assert(sourcePermutation.size() ==  childTargetPositions.size());
		return sourcePermutation;
	}
	
	private static Set<Integer> computeChildMappingSetForInCompleteChartEntry(Set<Integer> targetPositions ,Map<Set<Integer>,Integer> setsPositionMappingTable )
	{
		HashSet<Integer> childMappingSet = new HashSet<Integer>();
		
		// The node is incomplete, and we add one mapping position for each 
		// of the elements of its target mapping set
		for(int pos : targetPositions)
		{
			HashSet<Integer> key = new HashSet<Integer> ();
			key.add(pos);
			int mappingPosition = setsPositionMappingTable.get(key);
			childMappingSet.add(mappingPosition);
		}
		
		return childMappingSet;
	}
	
	
	private static Set<Integer> computeChildMappingSetForCompleteChartEntry(Set<Integer> targetPositions ,Map<Set<Integer>,Integer> setsPositionMappingTable )
	{
		HashSet<Integer> childMappingSet = new HashSet<Integer>();
		int mappingPosition = setsPositionMappingTable.get(targetPositions);
		childMappingSet.add(mappingPosition);
		return childMappingSet;
	}
	
	private static Set<Integer> extraBoundTargetPositionAppearingWithBoundTargetPosition(EbitgInference inference, int boundTargetPosition)
	{
		HashSet<Integer> result = new HashSet<Integer> ();
		
		for(EbitgNode childNode : inference.getNodes())
		{
			if(childNode.getBasicBoundTargetPositions().contains(boundTargetPosition))
			{
				result.addAll(childNode.getExtraBoundTargetPositions());
			}
		}
		return result;
	}
	
}
