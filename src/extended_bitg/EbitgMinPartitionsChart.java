/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EbitgMinPartitionsChart 
{
	final List<PartitionEntry> shortestPartitions;
	
	private EbitgMinPartitionsChart(List<PartitionEntry> shortestPartitions)
	{
		this.shortestPartitions = shortestPartitions;
	}
	
	public static EbitgMinPartitionsChart createMinPartitionsChart(EbitgChartEntry entry, EbitgChart theChart)
	{
		return new EbitgMinPartitionsChart(constructViterbiMinPartitionsChart(entry, theChart));
	}
	
	protected List<PartitionEntry> getShortestPartitions()
	{
		return Collections.unmodifiableList(shortestPartitions);
	}
	
	/**
	 * Compute a chart with viterbi backpointers that specify the minimal number of components partitions
	 * ArrayList<PartitionEntry> shortestPartitions = constructViterbiMinPartitionsChart(entry);
     * 
	 * @param entry
	 * @param theChart
	 * @return
	 */
	public static List<PartitionEntry>  constructViterbiMinPartitionsChart( EbitgChartEntry entry, EbitgChart theChart)
	{
		int entryStart = entry.getI();
		
		List<PartitionEntry> shortestPartitions = createInitialShortestPartitionsList(entry);
		
		for(int i = 0; i < entry.sourceSpanLength(); i++)
		{
			// Loop over inferrable entries starting from i
			for(int j = i; j < entry.sourceSpanLength(); j++)
			{
				EbitgChartEntry chartEntry = theChart.getChartEntry(entryStart + i, entryStart + j); 
				if(chartEntry.hasInferences())
				{
					updateShortesPartitionsWithPartitionsContainingChartEntry(chartEntry, shortestPartitions, i, j);
				}
			}	
		}
		
		return shortestPartitions;
	}
	
	public static  List<PartitionEntry> createInitialShortestPartitionsList( EbitgChartEntry entry)
	{
		List<PartitionEntry> shortestPartitions = new ArrayList<PartitionEntry>();
		//PartitionEntry[] shortestPartitions = new PartitionEntry[entrySpanLength + 1];
		for(int i = 0; i < entry.sourceSpanLength() + 1; i++)
		{
			PartitionEntry partitionEntry = new PartitionEntry();
			partitionEntry.setIndex(i + entry.getI());
			
			// Add the new partition entry. Notice that the entry add index n covers 
			// the subrange of the chart entry [0, n-2], for n>0. For n==0 it covers nothing.
			shortestPartitions.add(partitionEntry);
		}
		// The PartitionEntry at index zero covers nothing of the chart entry, and has cost zero
		shortestPartitions.get(0).setMinimalCost(0);
		return shortestPartitions;
	}
	

	public static void updateShortesPartitionsWithPartitionsContainingChartEntry(EbitgChartEntry chartEntry,List<PartitionEntry> shortestPartitions,int i, int j)
	{
	  // One might wonder: why is the index j+1 and not j, when we look up the 
	  // best existing path?
	  // The reason is that a partition that ends with a chart entry [i,j]
	  // corresponds to a  shortestPartitions lattice entry with index (j+1),
	  // since  shortestPartitions has n+1 elements, and the element at position 0 
	  // is the element that spans nothing, while the element at position n, spans 
	  // positions 0 - n-1
		int costBestExistingPath = shortestPartitions.get(j+1).getMinimalCost();
		int costNewPath = shortestPartitions.get(i).getMinimalCost() + 1;
		
		if(costNewPath < costBestExistingPath)
		{
			// A better alternative has been found, replace the viterbi list and minimal cost
			shortestPartitions.get(j+1).setMinimalCost(costNewPath);
			shortestPartitions.get(j+1).replaceViterbiParents(shortestPartitions.get(i));
		}
		else if (costNewPath == costBestExistingPath)
		{
			// an equally good alternative is found: keep it as well
			shortestPartitions.get(j+1).addViterbiParent(shortestPartitions.get(i));
		}
	}
	
	protected PartitionEntry getLastPartitionEntry()
	{
		return this.shortestPartitions.get(this.shortestPartitions.size() - 1);
	}
	
	
}
