/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import bitg.IntegerSetMethods;
import bitg.MultiplePartsInferenceCandidate;
import extended_bitg.EbitgChartEntry.EntryType;
import static extended_bitg.EbitgInference.createEbitgInference;

public class EbitgChartFiller 
{
	// upper bound on the amount of infernces per node that is still accepted
	//public static final int MAX_ALLOWED_INFERENCES_PER_NODE = 100000;
	public final int maxAllowedInferencesPerNode;
	
	public EbitgChart theChart;
	
	public  EbitgChartFiller(int maxAllowedInferencesPerNode, EbitgChart theChart)
	{
		this.maxAllowedInferencesPerNode = maxAllowedInferencesPerNode;
		this.theChart = theChart;
	}
	
	/**
	 * Method determining whether inference should be added for a chart entry. This
	 * is only the case if there are no inferences added yet before and it is possibly possible to find inferences
	 * @param entry
	 * @return
	 */
	public boolean shouldAddInferencesToChartEntry(EbitgChartEntry entry)
	{
		if(entry.hasInferences() || (!constitutesPossiblyDerivableChartEntry(entry)))
		{	
			return false;
		}	
		return true;
	}
	
	/**
	 * Algorithm that adds the inferences for a chart entry. The algorithm
	 * starts by finding a list of minimal-number-of-components partitions, devisions into 
	 * a minimal number of parts that are all inferable and from which the chart entry as.
	 * Using the list of minimum partitions, a new list of possible combined inferences 
	 * is then created, which results from enumerating the different choices into different 
	 * partitions and for each partition the different possible choices of inferences for each 
	 * of the parts that lead to a combined valid inference for the entire chart entry. 
	 * a whole can be inferred. The algorithm returns a boolean that indicates whether a valid
	 * inference could be found and added to the chart entry. This need not always be the case
	 * since depending on the alignment, some source positions may be inferable only 
	 * as a group in one go. Trying to find an inference for a subset of such a group then 
	 * has to fail if the algorithm is consistent. 
	 * @param i
	 * @param j
	 * @return Whether a legal inference has been found
	 */
	public boolean addInferncesToChartEntry(int i, int j) throws Exception
	{
		//System.out.println("Find infernces for chart entry: <" + i + "," + j + ">");
		
		// 1. make sure that the leftmost and rightmost source position in the entry's range
		// are part of the same alignment chunk
		
		EbitgChartEntry entry =  theChart.getChartEntry(i,j);
		
		if(!shouldAddInferencesToChartEntry(entry))
		{
			return false;
		}
		
		// 2.Now partition the span i..j in different Chart entries: those that contain 
		// source positions that are part of the alignment chunk of the left and right most 
		// source entry, and all chart entries that fill up whatever source positions are not 
		// part of the complex alignment chunk starting at the left end ending at the right of this
		// chart entry
		
		boolean foundLegalInference = false;	 
		
		
		// First try to find partitions with the simple O(N) complexity algorithm that tests all N
	    // N-1 possible split points for the entry
		List<List<EbitgChartEntry>> partitionings = findPossibleBinaryDecompositions(entry);
		
		// No binary partitions exist, so use the more expensive Viterbi algorithm to find non-binary partitions
		// Note that this can happen at most for N non-overlapping spans, with a cost of O(N * N^2)
		// so that the total algorithm complexity remains O(n^3). Without this optimization, computing 
		// partitions with the Viterbi algorithm for all spans, the algorithm 
		// complexity becomes O(n^4).
		if(partitionings.isEmpty()){
		  EbitgPartitionFinder partitionFinder = EbitgPartitionFinder.createEbitgPartitionFinder(entry, theChart);
		   partitionings = partitionFinder.generatePartitionings(); 
		}
		
		//System.out.println("minComponentPartitions.size(): " +  minComponentPartitions.size());	
		
		int maxExpectedInferences = computeUpperBoundPossibleInferences(partitionings);
		//System.out.println("max expected inferences: " + maxExpectedInferences);
		
		if(maxExpectedInferences >  maxAllowedInferencesPerNode )
		{
			throw new  Exception("Exceeded the maximum allowd number of inferences per node");
		}
		
		for(List<EbitgChartEntry>  partitioning : partitionings)
		{			
			// add the legal inferences for this partitioning, if any
			boolean foundLegalInferenceForParitioning = addLegalInferencesForPartitioning(partitioning, entry);
			
			if(foundLegalInferenceForParitioning)
			{
				foundLegalInference = true;
			}
		}
		 return  foundLegalInference;
	}
	
	/**
	 * Method that finds a list of possible binary decompositions, if the chart entry is binarizable.
	 * This is useful, as it allows an optimization, in which case the Viterbi algorithm only needs
	 * to be used to find partitions for the <= N (were N is the source input length) spans 
	 * that have no binary decomposition. 
	 * @param entry
	 * @return
	 */
	private List<List<EbitgChartEntry>> findPossibleBinaryDecompositions(EbitgChartEntry entry){
	  List<List<EbitgChartEntry>> result = new ArrayList<List<EbitgChartEntry>>();
	  for(int splitPoint = entry.getI() + 1;  splitPoint<= entry.getJ(); splitPoint++){
	    EbitgChartEntry firstEntry = theChart.getChartEntry(entry.getI(), splitPoint -1);
	    EbitgChartEntry secondEntry = theChart.getChartEntry(splitPoint, entry.getJ());
	    if(firstEntry.hasInferences() && secondEntry.hasInferences()){
	      List<EbitgChartEntry> binaryDecomposition = new ArrayList<EbitgChartEntry>();
	      binaryDecomposition.add(firstEntry);
	      binaryDecomposition.add(secondEntry);
	      result.add(binaryDecomposition);
	    }	    
	  }	  
	  return result;
	}
	
	
	private boolean addLegalInferencesForPartitioning(List<EbitgChartEntry>  partitioning, EbitgChartEntry entry)
	{
		boolean foundLegalInference = false;
	
		List<EbitgInference>  possibleInferences =  generateAllPossibleKMultiPartInferences(partitioning,entry);
					 
		if(!possibleInferences.isEmpty())
		{
			 //System.out.println(":)Found possible inference");
		 
			for(EbitgInference inference : possibleInferences)
			{
				if(allTargetPositionsCoveredOrNonFinalInference(inference,entry))
				{
					entry.addInference(inference, theChart);
					foundLegalInference =true;
				}
			}
		 	setPropertiesChartEntryGivenFoundInferences(entry, possibleInferences);
		}
		
		return foundLegalInference; 
	}
	
	public boolean allTargetPositionsCoveredOrNonFinalInference(EbitgInference inference, EbitgChartEntry entry)
	{
		//System.out.println(" addInferncesToChartEntry : Found inference: " + inference);
		// To be admissible the inference must either cover the entire range of target positions
		// or it must not be the entry that covers the entire source span
		// This final test serves to prevent incomplete inferences being added to the final - source spanning - chart entry
		if(theChart.allTargetPositionsCovered(inference) || (!theChart.isWholeSourceRangeCoveringEntry(entry)))
		{
			return true;
		}
		return false;
	}
	
	public int computeUpperBoundPossibleInferences(List<List<EbitgChartEntry>> partitionings)
	{
		int upperboundPossibleInferences = 0;
		
		for(List<EbitgChartEntry>  partitioning : partitionings)
		{	
			int upperBoundPossibleInferencesPartitioning = 1;
			
			for(EbitgChartEntry entry : partitioning)
			{
				int multiplier = entry.getNodes().size();
				//System.out.println("multiplier: " + multiplier);
				
				if(upperBoundPossibleInferencesPartitioning >= (Integer.MAX_VALUE / multiplier))
				{
					System.out.println("More possible inferences than Integer.MAX_VALUE. Returning Integer.MAX_VALUE");
					return Integer.MAX_VALUE;
				}
				else
				{	
					upperBoundPossibleInferencesPartitioning *= multiplier;
					//ESystem.out.println("upperBoundPossibleInferencesPartitioning" + upperBoundPossibleInferencesPartitioning);
				}	
			}
			
			if(upperboundPossibleInferences > (Integer.MAX_VALUE -  upperBoundPossibleInferencesPartitioning))
			{
				System.out.println("More possible inferences than Integer.MAX_VALUE. Returning Integer.MAX_VALUE");
				return Integer.MAX_VALUE;
			}
			else
			{	
				upperboundPossibleInferences += upperBoundPossibleInferencesPartitioning;
			}	
		}
		
		
		return upperboundPossibleInferences;
		
	}
	
	
	public void setPropertiesChartEntryGivenFoundInferences(EbitgChartEntry entry, List<EbitgInference>  inferences)
	{
		if(inferences.size() > 0)
		{	
			 // A chart entry with a non-atomic derivation is by definition a complete one 
			 // (otherwise the derivation would not have been allowed)
			 entry.setIsComplete(true);
			 // A chart entry with a non-atomic derivation must have some alignment to 
			 // the target involved
			 entry.setIsAligned(true);
		}
		
		EbitgInference representativeInference = inferences.get(0);
		 // Set type of chart entry to binary if the first inference found is of type BITT
		 // (this implies all other inferences must be BITT as well)
		 if(representativeInference.isOfInferenceType(EbitgInference.InferenceType.BITT))
		 {
			 entry.setEntryType(EntryType.BITT);
			 
			 if(!representativeInference.isInverted())
			 {
				 entry.setContainsMonontoneBittInferences(true);
			 }
			 	 
		 }
		 else
		 {
			 entry.setEntryType(EntryType.Complex);	
		 }
		
	}
	
	/** Method that checks whether a chart entry can be derivable in principle. 
	 * A Chart entry is in principle derivable if :
	 * 1. All the source positions that are required through dependencies of atomic 
	 * entries occurring in the range i..j fall within the range i..j
	 * 2. The set off target positions that must or can be mapped to by the set of
	 * atomic entries for the range i...j forms a consecutive sequence 
	 *  
	 * @param i 
	 * @param j
	 * @return Boolean indicating if the chart entry could in principle be derivable
	 */
	
	public boolean constitutesPossiblyDerivableChartEntry(EbitgChartEntry entry)
	{
		HashSet<Integer> nescessarySourcePositions = new HashSet<Integer>();
		HashSet<Integer> mappedTargetPositions = new HashSet<Integer>(); 
		
		for(int pos = entry.getI(); pos <= entry.getJ(); pos++)
		{
			EbitgChartEntry atomicEntry = theChart.getChartEntry(pos,pos);
			nescessarySourcePositions.addAll(atomicEntry.getDependentSourcePositions());
			mappedTargetPositions.addAll(atomicEntry.getBindableTargetPositions());
		}
		
		// Check that all required source positions are within the range  i...j
		for(int pos : nescessarySourcePositions)
		{
			// If some included chunk requires a source position outside the range i ... j
			if((pos < entry.getI()) || (pos > entry.getJ()))
			{
				//System.out.println("constitutesDerivableChartEntry - missing required source positions => false");
				return false;
			}	
		}
		
		
		// Return false if the mapped target positions do not form a consecutive range
		if(!IntegerSetMethods.formsConsecutiveRange(mappedTargetPositions))
		{
			//System.out.println("constitutesDerivableChartEntry - target positions do not form consecutive range => false");
			return false;
		}
		
		return true;
		
	}
	
	private boolean canBITGInferencesBeFound(List< MultiplePartsInferenceCandidate> candidateSet)
	{
		boolean bitgInferencesCanBeFound = false;		
		if(candidateSet.size() > 0)
		{
			if(candidateSet.get(0).getParts().size() == 2)
			{	
			
				if(canFormBITGInference(candidateSet))
				{
					// if one bitg inference can be found, all valid inferences must be bitg
					bitgInferencesCanBeFound = true;
				}	
			}
		}
		return bitgInferencesCanBeFound;
	}
	
	
	public List<EbitgInference>  generateAllPossibleKMultiPartInferences(List<EbitgChartEntry> involvedEntries, EbitgChartEntry containingChartEntry)
	{
		//System.out.println("genereateAllPossibleKMultiPartDerivations, involvedEntries.size(): " +involvedEntries.size());
		List< MultiplePartsInferenceCandidate> candidateSet = generateMultiplePartsInferenceCandidateSet(involvedEntries);
		List<EbitgInference> generatedInferences =  new ArrayList<EbitgInference>(); 
		List<EbitgChartEntry> nodeChartEntries = new ArrayList<EbitgChartEntry> (involvedEntries);
		boolean bitgInferencesCanBeFound = canBITGInferencesBeFound(candidateSet);
		
		// Check for all the members of candidateSet if they contain consecutive target ranges, 
		// if so they are accepted
		for(MultiplePartsInferenceCandidate candidate :candidateSet)
		{
			if(IntegerSetMethods.formsConsecutiveRange(candidate.getBoundTargetPositions()))
			{
				EbitgInference newInference; 
			
				if (bitgInferencesCanBeFound)
				{
					newInference = createBitgInferenceFromMultiPartsInferenceCandidate(candidate, nodeChartEntries,containingChartEntry);
				}
				else 
				{ 
					// accepted candidate, create new Derivation using its parts
					newInference = createEbitgInference(candidate.getParts(),nodeChartEntries,containingChartEntry);
				}
				//System.out.println("candidate : " + candidate.getBoundTargetPositions());
				//System.out.println(" newInference : " + newInference.getBoundTargetPositions());
				assert(IntegerSetMethods.formsConsecutiveRange(newInference.getBoundTargetPositions()));
				//System.out.println("Nodes: " + newInference.getNodes());
				assert(newInference.getNodes().size() == involvedEntries.size());
				generatedInferences.add(newInference);
			}	
		}
		return generatedInferences;
	}
	
	private EbitgInference createBitgInferenceFromMultiPartsInferenceCandidate(MultiplePartsInferenceCandidate candidate, List<EbitgChartEntry> nodeChartEntries,EbitgChartEntry containingChartEntry)
	{
		EbitgInference newInference;
		EbitgNode leftNode = candidate.getParts().get(0);
		EbitgNode rightNode = candidate.getParts().get(1);
		newInference = generateBitgInference(leftNode,rightNode, nodeChartEntries,containingChartEntry);
		return newInference;
	}
	
	/** Determine if any  candidate in the set can form a BITG inference. This requires
	 *  that :
	 *  1. It consists of 2 nodes
	 *  2. These two nodes have independent target sets that form consecutive ranges,
	 *  which is checked by the determineAdjacency method
	 */
	boolean canFormBITGInference(List<MultiplePartsInferenceCandidate> candidateSet)
	{
		// we have to check for all candidates and not just for the first one.
		// In case of null-bound target positions, otherwise we might incorrectly decide
		// based on adjacency information that ITG is not possible
		for(MultiplePartsInferenceCandidate candidate : candidateSet)
		{	
			//System.out.println(candidate);
			
			ArrayList<EbitgNode> nodes = candidate.getParts();
			
			if(nodes.size() == 2)
			{
				int adjacency = determineAdjacency(nodes.get(0), nodes.get(1));
				
				if(adjacency >= 0)
				{
					 return true;
				}
			}
		}	
		return false;
	}
	
	
	public EbitgInference generateBitgInference(EbitgNode leftNode,EbitgNode rightNode, List<EbitgChartEntry> nodeChartEntries,EbitgChartEntry containingChartEntry)
	{
		int adjacency = determineAdjacency(leftNode,rightNode);
	
		if(adjacency >= 0 )
		{
			Boolean inverted;
			
			if(adjacency == 0)
			{
				inverted = false;
			}
			else
			{
				inverted = true;
			}
			
			EbitgInference inference = createEbitgInference(leftNode,rightNode,nodeChartEntries,inverted,containingChartEntry);
			
			return inference;
		}
		
		throw new RuntimeException("EbitgChartFiller : Error: Trying to generate bitg inference were this is not possible");
	}	
	
	
	/**
	 * 	Method that generates a set of candidates for possible multiple part inferences.
	 * 	Every candidate in the set consist itself of a set of nodes, such that none of the nodes 
	 *  in this set have overlapping extra bound target positions.
	 * @param involvedEntries
	 * @return A list of generated MultipPartsInferenceCandidate candidate for the list of involved entries
	 */
	public ArrayList< MultiplePartsInferenceCandidate> generateMultiplePartsInferenceCandidateSet(List<EbitgChartEntry> involvedEntries)
	{
		ArrayList< MultiplePartsInferenceCandidate> candidateSet = new ArrayList<MultiplePartsInferenceCandidate >(); 
		
		// initialization
		EbitgChartEntry firstEntry = involvedEntries.get(0);
	
		// Add one initial candidate for every possible derivation of the first involved chart entry
		for(EbitgNode node : firstEntry.getNodes())
		{
			MultiplePartsInferenceCandidate  candidate = new  MultiplePartsInferenceCandidate(new EbitgNode(node)); 
			candidateSet.add(candidate);
		}
		
		// Iterative extension candidates
		for(int i = 1; i < involvedEntries.size(); i++)
		{
			//System.out.println("candidate set: " + candidateSet);
			
			ArrayList< MultiplePartsInferenceCandidate> newCandidateSet = new ArrayList<MultiplePartsInferenceCandidate >(); 
			
			EbitgChartEntry entry = involvedEntries.get(i);
			
			
			for(EbitgNode node : entry.getNodes())
			{
				for(MultiplePartsInferenceCandidate candidate :candidateSet)
				{
					if(candidate.canBeExtendedBy(node))
					{
						// make a copy of candidate
						MultiplePartsInferenceCandidate newCandidate = new MultiplePartsInferenceCandidate(candidate);
						// extend the copy
						newCandidate.extend(new EbitgNode(node));
						
						newCandidateSet.add(newCandidate);
					}
				}
			}
			// replace candidateSet with the new set
			candidateSet = newCandidateSet;
		}
		
		return candidateSet;
	}
	
	
		
	public int determineAdjacency(EbitgNode firstNode, EbitgNode secondNode)
	{			
		int adjacency = -1;
		
		int minFirst = firstNode.getMinTargetPos();
		int maxFirst = firstNode.getMaxTargetPos();
		int minSecond = secondNode.getMinTargetPos();
		int maxSecond = secondNode.getMaxTargetPos();
		
		/*
		if((firstNode.getExtraBoundTargetPositions().size() > 0) && (firstNode.getBoundTargetPositions().size() == 2)  && (secondNode.getBoundTargetPositions().size() >  8)) 
		{	
			System.out.println("\ndeteermineAdjacency");
			System.out.println("minFirst: " + minFirst + "maxFirst : " + maxFirst + "minSecond: " + minSecond + "maxSecond : " + maxSecond);
			System.out.println("firstNode.getExtraBoundTargetPositions() : " + firstNode.getExtraBoundTargetPositions());
			System.out.println("secondNode.getExtraBoundTargetPositions() : " + secondNode.getExtraBoundTargetPositions());
			System.out.println("firstNode.getBoundTargetPositions() : " + firstNode.getBoundTargetPositions());
			System.out.println("secondNode.getBoundTargetPositions() : " + secondNode.getBoundTargetPositions());
		}
		*/
			
		// monotonous:all target positions first node precede those of second node
		if((minSecond - maxFirst) == 1)
		{
			adjacency = 0;
		}
		// inversion:all target positions second node precede those of first node
		else if((minFirst - maxSecond) == 1)
		{
			adjacency = 1;
		}
		// discontinuous.: neither positions of first nor second node completely precede those of other node
		else
		{
			adjacency = -1;
		}
		
		return adjacency;
	}
}
