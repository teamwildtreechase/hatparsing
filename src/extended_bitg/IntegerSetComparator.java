/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package extended_bitg;

import java.util.Comparator;
import java.util.Set;

import bitg.IntegerSetMethods;

public class IntegerSetComparator implements Comparator<Set<Integer>>
{
	@Override
	public int compare(Set<Integer> set1, Set<Integer> set2) 
	{
		// Find minimum and maximum values of the two sets
		int set1Min = IntegerSetMethods.findMinInteger(set1);
		int set1Max = IntegerSetMethods.findMaxInteger(set1);
		int set2Min = IntegerSetMethods.findMinInteger(set2);
		int set2Max = IntegerSetMethods.findMaxInteger(set2);
		
		// All elements of set1 are strictly smaller than those of set2
		// implying set1 strictly follows set2
		if(set1Max < set2Min)
		{
			return  -1;
		}
		// All elements of set2 are strictly smaller than those of set1
		// implying set2 strictly follows set1
		else if(set2Max < set1Min)
		{
			return 1;
		}
		// Neither set1 nor set2 strictly precedes the other: the two sets are incomparable,
		// their ordering is arbitrary
		else
		{
			return 0;
		}
	}
}
