/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;


public class BITGChartEntry 
{

	protected int i; // the lower and upperbound of the source range covered by this node
	protected int j;
	protected ArrayList<BITGInference > inferences;
	
	// the lowest word index covered by the target range of this chart entry
	private int minTargetPos;
	// the highest word index covered by the target range of this chart entry
	private int maxTargetPos;
	
	
	public BITGChartEntry(int i, int j)
	{
		this.i = i;
		this.j = j;
		// initialize inferences to an empty list
		inferences = new ArrayList< BITGInference >();
	
	}
	
	
	public String toString()
	{
		String s = "";
		s += "ChartEntry : <"+ i +"," + j + ">";
		
		// loop over the inferences
		for( BITGInference  d : inferences)
		{
			s += "\n\t" +d.toString();
		}
		
		return s;
	}
	
	public void addDerivation( BITGInference  d)
	{
		this.inferences.add(d);
	}
	
	public  BITGInference  getInference(int i)
	{
		return inferences.get(i);
	}
	
	public  ArrayList<BITGInference> getInferences()
	{
		return this.inferences;
	}
	
	public void setMinTargetPos(int min)
	{
		this.minTargetPos = min;
	}
	
	public int getMinTargetPos()
	{
		return this.minTargetPos;
	}
	
	public void setMaxTargetPos(int max)
	{
		this.maxTargetPos = max;
	}
	
	public int getMaxTargetPos()
	{
		return this.maxTargetPos;
	}
	
	public Boolean isDerivable()
	{
		// A Chart entry is derivable if it has at least one derivation listed
		return (inferences.size() > 0);
	}
	
	// Setters for i and j, to be used to override automatically set values when Chart Entries
	// are used in a multi-dimensional array
	public void setI(int i)
	{
		this.i = i;
	}
	
	public void setJ(int j)
	{
		this.j = j;
	}

	public int getI()
	{
		return i;
	}
	
	public int getJ()
	{
		return j;
	}
	
	public boolean isPrimitive()
	{
		return(i == j);
	}
	
	public int spanLength()
	{
		return j - i + 1;
	}
	
	

}
