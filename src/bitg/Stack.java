/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;


public class Stack <Element>
{
	ArrayList<Element> stack;
	
	public Stack()
	{
		stack = new ArrayList<Element>();
	}
	
	// Copy constructor
	public Stack(Stack<Element> toCopy)
	{
		this.stack = new ArrayList<Element>();
		
		for(Element e : toCopy.stack)
		{
			this.stack.add(e);
		}
	}
	
	public Element pop()
	{
		return stack.remove(stack.size() - 1);
	}

	public void push(Element elt)
	{
		stack.add(elt);
	}
	
	public Element getTop()
	{
		if(stack.size() > 0)
		{	
			return stack.get(stack.size() - 1);
		}
		else
		{
			return null;
		}
		
	}
	
	public boolean isEmpty()
	{
		return stack.isEmpty();
	}
	
	public ArrayList<Element> getStack()
	{
		return stack;
	}
}
