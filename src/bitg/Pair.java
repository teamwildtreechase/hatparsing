/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

public class Pair<Element1, Element2> 
{
	public Element1 first;
	public Element2 last;
	
	public Pair(Element1 first, Element2 last)
	{
		this.first = first;
		this.last = last;
	}

}
