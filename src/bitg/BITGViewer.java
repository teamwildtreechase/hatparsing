/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import tsg.TSNodeLabel;
import viewer.*;
import viewer_io.ButtonIOComponent;
import viewer_io.ButtonIOComponentTable;
import viewer_io.CheckBoxTable;
import viewer_io.ExporterAllPDF;
import viewer_io.ExporterEPS;
import viewer_io.ExporterPDF;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.itextpdf.text.Font;

import tsg.corpora.Wsj;
import util.FileUtil;




@SuppressWarnings("serial")
public class BITGViewer extends TreeViewer<TSNodeLabel> {
	
	private static final String ButtonName = "BITGButton";
	private static TSNodeLabel initialSentence = initialSentence();    

	public static TSNodeLabel initialSentence() {
		try {
			return new TSNodeLabel("(S (V-H load) (NP (D a) (N-H corpus)))");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
    public BITGViewer(ButtonIOComponentTable buttonIOComponentTable, ConstTreePanel treePanel, CheckBoxTable checkBoxTable) 
    {
        super(buttonIOComponentTable, treePanel, checkBoxTable);                  
    }
    
    public static BITGViewer createBitgViewer()
    {
    	//Set up the drawing area.
    	CheckBoxTable checkBoxTable = CheckBoxTable.createCheckBoxTable();
		ConstTreePanel constTreePanel = ConstTreePanel.createConstTreePanel(checkBoxTable);
		constTreePanel.setBackground(Color.white);
		constTreePanel.setFocusable(false);
    	
    	return new BITGViewer(ButtonIOComponentTable.createButtonIOComponentTable(), constTreePanel, checkBoxTable);
    }
    
    public void setInitialSentence() {
    	ArrayList<TSNodeLabel> treebank = new ArrayList<TSNodeLabel>();
        treebank.add(initialSentence);
        loadTreebank(treebank, false);
    }
     
    
	protected void loadTreebank(File treebankFile, boolean resetSpinner) {                		
		corpusFile = treebankFile;
		ArrayList<TSNodeLabel> treebank = null;
		try {
			treebank = Wsj.getTreebank(treebankFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadTreebank(treebank, resetSpinner);
	}
	
	@Override
	protected void createInputFields() 
	{
		
		JTextField permField = new JTextField("1,2,3");
		
		java.awt.Font f = new java.awt.Font("Arial",Font.BOLD, 18);
		permField.setFont(f);
		
		setIoComponentsTable(ButtonIOComponentTable.createButtonIOComponentTable());
		getIoComponentsTable().addComponent(BITGViewer.ButtonName, new ButtonIOComponent(new JButton("Compute binary itg trees"),permField ,null, new JLabel("Permutation:")));
		
	}
	
	protected void initializeTreePanel() 
	{
	}
    
	
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("BITG Tree Viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final BITGViewer treeViewer = BITGViewer.createBitgViewer();
        treeViewer.initializeTreePanel();
        Dimension area = treeViewer.treePanel.getArea();
        frame.setMinimumSize(new Dimension(area.width+5, area.height+5));
        treeViewer.setOpaque(true); //content panes must be opaque
        frame.setContentPane(treeViewer);
        
        frame.addWindowListener (new WindowAdapter () {
            public void windowClosing (WindowEvent e) {
            	treeViewer.doBeforeClosing();
            }
        });

        
      //Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem openTreebank = new JMenuItem("Open Treebank");        
        JMenuItem exportEps = new JMenuItem("Export to EPS");
        JMenuItem exportPdf = new JMenuItem("Export to PDF");
        JMenuItem exportAllToPdf = new JMenuItem("Export All to PDF");
        //JMenuItem exportAllToXml = new JMenuItem("Export All to XML");
        JMenuItem quit = new JMenuItem("Quit");
        final JFileChooser fc = new JFileChooser();
        
        openTreebank.setAccelerator(KeyStroke.getKeyStroke('O', java.awt.event.InputEvent.ALT_MASK));
        exportEps.setAccelerator(KeyStroke.getKeyStroke('E', java.awt.event.InputEvent.ALT_MASK));
        exportPdf.setAccelerator(KeyStroke.getKeyStroke('P', java.awt.event.InputEvent.ALT_MASK));	   
        exportAllToPdf.setAccelerator(KeyStroke.getKeyStroke('A', java.awt.event.InputEvent.ALT_MASK));
        //exportAllToXml.setAccelerator(KeyStroke.getKeyStroke('X', java.awt.event.InputEvent.ALT_MASK));
        quit.setAccelerator(KeyStroke.getKeyStroke('Q', java.awt.event.InputEvent.ALT_MASK));
        
        openTreebank.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int returnVal = fc.showOpenDialog(treeViewer);	                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	File selectedFile = fc.getSelectedFile();
                	treeViewer.loadTreebank(selectedFile, true);
                }               
            }
        });
        exportEps.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int returnVal = fc.showSaveDialog(treeViewer.treePanel);	                
                if (returnVal == JFileChooser.APPROVE_OPTION) {          
                	File selectedFile = fc.getSelectedFile();
                	selectedFile = FileUtil.changeExtention(selectedFile, "eps");
                	ExporterEPS.exportToEPS(selectedFile, treeViewer.treePanel);
                }
            }
        });
        exportPdf.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int returnVal = fc.showSaveDialog(treeViewer.treePanel);	                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	File selectedFile = fc.getSelectedFile();
	                selectedFile = FileUtil.changeExtention(selectedFile, "pdf");
	                ExporterPDF.exportToPDF(selectedFile, treeViewer.treePanel);
                }	                    
            }
        });
        exportAllToPdf.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int returnVal = fc.showSaveDialog(treeViewer.treePanel);	                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	File selectedFile = fc.getSelectedFile();
	                selectedFile = FileUtil.changeExtention(selectedFile, "pdf");
	                ExporterAllPDF.exportAllToPDF(selectedFile,  treeViewer.treePanel);
                }
            }
        });
        /*exportAllToXml.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int returnVal = fc.showSaveDialog(treeViewer.treePanel);	                
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	File selectedFile = fc.getSelectedFile();
                	selectedFile = FileUtil.changeExtention(selectedFile, "xml");
                    ((TesniereTreePanel)treeViewer.treePanel).exportAllToXML(selectedFile);
                }
            }
        });*/
        quit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	treeViewer.doBeforeClosing();
                System.exit(0);
            }
        });
            
        
        treeViewer.getIoComponentsTable().get(0).getTheButton().addActionListener(new ActionListener()
        {
        	public void actionPerformed(ActionEvent e)
        	{
        	
        			System.out.println(">>> Produce ITG trees for permutation...");    			
        			
        			 String permutationString = treeViewer.getIoComponentsTable().get(0).getTextfieldContents();
        			 BITGChartBuilder.buildAndWriteTreesForPermutation(permutationString);
        			 
        			  File f = new File("tempITGTrees.txt");
        		      treeViewer.loadTreebank(f, true);
        			
        			//String outputFilePath =  treeViewer.ioComponents[0].getTheTextField().getText();
        			//((AlignmentTreePanel)treeViewer.treePanel).produceReorderedSourceFile(outputFilePath);
        			
        	
        		
        		
        	}
        });
        
        file.add(openTreebank);
        file.add(exportEps);
        file.add(exportPdf);
        file.add(exportAllToPdf);
        //file.add(exportAllToXml);
        file.add(quit);
        openTreebank.setMnemonic('O');	        
        exportPdf.setMnemonic('P');
        exportAllToPdf.setMnemonic('A');
        //exportAllToXml.setMnemonic('X');
        quit.setMnemonic('Q');	        	        

        menuBar.add(file);
        frame.setJMenuBar(menuBar);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
	
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.    	
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

	@Override
	public boolean loadTreebankAndSetSpinner(File file, boolean resetSpinner) {
	
		List<TSNodeLabel> treeBank  = null;
		try 
		{
			treeBank = Wsj.getTreebank(file);
			super.loadTreebank(treeBank, resetSpinner);
			return true;
		} catch (Exception e) {
				
			e.printStackTrace();
			return false;
		}
		
	}


}
