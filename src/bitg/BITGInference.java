/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

// Very simple class that stores the nescessary info of a derivation, consisting 
// of the split point and wether or not this derivation requires the left and right parts to 
// be inverted
public class BITGInference 
{
	public Boolean inverted;
	
	// The splitPoint is defined as the last index belonging to the left part that is 
	// covered by the left tree that is combined by the right tree to form this derivation
	public int splitPoint;
	
	public BITGInference(Boolean inverted, int splitPoint)
	{
		this.inverted = inverted;
		this.splitPoint = splitPoint;
	}

	public String toString()
	{
		String s = "{Derivation} ";
		
		s += "Inverted : " + inverted + " splitPoint: " + splitPoint;
		
		return s;
	}
}
