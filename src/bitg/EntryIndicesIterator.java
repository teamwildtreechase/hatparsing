/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;

import util.Pair;

public class EntryIndicesIterator 
{
	
	ArrayList<Pair<Integer>> indexPairs; 
	int curIndex = 0;
	
	public  EntryIndicesIterator(int sourceLength, int minLength, int maxLength)
	{
	
		indexPairs = new ArrayList<Pair<Integer>>();
		
		// the maxLength may not be bigger than the source length
		maxLength = Math.min(maxLength, sourceLength);
		
		for(int spanLength = minLength; spanLength <= maxLength; spanLength++)
		{
			//System.out.println("\nEntries with spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (sourceLength - spanLength + 1); beginIndex++)
			{
				int i = beginIndex;
				int j = beginIndex + spanLength - 1;
				
				indexPairs.add(new Pair<Integer>(i,j));
			}
		}
	}	

	public boolean hasMoreElements()
	{
		return(curIndex < indexPairs.size());
	}
   
	public Pair<Integer> nextElement()
	{
		Pair<Integer> p = indexPairs.get(curIndex);
		curIndex++;
		return p;
	}
	
}
