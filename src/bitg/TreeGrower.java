/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

// Class that grows a binary tree from the chart
public class TreeGrower 
{
	private BITGChartBuilder bitg;
	Stack<PartialTree> partialTrees;
	private Stack<PartialTree> completedTrees;
	
	public TreeGrower(	BITGChartBuilder bitg)
	{
		this.setBitg(bitg);
		partialTrees = new Stack<PartialTree>();
		setCompletedTrees(new Stack<PartialTree>());
	}
	
	
	public void growTrees()
	{
		BITGChartEntry rootEntry = getBitg().getChartEntry(0, getBitg().getSourceLength() - 1);
		PartialTree rootTree = new PartialTree(rootEntry);
		partialTrees.push(rootTree);
	
		int i = 0;
		while(!partialTrees.isEmpty())
		{
			System.out.println("GrowTrees: iteration" + i);
			PartialTree t = partialTrees.pop();
			growPartialTree(t);
			i++;
		}
	
	}
	
	public void growPartialTree(PartialTree t)
	{

		t.printStack();
		System.out.println("lastFinishedLeafIndex: " + t.lastFinishedLeafIndex);
		
		if(t.chartEntryStack.isEmpty())
		{
			t.extendTreeStringRepresentation(".");
			getCompletedTrees().push(t);
			System.out.println("Completed tree:" + t.getTreeString());
			return;
		}
		

		// get the top entry from the stack		
		BITGChartEntry topEntry = t.topChartEntry();
		int i = topEntry.getI();
		int j = topEntry.getJ();
		String nodeLabel;
		
				
		if(topEntry.isPrimitive())			
		{	
			System.out.println(">> Case Primitive");
			
			// remove the entry from the stack
			t.popChartEntry();
		
			
			nodeLabel = "" + getBitg().permutation[i];
			t.extendTreeStringRepresentation("(" + nodeLabel + ")");
			
			// put the partial tree back on the stack
			System.out.println("Inomplete tree:" + t.getTreeString());
			t.lastFinishedLeafIndex++;
			partialTrees.push(t);
				
		}
		else
		{
			System.out.println(">> Case Complex");
			
			if(j <= t.lastFinishedLeafIndex)
			{
				System.out.println("Finished subtree");
				t.popChartEntry();
				// add closing bracket and point
				t.extendTreeStringRepresentation(")");
				partialTrees.push(t);
				
			}
			
			else
			{	
				for(BITGInference d : topEntry.getInferences())
				{
					PartialTree extension = new PartialTree(t);
	
					if(d.inverted)
					{
						nodeLabel = "<>";
					}
					else
					{
						nodeLabel = "[]";
					}
					extension.extendTreeStringRepresentation("(" + nodeLabel);
					
					// push chart entries for the left and right children the top is split into for
					// this derivation
					// push the left part last to be on top of the stack
					extension.pushChartEntry(getBitg().getChartEntry(d.splitPoint + 1, j));
					extension.pushChartEntry(getBitg().getChartEntry(i,d.splitPoint));
				
					partialTrees.push(extension);
					
					System.out.println("Current tree:" + extension.getTreeString());
				}
			}	
		}
	}	
	
	public void writeCompletedTreesToFile(String fileName)
	{
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
			ArrayList<PartialTree> trees = getCompletedTrees().getStack();
			
			for(PartialTree tree : trees)
			{
				writer.write("\n"+ tree.getTreeString());
			}		
		
			writer.close();
		
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void printCompletedTrees()
	{
		ArrayList<PartialTree> trees = getCompletedTrees().getStack();
		
		System.out.println("Completed trees: ");
		
		for(PartialTree tree : trees)
		{
			System.out.println(tree.getTreeString());
		}
	}


	public void setBitg(BITGChartBuilder bitg) {
		this.bitg = bitg;
	}


	public BITGChartBuilder getBitg() {
		return bitg;
	}


	public void setCompletedTrees(Stack<PartialTree> completedTrees) {
		this.completedTrees = completedTrees;
	}


	public Stack<PartialTree> getCompletedTrees() {
		return completedTrees;
	}
	
}
