/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;
import java.util.HashSet;

import alignmentStatistics.SetMethods;

import extended_bitg.EbitgNode;


public class MultiplePartsInferenceCandidate 
{
	private ArrayList<EbitgNode> parts;
	private HashSet<Integer> extraBoundTargetPositions;
	private HashSet<Integer> boundTargetPositions;
	public final SetMethods<Integer> setMethods  = new SetMethods<Integer> ();
	
	
	public MultiplePartsInferenceCandidate()
	{
		this.parts = new ArrayList<EbitgNode>();
		this.extraBoundTargetPositions = new HashSet<Integer>(); 
		this.boundTargetPositions = new HashSet<Integer>(); 
	}
	
	public MultiplePartsInferenceCandidate(EbitgNode firstPart)
	{
		this();
		this.parts.add(firstPart);
		this.extraBoundTargetPositions.addAll(firstPart.getExtraBoundTargetPositions());
		this.boundTargetPositions.addAll(firstPart.getBasicBoundTargetPositions());
		this.boundTargetPositions.addAll(firstPart.getExtraBoundTargetPositions());
	}
	
	/** Copy constructor
	 * 
	 * @param toCopy
	 */
	public MultiplePartsInferenceCandidate(MultiplePartsInferenceCandidate toCopy)
	{
		// copy the parts ArrayList
		this.parts = new ArrayList<EbitgNode>(toCopy.parts);
		// copy the bound target positions
		this.extraBoundTargetPositions = new HashSet<Integer>(toCopy.extraBoundTargetPositions);
		this.boundTargetPositions = new  HashSet<Integer>(toCopy.boundTargetPositions);
	}

	
	public HashSet<Integer> getBoundTargetPositions()
	{
		return boundTargetPositions;
	}
	
	public HashSet<Integer> getExtraBoundTargetPositions()
	{
		return extraBoundTargetPositions;
	}
	
	public ArrayList<EbitgNode> getParts()
	{
		return parts;
	}
	
	public void extend(EbitgNode part)
	{
		assert(canBeExtendedBy(part));
		
		parts.add(part);
		extraBoundTargetPositions.addAll(part.getExtraBoundTargetPositions());
		boundTargetPositions.addAll(part.getBasicBoundTargetPositions());
		boundTargetPositions.addAll(part.getExtraBoundTargetPositions());
	}
	
	
	public boolean canBeExtendedBy(EbitgNode part)
	{
		// The extension is possible if the extending part contains no target positions already present
		//in the MultiplePartsDerivation to be extended
		return setMethods.hasEmptyIntersection(extraBoundTargetPositions,part.getExtraBoundTargetPositions());
	}
	
	public String toString()
	{
		String result = "";
		result += "MultiplePartsDerivationCandidate noParts: " + this.parts.size();
		for(EbitgNode part : parts)
		{	
			result += "\npart: " + part.toString();
		}
		//result += "MultiplePartsDerivationCandidate" + " extraBoundTargetPositions: " + this.getExtraBoundTargetPositions();
		return result;
	}

}


