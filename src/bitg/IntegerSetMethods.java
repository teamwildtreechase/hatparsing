/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;

public class IntegerSetMethods 
{
	public static int findMinInteger (Set<Integer> set) 
	{
		// Return -1 for empty sets
		if(set.isEmpty())
		{
			return -1;
		}
		return Collections.min(set);
	}
	
	public static int findMaxInteger (Set<Integer> set) 
	{
		// Return -1 for empty sets
		if(set.isEmpty())
		{
			return -1;
		}
		return Collections.max(set);
	}
	
	public static boolean formsConsecutiveRange(Set<Integer> set)
	{
		int min = findMinInteger(set);
		int max = findMaxInteger(set);
		
		// The maximum contained range length is the lengt of range min ... max, 
		// which equals (max - min) + 1
		int maxContainedRangeLength = (max - min) + 1;
		
		// Since the set contains only unique numbers, we know that if its length is equal to 
		// the maximum contained range length, we must have a consecutive range
		if(set.size() == maxContainedRangeLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static String setString(Set<Integer> set, int offset, boolean addBrackets)
	{
		String result = "";
		
		if(addBrackets)
		{	
			result = "{";
		}
		
		ArrayList<Integer> elements = new ArrayList<Integer>(set);
		
		for(int i = 0; i < elements.size() -1 ; i++)
		{
			result += (elements.get(i) - offset) + ",";
		}
		
		// add the last element
		if(elements.size() > 0)
		{
			result += (elements.get(elements.size() - 1)  - offset) ; 
		}
	
		
		if(addBrackets)
		{	
			result += "}";
		}
			
		return result;
	}
	
	public static String setsString(ArrayList<Set<Integer>> sets)
	{
		String result = "[";
		int min = Integer.MAX_VALUE;
		
		// find the minimum over the set of sets
		for(Set<Integer> set : sets)
		{
			int setMin = IntegerSetMethods.findMinInteger(set);
			if(setMin < min)
			{
				min = setMin;
			}
		}
		
		ListIterator<Set<Integer>> listIterator = sets.listIterator();
		while(listIterator.hasNext())
		{
			Set<Integer> set = listIterator.next();
			result += IntegerSetMethods.setString(set, min,true); 
			if(listIterator.hasNext())
			{	
				result +=	",";
			}	
		}
		
		result += "]";
		
		return result;
	}

	public static Set<Integer> createConsecutiveRangeSet(int minPos, int maxPos)
	{
		Set<Integer> result = new HashSet<Integer>();
		for(int i = minPos; i <= maxPos; i++)
		{
			result.add(i);
		}
		return result;
	}
	
}
