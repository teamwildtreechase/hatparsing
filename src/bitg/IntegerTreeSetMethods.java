/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;
import java.util.TreeSet;

public class IntegerTreeSetMethods 
{
	public static int findMinInteger (TreeSet<Integer> set) 
	{
		// Return -1 for empty sets
		if(set.isEmpty())
		{
			return -1;
		}
		
		int min = Integer.MAX_VALUE;
		
		for(int num : set)
		{
			if(num < min)
			{
				min = num;
			}
		}
		return min;
	}
	
	public static int findMaxInteger (TreeSet<Integer> set) 
	{
		// Return -1 for empty sets
		if(set.isEmpty())
		{
			return -1;
		}
		
		int max = Integer.MIN_VALUE;
		
		for(int num : set)
		{
			if(num > max)
			{
				max = num;
			}
		}
		return max;
	}
	
	public static boolean formsConsecutiveRange(TreeSet<Integer> set)
	{
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(int element : set)
		{
			if(element < min)
			{
				min = element;
			}
			
			if(element > max)
			{
				max = element;
			}
		}
		
		// The maximum contained range length is the lengt of range min ... max, 
		// which equals (max - min) + 1
		int maxContainedRangeLength = (max - min) + 1;
		
		
		// Since the set contains only unique numbers, we know that if its length is equal to 
		// the maximum contained range length, we must have a consecutive range
		if(set.size() == maxContainedRangeLength)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public static String setString(TreeSet<Integer> set, int offset)
	{
		String result = "[";
		
		ArrayList<Integer> elements = new ArrayList<Integer>(set);
		
		for(int i = 0; i < elements.size() -1 ; i++)
		{
			result += (elements.get(i) - offset) + ",";
		}
		
		// add the last element
		if(elements.size() > 0)
		{
			result += (elements.get(elements.size() - 1)  - offset) ; 
		}
		result += "]";
		
		return result;
	}
	
	public static String setsString(ArrayList<TreeSet<Integer>> sets)
	{
		String result = "[";
		int min = Integer.MAX_VALUE;
		
		// find the minimum over the set of sets
		for(TreeSet<Integer> set : sets)
		{
			int setMin = IntegerTreeSetMethods.findMinInteger(set);
			if(setMin < min)
			{
				min = setMin;
			}
		}
		
		for(int i = 0; i < sets.size() -1 ; i++)
		{
			TreeSet<Integer> set = sets.get(i);
			result += IntegerTreeSetMethods.setString(set, min) + ",";
		}
		
		if(sets.size() > 0)
		{
			result += (IntegerTreeSetMethods.setString(sets.get(sets.size() - 1), min)); 
		}
		
		result += "]";
		
		return result;
	}

}
