/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;
import java.util.HashMap;

public class ChartEntry<NodeType, InferenceType> 
{
	
	protected int i; // the lower and upperbound of the source range covered by this node
	protected int j;
	protected HashMap<NodeType, ArrayList<InferenceType>> nodeInferences;
	
	// the lowest word index covered by the target range of this chart entry
	private int minTargetPos;
	// the highest word index covered by the target range of this chart entry
	private int maxTargetPos;
	
	
	public ChartEntry(int i, int j)
	{
		this.i = i;
		this.j = j;
		// initialize nodeInferences to an empty HashMap
		nodeInferences = new HashMap<NodeType, ArrayList<InferenceType>>();
	
	}
	
	
	public String toString()
	{
		String s = "";
		s += "ChartEntry : <"+ i +"," + j + ">";
		
		// loop over the derived nodes
		for(NodeType key : nodeInferences.keySet())
		{	
			// loop over the nodeInferences for each of the nodes
			for(InferenceType d : nodeInferences.get(key))
			{
				s += "\n\t" +d.toString();
			}
		}
		
		return s;
	}
	
	public void addInference(InferenceType d, NodeType node)
	{
		
		ArrayList<InferenceType> inferrenceArray;
		
		if(this.nodeInferences.containsKey(node))
		{
			inferrenceArray = this.nodeInferences.get(node);
			inferrenceArray.add(d);
		}
		else
		{
			inferrenceArray = new ArrayList<InferenceType> ();
			inferrenceArray.add(d);
			this.nodeInferences.put(node, inferrenceArray);
		}
	}
	
	public InferenceType getInference(NodeType node, int i)
	{
		ArrayList<InferenceType> inferenceArray = nodeInferences.get(node);		
		return inferenceArray.get(i);
	}
	
	public  ArrayList<InferenceType> getInferences(NodeType node)
	{
	
		return nodeInferences.get(node);
	}
	
	public NodeType getNode(NodeType node)
	{
		for(NodeType mapNode : this.nodeInferences.keySet())
		{
			if(mapNode.equals(node))
			{
				return mapNode;
			}
		}
		throw new RuntimeException("Trying to get a  node that is not present in the chart entry");
	}
	
	
	/**
	 * @return All inferences for the chart entry, i.e. all inferences for all nodes
	 */
	public ArrayList<InferenceType> getAllInferences()
	{
		ArrayList<InferenceType> allInferences = new ArrayList<InferenceType>(); 
		
		for(NodeType node : this.getNodes())
		{
			allInferences.addAll(this.getInferences(node));
		}
		
		return allInferences;
	}
	
	public ArrayList<NodeType> getNodes()
	{
		return new ArrayList<NodeType>(nodeInferences.keySet());
	}
	
	
	public void setMinTargetPos(int min)
	{
		this.minTargetPos = min;
	}
	
	public int getMinTargetPos()
	{
		return this.minTargetPos;
	}
	
	public void setMaxTargetPos(int max)
	{
		this.maxTargetPos = max;
	}
	
	public int getMaxTargetPos()
	{
		return this.maxTargetPos;
	}
	
	// Setters for i and j, to be used to override automatically set values when Chart Entries
	// are used in a multi-dimensional array
	public void setI(int i)
	{
		this.i = i;
	}
	
	public void setJ(int j)
	{
		this.j = j;
	}

	public int getI()
	{
		return i;
	}
	
	public int getJ()
	{
		return j;
	}
	
	public boolean isPrimitive()
	{
		return(i == j);
	}
	
	public int spanLength()
	{
		return j - i + 1;
	}
	
	/**
	 * Method that tells whether the chart entry already has inferences (which can only be the case if it has
	 * already been visited once by the algorithm)
	 * @return : boolean indicating whether the chart entry contains any inferences
	 */
	public boolean hasInferences()
	{
		return (!this.nodeInferences.isEmpty()); 
	}
	

}
