/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;

import java.util.ArrayList;


public class PartialTree 
{
	
	private String treeString; // the Tree that has been build up so far in String format
	Stack<BITGChartEntry> chartEntryStack;
	int lastFinishedLeafIndex;

	public PartialTree()
	{
		lastFinishedLeafIndex = -1;
		chartEntryStack = new 	Stack<BITGChartEntry>();
		setTreeString("");
	}
	
	// Copy constructor
	public PartialTree(PartialTree toCopy)
	{
		this();
		this.lastFinishedLeafIndex = toCopy.lastFinishedLeafIndex;
		this.setTreeString(toCopy.getTreeString());
		
		// copy the chart entry stack
		this.chartEntryStack = new Stack<BITGChartEntry>(toCopy.chartEntryStack);
	}
	
	public PartialTree(BITGChartEntry entry)
	{
		this();
		chartEntryStack.push(entry);
	}

	public BITGChartEntry popChartEntry()
	{
		// return the last element from the stack and remove it
		return 	chartEntryStack.pop();
	}
	
	/** push a chart entry to the stack
	 * 
	 * @param entry
	 */
	public void pushChartEntry(BITGChartEntry entry)
	{
		chartEntryStack.push(entry);
	}
	
	public BITGChartEntry topChartEntry()
	{
		return chartEntryStack.getTop();
	}
	
	public void extendTreeStringRepresentation(String s)
	{
		setTreeString(getTreeString() + s);
	}
	
	public void printStack()
	{
		System.out.println("<Stack: >");
		ArrayList<BITGChartEntry> stack = chartEntryStack.getStack();
		for(int i = stack.size() - 1; i >= 0; i--)
		{
			BITGChartEntry entry = stack.get(i);
			System.out.println("{" + entry.getI() +"," + entry.getJ() +"},\t");
		}
		
		System.out.println("</Stack: >");
	}

	public void setTreeString(String treeString) {
		this.treeString = treeString;
	}

	public String getTreeString() {
		return treeString;
	}
	
}
