/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package bitg;



public class BITGChartBuilder 
{

	public int[] permutation;
	private BITGChartEntry[][] chart;
	private int sourceLength;
	
	public BITGChartBuilder(int[] perm)
	{
		// this should go be doable differently
		//permutation = new ArrayList(Arrays.asList(perm));
	
		this.permutation = perm;
		
		setSourceLength(perm.length);
		
		initializeChart();
	}	
	

	
	public String chartEntryStringExtra(BITGChartEntry entry)
	{
		String s = "\n|| ";
		s += entry.toString();
		s += "\nPermutation coverage: ";
		
		for(int i = entry.getI(); i <= entry.getJ(); i++)
		{
			s += permutation[i] + ",";
		}
		s += " ||\n";
		
		return s;
	}
	
	
	

	
	
	public BITGChartEntry getChartEntry(int i, int j)
	{
		return chart[i][j];
	}
	
	public void initializeChart()
	{
		// allocate the chart
		chart = new BITGChartEntry[getSourceLength()][getSourceLength()];
		
		// Set the proper i and j values for the entries in the chart
		for(int i = 0; i < getSourceLength(); i++)
		{
			// only initialize elements with j >= i
			for(int j = i; j < getSourceLength(); j++)
			{
				chart[i][j] = new BITGChartEntry(i,j);
			}
		}
					
	}
	
	// incrementally find all derivations bottomn up
	public void findDerivationsPermutation()
	{
		
		//System.out.println("Find derivations, sourceLengt: " + sourceLength);
		
		// loop over the span length from 0 to the length of the source permutation
		for(int spanLength = 1; spanLength <= getSourceLength(); spanLength++)
		{
			//System.out.println("Find derivations for spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (getSourceLength() - spanLength + 1); beginIndex++)
			{
				findDerivationsForChartEntry(beginIndex, beginIndex + spanLength -1);
			}
			
		}
	}
	
	
	// incrementally find all derivations bottomn up
	public void printChartContents()
	{
		
		System.out.println("Printing the contents of the chart:");
		
		// loop over the span length from 0 to the length of the source permutation
		for(int spanLength = 1; spanLength <= getSourceLength(); spanLength++)
		{
			System.out.println("\nEntries with spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (getSourceLength() - spanLength + 1); beginIndex++)
			{	
				BITGChartEntry entry = chart[beginIndex][beginIndex + spanLength - 1];
				if(entry != null)
				{	
					System.out.println(entry.toString());
				}	
			}	
		}
	}
	
	
	
	
	public void findDerivationsForChartEntry(int i, int j)
	{
		//System.out.println("Find derivations for chart entry: <" + i + "," + j + ">");
		
		// First get the entry
		BITGChartEntry entry = chart[i][j];
		
		if(i == j)
		{
			entry.addDerivation(new BITGInference(false, -1));
			entry.setMaxTargetPos(permutation[i]);
			entry.setMinTargetPos(permutation[i]);
		}
		else
		{
			for(int splitPoint = i; splitPoint < j; splitPoint++)
			{
				
				//System.out.println(">>SplitPoint: "+ splitPoint);
				
				BITGChartEntry leftPart = chart[i][splitPoint];
				BITGChartEntry rightPart = chart[splitPoint+1][j];
				
				// if both the left and the right part are derivable, then they can be combined 
				// under a new binary node in the tree that covers the range of this chart entry
				if(leftPart.isDerivable() && rightPart.isDerivable())
				{
					
					int leftMax = leftPart.getMaxTargetPos();
					int rightMax = rightPart.getMaxTargetPos();
					int leftMin = leftPart.getMinTargetPos();
					int rightMin = rightPart.getMinTargetPos();
					
					
					// We must check that put together, left and right will form a 
					// permutation of a consecutive series of integers
					// i.e. left and right form a permuted Sequence 
					// and the splitPoint gives a proper split of this sequence
					// See Huang et.al : Binarization of Synchronous Context Free Grammars for this terminology
					
					
					int adjacency = determineAdjacency(leftPart,rightPart);
					
					if(adjacency >= 0 )
					{
						Boolean inverted;
						
						if(adjacency == 0)
						{
							inverted = false;
						}
						else
						{
							inverted = true;
						}
						
						entry.addDerivation(new BITGInference(inverted,splitPoint));
						
						// This way we recompute the maximum covered word index every time we find 
						// a proper derivation, which is extra work, but still less so then 
						// looping over the entire range for entry and then taking the max over this
						entry.setMaxTargetPos(Math.max(leftMax, rightMax));
						entry.setMinTargetPos(Math.min(leftMin, rightMin));
						
						//System.out.println("Derivation for splitpoint " + splitPoint + " ok");
					}
					else
					{
						/*
						System.out.println("Non compatible left and right part, adjacency = "+ adjacency);
						System.out.println("splitPoint: " + splitPoint+ " -- {leftMin: "+ leftMin + " leftMax: " + leftMax +
								" || rightMin: "+ rightMin + " rightmax: " + rightMax+ "}");*/
					}
					
				}
				else
				{
					/*
					System.out.println("leftPart.isDerivable:" + leftPart.isDerivable());
					System.out.println("rightPart.isDerivable:" + rightPart.isDerivable());*/
				}
				
			}
		}
	}
	
	public int determineAdjacency(BITGChartEntry b, BITGChartEntry c)
	{
		int adjacency = -1;
		
		int minB = b.getMinTargetPos();
		int maxB = b.getMaxTargetPos();
		int minC = c.getMinTargetPos();
		int maxC = c.getMaxTargetPos();
		
		// monotonous:  all members right permutation all precede members left permutation
		if((minC - maxB) == 1)
		{
			adjacency = 0;
		}
		// swap: all members left permutation all precede members right permutation
		else if((minB - maxC) == 1)
		{
			adjacency = 1;
		}
		// Discontinuous.
		else
		{
			adjacency = -1;
		}
		
		return adjacency;
	}
	
	public String perrmutationString()
	{
		String s = "Permutation: ";
		
		for(int i = 0; i < permutation.length; i++)
		{
			s += permutation[i] + ",";
		}
		
		return s;
	}
	

	public static boolean testTreeGrowingExample1()
	{
		
		System.out.println("Performing TreeGrowing test...");
		// At bracketing level 2 none of these numbers can be bracketed, so there should be no derivation and 
		// no derivable entry of span length 2 either
		int[] permutation = new int[]{4,5,6,7,1,2,3}; 
		BITGChartBuilder b = new BITGChartBuilder(permutation);
		b.findDerivationsPermutation();
		
		TreeGrower tg = new TreeGrower(b);
		tg.growTrees();
		
		tg.printCompletedTrees();
		return true;		
	}
	
	
	public static void buildAndWriteTreesForPermutation(String permutation)
	{
			permutation = permutation.trim();
			String[] permutationSplitted = permutation.split(",");
			
			int[] permutationArray = new int[permutationSplitted.length];
			for(int i = 0; i < permutationArray.length; i++)
			{
				permutationArray[i] =  Integer.parseInt(permutationSplitted[i]); 
			}
			
			BITGChartBuilder b = new BITGChartBuilder(permutationArray);
			b.findDerivationsPermutation();
			
			TreeGrower tg = new TreeGrower(b);
			tg.growTrees();
			
			tg. writeCompletedTreesToFile("tempITGTrees.txt");
		
	}
	
	
	public static int[] invertPermutation(int[] permutation)
	{
		int[] invertedPermutation = new int[permutation.length];
		
		for(int i = 0; i < permutation.length; i++)
		{
			int num = permutation[i];
			invertedPermutation[num - 1] = i + 1;
		}
		
		return invertedPermutation;
	}
	
	
	public static void main(String[] args)
	{
		System.out.println("BITGChartBuilder started");
		
		/*
		BITGChartBuilder b1 = new BITGChartBuilder(new int[]{1,2,3});
		System.out.println(b1.perrmutationString());
		b1.findDerivationsPermutation();
		
		b1. printChartContents();
		*/
		
		BITGChartBuilder b2 = new BITGChartBuilder(new int[]{6,5,7,4,3,1,2});
		b2.findDerivationsPermutation();
		b2. printChartContents();
		
		testTreeGrowingExample1();
		
		int[] permutation = new int[]{3,1,2};
		int[] invertedPermutation = invertPermutation(permutation);
		
		System.out.println("Permutation: \n");
		for(int i = 0; i < permutation.length; i++)
		{
			System.out.println(permutation[i] + ",");
		}
		
		System.out.println("\nInverted Permutation: \n");
		for(int i = 0; i < permutation.length; i++)
		{
			System.out.println(invertedPermutation[i] + ",");
		}
	}



	public void setSourceLength(int sourceLength) {
		this.sourceLength = sourceLength;
	}



	public int getSourceLength() {
		return sourceLength;
	}
	
	
}
