/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Alignment 
{
	private final List<AlignmentPair> alignment;
	
	private Alignment(List<AlignmentPair> alignment)
	{
		this.alignment = alignment;
	}
	
	private static List<AlignmentPair> constructAlignmentPairs(String alignmentString)
	{
		if(alignmentString.length() > 0)
		{	
			// Trim the alignmentString to be more robust to superfluous whitespace at the beginning and end
			alignmentString = alignmentString.trim();
			
			// Split the line into words based on whitespace
			String[] wordAlignments = alignmentString.split("\\s");
			
			ArrayList<AlignmentPair> sentenceAlignments = new ArrayList<AlignmentPair>();
			// add the words from the array to the ArrayList
			for(int i = 0; i <  wordAlignments.length; i++)
			{
				String alignmentSubString =  wordAlignments[i];
				// Split the wordAlignment pair String further to get its parts
				String[] alignmentParts = alignmentSubString.split("-");
				// generate the actual AlignmentPair from the numbers extracted from the String
				AlignmentPair a = new AlignmentPair(Integer.parseInt(alignmentParts[0]),Integer.parseInt(alignmentParts[1]));
				sentenceAlignments .add(a);
			}
		

			return sentenceAlignments;
		
		}
		else
		{
			// No Alignment pairs (String is empty) : Return empty list of Alignment pairs
			return new ArrayList<AlignmentPair>();
		}
		
	}
	
	public static Alignment createAlignment(String alignmentString)
	{
		return new Alignment(constructAlignmentPairs(alignmentString));
	}
	
	
	public static Alignment createAlignment(List<AlignmentPair> alignmentPairs)
	{
		return new Alignment(alignmentPairs);
	}
	

	public static Alignment createInvertedAlignment(Alignment alignment)
	{
		List<AlignmentPair> alignmentInvertedPairs = new ArrayList<AlignmentPair>();
		for(AlignmentPair alignmentPair : alignment.getAlignmentPairs())
		{
			alignmentInvertedPairs.add(AlignmentPair.createInvertedAlignmentPair(alignmentPair));
		}
		return createAlignment(alignmentInvertedPairs);
	}
	
	
	
	public AlignmentPair get(int i)
	{
		return this.alignment.get(i);
	}
	
	public void add(AlignmentPair alignmentPair)
	{
		this.alignment.add(alignmentPair);
	}
	
	public int size()
	{
		return this.alignment.size();
	}
	
	public List<AlignmentPair> getAlignmentPairs()
	{
		return this.alignment;
	}
	
	public Set<Integer> getUnalignedSourcePositions(int sourceLength)
	{
		Set<Integer> alignedSourcePositions = new HashSet<Integer>();
		for(AlignmentPair alignmentPair : this.alignment)
		{
			alignedSourcePositions.add(alignmentPair.getSourcePos());
		}
		
		Set<Integer> result = new HashSet<Integer>();
		for(int i = 0; i < sourceLength; i++)
		{
			if(!alignedSourcePositions.contains(i))
			{
				result.add(i);
			}
		}
		return result;
	}
	
	private int getLastAlignmentPairIndex()
	{
		return this.alignment.size() -1;
	}

	public String getAlignmentAsString()
	{
		String result = "";
		
		for(int i =  0; i < getLastAlignmentPairIndex() ; i++)
		{
			AlignmentPair alignmentPair = this.alignment.get(i);
			result += alignmentPair.getSourcePos() + "-" + alignmentPair.getTargetPos() + " ";
		}
		
		if(this.getLastAlignmentPairIndex() >= 0)
		{
			AlignmentPair alignmentPair = this.alignment.get(getLastAlignmentPairIndex());
			result += alignmentPair.getSourcePos() + "-" + alignmentPair.getTargetPos();
		}
		return result;
	}	
	
	public Set<Integer> getAllAlignedSourcePositions()
	{
		Set<Integer> alignedSourcePositions =  new HashSet<Integer>();
		for(AlignmentPair pair : this.alignment)
		{
			alignedSourcePositions.add(pair.getSourcePos());
		}
		return alignedSourcePositions;
	}

	public Set<Integer> getAllAlignedTargetPositions()
	{
		Set<Integer> alignedTargetPositions = new HashSet<Integer>();
		for(AlignmentPair pair : this.alignment)
		{
			alignedTargetPositions.add(pair.getTargetPos());
		}
		return alignedTargetPositions;
	}
	
	
	public int getMaxAlignedSourcePos()
	{
		int result = -1;
		for(AlignmentPair pair : alignment)
		{
			result = Math.max(result, pair.getSourcePos());
		}
		return result;
	}
	
	public int getMaxAlignedTargetPos()
	{
		int result = -1;
		for(AlignmentPair pair : alignment)
		{
			result = Math.max(result, pair.getTargetPos());
		}
		return result;
	}
	
	/**
	 *  The minimum source length - without knowing the associated sentences, looking only at the alignment positions
	 * @return Minimum length for a compatible source String
	 */
	public int getMinCompatibleSourceLengt()
	{
		return getMaxAlignedSourcePos() + 1;
	}
	
	/**
	 *  The minimum target length - without knowing the associated sentences, looking only at the alignment positions
	 * @return Minimum length for a compatible target String
	 */
	public int getMinCompatibleTargetLength()
	{
		return getMaxAlignedTargetPos() + 1;
	}
	
	public String toString()
	{
		String result = "";
		result += "<Alignment>";
		result += getAlignmentAsString();
		result += "</Alignment>";
		return result;
	}
	
}
