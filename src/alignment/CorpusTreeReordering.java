/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import tsg.TSNodeLabel;
import util.ConfigFile;
import util.FileStatistics;
import viewer.TreeFileIterator;
import viewer.TreeStruct;

public class CorpusTreeReordering 
{

	private final NumberFormat nf;
	int numReorderedSentences;
	private boolean ignoreNulls;
	private boolean outputPerSentence;
	private boolean writeReorderingLabels;
	
	private CorpusTreeReordering(NumberFormat nf,boolean ignoreNulls, boolean outputPerSentence, boolean writeReorderingLabels)
	{
		this.nf = nf;
		this.numReorderedSentences = 0;
		this.ignoreNulls = ignoreNulls;
		this.outputPerSentence = outputPerSentence;
		this.writeReorderingLabels = writeReorderingLabels;
	}
	
	
	public static CorpusTreeReordering createCorpusTreeReordering(boolean ignoreNulls,boolean outputPerSentence, boolean  writeReorderingLabels)
	{
		NumberFormat nf  = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		return new CorpusTreeReordering(nf,ignoreNulls,outputPerSentence,  writeReorderingLabels);
	}
	
	
	
	private BufferedReader createAlignmentsReaderSkppingLines(int initialSkip, String oracleAlignmentsFileName) throws IOException
	{
		BufferedReader alignmentsReader = new BufferedReader(new FileReader(oracleAlignmentsFileName));
		
		// Skip also the first initialSkip lines of the alignments file
		for(int i = 0; i < initialSkip; i++)
		{
			alignmentsReader.readLine();
		}
		return alignmentsReader;
	}
	

	/**
	 * Function that writes out a file with the source sentences in reordered form
	 * 
	 * @param stringOutputFileName
	 * @param logFile
	 * @param oracleTreeFileName
	 * @param oracleAlignmentsFileName
	 * @param ignoreNulls
	 * @param initialSkip
	 * @param outputPerSentence
	 */
	public void produceReorderedSourceFile(String  stringOutputFileName,String treeOutputFileName, String logFile, String oracleTreeFileName, String oracleAlignmentsFileName, int initialSkip)
	{
	
		try
		{
			System.out.println("oracleTreeFileName:" + oracleTreeFileName + "\t oracleAlignmentsFileName :" + oracleAlignmentsFileName);
						
			// An iterator over tree-strings
			TreeFileIterator treeIterator = new TreeFileIterator(oracleTreeFileName, initialSkip);
			
			// count the number of sentences by counting the number of lines from the alignments file
			int numSentences = FileStatistics.countLines(oracleAlignmentsFileName) - initialSkip;
			
			BufferedWriter stringOutWriter = new BufferedWriter(new FileWriter(stringOutputFileName));
			BufferedWriter treeOutWriter = new BufferedWriter(new FileWriter(treeOutputFileName));
			BufferedReader alignmentsReader = createAlignmentsReaderSkppingLines(initialSkip, oracleAlignmentsFileName);
			
			String sourceTreeString, alignmentString;
			
			int sentenceNumber = 1;
			
			
			System.out.println("Ready for looping over trees...");
			
			long timeStart = System.currentTimeMillis();
			
			while( ((sourceTreeString = treeIterator.getNextTree()) != "") && ((alignmentString = alignmentsReader.readLine()) != null) )
			{
				
				if(outputPerSentence)
				{	
	
					System.out.print("\nProcessing sentence " + sentenceNumber + "\t");
					System.out.println("source tree String: \n " + sourceTreeString + "\n\n");
					System.out.println("alignmentString: " + alignmentString);
				}
				
				TreeStruct treeStruct = produceReorderedTreeStruct(sourceTreeString, alignmentString);
				writeReorderedSentenceFromReorderedTree(treeStruct, stringOutWriter);
				writeReorderedTree(treeStruct, treeOutWriter,  writeReorderingLabels);
				sentenceNumber++;
				
				printProgress(timeStart, sentenceNumber, numSentences);
					
			}
			writeReorderingLogFile(logFile, stringOutputFileName, sentenceNumber, numReorderedSentences, ignoreNulls);
			
			System.out.println("Finished looping over trees");
			// close the file used by the tree iterator
			treeIterator.closeFile();
			stringOutWriter.close();
			treeOutWriter.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
				
	}
	
	private void writeReorderedSentenceFromReorderedTree(TreeStruct treeStruct, BufferedWriter outWriter) throws IOException
	{
		for(TSNodeLabel node : treeStruct.getLexicals())					
		{
			String word = node.label();
			outWriter.write(word + " ");
		}
		outWriter.write("\n");
	}
	
	private void writeReorderedTree(TreeStruct treeStruct, BufferedWriter outWriter, boolean writeReorderingLabels) throws IOException
	{
		if(writeReorderingLabels)
		{
			outWriter.write(treeStruct.getTree().getTreeWithReorderingInformationString());
		}
		else
		{	
			outWriter.write(treeStruct.getTree().toString());
		}	
		outWriter.write("\n");
	}
	
	
	private TreeStruct produceReorderedTreeStruct(String sourceTreeString, String alignmentString) throws Exception
	{
		//System.out.println("sourceTreeString: " + sourceTreeString);
		TSNodeLabel sourceTree = new TSNodeLabel(sourceTreeString);
		
		//System.out.println("alignmentString: " + alignmentString);
		Alignment alignment = Alignment.createAlignment(alignmentString);
		//System.out.println("Alignment pairs generated ");
		
		TreeStruct treeStruct = new TreeStruct(sourceTree);
		TreeAlignmentStruct treeAlignmentStruct = new TreeAlignmentStruct(treeStruct);
		
		// Set alignments ans compute consistency information
		treeAlignmentStruct.setAlignmentsAndComputeConsistency(alignment);
		
		TreeReordering reordering = new TreeReordering(treeStruct);
		
		boolean orderChanged = reordering.performReordering(ignoreNulls);
		
		if(orderChanged)
		{
			numReorderedSentences++;
			if(outputPerSentence)
			{	
				System.out.println("CorpusTreeReordering orderChanged: " + orderChanged);
			}
		}
		
		return treeStruct;
	}
	
	private void printProgress(long timeStart, int sentenceNumber, int numSentences)
	{
		double percentageCompleted = (sentenceNumber * 100.0) / numSentences;
		// Print progress, format the double with percentage completed with 
		// NumberFormat nf
		
		if(outputPerSentence)
		{	
			System.out.println("Finished " + nf.format(percentageCompleted) + "%");
		}
		else if((sentenceNumber % 2000) == 0)
		{
			long time = System.currentTimeMillis();
			long timePassed = time - timeStart;
			double fractionCompleted = ((double)sentenceNumber) / numSentences;
			double fractionLeft = 1 - fractionCompleted;
			long expectedTimeRemaining = (long) (((fractionLeft / fractionCompleted) * timePassed) / 1000);
			System.out.println("Finished " + nf.format(percentageCompleted) + "%");
			System.out.println("Expected: " + expectedTimeRemaining + " seconds remaining");
		}
	}
	
	private static void writeReorderingLogFile(String logFile, String outputFileName, int sentenceNumber, int numReorderedSentences, boolean ignoreNulls) throws IOException
	{
		BufferedWriter logFileWriter = new BufferedWriter(new FileWriter(logFile));
		logFileWriter.write("<Reorderering log>");
		logFileWriter.write(CorpusTreeReordering.getDateTime()); // write a date/time when the reordering was performed
		logFileWriter.write("\nReeordering type : ignore nulls (=relaxed tree constraint) :" + ignoreNulls);
		logFileWriter.write("\nProduced result file: " + outputFileName);
		logFileWriter.write("\nSentences processed : " + (sentenceNumber - 1));
		logFileWriter.write("\nSentences reordered : " + numReorderedSentences);
		logFileWriter.write("\n</Reorderering log>");
		logFileWriter.close();
	}
	

   public static String getDateTime() 
   {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
   
   public static String getDateIimeWithoutWhiteSpace() 
   {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateTime =  dateFormat.format(date);
        String[] parts = dateTime.split(" ");
        return  parts[0].replaceAll("/", "-") +"#" + parts[1].replaceAll("/", "-");
    }
   
   
   
   private static String configurationOptionsString()
   {
	   String result = "\n\n####### CONFIGURATION OPTIONS: #############\n\n";
	   result += "\nFor the configuration file there are the following settings : " +
			   "\n ### INPUTS  : ###" +
			   "\n - oracleCorpusLocation : the path to the folder were all files are located " +
			   "\n - oracleParseFileName : the name of the file containing the parses/trees to be reordered " +
			   "\n - oracleAlignmentsName : the name of the file containing the alignments used for reordering " +
			   "\n ### OUTPUTS  : ###" +
			   "\n - oracleOutputLocation :  : the path to the folder were output will be generated" +
			   "\n - oracleReorderingStringOutputName : the name of the file were reorderd strings (tree yields) will be written" +
			   "\n - oracleReorderingTreeOutputName : the name of the file were reorderd trees will be written" +
			   "\n - oracleReorderingLogFileName : the name of the file were log reordering information will be written" +   
			   "\n ### SETTINGS  : ###" +
			   "\n - ignoreNulls : whether nulls may be ignored during reordering (default is \"true\" , use \"false\"for more constrained reordering " +
			   "\n  This is the strict version of reordering, that disallows sub-trees that have null aligned words to change their position (but nodes before and after" + 
			   "\n  such nodes are still allowed to changed). This is implemented as sorting all the biggest sublists of the daughter list that have no null-alignment containing" + 
			   "\n node in them." +
			   "\n \n - writeReorderingLabels : Whether the reordering labels should be written (or just the reordered trees) the default is yes " +
			   "\n - outputPerSentence : whether an output line should be written for every sentence. default is yes, can be turned off for large tasks.";
			   
			   return result;
   }
   
   
   private static  void printUsageMessage(String[] args)
   {
	   System.out.println("Wrong usage, uage: java treeReordering configFileName");
		
	   System.out.println("Received args: ");
		int i = 0;
		for(String arg : args)
		{
			System.out.println("arg " + i + " " + arg);
			i++;
		}
		
		System.out.println(configurationOptionsString());
   }
   
	
	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			printUsageMessage(args);
			return;
		}
		
		ConfigFile theConfig;
		try 
		{
			theConfig = new ConfigFile(args[0]);
			
			// Input locations
			String corpusLocation =  theConfig.getValue("oracleCorpusLocation");
			String oracleAlignmentsFileName  = corpusLocation  +  theConfig.getValue("oracleAlignmentsName");
			String oracleParseFileName  =  corpusLocation  + theConfig.getValue("oracleParseFileName");
			// Output locations
			String oracleOutputLocation = theConfig.getValue("oracleOutputLocation"); 
			String stringOutputFileName =  oracleOutputLocation + theConfig.getValue("oracleReorderingStringOutputName");
			String treeOutputFileName =  oracleOutputLocation + theConfig.getValue("oracleReorderingTreeOutputName");
			String logFileName =  oracleOutputLocation + theConfig.getValue("oracleReorderingLogFileName");
			
			String ignoreNullsString = theConfig.getStringIfPresentOrReturnDefault("ignoreNulls", "true").toLowerCase();
			boolean ignoreNulls = ignoreNullsString.equals("true");
			
			String writeReorderingLabelsString = theConfig.getStringIfPresentOrReturnDefault("writeReorderingLabels", "true").toLowerCase();
			boolean writeReorderingLabels =  writeReorderingLabelsString.equals("true");
			
			String outputPerSentenceString = theConfig.getStringIfPresentOrReturnDefault("outputPerSentence", "true").toLowerCase();
			boolean outputPerSentence =  outputPerSentenceString.equals("true");
			
			
			// Try to read initialSkip from the configfile
			String skipString =  theConfig.getValue("initialSkip");
			
			int initialSkip = 0;
			
			// if initalSkip was found, extract its value, otherwise keep it 0
			if(skipString != null)
			{	
				initialSkip = Integer.parseInt(skipString);
			}	
			
			CorpusTreeReordering corpusTreeReordering = CorpusTreeReordering.createCorpusTreeReordering(ignoreNulls, outputPerSentence,writeReorderingLabels);
			corpusTreeReordering.produceReorderedSourceFile(stringOutputFileName,treeOutputFileName,logFileName, oracleParseFileName, oracleAlignmentsFileName, initialSkip);
		
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println("Oracle File not readable");
		}
	}
	
}
