/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.FileUtil;
import alignment.AlignmentTriple;
import alignment.ConfigFileCreater;
import junit.framework.Assert;
import alignment.ArtificialCorpusProperties;

public class ArtificialCorpusCreator {

	private List<AlignmentTriple> alignmentTriples;
	private final List<String> targetParses;
	private BufferedWriter sourceWriter, targetWriter, alignmentWriter, sourceTestFilterWriter, targetParseFileWriter;
	private final ConfigFileCreater configFileCreater;
	protected final static String GrammarSourceFilterFileName = "grammerSourceFilterFile.txt";
	protected final static String TargetParseProperty = "targetParseFileName";
	protected final static String TargetParseFileName = "targetParseFile.txt";
	protected final String testCorpusFolderName;

	protected ArtificialCorpusCreator(List<AlignmentTriple> alignmentTriples, BufferedWriter sourceWriter, BufferedWriter targetWriter,
			BufferedWriter alignmentWriter, BufferedWriter sourceTestFilterWriter, ConfigFileCreater configFileCreater, BufferedWriter targetParseFileWriter,
			List<String> targetParses, String testCorpusFolderName) {
		this.alignmentTriples = alignmentTriples;
		this.sourceWriter = sourceWriter;
		this.targetWriter = targetWriter;
		this.alignmentWriter = alignmentWriter;
		this.sourceTestFilterWriter = sourceTestFilterWriter;
		this.targetParseFileWriter = targetParseFileWriter;
		this.configFileCreater = configFileCreater;
		this.targetParses = targetParses;
		this.testCorpusFolderName = testCorpusFolderName;
	}

	private static ConfigFileCreater createConfigFileCreaterAlignmentOnly(String testCorpusFolderName) {
		ConfigFileCreater configFileCreater = ConfigFileCreater.createStandardConfigFileCreaterWithCasedFiles(testCorpusFolderName);
		return configFileCreater;
	}

	protected static ArtificialCorpusCreator createArtificialCorpusCreator(ConfigFileCreater configFileCreater, List<AlignmentTriple> alignmentTriples,
			List<String> targetParses, String testCorpusFolderName) throws IOException {

		String sourceFileName = testCorpusFolderName + configFileCreater.getSourceFileName();
		String targetFileName = testCorpusFolderName + configFileCreater.getTargetFileName();
		String alignmentsFileName = testCorpusFolderName + configFileCreater.getAlignmentsFileName();

		BufferedWriter targetParseWriter = null;
		if (targetParses != null) {
			String targetParseFileName = testCorpusFolderName + configFileCreater.getTargetParseFileName();
			targetParseWriter = createWriter(targetParseFileName);
		}
		String grammerSourceFilterFile = testCorpusFolderName + GrammarSourceFilterFileName;

		ArtificialCorpusCreator result = new ArtificialCorpusCreator(alignmentTriples, createWriter(sourceFileName), createWriter(targetFileName),
				createWriter(alignmentsFileName), createWriter(grammerSourceFilterFile), configFileCreater, targetParseWriter, targetParses,
				testCorpusFolderName);
		result.writeCorpus();
		return result;
	}

	protected static ArtificialCorpusCreator createArtificialCorpusCreator(List<AlignmentTriple> alignmentTriples, List<String> targetParses,
			String testCorpusFolderName) throws IOException {

		FileUtil.createFolderIfNotExisting(testCorpusFolderName);
		ConfigFileCreater configFileCreater = createConfigFileCreaterAlignmentOnly(testCorpusFolderName);
		return createArtificialCorpusCreator(configFileCreater, alignmentTriples, targetParses, testCorpusFolderName);
	}

	public static ArtificialCorpusCreator createArtificialCorpusCreator(ArtificialCorpusProperties artificialCorpusProperties, String testCorpusFolderName) throws IOException {
		List<AlignmentTriple> alignmentTriples = ArtificialCorpusCreator.createAlignemntTriples(artificialCorpusProperties);
		return createArtificialCorpusCreator(alignmentTriples, null, testCorpusFolderName);
	}

	protected static List<AlignmentTriple> createAlignemntTriples(ArtificialCorpusProperties artificialCorpusProperties) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		result.addAll(createMonontoneAlignments(artificialCorpusProperties.noMonotoneAlignments, artificialCorpusProperties.monotoneLength));
		result.addAll(createFullyConnectedAlignments(artificialCorpusProperties.noFullConnectedAlignments,
				artificialCorpusProperties.fullConnectedAlignmentLength));
		result.addAll(createFourPermutationAlignments(artificialCorpusProperties.noFourPermutationAlignments,
				artificialCorpusProperties.fourPermutationAlignmentGroups));
		result.addAll(createLenghtOneAlignments(artificialCorpusProperties.noLengthOneAlignments));
		result.addAll(createNoInternalNodesAlignments(artificialCorpusProperties.noNoInternalNodesAlignments,
				artificialCorpusProperties.noNoInternalNodesLength));
		return result;
	}

	private static BufferedWriter createWriter(String fileName) throws IOException {
		return new BufferedWriter(new FileWriter(fileName));
	}

	private static List<AlignmentTriple> createNoInternalNodesAlignments(int number, int length) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		for (int i = 0; i < number; i++) {
			result.add(AlignmentTriple.createNoInternalNodesAlignment(length));
		}
		return result;
	}

	private static List<AlignmentTriple> createMonontoneAlignments(int number, int length) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		for (int i = 0; i < number; i++) {
			result.add(AlignmentTriple.createMonontoneAlignmentTriple(length));
		}
		return result;
	}

	private static List<AlignmentTriple> createLenghtOneAlignments(int number) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		for (int i = 0; i < number; i++) {
			result.add(AlignmentTriple.createLengthOneAlignment());
		}
		return result;
	}

	private static List<AlignmentTriple> createFullyConnectedAlignments(int number, int length) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		for (int i = 0; i < number; i++) {
			result.add(AlignmentTriple.createFullyConnectedAlignmentTriple(length));
		}
		return result;
	}

	private static List<AlignmentTriple> createFourPermutationAlignments(int number, int noGroups) {
		List<AlignmentTriple> result = new ArrayList<AlignmentTriple>();

		for (int i = 0; i < number; i++) {
			result.add(AlignmentTriple.createFourPermutationAlignmentTriple(noGroups));
		}
		return result;
	}

	private void writeSourceSentences() throws IOException {
		for (AlignmentTriple alignmentTriple : this.alignmentTriples) {
			sourceWriter.write((alignmentTriple.getSourceSentenceAsString() + "\n").toLowerCase());
		}
	}

	private void writeGrammarSourceFilter() throws IOException {
		// only the first source sentence is used
		this.sourceTestFilterWriter.write((this.alignmentTriples.get(0).getSourceSentenceAsString() + "\n").toLowerCase());
	}

	private void writeTargetSentences() throws IOException {
		for (AlignmentTriple alignmentTriple : this.alignmentTriples) {
			targetWriter.write((alignmentTriple.getTargetSentenceAsString() + "\n").toLowerCase());
		}
	}

	private boolean hasCasedTargetSentences() {
		return (this.configFileCreater.getTargetFileNameCased() != null);
	}

	private boolean hasCasedSourceSentences() {
		return (this.configFileCreater.getSourceFileNameCased() != null);
	}

	private void writeSourcetSentencesCasedIfPresent() throws IOException {
		Assert.assertTrue(hasCasedSourceSentences());
		if (hasCasedSourceSentences()) {
			System.out.println("hasCasedSourceSentences!!!");
			BufferedWriter sourceCasedWriter = createWriter(testCorpusFolderName + this.configFileCreater.getSourceFileNameCased());
			for (AlignmentTriple alignmentTriple : this.alignmentTriples) {
				sourceCasedWriter.write(alignmentTriple.getTargetSentenceAsString() + "\n");
			}
			sourceCasedWriter.close();
		}
	}

	private void writeTargetSentencesCasedIfPresent() throws IOException {
		if (hasCasedTargetSentences()) {
			BufferedWriter targetCasedWriter = createWriter(testCorpusFolderName + this.configFileCreater.getTargetFileNameCased());
			for (AlignmentTriple alignmentTriple : this.alignmentTriples) {
				targetCasedWriter.write(alignmentTriple.getTargetSentenceAsString() + "\n");
			}
			targetCasedWriter.close();
		}
	}

	private void writeAlignments() throws IOException {
		for (AlignmentTriple alignmentTriple : this.alignmentTriples) {
			alignmentWriter.write(alignmentTriple.getAlignmentAsString() + "\n");
		}
	}

	private void writeConfigFile() throws IOException {
		configFileCreater.writeConfigFile();
	}

	private void writeTargetParses() throws IOException {
		if ((targetParseFileWriter != null)) {
			assert (targetParses != null);
			for (String parse : targetParses) {
				targetParseFileWriter.write(parse + "\n");
			}
		}
	}

	public void writeCorpus() throws IOException {
		System.out.println(">>>Writing the corpus");
		writeSourceSentences();
		writeTargetSentences();
		writeAlignments();
		writeConfigFile();
		writeGrammarSourceFilter();
		writeTargetParses();
		sourceWriter.close();
		targetWriter.close();
		alignmentWriter.close();
		sourceTestFilterWriter.close();

		if (targetParseFileWriter != null) {
			targetParseFileWriter.close();
		}

		writeSourcetSentencesCasedIfPresent();
		writeTargetSentencesCasedIfPresent();

	}

}
