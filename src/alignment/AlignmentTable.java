/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import tsg.TSNodeLabel;
import viewer.TreeStruct;


// Class that stores the Alignment information
public class AlignmentTable 
{

	// The alignments stored as a mapping from source to target positions
	private IdentityHashMap<Integer, ArrayList<Integer>> alignmentMappingTable;;
	// The alignments stored as a (backward) mapping from  target to source positions
	private IdentityHashMap<Integer, ArrayList<Integer>> alignmentBackwardsMappingTable;
	
	// Default Constructor
	public AlignmentTable () {}
	



	public void setAlignments(Alignment sentenceAlignments, int noSourceWords)
	{
		// Set up a mapping table that maps source to target positions, for the available alignments
		setAlignmentMappingTable(new IdentityHashMap<Integer, ArrayList<Integer>>());
		setAlignmentBackwardsMappingTable(new IdentityHashMap<Integer, ArrayList<Integer>>());
		for(AlignmentPair ap : sentenceAlignments.getAlignmentPairs())
		{
			int tPos = ap.getTargetPos(); 
			int sPos =  ap.getSourcePos();
										
		
			// Insert tuple into alignmentMappingTable
			addHashLookupEntry(getAlignmentMappingTable(), sPos, tPos);
								
			// Insert tuple into alignmentBackwardMappingTable
			addHashLookupEntry(getAlignmentBackwardsMappingTable(), tPos, sPos);
			
		
		}
	}
	
	public void enrichTreeWithAlignments(TreeStruct treeStruct)
	{
		if(treeStruct.getTree() != null)
		{
		
			// Go over the lexical nodes of the tree and enrich them with alignment information
			for(int i = 0; i <  treeStruct.getNumLexicalItems(); i++)
			{	
					TSNodeLabel sourceWordNode = treeStruct.getLexicalItem(i);
					
					if(getAlignmentMappingTable().containsKey(i))
					{	
						sourceWordNode.targetAlignments = getAlignmentMappingTable().get(i);
					}
					else
					{
						sourceWordNode.targetAlignments = null;
					}
			}
		
		}
		
	}
	
	public <T> void addHashLookupEntry(IdentityHashMap<T,ArrayList<T>> table, T pos1, T pos2)
	{
		if(table.containsKey(pos1))
		{
			ArrayList<T> mappings = table.get(pos1);
			mappings.add(pos2);
		}
		else
		{	
			ArrayList<T>  mappings  = new ArrayList<T>();
			mappings.add(pos2);
			table.put(pos1, mappings);
		}	
		
	}




	public void setAlignmentBackwardsMappingTable(
			IdentityHashMap<Integer, ArrayList<Integer>> alignmentBackwardsMappingTable) {
		this.alignmentBackwardsMappingTable = alignmentBackwardsMappingTable;
	}




	public IdentityHashMap<Integer, ArrayList<Integer>> getAlignmentBackwardsMappingTable() {
		return alignmentBackwardsMappingTable;
	}




	public void setAlignmentMappingTable(IdentityHashMap<Integer, ArrayList<Integer>> alignmentMappingTable) {
		this.alignmentMappingTable = alignmentMappingTable;
	}




	public IdentityHashMap<Integer, ArrayList<Integer>> getAlignmentMappingTable() {
		return alignmentMappingTable;
	}
	
}
