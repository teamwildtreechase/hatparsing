/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.ArrayList;
import tsg.TSNodeLabel;
import viewer.TreeStruct;

/**
 * This class stores a tree and associated alignment information and operations
 * @author gemaille
 *
 */
public class TreeAlignmentStruct 
{
	private AlignmentTable  alignmentTable;
	TreeStruct theTreeStruct;
	
	public TreeAlignmentStruct(TreeStruct theTreeStruct)
	{
		this.theTreeStruct = theTreeStruct;
		alignmentTable = new AlignmentTable();
	}
	
	/**
	 *  Interface method that can be called to set the alignments in the tables as well as in the 
	 *  trees.
	 * @param sentenceAlignments : the ArrayList of Alignmentpairs containing the alignments
	 */
	public void setAlignments(Alignment sentenceAlignments)
	{
		//System.out.println("sentenceAlignments: " + sentenceAlignments);
		//System.out.println("theTreeStruct: " + theTreeStruct);
		
		int noSourceWords = theTreeStruct.getTree().getTerminalDescendants().size();
		alignmentTable.setAlignments(sentenceAlignments,noSourceWords);
		alignmentTable.enrichTreeWithAlignments(theTreeStruct);
	}
	
	
	public void setAlignmentsAndComputeConsistency(Alignment sentenceAlignments)
	{
		 setAlignments(sentenceAlignments);
		 
		// Generate an alignmentConsistency checker for this AlignmentTreePanel
		AlignmentConsistencyChecker acc = new AlignmentConsistencyChecker(this);
		// Check and set alignmentViolations for all the nodes in the source tree
		acc.setAlignmentViolations();
	}
	
	public 	ArrayList<Integer> getBackwardAlignmentsForPos(int i)
	{
		return alignmentTable.getAlignmentBackwardsMappingTable().get(i);
	}
			
	/**
	 *  Getter for the alignment position
	 * @param i : the position
	 * @return ArrayList<Integer> of target alignment positions
	 */
	public 	ArrayList<Integer> getAlignmentsForPos(int i)
	{
		return alignmentTable.getAlignmentMappingTable().get(i);
	}
		
	// Method that gets the target alignment position of a certain source word
	public ArrayList<Integer>  getTargetAlignments(TSNodeLabel node)
	{
		return node.targetAlignments;	
	}
	
	public int[] subtreeAlignmentInformation(TSNodeLabel node)
	{
		int[] alignmentInfo = new int[4];
		
		// two variables that keep track of the minimum and maximum alignment position of the 
		// the descendants of the node
		int min = Integer.MAX_VALUE;
		int max = -1;
		
		int nullAlignedTerminals = 0;
		
		ArrayList<TSNodeLabel> terminalDescendants = node.getTerminalDescendants();
		
		for(TSNodeLabel descendant : terminalDescendants)
		{
			ArrayList<Integer> targetAlignments =  getTargetAlignments(descendant);
			
			// if the source word has an alignment in the target string
			// (ohterwise getTargetAlignment will return null)
			if(targetAlignments != null)				
			{	
				// loop over the possibly multiple target alignment positions of this source leaf node
				for(int targetAlignment : targetAlignments)
				{	
				
					if(targetAlignment < min)
					{
						min = targetAlignment;
					}
					if(targetAlignment > max)
					{
						max = targetAlignment;
					}
					
				}
				
			}
			else
			{
				nullAlignedTerminals++;
			}	
				
			
			
		}
		
		// The fist two attributes we are interested in are the minimum and maximum alignment target
		// positions over descendants rooted at this node
		alignmentInfo[0] = min;
		alignmentInfo[1] = max;
		// the third attribute we are interested in is the number of terminal descendants under the node
		alignmentInfo[2] = terminalDescendants.size();
		
		
		// The fourt attribute we are interested in is the number of unaligned terminal nodes that are 
		// governed by the node
		alignmentInfo[3] = nullAlignedTerminals;
		
 		return alignmentInfo;
	}
	
}
