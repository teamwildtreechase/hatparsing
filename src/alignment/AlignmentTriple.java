/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import util.InputReading;

public class AlignmentTriple {
	public static final String NullWordString = "###NULL###";

	private List<String> sourceSentence;
	private List<String> targetSentence;
	private Alignment alignment;

	private AlignmentTriple(List<String> sourceSentence, List<String> targetSentence, Alignment alignment) {
		this.sourceSentence = sourceSentence;
		this.targetSentence = targetSentence;
		this.alignment = alignment;
	}

	public static AlignmentTriple createAlignmentTriple(String sourceString, String targetString, String alignmentString) {
		List<String> sourceSentence = InputReading.getWordsFromString(sourceString);
		List<String> targetSentence = InputReading.getWordsFromString(targetString);

		// System.out.println("sourceString: " + sourceString + "targetString: "
		// + targetString +" alignmentString: " + alignmentString);
		Alignment alignment = Alignment.createAlignment(alignmentString);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createNullExtendedAlignmentTriple(String sourceString, String targetString, String alignmentString) {
		List<String> sourceSentence = InputReading.getWordsFromString(sourceString);
		List<String> extendedTargetSentence = getNullExtendedTargetSentence(InputReading.getWordsFromString(targetString));

		// System.out.println("sourceString: " + sourceString + "targetString: "
		// + targetString +" alignmentString: " + alignmentString);
		Alignment extendedAlignment = getNullExtendedAlignment(alignmentString, sourceSentence.size(), extendedTargetSentence.size() - 1);
		return new AlignmentTriple(sourceSentence, extendedTargetSentence, extendedAlignment);
	}

	private static List<String> getNullExtendedTargetSentence(List<String> targetSentence) {
		List<String> result = new ArrayList<String>(targetSentence);
		result.add(NullWordString);
		return result;
	}

	private static Alignment getNullExtendedAlignment(String alignmentString, int sourceLength, int originalTargetLength) {
		Alignment basicAlignment = Alignment.createAlignment(alignmentString);
		Alignment extendedAlignment = Alignment.createAlignment(alignmentString);

		for (int i : basicAlignment.getUnalignedSourcePositions(sourceLength)) {
			extendedAlignment.add(new AlignmentPair(i, originalTargetLength));
		}

		return extendedAlignment;
	}

	public static AlignmentTriple createMonontoneAlignmentTriple(int length) {
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(length);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(length);
		Alignment alignment = createMonotoneAlignment(length);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createFullyInvertedAlignmentTriple(int length) {
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(length);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(length);
		Alignment alignment = createFullyInvertedAlignment(length);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createLengthOneAlignment() {
		return createMonontoneAlignmentTriple(1);
	}

	public static AlignmentTriple createFullyConnectedAlignmentTriple(int length) {
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(length);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(length);
		Alignment alignment = createFullyConnectedAlignment(length);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createFourPermutationAlignmentTriple(int noGroups) {
		int length = noGroups * 4;
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(length);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(length);
		Alignment alignment = createFourPermutationAlignment(noGroups);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createNoInternalNodesAlignment(int length) {
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(length);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(length);
		Alignment alignment = createNoInternalNodesAlignment();
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static AlignmentTriple createHalfConnectedSourceAlignmentTriple(int noGroups) {
		List<String> sourceSentence = ArtificialSentenceCreater.createArtificialSourceSentence(noGroups * 2);
		List<String> targetSentence = ArtificialSentenceCreater.createArtificialTargetSentence(noGroups * 2);
		Alignment alignment = createHalfConnectedSourceAlignment(noGroups);
		return new AlignmentTriple(sourceSentence, targetSentence, alignment);
	}

	public static String createAlignmentString(List<AlignmentPair> alignment) {
		String result = "";
		for (AlignmentPair pair : alignment) {
			result += pair.getSourcePos() + "-" + pair.getTargetPos() + " ";
		}

		result = result.trim();

		return result;

	}

	public static String computeInvertedAlignmentString(String alignmentString) {
		Alignment invertedAlignment = Alignment.createInvertedAlignment(Alignment.createAlignment(alignmentString));

		return invertedAlignment.getAlignmentAsString();
	}

	public static String createMonotoneAlignmentString(int length) {
		Alignment monotoneAlignment = createMonotoneAlignment(length);
		return monotoneAlignment.getAlignmentAsString();
	}

	private static Alignment createMonotoneAlignment(int length) {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		for (int i = 0; i < length; i++) {
			alignmentPairs.add(new AlignmentPair(i, i));
		}

		return Alignment.createAlignment(alignmentPairs);
	}

	private static Alignment createFullyInvertedAlignment(int length) {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		for (int i = 0; i < length; i++) {
			alignmentPairs.add(new AlignmentPair(i, ((length - 1) - i)));
		}

		return Alignment.createAlignment(alignmentPairs);
	}

	private static Alignment createHalfConnectedSourceAlignment(int noGroups) {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		int group, i;
		for (group = 0; group < noGroups - 1; group++) {
			i = group * 2;
			alignmentPairs.add(new AlignmentPair(i, i));
			alignmentPairs.add(new AlignmentPair(i, i + 1));
		}
		i = group * 2;
		alignmentPairs.add(new AlignmentPair(i + 1, i));
		alignmentPairs.add(new AlignmentPair(i + 1, i + 1));

		return Alignment.createAlignment(alignmentPairs);
	}

	private static Alignment createFullyConnectedAlignment(int length) {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j++) {
				alignmentPairs.add(new AlignmentPair(i, j));
			}
		}

		return Alignment.createAlignment(alignmentPairs);
	}

	private static Alignment createFourPermutationAlignment(int noGroups) {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();

		for (int groupNo = 0; groupNo < noGroups; groupNo++) {
			int offSet = groupNo * 4;

			alignmentPairs.add(new AlignmentPair(offSet, offSet + 2));
			alignmentPairs.add(new AlignmentPair(offSet + 1, offSet));
			alignmentPairs.add(new AlignmentPair(offSet + 2, offSet + 3));
			alignmentPairs.add(new AlignmentPair(offSet + 3, offSet + 1));
		}

		return Alignment.createAlignment(alignmentPairs);
	}

	public static Alignment createNoInternalNodesAlignment() {
		List<AlignmentPair> alignmentPairs = new ArrayList<AlignmentPair>();
		alignmentPairs.add(new AlignmentPair(0, 0));
		return Alignment.createAlignment(alignmentPairs);
	}

	public List<String> getSourceSentence() {
		return this.sourceSentence;
	}

	public String getSourceSentenceAsString() {
		String result = "";
		for (String word : this.sourceSentence) {
			result += word + " ";
		}
		return result;
	}

	public String getTargetSentenceAsString() {
		String result = "";
		for (String word : this.targetSentence) {
			result += word + " ";
		}
		return result;
	}

	public List<String> getTargetSentence() {
		return this.targetSentence;
	}

	public Alignment getAlignment() {
		return this.alignment;
	}

	public String getAlignmentAsString() {
		return this.alignment.getAlignmentAsString();
	}

	public int getSourceLength() {
		return this.sourceSentence.size();
	}

	public int getTargetLength() {
		return this.targetSentence.size();
	}

	private Set<Integer> getAllSourcePositions() {
		Set<Integer> allSourcePositions = new HashSet<Integer>();
		for (int i = 0; i < this.sourceSentence.size(); i++) {
			allSourcePositions.add(i);
		}
		return allSourcePositions;
	}

	private Set<Integer> getAllTargetPositions() {
		Set<Integer> allTargetPositions = new HashSet<Integer>();
		for (int i = 0; i < this.targetSentence.size(); i++) {
			allTargetPositions.add(i);
		}
		return allTargetPositions;
	}

	private Set<Integer> getAllAlignedSourcePositions() {
		return this.alignment.getAllAlignedSourcePositions();
	}

	private Set<Integer> getAllAlignedTargetPositions() {
		return this.alignment.getAllAlignedTargetPositions();
	}

	public boolean hasUnalignedSourcePositions() {
		Set<Integer> unalignedSourcePositions = getAllSourcePositions();
		unalignedSourcePositions.removeAll(getAllAlignedSourcePositions());
		return !(unalignedSourcePositions.isEmpty());
	}

	public boolean hasUnalignedTargetPositions() {
		Set<Integer> unalignedTargetPositions = getAllTargetPositions();
		unalignedTargetPositions.removeAll(getAllAlignedTargetPositions());
		return !(unalignedTargetPositions.isEmpty());
	}

	public String toString() {
		String result = "AlignmentTriple";
		result += "\n" + this.sourceSentence;
		result += "\n" + this.targetSentence;
		result += "\n" + this.alignment;
		return result;
	}

}
