/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */

package alignment;

import java.util.ArrayList;
import tsg.TSNodeLabel;


/** 
 * Class containing methods for checking consistency of Alignments under different assumptions 
 *  i.e. generalized ITG constraints (only permutation of children in tree allowed) 
 */ 

public class AlignmentConsistencyChecker 
{
	//AlignmentTreePanel atp;
	TreeAlignmentStruct theTreeAlignmentStruct;
	
	public AlignmentConsistencyChecker(TreeAlignmentStruct theTreeAlignmentStruct)
	{
		this.theTreeAlignmentStruct =  theTreeAlignmentStruct;
	}
	
	/** Method that loops over all the nodes in the tree and calls method 
	 * findSubtreeViolations to set the alignment violations for them
	 */
	
	public void setAlignmentViolations()
	{
		for(TSNodeLabel node: theTreeAlignmentStruct.theTreeStruct.getNodesArray())
		{			
			findSubtreeViolations(node);
			
			// if the list of violators that has been found is nonempty, then set 
			// rootsTreeAlignmentViolation to true
			node.rootsTreeAlignmentViolation =   (node.alignmentViolaters.size() > 0);
			
		}
	}
	


	
	/** Method that finds and set subtree violations for a node. The method first looks up the
	 * alignmentspan of the node. Then it uses the alignmentBackwardsMappingTable to find all 
	 * source nodes that align to this range. If any of these aligning source nodes do not belong 
	 * to the subtree rooted at node, then they are marked as alignment violators and added to the 
	 * violators list of node. Also the boolean of violation is then set for these violating nodes
	 * @param node the source tree node to find the subtree violations for
	 */	
	public void findSubtreeViolations(TSNodeLabel node)
	{
		boolean debugOutput = false;
		
		
		int[] alignmentInfo = theTreeAlignmentStruct.subtreeAlignmentInformation(node);
		// extract the found minimum and maximum alignment and store in the properties of the node
		int minAlignment =  node.spanMin =  alignmentInfo[0];
		int maxAlignment =  node.spanMax =alignmentInfo[1];
		
		// Set the number of unaligned governed terminals in the node
		int numUnalignedGovernedterminals = alignmentInfo[3];
		node.setNumUnalignedGovernedTerminals(numUnalignedGovernedterminals);
		
		node.alignmentViolaters = new ArrayList<TSNodeLabel>();
		
		
		/*
		if(node.toStringOneLevel().contains("NP NP PP"))
		{
			System.out.println("Found interesting node, print debug info");
			debugOutput = true;
		}
		*/
		
		// loop over the target alignment range "claimed" by the subtree rooted at node
		for(int i = minAlignment;i <= maxAlignment;  i++ )
		{
			// get the originating source position
			
			ArrayList<Integer> sourceAlignments = theTreeAlignmentStruct.getBackwardAlignmentsForPos(i);
			
			
			if(sourceAlignments != null)
			{
				for(int sourcePos : sourceAlignments)
				{		
					TSNodeLabel sourceNode = theTreeAlignmentStruct.theTreeStruct.getLexicalItem(sourcePos);
					
					if(debugOutput)
					{
						System.out.println("Source range aligned node:" + sourceNode.toStringOneLevel());
					}
					
					// Now we have to check whether this source node is part of the set of terminal nodes
					// belonging to the tree rooted at node
					if (!node.rootsNode(sourceNode))
					{
						sourceNode.violatesSourceSubtreeAlignment = true;
						// add sourceNode to the list of alignment violators of node
						node.alignmentViolaters.add(sourceNode);
						
						if(debugOutput)
						{
							System.out.println("Node not contained in subtree and causing violation");
						}
					}
					
				}
			}
		}
			
	}
	
	
	/*
	public boolean rootsTreeAlignmentViolation(TSNodeLabel node, boolean ignoreUnalignedWords)
	{
	
		// If the node is terminal or has only one child, there can be no constraint violation at this level in either case
		if(node.isTerminal() ||( node.daughters.length < 2))
		{
			return false;
		}
		
		int[] alignmentInfo = subtreeAlignmentInformation(node);
		int minAlignment =  alignmentInfo[0];
		int maxAlignment =  alignmentInfo[1];
		int noDescendants =  alignmentInfo[2];
		
		int targetAlignmentSpan = (maxAlignment - minAlignment) + 1;
		
		System.out.println(">> node:" + node.label() + "[min,max]: ["+ minAlignment +"," + maxAlignment + "] targetAlignmentSpan: + " + targetAlignmentSpan + " noDescendants: " + noDescendants);
		
		if(ignoreUnalignedWords)
		{
			int unalignedWords = countUnalignedWords(minAlignment,maxAlignment);
			targetAlignmentSpan = targetAlignmentSpan - unalignedWords;
			
			System.out.println("Unaligned words: " + unalignedWords);
		}
		
		// If the alignment span in the target is bigger than the number of descendants, this 
		// means the alignment can not be achieved by simply permuting the children rooted at node
		// and thus we have a so called "tree alignment violation"
		if(targetAlignmentSpan > noDescendants)
		{
			return true;
		}
	
		return false;
	}
	*/
	
	/**
	 * Function that counts the number of un-aligned words over a target range,
	 * by checking for every target position in the range whether an alignment entry 
	 * is stored for it in the alignmentBackwardsMappingTable
	 * 
	 * @param targetMinPos the lower bound of the target range
	 * @param targetMaxPos the upper bound of the target range
	 */
	public int countUnalignedWords(int targetMinPos, int targetMaxPos)
	{
		int unalignedWords = 0;
		for(int i = targetMinPos; i <= targetMaxPos; i++)
		{
			ArrayList<Integer> alignments = theTreeAlignmentStruct.getBackwardAlignmentsForPos(i);
			if(alignments == null)
			{
				unalignedWords++;
			}
			
		}
		
		return unalignedWords;
	}
	

	

	
	
	

}
