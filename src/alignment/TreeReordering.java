/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.Arrays;
import java.util.Comparator;
import java.util.IdentityHashMap;
import tsg.TSNodeLabel;
import viewer.TreeStruct;

/**
 * Class that takes care of reordering of trees
 * @author gemaille
 *
 */
public class TreeReordering 
{
	TreeStruct theTreeStruct;
	
	
	public TreeReordering(TreeStruct theTreeStruct)
	{
		this.theTreeStruct = theTreeStruct;
	}
	
	
	boolean isValildReordering(int[] newOrder,int numElements)
	{
		// fist basic check of consistency: the number of elements in the order must be 
		// equal to the number of elements we want to reorder
		if(newOrder.length != numElements)
		{
			return false;
		}
		else
		{
			int[] coveredIndices = new int[numElements];
			
			for(int i = 0; i < numElements; i++)
			{
				int index = newOrder[i];
				
				//check if index out of bounds
				if((index >= numElements) || (index < 0))
				{
					// out of bound index: not a valid reordering
					return false;
				}
				else
				{
					// set the value of of this index in coveredIndices to 1
					coveredIndices[index] = 1;
				}
					
			}
			int numCoveredIndices = 0;
			for(int i = 0; i < numElements; i++)
			{
				numCoveredIndices += coveredIndices[i];
			}
			
			// If everything is allright this implies that all indices are covered or equivalently 
			// the number of covered indices matches the number of elements
			if(numCoveredIndices == numElements)
			{
				return true;
			}
		}
		
		// Something is wrong so return false
		return false;
	}
	
	void invertChildrenOrder(TSNodeLabel n)
	{
		int numRootChildren = n.daughters.length;
		int[] newOrder = new int[numRootChildren];
		for(int i =0; i < numRootChildren; i++)
		{
			newOrder[i] = (numRootChildren-1) - i;
		}
		try{
			reorderChild(n, newOrder);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	
	
	// Function that reorders the child nodes of the input node and puts them in the order given by the
	// supplied order int[]
	void reorderChild(TSNodeLabel n, int[] newOrder) throws Exception
	{
		
		/*
		// Basic sanity check: the number of indices in the new order must match the number of daughters
		if(n.daughters.length != newOrder.length)
		{
			throw new Exception();
		}
		*/
		
		// perform thorough check of valid order
		if(!isValildReordering(newOrder,n.daughters.length))
		{
			throw new Exception("Invalid order specification!");
		}
		
		TSNodeLabel[] newlyOrderedDaughters = new TSNodeLabel[n.daughters.length];
		
		for(int i = 0; i < newOrder.length; i++)
		{
			int newIthDaughter = newOrder[i];
			newlyOrderedDaughters[i] = n.daughters[newIthDaughter];
		}	
		
		// replace daughers by reordered daughters
		n.daughters = newlyOrderedDaughters;
		
	}

	private IdentityHashMap<TSNodeLabel,Integer> createOriginalDaughterIndicesTable(TSNodeLabel node)
	{
		IdentityHashMap<TSNodeLabel,Integer> daughterIndices = new IdentityHashMap<TSNodeLabel, Integer>();
		for(int i = 0; i < node.getNoDaughters(); i++)
		{
			daughterIndices.put(node.daughters[i],i);
		}
		return daughterIndices;
	}
	
	private void sortDaughterNodes(TSNodeLabel node, boolean ignoreNulls)
	{
		Comparator<TSNodeLabel> spanComparator = new AlignmentSpanComparator();
		if(ignoreNulls)
		{	//This is the normal sorting associated with the relaxed reordering 
			// following the alignments under tree constraints
			Arrays.sort(node.daughters,spanComparator);
		}
		else
		{
			// This is the strict version of reordering, that disallows sub-trees that 
			// have null aligned words to change their position (but nodes before and after 
			// such nodes are still allowed to changed). This is implemented as sorting all 
			// the biggest sublists of the daughter list that have no null-alignment containing 
			// node in them.
			nonNullAlignedNodeMovingSort(node);
		}
	}
	
	private boolean determinePostSortingIndicesAndOrderChangeProperties(TSNodeLabel node, IdentityHashMap<TSNodeLabel,Integer> daughterIndices)
	{
		node.relativeChildOrder = new int[node.getNoDaughters()];
	
		node.childOrderChanged = false;
		
		for(int i = 0; i < node.getNoDaughters(); i++)
		{
			node.relativeChildOrder[i] = daughterIndices.get(node.daughters[i]);
			if(node.relativeChildOrder[i] != i)
			{
				node.daughters[i].ownParentRelativePositionChanged = true;
				node.childOrderChanged = true;
			}
		}
		
		node.subtreeReordered = true;
		
		return node.childOrderChanged;
	}
	
	public boolean reorderChildrenToMatchAlignmentOrder(TSNodeLabel node, boolean ignoreNulls)
	{
		// Create a map specifying the positions of the daughters before sorting
		IdentityHashMap<TSNodeLabel,Integer> daughterIndices = createOriginalDaughterIndicesTable(node);
		// Sort the daughters based on their target position bindings
		sortDaughterNodes(node, ignoreNulls);
		// determine the post sorting indices using the original indices of the daughters
		boolean orderChanged = determinePostSortingIndicesAndOrderChangeProperties(node,daughterIndices);
		
		
		if(node.relativeChildOrder == null)
		{
			throw new RuntimeException("TreeReordering: node.relativeChildOrder null after reordering");
		}
		
		return orderChanged;
	}
	
	
	public void nonNullAlignedNodeMovingSort(TSNodeLabel node)
	{
		Comparator<TSNodeLabel> spanComparator = new AlignmentSpanComparator();
		int maxSublistStart = -1;
		int maxSublistEnd = -1;
		
		for(int i = 0; i < node.daughters.length; i++)
		{
			boolean daughterHasNullAlignedTerminals = (node.daughters[i].getNumUnalignedGovernedTerminals() > 0);
			
			if(daughterHasNullAlignedTerminals)
			{
				// We have collected a valid sublist of minimally length 2, so sort it 
				if((maxSublistStart >= 0) &&  (maxSublistEnd >= 0))
				{
					Arrays.sort(node.daughters,maxSublistStart, maxSublistEnd, spanComparator);
				}
				
				// Reset the start and end positions for the next sublist to be collected
				maxSublistStart = -1;
				maxSublistEnd = -1;
			}
			else
			{
				if(maxSublistStart >= 0)
				{
					// We have already a start, so we set the end (+1 since it is exclusive)
					maxSublistEnd = i + 1;
				}
				else
				{
					maxSublistStart = i;
				}
			}
		}
	}
	
	
	public boolean performReordering(boolean ignoreNulls)
	{
		//System.out.println("TreeAlignmentVisualizer.performReordering called ...");
		
		boolean orderChanged = false;
		
		for(int i = 0; i < theTreeStruct.getNodesArray().length; i++)
		{
			TSNodeLabel node = theTreeStruct.getNodesArray()[i];
			
			// only reorder non-terminal nodes
			if(! node.isTerminal())
			{
				
				// only reorder if there are no alignmentviolations for the subtree rooted at this node
				// we don't have to care about this, we can only do legal operations anyway, so if they improve the order in some sense: Great, let's do it				// if(!node.rootsTreeAlignmentViolation) 
				{
					
					
					// good to go: perform the reordering, and update orderChanged
					boolean childOrderChanged =  reorderChildrenToMatchAlignmentOrder(node, ignoreNulls);
					orderChanged = (orderChanged || childOrderChanged);
				}
				
				if(node.relativeChildOrder == null)
				{
					throw new RuntimeException("TreeReordering: node.relativeChildOrder null after reordering");
				}
			}
			
		}
		
	
		if(orderChanged)
		{
			// The order has changed, so to be safe we should recompute the access tables
			//  (This might do some extra work, so this could still be optimized)
			theTreeStruct.generateAccessTables();
		}
		
		
		return orderChanged;
	
	}
	
	
}	
