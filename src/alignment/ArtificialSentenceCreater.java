/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.ArrayList;
import java.util.List;

import util.Utility;

public class ArtificialSentenceCreater {

	public static final String SOURCE_WORD = "s";
	public static final String TARGET_WORD = "t";
	public static final String ERROR_WORD = "e";

	public static List<String> createArtificialSourceSentence(int length) {
		return createArtificialSentence(length, SOURCE_WORD);
	}

	public static List<String> createArtificialTargetSentence(int length) {
		return createArtificialSentence(length, TARGET_WORD);
	}

	private static List<String> createArtificialSentence(int length, String wordName) {
		List<String> result = new ArrayList<String>();

		for (int i = 0; i < length; i++) {
			result.add(wordName + i);
		}
		return result;
	}

	public static List<String> createArtificialSentence(String wordName, int length, int sentenceNumber) {
		List<String> result = new ArrayList<String>();

		for (int i = 0; i < length; i++) {
			result.add(wordName + sentenceNumber + "_" + i);
		}
		return result;
	}

	public static List<String> createArtificialSentenceWithDifferentOddAndEvenWirds(String oddWordName, String evenWordName, int length, int sentenceNumber) {
		List<String> result = new ArrayList<String>();

		for (int i = 0; i < length; i++) {
			if (Utility.isEven(i)) {
				result.add(evenWordName + sentenceNumber + "_" + i);
			} else {
				result.add(oddWordName + sentenceNumber + "_" + i);
			}
		}
		return result;
	}
}
