/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.util.List;

public class AlignmentTripleData 
{

	private List<List<String>> sourceSentences;
	private List<List<String>> targetSentences;
	private List<Alignment> alignments;	
	
	
	public void setTargetSentences(List<List<String>> sentencesOfSecondLanguage) {
		this.targetSentences = sentencesOfSecondLanguage;
	}
	
	public void setSourceSentences(List<List<String>> sourceSentences) 
	{
		this.sourceSentences = sourceSentences;
	}
	
	public void setAlignment(List<Alignment> alignments2) 
	{
		this.alignments = alignments2;
	}
	
	public Alignment getAlignment(int index)
	{
			if((this.alignments != null ) && (index < this.alignments.size()))
			{	
				return this.alignments.get(index);
			}
			return null;
	}
	
	public List<String> getSourceSentence(int index)
	{
		if((this.sourceSentences != null) && (index < this.sourceSentences.size()))
		{	
			return this.sourceSentences.get(index);
		}
		return null;
	}
	
	public List<String> getTargetSentence(int index)
	{
		if((this.targetSentences != null ) && (index < this.targetSentences.size()))
		{	
			return this.targetSentences.get(index);
		}
		return null;
	}
	
		
	public int getNumAlignments()
	{
		if(this.alignments != null)
		{	
			return this.alignments.size();
		}
		
		return 0;
	}
	
	
	public boolean hasSourceSentences()
	{
		return (this.sourceSentences != null);
	}
	
	public boolean hasTargetSentences()
	{
		return (this.targetSentences != null);
	}
	
	public boolean hasAlignments()
	{
		return (this.alignments != null);
	}
	
	public int getCorpusSize()
	{
		return Math.min(Math.min(this.alignments.size(), this.targetSentences.size()), this.alignments.size());
	}
	
	public int getMaxOfSizes()
	{
		return Math.max(Math.max(this.alignments.size(), this.targetSentences.size()), this.alignments.size());
	}
}
