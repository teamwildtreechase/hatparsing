/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.FileUtil;

public class ConfigFileCreater {
	private final Map<String, String> configFileProperties;
	private final List<String> extraPropertyLabels;
	private final String configFilePath;
	private static String CorpusLocationLabel = "corpusLocation";
	private static String SourceFileNameLabel = "sourceFileName";
	private static String TargetFileNameLabel = "targetFileName";
	private static String SourceFileNameCasedLabel = "sourceFileNameCased";
	private static String TargetFileNameCasedLabel = "targetFileNameCased";
	private static String AlignmentsFileNameLabel = "alignmentsFileName";
	private static String TargetParseFileNameLabel = "targetParseFileName";

	private static final String StandardSourceFileName = "sourceFile.txt";
	private static final String StandardTargetFileName = "targetFile.txt";
	private static final String StandardSourceFileNameCased = "sourceFile.Cased.txt";
	private static final String StandardTargetFileNameCased = "targetFile.Cased.txt";
	private static final String StandardAlignmentsFileName = "alignmentFile.txt";
	private static final String StandardConfigFileName = "configFile.txt";
	private static final String StandardTargetParseFileName = "targetParseFile.txt";

	private static List<String> BasicProperties = Arrays.asList(CorpusLocationLabel, SourceFileNameLabel, TargetFileNameLabel, AlignmentsFileNameLabel);

	protected ConfigFileCreater(Map<String, String> configFileProperties, String configFilePath) {
		this.configFileProperties = configFileProperties;
		this.extraPropertyLabels = new ArrayList<String>();
		this.configFilePath = configFilePath;
	}

	public static ConfigFileCreater createConfigFileCreater(String corpusLocation, String sourceFileName, String targetFileName, String alignmentsFileName, String configFilePath) {
		return new ConfigFileCreater(createBasicConfigFileMap(corpusLocation, sourceFileName, targetFileName, alignmentsFileName, "UNDEFINED"), configFilePath);
	}

	public static ConfigFileCreater createStandardConfigFileCreater(String corpusLocation, String configFilePath) {
		return new ConfigFileCreater(createStandardBasicConfigFileMap(corpusLocation), configFilePath);
	}

	public static ConfigFileCreater createStandardConfigFileCreaterWithCasedFiles(String corpusLocation) {
		return new ConfigFileCreater(createStandardBasicConfigFileMapWithCasedFiles(corpusLocation), getStandardConfigFilePath(corpusLocation));
	}

	protected static String getStandardConfigFilePath(String corpusLocation) {
		return corpusLocation + StandardConfigFileName;
	}

	public static Map<String, String> createBasicConfigFileMap(String corpusLocation, String sourceFileName, String targetFileName, String alignmentsFileName, String targetParseFileName) {
		Map<String, String> result = new HashMap<String, String>();
		result.put(CorpusLocationLabel, corpusLocation);
		result.put(SourceFileNameLabel, sourceFileName);
		result.put(TargetFileNameLabel, targetFileName);
		result.put(AlignmentsFileNameLabel, alignmentsFileName);
		result.put(TargetParseFileNameLabel, targetParseFileName);
		return result;
	}

	public static Map<String, String> createBasicConfigFileMapWithCasedFiles(String corpusLocation, String sourceFileName, String targetFileName, String alignmentsFileName,
			String targetParseFileName, String sourceCasedFileName, String targetCasedFileName) {
		Map<String, String> result = createBasicConfigFileMap(corpusLocation, sourceFileName, targetFileName, alignmentsFileName, targetParseFileName);
		result.put(SourceFileNameCasedLabel, sourceCasedFileName);
		result.put(TargetFileNameCasedLabel, targetCasedFileName);
		return result;
	}

	protected static Map<String, String> createStandardBasicConfigFileMap(String corpusLocation) {
		return createBasicConfigFileMap(corpusLocation, StandardSourceFileName, StandardTargetFileName, StandardAlignmentsFileName, StandardTargetParseFileName);
	}

	protected static Map<String, String> createStandardBasicConfigFileMapWithCasedFiles(String corpusLocation) {
		return createBasicConfigFileMapWithCasedFiles(corpusLocation, StandardSourceFileName, StandardTargetFileName, StandardAlignmentsFileName, StandardTargetParseFileName,
				StandardSourceFileNameCased, StandardTargetFileNameCased);
	}

	private void writeBasicProperties(BufferedWriter outputWriter) throws IOException {
		for (String basicProperty : BasicProperties) {
			String propertyValue = this.configFileProperties.get(basicProperty);
			outputWriter.write(basicProperty + " = " + propertyValue + "\n");
		}
	}

	private List<String> getExtraPropertyLabels() {
		return extraPropertyLabels;
	}

	private void writeExtraProperties(BufferedWriter outputWriter) throws IOException {
		for (String propertyLabel : getExtraPropertyLabels()) {
			String propertyValue = this.configFileProperties.get(propertyLabel);
			outputWriter.write(propertyLabel + " = " + propertyValue + "\n");
		}
	}

	public void addProperty(String propertyName, String propertyValue) {
		this.configFileProperties.put(propertyName, propertyValue);
		this.extraPropertyLabels.add(propertyName);
	}

	public String getSourceFileName() {
		return this.configFileProperties.get(SourceFileNameLabel);
	}

	public String getTargetFileName() {
		return this.configFileProperties.get(TargetFileNameLabel);
	}
	
	public String getSourceFileNameCased() {
		return this.configFileProperties.get(SourceFileNameCasedLabel);
	}
	
	public String getTargetFileNameCased() {
		return this.configFileProperties.get(TargetFileNameCasedLabel);
	}

	public String getTargetParseFileName() {
		return this.configFileProperties.get(TargetParseFileNameLabel);
	}

	public String getAlignmentsFileName() {
		return this.configFileProperties.get(AlignmentsFileNameLabel);
	}

	public String getCorpusLocation() {
		return this.configFileProperties.get(CorpusLocationLabel);
	}

	public String getConfigFilePath() {
		return configFilePath;
	}

	private String getConfigFileContainingFolderName() {
		File configFile = new File(this.configFilePath);
		return configFile.getParent();
	}

	public void writeConfigFile() {
		try {
			System.out.println("ConfigFileCreater : Creating config file containing folder if not existing");
			FileUtil.createFolderIfNotExisting(getConfigFileContainingFolderName());

			BufferedWriter configFileWriter = new BufferedWriter(new FileWriter(this.configFilePath));
			writeBasicProperties(configFileWriter);
			writeExtraProperties(configFileWriter);
			configFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
