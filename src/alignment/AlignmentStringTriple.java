/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;


public class AlignmentStringTriple extends SourceString
{
	private final String targetString;
	private final String alignmentString;
	
	protected AlignmentStringTriple(String sourceString, String targetString, String alignmentString)
	{
		super(sourceString);
		this.targetString = targetString;
		this.alignmentString = alignmentString;
	}
	
	public static AlignmentStringTriple createTrimmedAlignmentStringTriple(String sourceString, String targetString, String alignmentString)
	{
		sourceString = sourceString.trim();
		targetString = targetString.trim();
		alignmentString = alignmentString.trim();
		return new AlignmentStringTriple(sourceString, targetString, alignmentString);
	}
	
	public String getTargetString()
	{
		return this.targetString;
	}
	
	public String getAlignmentString()
	{
		return this.alignmentString;
	}
}
