/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

public class ArtificialCorpusProperties 
{
	public final int noMonotoneAlignments; // =5
	public final int noFullConnectedAlignments ; //= 5;
	public final int monotoneLength; // = 10;
	public final int fullConnectedAlignmentLength; // = 5;
	public final int noFourPermutationAlignments; // = 5;
	public final int fourPermutationAlignmentGroups; // = 2;
	public final int noLengthOneAlignments; // = 5;
	public final int noNoInternalNodesAlignments; // = 2;
	public final int noNoInternalNodesLength; // = 5;
	
	public ArtificialCorpusProperties(int noMonotoneAlignments, int monotoneLength, 
			                         int noFullConnectedAlignments, int fullConnectedAlignmentLength,
			                         int noFourPermutationAlignments, int fourPermutationAlignmentGroups,
									 int noLengthOneAlignments,
									 int noNoInernalNodesAlignments,int noInternalNodesLength)
	{
		this.noMonotoneAlignments = noMonotoneAlignments;
		this.monotoneLength = monotoneLength;
		this.noFullConnectedAlignments = noFullConnectedAlignments;
		this.fullConnectedAlignmentLength = fullConnectedAlignmentLength;
		this.noFourPermutationAlignments = noFourPermutationAlignments;
		this.fourPermutationAlignmentGroups = fourPermutationAlignmentGroups;
		this.noLengthOneAlignments = noLengthOneAlignments;
		this.noNoInternalNodesAlignments = noNoInernalNodesAlignments;
		this.noNoInternalNodesLength = noInternalNodesLength;
	}
	
	
	public static ArtificialCorpusProperties createArtificialCorpusProperties()
	{
		
		int noMonotoneAlignments = 5;
		int noFullConnectedAlignments = 10;
		int noFourPermutationAlignments = 5; 
		int noLengthOneAlignments = 5;
		int noNoInernalNodesAlignments = 2;
		
		/*
		int noMonotoneAlignments = 2;
		int noFullConnectedAlignments = 0;
		int noFourPermutationAlignments = 5; 
		int noLengthOneAlignments = 0;
		int noNoInernalNodesAlignments = 0;
	    */
		// lengths
		int monotoneLength = 5;
		int fullConnectedAlignmentLength = 5;
		int fourPermutationAlignmentGroups = 2;
		int noNoInternalNodesLength = 5;
	
		return new ArtificialCorpusProperties(noMonotoneAlignments, monotoneLength, noFullConnectedAlignments, fullConnectedAlignmentLength, noFourPermutationAlignments, fourPermutationAlignmentGroups, noLengthOneAlignments, noNoInernalNodesAlignments, noNoInternalNodesLength);
	}
	
	public static ArtificialCorpusProperties createArtificialCorpusProperties2()
	{
		
		int noMonotoneAlignments = 5;
		int noFullConnectedAlignments = 10;
		int noFourPermutationAlignments = 5; 
		int noLengthOneAlignments = 5;
		int noNoInernalNodesAlignments = 2;
		
		// lengths
		int monotoneLength = 10;
		int fullConnectedAlignmentLength = 10;
		int fourPermutationAlignmentGroups = 4;
		int noNoInternalNodesLength = 10;
	
		return new ArtificialCorpusProperties(noMonotoneAlignments, monotoneLength, noFullConnectedAlignments, fullConnectedAlignmentLength, noFourPermutationAlignments, fourPermutationAlignmentGroups, noLengthOneAlignments, noNoInernalNodesAlignments, noNoInternalNodesLength);
	}

	
	public double totalAlignments()
	{
		return this.noFullConnectedAlignments + this.noMonotoneAlignments + 
		this.noFourPermutationAlignments + this.noLengthOneAlignments + this.noNoInternalNodesAlignments;
	}
	
	
	public double monotoneFraction()
	{
		return ((double)this.noMonotoneAlignments) / totalAlignments();
	}
	
	public double lengthOneFraction()
	{
		return ((double)this.noLengthOneAlignments) / totalAlignments();
	}
	
	public double fullyConnectedFraction()
	{
		return ((double)this.noFullConnectedAlignments) / totalAlignments();
	}
	
	public double noInternalNodesFraction()
	{
		return ((double)this.noNoInternalNodesAlignments) / totalAlignments();
	}
	
	public double fourPermuationLength()
	{
		return this. fourPermutationAlignmentGroups * 4;
	}
	
	public double fourPermutationFraction()
	{
		return ((double)this. noFourPermutationAlignments) / totalAlignments();
	}
}
