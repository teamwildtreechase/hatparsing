/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package alignment;

import java.util.Comparator;

import tsg.TSNodeLabel;

/**
 * This comparator compares two TSNodeLabel instances, taking their Alignment spans
 * implemented by the spanMin and spanMax properties to do the comparison. Only if the span 
 * of one of the two nodes stricly preceds that of the other will a nonzero result be returned 
 * by the compare method.
 * 
 * @author gemaille
 *
 */

public class AlignmentSpanComparator implements Comparator<TSNodeLabel> 
{

	public int compare(TSNodeLabel node1, TSNodeLabel node2) 
	{
		// if one of the nodes roots completely unaligned trees
		if((node1.spanMax < 0) || (node2.spanMax < 0))
		{
			// No proper order specified
			return 0;	
		}
		
		
		if((node1.spanMin < node2.spanMin) && (node1.spanMax < node2.spanMin))
		{
			return -1;
		}
		else if((node2.spanMin < node1.spanMin) && (node2.spanMax < node1.spanMin))
		{
			return 1;	
		}
		else
		{
			// spans overlap: no proper order is specified 
			return 0;
		}
	}
	
	

}
