/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package alignment;

/** 
 * Small class that stores the pair of sourcePos-targetPos that defines the alignment  between two words 
 * 
 */

public class AlignmentPair 
{
	private final int sourcePos;
	private final int targetPos;
	
	public AlignmentPair(int sourcePos, int targetPos)
	{
		this.sourcePos = sourcePos;
		this.targetPos = targetPos;
	}
	
	
	public AlignmentPair(AlignmentPair toCopy)
	{
		this(toCopy.sourcePos, toCopy.targetPos);
	}
	
	public static AlignmentPair createInvertedAlignmentPair(AlignmentPair alignmentPair)
	{
		return new AlignmentPair(alignmentPair.targetPos, alignmentPair.sourcePos);
	}
	/** 
	 * Source position getter
	 * @return source alignment position
	 */
	public int getSourcePos()
	{
		return sourcePos;
	}

	/** 
	 * Target position getter
	 * @return target alignment position
	 */
	public int getTargetPos()
	{
		return targetPos;
	}

	
	
	public boolean equals(Object alignmentPairObject)
	{
		if(alignmentPairObject instanceof AlignmentPair)
		{
			AlignmentPair alignmentPair = ((AlignmentPair)alignmentPairObject);
			return((this.sourcePos == alignmentPair.sourcePos) && (this.targetPos == alignmentPair.targetPos));
		}
		return false;
	}
	
	public String toString()
	{
		String result = "";
		result = sourcePos + "-" + targetPos;
		return result;
	}
	
}
