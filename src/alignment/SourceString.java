/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignment;

public class SourceString {

	private final String sourceString;

	protected SourceString(String sourceString) {
		this.sourceString = sourceString;
	}

	public static SourceString createSourceString(String sourceString) {

		sourceString = sourceString.trim();
		return new SourceString(sourceString);
	}

	public String getSourceString() {
		return this.sourceString;
	}

}
