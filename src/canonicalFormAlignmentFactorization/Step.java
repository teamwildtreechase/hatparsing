package canonicalFormAlignmentFactorization;

public class Step {
	private int minLeftBoundPos;
	private final int maxLeftBoundPos;
	private final int value;

	private Step(int minLeftBoundPos, int maxLeftBoundPos, int value) {

		this.minLeftBoundPos = minLeftBoundPos;
		this.maxLeftBoundPos = maxLeftBoundPos;
		this.value = value;
	}

	public static Step createStep(int minLeftBoundPos, int maxLeftBoundPos, int value) {
		return new Step(minLeftBoundPos, maxLeftBoundPos, value);
	}

	public Step createUpdatedStep(int newValue) {
		return new Step(this.getMinLeftBoundPos(), this.getMaxLeftBoundPos(), newValue);
	}

	public int getMinLeftBoundPos() {
		return minLeftBoundPos;
	}

	public void setMinLeftBoundPos(int minLeftBoundPos) {
		this.minLeftBoundPos = minLeftBoundPos;
	}

	public int getMaxLeftBoundPos() {
		return maxLeftBoundPos;
	}

	public int getValue() {
		return value;
	}

	public boolean xValueWithinStepBounds(int x) {
		return (x >= this.minLeftBoundPos) && (x <= this.maxLeftBoundPos);
	}

	public String toString() {
		String result = "<Step>" + " ";
		result += "minLeftPos:" + minLeftBoundPos + " maxLeftPos: " + maxLeftBoundPos + " value: "
				+ value + " ";
		result += "</Step>";
		return result;
	}

}
