package canonicalFormAlignmentFactorization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.junit.Assert;
import util.Pair;
import alignment.Alignment;

public class CanonicalFormHATFactorizationShiftReduce extends CanonicalFormHATFactorization {

	private final List<List<Integer>> l;
	private final List<List<Integer>> u;

	protected CanonicalFormHATFactorizationShiftReduce(Alignment alignment, int sourceLength,
			int targetLength, List<Integer> eCumulativeLinkCount,
			List<Integer> fCumulativeLinkCount, List<List<Integer>> l, List<List<Integer>> u, int verbosity) {
		super(alignment, sourceLength, targetLength, eCumulativeLinkCount, fCumulativeLinkCount,verbosity);
		this.l = l;
		this.u = u;
	}

	public static CanonicalFormHATFactorizationShiftReduce createCanonicalFormHATFactorizationShiftReduce(
			Alignment alignment, int sourceLength, int targetLength,int verbosity) {
		List<List<Integer>> l = createIntegerMatrixInitializedToValue(targetLength, targetLength,
				INTIAL_L_VALUE);
		List<List<Integer>> u = createIntegerMatrixInitializedToValue(targetLength, targetLength,
				INTIAL_U_VALUE);
		List<Integer> eCumulativeLinkCount = createIntegerListInitializedToValue(sourceLength, -1);
		List<Integer> fCumulativeLinkCount = createIntegerListInitializedToValue(targetLength, -1);

		return new CanonicalFormHATFactorizationShiftReduce(alignment, sourceLength, targetLength,
				eCumulativeLinkCount, fCumulativeLinkCount, l, u,verbosity);
	}

	public List<Pair<Integer>> shiftReduceAlgorithm() {

		// Initialize u an l (values of span 1 entries are computed)
		initializeL();
		initializeU();

		// First we must compute the cumulative link counts
		computeCumulativeLinkCounts();

		System.out.println("State after initialization: " + NL + getStateString());

		List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();
		LinkedList<Integer> xList = new LinkedList<Integer>();
		// initialization stack, note that we add 0, instead of 1
		// as in the paper, since we work with permutation indices 0 ... (n-1)
		// instead of 1 ... n
		xList.add(0);

		for (int y = 0; y < targetLength; y++) {

			// This check is necessary to avoid duplicate reductions in the
			// output,
			// while still getting the atomic reductions of length 1 (i.e. not
			// directly
			// starting from reductions of length 2)
			if (y > xList.getLast()) {
				// append x to y
				xList.add(y);
			}

			int minimalXSeenInReduction = -1;
			Iterator<Integer> descendingIterator = xList.descendingIterator();
			int x;
			while (descendingIterator.hasNext()) {
				System.out.println("Inner loop shift-reduce algorithm");
				x = descendingIterator.next();
				System.out.println("x: " + x + " y: " + y);

				if (x < y) {

					// compute u(x,y) from u(x,y-1) and u(y,y)
					int value = Math.max(getUValue(x, y - 1), getUValue(y, y));
					System.out.println("newValue u(" + x + "," + y + "): " + value);
					setUValue(x, y, value);

					// compute l(x,y) from l(x,y-1) and l(y,y)
					value = Math.min(getLValue(x, y - 1), getLValue(y, y));
					System.out.println("newValue l(" + x + "," + y + "): " + value);
					setLValue(x, y, value);
				}

				if (f(x, y) == 0) {
					System.out.println("[" + x + "," + y + "] is a phrase");
					result.add(new Pair<Integer>(x, y));

					minimalXSeenInReduction = x;
				}

			}

			if ((minimalXSeenInReduction >= 0)) {
				// remove [x+1,y] from xList
				// This can be done efficiently by removing elements
				// from
				// the
				// top until element x+1 is removed
				int removedElement = -1;

				while (xList.getLast() != minimalXSeenInReduction) {
					removedElement = xList.removeLast();
					System.out.println(">>>Removed element: " + removedElement);
				}
			}
		}

		System.out.println("end system state: " + getStateString());

		return result;

	}

	private int f(int x, int y) {
		Assert.assertTrue(x >= 0);
		Assert.assertTrue(y >= 0);
		System.out.println("x: " + x);
		System.out.println("y: " + y);
		int numLinksBackFromTargetRangeToSource = 0;

		System.out.println("getLValue(" + x + "," + y + "): " + getLValue(x, y));
		System.out.println("getUValue(" + x + "," + y + "): " + getUValue(x, y));
		int linkCountUpToEndRangeInclusive = fCumulativeLinkCount.get(getUValue(x, y));
		int linkCountUpToBeginRangeExclusive = 0;
		if (getLValue(x, y) > 0) {
			linkCountUpToBeginRangeExclusive = fCumulativeLinkCount.get(getLValue(x, y) - 1);
		}

		numLinksBackFromTargetRangeToSource = linkCountUpToEndRangeInclusive
				- linkCountUpToBeginRangeExclusive;

		System.out.println(" linkCountUpToEndRangeInclusive : " + linkCountUpToEndRangeInclusive);
		System.out.println(" linkCountUpToBeginRangeExclusive : "
				+ linkCountUpToBeginRangeExclusive);

		int numLinksForwardFromSourceRangeToTarget = 0;
		int cumulativeLinkCountSourceUpToYInclusive = eCumulativeLinkCount.get(y);
		int cumulativeLinkCountSourceUpToXExclusive = 0;

		if (x > 0) {
			cumulativeLinkCountSourceUpToXExclusive = eCumulativeLinkCount.get(x - 1);
		}
		System.out.println(" cumulativeLinkCountSourceUpToYInclusive:"
				+ cumulativeLinkCountSourceUpToYInclusive);
		System.out.println(" cumulativeLinkCountSourceUpToXExclusive:"
				+ cumulativeLinkCountSourceUpToXExclusive);
		numLinksForwardFromSourceRangeToTarget = cumulativeLinkCountSourceUpToYInclusive
				- cumulativeLinkCountSourceUpToXExclusive;

		System.out.println("numLinksBackFromTargetRangeToSource: "
				+ numLinksBackFromTargetRangeToSource);
		System.out.println("numLinksForwardFromSourceRangeToTarget: "
				+ numLinksForwardFromSourceRangeToTarget);
		int result = numLinksBackFromTargetRangeToSource - numLinksForwardFromSourceRangeToTarget;
		System.out.println("f-value: " + result);
		Assert.assertTrue(result >= 0);
		return result;
	}

	private String getStateString() {
		String result = "";
		result += "<System State>" + NL;
		result += getCumulativeCountsString();
		result += "l: " + NL;
		result += getMatrixString(l) + NL;
		result += "u: " + NL;
		result += getMatrixString(u) + NL;
		result += "</System State>" + NL;
		return result;
	}

	public int getLValue(int i, int j) {
		return getMatrixValue(l, i, j);
	}

	public void setLValue(int i, int j, int value) {
		setMatrixValue(l, i, j, value);
	}

	public int getUValue(int i, int j) {
		return getMatrixValue(u, i, j);
	}

	public void setUValue(int i, int j, int value) {
		setMatrixValue(u, i, j, value);
	}

	private void initializeL() {
		int i = 0;
		for (int lValue : computeSpanOneEntriesLowValues(alignment, sourceLength)) {
			setLValue(i, i, lValue);
			i++;
		}
	}

	private void initializeU() {
		int i = 0;
		for (int lValue : computeSpanOneEntriesUpValues(alignment, sourceLength)) {
			setUValue(i, i, lValue);
			i++;
		}
	}

}
