package canonicalFormAlignmentFactorization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;

public class StepBasedTargetBoundFunctionRepresentation {

	private final List<StepList> stepLists;
	private final StepUpdateFunction stepUpdateFunction;
	private final int verbosity;

	private StepBasedTargetBoundFunctionRepresentation(List<StepList> stepLists,
			StepUpdateFunction stepUpdateFunction,int verbosity) {
		this.stepLists = stepLists;
		this.stepUpdateFunction = stepUpdateFunction;
		this.verbosity = verbosity;
	}

	public static StepBasedTargetBoundFunctionRepresentation createStepBasedLowRepresentation(int verbosity) {
		List<StepList> stepLists = new ArrayList<StepList>();
		return new StepBasedTargetBoundFunctionRepresentation(stepLists, new MinUpdateFunction(),verbosity);
	}

	public static StepBasedTargetBoundFunctionRepresentation createStepBasedUpRepresentation(int verbosity) {
		List<StepList> stepLists = new ArrayList<StepList>();
		return new StepBasedTargetBoundFunctionRepresentation(stepLists, new MaxUpdateFunction(),verbosity);
	}

	private boolean isUpdated(Step step, Step updatedStep) {
		return step.getValue() != updatedStep.getValue();
	}

	private boolean haveSameValue(Step step1, Step step2) {
		return step1.getValue() == step2.getValue();
	}

	private void extendStepToStepOnLeft(Step step, Step stepOnLeft) {
		step.setMinLeftBoundPos(stepOnLeft.getMinLeftBoundPos());
	}

	/*
	 * public int getValue(int x, int y) { Assert.assertTrue(x <= y); StepList
	 * stepList = this.stepLists.get(y);
	 * 
	 * for (Step step : stepList.getStepList()) { if ((x >=
	 * step.getMinLeftBoundPos()) && (x <= step.getMaxLeftBoundPos())) { return
	 * step.getValue(); } } throw new
	 * RuntimeException("Error: value not found"); }
	 */

	public int getValueUpwardsIterating(int x, int y) {
		Assert.assertTrue(x <= y);
		StepList stepList = this.stepLists.get(y);
		return stepList.getValueUpwardsIterating(x);
	}

	public int getValueDownwardsIterating(int x, int y) {
		Assert.assertTrue(x <= y);
		StepList stepList = this.stepLists.get(y);
		return stepList.getValueDownwardsIterating(x);
	}

	private String stepTypeString() {
		if (stepUpdateFunction instanceof MaxUpdateFunction) {
			return "up";
		} else if (stepUpdateFunction instanceof MinUpdateFunction) {
			return "low";
		} else {
			throw new RuntimeException("Error: unknown type stepUpdatefunction");
		}
	}

	public Step computeUpdatedStepListAndReturnLeftMostUpdatedStep(int addedValue, int y) {
		Step result = null;

		LinkedList<Step> newStepList = new LinkedList<Step>();
		// First we create and add a new step for the minLeftBound/maxLeftBound
		// range [y,y]
		Step lastAddedStep = Step.createStep(y, y, addedValue);
		newStepList.add(lastAddedStep);

		if (!this.stepLists.isEmpty()) {
			StepList previousStepsList = this.stepLists.get(stepLists.size() - 1);
			Iterator<Step> descendingIterator = previousStepsList.descendingIterator();

			while (descendingIterator.hasNext()) {
				Step step = descendingIterator.next();
				Step updatedStep = getUpdatedStep(step, addedValue);

				if (isUpdated(step, updatedStep)) {
					result = step;
				}

				if ((lastAddedStep != null) && (haveSameValue(updatedStep, lastAddedStep))) {
					// The new updated step has the same value as the last
					// updated
					// step
					// They should be merged into one updated step
					extendStepToStepOnLeft(lastAddedStep, updatedStep);
				} else {
					newStepList.addFirst(updatedStep);
					lastAddedStep = updatedStep;
				}
			}
		}

		// Add the new updated steps list
		this.stepLists.add(StepList.createStepList(newStepList,verbosity));

		System.out.println("New (" + stepTypeString() + ") steps list: " + newStepList);

		return result;
	}

	public Step getUpdatedStep(Step previousStep, int addedValue) {
		int oldValue = previousStep.getValue();
		int newValue = this.stepUpdateFunction.updatedValue(oldValue, addedValue);
		return previousStep.createUpdatedStep(newValue);
	}

	private static interface StepUpdateFunction {
		public int updatedValue(int oldValue, int addedValue);
	}

	public static class MaxUpdateFunction implements StepUpdateFunction {

		@Override
		public int updatedValue(int oldValue, int addedValue) {
			return Math.max(oldValue, addedValue);
		}

	}

	public static class MinUpdateFunction implements StepUpdateFunction {

		@Override
		public int updatedValue(int oldValue, int addedValue) {
			return Math.min(oldValue, addedValue);
		}

	}
}
