package canonicalFormAlignmentFactorization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;

import util.ObjectInteger;
import util.Pair;

/**
 * This is a re-implementation of the O(n log n) efficient factorization of PETs
 * described in "Factoring Synchronous Grammers by Sorting" by Gildea, Satta and
 * Zhang (2006)
 * 
 * @author gemaille
 * 
 */

public class CanonicFormPETFactorization {

	private static final String NL = "\n";

	// This is called "h" in the paper
	private List<Integer> permutation;
	private List<ReductionWithNumChildrenInformation> reductionsListSortedByIncreasingSize;

	private final List<HItem> hItemsOriginalOrder;
	private List<HItem> hItems;
	private List<Integer> v;
	private List<Integer> vMin;
	private List<Integer> vMax;
	private final int verbosity; // 0 - 4;

	private CanonicFormPETFactorization(List<Integer> permutation, List<HItem> hItemsOriginalOrder,
			List<HItem> hItems, List<Integer> v, List<Integer> vMin, List<Integer> vMax,
			int verbosity) {
		this.permutation = permutation;
		this.hItemsOriginalOrder = hItemsOriginalOrder;
		this.hItems = hItems;
		this.v = v;
		this.vMin = vMin;
		this.vMax = vMax;
		this.reductionsListSortedByIncreasingSize = new ArrayList<ReductionWithNumChildrenInformation>();
		this.verbosity = verbosity;
	}

	private static List<Integer> getMonotonePermutation(int permutationLength) {
		List<Integer> result = new ArrayList<Integer>();
		for (int i = 0; i < permutationLength; i++) {
			result.add(i);
		}
		return result;
	}

	public static CanonicFormPETFactorization createCanonicFormPETFactorization(
			List<Integer> permutation, int verboisty) {
		int permutationLength = permutation.size();
		// Initialization of h lists as in paper with monotone sequences

		List<Integer> v = getMonotonePermutation(permutationLength);
		List<Integer> vMin = getMonotonePermutation(permutationLength);
		List<Integer> vMax = getMonotonePermutation(permutationLength);

		List<HItem> hItems = new ArrayList<HItem>();
		List<HItem> hItemsOriginalOrder = new ArrayList<HItem>();
		for (int i = 0; i < permutationLength; i++) {
			HItem hItem = HItem.createHItem(i);
			hItemsOriginalOrder.add(hItem);
			hItems.add(hItem);

		}

		return new CanonicFormPETFactorization(permutation, hItemsOriginalOrder, hItems, v, vMin,
				vMax, verboisty);
	}

	public static CanonicFormPETFactorization createCanonicFormPETFactorizationFromPermutationString(
			String permutationString, int verbosity) {
		if (!permutationString.substring(0, 1).equals("[")) {
			System.out.println("Error: invalid permutation string, not starting with \"[\"");
			System.exit(1);
		}
		if (!permutationString
				.substring(permutationString.length() - 1, permutationString.length()).equals("]")) {
			System.out.println("Error: invalid permutation string, not ending with \"]\"");
			System.exit(1);
		}
		String[] parts = permutationString.substring(1, permutationString.length() - 1).split(",");

		List<Integer> permutation = new ArrayList<Integer>();
		for (String part : parts) {
			permutation.add(new Integer(part));
		}
		return createCanonicFormPETFactorization(permutation, verbosity);
	}

	private void printlnIfVerbosityHighEnough(String x, int minRequiredVerbosity) {
		if (verbosity >= minRequiredVerbosity) {
			System.out.println(x);
		}
	}

	public static String integerListString(List<Integer> integerList) {
		String result = "";
		for (Integer element : integerList) {
			result += "  " + element;
		}
		return result.trim();
	}

	private static String hItemListViewString(List<HItem> hItems, HItemView hItemView) {
		String result = "";
		for (HItem hItem : hItems) {
			result += "  " + hItemView.getViewedDimension(hItem);
		}
		return result.trim();
	}

	public String stateString() {
		String result = NL + NL + "<Algorithm State>" + NL;
		result += "permutation: " + integerListString(permutation) + NL;
		result += "h: " + hItemListViewString(hItems, new HItemHView()) + NL;
		result += "hMin: " + hItemListViewString(hItems, new HItemHMinView()) + NL;
		result += "hMax: " + hItemListViewString(hItems, new HItemHMaxView()) + NL;
		result += "v: " + integerListString(v) + NL;
		result += "vMin: " + integerListString(vMin) + NL;
		result += "vMax: " + integerListString(vMax) + NL;

		result += reductionsFoundString();
		result += "</Algorithm State>" + NL;
		return result;
	}

	private String reductionString(ReductionWithNumChildrenInformation reduction) {
		String result = "";
		for (int i = 0; i < reduction.getLeft(); i++) {
			result += "-";
		}

		for (int i = reduction.getLeft(); i <= reduction.right; i++) {
			result += "#";
		}

		for (int i = reduction.right + 1; i < permutation.size(); i++) {
			result += "-";
		}

		result += " numChildren: " + reduction.getNumChildren();
		return result;
	}

	public String reductionsFoundString() {
		String result = "";
		result += "<Reductions Found>" + NL;
		for (ReductionWithNumChildrenInformation reduction : reductionsListSortedByIncreasingSize) {
			result += reductionString(reduction) + NL;
		}
		result += "</Reductions Found>" + NL;

		return result;
	}

	public Set<Pair<Integer>> karize() {
		System.out.println("State before starting: " + this.stateString());

		for (int size = 1; size <= permutation.size(); size = size * 2) {
			for (int min = 0; min < permutation.size() - size; min = min + 2 * size) {
				int div = min + size - 1;
				int max = Math.min(permutation.size() - 1, min + 2 * size - 1);

				merge(min, div, max);
			}

		}
		Set<Pair<Integer>> result = new HashSet<Pair<Integer>>();

		for (Reduction reduction : this.reductionsListSortedByIncreasingSize) {
			result.add(new Pair<Integer>(reduction.getLeft(), reduction.getRight()));
		}

		System.out.println("State when ending: " + this.stateString());

		return result;

	}

	/**
	 * Compute a shifted permutation from a permutation which contains gaps, by
	 * computing relative positions using sorting
	 * 
	 * @param permutation
	 * @return
	 */
	public static List<Integer> computeUnShiftedPermutation(List<Integer> permutation) {

		List<Integer> sortedPermutation = new ArrayList<Integer>(permutation);
		Collections.sort(sortedPermutation);
		Map<Integer, Integer> permutationToSortPositionMap = new HashMap<Integer, Integer>();
		int index = 0;
		for (Integer element : sortedPermutation) {
			permutationToSortPositionMap.put(element, index);
			index++;
		}
		List<Integer> result = new ArrayList<Integer>();
		for (Integer elementOriginalPermutation : permutation) {
			result.add(permutationToSortPositionMap.get(elementOriginalPermutation));
		}

		System.out.println("unshifted permutation: " + result);

		return result;
	}

	public List<Integer> computeAscendingIndexOrderOfPermutation(List<Integer> permutation) {

		// We make a list with ObjectInteger objects, taking the index value in
		// the
		// permutation as the "object" and the (permutation) value as the value
		// to be used for sorting
		List<ObjectInteger<Integer>> permutationIndexPermutationValuePairList = new ArrayList<ObjectInteger<Integer>>();
		for (int i = 0; i < permutation.size(); i++) {
			permutationIndexPermutationValuePairList.add(new ObjectInteger<Integer>(i, permutation
					.get(i)));
		}

		// We then sort the permutationIndexPermutationValuePairList
		// so that the elements are sorted by Permutation value
		Collections.sort(permutationIndexPermutationValuePairList);

		// The order of the object integers is now such that if we loop over
		// them,
		// and look at the objects, we get a list of indices from the original
		// permutation
		// such that the resulting permutation elements for these indices are
		// in ascending order

		List<Integer> result = new ArrayList<Integer>();
		for (ObjectInteger<Integer> element : permutationIndexPermutationValuePairList) {
			result.add(element.getObject());
		}
		return result;
	}

	public static <T> void reorderRangeWithProperPermutation(List<T> integerList,
			List<Integer> permutation, int minPosition, int maxPosition) {

		List<T> subList = new ArrayList<T>(integerList.subList(minPosition, maxPosition + 1));
		List<T> resortedSublist = new ArrayList<T>();
		for (int i = 0; i < subList.size(); i++) {
			resortedSublist.add(subList.get(permutation.get(i)));
		}

		int index = 0;
		// Now replace the range in the integerList with the resortedSublist
		for (int i = minPosition; i <= maxPosition; i++) {
			integerList.set(i, resortedSublist.get(index));
			index++;
		}

	}

	public List<HItem> getCopyListHItemsOriginalOrder() {
		List<HItem> result = new ArrayList<HItem>();
		for (HItem hItem : hItemsOriginalOrder) {
			result.add(hItem);
		}
		return result;
	}

	public boolean hMinValueChangedFromOneBelow(int i) {
		return hItems.get(i).getHMin() != hItems.get(i - 1).getHMin();
	}

	public boolean hMaxValueChangedFromOneAbove(int i) {
		return hItems.get(i).getHMax() != hItems.get(i + 1).getHMax();
	}

	
	/**
	 * 
	 * Find reductions within the min-max range, and add them to the set of
	 * found reductions.
	 * 
	 *  The idea is:
     *  1. For the sub-permutation considered by merge, the sub-permutation index order corresponding
     *    to this sub-permutation sorted from small to large is determined.
     * 2. This index order is used to re-order h, so that reading out h from left to right
     *    (within the merge box) gives h values that, when mapped back to the values in the orginal
     *    permutation array (e.g. h[0]=9, h[1]=6    ====>   permutation[9], permutation[6]
     *    gives an ascending order (e.g. 2,4) . This re-sorted order is then used by the reduction test,
     *    to effectively form and test candidate reductions, which are step by step expanded to the left
     *    and right until they are complete or it is determined that the can not correspond to
     *    proper permutation because they contain a gap.
     *    
     *    Answers to two questions:
     *    
     *    1. Why work with sub-permutation indices, and not with the original permutation values?
     *    2. Why is constant (re) sorting necessary?
     *    Answer: The main motivation to work with the within-subsequence indices rather than with the original 
     *    permutation values, is that this permits the direct mapping from within-subsequence permutation values     
     *    to increased boundaries that can be mapped back and forth in horizontal and vertical direction, 
     *    enabling the efficient boundary extension within the scan algorithm which would not be possible  
     *    otherwise.
	 *	  To understand why sorting is also necessary note that the spans that are reduced do not necessarily 
	 *	  need to span the entire subsequence, and the fact that $h$ and $v$ are sorted permits to 
	 *	  gradually expand the boundaries from the center of the subsequence, while guaranteeing that whenever
	 *    a reduction is found, smaller valid reductions either do not exist 
	 *    or else have already been added.
	 * 
	 * 
	 * @param min
	 * @param div
	 * @param max
	 */
	public void merge(int min, int div, int max) {

		printlnIfVerbosityHighEnough("<<<Merge(" + min + "," + div + "," + max + ")>>>", 1);

		List<Integer> shiftedPermutation = computeAscendingIndexOrderOfPermutation(permutation
				.subList(min, max + 1));
		// sort hItems[min ... max] according to permutation[i]
		hItems = getCopyListHItemsOriginalOrder();
		printlnIfVerbosityHighEnough("Now perform within merge range resorting of h...",1);
		printlnIfVerbosityHighEnough("Resorting h sub-rang [" + min +"," + max +"] taking order given by this sub-part of permutation re-sorted to be ascending"  , 1);
		reorderRangeWithProperPermutation(hItems, shiftedPermutation, min, max);

		int vMaxTemp = -1;

		
		// We now update the values in vMin and vMax to capture the 
		// reductions found before and represented by blocks of 
		// equal hMin and hMax values
		int vMinTemp = min;
		for (int i = min; i <= max; i++) {
			v.set(hItems.get(i).getH(), i);

			if ((i == min) || hMinValueChangedFromOneBelow(i)) {
				vMinTemp = i;
			}
			vMin.set(hItems.get(i).getH(), vMinTemp);
		}

		for (int i = max; i >= min; i--) {

			if ((i == max) || hMaxValueChangedFromOneAbove(i)) {
				vMaxTemp = i;
			}
			vMax.set(hItems.get(i).getH(), vMaxTemp);
		}

		Reduction reduction = scan(div);
		printlnIfVerbosityHighEnough("reduction: " + reduction, 2);

		// Look for reductions
		if (reduction != null) {
			reduce(reduction);

			// Search for the next reductions: Left and right boundaries
			// initialized to be
			// adjacent to left edge of previous reduction
			Reduction leftReduction = getLeftAcrossLeftBoundaryReduction(reduction, min);
			Reduction rightReduction = getAcrossRightBoundaryReduction(reduction, max);

			Reduction previousSmallerReduction = reduction;

			while ((leftReduction != null) || (rightReduction != null)) {
				Reduction smallerReduction = getSmallerReduction(leftReduction, rightReduction);

				if (smallerReduction.equals(previousSmallerReduction)) {
					System.out
							.println("Error in fucntion \"merge\": created the same reduction twice - quiting");
					throw new RuntimeException();
				}

				printlnIfVerbosityHighEnough("smaller reduction: " + smallerReduction, 2);
				reduce(smallerReduction);

				leftReduction = getLeftAcrossLeftBoundaryReduction(smallerReduction, min);
				rightReduction = getAcrossRightBoundaryReduction(smallerReduction, max);
				previousSmallerReduction = smallerReduction;
			}
		}

	}

	public Reduction getLeftAcrossLeftBoundaryReduction(Reduction reduction, int min) {
		// We have to make sure that the position on the left edge is (one
		// position left of
		// the left boundary position) is still inside the permutation,
		// otherwise we
		// should skip it
		int leftAdjacentPosition = reduction.getPositionLeftAdjacentToLeftEdge();
		if ((leftAdjacentPosition >= min) && (leftAdjacentPosition >= 0)) {
			return scan(reduction.getPositionLeftAdjacentToLeftEdge());
		}
		return null;
	}

	public Reduction getAcrossRightBoundaryReduction(Reduction reduction, int max) {
		// We have to make sure that the position on the right edge is not the
		// last position
		// otherwise there is no boundary to "go over" and one position further
		// would fall
		// outside the permutation
		int positionOnRightEdge = reduction.getPositionOnRightEdge();

		if ((positionOnRightEdge < max) && (positionOnRightEdge < this.permutation.size() - 1)) {
			return scan(reduction.getPositionOnRightEdge());
		}
		return null;
	}

	/**
	 * Get the smaller of the two reductions, or if one is null, the one that is
	 * not null
	 * 
	 * @param reduction1
	 * @param reduction2
	 * @return
	 */
	public Reduction getSmallerReduction(Reduction reduction1, Reduction reduction2) {
		printlnIfVerbosityHighEnough("Finding the smaller of the reductions", 3);
		Assert.assertTrue((reduction1 != null) || (reduction2 != null));

		if (reduction1 == null) {
			printlnIfVerbosityHighEnough("First reduction not existing - choose second", 3);
			return reduction2;
		}

		if (reduction2 == null) {
			printlnIfVerbosityHighEnough("Second reduction not existing - choose first", 3);
			return reduction1;
		}

		int reduction1MumChildren = getNumberOfChildrenReduction(reduction1);
		int reduction2NumChildren = getNumberOfChildrenReduction(reduction2);

		printlnIfVerbosityHighEnough("reduction1NumChildren: " + reduction1MumChildren
				+ " reduction2NumChildren " + reduction2NumChildren, 3);
		if (reduction1MumChildren <= reduction2NumChildren) {

			if (reduction1MumChildren == reduction2NumChildren) {
				printlnIfVerbosityHighEnough(
						"Tie: both equal number of chilren => go with left alternative", 3);
			}
			return reduction1;
		}
		return reduction2;
	}

	private Set<Integer> getSetOfSourcePositionsToCover(Reduction reduction) {
		int minSourcePos = reduction.getLeft();
		int maxSourcePos = reduction.getRight();

		//
		Set<Integer> sourcePositionsToCover = new HashSet<Integer>();
		for (int i = minSourcePos; i <= maxSourcePos; i++) {
			sourcePositionsToCover.add(i);
		}
		return sourcePositionsToCover;
	}

	public boolean reductionContained(Reduction parentReduction, Reduction childReduction) {
		boolean result = (childReduction.left >= parentReduction.getLeft())
				&& (childReduction.getRight() <= parentReduction.getRight());

		// System.out.println("reductionContained(" + parentReduction + "," +
		// childReduction + "): "
		// + result);

		return result;
	}

	private boolean withinRange(int number, int rangeMin, int rangeMax) {
		return (number >= rangeMin) && (number <= rangeMax);
	}

	private boolean reductionContainsUncoveredPositions(Reduction reduction,
			Set<Integer> uncoveredPositions) {

		for (Integer uncoveredPosition : uncoveredPositions) {
			if (withinRange(uncoveredPosition, reduction.getLeft(), reduction.getRight())) {
				return true;
			}
		}
		return false;

	}

	public int getNumberOfChildrenReduction(Reduction reduction) {
		Set<Integer> sourcePositionsToCover = getSetOfSourcePositionsToCover(reduction);

		int numberOfChildrenReduction = 0;
		int reductionIndex = this.reductionsListSortedByIncreasingSize.size() - 1;
		while (!sourcePositionsToCover.isEmpty() && (reductionIndex >= 0)) {
			Reduction previousReduction = reductionsListSortedByIncreasingSize.get(reductionIndex);

			// If the reduction contains the previous reduction
			if (reductionContained(reduction, previousReduction)
					&& reductionContainsUncoveredPositions(previousReduction,
							sourcePositionsToCover)) {
				for (int i = previousReduction.getLeft(); i <= previousReduction.getRight(); i++) {
					sourcePositionsToCover.remove(i);
				}
				numberOfChildrenReduction++;
			}
			reductionIndex--;
		}

		printlnIfVerbosityHighEnough("soucePostiontsToCover:" + sourcePositionsToCover, 4);
		// The source positionsToCover left have to be created as atomic
		// children (reductions of size 1) and are added as such to the total
		// number of children
		numberOfChildrenReduction += sourcePositionsToCover.size();
		return numberOfChildrenReduction;

	}

	public void reduce(Reduction reduction) {
		for (int i = reduction.getBottom(); i <= reduction.getTop(); i++) {
			hItems.get(i).sethMin(reduction.getLeft());
			hItems.get(i).sethMax(reduction.getRight());
		}

		for (int i = reduction.getLeft(); i <= reduction.getRight(); i++) {
			vMin.set(i, reduction.getBottom());
			vMax.set(i, reduction.getTop());
		}

		printlnIfVerbosityHighEnough(
				"reduce:" + reduction.getLeft() + "..." + reduction.getRight(), 1);
		// Add the reduction to the list
		printlnIfVerbosityHighEnough("Adding reduction: " + reduction
				+ " to list of reductions ...", 1);

		int numChildren = getNumberOfChildrenReduction(reduction);
		this.reductionsListSortedByIncreasingSize.add(new ReductionWithNumChildrenInformation(
				reduction.getLeft(), reduction.getRight(), reduction.getBottom(), reduction
						.getTop(), numChildren));
	}

	public Reduction scan(int div) {
		printlnIfVerbosityHighEnough(NL + ">>called scan(" + div + ")", 1);
		System.out.println("state: " + stateString());

		int left = Integer.MIN_VALUE;
		int right = Integer.MIN_VALUE;
		int newLeft = div;
		int newRight = div + 1;
		int newTop = Integer.MIN_VALUE;
		int newBot = Integer.MAX_VALUE;

		int bot = -1;
		int top = -1;

		// For debugging to avoid infinite loops
		int maxLoops = 1000;
		int loopNum = 0;

		while (loopNum < maxLoops) {
			printlnIfVerbosityHighEnough("scan ... loop number: " + loopNum, 2);

			int numFirstLoops = 0;

			printlnIfVerbosityHighEnough("newLeft: " + newLeft + "  newRight: " + newRight, 2);

			for (int x = newLeft; x <= newRight;) {
				printlnIfVerbosityHighEnough("Scan - first loop", 3);
				printlnIfVerbosityHighEnough("x (before update): " + x, 3);
				newTop = Math.max(newTop, vMax.get(x));
				newBot = Math.min(newBot, vMin.get(x));

				
				printlnIfVerbosityHighEnough("hItems.get(vMin.get(x)).getHMax(): " + hItems.get(vMin.get(x)).getHMax(),3);
				printlnIfVerbosityHighEnough("hItems.get(v.get(x)).getHMax(): " + hItems.get(v.get(x)).getHMax(),3);
				// Assert.assertEquals(vMin.get(x), v.get(x));  // This is not always true
				// This is always true
				Assert.assertEquals(hItems.get(vMin.get(x)).getHMax(),hItems.get(v.get(x)).getHMax()); 
				
				/** skip to end of new reduced block  (1) */
				int xOld = x;
				x = hItems.get(vMin.get(x)).getHMax() + 1;  
				/** // (2)   The following is equivalent */
				// x = hItems.get(v.get(x)).getHMax() + 1; 
				 
				
				/** Note on increasing x:
				 * The function v gives (relative) target positions for source positions
				 * the function h maps back from relative target positions to relative 
				 *  source positions. vMin/vMax give the minimum/maximum relative
				 *  target positions for source positions part of the same already 
				 *  produced reduction. Similarly hMin/hMax give the minimum/maximum 
				 *  relative source positions for it.
				 *  
				 *  When we start from x, take the vMin value for that x, and then 
				 *  map back to the hMax of the result, we don't leave the reduction x
				 *  is part of but in fact directly jump to its right horizontal (= source)
				 *  boundary. So we don't even need to take vMin, but also v itself will 
				 *  do, if we take hMax of it afterwards anyhow. The latter could have 
				 *  perhaps been more intuitive
				 *  
				 *  Also we map from source to target and back to the maximum of the block,
				 *  and finally add 1. This means x is always increased by at least one.
				 *  We can test this assumption as well below
				 *  
				 */
				
				// Testing that x is always increased by at least one
				Assert.assertTrue(x >= (xOld + 1));

				printlnIfVerbosityHighEnough("x (after update): " + x, 3);
				printlnIfVerbosityHighEnough("newBot: " + newBot, 3);
				printlnIfVerbosityHighEnough("newTop: " + newTop, 3);

				// skip section scanned on last iteration
				if (x == left) {
					x = right + 1;
					System.out.println("x(3): " + x);
				}

				if (numFirstLoops > 30) {
					throw new RuntimeException("too many first loops");
				}

				numFirstLoops++;
			}
			right = newRight;
			left = newLeft;

			printlnIfVerbosityHighEnough("The reduction test...", 2);
			printlnIfVerbosityHighEnough(" newTop: " + newTop + " newBot: " + newBot
					+ "(newTop - newBot): " + (newTop - newBot), 3);
			printlnIfVerbosityHighEnough("h.get(newTop))" + hItems.get(newTop).getH(), 3);
			printlnIfVerbosityHighEnough("get(h.get(newBot))" + hItems.get(newBot).getH(), 3);
			printlnIfVerbosityHighEnough(
					"permutation.get(h.get(newTop))" + permutation.get(hItems.get(newTop).getH()),
					3);
			printlnIfVerbosityHighEnough(
					"permutation.get(h.get(newBot))" + permutation.get(hItems.get(newBot).getH()),
					3);
			printlnIfVerbosityHighEnough(
					("(permutation.get(h.get(newTop)) - permutation.get(h.get(newBot))): " + (permutation
							.get(hItems.get(newTop).getH()) - permutation.get(hItems.get(newBot)
							.getH()))), 3);

			printlnIfVerbosityHighEnough("testing if (partial) reduction is permissable for range[" + newBot + "," + newTop
					+ "]:", 1);

			// The reduction test
			if ((newTop - newBot) < (permutation.get(hItems.get(newTop).getH()) - permutation
					.get(hItems.get(newBot).getH()))) {
				printlnIfVerbosityHighEnough(">>> not a valid reduction", 1);
				return null;
			}
			else{
				printlnIfVerbosityHighEnough(">>> range is ok",1);
			}
			// Vertical scan

			System.out.println("newBot: " + newBot + "  newTop: " + newTop);

			int numSecondLoops = 0;
			for (int y = newBot; y <= newTop;) {
				printlnIfVerbosityHighEnough("Scan - second loop", 3);

				newLeft = Math.min(newLeft, hItems.get(y).getHMin());
				// System.out.println("y (before): " + y);
				// System.out.println("newRight: " + newRight);
				// System.out.println(" hItems.get(y).getHMax(): " +
				// hItems.get(y).getHMax());
				newRight = Math.max(newRight, hItems.get(y).getHMax());

				// Skip to end of reduced block
				int yOld = y;
				y = vMax.get(hItems.get(y).getHMin()) + 1;
				
				
				// Testing that y is always increased by at least one
				Assert.assertTrue(y >= (yOld + 1));

				// System.out.println("y: " + y);
				// System.out.println("newLeft: " + newLeft);
				// System.out.println("newRight: " + newRight);

				// Skip section on last iteration
				// System.out.println("bot: " + bot);
				if (y == bot) {
					y = top + 1;
				}

				if (numSecondLoops > 30) {
					throw new RuntimeException("too many second loops");
				}
				numSecondLoops++;
			}

			top = newTop;
			bot = newBot;

			// If no change to boundaries, reduce
			if ((newRight == right) && (newLeft == left)) {
				Reduction result = Reduction.createRedution(left, right, bot, top);
				printlnIfVerbosityHighEnough("scan result: " + result, 1);
				return result;
			}

			loopNum++;
		}
		return null;
	}

	public Set<Pair<Integer>> getMinimalFactorizationAsSetOfMinimalReductionRanges() {
		// Not implemented yet
		// return Collections.emptySet();

		return karize();
	}

	protected static class Reduction {
		private final int left;
		final int right;
		private final int bottom;
		private final int top;

		private Reduction(int left, int right, int bottom, int top) {
			this.left = left;
			this.right = right;
			this.bottom = bottom;
			this.top = top;
		}

		public static final Reduction createRedution(int left, int right, int bottom, int top) {
			return new Reduction(left, right, bottom, top);
		}

		public int getBottom() {
			return bottom;
		}

		public int getRight() {
			return right;
		}

		public int getLeft() {
			return left;
		}

		public int getTop() {
			return top;
		}

		public String toString() {
			String result = "<reduction> bottom:" + bottom + " top: " + top + " left: " + left
					+ " right:" + right + "</reduction>";
			return result;
		}

		public boolean equals(Object reductionObject) {
			if (reductionObject instanceof Reduction) {
				Reduction reduction = (Reduction) reductionObject;

				return (this.left == reduction.left) && (this.right == reduction.right)
						&& (this.bottom == reduction.bottom) && (this.top == reduction.top);
			}
			return false;

		}

		public int hashCode() {
			List<Integer> elementsList = Arrays.asList(left, right, bottom, top);
			return elementsList.hashCode();
		}

		public int getPositionLeftAdjacentToLeftEdge() {
			return this.left - 1;
		}

		public int getPositionOnRightEdge() {
			return this.right;
		}
	}

	private static class ReductionWithNumChildrenInformation extends Reduction {
		private final int numChildren;

		public ReductionWithNumChildrenInformation(int left, int right, int bottom, int top,
				int numChildren) {
			super(left, right, bottom, top);
			this.numChildren = numChildren;

		}

		public int getNumChildren() {
			return numChildren;
		}
	}

	private static class HItem {

		private final int h;
		private int hMin;
		private int hMax;

		private HItem(int h, int hMin, int hMax) {
			this.h = h;
			this.sethMin(hMin);
			this.sethMax(hMax);
		}

		public static HItem createHItem(int h) {
			return new HItem(h, h, h);
		}

		public int getHMin() {
			return hMin;
		}

		public void sethMin(int hMin) {
			this.hMin = hMin;
		}

		public int getHMax() {
			return hMax;
		}

		public void sethMax(int hMax) {
			this.hMax = hMax;
		}

		public int getH() {
			return h;
		}
	}

	private static interface HItemView {
		public int getViewedDimension(HItem hItem);
	}

	public static class HItemHView implements HItemView {

		@Override
		public int getViewedDimension(HItem hItem) {
			return hItem.getH();
		}
	}

	public static class HItemHMinView implements HItemView {

		@Override
		public int getViewedDimension(HItem hItem) {
			return hItem.getHMin();
		}
	}

	public static class HItemHMaxView implements HItemView {

		@Override
		public int getViewedDimension(HItem hItem) {
			return hItem.getHMax();
		}
	}

}
