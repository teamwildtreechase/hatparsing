package canonicalFormAlignmentFactorization;

import java.util.Iterator;
import java.util.LinkedList;

public class StepList {
	private final LinkedList<Step> stepList;
	/**
	 * The upwardsStepIterator and downwardsStepIterator help to efficiently
	 * (with amortized linear time performance) get values for consecutive x
	 * values that are monotonically ascending/descending as used by the linear
	 * time reduction algorithm
	 */
	private Iterator<Step> upwardsStepIterator;
	private Iterator<Step> downwardsStepIterator;
	private Step currentStepUpwardsIterating;
	private Step currentStepDownwardsIterating;
	private final int verbosity;

	private StepList(LinkedList<Step> stepList, Iterator<Step> upwardsStepIterator,
			Iterator<Step> downwardsStepIterator,int verbosity) {
		this.stepList = stepList;
		this.upwardsStepIterator = upwardsStepIterator;
		this.downwardsStepIterator = downwardsStepIterator;
		this.verbosity = verbosity;
	}

	public static StepList createStepList(int verbosity) {
		LinkedList<Step> stepList = new LinkedList<Step>();
		Iterator<Step> upwardsIterator = stepList.iterator();
		Iterator<Step> downwardsIterator = stepList.descendingIterator();
		return new StepList(stepList, upwardsIterator, downwardsIterator,verbosity);
	}

	public static StepList createStepList(LinkedList<Step> stepList,int verbosity) {
		Iterator<Step> upwardsIterator = stepList.iterator();
		Iterator<Step> downwardsIterator = stepList.descendingIterator();
		return new StepList(stepList, upwardsIterator, downwardsIterator,verbosity);
	}

	public LinkedList<Step> getStepList() {
		return this.stepList;
	}

	public Iterator<Step> descendingIterator() {
		return this.stepList.descendingIterator();
	}

	/*
	 * public void add(Step step) { this.stepList.add(step); }
	 * 
	 * public void addFirst(Step step) { this.stepList.addFirst(step); }
	 */

	public void throwErrorIfResetsIteratorMoreThanOnce(int iteratorResets, int x) {
		if (iteratorResets > 1) {
			throw new RuntimeException("Error : x" + x + "  not contained in stepslist"
					+ this.stepList);
		}
	}

	public void throwErrorIfStepsListIsEmpty() {
		if (stepList.isEmpty()) {
			throw new RuntimeException("Error: steplist is empty");
		}

	}

	public int getValueDownwardsIterating(int x) {

	  printlnIfVerbosityHighEnough("getValueDownwardsIterating(" + x + ")",3);
		throwErrorIfStepsListIsEmpty();

		int iteratorResets = 0;
		int numStepUpdates = 0;

		while ((currentStepDownwardsIterating == null)
				|| (!currentStepDownwardsIterating.xValueWithinStepBounds(x))) {

			throwErrorIfResetsIteratorMoreThanOnce(iteratorResets, x);

			if (downwardsStepIterator.hasNext()) {
				currentStepDownwardsIterating = downwardsStepIterator.next();
				numStepUpdates++;
			} else {
				downwardsStepIterator = this.stepList.descendingIterator();
				iteratorResets++;
			}
		}
		printlnIfVerbosityHighEnough(">> numStepUpdates: " + numStepUpdates,3);
		return currentStepDownwardsIterating.getValue();
	}

	public int getValueUpwardsIterating(int x) {
	  printlnIfVerbosityHighEnough("getValueUpwardsIterating(" + x + ")",3);

		throwErrorIfStepsListIsEmpty();

		int iteratorResets = 0;
		int numStepUpdates = 0;

		while ((currentStepUpwardsIterating == null)
				|| (!currentStepUpwardsIterating.xValueWithinStepBounds(x))) {

			throwErrorIfResetsIteratorMoreThanOnce(iteratorResets, x);

			if (upwardsStepIterator.hasNext()) {
				//System.out.println("stepList: " + stepList);
				currentStepUpwardsIterating = upwardsStepIterator.next();
				numStepUpdates++;
			} else {
				upwardsStepIterator = this.stepList.iterator();
				printlnIfVerbosityHighEnough("upwards iterator reset",3);
				iteratorResets++;
			}
		}
		printlnIfVerbosityHighEnough(">> numStepUpdates: " + numStepUpdates,3);
		return currentStepUpwardsIterating.getValue();
	}

    protected void printlnIfVerbosityHighEnough(String x, int minRequiredVerbosity) {
      if (verbosity >= minRequiredVerbosity) {
          System.out.println(x);
      }
  }
	
}
