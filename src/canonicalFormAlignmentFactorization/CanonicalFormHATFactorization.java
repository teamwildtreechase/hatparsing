package canonicalFormAlignmentFactorization;

import java.util.ArrayList;
import java.util.List;

import util.IntegerMatrixCreator;
import alignment.Alignment;
import alignment.AlignmentPair;

/**
 * This class is a reimplementation of the Algorithms described in "Extracting
 * Synchronous Grammar Rules from Word-Level Alignments in Linear Time" by
 * Zhang, Gildea and Chiang. This is also known as the paper introducing
 * "Normalized Decomposition Trees"
 * 
 * @author gemaille
 * 
 */

public abstract class CanonicalFormHATFactorization {
	protected static final String NL = "\n";
	// gives problems for permuations more than 1000 elements, but used
	// for readability results
	protected static final int INTIAL_L_VALUE = 1000;
	protected static final int INTIAL_U_VALUE = -1;

	protected final Alignment alignment;
	protected final int sourceLength; // the sourceLength, also known as "n"
	protected final int targetLength; // the targetLength, also known as "m"

	protected final List<Integer> eCumulativeLinkCount;
	protected final List<Integer> fCumulativeLinkCount;
	
	private final int verbosity; // 0 - 4;

	protected CanonicalFormHATFactorization(Alignment alignment, int sourceLength,
			int targetLength, List<Integer> eCumulativeLinkCount, List<Integer> fCumulativeLinkCount,int verbosity) {
		this.alignment = alignment;
		this.sourceLength = sourceLength;
		this.targetLength = targetLength;
		this.eCumulativeLinkCount = eCumulativeLinkCount;
		this.fCumulativeLinkCount = fCumulativeLinkCount;
		this.verbosity = verbosity;

	}

	protected static List<Integer> createIntegerListInitializedToValue(int length, int value) {
		List<Integer> result = new ArrayList<Integer>();
		for (int i = 0; i < length; i++) {
			result.add(value);
		}
		return result;

	}

	protected static List<List<Integer>> createIntegerMatrixInitializedToValue(int numRows,
			int numColumns, int value) {
		IntegerMatrixCreator integerMatrixCreator = new IntegerMatrixCreator();
		List<List<Integer>> result = integerMatrixCreator.createNumberMatrix(numRows, numColumns);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				result.get(i).set(j, value);
			}
		}
		return result;
	}

	protected static <T> T getMatrixValue(List<List<T>> matrix, int i, int j) {
		return matrix.get(i).get(j);
	}

	protected static <T> void setMatrixValue(List<List<T>> matrix, int i, int j, T value) {
		matrix.get(i).set(j, value);
	}

	protected static <T> String getMatrixString(List<List<T>> matrix) {
		String result = "";
		for (int row = 0; row < matrix.size(); row++) {
			for (int col = 0; col < matrix.get(0).size(); col++) {
				result += " " + getMatrixValue(matrix, row, col);
			}
			result += NL;
		}
		return result;
	}

	protected void computeCumulativeLinkCounts() {
		List<Integer> sourcePosLinkCounts = createIntegerListInitializedToValue(sourceLength, 0);
		List<Integer> targetPosLinkCounts = createIntegerListInitializedToValue(targetLength, 0);

		for (AlignmentPair alignmentPair : alignment.getAlignmentPairs()) {
			// increment count for source position
			int oldSourceLinkCount = sourcePosLinkCounts.get(alignmentPair.getSourcePos());
			sourcePosLinkCounts.set(alignmentPair.getSourcePos(), oldSourceLinkCount + 1);

			// increment count for target position
			int oldTargetLinkCount = targetPosLinkCounts.get(alignmentPair.getTargetPos());
			targetPosLinkCounts.set(alignmentPair.getTargetPos(), oldTargetLinkCount + 1);
		}

		int cumulativeSum = 0;
		for (int i = 0; i < sourcePosLinkCounts.size(); i++) {
			cumulativeSum += sourcePosLinkCounts.get(i);
			this.eCumulativeLinkCount.set(i, cumulativeSum);
		}

		cumulativeSum = 0;
		for (int j = 0; j < targetPosLinkCounts.size(); j++) {
			cumulativeSum += targetPosLinkCounts.get(j);
			this.fCumulativeLinkCount.set(j, cumulativeSum);
		}
	}

	protected static List<Integer> computeSpanOneEntriesLowValues(Alignment alignment,
			int sourceLength) {
		List<Integer> result = createIntegerListInitializedToValue(sourceLength, INTIAL_L_VALUE);
		for (AlignmentPair alignmentPair : alignment.getAlignmentPairs()) {
			int i = alignmentPair.getSourcePos();
			int j = alignmentPair.getTargetPos();
			int oldMin = result.get(i);
			int newMin = Math.min(oldMin, j);
			System.out.println("initializeL(" + i + "): " + j + " oldMin: " + oldMin + " newMin: "
					+ newMin);
			result.set(i, newMin);
		}
		return result;
	}

	protected static List<Integer> computeSpanOneEntriesUpValues(Alignment alignment,
			int sourceLength) {
		List<Integer> result = createIntegerListInitializedToValue(sourceLength, INTIAL_U_VALUE);
		for (AlignmentPair alignmentPair : alignment.getAlignmentPairs()) {
			int i = alignmentPair.getSourcePos();
			int j = alignmentPair.getTargetPos();
			int oldMax = result.get(i);
			int newMax = Math.max(oldMax, j);
			System.out.println("initializeU(" + i + "): " + j + " oldMax: " + oldMax + " newMax: "
					+ newMax);
			result.set(i, newMax);
		}
		return result;
	}
	
	protected String getCumulativeCountsString() {
		String result = "";
		result += "eCumulativeLinkCount: "
				+ CanonicFormPETFactorization.integerListString(eCumulativeLinkCount) + NL;
		result += "fCumulativeLinkCount: "
				+ CanonicFormPETFactorization.integerListString(fCumulativeLinkCount) + NL;
		return result;
	}
	
	 protected void printlnIfVerbosityHighEnough(String x, int minRequiredVerbosity) {
       if (verbosity >= minRequiredVerbosity) {
           System.out.println(x);
       }
   }

}
