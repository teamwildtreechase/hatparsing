package canonicalFormAlignmentFactorization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.junit.Assert;
import util.Pair;
import alignment.Alignment;

public class CanonicalFormHATFactorizationLinearTime extends CanonicalFormHATFactorization {

	private final StepBasedTargetBoundFunctionRepresentation lowStepBasedRepresentation;
	private final StepBasedTargetBoundFunctionRepresentation upStepBasedRepresentation;
	private final List<Integer> lowSpanOneEntries;
	private final List<Integer> upSpanOneEntries;

	protected CanonicalFormHATFactorizationLinearTime(Alignment alignment, int sourceLength,
			int targetLength, List<Integer> eCumulativeLinkCount,
			List<Integer> fCumulativeLinkCount,
			StepBasedTargetBoundFunctionRepresentation lowStepBasedRepresentation,
			StepBasedTargetBoundFunctionRepresentation upStepBasedRepresentation,
			List<Integer> lowSpanOneEntries, List<Integer> upSpanOneEntries, int verbosity) {
		super(alignment, sourceLength, targetLength, eCumulativeLinkCount, fCumulativeLinkCount,verbosity);

		this.lowStepBasedRepresentation = lowStepBasedRepresentation;
		this.upStepBasedRepresentation = upStepBasedRepresentation;
		this.lowSpanOneEntries = lowSpanOneEntries;
		this.upSpanOneEntries = upSpanOneEntries;
	}

	public static CanonicalFormHATFactorizationLinearTime createCanonicialFormHATFactorizationLinearTime(
			Alignment alignment, int sourceLength, int targetLength, int verbosity) {
		List<Integer> eCumulativeLinkCount = createIntegerListInitializedToValue(sourceLength, -1);
		List<Integer> fCumulativeLinkCount = createIntegerListInitializedToValue(targetLength, -1);

		StepBasedTargetBoundFunctionRepresentation lowStepBasedRepresentation = StepBasedTargetBoundFunctionRepresentation
				.createStepBasedLowRepresentation(verbosity);
		StepBasedTargetBoundFunctionRepresentation upStepBasedRepresentation = StepBasedTargetBoundFunctionRepresentation
				.createStepBasedUpRepresentation(verbosity);

		List<Integer> lowSpanOneEntries = CanonicalFormHATFactorization
				.computeSpanOneEntriesLowValues(alignment, sourceLength);
		List<Integer> upSpanOneEntries = CanonicalFormHATFactorization
				.computeSpanOneEntriesUpValues(alignment, sourceLength);

		return new CanonicalFormHATFactorizationLinearTime(alignment, sourceLength, targetLength,
				eCumulativeLinkCount, fCumulativeLinkCount, lowStepBasedRepresentation,
				upStepBasedRepresentation, lowSpanOneEntries, upSpanOneEntries,verbosity);
	}

	public List<Pair<Integer>> linearTimeAlgorithm() {

		List<Pair<Integer>> result = new ArrayList<Pair<Integer>>();

		// Initialize u an l (values of span 1 entries are computed)
		// First we must compute the cumulative link counts
		computeCumulativeLinkCounts();

		printlnIfVerbosityHighEnough("State after initialization: " + NL + getStateString(),1);

		LinkedList<Integer> xList = new LinkedList<Integer>();
		// initialization stack, note that we add 0, instead of 1
		// as in the paper, since we work with permutation indices 0 ... (n-1)
		// instead of 1 ... n
		// xList.add(0);

		for (int y = 0; y < sourceLength; y++) {

		    printlnIfVerbosityHighEnough("\n\n>>>Adding y=" + y,1);
			xList.add(y);

			// Update u and l
			int addedValueForUp = upSpanOneEntries.get(y);
			Step upLeftMostUpdatedStep = upStepBasedRepresentation
					.computeUpdatedStepListAndReturnLeftMostUpdatedStep(addedValueForUp, y);
		     printlnIfVerbosityHighEnough("upLeftMostUpdatedStep: " + upLeftMostUpdatedStep + NL,2);

			int addedValueForLow = lowSpanOneEntries.get(y);
			Step lowLeftMostUpdatedStep = lowStepBasedRepresentation
					.computeUpdatedStepListAndReturnLeftMostUpdatedStep(addedValueForLow, y);
		     printlnIfVerbosityHighEnough("lowLeftMostUpdatedStep: " + lowLeftMostUpdatedStep + NL,2);

			if ((lowLeftMostUpdatedStep != null) || (upLeftMostUpdatedStep != null)) {
				xList = computeNewXListUsingMonotonicityFilteringUsingPivots(
						lowLeftMostUpdatedStep, upLeftMostUpdatedStep, xList, y);
			}

		     printlnIfVerbosityHighEnough("xList after updating: " + xList,2);

			if (xList.size() >= 2) {
				updateListBasedOnMonoticityY(xList, y);
			}

		     printlnIfVerbosityHighEnough("xList after step 3: " + xList,2);

			reduce(xList, result, y);

		    printlnIfVerbosityHighEnough("\nxList end iteration: " + xList,1);
		    
		    printlnIfVerbosityHighEnough("\nState end iteration: " + NL + getStateString() + NL + "=====================" ,1);
		}

	      printlnIfVerbosityHighEnough(">>>result: " + result + NL + NL,1);

		return result;
	}

	private void reduce(LinkedList<Integer> xList, List<Pair<Integer>> result, int y) {

		int lastXWithFValueZero = -1;

		while (!xList.isEmpty()) {
			int x = xList.getLast();

			int fValue = getFvalueDownwardsIterating(x, y);

			if (fValue == 0) {
				result.add(new Pair<Integer>(x, y));
				lastXWithFValueZero = x;
				xList.removeLast();
			} else {

				break;
			}
		}

		// Put back the last X with fValueZero, it should be kept in the
		// x list
		if (lastXWithFValueZero >= 0) {
			xList.add(lastXWithFValueZero);
		}
		return;
	}

	private void updateListBasedOnMonoticityY(LinkedList<Integer> xList, int y) {
      printlnIfVerbosityHighEnough("Check monotonicity of y and remove if nescessary",3);

		int lastXBeforeY = getOneButLastElement(xList);

		int fValueBefore = getFvalueUpwardsIterating(lastXBeforeY, y);
		int fValueY = getFvalueUpwardsIterating(y, y);

		if (fValueY > fValueBefore) {
			// violates monotonicity
			xList.removeLast();

		     printlnIfVerbosityHighEnough("Remove y",3);
		}
	}

	private int getOneButLastElement(LinkedList<Integer> linkedList) {
		Assert.assertTrue(linkedList.size() >= 2);
		Iterator<Integer> listIterator = linkedList.descendingIterator();

		listIterator.next();
		return listIterator.next();

	}

	private LinkedList<Integer> computeNewXListUsingMonotonicityFilteringUsingPivots(
			Step lowLeftMostUpdatedStep, Step upLeftMostUpdatedStep, LinkedList<Integer> xList,
			int y) {

		LinkedList<Integer> xListNew = new LinkedList<Integer>();
		PivotValues pivotValues = PivotValues.createPivotValues(lowLeftMostUpdatedStep,
				upLeftMostUpdatedStep);
	      printlnIfVerbosityHighEnough("Pivot values: " + pivotValues,2);

		// boolean alignedBRangeElementSeen = false;
		// boolean alignedCRangeElementSeen = false;

		Iterator<Integer> xListIterator = xList.listIterator();

		int lastXBefore = xList.getFirst();
		int fValueLastXBefore = getFvalueUpwardsIterating(lastXBefore, y);

		while (xListIterator.hasNext()) {

			int x = xListIterator.next();
		     printlnIfVerbosityHighEnough("x: " + x,3);

			// All the values from range a (before x1*) are kept
			if (isInRangeA(x, pivotValues)) {
				xListNew.add(x);
			}

			if (isInRangeB(x, pivotValues)) {
		        printlnIfVerbosityHighEnough("x: " + x + " is in range B",3);

				// if (alignedBRangeElementSeen) {
				// System.out.println("x:" + x + " removed");
				// }
				// If source[x] has links
				// else

				if (getELinkCount(x, x) > 0) {
					// alignedBRangeElementSeen = true;

					int fValue = getFvalueUpwardsIterating(x, y);

					if (fValue > fValueLastXBefore) {
				      printlnIfVerbosityHighEnough("x:" + x + " removed",3);
					} else {

						System.out.println("x:" + x + " kept");
						xListNew.add(x);
						lastXBefore = x;
						fValueLastXBefore = fValue;
					}
				} else {
			       printlnIfVerbosityHighEnough("x:" + x + " unaligned so removed",3);
				}
			}

			if (isInRangeC(x, pivotValues)) {

		        printlnIfVerbosityHighEnough("x: " + x + " is in range C",3);

				// if (alignedCRangeElementSeen) {
				// System.out.println("x:" + x + " removed");
				// }
				// If source[x] has links
				// else
				if (getELinkCount(x, x) > 0) {
					// alignedCRangeElementSeen = true;

					int fValue = getFvalueUpwardsIterating(x, y);

					if (fValue > fValueLastXBefore) {
				      printlnIfVerbosityHighEnough("x:" + x + " removed",3);
					} else {
				      printlnIfVerbosityHighEnough("x:" + x + " kept",3);
						xListNew.add(x);
						lastXBefore = x;
						fValueLastXBefore = fValue;
					}

				} else {
			       printlnIfVerbosityHighEnough("x:" + x + " unaligned so removed",3);
				}
			}

			if (isInRangeD(x, pivotValues, y)) {
		        printlnIfVerbosityHighEnough("x:" + x + " in range D, so removed",3);
			}
		}

		xListNew.add(y);
		return xListNew;

	}

	private void printFValueIntermediateValues(int uXY, int lXY,int linkCountXToYEnglish){
      printlnIfVerbosityHighEnough("lXY: " + lXY,3);
      printlnIfVerbosityHighEnough("uXY: " + uXY,3);
      printlnIfVerbosityHighEnough("linkCountXToYEnglish: " + linkCountXToYEnglish,3);
	}
	
	public int getFvalueUpwardsIterating(int x, int y) {
        printlnIfVerbosityHighEnough("getFValueUpwardsIterating(" + x + "," + y + ")",3);
		int uXY = upStepBasedRepresentation.getValueUpwardsIterating(x, y);
		int lXY = lowStepBasedRepresentation.getValueUpwardsIterating(x, y);
		int linkCountXToYEnglish = getELinkCount(x, y);
		int linkCountLowToUpFrench = getFLinkCount(lXY, uXY);
		printFValueIntermediateValues(uXY, lXY, linkCountXToYEnglish);
		return linkCountLowToUpFrench - linkCountXToYEnglish;
	}

	public int getFvalueDownwardsIterating(int x, int y) {
	    printlnIfVerbosityHighEnough("getFValueDownwardsIterating(" + x + "," + y + ")",3);
		int uXY = upStepBasedRepresentation.getValueDownwardsIterating(x, y);
		int lXY = lowStepBasedRepresentation.getValueDownwardsIterating(x, y);
		int linkCountXToYEnglish = getELinkCount(x, y);
		int linkCountLowToUpFrench = getFLinkCount(lXY, uXY);
		printFValueIntermediateValues(uXY, lXY, linkCountXToYEnglish);
		return linkCountLowToUpFrench - linkCountXToYEnglish;
	}

	public int getELinkCount(int minSourcePos, int maxSourcePos) {
		int result = eCumulativeLinkCount.get(maxSourcePos);
		if (minSourcePos > 0) {

			result = result - eCumulativeLinkCount.get(minSourcePos - 1);
		}
		return result;
	}

	public int getFLinkCount(int minTargetPos, int maxTargetPos) {
		int result = fCumulativeLinkCount.get(maxTargetPos);
		if (minTargetPos > 0) {

			result = result - fCumulativeLinkCount.get(minTargetPos - 1);
		}
		return result;
	}

	private boolean isInRangeA(int x, PivotValues pivotValues) {
		return (x < pivotValues.getX1());
	}

	private boolean isInRangeB(int x, PivotValues pivotValues) {
		return (x >= pivotValues.getX1()) && (x < pivotValues.getX2());
	}

	private boolean isInRangeC(int x, PivotValues pivotValues) {
		return (x >= pivotValues.getX2()) && (x <= pivotValues.getY1());
	}

	private boolean isInRangeD(int x, PivotValues pivotValues, int y) {
		// Any x above y1* is in range D, unless it is y itself
		if (x != y) {
			return (x > pivotValues.getY1());
		}
		return false;
	}

	private String getStateString() {
		String result = "";
		result += "<System State>" + NL;
		result += "alignment: " + alignment.getAlignmentAsString() + NL;
		result += getCumulativeCountsString();
		result += "lowSpanOneEntries: " + NL;
		result += CanonicFormPETFactorization.integerListString(lowSpanOneEntries) + NL;
		result += "u: " + NL;
		result += CanonicFormPETFactorization.integerListString(upSpanOneEntries) + NL;
		result += "</System State>" + NL;
		return result;
	}

	private static class PivotValues {
		private final int x1;
		private final int x2;
		private final int y1;
		private final int y2;

		private PivotValues(int x1, int x2, int y1, int y2) {
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public static PivotValues createPivotValues(Step lowLeftMostStep, Step upLeftMostStep) {
			Assert.assertTrue((lowLeftMostStep != null) || (upLeftMostStep != null));

			int x1;
			int x2;
			int y1;
			int y2;

			if (lowLeftMostStep == null) {
				x1 = x2 = upLeftMostStep.getMinLeftBoundPos();
				y1 = y2 = upLeftMostStep.getMaxLeftBoundPos();

			} else if (upLeftMostStep == null) {

				x1 = x2 = lowLeftMostStep.getMinLeftBoundPos();
				y1 = y2 = lowLeftMostStep.getMaxLeftBoundPos();
			} else {
				x1 = Math.min(lowLeftMostStep.getMinLeftBoundPos(),
						upLeftMostStep.getMinLeftBoundPos());
				x2 = Math.max(lowLeftMostStep.getMinLeftBoundPos(),
						upLeftMostStep.getMinLeftBoundPos());

				y1 = Math.min(lowLeftMostStep.getMaxLeftBoundPos(),
						upLeftMostStep.getMaxLeftBoundPos());
				y2 = Math.max(lowLeftMostStep.getMaxLeftBoundPos(),
						upLeftMostStep.getMaxLeftBoundPos());
			}

			return new PivotValues(x1, x2, y1, y2);

		}

		public int getX1() {
			return x1;
		}

		public int getX2() {
			return x2;
		}

		public int getY1() {
			return y1;
		}

		// public int getY2() {
		// return y2;
		// }

		public String toString() {
			String result = "<PivotValues> ";
			result += "x1: " + x1 + " x2: " + x2 + " y1: " + y1 + " y2: " + y2;
			result += " </PivotValues>";
			return result;
		}
	}
}
