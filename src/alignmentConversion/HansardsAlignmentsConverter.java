/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HansardsAlignmentsConverter 
{

	public static void perFormConversion(String sourceFileName, String targetFileName, boolean usePossibleAlignments)
	{
		BufferedReader sourceReader;
		BufferedWriter outputWriter;
	
		System.out.println("Perform conversion of alignments...");
		
		
		try 
		{
			sourceReader = new BufferedReader(new FileReader(sourceFileName));
			outputWriter = new BufferedWriter(new FileWriter(targetFileName));
			
			int exampleNo = 1;
			String alignmentString;
			
			List<HansardsAlignmentItem> alignmentItems = new ArrayList<HansardsAlignmentItem>();
			while( ((alignmentString = sourceReader.readLine()) != null)) 
			{	
				HansardsAlignmentItem alignmentItem = HansardsAlignmentItem.createHansardsAlignmentItem(alignmentString);
				
				if(alignmentItem.exampleNumber != exampleNo)
				{
					// Write away the String for the previous example based on the collected list of HansardAlignmentItems
					writeSentenceAlignmentString(alignmentItems, outputWriter,usePossibleAlignments);
					// reset the list of alignment items
					alignmentItems  = new ArrayList<HansardsAlignmentItem>();
					// Add the first item of the new batch
					alignmentItems.add(alignmentItem);
					exampleNo = alignmentItem.exampleNumber;
				}
				else
				{
					alignmentItems.add(alignmentItem);
				}
			}
			
			// write the last sentence alignment String
			writeSentenceAlignmentString(alignmentItems, outputWriter,usePossibleAlignments);
			outputWriter.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	
		
	}		

	private static void writeSentenceAlignmentString(List<HansardsAlignmentItem> alignmentItems, BufferedWriter outputWriter, boolean usePossibleAlignments) throws IOException
	{
		if(alignmentItems.size() > 0)
		{	
			String sentenceAlignmentString = createStandardAlignmentStringFromHansardsAlignmentItemList(alignmentItems,usePossibleAlignments);
			System.out.println("Full Sentence Alignment: " + sentenceAlignmentString);
			outputWriter.write(sentenceAlignmentString + "\n");
		}	
	}
	
	
	public static String createStandardAlignmentStringFromHansardsAlignmentItemList(List<HansardsAlignmentItem> alignmentItems, boolean usePossibleAlignments)
	{
		String result = "";
		Iterator<HansardsAlignmentItem> itemIterator = alignmentItems.iterator();
		
		while(itemIterator.hasNext())
		{
			HansardsAlignmentItem item = itemIterator.next();
			
			if(alignmentTypeIsSureOrAlsoIncludePossibleAlignments(item,usePossibleAlignments))
			{	
				result += item.createAlignmentPairStringCorrectedToStartFromZero();
			
				if(itemIterator.hasNext())
				{
					result += " ";
				}
			}	
		}
		return result;
	}

	private static boolean  alignmentTypeIsSureOrAlsoIncludePossibleAlignments(HansardsAlignmentItem item, boolean usePossibleAlignments)
	{
		return (item.alignmentType.equals(HansardsAlignmentItem.HannsardAlignmentType.Sure) || usePossibleAlignments);
	}
	
	
	public static void main(String[] args)
	{
		
		if(args.length != 3)
		{
			System.out.println("Wrong usage, Usage: java -jar performHansardsFormatConversion InputAlignmentFileName OutputAlignmentFileName UsePossibleAlignments" +
					"\n e.g: >> java -jar performHansardsFormatConversion alignments.txt true");
			System.exit(0);
		}
		
		boolean usePossibleAlignments =  Boolean.parseBoolean(args[2]);;
		
		if(usePossibleAlignments)
		{
			System.out.println("Performing conversion using the sure and possible alignments");
			
		}
		else
		{
			System.out.println("Performing conversion using only the sure alignments");
		}
		
		String hansardsItemString = "0001 1 1 S";
		System.out.println("number: " + HansardsAlignmentItem.createHansardsAlignmentItem(hansardsItemString));
		
		String sourceFileName = args[0];
		String targetFileName = args[1];
		//String sourceFileName = "/home/gemaille/atv-bitg-workingdirectory/Hansards-English-French-Hand-Aligned/answers/test.wa.nonullalign";
		//String targetFileName = "/home/gemaille/atv-bitg-workingdirectory/Hansards-English-French-Hand-Aligned/answers/test.wa.nonullalign-converted";
		perFormConversion(sourceFileName, targetFileName, usePossibleAlignments);
	}
	
}
