/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;


public class HansardsCorpusConverter 
{

   // 
   private static String HANSARDS_ENGLISH_ENCODING = "UTF8";
   // This seems to be the right encoding, see: http://scratchpad.wikia.com/wiki/Character_Encoding_Recommendation_for_Languages
   // Used for Western European languages according to kate as well
   // The French source file in the Hansards corpus uses a different encoding
   private static String HANSARDS_FRENCH_ENCODING = "ISO8859_1";
   
  
  
	private String hansardsDirectoryLocation;
	private String outputDirectoryLocation;
	
	private HansardsCorpusConverter(String hansardsDirectoryLocation, String outputDirectoryLocation)
	{
		this.hansardsDirectoryLocation = hansardsDirectoryLocation;
		this.outputDirectoryLocation = outputDirectoryLocation;
	}
	
	public static HansardsCorpusConverter createHansardsCorpusConverter(String  hansardsDirectoryLocation, String outputDirectoryLocation)
	{
		return new HansardsCorpusConverter(hansardsDirectoryLocation, outputDirectoryLocation);
	}
	
	public void performConversion()
	{
		boolean usePossibleAlignments = true;
		HansardsAlignmentsConverter.perFormConversion(getHansardsAlignmentsInputLocation(), getHansardsAlignmentsOutputLocation(usePossibleAlignments), usePossibleAlignments);
		usePossibleAlignments = false;
		HansardsAlignmentsConverter.perFormConversion(getHansardsAlignmentsInputLocation(), getHansardsAlignmentsOutputLocation(usePossibleAlignments), usePossibleAlignments);
		HansardsSourceAndTargetConverter.perFormConversion(getHansardsSourceInputLocation(), getHansardsSourceOutputLocation(),HANSARDS_ENGLISH_ENCODING);
		HansardsSourceAndTargetConverter.perFormConversion(getHansardsTargetInputLocation(), getHansardsTargetOutputLocation(),HANSARDS_FRENCH_ENCODING);
	}
	
	private String getHansardsAlignmentsInputLocation()
	{
		return hansardsDirectoryLocation + "/answers/test.wa.nonullalign"; 
	}
	
	private String getHansardsAlignmentsOutputLocation(boolean usePossibleAlignments)
	{
		String result =  outputDirectoryLocation + "/alignments";
		
		if(usePossibleAlignments)
		{
			result += "-SureAndPossible";
		}
		else
		{
			result += "-SureOnly";
		}
		result += ".txt";
		return result;
	}
	
	private String getHansardsSourceInputLocation()
	{
		return hansardsDirectoryLocation + "/test/test.e"; 
	}

	private String getHansardsSourceOutputLocation()
	{
		return outputDirectoryLocation + "/english-converted"; 
	}
	
	private String getHansardsTargetInputLocation()
	{
		return hansardsDirectoryLocation + "/test/test.f"; 
	}

	private String getHansardsTargetOutputLocation()
	{
		return outputDirectoryLocation + "/french-converted"; 
	}
	
	public static String usageString()
	{
		String result = "";
		result += "usage : java -jar convertHansardsCorpus.jar HansardsCorpusLocation ConvertedHansardsLocation";
		return result;	
	}
	
	
	public static void main(String[] args)
	{
		if(args.length != 2)
		{	
			System.out.println(usageString());
			System.exit(0);
		}	
		HansardsCorpusConverter corpusConverter = createHansardsCorpusConverter(args[0], args[1]);
		corpusConverter.performConversion();
	}	
}

