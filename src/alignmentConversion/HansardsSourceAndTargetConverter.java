/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import util.FileUtil;

public class HansardsSourceAndTargetConverter 
{
	public static void perFormConversion(String sourceFileName, String targetFileName,String sourceFileEncoding)
	{
		BufferedReader sourceReader = null;
		BufferedWriter outputWriter = null;
	
		System.out.println("Perform conversion of alignments...");
		InputStreamReader reader = null;
		
		try 
		{
		    reader = new InputStreamReader(new FileInputStream(sourceFileName),sourceFileEncoding);
			sourceReader = new BufferedReader(reader);
			outputWriter = new BufferedWriter(new FileWriter(targetFileName));
			
			String line;
			
			while( ((line = sourceReader.readLine()) != null)) 
			{	
				outputWriter.write(getStringWithoutTags(line) + "\n");
			}
			
			outputWriter.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally{
		  FileUtil.closeCloseableIfNotNull(reader);
		  FileUtil.closeCloseableIfNotNull(sourceReader);
		  FileUtil.closeCloseableIfNotNull(outputWriter);
		}
	}
	
	public static String getStringWithoutTags(String sentenceWithTags)
	{
		String result = sentenceWithTags;
		//System.out.println("result-0: " + result);
		result = result.substring(result.indexOf(">") + 1);
		//System.out.println("result-1: " + result);
		result = result.substring(0,result.indexOf("<"));
		//System.out.println("result-2: " + result);
		
		// Remove superfluous whitespace on the left and right of the sentence
		result = result.trim();
		
		return result;
	}
	
	public static void main(String[] args)
	{
		String sourceFileName = "/home/gemaille/atv-bitg-workingdirectory/Hansards-English-French-Hand-Aligned/test/test.e";
		String targetFileName = "/home/gemaille/atv-bitg-workingdirectory/Hansards-English-French-Hand-Aligned/test/test.e-converted";
		perFormConversion(sourceFileName, targetFileName, "UTF8");
	}
	
}
