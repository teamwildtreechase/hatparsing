/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GizaPPSourceAlignmentSet 
{
	int sourcePosition;
	List<Integer> alignedTargetPositions;

	private GizaPPSourceAlignmentSet(int sourcePosition,List<Integer> alignedTargetPositions)
	{
		this.sourcePosition = sourcePosition;
		this.alignedTargetPositions = alignedTargetPositions;
	}
	
	public static GizaPPSourceAlignmentSet createGizaPPSourceAlignmentSet(int sourcePosition, String gizaAlignmentsListString)
	{
		List<Integer> alignedTargetPositions = getTargetPositionsListFromGizaListString(gizaAlignmentsListString);
		return new GizaPPSourceAlignmentSet(sourcePosition,alignedTargetPositions);
	}
	
	
	private static String createStrippedListString(String listString)
	{
		return listString.substring(1, listString.length() - 1).trim();
	}
	
	private static List<Integer> getTargetPositionsListFromGizaListString(String listString)
	{
		List<Integer> result = new ArrayList<Integer>();
		
		String strippedListsString = createStrippedListString(listString);
		if(strippedListsString.length() > 0)
		{	
			List<String> numberStrings = Arrays.asList((strippedListsString.split(" ")));
			
			for(String numberString : numberStrings)
			{
				int gizaTargetPos = Integer.parseInt(numberString);
				int targetPosStartingFromZero = gizaTargetPos - 1;
				result.add(targetPosStartingFromZero);
			}
		}	
		return result;
	}
	
	private  String createAlignmentStringElement(int targetPos)
	{
		return "" + this.sourcePosition + "-" + targetPos;
	}
	
	public String createAlignmentString()
	{
		String result = "";
		Iterator<Integer> targetPositionsIterator = this.alignedTargetPositions.iterator();
		
		while(targetPositionsIterator.hasNext())
		{
			int targetPosition = targetPositionsIterator.next();
			result += createAlignmentStringElement(targetPosition);
			
			if(targetPositionsIterator.hasNext())
			{
				result += " ";
			}
		}
		
		return result;
	}
}
