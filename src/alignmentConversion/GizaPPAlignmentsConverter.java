/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GizaPPAlignmentsConverter 
{

	public static void perFormConversion(String sourceFileName, String targetFileName)
	{
		BufferedReader sourceReader;
		BufferedWriter outputWriter;
	
		System.out.println("Perform conversion of alignments...");
		
		
		try 
		{
			sourceReader = new BufferedReader(new FileReader(sourceFileName));
			outputWriter = new BufferedWriter(new FileWriter(targetFileName));
			
			int lineNo = 1;
			String alignmentString;
			while( ((alignmentString = sourceReader.readLine()) != null)) 
			{
				if(isAlignmentLine(lineNo))
				{
					outputWriter.write(convertGizaStringToAlignmentString(alignmentString) + "\n");	
				}
				
				if((lineNo % 1000)  == 0)
				{
					System.out.println("Processing line" + lineNo);
				}
				
				lineNo++;
			}
			outputWriter.close();
			
			
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	
		
	}		
	
	public static boolean isAlignmentLine(int lineNo)
	{
		return ((lineNo % 3) == 0);
	}
	
	public static String convertGizaStringToAlignmentString(String gizaString)
	{
		String result = "";
		String noBraketsPattern = "[^\\{\\}]*";
		String patternString = "\\{" + noBraketsPattern + "\\}";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(gizaString);

		// skip the first position, corresponding to the empty cept
		matcher.find();
		
		int sourcePosition = 0;

		// Find all matches after the first
		while (matcher.find()) 
	    {
	      // Get the matching string
	      String match = matcher.group();
	      //System.out.println("match : " + match);
	      GizaPPSourceAlignmentSet sourceAlignmentSet = GizaPPSourceAlignmentSet.createGizaPPSourceAlignmentSet(sourcePosition, match);
	      
	      String alignmentPartString = sourceAlignmentSet.createAlignmentString();
	      //System.out.println("AlignmentPartString: " + alignmentPartString);
	      if(alignmentPartString.length() > 0)
	      {	  
	    	  result +=  alignmentPartString + " ";
	      }	  
	      sourcePosition++;
	    }
	   
		// remove superfluous whitespace
		result = result.trim();
		
		//System.out.println("Generated alignment String: " + result);
		
	    return result;
	}
	
	
	public static void testExample()
	{		
		String gizaAlignmentString = "NULL ({ 5 }) ik ({ 1 }) wil ({ 2 4 6 }) ook ({ 3 }) mijn ({ 7 }) goede ({ 8 }) collega ({ 9 }) en ({ 10 }) vriend ({ 11 }) toomas ({ 12 }) ilves ({ 13 14 }) gelukwensen ({ }) in ({ 15 }) zijn ({ 16 }) nieuwe ({ 17 }) functie ({ 18 }) en ({ 19 }) bij ({ 20 }) het ({ }) promoten ({ 21 }) van ({ 23 }) meer ({ 22 }) europese ({ 24 }) ideeën ({ }) in ({ 25 }) estland ({ 26 }) en ({ 27 }) meer ({ 28 }) estse ({ 29 }) ideeën ({ 30 }) hier ({ 31 }) . ({ 32 })"; 
		String result = convertGizaStringToAlignmentString(gizaAlignmentString);
		System.out.println("Generated alignment string: " + result);
	}
	
	
	public static String usageString()
	{
		String result = "";
		result += "usage : java -jar convertGIZAToListAlignments.jar GizaAlignmentsFileName OutputFileName";
		result += "\n Be aware that the GIZA file contains alignments with three lines per alingment." +
				"\nThe third of this line contains the source words with the target positions they map to." +
				"\nThis will be used as the source side for the alignment, the positions will be outputted," +
				"after substraction by 1 to start from 0 as the target positions. " +
				"\n\nWARNING: DO NOT SIMPLY THRUST ON THE FILENAME USED AS INPUT.\nASSURE YOURSELF YOU USE THE RIGHT INPUT FILE AND NAME " +
				"FOR THE OUTPUT\nBY INSPECTING THE INPUT FILE!";
			
		return result;	
	}
	
	
	public static void main(String[] args)
	{
		if(args.length != 2)
		{	
			System.out.println(usageString());
			System.exit(0);
		}	
		perFormConversion(args[0], args[1]);
	}	
}
