/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.util.Arrays;
import java.util.List;

import alignment.AlignmentPair;

public class HansardsAlignmentItem 
{
	public static enum HannsardAlignmentType{Sure,Possible};
	protected int exampleNumber;
	protected AlignmentPair alignmentPair;
	protected HannsardAlignmentType alignmentType;
	
	private HansardsAlignmentItem(int exampleNumber, AlignmentPair alignmentPair, HannsardAlignmentType alignmentType)
	{
		this.exampleNumber = exampleNumber;
		this.alignmentPair = alignmentPair;
		this.alignmentType = alignmentType;
	}
	
	public static HansardsAlignmentItem createHansardsAlignmentItem(String hansardItemString)
	{
		List<String> parts = createHansardParts(hansardItemString);
		int exampleNumber = getNumberFromString(parts.get(0));
		AlignmentPair alignmentPair = new AlignmentPair(getNumberFromString(parts.get(1)), getNumberFromString(parts.get(2)));
		HannsardAlignmentType alignmentType = getHansardAlignmentTypeFromString(parts.get(3));
		return new HansardsAlignmentItem(exampleNumber, alignmentPair, alignmentType);
	}
	
	private static List<String> createHansardParts(String hansardItemString)
	{
		return Arrays.asList(hansardItemString.split(" "));
	}
	
	private static HannsardAlignmentType getHansardAlignmentTypeFromString(String typeString)
	{
		if(typeString.equals("S"))
		{
			return HannsardAlignmentType.Sure;
		}
		else if(typeString.equals("P"))
		{
			return HannsardAlignmentType.Possible;
		}
		else
		{
			throw new RuntimeException("Error in getting Hansard Alignment Type: unrecognized type String");
		}
	}
	
	public static int getNumberFromString(String numberString)
	{
		return Integer.parseInt(numberString);
	}
	
	public String createAlignmentPairStringCorrectedToStartFromZero()
	{
		AlignmentPair startingFromZeroAlignmentPair = new AlignmentPair(this.alignmentPair.getSourcePos() - 1, this.alignmentPair.getTargetPos() - 1);
		return startingFromZeroAlignmentPair.toString();
	}
	
	public String toString()
	{
		String result = "";
		result += "\n<HansardAlignmentItem>";
		result += "\nExample number: " + exampleNumber;
		result += "\nAlignment: " + alignmentPair;
		result += "\nAlignmentType: " + alignmentType;
		result += "\n</HansardAlignmentItem>";
		return result;
	}
}
