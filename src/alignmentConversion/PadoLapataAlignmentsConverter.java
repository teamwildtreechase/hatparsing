/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import util.FileUtil;
import alignment.ConfigFileCreater;

public class PadoLapataAlignmentsConverter 
{
	BufferedReader inputReader;
	BufferedWriter sourceWriter, targetWriter, alignmentsWriter;
	private static final String SourceFileName = "source.txt";
	private static final String TargetFileName = "target.txt";
	private static final String AlignmentsFileName = "alignments.txt";
	private static final String ConfigFileName = "configfile-PadoLapata-en-ge.txt";
	private static final int MaxAllowedInferencesPerNode = 400000;
	private static String HatPropertiesFileName = "hatProperties-LREC2008-PadoLapata-EN-GE-";
	private static String TreeStatisticsOutputFolder = "treeStatistics-PadoLapata-EN-GE-";
	private static String TreeStatisticsFileName = "EbitgTreeStatistics-PadoLapata-EN-GE.csv";
	
	private PadoLapataAlignmentsConverter(BufferedReader inputReader,BufferedWriter sourceWriter,BufferedWriter targetWriter,BufferedWriter alignmentsWriter)
	{
		this.inputReader = inputReader;
		this.sourceWriter = sourceWriter;
		this.targetWriter = targetWriter;
		this.alignmentsWriter = alignmentsWriter;
	}
	
	public static PadoLapataAlignmentsConverter createPadoLapataAlignmentsConverter(String inputFilePath, String outputFolderPath, boolean atomicPartsMustAllShareSameAlignmentChunk)
	{
		String sourceFilePath = outputFolderPath + SourceFileName;
		String targetFilePath = outputFolderPath + TargetFileName;
		String alignmentsFilePath = outputFolderPath + AlignmentsFileName;
		
		try 
		{
			FileUtil.createFolderIfNotExisting(outputFolderPath);
			ConfigFileCreater configFileCreater = ConfigFileCreater.createConfigFileCreater(outputFolderPath, SourceFileName,TargetFileName, AlignmentsFileName, outputFolderPath + ConfigFileName);
			addTreeStatisticProrperties(configFileCreater, atomicPartsMustAllShareSameAlignmentChunk);
			configFileCreater.writeConfigFile();
			
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFilePath));
			BufferedWriter sourceWriter = new BufferedWriter(new FileWriter(sourceFilePath));
			BufferedWriter targetWriter = new BufferedWriter(new FileWriter(targetFilePath));
			BufferedWriter alignmentsWriter = new BufferedWriter(new FileWriter(alignmentsFilePath));
			return new PadoLapataAlignmentsConverter(inputReader, sourceWriter, targetWriter, alignmentsWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static void addTreeStatisticProrperties(ConfigFileCreater configFileCreater, boolean atomicPartsMustAllShareSameAlignmentChunk)
	{
		configFileCreater.addProperty("hatPropertiesFileName",HatPropertiesFileName); 
		
		if(atomicPartsMustAllShareSameAlignmentChunk)
		{
			configFileCreater.addProperty("treeStatisticsOutputFolder",TreeStatisticsOutputFolder + "-DiscontinuousAtomicTranslationEquivalentsAllowed");
		}
		else
		{
			configFileCreater.addProperty("treeStatisticsOutputFolder",TreeStatisticsOutputFolder);	
		}
		
		configFileCreater.addProperty("treeStatisticsFileName", TreeStatisticsFileName);
		configFileCreater.addProperty("maxAllowedInferencesPerNode", "" + MaxAllowedInferencesPerNode);
		configFileCreater.addProperty("atomicPartsMustAllShareSameAlignmentChunk", "" + atomicPartsMustAllShareSameAlignmentChunk);
	}
	
	public void createConvertedCorpus()
	{
		System.out.println("PadoLapataAlignmentConverter: createConvertedCorpus...");
		PadoLapataAlignmentTriple alignmentTriple;
		try 
		{
			while((alignmentTriple = PadoLapataAlignmentTriple.createPadoLapataAlignmentTriple(inputReader)) != null)
			{	
					this.sourceWriter.write(alignmentTriple.getSourceString() + "\n");
					this.targetWriter.write(alignmentTriple.getTargetString() + "\n") ;
					this.alignmentsWriter.write(alignmentTriple.getAlignmentString() + "\n"); 
			}
			
			this.sourceWriter.close();
			this.targetWriter.close();
			this.alignmentsWriter.close();
			
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		String inputFilePath = "/home/gemaille/atv-bitg-workingdirectory-oldsvn/trunk/AlignedCorperaManual/PadoLapataEnglishGerman/corrected_alignment";
		String outputFolderPath = "/home/gemaille/atv-bitg-workingdirectory-oldsvn/trunk/AlignedCorperaManual/PadoLapataEnglishGermanConverted/";
		boolean atomicPartsMustAllShareSameAlignmentChunk = false;
		PadoLapataAlignmentsConverter alignmentConverter = PadoLapataAlignmentsConverter.createPadoLapataAlignmentsConverter(inputFilePath, outputFolderPath,atomicPartsMustAllShareSameAlignmentChunk);
		
		alignmentConverter.createConvertedCorpus();
	}
}
