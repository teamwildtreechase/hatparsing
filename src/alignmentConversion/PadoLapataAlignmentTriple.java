/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;



public class PadoLapataAlignmentTriple 
{
	private final int exampleNumber;
	private final String sourceString, targetString, alignmentString;
	private static final String PadoLapataAlignmentPartSeparator = "\t";
	private static final String PadoLapataAlignmentSeparator = ":";
	private static final String EbitgAlignmentSeparator = "-";

	private PadoLapataAlignmentTriple(int exampleNumber,String sourceString, String targetString, String alignmentString)
	{
		this.exampleNumber = exampleNumber;
		this.sourceString = sourceString;
		this.targetString = targetString;
		this.alignmentString = alignmentString;
	}
	
	public static PadoLapataAlignmentTriple createPadoLapataAlignmentTriple(BufferedReader inputReader)
	{
		try 
		{
			List<String> lineList = new ArrayList<String>();
			for(int i = 0; i < 4; i++)
			{
				String line = inputReader.readLine();
				if(line == null)
				{
					return null;
				}
				lineList.add(line);
			}			
			int exampleNumber = Integer.parseInt(lineList.get(0));
			String sourceString = lineList.get(1);
			String targetString = lineList.get(2);
			String alignmentString = convertPadoLapataAlingmentStringToEbitgFormat(lineList.get(3));
			
			return new PadoLapataAlignmentTriple(exampleNumber,sourceString, targetString, alignmentString);
		} 
		catch (NumberFormatException e) 
		{	
			e.printStackTrace();
			return null;
		} 
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String convertPadoLapataAlingmentStringToEbitgFormat(String alignmentString)
	{
		String result = "";
		
		
		String[] alignmentParts = alignmentString.split(PadoLapataAlignmentPartSeparator);
		
		for(String alignmentPart : alignmentParts)
		{
			result += convertAlignmentPart(alignmentPart);
		}
		
		return result;
	}
	
	public static String convertAlignmentPart(String alignmentPart)
	{
		String result = "";
		//System.out.println("alignmentPart: " + alignmentPart);
		String[] sourceAndTagetPosList = alignmentPart.split(PadoLapataAlignmentSeparator);
		Assert.assertEquals(2,sourceAndTagetPosList.length);
		
		if(!isEmptyCept(sourceAndTagetPosList[0]))
		{	
			String[] targetPositionStrings = sourceAndTagetPosList[1].split(" ");
			
			for(String targetPosString : targetPositionStrings)
			{
				result += computeConvertedAlignmentPair(sourceAndTagetPosList[0], targetPosString) + " ";	
			}
		}
		return result;
	}
	
	public static boolean isEmptyCept(String sourceString)
	{
		return Integer.parseInt(sourceString) == 0;
		
	}
	
	
	public static String computeConvertedAlignmentPair(String sourcePosString, String targetPosString)
	{
		int convertedSourcePos = computeConvertedAlignmentPos(sourcePosString);
		int convertedTargetPos = computeConvertedAlignmentPos(targetPosString);
		String result = convertedSourcePos + EbitgAlignmentSeparator + convertedTargetPos;
		
		System.out.println("computeConvertedAlignmentPair result" + result);
		return result;
	}
	
	public static int computeConvertedAlignmentPos(String alignmentPos)
	{
		return Integer.parseInt(alignmentPos) - 1;
	}
	
	public String getSourceString()
	{
		return this.sourceString;
	}
	
	public String getTargetString()
	{
		return this.targetString;
	}
	
	public String getAlignmentString()
	{
		return this.alignmentString;
	}
	
	public int getExampleNumber()
	{
		return this.exampleNumber;
	}
}
