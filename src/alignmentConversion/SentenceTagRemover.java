/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package alignmentConversion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceTagRemover 
{
	private final String tagPatternString = "<[^>]*>"; 
	private final Pattern tagPattern = Pattern.compile(tagPatternString);
	
	public SentenceTagRemover()
	{
		
	}
	
	private String createSentenceWithoutTags(String sentenceWithTags)
	{
		String result = "";
		
		Matcher matcher = tagPattern.matcher(sentenceWithTags);
		
		int sentenceBeginIndex, sentenceEndIndex;
		
		if(matcher.find())
		{
			sentenceBeginIndex = matcher.end();
		}
		else
		{
			throw new RuntimeException("createSentenceWithoutTags : Error :did not found first tag");
		}
		
		if(matcher.find())
		{
			sentenceEndIndex = matcher.start();
		}
		else
		{
			throw new RuntimeException("createSentenceWithoutTags : Error :did not found second tag");
		}
		
		result = sentenceWithTags.substring(sentenceBeginIndex,sentenceEndIndex);
		
		result = result.trim();
		return result;
	}
	
	
	public void createSentencesWithoutTags(String inputFileName,String outputFileName)
	{
		
		try {
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			
			String sentenceWithTags;
			while((sentenceWithTags = inputReader.readLine()) != null)
			{
				String lineToWrite = createSentenceWithoutTags(sentenceWithTags) + "\n";
				outputWriter.write(lineToWrite);
			}
			
			inputReader.close();
			outputWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			int i = 0;
			for(String arg : args)
			{
				System.out.println("arg[" + i + "]" + arg);
				i++;
			}
			
			System.out.println("Usage: java - jar sentenceTagRemover InputFileName OutputFileName");
			System.exit(0);
		}
		
		String inputFileName = args[0];
		String outputFileName = args[1];
		
		SentenceTagRemover sentenceTagRemover = new SentenceTagRemover();
		sentenceTagRemover.createSentencesWithoutTags(inputFileName,outputFileName);
	}
	
}
