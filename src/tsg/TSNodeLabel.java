/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package tsg;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.Vector;
import settings.Parameters;
import symbols.Symbol;
import symbols.SymbolList;
import symbols.SymbolString;
import tsg.corpora.ConstCorpus;
import tsg.corpora.Wsj;
import util.FileUtil;
import util.Pair;
import util.PrintProgress;
import util.Utility;

/** Class storing the tree nodes used by the TreeAlignmentVisualizer */

public class TSNodeLabel {

	public Label label;
	public TSNodeLabel parent;
	public boolean headMarked;
	public boolean isLexical;
	
	// boolean indicating whether the subtree rooted at this node contains an alignment violation 
	// under the condition that tree structure must be maintained, and reordering may only take place
	// by permuting the children of subtrees.
	public boolean rootsTreeAlignmentViolation;
	public boolean violatesSourceSubtreeAlignment;
	// list of tree nodes that violate the target alignment range claimed by the subtree rooted at this node
	public ArrayList<TSNodeLabel> alignmentViolaters;
	
	// flag that indicates wether the subtree rooted at this node has been fully reordered
	public boolean subtreeReordered;
	
	public TSNodeLabel[] daughters;
	
	
	// spanMin and spanMax define the span over the target words were leave nodes rooted at this tree 
	//  align to, in other words the source span that is "claimed" by the subtree rooted at this node
	public int spanMin;
	public int spanMax;
	
	// This is perhaps questionable, storing the alignment in the tree nodes, but it makes everything
	// a lot simpler and more efficient to compute and spares a lot of bookkeeping when reordering 
	// the tree nodes: the alignment is then changed automatically with the tree
	public  ArrayList<Integer> targetAlignments;  // this field should only have a value > 0 for leaf nodes
	public boolean childOrderChanged; // this field indicates whether the order of the children under this node has been changed
	public boolean ownParentRelativePositionChanged; // this field indicates whether position of this node changed relative to the parent
	public int[] relativeChildOrder = null; // This field indicates the relative order after reordering of the children of the nodes has been performed
	
	private ExtraLabelsTable extraLabelsTable;
	
	private int numUnalignedGovernedTerminals;
	
	// Values that keep track of the span in the source of the node
	// Instantiate these values to the maximum and minimum respectively,
	// so they can be computed bottom up from the node's children
	private int sourceSpanMin = Integer.MAX_VALUE;
	private int sourceSpanMax = Integer.MIN_VALUE;
	
	
	protected TSNodeLabel() 
	{	
	}
	
	public TSNodeLabel(String s) throws Exception {
		this(s, true);
	}
	
	public TSNodeLabel(String s, boolean allTerminalsAreLexical) throws Exception {		
		this(ParenthesesBlockPenn.getParenthesesBlocks(s), allTerminalsAreLexical);
	}
	
	public TSNodeLabel nonRecursiveCopy() {
		TSNodeLabel result = new TSNodeLabel();
		result.label = this.label;
		result.headMarked = this.headMarked;
		result.isLexical = this.isLexical;		
		return result;
	}
	
	/**
	 * Clone method: copy all fields and the recursive (sub)treestructure in a deep way
	 */
	public TSNodeLabel clone() {
		TSNodeLabel result = new TSNodeLabel();
		result.label = this.label;
		result.headMarked = this.headMarked;
		result.isLexical = this.isLexical;
		
		if(this.targetAlignments != null)
		{	
			result.targetAlignments =  new ArrayList<Integer>(this.targetAlignments);
		}
		else
		{
			result.targetAlignments = null;
		}
		// tODO: should also copy alignment violation information, but then has to clone subtree
		// first before this can be done
		
		if (this.isTerminal()) return result;
		int prole = this.prole();
		result.daughters = new TSNodeLabel[prole];
		for(int i=0; i<prole(); i++) {
			result.daughters[i] = this.daughters[i].clone();
			result.daughters[i].parent = result;
		}
		
		// copy the number of unaligned goverende terimals
		result.numUnalignedGovernedTerminals =  this.numUnalignedGovernedTerminals;
		
		return result;
	}
	
	
	/**
	 *  Getter method for the number of unaligned terminals that descend from this node
	 * @return The number of un-aligned governed terminals
	 */
	public int  getNumUnalignedGovernedTerminals()
	{
		return  numUnalignedGovernedTerminals;
	}
	
	/**
	 * Setter for the number of unaligned governed terminals
	 * @param numUnalignedGovernedTerminals : New number of unaligned governed terminals
	 */
	public void setNumUnalignedGovernedTerminals(int  numUnalignedGovernedTerminals)
	{
		this. numUnalignedGovernedTerminals =  numUnalignedGovernedTerminals;
	}
	
	public TSNodeLabel addTop() {
		if (this.label().equals("TOP")) return this;
		TSNodeLabel top = new TSNodeLabel();
		top.label = new Label("TOP");
		top.daughters = new TSNodeLabel[]{this};
		this.parent = top;
		return top;
	}
	
	private TSNodeLabel(ParenthesesBlockPenn p, boolean allTerminalsAreLexical) {
		this();
		if (p.isTerminal())
		{	
			acquireTerminalLabel(p, allTerminalsAreLexical);
		}
		else 
		{
			acquireNonTerminalLabel(p);
			this.daughters = new TSNodeLabel[p.subBlocks.size()];
			int i=0;
			for(ParenthesesBlockPenn pd : p.subBlocks) {
				this.daughters[i] = new TSNodeLabel(pd, allTerminalsAreLexical);
				this.daughters[i].parent = this;
				i++;
			}
		}	
	}
	
	protected void acquireNonTerminalLabel(ParenthesesBlockPenn p) 
	{
		String extractedLabel =  ExtraLabelsTable.extractLabelString(p.label);
				
		if (extractedLabel.endsWith("-H")) 
		{
			this.headMarked = true;
			this.label = new Label(extractedLabel.substring(0, extractedLabel.length()-2));
		}
		else this.label = new Label(extractedLabel);
		
		this.extraLabelsTable = ExtraLabelsTable.createExtraLabelsTable(p.label); 
	}
	
	protected void acquireTerminalLabel(ParenthesesBlockPenn p, boolean allTerminalsAreLexical) 
	{
		String extractedLabel = ExtraLabelsTable.extractLabelString(p.label);
		
		if (allTerminalsAreLexical) {
			this.label = new Label(extractedLabel);
			this.isLexical = true;
		}
		else {
			if (extractedLabel.indexOf("\"")==-1) {
				this.label = new Label(extractedLabel);
			}
			else {
				this.label = new Label(extractedLabel.replaceAll("\"",""));
				this.isLexical = true;
			}	
		}
		this.extraLabelsTable = ExtraLabelsTable.createExtraLabelsTable(p.label);
	}
	
	
	public String getExtraLabel(String extraLabelName)
	{
		if(extraLabelsTable != null)
		{	
			return extraLabelsTable.getExtraLabel(extraLabelName);
		}
		return null;
	}

	
	public ExtraLabelsTable getExtraLabelsTable()
	{
		return extraLabelsTable;
	}
	
	
			
	public void relabel(String newLabel) {
		this.label = new Label(newLabel);
	}
	
	/**
	 * Replace all the occurances of oldLabel with newLabel in the
	 * current TreeNode. 
	 * @param oldLabel
	 * @param newLabel
	 */
	public void replaceLabels(String oldLabel, String newLabel) {
		if (this.label().equals(oldLabel))
		{	
			this.label = new Label(newLabel);
		}	
		if (this.daughters==null) return;
		for(TSNodeLabel d : this.daughters) {
			d.replaceLabels(oldLabel, newLabel);
		}
	}
	
	public boolean isHeadMarked() {
		return this.headMarked;
	}
	
	public boolean isTerminal() {
		return this.daughters == null;
	}
	
	public boolean isPreLexical() 
	{
		return ((this.daughters != null) && (this.daughters.length==1) && (this.daughters[0].isLexical));
	}
	
	public boolean isPreTerminal()
	{
		
		if(this.daughters != null)
		{
			for(TSNodeLabel daughter : this.daughters)
			{
				if(!daughter.isTerminal())
				{
					return false;
				}
			}
			return true;
		}
		return false;
	
	}
		
	public boolean hasOnlyPrelexicalDaughters() {
		if (this.daughters==null) return false;
		for(TSNodeLabel d : daughters) {
			if (!d.isPreLexical()) return false;
		}
		return true;
	}
	
	public String label() {
		return this.label.toString();
	}
	
	public String cfgRule() {
		String result = this.label();
		for(TSNodeLabel d : daughters) {
			result += " " + d.label(false, true);			
		}
		return result;
	}
	
	public TSNodeLabel cfgRuleNode() {
		TSNodeLabel result = new TSNodeLabel();
		result.label = this.label;				
		result.headMarked = this.headMarked;
		result.isLexical = this.isLexical;
		if (this.isTerminal()) return result;
		int prole = this.prole();
		result.daughters = new TSNodeLabel[prole];
		for(int i=0; i<prole(); i++) {			
			result.daughters[i] = new TSNodeLabel();
			result.daughters[i].parent = result;
			result.daughters[i].label = this.label;
			result.daughters[i].headMarked = this.headMarked;
			result.daughters[i].isLexical = this.isLexical;
		}
		return result;		
	}
	
	public SymbolList cfgRuleSymbol() {
		Vector<Symbol> rule = new Vector<Symbol>();
		rule.add(new SymbolString(this.label()));
		for(TSNodeLabel d : daughters) {
			rule.add(new SymbolString(d.label()));			
		}
		SymbolList result = new SymbolList(rule);
		return result;
	}
	
	public String compressToCFGRule() {
		StringBuilder sb = new StringBuilder(this.label());
		fillStringBuilderWithTerminals(sb);
		return sb.toString();
	}
	
	public void fillStringBuilderWithTerminals(StringBuilder sb) {
		if (this.isTerminal()) {
			sb.append(" " + this.label(false, true));
			return;
		}
		for(TSNodeLabel d  : this.daughters) {
			d.fillStringBuilderWithTerminals(sb);
		}
	}
	
	public String compressToCFGRulePosLex(char lexPosSeparatorChar) {
		StringBuilder sb = new StringBuilder(this.label());
		fillStringBuilderWithTerminalsPosLex(sb, lexPosSeparatorChar);
		return sb.toString();
	}
	
	public void fillStringBuilderWithTerminalsPosLex(StringBuilder sb, char lexPosSeparatorChar) {
		if (this.isLexical) {
			sb.append(" \"" + parent.label() + lexPosSeparatorChar + label() + "\"");
			return;
		}
		if (this.isTerminal()) {
			sb.append(" " + label());
			return;
		} 
		for(TSNodeLabel d  : this.daughters) {
			d.fillStringBuilderWithTerminalsPosLex(sb, lexPosSeparatorChar);
		}
	}
	
	/**
	 * @return true if the current TreeNodeLabel is a unique daughter, 
	 * i.e. it's parent has only one daughter.
	 */
	public boolean isUniqueDaughter() {
		TSNodeLabel parent = this.parent;
		if (parent==null) return false;
		return (this.parent.daughters.length==1);
	}

	
	public String label(boolean printHeads, boolean printLexiconInQuotes) {
		if (this.isLexical && printLexiconInQuotes) return "\"" + label() + "\"";
		if (!printHeads || !this.headMarked) return label();		
		return label() + "-H";
	}
	
	
	private String reorderingLabel()
	{
		String result = "";
		
		if(this.getNoDaughters() > 0)
		{	
			//System.out.println("TSNodeLabel.reorderingLabel  this.ownParentRelativePositionChanged" + this.ownParentRelativePositionChanged + " this.isTerminal() " + this.isTerminal());
			int noElements = this.relativeChildOrder.length;
			result += "#[";
			for(int i = 0; i < (noElements - 1); i++)
			{
				result += this.relativeChildOrder[i] + ",";
			}
			result += this.relativeChildOrder[noElements - 1] + "]#";
		}	
		
		return result;
	}
	
	public String labelWithReorderinInformation()
	{
		return this.reorderingLabel() + this.label();
	}
	
	  public String getTreeWithReorderingInformationString() {
	    	if (this.isTerminal())
	    	{	
	    		return labelWithReorderinInformation();
	    	}
	    	
	    	StringBuilder result = new StringBuilder("(" + labelWithReorderinInformation() + " ");
			int size = this.daughters.length;
			int i=0;
	    	for(TSNodeLabel p : this.daughters) {
	    		result.append(p.getTreeWithReorderingInformationString());
	    		if (++i!=size) result.append(" ");
	    	}
	    	result.append(")");
	    	return result.toString();
	    }
	
	
    public String toString() {
    	if (this.isTerminal()) return label();
    	StringBuilder result = new StringBuilder("(" + label() + " ");
		int size = this.daughters.length;
		int i=0;
    	for(TSNodeLabel p : this.daughters) {
    		result.append(p.toString());
    		if (++i!=size) result.append(" ");
    	}
    	result.append(")");
    	return result.toString();
    }
    
	public String toStringExtraParenthesis() {
		return "( " + this.toString() + ")";
		
	}
    
    public String toString(boolean printHeads, boolean printLexiconInQuotes) {
    	if (this.isLexical && printLexiconInQuotes) {
    		return "\"" + label() + "\"";
    	}
    	if (this.isTerminal()) return label();
    	StringBuilder result = new StringBuilder("(" + label(printHeads, printLexiconInQuotes) + " ");
		int size = this.daughters.length;
		int i=0;
    	for(TSNodeLabel p : this.daughters) {
    		result.append(p.toString(printHeads, printLexiconInQuotes));
    		if (++i!=size) result.append(" ");
    	}
    	result.append(")");
    	return result.toString();
	}
    
    public String toStringOneLevel() {
    	return toStringOneLevel(false,false);
    }
    
    public String toStringOneLevel(boolean printHeads, boolean printLexiconInQuotes) {
    	if (this.isLexical && printLexiconInQuotes) {
    		return "\"" + label() + "\"";
    	}
    	if (this.isTerminal()) return label();
    	StringBuilder result = new StringBuilder("(" + label(printHeads, printLexiconInQuotes) + " ");
		int size = this.daughters.length;
		int i=0;
    	for(TSNodeLabel p : this.daughters) {
    		result.append(p.label(printHeads, printLexiconInQuotes));
    		if (++i!=size) result.append(" ");
    	}
    	result.append(")");
    	return result.toString();
	}
    
    //non lexical non prelexical
    public int countInternalNodes() {		
		if (this.isPreLexical()) return 0;
		int result = 1;
		for(TSNodeLabel TN : this.daughters) {
			result += TN.countInternalNodes();
		}
		return result;
	}
    
	public int countAllNodes() {
		int result = 1;
		if (this.isTerminal()) return result; 
		for(TSNodeLabel TN : this.daughters) {
			result += TN.countAllNodes();
		}
		return result;
	}
	
	public int countLexicalNodes() {
		if (this.isLexical) return 1;		
		if (this.isTerminal()) return 0;
		int result = 0;
		for(TSNodeLabel TN : this.daughters) {
			result += TN.countLexicalNodes();
		}
		return result;
	}
	
	/**
	 * Returns the number of lexical nodes yielded by the current TreeNode,
	 * excluding the pos-tags present in the input array excludePosLabels which
	 * has to be sorted beforehand (as with Arrays.sort(excludeLexLabels)).
	 */
	public int countLexicalNodesExcludingPosLabels(String[] excludePosLabels) {
		int result = 0;
		if (this.isPreLexical()) {
			return  (Arrays.binarySearch(excludePosLabels, this.label())<0) ? 1 : 0;
		}
		for(TSNodeLabel TN : this.daughters) result += TN.countLexicalNodesExcludingPosLabels(excludePosLabels);
		return result;
	}
	
	public int countLexicalNodesExcludingCatLabels(String[] excludeLexLabels) {
		int result = 0;
		if (this.isLexical) {
			return  (Arrays.binarySearch(excludeLexLabels, this.parent.label())<0) ? 1 : 0;
		}
		for(TSNodeLabel TN : this.daughters) result += TN.countLexicalNodesExcludingCatLabels(excludeLexLabels);
		return result;
	}
	
	public int countTerminalNodes() {
		if (this.isTerminal()) return 1;		
		int result = 0;
		for(TSNodeLabel TN : this.daughters) {
			result += TN.countLexicalNodes();
		}
		return result;
	}
	
	/**
	 * Returns the number of POS which are equal in label with the 
	 * gold treee in input. The count excludes the gold labels present in the input array
	 * exludePOS which should be sorted beforehand (i.e. Arrays.sort(excludeLabels)).
	 */
	public int countCorrectPOS(TSNodeLabel TNgold, String[] exludePOS) {
		List<TSNodeLabel> thisLex = this.collectLexicalItems();
		List<TSNodeLabel> goldLex = TNgold.collectLexicalItems();
		int result = 0;
		for(int i=0; i<thisLex.size(); i++) {
			String thisPOS = thisLex.get(i).parent.label();
			String goldPOS = goldLex.get(i).parent.label();
			if (Arrays.binarySearch(exludePOS, goldPOS)>=0) continue;
			if (thisPOS.equals(goldPOS)) result++;
		}
		return result;
	}
	
	/**
	 * Returns a LinkedList of type TreeNodeLabel containing the lexical items of
	 * the current TreeNodeLabel. 
	 */
	public ArrayList<TSNodeLabel> collectLexicalItems() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectLexicalItems(result);
		return result;
	}
	
	/**
	 * Method to implement collectLexicalItems()
	 * @param lexItems
	 */
	private void collectLexicalItems(List<TSNodeLabel> lexItems) {
		if (this.isTerminal()) return;
		for(TSNodeLabel TN : this.daughters) {
			if (TN.isLexical) lexItems.add(TN);
			else TN.collectLexicalItems(lexItems);
		}		
	}
	
	/**
	 * Returns a LinkedList of type TreeNodeLabel containing the lexical items of
	 * the current TreeNodeLabel. 
	 */
	public ArrayList<TSNodeLabel> collectPreLexicalItems() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectPreLexicalItems(result);
		return result;
	}	
	
	/**
	 * Method to implement collectLexicalItems()
	 * @param lexItems
	 */
	private void collectPreLexicalItems(List<TSNodeLabel> prelexItems) {
		if (this.isTerminal()) return;
		if (this.isPreLexical()) prelexItems.add(this);
		else for(TSNodeLabel TN : this.daughters) {
			TN.collectPreLexicalItems(prelexItems);
		}		
	}
	
	public ArrayList<Label> collectPreLexicalLabels() {
		ArrayList<Label> result = new ArrayList<Label>();
		this.collectPreLexicalLabels(result);
		return result;
	}
	
	private void collectPreLexicalLabels(List<Label> prelexLabels) {
		if (this.isTerminal()) return;
		if (this.isPreLexical()) prelexLabels.add(this.label);
		else for(TSNodeLabel TN : this.daughters) {
			TN.collectPreLexicalLabels(prelexLabels);
		}		
	}
	
	/**
	 * Returns a LinkedList of type TreeNodeLabel containing the terminal items of
	 * the current TreeNodeLabel. 
	 */
	public ArrayList<TSNodeLabel> collectTerminalItems() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectTerminalItems(result);
		return result;
	}
	
	/**
	 * Method to implement collectTerminals()
	 * @param terminals
	 */
	private void collectTerminalItems(List<TSNodeLabel> terminals) {
		if (this.isTerminal()) return;
		for(TSNodeLabel TN : this.daughters) {
			if (TN.isTerminal()) terminals.add(TN);
			else TN.collectTerminalItems(terminals);
		}		
	}
	
	/**
	 * Returns a LinkedList of type TreeNodeLabel containing all items of
	 * the current TreeNode, besides the one being a unique daughter of its parent.
	 */	
	public ArrayList<TSNodeLabel> collectConstituentNodes() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectAllNodes(result);
		for(ListIterator<TSNodeLabel> i = result.listIterator(); i.hasNext(); ) {
			TSNodeLabel node = i.next();
			TSNodeLabel parent = node.parent;
			if (node.isLexical || node.isPreLexical() || (parent!=null && parent.prole()==1)) i.remove();			
		}
		return result;
	}
	
	/**
	 * Returns a ArrayList of type TreeNodeLabel containing all items of
	 * the current TreeNode not being lexical or prelexical. 
	 */
	public ArrayList<TSNodeLabel> collectInternalNodes() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectInternalNodes(result);
		return result;
	}
	
	/**
	 * Method to implement collectInternalNodes()
	 * @param terminals
	 */
	private void collectInternalNodes(List<TSNodeLabel> allNodes) {
		if (this.isPreLexical()) return;
		allNodes.add(this);
		if (this.isTerminal()) return;
		for(TSNodeLabel TN : this.daughters) {
			TN.collectInternalNodes(allNodes);
		}		
	}
	
	/**
	 * Returns a ArrayList of type TreeNodeLabel containing all items of
	 * the current TreeNode not being lexical 
	 */
	public ArrayList<TSNodeLabel> collectNonLexicalNodes() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectNonLexicalNodes(result);
		return result;
	}
	
	/**
	 * Method to implement collectNonLexicalNodes()
	 * @param terminals
	 */
	private void collectNonLexicalNodes(List<TSNodeLabel> allNodes) {
		if (this.isLexical) return;
		allNodes.add(this);		
		for(TSNodeLabel TN : this.daughters) {
			TN.collectNonLexicalNodes(allNodes);
		}		
	}
	
	
	/**
	 * Returns a LinkedList of type TreeNodeLabel containing all items of
	 * the current TreeNode. 
	 * 
	 * @return All items including and descending from the current node. 
	 * The order in which the nodes are returned guarantees that any node 
	 * precedes all its descendants in the list.
	 */
	public ArrayList<TSNodeLabel> collectAllNodes() {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		this.collectAllNodes(result);
		return result;
	}
	
	/**
	 * Method to implement collectTerminals()
	 * @param terminals
	 */
	private void collectAllNodes(List<TSNodeLabel> allNodes) {
		allNodes.add(this);
		if (this.isTerminal()) return;
		for(TSNodeLabel TN : this.daughters) {
			TN.collectAllNodes(allNodes);
		}		
	}
	
	
	public boolean sameLabel(TSNodeLabel nodeB) {		
		return this.label.equals(nodeB.label);
	}
	
	public boolean sameDaughtersLabel(TSNodeLabel nodeB) {
		if (this.daughters.length != nodeB.daughters.length) return false;
		for(int i=0; i<this.daughters.length; i++) {
			if (!this.daughters[i].sameLabel(nodeB.daughters[i])) return false;
		}
		return true;
	}
	
	public int hashCode() {
		int result = 31 + this.label.id;
		if (!this.isTerminal()) {
			for(TSNodeLabel d : this.daughters) {
				result = 31 * result + d.hashCode(); 
			}
		}
		return result;
	}
	
	public int prole() {
		if (this.daughters==null) return 0;
		return this.daughters.length;
	}
	
	public boolean equals(Object o) {
		if (o==this) return true;
		if (o instanceof TSNodeLabel) {
			TSNodeLabel anOtherNode = (TSNodeLabel) o;
			return this.equals(anOtherNode);
		}
		return false;		
	}
	
	public boolean equals(TSNodeLabel anOtherNode) {
		if (anOtherNode.label.id != this.label.id) return false;
		if (this.isTerminal() || anOtherNode.isTerminal()) {
			return this.isTerminal() == anOtherNode.isTerminal();
		}
		int thisProle = this.prole();
		if (anOtherNode.prole() != thisProle) return false;
		for(int d=0; d<thisProle; d++) {				
			if (!anOtherNode.daughters[d].equals(daughters[d])) return false;
		}
		return true;
	}
	
	public int maxDepth() {
		if (this.isTerminal()) return 0;		
		int maxDepth = 0;
		for(TSNodeLabel t : this.daughters) {
			int tDepth = t.maxDepth();
			if (tDepth > maxDepth) maxDepth = tDepth;
		}
		return maxDepth+1;
	}
	
	public int maxBranching() {
		if (this.isTerminal()) return 0;		
		int maxBranching = this.prole();
		for(TSNodeLabel t : this.daughters) {
			int tBranching = t.maxBranching();
			if (tBranching > maxBranching) maxBranching = tBranching;
		}
		return maxBranching;
	}
	
	/**
	 * Return the hight of the current TreeNodeLabel in the tree
	 */	
	public int hight() {		
		int hight = 0;
		TSNodeLabel TN = this;
		while(TN.parent!=null) {
			TN = TN.parent;
			hight++;
		}
		return hight;		
	}
	
	/**
	 * Remove the semantic tags of the type "-SBJ" in the labels TreeNodes within 
	 * the current treenode. 
	 */	
	public void removeSemanticTags() {
		if (this.isLexical) return;
		if (this.label.hasSemTags()) {
			this.relabel(TSNode.removeSemanticTag(label()));
		}
		for(TSNodeLabel D : this.daughters) D.removeSemanticTags();
	}
		
	public void removeHeadLabels() {
		this.headMarked = false;
		if (this.isTerminal()) return;		
		for(TSNodeLabel D : this.daughters) D.removeHeadLabels();
	}
	
	/**
	 * Remove rules of the kind X -> X
	 */
	public void removeRedundantRules() {
		if (this.isPreLexical()) return;
		if (this.prole()==1 && this.label.equals(this.daughters[0].label)) {
			this.daughters = this.daughters[0].daughters;
			for(TSNodeLabel D : this.daughters) D.parent = this;
			this.removeRedundantRules();
		}
		else {
			for(TSNodeLabel D : this.daughters) D.removeRedundantRules();
		}
	}
		
	public void replaceAllLabels(String regexMatch, String replace) {
		if (this.isTerminal()) return;
		String labelString = this.label();
		this.relabel(labelString.replaceAll(regexMatch, replace));
		for(int i=0; i<this.daughters.length; i++) this.daughters[i].replaceAllLabels(regexMatch, replace);
	}
	
	public void renameAllConstituentLabels(Label newLabel) {
		if (this.isLexical || this.isPreLexical()) return;
		this.label = newLabel;		
		for(int i=0; i<this.daughters.length; i++) {
			this.daughters[i].renameAllConstituentLabels(newLabel);
		}
	}
	
	public boolean checkHeadCorrespondance(TSNodeLabel o) {
		int prole = this.prole();
		if (prole==0) return true;
		for(int i=0; i<prole; i++) {
			TSNodeLabel d = this.daughters[i];
			TSNodeLabel dO = o.daughters[i];                               
			if (d.headMarked != dO.headMarked) {
				System.err.println("Not matching heads: " +
						"\n\t" + this.toStringOneLevel(true, false) +
						"\n\t" + o.toStringOneLevel(true, false));
				return false;
			}
			if (!d.checkHeadCorrespondance(dO)) return false;
		}
		return true;
	}
	
	public boolean rootsNode(TSNodeLabel terminal)
	{
		// if this node directly exactly matches the node, return succes
		if(this == terminal) // use == comparison instead of equals, since exact object-id equality is what is demanded here
		{
			return true;
		}
		else
		{
			if(this.daughters != null)
			{	
				// Recursively check if any of the daughters exactly matches the node
				for(TSNodeLabel child : this.daughters)
				{
					if(child.rootsNode(terminal))
					{
						return true;
					}
				}
			}
			
			return false;
		}
		
	}
	
	
	
	
	public boolean containsRecursive(TSNodeLabel otherTree) {
		if (this.containsNonRecursiveFragment(otherTree)) return true;
		if (this.isTerminal()) return false;
		for(TSNodeLabel d : this.daughters) {
			if (d.containsRecursive(otherTree)) return true;
		}
		return false;
	}
	
	public boolean containsNonRecursiveFragment(TSNodeLabel otherTree) {
		if (this.label.equals(otherTree.label)) {
			if (otherTree.isTerminal()) return true;
			if (this.isTerminal()) return false;
			int prole = this.prole();
			if (prole!=otherTree.prole()) return false;			
			for(int i=0; i<prole; i++) {
				TSNodeLabel thisDaughter = this.daughters[i];
				TSNodeLabel otherDaughter = otherTree.daughters[i];
				if (!thisDaughter.containsNonRecursiveFragment(otherDaughter)) return false;
			}
			return true;			
		}
		return false;
	}
	
	public int countRecursiveFragment(TSNodeLabel otherTree) {		
		int count = this.containsNonRecursiveFragment(otherTree) ? 1 : 0;
		if (this.isTerminal()) return count;
		for(TSNodeLabel d : this.daughters) {
			count += d.countRecursiveFragment(otherTree);
		}
		return count;
	}
	
	/**
	 * Computes the number of subTrees (fragements of indefinite depth) in the current TSNodeLabel.
	 * @return number of subTrees (DOP-like fragments)
	 */
	public BigInteger countSubTrees() {
		if (this.isLexical) return BigInteger.ZERO;
		int prole = this.prole();
		BigInteger result = this.daughters[0].countSubTrees().add(BigInteger.ONE);
		if (prole==1) return result;		
		for(int i=1; i<prole; i++) {
			BigInteger count = this.daughters[i].countSubTrees().add(BigInteger.ONE);
			result = result.multiply(count);			
		}
		return result;
	}
	
	/**
	 * Computes the number of subTrees (fragements of indefinite depth) in the current TSNodeLabel.
	 * @return number of subTrees (DOP-like fragments)
	 */
	public BigInteger[] countTotalFragments() {
		if (this.isLexical) return new BigInteger[]{BigInteger.ZERO,BigInteger.ZERO};	
		BigInteger[] result = new BigInteger[]{BigInteger.ONE,BigInteger.ZERO};
		
		for(TSNodeLabel d : this.daughters) {
			BigInteger[] countTotalFragmentsD = d.countTotalFragments();
			result[0] = result[0].multiply(countTotalFragmentsD[0].add(BigInteger.ONE));			
			result[1] = result[1].add(countTotalFragmentsD[1]);			
		}
		result[1] = result[1].add(result[0]);
		return result;
	}
	
	/**
	 * Computes the number of subTrees (fragements of indefinite depth) in the current TSNodeLabel and distribute them in depth
	 * @return number of subTrees (DOP-like fragments)
	 */
	public BigInteger[][] countTotalFragmentsDepth() {
		if (this.isPreLexical()) return new BigInteger[][]{{BigInteger.ONE},{BigInteger.ONE}};
		int prole = this.prole();
		BigInteger[][][] resultDaughers = new BigInteger[prole][][];
		int[] maxDepthDaughters = new int[prole];
		int currentMaxDepth = 0;
		for(int d=0; d<prole; d++) {
			BigInteger[][] resultD = daughters[d].countTotalFragmentsDepth();
			resultDaughers[d] = resultD;
			int maxDepthD = resultD[0].length;
			maxDepthDaughters[d] = maxDepthD;
			if (maxDepthD>currentMaxDepth) currentMaxDepth = maxDepthD;	
		}
		currentMaxDepth++;
		BigInteger[][] result = new BigInteger[2][currentMaxDepth];
		Arrays.fill(result[0], BigInteger.ZERO);
		Arrays.fill(result[1], BigInteger.ONE);
		int iPrevious = 0;
		for(int i=1; i<currentMaxDepth; i++) {
			for(int d=0; d<prole; d++) {
				int maxDepthD = maxDepthDaughters[d];
				boolean reachEndDepthOfDaughter = (maxDepthD<i); 
				BigInteger countDi =  null;
				if (reachEndDepthOfDaughter) countDi = resultDaughers[d][1][maxDepthD-1];
				else {
					countDi = resultDaughers[d][1][iPrevious];
					result[0][iPrevious] = result[0][iPrevious].add(resultDaughers[d][0][iPrevious]);
				}
				countDi = countDi.add(BigInteger.ONE);
				result[1][i] = result[1][i].multiply(countDi); 
			}
			iPrevious++;
		}
		result[0][0] = result[0][0].add(BigInteger.ONE);
		for(int i=1; i<currentMaxDepth; i++) {
			result[0][i] = result[0][i].add(result[1][i].subtract(result[1][i-1])); 
		}
		return result;
	}
	
	/**
	 * Computes the number of subTrees (fragements of indefinite depth) in the current TSNodeLabel and distribute them in depth
	 * @return number of subTrees (DOP-like fragments)
	 */
	public BigInteger[][] countTotalFragmentsDepthMaxBranching(int maxBranching) {
		if (this.isPreLexical()) return new BigInteger[][]{{BigInteger.ONE},{BigInteger.ONE}};
		int prole = this.prole();
		BigInteger[][][] resultDaughers = new BigInteger[prole][][];
		int[][] maxDepthsDaughters = new int[prole][2];
		int currentMaxDepth = 0;
		//boolean allMaxDepthAreMaxBranching = true;
		for(int d=0; d<prole; d++) {
			BigInteger[][] resultD = daughters[d].countTotalFragmentsDepthMaxBranching(maxBranching);
			resultDaughers[d] = resultD;
			int maxDepthD0 = resultD[0].length;
			int maxDepthD1 = resultD[1].length;
			maxDepthsDaughters[d] = new int[]{maxDepthD0,maxDepthD1};
			if (maxDepthD0>currentMaxDepth) currentMaxDepth = maxDepthD0;
			/*if (maxDepthD0>currentMaxDepth) {
				currentMaxDepth = maxDepthD0;
				allMaxDepthAreMaxBranching = resultD[1][0].equals(BigInteger.ZERO); 
			}
			else if (maxDepthD0==currentMaxDepth) {
				if (allMaxDepthAreMaxBranching && !resultD[1][0].equals(BigInteger.ZERO)) {
					allMaxDepthAreMaxBranching = false;
				}
			}*/
		}
		if (prole>maxBranching) {
			BigInteger[][] result = new BigInteger[][]{
					new BigInteger[currentMaxDepth], new BigInteger[]{BigInteger.ZERO}};
			Arrays.fill(result[0], BigInteger.ZERO);
			for(int d=0; d<prole; d++) {
				for(int i=0; i<maxDepthsDaughters[d][0]; i++) {
					result[0][i] = result[0][i].add(resultDaughers[d][0][i]);
				}
			}
			return result;
		}
		//if (!allMaxDepthAreMaxBranching) currentMaxDepth++;
		currentMaxDepth++;
		BigInteger[][] result = new BigInteger[2][currentMaxDepth];
		Arrays.fill(result[0], BigInteger.ZERO);
		Arrays.fill(result[1], BigInteger.ONE);
		int iPrevious = 0;
		for(int i=1; i<currentMaxDepth; i++) {
			for(int d=0; d<prole; d++) {
				int maxDepthD1 = maxDepthsDaughters[d][1];
				int maxDepthD0 = maxDepthsDaughters[d][0];
				BigInteger countDi = maxDepthD1<i ?
					resultDaughers[d][1][maxDepthD1-1] :
					resultDaughers[d][1][iPrevious];
				if (maxDepthD0>=i) {
					result[0][iPrevious] = result[0][iPrevious].add(resultDaughers[d][0][iPrevious]);
				}
				countDi = countDi.add(BigInteger.ONE);
				result[1][i] = result[1][i].multiply(countDi); 
			}
			iPrevious++;
		}
		result[0][0] = result[0][0].add(BigInteger.ONE);
		for(int i=1; i<currentMaxDepth; i++) {
			result[0][i] = result[0][i].add(result[1][i].subtract(result[1][i-1])); 
		}
		return result;
	}
	
	
	
	public static ArrayList<TSNodeLabel> getTreebank(File inputFile) throws Exception {
		return getTreebank(inputFile, FileUtil.defaultEncoding, Integer.MAX_VALUE);
	}
	
	public static ArrayList<TSNodeLabel> getTreebank(File inputFile, boolean allTermAreLex) throws Exception {
		return getTreebank(inputFile, FileUtil.defaultEncoding, Integer.MAX_VALUE, allTermAreLex);
	}
			
	public static ArrayList<TSNodeLabel> getTreebank(
			File inputFile, String encoding, int LL, boolean allTermAreLex) throws Exception {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		Scanner scan = FileUtil.getScanner(inputFile, encoding);
		//PrintProgress.start("Reading Treebank: ");
		while(scan.hasNextLine()) {			
			String line = scan.nextLine();
			if (line.equals("")) continue;
			//PrintProgress.next();
			//line = line.replaceAll("\\\\", "");
			TSNodeLabel lineStructure = new TSNodeLabel(line, allTermAreLex);
			if (lineStructure.countLexicalNodes() > LL) continue;
			result.add(lineStructure);
		}
		//PrintProgress.end();
		return result;
	}
	
	public static ArrayList<TSNodeLabel> getTreebank(
			File inputFile, String encoding, int LL) throws Exception {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		Scanner scan = FileUtil.getScanner(inputFile, encoding);
		//PrintProgress.start("Reading Treebank: ");
		while(scan.hasNextLine()) {			
			String line = scan.nextLine();
			if (line.equals("")) continue;			
			//PrintProgress.next();
			//line = line.replaceAll("\\\\", "");
			line = ConstCorpus.adjustParenthesisation(line);
			TSNodeLabel lineStructure = new TSNodeLabel(line);
			if (lineStructure.countLexicalNodes() > LL) continue;
			result.add(lineStructure);
		}
		//PrintProgress.end();
		return result;
	}
	
	public static ArrayList<TSNodeLabel> getTreebankFirst(
			File inputFile, int n) throws Exception {
		ArrayList<TSNodeLabel> result = new ArrayList<TSNodeLabel>();
		Scanner scan = FileUtil.getScanner(inputFile, FileUtil.defaultEncoding);
		PrintProgress.start("Reading Treebank: ");
		int i = 0;
		while(scan.hasNextLine() && i<n) {			
			String line = scan.nextLine();
			if (line.equals("")) continue;
			PrintProgress.next();
			TSNodeLabel lineStructure = new TSNodeLabel(line);
			result.add(lineStructure);
			i++;
		}
		PrintProgress.end();
		return result;
	}
	
	public static void printTreebankToFile(File outputFile,
			ArrayList<TSNodeLabel> constTreebank, boolean heads, boolean lexQuotes) {
		PrintWriter pw = FileUtil.getPrintWriter(outputFile);
		for(TSNodeLabel t : constTreebank) {
			pw.println(t.toString(heads, lexQuotes));
		}
		pw.close();
	}	
	
	public static void getTreebankComment(File treebankFile,
			ArrayList<TSNodeLabel> treebank, ArrayList<String> treebankComments) throws Exception {		
		Scanner scan = FileUtil.getScanner(treebankFile, FileUtil.defaultEncoding);
		PrintProgress.start("Reading Treebank: ");
		while(scan.hasNextLine()) {			
			String line = scan.nextLine();
			line = line.trim();			
			if (line.equals("") || line.charAt(0)!='(') continue;
			String[] lineSplit = line.split("\t");
 			PrintProgress.next();
			TSNodeLabel lineStructure = new TSNodeLabel(lineSplit[0]);			
			treebank.add(lineStructure);
			treebankComments.add(lineSplit.length==1 ? null : lineSplit[1]);
		}
		PrintProgress.end();
	}
	
	public static void getTreebankCommentFromString(String treebankString,
			ArrayList<TSNodeLabel> treebank, ArrayList<String> treebankComments) throws Exception {				
		PrintProgress.start("Reading Treebank: ");
		String[] treebankStringLines = treebankString.split("\n");
		for(String line : treebankStringLines) {			
			line = line.trim();			
			if (line.equals("") || line.charAt(0)!='(') continue;
			String[] lineSplit = line.split("\t");
 			PrintProgress.next();
			TSNodeLabel lineStructure = new TSNodeLabel(lineSplit[0]);			
			treebank.add(lineStructure);
			treebankComments.add(lineSplit.length==1 ? null : lineSplit[1]);
		}
		PrintProgress.end();
	}
	
	public static void makeTreebankClausureUnderSubsetRelation(
			File inputFile, File outputFile) throws Exception {
		ArrayList<TSNodeLabel> treebank = getTreebank(inputFile);
		PrintWriter pw = FileUtil.getPrintWriter(outputFile);
		ListIterator<TSNodeLabel> i1 = treebank.listIterator();
		PrintProgress.start("Making closure:");
		while(i1.hasNext()) {
			PrintProgress.next();
			TSNodeLabel t1 = i1.next();
			int nextIndex = i1.previousIndex()+1;
			ListIterator<TSNodeLabel> i2 = treebank.listIterator(nextIndex);
			boolean foundSuperSet=false;
			while(i2.hasNext()) {
				TSNodeLabel t2 = i2.next();
				if (t2.containsRecursive(t1)) {
					foundSuperSet = true;
					break;
				}
				if (t1.containsRecursive(t2)) {
					i2.remove();
				}
			}
			if (!foundSuperSet) pw.println(t1);
		}
		pw.close();
		PrintProgress.end();
	}
	
	public String toFlatSentence() {		
		ArrayList<TSNodeLabel> lexItems = this.collectLexicalItems();
		StringBuilder result = new StringBuilder();
		result.append(lexItems.get(0).label());
		if (lexItems.size()==1) return result.toString();
		ListIterator<TSNodeLabel> i = lexItems.listIterator(1);
		while(i.hasNext()) {
			result.append(" " + i.next().label());
		}
		return result.toString();
	}
	
	public String[] toFlatWordArray() {		
		ArrayList<TSNodeLabel> lexItems = this.collectLexicalItems();
		String[] result = new String[lexItems.size()];
		int i = 0;
		for(TSNodeLabel l : lexItems) {
			result[i] = l.label();
			i++;
		}
		return result;
	}
	
	public void adjustLexicalItems(String[] sentenceWords) {
		ArrayList<TSNodeLabel> lexItems = this.collectLexicalItems();
		Iterator<TSNodeLabel> lexIter = lexItems.iterator();
		for(String w : sentenceWords) {
			TSNodeLabel lexNode = lexIter.next();
			if (!lexNode.label().equals(w)) lexNode.label = new Label(w);
		}
	}
	
	public void removePreterminalWithPrefix(String prefix, int position) {
		if (this.isLexical) return;
		if (this.label().startsWith(prefix)) {
			TSNodeLabel lexDaughter = daughters[0];
			parent.daughters[position] = lexDaughter;
			lexDaughter.parent = this.parent;
			return;
		}
		position = 0;
		for(TSNodeLabel d : daughters) {			
			d.removePreterminalWithPrefix(prefix, position);
			position++;
		}		
	}
	
	public TSNodeLabel replaceRulesWithFragments(
			Hashtable<String, TSNodeLabel> ruleFragmentTable) {
		String cfgRule = this.cfgRule();
		TSNodeLabel fragment = ruleFragmentTable.get(cfgRule);		
		if (fragment==null) return this;
		TSNodeLabel result = fragment.clone();
		ArrayList<TSNodeLabel> terminals = result.collectTerminalItems();
		Iterator<TSNodeLabel> termIter = terminals.iterator();
		for(TSNodeLabel d : daughters) {
			TSNodeLabel term = termIter.next();
			if (term.isLexical) continue;
			TSNodeLabel subFragment = d.replaceRulesWithFragments(ruleFragmentTable);			
			term.daughters = subFragment.daughters;
			for(TSNodeLabel d1 : subFragment.daughters) {
				d1.parent = subFragment;
			}			
		}
		return result;
	}


	public int indexOfHeadDaughter() {
		int i=0;
		for(TSNodeLabel d : this.daughters) {
			if (d.headMarked) return i;
			i++;
		}
		return -1;
	}

	public boolean findLastDaughterIN() {		
		if (this.isTerminal()) return false;
		if (this.parent!=null && this.parent.label().equals("VP") && this.daughters[this.prole()-1].label().equals("IN")) return true;
		for(TSNodeLabel d : daughters) {
			if (d.findLastDaughterIN()) return true;
		}
		return false;
	}	
	


	public boolean yieldsOneWord() {
		if (this.isPreLexical()) return true;
		if (this.prole()>1) return false;
		return this.daughters[0].yieldsOneWord();
	}

	/**
	 * The current node is not a terminal, and not a preterminal 
	 * @return true if all the daughters are not preterminals
	 */
	public boolean allDaughtersAreNonPreterminal() {		
		for(TSNodeLabel d : this.daughters) {
			if (d.isPreLexical()) return false;
		}
		return true;
	}

	public TSNodeLabel getHeadDaughter() {
		if (this.daughters==null) return null;
		for(TSNodeLabel d : daughters) {
			if (d.headMarked) return d;
		}
		return null;
	}

	public int countNonPreterminalDaughters() {
		if (this.daughters==null) return 0;
		int result = 0;
		for(TSNodeLabel d : daughters) {
			if (!d.isPreLexical()) result++;
		}
		return result;
	}
	
	public TSNodeLabel nextWord() {
		TSNodeLabel current = this;
		TSNodeLabel ancestor = this.parent;		
		do {
			if (ancestor==null) return null;
			TSNodeLabel rightSister = current.getRightSister();
			if (rightSister==null) {
				current = ancestor;
				ancestor = ancestor.parent;
			}
			else {
				do {
					rightSister = rightSister.daughters[0];					
				} while (!rightSister.isLexical);
				return rightSister;
			}
		} while (true);
	}
	
	public TSNodeLabel previousWord() {
		TSNodeLabel current = this;
		TSNodeLabel ancestor = this.parent;		
		do {
			if (ancestor==null) return null;
			TSNodeLabel leftSister = current.getLeftSister();
			if (leftSister==null) {
				current = ancestor;
				ancestor = ancestor.parent;
			}
			else {
				do {
					leftSister = leftSister.daughters[leftSister.prole()-1];					
				} while (!leftSister.isLexical);
				return leftSister;
			}
		} while (true);
	}

	public TSNodeLabel getRightSister() {
		int prole = parent.prole();
		if (prole==1) return null;
		int lastChildIndex = prole-1;
		TSNodeLabel[] sibling = parent.daughters;
		for(int i=0; i<lastChildIndex; i++) {
			TSNodeLabel n = sibling[i];
			if (n==this) return sibling[i+1];
		}
		return null;
	}
	
	public TSNodeLabel getLeftSister() {
		int prole = parent.prole();
		if (prole==1) return null;		
		TSNodeLabel[] sibling = parent.daughters;
		TSNodeLabel previous = null;
		for(int i=0; i<prole; i++) {
			TSNodeLabel n = sibling[i];
			if (n==this) return previous;
			previous = n;
		}
		return null;
	}

	public int headCount() {
		if (daughters==null) return 0;
		int result = 0;		
		for(TSNodeLabel d : daughters) {
			if (d.headMarked) result++;
		}
		return result;
	}

	public void fillHeadDaughters(TSNodeLabel[] heads) {
		int i=0;
		for(TSNodeLabel d : daughters) {
			if (d.headMarked) heads[i++] = d;
		}		
	}
	
	/**
	 * Prune the subtrees labeled by the input label from the current tree 
	 */
	public boolean pruneSubTrees(String tag) {
		if (this.toString().indexOf(tag)==-1) return false;
		if (this.label().equals(tag)) return true;
		if (this.isTerminal()) return false;
		// contains traces
		ArrayList<TSNodeLabel> nonTraces = new ArrayList<TSNodeLabel>();
		for(int i=0; i<this.daughters.length; i++) {
			TSNodeLabel D = this.daughters[i];
			if (!D.pruneSubTrees(tag)) nonTraces.add(D);
		}
		if (nonTraces.size() == this.daughters.length) return false;
		if (nonTraces.isEmpty()) return true;
		if (nonTraces.size()==1) {
			TSNodeLabel uniqueDaughter = nonTraces.get(0);
			if (uniqueDaughter.sameLabel(this)) {
				this.daughters = uniqueDaughter.daughters;
				for(TSNodeLabel d : this.daughters) {
					d.parent = this;
				}
				return false;
			}
		}
		this.daughters = (TSNodeLabel[]) nonTraces.toArray(new TSNodeLabel[nonTraces.size()]);
		return false;
	}
	
	/**
	 * Prune the subtrees labeled by one of the label in the input array removeLabels
	 * from the current tree. The input array removeLabels should be sorted beforehand
	 * (as for instance with Arrays.sort(removeLabels); 
	 */
	public TSNodeLabel pruneSubTrees(String[] removeLabels) {
		if (Arrays.binarySearch(removeLabels, this.label())>=0) return this;
		if (this.isTerminal()) return null;
		// contains traces
		LinkedList<TSNodeLabel> nonTraces = new LinkedList<TSNodeLabel>();
		for(int i=0; i<this.daughters.length; i++) {
			TSNodeLabel D = this.daughters[i];
			if (D.pruneSubTrees(removeLabels)==null) nonTraces.add(D);
		}
		if (nonTraces.size() == this.daughters.length) return null;
		if (nonTraces.isEmpty()) return this;
		this.daughters = (TSNodeLabel[]) nonTraces.toArray(new TSNodeLabel[nonTraces.size()]);
		return null;
	}
	
	/**
	 * Returns the yield of the current TreeNode i.e. all the words
	 * it dominates separated by single spaces.
	 */
	public String getYield() {
		List<TSNodeLabel> terminals = this.collectLexicalItems();
		String result = "";
		for(TSNodeLabel TN : terminals) {
			result += TN.label() + " ";
		}
		result = result.trim();
		return result;
	}
	
	/**
	 * This method compute all subtrees up to depth maxDepth in 
	 * the current TreeNode.
	 * @param maxDepth maxDepth of subtrees
	 * @return a LinkedList of TreeNode each being a subtree of maximum depth maxDepth
	 */
	public ArrayList<String> allSubTrees(int maxDepth, int maxProle) {
		Pair<ArrayList<String>> duet = this.allSubTreesB(maxDepth, maxProle);
		ArrayList<String> result = duet.getFirst();
		result.addAll(duet.getSecond());
		return result;
	}
	
	/**
	 * Method to cumpute allSubTrees(int maxDepth))
	 */
	private Pair<ArrayList<String>> allSubTreesB(int maxDepth, int maxProle) {
		// subTrees[0] = NEW subTrees;
		// subTrees[1] = OLD subTrees;
		Pair<ArrayList<String>> subTrees = 
			new Pair<ArrayList<String>> (new ArrayList<String>(), new ArrayList<String>());
		if (this.isTerminal()) return subTrees;
		int prole = this.daughters.length;
		if (prole>maxProle) {
			for(TSNodeLabel TN : this.daughters) {
				Pair<ArrayList<String>> daugherSubTreesNewOld = TN.allSubTreesB(maxDepth, maxProle);
				subTrees.getSecond().addAll(daugherSubTreesNewOld.getFirst());
				subTrees.getSecond().addAll(daugherSubTreesNewOld.getSecond());
			}
			subTrees.getFirst().add("(" + this.cfgRule() + ")");
		}
		else {
			ArrayList<ArrayList<String>> daughterSubTrees = new ArrayList<ArrayList<String>>(prole);
			int[] daughterSubTreesSize = new int[prole];
			int index = 0;
			for(TSNodeLabel TN : this.daughters) {				
				Pair<ArrayList<String>> daugherSubTreesNewOld = TN.allSubTreesB(maxDepth, maxProle);			
				subTrees.getSecond().addAll(daugherSubTreesNewOld.getFirst());
				subTrees.getSecond().addAll(daugherSubTreesNewOld.getSecond());
				ArrayList<String> TNlist = daugherSubTreesNewOld.getFirst();
				TNlist.add(TN.label(false, true));
				daughterSubTrees.add(TNlist);
				for(ListIterator<String> l = TNlist.listIterator(); l.hasNext();) {
					String sT = (String)l.next();
					TSNode TNsT = new TSNode(sT, false);
					if (TNsT.maxDepth(true)==maxDepth) l.remove();
				}
				daughterSubTreesSize[index] = TNlist.size();
				index++;
			}
			int[][] combinations = Utility.combinations(daughterSubTreesSize);
			for(int i=0; i<combinations.length; i++) 
			{
				String rule = "(" + this.label(false, false) + " ";
				for(int j=0; j<prole; j++) 
				{
					String sT = (String)daughterSubTrees.get(j).get(combinations[i][j]);				
					rule += sT;
					rule += (j==prole-1) ? ")" : " ";
				
				}
				subTrees.getFirst().add(rule);
			}		
		}
		return subTrees;
	}

	public static Hashtable<String, HashSet<String>> getLexPosTableFromTreebank(
			ArrayList<TSNodeLabel> treebank) {
		Hashtable<String, HashSet<String>> result = new Hashtable<String, HashSet<String>>(); 
		for(TSNodeLabel t : treebank) {
			ArrayList<TSNodeLabel> lex = t.collectLexicalItems();
			for(TSNodeLabel l : lex) {
				String l_label = l.label(); 
				HashSet<String> lStoredPos = result.get(l_label);
				if (lStoredPos==null) {
					lStoredPos = new HashSet<String>();
					result.put(l_label, lStoredPos);
				}
				lStoredPos.add(l.parent.label());
			}
		}
		return result;
	}	
	
	public static HashSet<String> getPosSetFromTreebank(ArrayList<TSNodeLabel> treebank) {
		HashSet<String> result = new HashSet<String>(); 
		for(TSNodeLabel t : treebank) {
			ArrayList<TSNodeLabel> lex = t.collectLexicalItems();
			for(TSNodeLabel l : lex) {								
				result.add(l.parent.label());
			}
		}
		return result;
	}
	
	/**
	 * Returns a LinkedList of type String containing the terminals of
	 * the current TreeNode. 
	 */
	public List<String> collectTerminalStrings() {		
		List<TSNodeLabel> terminals = this.collectTerminalItems();
		List<String> result = new ArrayList<String>(terminals.size());
		for(TSNodeLabel TN : terminals) result.add(TN.toString(false, false));
		return result;
	}

	/**
	 * @return  The left most daughter of the current tree
	 */
	public TSNodeLabel firstDaughter() {
		return this.daughters[0];
	}
	
	/**
	 * @return The right most daughter of the current tree
	 */
	public TSNodeLabel lastDaughter() {
		return this.daughters[this.prole()-1];
	}
	
	/**
	 * @return The leftmost descendant of the current tree node
	 */
	public TSNodeLabel getLeftmostLexicon() {
		if (this.isLexical) return this;
		return this.firstDaughter().getLeftmostLexicon();		
	}
	
	/**
	 * @return  The leftmost descendant pre-lexical item of the current tree node
 	 */
	public TSNodeLabel getLeftmostPreLexicon() {
		if (this.isPreLexical()) return this;
		return this.firstDaughter().getLeftmostPreLexicon();		
	}
	
	/**
	 * @return  The rightmost descendant of the current tree node
	 */
	public TSNodeLabel getRightmostLexicon() {
		while(!this.isLexical) {
			return this.lastDaughter().getRightmostLexicon();
		}
		return this;
	}
	

	
	public static void printSearch(ArrayList<TSNodeLabel> treebank, TSNodeLabel target, int hits) {
		int count = 0;
		for(TSNodeLabel t : treebank) {
			if (t.containsRecursive(target)) {
				System.out.println(t);
				count++;
				if (count==hits) break;
			}
		}
	}
	
	
	public static int countFragmentInTreebank(ArrayList<TSNodeLabel> treebank, TSNodeLabel target) {
		int count = 0;
		for(TSNodeLabel t : treebank) {
			count += t.countRecursiveFragment(target);
		}
		return count;
	}
	
	public static void removeSemanticTags(ArrayList<TSNodeLabel> treebank) {
		for(TSNodeLabel t : treebank) t.removeSemanticTags();
	}
	
	public static void replaceLabelTreebank(ArrayList<TSNodeLabel> treebank, String oldLabel, String newLabel) {
		for(TSNodeLabel inputTree : treebank) {
			inputTree.replaceLabels(oldLabel, newLabel);
		}
	}
	
	
	public static void findInTUT() throws Exception {
		File treebankFile = new File(Parameters.corpusPath + "Evalita09/Treebanks/Constituency/" +
		"TUTinPENN-train.readable.notraces.quotesFixed.noSemTags.penn");
		ArrayList<TSNodeLabel> treebank = TSNodeLabel.getTreebank(treebankFile, "UTF-8",Integer.MAX_VALUE);
		TSNodeLabel target = new TSNodeLabel("(VMA~RE mette)"); 
		printSearch(treebank, target,100);
	}
	

	/**
	 * Check if there are coordinated structure in which the two coordinants are
	 * not specified as separate constituents. It only applies to constituents yielding
	 * PoS tags only.
	 
	 * @param CClabels
	 * @param ignoreInitialLabels
	 */
	public void fixCCPatterns(String[] CClabels, String[] ignoreInitialLabels) {
		if (this.isPreLexical()) return;
		BitSet indexesCC = this.indexDaughtersWithLabels(CClabels);	
		if (indexesCC!=null) {
			int prole = this.prole();
			int startIndex = 0;
			for(int di=0; di<prole; di++) {
				TSNodeLabel d = this.daughters[di];
				if (Arrays.binarySearch(ignoreInitialLabels, d.label())<0) {
					startIndex = di;
					break;
				}
			}			
			int patternsNumber = indexesCC.cardinality()+1;			
			Vector<ArrayList<String>> patterns = new Vector<ArrayList<String>>(patternsNumber);
			for(int i=0; i<patternsNumber; i++) patterns.add(new ArrayList<String>());
			int currentPatternIndex = 0;
			for(int di=startIndex; di<prole; di++) {
				if (indexesCC.get(di)) {
					currentPatternIndex++;
					continue;
				}
				TSNodeLabel d = this.daughters[di];
				patterns.get(currentPatternIndex).add(d.label());								
			}
			ArrayList<String> firstPattern = patterns.get(0);
			int patternSize = firstPattern.size();
			if (patternSize>1) {
				boolean equalPatterns = true;				
				for(int i=1; i<patternsNumber; i++) {
					if (!patterns.get(i).equals(firstPattern)) {
						equalPatterns = false;
						break;
					}
				}
				if (equalPatterns) {
					String groupsLabel = this.label();						
					String lineReport = "Building separate constituents for coordination patter: " + this.toStringOneLevel() + " ---> ";
					int bI = startIndex -2;
					for(int i=0; i<patternsNumber; i++) {
						bI += 2;
						int eI = bI + patternSize - 1;
						this.groupConsecutiveDaughters(bI, eI, groupsLabel);					
					}
					lineReport += this.toStringOneLevel();
					Parameters.logPrintln(lineReport);
				}
			}
		}	
		for(TSNodeLabel d : this.daughters) {
			d.fixCCPatterns(CClabels,ignoreInitialLabels);
		}
		
	}
	
	public int countDaughtersWithLabel(String[] labels) {
		if (this.isLexical) return 0;
		int count = 0;
		boolean found = false;
		for(TSNodeLabel d : this.daughters) {
			if (Arrays.binarySearch(labels, d.label())>=0) count++;
		}
		if (found) count++;
		return count;
	}
	
	public BitSet indexDaughtersWithLabels(String[] labels) {		
		BitSet result = new BitSet();
		for(int di=0; di<prole(); di++) {
			TSNodeLabel d = this.daughters[di];
			if (Arrays.binarySearch(labels, d.label())>=0) {
				result.set(di);
			}
		}
		return result.isEmpty() ? null : result;
	}
	
	public static int maxDepthTreebank(ArrayList<TSNodeLabel> treebank) {
		int maxDepth = -1;		
		for(TSNodeLabel t : treebank) {			
			int depth = t.maxDepth();
			if (depth>maxDepth) maxDepth = depth;
		}
		return maxDepth;
	}
	
	public static int maxBranchingTreebank(ArrayList<TSNodeLabel> treebank) {
		int maxBranching = -1;		
		for(TSNodeLabel t : treebank) {			
			int branching = t.maxBranching();
			if (branching>maxBranching) maxBranching = branching;
		}
		return maxBranching;
	}
	
	public void groupConsecutiveDaughters(int beginIndex, int endIndex, String groupLabel) {
		int groupSize = endIndex-beginIndex+1;
		int prole = this.prole();
		TSNodeLabel groupNode = new TSNodeLabel();
		groupNode.label = new Label(groupLabel);
		groupNode.parent = this;
		TSNodeLabel[] newGroupDaughters = new TSNodeLabel[groupSize];				 
		groupNode.daughters = newGroupDaughters;
		int k=0;
		for(int j=beginIndex; j<=endIndex; j++) {
			newGroupDaughters[k] = this.daughters[j];
			newGroupDaughters[k].parent = groupNode;
			k++;
		}
		int newProle = prole - groupSize + 1;
		TSNodeLabel[] newDaughters = new TSNodeLabel[newProle];
		k=0;
		for(int j=0; j<prole; j++) {
			if (j==beginIndex) {
				newDaughters[k++] = groupNode;
				continue;
			}
			if (j>beginIndex && j<=endIndex) continue;
			newDaughters[k++] = this.daughters[j];
		}
		this.daughters = newDaughters;
	}
	
	/**
	 * Labels should be PoS (e.g. NNP, NNPS)
	 * @param labels
	 * @param groupLabel
	 */
	public void groupConsecutiveDaughters(String[] labels, String groupLabel) 
	{
		groupConsecutiveDaughters(labels, groupLabel,0);
	}
	
	private void groupConsecutiveDaughters(String[] labels, String groupLabel, int startIndex) {
		if (this.isLexical) return;
		int beginIndex = -1;
		int endIndex = -1;
		int prole = this.prole();
		while(startIndex<prole) {
			TSNodeLabel d = this.daughters[startIndex];
			if (Arrays.binarySearch(labels, d.label())>=0) {
				if (beginIndex==-1) beginIndex = startIndex;
				endIndex = startIndex;
			}
			else {
				if (endIndex!=-1) break;				
			}
			startIndex++;
		}				
		if (beginIndex!=-1) {
			int groupSize = endIndex-beginIndex+1;
			if (groupSize>1 && groupSize!=prole) {
				String lineReport = "Grouped nodes: " + this.toStringOneLevel() + " --> ";
				groupConsecutiveDaughters(beginIndex, endIndex, groupLabel);				
				lineReport += this.toStringOneLevel();
				Parameters.logPrintln(lineReport);								
			}					
		}
		if (startIndex<prole) this.groupConsecutiveDaughters(labels, groupLabel, startIndex);
		else for(TSNodeLabel d : this.daughters) d.groupConsecutiveDaughters(labels, groupLabel,0);	
	}
	
	public void removeUniqueProductions(String[] labels) {
		if (this.isLexical) return;		
		int i = 0;
		for(TSNodeLabel d : daughters) {			
			if (d.hasUniqueDaugher(labels)) {				
				this.daughters[i] = this.daughters[i].daughters[0];
				this.daughters[i].parent = this;
			}
			else d.removeUniqueProductions(labels);
			i++;
		}				
	}
	
	public boolean hasUniqueDaugher(String[] labels) {
		if (this.isLexical || this.label().indexOf('-')>0) return false;
		if (this.prole()>1) return false;
		String firstLabel = this.daughters[0].label(); 
		return (Arrays.binarySearch(labels, firstLabel)>=0);
	}
	
	public boolean hasUniquePathToLexicalItem() {
		if (this.isLexical) return true;
		if (this.prole()>1) return false;
		return (this.daughters[0].hasUniquePathToLexicalItem());
	}
	
	public void fixPunctInNNP() {
		if (this.isLexical) return;
		if ((this.label().startsWith("NP") || this.label().startsWith("NML")) && this.prole()==2) {
			TSNodeLabel firstDaughter = this.daughters[0];
			TSNodeLabel secondDaughter = this.daughters[1];
			if (firstDaughter.label().startsWith("NNP") && secondDaughter.label().equals(".")) 
			{
				//String lineReport = "Fix dot in NNP: " + this.toStringOneLevel() + " --> ";
				TSNodeLabel term = firstDaughter.daughters[0];
				term.relabel(term.label()+"."); 			
				this.daughters = new TSNodeLabel[]{firstDaughter};
				//lineReport += this.toStringOneLevel();
			}				
		}
		for(TSNodeLabel d : daughters) {
			d.fixPunctInNNP();
		}
	}

	
	//public static String regexMatch = ".*[-=]\\d+.*";
	public static String dashEqualDigits = "[-=]\\d+";
	public static String nullTag = "-NONE-";	
	/**
	 * Remove removeNullProductions, removeNumbersInLables (traces), removeRedundantRules (X->X)
	 */
	public void makeCleanWsj() {	
		this.pruneSubTrees(nullTag);
		this.replaceAllLabels(dashEqualDigits, "");
		this.removeRedundantRules();		
	}
	
	public static Label WHNP_label = new Label("WHNP");
	
	public void findWHNP(int line) {
		if (this.isLexical) return;
		if (this.label.equals(WHNP_label) && prole()>1) System.out.println(line + ":" + this);
		for(TSNodeLabel d : daughters) d.findWHNP(line);
	}
	
	public static void findWHNP(File inputFile) throws Exception {
		Scanner scan = FileUtil.getScanner(inputFile);
		int lineNumber = 1;
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			TSNodeLabel tree = new TSNodeLabel(line);
			tree.findWHNP(lineNumber);
			lineNumber++;
		}
	}
	
	public void collectNodesPos(TreeSet<String> nodeLabels, TreeSet<String> posLabels) {		
		if (this.isLexical) return;
		if (this.isPreLexical()) {
			posLabels.add(this.label());
			return;
		}
		nodeLabels.add(this.label());
		for(TSNodeLabel d : daughters) d.collectNodesPos(nodeLabels, posLabels); 
	}
	
	public void markCircumstantial(String[] advSemTagSorted, String suffix) {
		if (this.isLexical || this.isPreLexical()) return;		
		String label = this.label();
		String[] semTags = label.split("[-=]");
		if (semTags.length>1) {
			for(int i=1; i<semTags.length; i++) {
				if (Arrays.binarySearch(advSemTagSorted, semTags[i])>=0) {
					String newLabel = semTags[0] + suffix + label.substring(semTags[0].length());					
					this.relabel(newLabel);
					break;
				}
			}
		}		
		for(TSNodeLabel d : daughters) d.markCircumstantial(advSemTagSorted, suffix);
	}	
	
	public boolean samePos(TSNodeLabel other) {
		ArrayList<TSNodeLabel> thisLex = this.collectLexicalItems();
		ArrayList<TSNodeLabel> otherLex = other.collectLexicalItems();
		if (thisLex.size()!=otherLex.size()) return false;
		Iterator<TSNodeLabel> thisLexIter = thisLex.iterator();
		Iterator<TSNodeLabel> otherLexIter = otherLex.iterator();
		while(thisLexIter.hasNext()) {
			TSNodeLabel thisL = thisLexIter.next();
			TSNodeLabel otherL = otherLexIter.next();
			if (!thisL.parent.label.equals(otherL.parent.label)) return false;
		}
		return true;
	}
	
	public static void removeSemanticTagsInTreebank(ArrayList<TSNodeLabel> treebank) {
		for(TSNodeLabel t : treebank) {
			t.removeSemanticTags();
		}						
	}
	
	public static void findInWSJ() throws Exception {
		File treebankFile = new File(Wsj.WsjOriginalReadable + "wsj-00.mrg");//"wsj-complete.mrg");		
		ArrayList<TSNodeLabel> treebank = TSNodeLabel.getTreebank(treebankFile);
		TSNodeLabel target = new TSNodeLabel("(VP * (S NP-SBJ NP-PRD))", false); 
		printSearch(treebank, target, Integer.MAX_VALUE);
	}
	
	public static void findInFragments(TSNodeLabel target) throws Exception {
		String dir = Parameters.resultsPath + "TSG/TSGkernels/Wsj/KenelFragments/SemTagOff_Top/all/";
		File fragmentsFile = new File(dir + "fragments_MUB_freq_all.txt");
		Scanner scan = FileUtil.getScanner(fragmentsFile);
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			if (line.equals("")) continue;
			String fragmentString  = line.split("\t")[0];
			TSNodeLabel fragment = new TSNodeLabel(fragmentString, false);
			if (fragment.containsRecursive(target)) System.out.println(line);
		}		
	}
	
	// Method added by Gideon that collects and returns a list of terminal descendants of a node
	public ArrayList<TSNodeLabel> getTerminalDescendants()
	{
		ArrayList<TSNodeLabel> collectedNodes = new ArrayList<TSNodeLabel>();
		ArrayList<TSNodeLabel> collectedTerminals  = new ArrayList<TSNodeLabel>();
		
		collectedNodes.add(this);
		
		while(collectedNodes.size() > 0)
		{
			// get and remove the first node
			TSNodeLabel n = collectedNodes.remove(0);
			
			if(n.isTerminal())
			{
				collectedTerminals.add(n);
			}
			else
			{
				// add all the daughters to the collectedNodes list
				for(TSNodeLabel daughter : n.daughters)
				{
					collectedNodes.add(daughter);
				}				
			}		
		}
		
		return collectedTerminals;
	}
	
	
	/**
	 * @return The source span for this node (only meaningful when computed by 
	 * calling computeSourceSpans for the tree root first)
	 */
	public Pair<Integer> getSourceSpan()
	{
		return new Pair<Integer>(this.sourceSpanMin, this.sourceSpanMax);
	}
	
	/**
	 * The method computeSourceSpans computes the source spans i.e. minimal and maximal 
	 * terminal/leaf/word positions covered for each node 
	 */
	public void computeSourceSpans()
	{
		// Get the terminal items, which are ordered from left to right
		List<TSNodeLabel> terminalItems = this.collectTerminalItems();
		
		int pos = 0;
		// First we compute and set the source spans for the lexical items
		for(TSNodeLabel node : terminalItems)
		{
			node.sourceSpanMin = node.sourceSpanMax =  pos;
			pos++;
		}
		
		// The method collectAllNodes returns the nodes in such a way that parents precede all their
		// descendants, which is a handy property we will use to bottom up
		// compute the sourceSpanMin and sourceSpanMax values
		List<TSNodeLabel>  allNodesParentsPrecedeDescendants = this.collectAllNodes();
		
		// Loop trough allNodesParentsPrecedeDescendants in reverse order
		for(int i =  allNodesParentsPrecedeDescendants.size() - 1; i >= 0; i--)
		{
			TSNodeLabel node = allNodesParentsPrecedeDescendants.get(i);
			
			// Loop over the node's children, and update the sourceSpanMin and 
			// sourceSpanMax with the min and max values they have
			
			if(!node.isTerminal())
			{	
				for(TSNodeLabel child : node.daughters)
				{
					node.sourceSpanMin = Math.min(child.sourceSpanMin, node.sourceSpanMin);
					node.sourceSpanMax = Math.max(child.sourceSpanMax, node.sourceSpanMax);
				}
			}	
		}
	}
	
	public static int findObjectPositionInArray(Object[] array, Object object)
	{
		for(int i = 0; i < array.length; i++)
		{
			
			//System.out.println("\n\n" + array[i] + "\n equals \n" + object +  "   ?");
			
			if(array[i].equals(object))
			{
				return i;
			}
		}
		
		return -1;
	}

	public void flattenParentByRemovingNodeAndInsertingChildren()
	{
		TSNodeLabel parent = this.parent;
		TSNodeLabel[] daughters = this.daughters;
		
		// Find the position of the node in the parents array of daughters
		int parentRelativePos = findObjectPositionInArray(this.parent.daughters, this);
		
		int oldNumParentDaughters = parent.daughters.length;
		int numNodeDaughters = daughters.length;
		int newNumParentDaughters = oldNumParentDaughters + numNodeDaughters - 1;
		int numParentDaughtersAfterNode =  oldNumParentDaughters - parentRelativePos - 1;
		
		// new array for the daughters of the parent
		TSNodeLabel[] newParentDaughters = new TSNodeLabel[newNumParentDaughters];
		
		
		// Copy the preceding daughters into the new parent daughters array
		for(int i = 0; i <  parentRelativePos; i++)
		{
			newParentDaughters[i] = parent.daughters[i];
		}
		
		// Copy the daughters of the internalNode under the parent
		for(int i = parentRelativePos, j = 0; i < newNumParentDaughters -  numParentDaughtersAfterNode; i++, j++)
		{
			newParentDaughters[i] = daughters[j];
		}
		
		
		for(int i = parentRelativePos + numNodeDaughters, j = parentRelativePos  + 1; i < newNumParentDaughters; i++, j++)
		{
			newParentDaughters[i] = parent.daughters[j];
		}
		
		// replace the old daughters of parent with the new set
		parent.daughters = newParentDaughters;
		
		// Finally set the parent right in the new daughters
		for(int i = 0; i < parent.daughters.length; i++)
		{
			parent.daughters[i].parent = parent;
		}
	}
	
	public int getNoDaughters()
	{
		if(this.daughters == null)
		{
			return 0;
		}
		return this.daughters.length;
	}
	
	public static void main(String[] args) throws Exception {
    	//findInWSJ();  // commented out by Gideon    	
    	//findInFragments(new TSNodeLabel("(NP DT JJ NN CC NN NN NN)", false));
    	//findPartialFragment(3150, "gave");
    	
    	//String tree = "(S (NP-SBJ (DT The) (NN luxury) (NN auto) (NN maker)) (NP-TMP (JJ last) (NN year)) (VP (VBD sold) (NP (CD 1,214) (NNS cars)) (PP-LOC (IN in) (NP (DT the) (NNP U.S.)))))";
    	//String partialFragmetn = "(VP (VBD \"sold\") NP)";
    	
    }



	


		

}
