/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package tsg;

public class TSNodeLabelFreq {

	private TSNodeLabel tree;
	private int freq;
	
	public TSNodeLabelFreq(TSNodeLabel tree, int freq) {
		this.tree = tree;
		this.freq = freq;
	}
	
	public TSNodeLabel tree() {
		return tree;
	}
	
	public int freq() {
		return freq;
	}
	
	public String toString(boolean printHeads, boolean printLexiconInQuotes) {
		return freq + "\t" + tree.toString(printHeads, printLexiconInQuotes);
	}
		
}
