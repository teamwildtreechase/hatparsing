/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package tsg;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

import util.Locking;

public class Label {

	private final static ConcurrentMap<String, Integer> IDtableLabel = new ConcurrentHashMap<String, Integer>(16, (float) 0.75, 32);
	private final static List<String> IDtableReverseLabel = new ArrayList<String>();
	private final static List<Label> labelVector = new ArrayList<Label>();
	// The minimum wait time for a lock to report a warning
	private static final long MinWaitTimeLockWarning = 500;
	private static final boolean UseFairLockingStrategy = true;
	private final static ReentrantLock lock = new ReentrantLock(UseFairLockingStrategy);

	protected static int labelCounter;

	public int id;

	public static int getLabelCounter() {
		return labelCounter;
	}

	public Label(String label) {
		Integer storedId = IDtableLabel.get(label);
		if (storedId == null) {
			storedId = putLabelIfAbsent(label, this);
		}
		this.id = storedId;
	}

	// Gideon : This method is nescessary to assure thread-safety when used in multi-threaded code
	// We need to make the method static to assure synchronization at class level
	public static Integer putLabelIfAbsent(String label, Label labelInstance) {
		acquireLockWitmMaxWaitTimeWarning();
		try {
			Integer storedId = IDtableLabel.get(label);
			if (storedId == null) {
				storedId = labelCounter++;
				IDtableLabel.put(label, storedId);
				IDtableReverseLabel.add(label);
				labelVector.add(labelInstance);
			}
			return storedId;
		} finally {
			releaseLock();
		}
	}

	/**
	 * Assign a new id to the current label. Careful: all the nodes associated with this label will be renamed.
	 * 
	 * @param newLabel
	 */
	public void relabel(String newLabel) {
		Integer storedId = IDtableLabel.get(newLabel);
		if (storedId == null) {
			storedId = putLabelIfAbsent(newLabel, this);
		}
		this.id = storedId;
	}

	public String toString() {
		assert (IDtableReverseLabel.get(this.id) != null);
		return IDtableReverseLabel.get(this.id);
	}

	public String toStringWithoutSemTags() {
		String labelString = this.toString();
		int dash_index = labelString.indexOf('-');
		if (dash_index > 0) { // avoid to deal with -NONE-, -RRB-, -RCB-, ...
			return labelString.substring(0, dash_index);
		}
		return labelString;
	}

	public String[] getSemTags() {
		return this.toString().split("[-=]");
	}

	public boolean hasSemTags() {
		return this.toString().indexOf('-') > 0;
	}

	public Label getLabelWithoutSemTags() {
		String labelString = this.toString();
		int dashIndex = labelString.indexOf('-');
		if (dashIndex == -1)
			return this;
		return new Label(labelString.substring(0, dashIndex));
	}

	public boolean equals(Object anObject) {
		if (this == anObject) {
			return true;
		}
		if (anObject instanceof Label) {
			Label anotherLabel = (Label) anObject;
			return this.id == anotherLabel.id;
		}
		return false;
	}

	public int hashCode() {
		return id;
	}

	public boolean equals(Label anotherLabel) {
		return this.id == anotherLabel.id;
	}

	protected static void acquireLockWitmMaxWaitTimeWarning() {
		Locking.acquireLockWitmMaxWaitTimeWarning(lock, MinWaitTimeLockWarning, Label.class, "Label");
	}

	protected static void releaseLock() {
		lock.unlock();
	}

}
