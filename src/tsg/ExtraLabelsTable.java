/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package tsg;

import extended_bitg.TreeRepresentation;
import util.Span;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.Pair;

public class ExtraLabelsTable 
{

	private final Map<String,String> extraLabelsMap;
	
	private ExtraLabelsTable( Map<String,String> extraLabelsMap)
	{
		this.extraLabelsMap = extraLabelsMap;
	}
	
	public static ExtraLabelsTable createExtraLabelsTable(String labelAndExtraLabels)
	{
		return new ExtraLabelsTable(extractLabelAndExtraLabelsMap(labelAndExtraLabels));
	}
	
	private static Span getFirstPatternMatchSpan(String contentsString, String patternString)
	{
		Span result = null;
		 //Pattern pattern =  Pattern.compile("\\@.*\\@");
		Pattern pattern =  Pattern.compile(patternString); 
		Matcher matcher = pattern.matcher(contentsString);
		
		boolean found = matcher.find();
		
		if(!found)
		{
			 return result;
		}
		else
		{			
			return new Span(matcher.start(), matcher.end() -1);
		}	
	}
	
	public static Pair<String> extractLabelAndExtraLabelsString(String labelAndExtraLabels)
	{
		Pair<String> result;
		
		assert(labelAndExtraLabels != null);
		Span extraLabelsMatchSpan = getFirstPatternMatchSpan(labelAndExtraLabels, "<ExtraLabels>.*</ExtraLabels>");
		if(extraLabelsMatchSpan != null)
		{
			String label = labelAndExtraLabels.substring(0,extraLabelsMatchSpan.getFirst()) +  labelAndExtraLabels.substring(extraLabelsMatchSpan.getSecond() + 1);
			String extraLabels = labelAndExtraLabels.substring(extraLabelsMatchSpan.getFirst(), extraLabelsMatchSpan.getSecond() + 1);
			result = new Pair<String>(label,extraLabels); 
		}
		else
		{
			result = new Pair<String>(labelAndExtraLabels,"");
			
		}
		
		return result;
	}	
	
	public static String extractLabelString(String labelAndExtraLabels)
	{
		String result =  extractLabelAndExtraLabelsString(labelAndExtraLabels).getFirst();
		assert(!result.contains("ExtraLabels"));
		return result;
	}

	private static String getExtraLabelsStringWithoutTags(String extraLabelsStringWithTags)
	{
		String result = "";
		if(extraLabelsStringWithTags.length() > 0 )
		{	
			String leftTag = TreeRepresentation.getExtraLabelsLeftTag();
			String rightTag = TreeRepresentation.getExtraLabelsRightTag(); 
			result = extraLabelsStringWithTags.substring(extraLabelsStringWithTags.indexOf(leftTag) + leftTag.length(),extraLabelsStringWithTags.indexOf(rightTag));
		}	
		return result;
	}
	
	private static Pair<String> getLabelNameLabelPair(String extraLabelString)
	{
		System.out.println("extraLabelString: " + extraLabelString);
		Span firstTagSpan = getFirstPatternMatchSpan(extraLabelString, "<\\w*>");
		Span secondTagSpan = getFirstPatternMatchSpan(extraLabelString, "</\\w*>");
		String labelName = extraLabelString.substring(firstTagSpan.getFirst() + 1, firstTagSpan.getSecond());
		String label = extraLabelString.substring(firstTagSpan.getSecond() + 1,secondTagSpan.getFirst());
		return new Pair<String>(labelName,label);
	}
	
	
	
	
	public static Map<String,String> extractLabelAndExtraLabelsMap(String labelAndExtraLabels)
	{
		Pair<String> labelAndExtraLabelsString;
		Map<String,String> result = new HashMap<String, String>();
		try 
		{
			labelAndExtraLabelsString = extractLabelAndExtraLabelsString(labelAndExtraLabels);
			assert(labelAndExtraLabelsString.getSecond() != null);
			String extraLabelsWithoutTags = getExtraLabelsStringWithoutTags(labelAndExtraLabelsString.getSecond());
			
			if(extraLabelsWithoutTags.length() > 0 )
			{	
				String[] extraLabels = extraLabelsWithoutTags.split("&");
				
				for(String extraLabelString : extraLabels)
				{
					Pair<String> labelNameLabelPair = getLabelNameLabelPair(extraLabelString);
					result.put(labelNameLabelPair.getFirst(), labelNameLabelPair.getSecond());
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public String getExtraLabel(String extraLabelName)
	{
		return extraLabelsMap.get(extraLabelName);
	}

	public String toString()
	{
		String result = "";
		for(Entry<String,String> entry : this.extraLabelsMap.entrySet())
		{
			result += entry + ",";
		}
		return result;
	}
}
