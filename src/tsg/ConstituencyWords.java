/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package tsg;
import java.util.*;

import util.Utility;

public class ConstituencyWords extends Constituency{
	int initialIndex, finalIndex;
	
	public ConstituencyWords(TSNodeLabel node, int initialIndex, int finalIndex, boolean countLabel) {
		super(node, countLabel);
		this.initialIndex = initialIndex;
		this.finalIndex = finalIndex;
	}
	
	public Constituency unlabeledCopy() {
		return new ConstituencyWords(node, initialIndex, finalIndex, false);
	}
	
	/**
	 * Returns a LinkedList of type Constituency containing the constituencies
	 * of the current treeNode excluding the labels present in the input array
	 * excludeLabels which should be sorted beforehands (i.e. Arrays.sort(excludeLabels)).
	 * If labeled is false we still exclude categories excludeLabels from statistics as in EvalB
	 * const_type: 0 (WORDS), 1 (CHARACTERS), 2 (YIELD)
	 */
	public static ArrayList<Constituency> collectConsituencies(TSNodeLabel tree, boolean labeled, 
			String[] excludeLabels, boolean includePos) {
		ArrayList<Constituency> result = new ArrayList<Constituency>();
		List<String> lexicalLabels = null;
		List<TSNodeLabel> lexicalNodes = tree.collectLexicalItems();
		lexicalLabels = tree.collectTerminalStrings();
		for(int i=0; i<lexicalNodes.size(); i++) {
			TSNodeLabel LN = lexicalNodes.get(i);
			LN.relabel(Integer.toString(i));
		}
		List<TSNodeLabel> allNodes = tree.collectAllNodes();		
		for(TSNodeLabel NT : allNodes) {
			if (NT.isLexical) continue;
			if (NT.isPreLexical() && !includePos) continue;
			if (Arrays.binarySearch(excludeLabels, NT.label())>=0) continue;
			int start = Integer.parseInt(NT.getLeftmostLexicon().label());
			int end = Integer.parseInt(NT.getRightmostLexicon().label());										
			result.add(new ConstituencyWords(NT, start, end, labeled));			
		}
		for(int i=0; i<lexicalNodes.size(); i++) {
			TSNodeLabel LN = lexicalNodes.get(i);
			LN.relabel(lexicalLabels.get(i));
		}
		return result;
	}
	
	
	public boolean isCrossing(ConstituencyWords c) {
		return (	this.initialIndex < c.initialIndex && 
					(this.finalIndex>=c.initialIndex && this.finalIndex < c.finalIndex) 
				||
					this.initialIndex>c.initialIndex && 
					(this.initialIndex<=c.finalIndex && this.finalIndex > c.finalIndex )
				);
	}

	public static int updateCrossingBrackets(List<Constituency> testConst, 
			List<Constituency> goldConst,  Hashtable<String, Integer> crossingBracketsCatTable) {
		int result = 0;
		for(Constituency testC : testConst) {
			for(Constituency goldC : goldConst) {
				if (((ConstituencyWords)testC).isCrossing((ConstituencyWords)goldC)) {
					result++;
					if (crossingBracketsCatTable!=null) {
						String key = testC.node.label() + "/" + goldC.node.label();
						Utility.increaseStringInteger(crossingBracketsCatTable, key, 1);
					}					
					break;
				}
			}
		}
		return result;
	}
	
	public boolean equals(Object anObject) {
		if (this == anObject) {
		    return true;
		}
		if (anObject instanceof ConstituencyWords) {
			ConstituencyWords c = (ConstituencyWords)anObject;
		    if ( (!countLabel || c.node.sameLabel(this.node)) && 
		    		c.initialIndex==this.initialIndex 
		    		&& c.finalIndex==this.finalIndex) return true;		    
		}		
		return false;
	}

	
	public static ArrayList<Constituency> toYieldConstituency(ArrayList<Constituency> list, 
			TSNodeLabel tree, boolean labeled) {
		ArrayList<Constituency> result = new ArrayList<Constituency>();
		ArrayList<TSNodeLabel> lexicon = tree.collectLexicalItems();
		for(Constituency C : list) {
			ConstituencyWords IC = (ConstituencyWords)C;
			List<TSNodeLabel> subLex = lexicon.subList(IC.initialIndex, IC.finalIndex+1);
			String yield = "";
			for(TSNodeLabel lex : subLex) yield += lex + " ";
			yield = yield.trim();
			result.add(new ConstituencyYield(IC.node, new Yield(yield), labeled));
		}
		return result;
	}
	
	public String toString() {
		return "(" + this.node.label() + ": " + this.initialIndex + "-" + this.finalIndex + ")";
	}
}
