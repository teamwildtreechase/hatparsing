/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package tsg;

import java.util.List;

import util.InputReading;

public class TreeStringConsistencyTester 
{

	/**
	 * Method that checks whether the tree and the string are consistent, that
	 * is the tree has the same number of terminal nodes as the String has words
	 * 
	 * @param s
	 * @return Whether the tree and the String are consistent
	 */
	public static boolean treeIsConsistenWithString(String s, TSNodeLabel tree) 
	{
		int numWordsString = InputReading.determineNumWordsString(s);
		int numWordsTree = tree.countTerminalNodes();

		List<String> terminalLabels = tree.collectTerminalStrings();

		// Check that the list of terminal labels does not contain the null
		// terminal
		if (terminalLabels.contains("null")) {
			return false;
		}

		if (numWordsString == numWordsTree) {
			return true;
		} 
		else 
		{
			return false;
		}
	}

	
}
