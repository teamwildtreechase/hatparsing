package hat_database;

public class HATsDBTableInputFileLocations {

	private final String languagePairDescriptionString;
	private final String hatPropertiesTableName, hatPropertiesFileName;
	private final CorpusFileLocations corpusFileLocations;

	private HATsDBTableInputFileLocations(String hatPropertiesTableName, String hatPropertiesFileName, CorpusFileLocations corpusFileLocations, String languagePairDescriptionString) {
		this.hatPropertiesTableName = hatPropertiesTableName;
		this.hatPropertiesFileName = hatPropertiesFileName;
		this.corpusFileLocations = corpusFileLocations;
		this.languagePairDescriptionString = languagePairDescriptionString;
	}

	public static HATsDBTableInputFileLocations createHATsDBTableInputFileLocations(String fullCorpusLocation,String hatPropertiesTableName, String hatPropertiesFileName, String sourceFileName,
			String targetFileName, String alignmentFileName, String languagePairDescriptionString) {
		return new HATsDBTableInputFileLocations(hatPropertiesTableName, hatPropertiesFileName, CorpusFileLocations.createCorpusFileLocations(fullCorpusLocation, sourceFileName, targetFileName,
				alignmentFileName), languagePairDescriptionString);
	}

	public static HATsDBTableInputFileLocations createStandardHATsDBTableInputFileLocations(CorpusFileLocations corpusFileLocations, String languagePairDescriptionString) {
		return new HATsDBTableInputFileLocations(createStandardHATPropertiesTableName(languagePairDescriptionString), createStandardHATPropertiesFileName(languagePairDescriptionString),
				corpusFileLocations, languagePairDescriptionString);
	}

	public static String createStandardHATPropertiesTableName(String languagePairDescriptionString) {
		return "hatProperties_" + languagePairDescriptionString;
	}

	public static String createStandardHATPropertiesFileName(String languagePairDescriptionString) {
		return "hatProperties" + languagePairDescriptionString + ".txt";
	}

	public String getFullCorpusLocation() {
		return this.corpusFileLocations.getCorpusLocation();
	}

	public String getHATPropertiesFileName() {
		return this.hatPropertiesFileName;
	}

	public String getHATPropertiesTablesName() {
		return this.hatPropertiesTableName;
	}

	public String getSourceFileLocation() {
		return this.corpusFileLocations.getSourceFileLocation();
	}

	public String getTargetFileLocation() {
		return this.corpusFileLocations.getTargetFileLocation();
	}

	public String getAlignmentsFileLocation() {
		return this.corpusFileLocations.getAlignmentsFileLocation();
	}

	public String getLanguagePairDescriptionString() {
		return languagePairDescriptionString;
	}

	public CorpusFileLocations getCorpusFileLocations()
	{
		return this.corpusFileLocations;
	}
}
