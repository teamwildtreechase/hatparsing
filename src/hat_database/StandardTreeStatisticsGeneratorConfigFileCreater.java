package hat_database;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import util.FileUtil;

public class StandardTreeStatisticsGeneratorConfigFileCreater {

	private static final String CONFIGURATION_HEADER_STRING = "%*************************" + "\n%This is an automatically generated configuration file, "
			+ "\n%used with MultiThreadTreeStatisticsGenerator to compute alignment statistics" + "\n%and a list with HAT properties, that is used to build a HAT Properties database"
			+ "\n%*************************";

	private static final String HATS_STATISTICS_NAME = "HATsStatistics";

	private final CorpusFileLocations corpusFileLocations;
	private final String languagePairDescriptionString;
	private final String rootOutputFolderPathForConfigurationFile;
	private final String outputFilePath;

	private StandardTreeStatisticsGeneratorConfigFileCreater(CorpusFileLocations corpusFileLocations, String languagePairDescriptionString, String rootOutputFolderPathForConfigurationFile,
			String outputFilePath) {
		this.corpusFileLocations = corpusFileLocations;
		this.languagePairDescriptionString = languagePairDescriptionString;
		this.rootOutputFolderPathForConfigurationFile = rootOutputFolderPathForConfigurationFile;
		this.outputFilePath = outputFilePath;
	}

	public static StandardTreeStatisticsGeneratorConfigFileCreater createStandardTreeStatisticsGeneratorConfigFileCreater(CorpusFileLocations corpusFileLocations,
			String languagePairDescriptionString, String rootOutputFolderPathForConfigurationFile, String outputFilePath) {
		return new StandardTreeStatisticsGeneratorConfigFileCreater(corpusFileLocations, languagePairDescriptionString, rootOutputFolderPathForConfigurationFile, outputFilePath);
	}

	private String getHATPropertiesFileName() {
		return HATsDBTableInputFileLocations.createStandardHATPropertiesFileName(this.languagePairDescriptionString);
	}

	private String getTreeStatisticsOutputFolder() {
		return HATS_STATISTICS_NAME + "-" + this.languagePairDescriptionString;
	}

	private String getTreeStatisticsFileName() {
		return HATS_STATISTICS_NAME + "-" + this.languagePairDescriptionString + ".csv";
	}

	public void writeConfigurationFile() {
		BufferedWriter fileWriter = null;
		try {

			fileWriter = new BufferedWriter(new FileWriter(outputFilePath));
			fileWriter.write(CONFIGURATION_HEADER_STRING + "\n\n");
			fileWriter.write("corpusLocation =  " + corpusFileLocations.getCorpusLocation() + "\n");
			fileWriter.write("sourceFileName =  " + corpusFileLocations.getSourceFileName() + "\n");
			fileWriter.write("targetFileName =  " + corpusFileLocations.getTargetFileName() + "\n");
			fileWriter.write("alignmentsFileName =  " + corpusFileLocations.getAlignmentsFileName() + "\n");
			fileWriter.write("hatPropertiesFileName = " + getHATPropertiesFileName() + "\n");
			fileWriter.write("treeStatisticsOutputFolder = " + getTreeStatisticsOutputFolder() + "\n");
			fileWriter.write("treeStatisticsFileName = " + getTreeStatisticsFileName() + "\n");
			fileWriter.write("rootOutputFolderPathForConfigurationFile = " + rootOutputFolderPathForConfigurationFile);
			fileWriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(fileWriter);
		}
	}
}
