/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class HatSelectionPanelButtonArea extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	protected final JButton performQueryButton, resetQueryButton, resetFromButton;
	protected JCheckBox useOriginalCorperaSpeedupCheckBox;
	
	protected HatSelectionPanelButtonArea(HATSelectionPanel containingPanel)
	{
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		

		
		this.useOriginalCorperaSpeedupCheckBox = new JCheckBox("Use original corpera for selection speedup", false);
		this.add(useOriginalCorperaSpeedupCheckBox);
		
		this.add(new JLabel("Query Execution"));
	
		JPanel buttonContainingPanel = new JPanel();
		buttonContainingPanel.setLayout(new BoxLayout(buttonContainingPanel,BoxLayout.X_AXIS));
		
		resetFromButton = createButton(containingPanel,"Reset FROM Field");
		buttonContainingPanel.add(resetFromButton);
		buttonContainingPanel.add(Box.createHorizontalStrut(30));
		
		resetQueryButton = createButton(containingPanel,"Reset WHERE Field");
		buttonContainingPanel.add(resetQueryButton);
		buttonContainingPanel.add(Box.createHorizontalStrut(30));
		
		performQueryButton = createButton(containingPanel,"Perform Selection");
		buttonContainingPanel.add(performQueryButton);
		this.add(buttonContainingPanel);
		

	}

	public static JButton createButton(ActionListener containingPanel, String buttonText,Dimension preferredSize)
	{
		JButton result  = new JButton(buttonText);
		result.addActionListener(containingPanel);
		result.setPreferredSize(preferredSize);
		return result;
	}
	
	public static JButton createButton(ActionListener containingPanel, String buttonText)
	{
		return createButton(containingPanel, buttonText, new Dimension(100,50));
	}
	
	
}
