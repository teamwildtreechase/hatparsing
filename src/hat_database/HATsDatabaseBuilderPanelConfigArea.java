package hat_database;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import util.ConfigCacher;

public class HATsDatabaseBuilderPanelConfigArea extends JPanel {

	public static final String HATS_DATABASE_BUILDER_PANEL_CONFIG_SPECIFICATOIN_PROPERTY = "hatsDatabaseBuilderConfigSpecficification";

	private static final String EXAMPLE_DATABASE_CONFIGURATION_CONFIGFILE_STRING = "% Below configuration file specifies the file locations nescessary for database building" + "\n"
			+ "% Comments, such as this line start with %. " + "\n" + "% Multiple corpera corresponding to multiple language pairs can be specified\n"
			+ "% It is important that all file specifications follow the same order over the language pairs" + "\n"
			+ "% (i.e. the source, target and alignment file for the first language pair should all come in first position)" + "\n\n\n" + "% Comma separated list of full paths to corpera folders"
			+ "\n" + "fullCorperaLocations = ./HATsDatabaseBuilderTestCorpus1/,./HATsDatabaseBuilderTestCorpus2/" + "\n\n"
			+ "% Comma separated list of source file names (these must be inside the full corpus locations)" + "\n" + "fullCorperaSourceNames = sourceFile.txt,sourceFile.txt" + "\n"
			+ "% Comma separated list of target file names (these must be inside the full corpus locations)" + "\n" + "fullCorperaTargetNames = targetFile.txt,targetFile.txt" + "\n"
			+ "% Comma separated list of alignment file names (these must be inside the full corpus locations)" + "\n" + "fullCorperaAlignmentsNames = alignmentFile.txt,alignmentFile.txt" + "\n"
			+ "% Comma separated list of language descriptor Strings" + "\n" + "languagePairDescriptionStrings = S1_T1,S2_T2" + "\n" + "% Root path were the generated files are put under" + "\n"
			+ "rootOutputFolderPath  = ./DB_Experiments_Folder/" +"\n\n" 
			+ "% Path specification of the created database file\n" 
			+ "hatPropertiesDatabaseLocation = ./hatsPropertiesDatabase.db";

	private static final long serialVersionUID = 1L;
	private final JTextArea configSpecificationField;

	private HATsDatabaseBuilderPanelConfigArea(JTextArea configSpecificationField) {
		this.configSpecificationField = configSpecificationField;
		this.add(new JLabel("Database Builder Configuration"));
		this.add(configSpecificationField);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	public static HATsDatabaseBuilderPanelConfigArea createHATsDatabaseBuilderPanelConfigArea(ConfigCacher configCacher) {
		JTextArea configSpecificationField = new JTextArea();
		configSpecificationField.setPreferredSize(new Dimension(1600, 500));
		configSpecificationField.setMaximumSize(new Dimension(1600, 500));
		configSpecificationField.setMinimumSize(new Dimension(1600, 500));
		configSpecificationField.setBackground(Color.WHITE);

		configCacher.registerEntry(HATS_DATABASE_BUILDER_PANEL_CONFIG_SPECIFICATOIN_PROPERTY, EXAMPLE_DATABASE_CONFIGURATION_CONFIGFILE_STRING, configSpecificationField);
		configCacher.updateRegisteredTextFields();
		return new HATsDatabaseBuilderPanelConfigArea(configSpecificationField);
	}

	public String getConfigFieldContentsAsString() {
		return this.configSpecificationField.getText();
	}

	public void resetConfigFieldContentsToDefault() {
		this.configSpecificationField.setText(EXAMPLE_DATABASE_CONFIGURATION_CONFIGFILE_STRING);
	}

}
