package hat_database;

public class CorpusFileLocations {

	private final String corpusLocation, sourceFileName, targetFileName, alignmentsFileName;

	private CorpusFileLocations(String corpusLocation, String sourceFileName, String targetFileName, String alignmentFileName) {
		this.corpusLocation = corpusLocation;
		this.sourceFileName = sourceFileName;
		this.targetFileName = targetFileName;
		this.alignmentsFileName = alignmentFileName;
	}

	public static CorpusFileLocations createCorpusFileLocations(String corpusLocation, String sourceFileName, String targetFileName, String alignmentsFileName) {
		return new CorpusFileLocations(corpusLocation, sourceFileName, targetFileName, alignmentsFileName);
	}

	public String getCorpusLocation() {
		return this.corpusLocation;
	}

	public String getSourceFileName() {
		return this.sourceFileName;
	}

	public String getSourceFileLocation() {
		return this.corpusLocation + sourceFileName;
	}

	public String getTargetFileName() {
		return this.targetFileName;
	}

	public String getTargetFileLocation() {
		return this.corpusLocation + targetFileName;
	}

	public String getAlignmentsFileName() {
		return this.alignmentsFileName;
	}

	public String getAlignmentsFileLocation() {
		return this.corpusLocation + this.alignmentsFileName;
	}

}
