/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.io.File;
import java.util.Map;

import alignment.ConfigFileCreater;

public class HatsDatabaseAutomaticConfigfileCreater extends ConfigFileCreater
{
	private static final String GeneratedFilesFolder = "HATsDBGeneratedFiles/";
	
	HatsDatabaseAutomaticConfigfileCreater(Map<String,String> configFileProperties, String configFilePath)
	{
		super(configFileProperties, configFilePath);
	}
	
	public static HatsDatabaseAutomaticConfigfileCreater createHatsDatabaseAutomaticConfigFileCreater(File hatsDatabaseFile)
	{
		String corpusLocation = getCorpusLocation(hatsDatabaseFile);
		HatsDatabaseAutomaticConfigfileCreater result = new HatsDatabaseAutomaticConfigfileCreater(createStandardBasicConfigFileMap(corpusLocation),getStandardConfigFilePath(corpusLocation));
		result.addHatDBConfigfileProperties(hatsDatabaseFile);
		return result;
	}
	
	private static String getContainingFolder(File databaseFile)
	{
		return databaseFile.getParent();
	}
	
	private static String getCorpusLocation(File databaseFile)
	{
		String corpusLocation = getContainingFolder(databaseFile) + "/" + GeneratedFilesFolder;
		System.out.println("corpusLocation: " + corpusLocation);
		return corpusLocation;
	}

	private void addHatDBConfigfileProperties(File hatsDatabaseFile)
	{
		this.addProperty(HatDBProperties.DatabaseLocationPropertyName, hatsDatabaseFile.getAbsolutePath());
		this.addProperty(HatDBProperties.SelectedSubCorpusLocationProperty, this.getCorpusLocation());
	}
	
	
	
}
