package hat_database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import alignment.ArtificialCorpusCreator;
import alignment.ArtificialCorpusProperties;
import alignmentStatistics.MultiThreadTreeStatisticsGenerator;
import util.ConfigFile;
import util.FileUtil;


public class HATsDatabaseBuilder {

	public static String HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME = "./HATsDatabaseBuilderTestCorpus1/";
	public static String HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME2 = "./HATsDatabaseBuilderTestCorpus2/";

	public static final String ROOT_OUTPUT_FOLDER_PATH_PROPERTY = "rootOutputFolderPath";
	private final HATsDBFullInputFileLocations haTsDBFullInputFileLocations;
	private final String databaseLocation;

	private HATsDatabaseBuilder(HATsDBFullInputFileLocations haTsDBFullInputFileLocations, String databaseLocation) {
		this.haTsDBFullInputFileLocations = haTsDBFullInputFileLocations;
		this.databaseLocation = databaseLocation;
	}

	public static HATsDatabaseBuilder createHATsDatabaseBuilder(HATsDBFullInputFileLocations haTsDBFullInputFileLocations, String databaseLocation) {
		return new HATsDatabaseBuilder(haTsDBFullInputFileLocations, databaseLocation);
	}

	public static HATsDatabaseBuilder createHATsDatabaseBuilder(ConfigFile theConfig) {
		String databaseLocation = theConfig.getStringIfPresentOrReturnDefault(HatDBProperties.DatabaseLocationPropertyName, "test.db");
		return new HATsDatabaseBuilder(HATsDBFullInputFileLocations.createStandardHaTsDBInputFileLocations(theConfig), databaseLocation);
	}

	/**
	 * Recompute the HATs properties database
	 */
	public String computeDatabase() {

		for (HATsDBTableInputFileLocations tableFileLocations : haTsDBFullInputFileLocations.getTableInputFileLocationsList()) {
			String tableName = tableFileLocations.getHATPropertiesTablesName();
			String hatPropertyFileName = tableFileLocations.getHATPropertiesFileName();

			System.out.println("hatPropertyFileName: " + hatPropertyFileName);
			DatabaseInterface dbInterface = new DatabaseInterface(databaseLocation);
			dbInterface.createHATsTableFromFiles(hatPropertyFileName, tableFileLocations.getSourceFileLocation(), tableFileLocations.getTargetFileLocation(),
					tableFileLocations.getAlignmentsFileLocation(), tableName);
			// close the connection to the database
			dbInterface.closeInterface();
		}
		
		try {
			String fullPathDatabaseLocation = new File(databaseLocation).getCanonicalPath();
			return fullPathDatabaseLocation;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static String generatedConfigurationFileName(String rootOutputFolderPath, String languagePairDescriptionString) {
		String result = rootOutputFolderPath + "hatsStatisticsComputationConfigFile-" + languagePairDescriptionString + ".txt";
		return result;
	}

	public static void computeHATsPropertiesFileForCorpus(CorpusFileLocations corpusFileLocations, String languagePairDescriptionString, String rootOutputFolderPath) {
		StandardTreeStatisticsGeneratorConfigFileCreater.createStandardTreeStatisticsGeneratorConfigFileCreater(corpusFileLocations, languagePairDescriptionString, rootOutputFolderPath,
				generatedConfigurationFileName(rootOutputFolderPath, languagePairDescriptionString)).writeConfigurationFile();
		MultiThreadTreeStatisticsGenerator.createMultiThreadTreeStatisticsGenerator(generatedConfigurationFileName(rootOutputFolderPath, languagePairDescriptionString), 1).produceStatistics(true);
	}

	public static HATsDBFullInputFileLocations computeHATsPropertiesFilesForCorpus(String configFilePath) {
		ConfigFile theConfig;
		try {
			theConfig = new ConfigFile(configFilePath);
			String rootOutputFolderPath = theConfig.getValueWithPresenceCheck(ROOT_OUTPUT_FOLDER_PATH_PROPERTY);
			FileUtil.createFolderIfNotExisting(rootOutputFolderPath);
			HATsDBFullInputFileLocations haTsDBFullInputFileLocations = HATsDBFullInputFileLocations.createStandardHaTsDBInputFileLocations(theConfig);

			for (HATsDBTableInputFileLocations hatsDBTableInputFileLocations : haTsDBFullInputFileLocations.getTableInputFileLocationsList()) {
				String languagePairDescriptionString = hatsDBTableInputFileLocations.getLanguagePairDescriptionString();
				computeHATsPropertiesFileForCorpus(hatsDBTableInputFileLocations.getCorpusFileLocations(), languagePairDescriptionString, rootOutputFolderPath);
			}
			return haTsDBFullInputFileLocations;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public static void computeSingleLanuagePairDatabase(CorpusFileLocations corpusFileLocations, String languagePairDescriptionString, String rootOutputFolderPathForConfigurationFile,
			String databaseLocation) {
		computeHATsPropertiesFileForCorpus(corpusFileLocations, languagePairDescriptionString, rootOutputFolderPathForConfigurationFile);
		HATsDBFullInputFileLocations haTsDBFullInputFileLocations = HATsDBFullInputFileLocations.createHaTsDBInputFileLocations(Arrays.asList(HATsDBTableInputFileLocations
				.createStandardHATsDBTableInputFileLocations(corpusFileLocations, languagePairDescriptionString)));
		HATsDatabaseBuilder haTsDatabaseBuilder = createHATsDatabaseBuilder(haTsDBFullInputFileLocations, databaseLocation);
		haTsDatabaseBuilder.computeDatabase();
	}

	public static String computeMultipleLanguagePairsDatabase(String configFilePath, String databaseLocation) {
		HATsDBFullInputFileLocations haTsDBFullInputFileLocations = computeHATsPropertiesFilesForCorpus(configFilePath);
		HATsDatabaseBuilder haTsDatabaseBuilder = createHATsDatabaseBuilder(haTsDBFullInputFileLocations, databaseLocation);
		return haTsDatabaseBuilder.computeDatabase();
	}

	public static List<String> createTestExampleCorpera() {
		// Create two different artificial corpera
		try {
			List<String> corperaLocationsList = new ArrayList<>();
			ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();
			ArtificialCorpusProperties artificialCorpusProperties2 = ArtificialCorpusProperties.createArtificialCorpusProperties2();
			ArtificialCorpusCreator.createArtificialCorpusCreator(artificialCorpusProperties, HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME);
			ArtificialCorpusCreator.createArtificialCorpusCreator(artificialCorpusProperties2, HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME2);

			String pathCorpus1 = new File(HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME).getCanonicalPath();
			String pathCorpus2 = new File(HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME2).getCanonicalPath();
			corperaLocationsList.add(pathCorpus1);
			corperaLocationsList.add(pathCorpus2);

			return corperaLocationsList;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
}
