package hat_database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;
import util.ConfigFile;

public class HATsDBFullInputFileLocations {

	public static final String LANGUAGE_PAIR_DESCRPITION_STRINGS_PROPERTY = "languagePairDescriptionStrings";
	private final List<HATsDBTableInputFileLocations> tableInputFileLocationsList;

	private HATsDBFullInputFileLocations(List<HATsDBTableInputFileLocations> tableInputFileLocationsList) {
		this.tableInputFileLocationsList = tableInputFileLocationsList;
	}

	public static HATsDBFullInputFileLocations createHaTsDBInputFileLocations(List<HATsDBTableInputFileLocations> tableInputFileLocationsList) {
		return new HATsDBFullInputFileLocations(tableInputFileLocationsList);
	}

	private static List<String> createStandardHATPropertiesTableNamesList(List<String> languagePairDescriptionStrings) {
		List<String> result = new ArrayList<>();
		for (String languagePairDescriptionString : languagePairDescriptionStrings) {
			result.add(HATsDBTableInputFileLocations.createStandardHATPropertiesTableName(languagePairDescriptionString));
		}
		return result;
	}

	private static List<String> createStandardHATPropertiesFileNamesList(List<String> languagePairDescriptionStrings) {
		List<String> result = new ArrayList<>();
		for (String languagePairDescriptionString : languagePairDescriptionStrings) {
			result.add(HATsDBTableInputFileLocations.createStandardHATPropertiesFileName(languagePairDescriptionString));
		}
		return result;
	}

	private static HATsDBFullInputFileLocations createHaTsDBInputFileLocations(List<String> fullCorperaLocations, List<String> fullCorperaSourceNames, List<String> fullCorperaTargetNames,
			List<String> fullCorperaAlignmentsNames, List<String> hatPropertiesFileNames, List<String> hatPropertiesTableNames, List<String> languagePairDescriptionStrings) {

		// Check that all input lists have the same number of elements
		List<List<String>> listOfLists = Arrays.asList(fullCorperaAlignmentsNames, hatPropertiesFileNames, hatPropertiesTableNames, fullCorperaSourceNames, fullCorperaTargetNames,
				fullCorperaAlignmentsNames);
		Assert.assertTrue(allListsInListHaveEqualLength(listOfLists));

		List<HATsDBTableInputFileLocations> tableInputFileLocationsList = new ArrayList<>();
		for (int i = 0; i < fullCorperaLocations.size(); i++) {
			HATsDBTableInputFileLocations haTsDBTableInputFileLocations = HATsDBTableInputFileLocations.createHATsDBTableInputFileLocations(
					fullCorperaLocations.get(i),
					hatPropertiesTableNames.get(i),
					hatPropertiesFileNames.get(i), fullCorperaSourceNames.get(i), fullCorperaTargetNames.get(i), fullCorperaAlignmentsNames.get(i),
					languagePairDescriptionStrings.get(i));
			tableInputFileLocationsList.add(haTsDBTableInputFileLocations);
		}

		return new HATsDBFullInputFileLocations(tableInputFileLocationsList);
	}

	public static HATsDBFullInputFileLocations createHaTsDBInputFileLocationsSpecifyingAllFileNamesExplicitly(ConfigFile theConfig) {

		List<String> fullCorperaLocations = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaLocationsPropertyName);
		List<String> hatPropertiesFileNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.HatPropertiesFileNamesPropertyName);
		List<String> hatPropertiesTableNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.HatPropertiesTableNamesPropertyName);
		List<String> fullCorperaSourceNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaSourceNamesPropertyName);
		List<String> fullCorperaTargetNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaTargetNamesPropertyName);
		List<String> fullCorperaAlignmentsNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaAlignmentsNamesPropertyName);
		List<String> languagePairDescriptionStrings = theConfig.getMultiValueParmeterAsList(LANGUAGE_PAIR_DESCRPITION_STRINGS_PROPERTY);

		return createHaTsDBInputFileLocations(fullCorperaLocations, fullCorperaSourceNames, fullCorperaTargetNames, fullCorperaAlignmentsNames, hatPropertiesFileNames, hatPropertiesTableNames,
				languagePairDescriptionStrings);
	}

	public static HATsDBFullInputFileLocations createStandardHaTsDBInputFileLocations(ConfigFile theConfig) {

		List<String> fullCorperaLocations = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaLocationsPropertyName);
		List<String> fullCorperaSourceNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaSourceNamesPropertyName);
		List<String> fullCorperaTargetNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaTargetNamesPropertyName);
		List<String> fullCorperaAlignmentsNames = theConfig.getMultiValueParmeterAsList(HatDBProperties.FullCorperaAlignmentsNamesPropertyName);
		List<String> languagePairDescriptionStrings = theConfig.getMultiValueParmeterAsList(LANGUAGE_PAIR_DESCRPITION_STRINGS_PROPERTY);
		List<String> hatPropertiesFileNames = createStandardHATPropertiesFileNamesList(languagePairDescriptionStrings);
		List<String> hatPropertiesTableNames = createStandardHATPropertiesTableNamesList(languagePairDescriptionStrings);

		
		System.out.println("FULLCORPERALOCATIONS:" + fullCorperaLocations);
		return createHaTsDBInputFileLocations(fullCorperaLocations, fullCorperaSourceNames, fullCorperaTargetNames, fullCorperaAlignmentsNames, hatPropertiesFileNames, hatPropertiesTableNames,
				languagePairDescriptionStrings);
	}

	private static <T> boolean allListsInListHaveEqualLength(List<List<T>> listOfLists) {
		if (!listOfLists.isEmpty()) {
			int firstElementSize = listOfLists.get(0).size();
			for (int i = 1; i < listOfLists.size(); i++) {
				if (listOfLists.get(i).size() != firstElementSize) {
					return false;
				}
			}
		}
		return true;
	}

	public HATsDBTableInputFileLocations getTableInputFileLocations(int i) {
		return this.tableInputFileLocationsList.get(i);
	}

	public List<HATsDBTableInputFileLocations> getTableInputFileLocationsList() {
		return this.tableInputFileLocationsList;
	}
}
