/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;
import alignmentStatistics.HATProperties;
import extended_bitg.EbitgTreeState.TreeType;

/**
 * This class serves to check if a certain condition holds for a certain HATProperties object.
 * Furthermore it checks whether the condition is a well formed and meaning full one and 
 * throws an exception otherwise. 
 * @author gemaille
 *
 */
public class HATPropertyChecker 
{

	private String propertyName;
	private String conditionName;
	private String conditionValue;
	
	
	public HATPropertyChecker(String propertyName, String conditionName, String conditionValue)
	{
		this.propertyName = propertyName;
		this.conditionName = conditionName;
		this.conditionValue = conditionValue;
	}
	
	
	
	public boolean checkProperty(HATProperties properties)
	{
		Object propertyValue = properties.getValue(propertyName);
		
		try 
		{
		
			if(propertyValue instanceof Integer)
			{
				return checkIntegerProperties((Integer)propertyValue,conditionName,conditionValue);
			}
			else if(propertyValue instanceof TreeType)
			{
				
				return checkTreeTypeProperties((TreeType)propertyValue,conditionName,conditionValue);
				
			}
			else if(propertyValue instanceof Boolean)
			{
				checkBooleanProperties((Boolean)propertyValue,conditionName,conditionValue);
			}
			else
			{
				throw new Exception("Error: unknown type of property value or property value could not be found");
			}
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	public boolean checkIntegerProperties(int propertyValue, String conditionName, String conditionValueString) throws Exception
	{
		int conditionValue = Integer.parseInt(conditionValueString);
		
		if(conditionName.equals("=="))
		{
			return(propertyValue == conditionValue);
		}
		else if(conditionName.equals("<="))
		{
			return(propertyValue <= conditionValue);
		}
		else if(conditionName.equals(">="))
		{
			return(propertyValue >= conditionValue);
		}
		else if(conditionName.equals("<"))
		{
			return(propertyValue < conditionValue);
		}
		else if(conditionName.equals(">"))
		{
			return(propertyValue > conditionValue);
		}
	
		throw new Exception("Error: unknown condition for integer value");
	}
	
	public boolean checkTreeTypeProperties(TreeType propertyValue, String conditionName, String conditionValueString) throws Exception
	{
		if(conditionName.equals("=="))
		{
			return(propertyValue.equals(HATProperties.parseTreeType(conditionValueString)));
		}
		
		throw new Exception("Unknown condition for TreeType" + conditionName);
	}
	
	public boolean checkBooleanProperties(Boolean propertyValue, String conditionName, String conditionValueString) throws Exception
	{
		if(conditionName.equals("=="))
		{
			return(propertyValue == Boolean.parseBoolean(conditionValueString));
		}
		
		throw new Exception("Unknown condition for Boolean value" + conditionName);
	
	}
	
}
