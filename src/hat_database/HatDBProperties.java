/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

public class HatDBProperties 
{
	public static final String SelectionQueryFileNamePropertyName = "selectionQueryFileName";
	public static final String DatabaseLocationPropertyName = "hatPropertiesDatabaseLocation";
	public static final String FullCorperaLocationsPropertyName = "fullCorperaLocations";
	public static final String HatPropertiesFileNamesPropertyName = "hatPropertiesFileNames";
	public static final String HatPropertiesTableNamesPropertyName = "hatPropertiesTableNames";
	public static final String FullCorperaSourceNamesPropertyName = "fullCorperaSourceNames";
	public static final String FullCorperaTargetNamesPropertyName = "fullCorperaTargetNames";
	public static final String FullCorperaAlignmentsNamesPropertyName = "fullCorperaAlignmentsNames";
	public static final String SelectedSubCorpusLocationProperty = "selectedSubCorpusLocation";
}
