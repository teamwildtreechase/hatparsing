/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import util.ConfigFile;

/**
 * This class uses a file SELECTEDEXAMPLES produced by HATSelectionGenerator in order to produce 
 * a selected version of the corpus (i.e, cource file, target file, alignment file)
 * containing only those examples that are contained in SELECTEDEXAMPLES 
 * @author gemaille
 *
 */
public class HATSubCorpusGenerator 
{
	private BufferedReader selectionReader, sourceReader, targetReader, alignmentsReader;
	private BufferedWriter selectedSourceWriter, selectedTargetWriter, selectedAlignmentsWriter;

	
	public HATSubCorpusGenerator(ConfigFile theConfig, String sourceFileName, String targetFileName, String alignmentsFileName, String hatPropertiesFileName)
	{
		String selectedSubCorpusLocation = theConfig.getValue("selectedSubCorpusLocation");	
		String sourceOutFileName = selectedSubCorpusLocation + theConfig.getValue("sourceFileName");
		String targetOutFileName = selectedSubCorpusLocation + theConfig.getValue("targetFileName");
		String alignmentsOutFileName = selectedSubCorpusLocation + theConfig.getValue("alignmentsFileName");
		
		try 
		{
			selectionReader = new BufferedReader(new FileReader(hatPropertiesFileName ));
			sourceReader = new BufferedReader(new FileReader(sourceFileName));
			targetReader = new BufferedReader(new FileReader(targetFileName));
			alignmentsReader = new BufferedReader(new FileReader(alignmentsFileName));
			
			selectedSourceWriter = new BufferedWriter(new FileWriter(sourceOutFileName));
			selectedTargetWriter = new BufferedWriter(new FileWriter(targetOutFileName));
			selectedAlignmentsWriter = new BufferedWriter(new FileWriter(alignmentsOutFileName));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}

	}
	
	public void writeSelectedCorpus()
	{
		try 
		{
			System.out.println(">>>HATSubCorpusGenerator.writeSelectedCorpus() called...");
			
			
			// Skip first line which contains headers
			String line = selectionReader.readLine();
			
			int lineNumber = 1;
			
			while((line = selectionReader.readLine()) != null)
			{
				String[] parts = line.split(",");
				
				// The first column of the CSV file contains the line numbers
				int selectedLineNumber = Integer.parseInt(parts[0].trim());
				
				String sourceLine, targetLine, alignmentsLine;
				sourceLine = targetLine = alignmentsLine = "";
				
				// Read lines until the one with the selected line number is read
				while(lineNumber <= selectedLineNumber)
				{
					sourceLine = sourceReader.readLine();
					targetLine = targetReader.readLine();
					alignmentsLine = alignmentsReader.readLine();
					
					lineNumber++;
				}
				
				selectedSourceWriter.write(sourceLine + "\n");
				selectedTargetWriter.write(targetLine + "\n");
				selectedAlignmentsWriter.write(alignmentsLine + "\n");
			}
			
			// close all the writers
			selectedSourceWriter.close();
			selectedTargetWriter.close();
			selectedAlignmentsWriter.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
}