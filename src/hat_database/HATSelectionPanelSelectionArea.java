/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.junit.Assert;

import util.ConfigCacher;

public class HATSelectionPanelSelectionArea extends JPanel {
	private static final String HatSelectionQueryPropertyName = "hatSelectionQuery";
	private static final String StandardQuery = "/*Put just \"1\" as where clause if you want to select everything*/\n\ntreeTypeAtomLengthOne = 'bijection' \t /* Type constraints can take values 'binarizable', 'bijection' or 'many' */"
			+ " \nAND treeTypeAtomLengthFive = 'bijection' \t /* treeTypeAtomLengthTen and treeTypeAtomLengthTwenty can also be used */"
			+ " \nAND alignmentTripleNumber < 1000000 \t /* Different range constraints on the alignment triples included can be used*/"
			+ "\nAND sourceLength < 20 AND targetLength < 20 /* Optional length constraints*/"
			+ "\nAND sourceString LIKE \"%\" \t\t /*Lexical constraints source*/"
			+ "\nAND targetString LIKE \"%\"  \t\t /*Lexical constraints target*/";

	private static final String DEFAULT_FROM_STRING = "hatPropertiesEnNe \t\t /* Can also be e.g. hatPropertiesEnDe or hatPropertiesEnFr */";
	private static final String FROM_FIELD_PROPERTY_NAME = "fromField";
	
	private static final long serialVersionUID = 1L;
	private JTextField selectLabelField, fromLabelField;
	protected JTextField fromField;
	private JTextField whereLabelField;
	private JTextArea whereArea;

	protected HATSelectionPanelSelectionArea(ConfigCacher configCacher) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Font f = new Font("sansserif", Font.PLAIN, 20);

		this.selectLabelField = createSelectionTextField("SELECT *", f);
		this.selectLabelField.setMaximumSize(new Dimension(1600, 50));
		this.add(new JLabel("Selection Query"));
		this.add(selectLabelField);

		fromLabelField = createSelectionTextField("FROM", f);
		fromLabelField.setMaximumSize(new Dimension(100, 50));
		fromField = new JTextField();
		fromField.setFont(f);
		fromField.setMaximumSize(new Dimension(1500, 50));
		fromField.setMinimumSize(new Dimension(1500, 50));
		configCacher.registerEntry(FROM_FIELD_PROPERTY_NAME, DEFAULT_FROM_STRING, fromField);
		
		JPanel fromSubPanel = new JPanel();
		fromSubPanel.setLayout(new BoxLayout(fromSubPanel, BoxLayout.X_AXIS));
		fromSubPanel.add(fromLabelField);
		fromSubPanel.add(fromField);
		this.add(fromSubPanel);

		whereLabelField = createSelectionTextField("WHERE", f);
		whereLabelField.setMaximumSize(new Dimension(1600, 50));
		this.add(whereLabelField);

		this.whereArea = new JTextArea();
		this.whereArea.setMaximumSize(new Dimension(1600, 400));
		this.whereArea.setFont(f);
		this.add(whereArea);

		configCacher.registerEntry(HATSelectionPanelSelectionArea.HatSelectionQueryPropertyName, HATSelectionPanelSelectionArea.StandardQuery, whereArea);
		configCacher.updateRegisteredTextFields();
		

	}
	
	public void resetWhereFieldToDefault()
	{
		this.setWhereField(StandardQuery);
	}
	
	public void resetFromFieldToDefault()
	{
		this.fromField.setText(DEFAULT_FROM_STRING);
	}

	public void setFromField(List<String> tableNames) {
		Assert.assertTrue(!tableNames.isEmpty());
		String newFromFieldContents = tableNames.get(0);
		if (tableNames.size() > 1) {
			newFromFieldContents += " \t\t /* Can also be e.g.";
			for (int i = 1; i < tableNames.size(); i++) {
				newFromFieldContents += " " + tableNames.get(i);
			}
			newFromFieldContents += "  */";
		}
		fromField.setText(newFromFieldContents);
	}

	private JTextField createSelectionTextField(String label, Font font) {
		JTextField result = new JTextField(label);
		result.setEditable(false);
		result.setFont(font);
		result.setBackground(Color.WHITE);
		result.setForeground(Color.RED);

		return result;
	}

	protected void setFromField(String value) {
		this.fromField.setText(value);
	}

	public JTextArea getQueryArea() {
		return this.whereArea;
	}

	protected void setWhereField(String value) {
		this.whereArea.setText(value);
	}

}
