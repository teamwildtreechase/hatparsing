/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import alignmentStatistics.HATProperties;

import extended_bitg.EbitgTreeState.TreeType;

/**
 * The Class DatabaseInterface implements methods that are used to setup a database of HATs properties and access this database to generate sub-corpera
 * satisfying certain conditions specified by the user.
 * 
 * @author gemaille
 * 
 */
public class DatabaseInterface {

	public Connection connection;
	public PreparedStatement preparedStatement;

	/**
	 * Constructor: takes a databaseLocation, and opens a connection to the database at this location
	 * 
	 * @param databaseLocation
	 *            : the location (full path) of the database
	 */
	public DatabaseInterface(String databaseLocation) {
		this.makeConnction(databaseLocation);
	}

	/**
	 * This method takes the locations of a properties file, source file, target file and alignmentFile and puts their contents into a database table with
	 * tableName. This table can then be used for example to generate sub-corpera that satisfy certain conditions.
	 * 
	 * @param propertiesFileLocation
	 *            : locations of the HATs properties file
	 * @param sourceFileLocation
	 *            : location of the source corpus file
	 * @param targetFileLocation
	 *            : location of the target corpus file
	 * @param alignmentsFileLocation
	 *            : location of the alignments corpus file
	 * @param tableName
	 */
	public void createHATsTableFromFiles(String propertiesFileLocation, String sourceFileLocation, String targetFileLocation, String alignmentsFileLocation, String tableName) {
		BufferedReader propertiesReader, sourceReader, targetReader, alignmentsReader;
		try {
			propertiesReader = new BufferedReader(new FileReader(propertiesFileLocation));
			sourceReader = new BufferedReader(new FileReader(sourceFileLocation));
			targetReader = new BufferedReader(new FileReader(targetFileLocation));
			alignmentsReader = new BufferedReader(new FileReader(alignmentsFileLocation));

			// Read the header
			String header = propertiesReader.readLine();
			String[] parts = header.split(",");

			Statement statement = connection.createStatement();

			// Drop the previous table if it exists
			String dropStatement = "drop table if exists " + tableName + ";";
			System.out.println("dropStament: " + dropStatement);
			statement.executeUpdate(dropStatement);

			// Form a table creation statement
			String createStatement = "create table " + tableName + " (";
			String insertStatement = "insert into " + tableName + " values (";
			for (int i = 0; i < parts.length; i++) {
				createStatement += parts[i].trim() + ",";
				insertStatement += "?" + ",";
			}

			// add the last three columns to the table creation statement and insert statement
			createStatement += "sourceString,targetString,alignmentsString);";
			insertStatement += "?,?,?);";

			// Create the new table
			System.out.println("createStament: " + createStatement);
			statement.executeUpdate(createStatement);

			preparedStatement = connection.prepareStatement(insertStatement);

			int lineNumber = 1;
			String propertiesLine, sourceLine, targetLine, alignmentsLine;

			// Read the properties line, source line, target line and alignments line and
			// verify that they are not empty
			while (((propertiesLine = propertiesReader.readLine()) != null) && ((sourceLine = sourceReader.readLine()) != null) && ((targetLine = targetReader.readLine()) != null)
					&& ((alignmentsLine = alignmentsReader.readLine()) != null)
			// && lineNumber <= 1000
			) {
				addHATPropertiesToPreparedtatement(propertiesLine, sourceLine, targetLine, alignmentsLine);

				// Add stament to the batch
				preparedStatement.addBatch();

				// Execute batch of 1000 row insertions
				if ((lineNumber % 1000) == 0) {
					System.out.println("Processing properties line: " + lineNumber);
					System.out.println("propertiesLine: " + propertiesLine);
					System.out.println("sourceLine: " + sourceLine);
					System.out.println("targetLine: " + targetLine);
					System.out.println("alignmentsLine: " + alignmentsLine);
					connection.setAutoCommit(false);
					preparedStatement.executeBatch();
					connection.setAutoCommit(true);
				}

				lineNumber++;
			}

			// Execute last batch insertion
			connection.setAutoCommit(false);
			preparedStatement.executeBatch();
			connection.setAutoCommit(true);

			// Index 1: type index
			String indexName = tableName + "_" + "TypeIndex";
			String indexCreateStatement = "CREATE INDEX " + indexName + " ON " + tableName + " (treeTypeAtomLengthOne , treeTypeAtomLengthFive , treeTypeAtomLengthTen , treeTypeAtomLengthTwenty)";
			System.out.println("indexCreateStatement : " + indexCreateStatement);
			statement.executeUpdate(indexCreateStatement);

			// Index 2 : length index
			indexName = tableName + "_" + "LengthIndex";
			indexCreateStatement = "CREATE INDEX " + indexName + " ON " + tableName + " (sourceLength, targetLength)";
			System.out.println("indexCreateStatement : " + indexCreateStatement);
			statement.executeUpdate(indexCreateStatement);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method that adds the HATProperties (for one row) to the prepared statement. It also adds the sourceString, targetString and alignmentString to the
	 * prepared statement. These are added as extra fields in the table to make it complete (i.e. a subcorpus can be fully generated from only the database),
	 * and also so that lexical querying is possible
	 * 
	 * @param propertiesString
	 *            : the String containing the HATProperties (As substrings, separated by commas)
	 * @param sourceString
	 *            : the source line of this alignment triple
	 * @param targetString
	 *            : the target line of this alignment triple
	 * @param alignmentsString
	 *            : the alignments of this alignment triple
	 */
	public void addHATPropertiesToPreparedtatement(String propertiesString, String sourceString, String targetString, String alignmentsString) {
		try {
			HATProperties properties = new HATProperties(propertiesString);

			// Set properties that can be determined whether or not the alignment triple could be parsed
			Integer alignmentTripleNumber = properties.getAlignmentTripleNumber();
			preparedStatement.setInt(1, alignmentTripleNumber);

			Integer sourceLength = (Integer) properties.getValue("sourceLength");
			preparedStatement.setInt(2, sourceLength);

			Integer targetLength = (Integer) properties.getValue("targetLength");
			preparedStatement.setInt(3, targetLength);

			Boolean isParsable = (Boolean) properties.getValue("isParsable");
			preparedStatement.setBoolean(4, isParsable);

			Boolean hasNullAlignments = false;
			Integer numAtomicNodes, numSourceNullBindingNodes, numBITTNodes, numPETNodes, numHATNodes;
			numAtomicNodes = numSourceNullBindingNodes = numBITTNodes = numPETNodes = numHATNodes = 0;
			String treeTypeAtomLengthOneString, treeTypeAtomLengthFiveString, treeTypeAtomLengthTenString, treeTypeAtomLengthTwentyString;
			treeTypeAtomLengthOneString = treeTypeAtomLengthFiveString = treeTypeAtomLengthTenString = treeTypeAtomLengthTwentyString = null;

			// Determine properties that only have values if the alignment triple could be parsed
			if (isParsable) {
				hasNullAlignments = (Boolean) properties.getValue("hasNullAlignments");
				numAtomicNodes = (Integer) properties.getValue("numAtomicNodes");
				numSourceNullBindingNodes = (Integer) properties.getValue("numSourceNullBindingNodes");
				numBITTNodes = (Integer) properties.getValue("numBITTNodes");
				numPETNodes = (Integer) properties.getValue("numPETNodes");
				numHATNodes = (Integer) properties.getValue("numHATNodes");
				treeTypeAtomLengthOneString = HATProperties.treeTypeString((TreeType) properties.getValue("treeTypeAtomLengthOne"));
				treeTypeAtomLengthFiveString = HATProperties.treeTypeString((TreeType) properties.getValue("treeTypeAtomLengthFive"));
				treeTypeAtomLengthTenString = HATProperties.treeTypeString((TreeType) properties.getValue("treeTypeAtomLengthTen"));
				treeTypeAtomLengthTwentyString = HATProperties.treeTypeString((TreeType) properties.getValue("treeTypeAtomLengthTwenty"));
			}

			// Set the properties which may or may not have gotten values in the previous block
			preparedStatement.setBoolean(5, hasNullAlignments);
			preparedStatement.setInt(6, numAtomicNodes);
			preparedStatement.setInt(7, numSourceNullBindingNodes);
			preparedStatement.setInt(8, numBITTNodes);
			preparedStatement.setInt(9, numPETNodes);
			preparedStatement.setInt(10, numHATNodes);
			preparedStatement.setString(11, treeTypeAtomLengthOneString);
			preparedStatement.setString(12, treeTypeAtomLengthFiveString);
			preparedStatement.setString(13, treeTypeAtomLengthTenString);
			preparedStatement.setString(14, treeTypeAtomLengthTwentyString);

			// Add the source, target and alignment as String columns to the table
			preparedStatement.setString(15, sourceString);
			preparedStatement.setString(16, targetString);
			preparedStatement.setString(17, alignmentsString);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set up a connection to the database using the jdbc sqllite driver
	 * 
	 * @param databaseLocation
	 *            : the location of the database file
	 */
	private void makeConnction(String databaseLocation) {

		try {
			Class.forName("org.sqlite.JDBC");
			String connectionString = "jdbc:sqlite:" + databaseLocation;
			connection = DriverManager.getConnection(connectionString);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public void closeInterface() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String reconstructHATPropertiesLineFromResultSet(ResultSet resultSet) {
		String result = "";

		try {
			result += resultSet.getInt(1) + ",";
			result += resultSet.getInt(2) + ",";
			result += resultSet.getInt(3) + ",";
			result += resultSet.getBoolean(5) + ",";
			result += resultSet.getBoolean(4) + ",";
			result += resultSet.getInt(6) + ",";
			result += resultSet.getInt(7) + ",";
			result += resultSet.getInt(8) + ",";
			result += resultSet.getInt(9) + ",";
			result += resultSet.getInt(10) + ",";
			result += resultSet.getString(11) + ",";
			result += resultSet.getString(12) + ",";
			result += resultSet.getString(13) + ",";
			result += resultSet.getString(14);

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return result;
	}

	public List<String> getTableNamesFromDatabase() {
		List<String> result = new ArrayList<String>();
		DatabaseMetaData md;
		try {
			md = this.connection.getMetaData();
			ResultSet rs = md.getTables(null, null, "%", null);
			while (rs.next()) {
				String tableName = rs.getString(3);
				result.add(tableName);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		this.closeInterface();
		return result;
	}

	public static List<String> getTableNamesFromDatabaseFilterOutIndexTables(String databasePath) {
		DatabaseInterface databaseInterface = new DatabaseInterface(databasePath);
		List<String> result = new ArrayList<String>();
		for (String tableName : databaseInterface.getTableNamesFromDatabase()) {
			if (!(tableName.toLowerCase().contains("index"))) {
				result.add(tableName);
				System.out.println("Proper table name:  " + tableName);
			}
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
		DatabaseInterface dbInterface = new DatabaseInterface("/scratch/wenniger/AlignedCorpera/test.db");

		Statement stat = dbInterface.connection.createStatement();
		stat.executeUpdate("drop table if exists people;");
		stat.executeUpdate("create table people (name, occupation);");
		PreparedStatement prep = dbInterface.connection.prepareStatement("insert into people values (?, ?);");

		prep.setString(1, "Gandhi");
		prep.setString(2, "politics");
		prep.addBatch();
		prep.setString(1, "Turing");
		prep.setString(2, "computers");
		prep.addBatch();
		prep.setString(1, "Wittgenstein");
		prep.setString(2, "smartypants");
		prep.addBatch();

		dbInterface.connection.setAutoCommit(false);
		prep.executeBatch();
		dbInterface.connection.setAutoCommit(true);

		ResultSet rs = stat.executeQuery("select * from people;");
		while (rs.next()) {
			System.out.println("name = " + rs.getString("name"));
			System.out.println("job = " + rs.getString("occupation"));
		}
		rs.close();

		dbInterface.closeInterface();
	}
}