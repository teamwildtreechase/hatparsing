/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import util.ConfigCacher;
import util.ConfigFile;

public class HATsDatabaseBuilderPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final HATsDatabaseComputationPanelButtonArea hatsDatabaseComputationPanelButtonArea;
	private final HATsDatabaseBuilderPanelConfigArea hatsDatabaseBuilderPanelConfigArea;

	private JFrame containingFrame;

	public HATsDatabaseBuilderPanel(ConfigCacher configCacher) {

		// Add empty horizontal space
		this.add(Box.createVerticalStrut(20));

		this.hatsDatabaseComputationPanelButtonArea = HATsDatabaseComputationPanelButtonArea.createHaTsDatabaseComputationPanelButtonArea(this);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(hatsDatabaseComputationPanelButtonArea);

		// Add empty horizontal space
		this.add(Box.createVerticalStrut(80));

		this.hatsDatabaseBuilderPanelConfigArea = HATsDatabaseBuilderPanelConfigArea.createHATsDatabaseBuilderPanelConfigArea(configCacher);
		this.add(hatsDatabaseBuilderPanelConfigArea);

		// Add empty horizontal space
		this.add(Box.createVerticalStrut(10));

	}

	private ConfigFile loadConfig() {
		String configFileLinesAsString = this.hatsDatabaseBuilderPanelConfigArea.getConfigFieldContentsAsString();
		List<String> configFileLines = Arrays.asList(configFileLinesAsString.split("\n"));
		ConfigFile theConfig = new ConfigFile(configFileLines);
		return theConfig;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(hatsDatabaseComputationPanelButtonArea.computeDatabaseButton)) {
			if (userConfirms()) {
				ConfigFile theConfig = loadConfig();
				String createdDatabaseLocation = computeDatabase(theConfig);
				String message = "HATs properties database has been created in :\n" + createdDatabaseLocation;
				JOptionPane.showMessageDialog(this.containingFrame, message, "" + "HATs properties database created", JOptionPane.PLAIN_MESSAGE);
			}

		}

		else if (e.getSource().equals(hatsDatabaseComputationPanelButtonArea.resetExampleConfigFileButton)) {
			this.hatsDatabaseBuilderPanelConfigArea.resetConfigFieldContentsToDefault();

		}

		else if (e.getSource().equals(hatsDatabaseComputationPanelButtonArea.createExampleCorperaButton)) {
			List<String> createdCorpusLocations = HATsDatabaseBuilder.createTestExampleCorpera();

			String message = "Created example corpera in:";
			for (String corpus : createdCorpusLocations) {
				message += "\n" + corpus;
			}
			// custom title, no icon
			JOptionPane.showMessageDialog(this.containingFrame, message, "" + "Example corpera created", JOptionPane.PLAIN_MESSAGE);
		}

	}

	public void setContainingFrame(JFrame containingFrame) {
		this.containingFrame = containingFrame;
	}

	public String computeDatabase(ConfigFile theConfig) {
		System.out.println("Computing HAT Properties Database");
		HATsDatabaseBuilder hatsDatabaseBuilder = HATsDatabaseBuilder.createHATsDatabaseBuilder(theConfig);
		return hatsDatabaseBuilder.computeDatabase();
	}

	public boolean userConfirms() {
		boolean result = false;

		final JOptionPane optionPane = new JOptionPane("This will compute the entire HAT database\n"
				+ "and will only work if you have properly specified all the configuration values, as in the example.\n" + "Do you want to proceed?", JOptionPane.QUESTION_MESSAGE,
				JOptionPane.YES_NO_OPTION);

		final JDialog dialog = new JDialog(containingFrame, "Click a button", true);
		dialog.setContentPane(optionPane);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent we) {
				// setLabel("Thwarted user attempt to close window.");
			}
		});

		optionPane.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				String prop = e.getPropertyName();

				if (dialog.isVisible() && (e.getSource() == optionPane) && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
					// If you were going to check something
					// before closing the window, you'd do
					// it here.
					dialog.setVisible(false);
				}
			}
		});
		dialog.pack();
		dialog.setVisible(true);

		int value = ((Integer) optionPane.getValue()).intValue();
		if (value == JOptionPane.YES_OPTION) {
			result = true;
			// setLabel("Good.");
		} else if (value == JOptionPane.NO_OPTION) {
			// setLabel("Try using the window decorations "
			// + "to close the non-auto-closing dialog. "
			// + "You can't!");

			result = false;
		}

		return result;
	}

}
