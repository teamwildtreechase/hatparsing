/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import alignmentStatistics.HATProperties;
import util.ConfigFile;
import util.FileUtil;

/**
 * This class is responsible for setting up a HATs properties database and using that database to generate sub-corpera satisfying certain conditions.
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class HATSelectionGenerator {
	String selectionOutputFileLocation, queryFileLocation, databaseLocation, selectedSubCorpusLocation;
	private final boolean writeSubCorpusFromDatabase;
	ConfigFile theConfig;

	private final HATsDBFullInputFileLocations haTsDBInputFileLocations;
	String sourceOutFileName, targetOutFileName, alignmentsOutFileName;

	/**
	 * Constructor : Reads out various required file locations from the ConfigFile given
	 * 
	 * @param theConfig
	 *            ConfigFile that should contain the required information about the locations of files required by HATSubCorpusGenerator
	 */
	public HATSelectionGenerator(ConfigFile theConfig, boolean writeSubCorpusFromDatabase) {
		this.theConfig = theConfig;
		this.writeSubCorpusFromDatabase = writeSubCorpusFromDatabase;

		selectedSubCorpusLocation = theConfig.getStringIfPresentOrReturnDefault("selectedSubCorpusLocation", "selected-corpus/");
		sourceOutFileName = selectedSubCorpusLocation + theConfig.getValue("sourceFileName");
		targetOutFileName = selectedSubCorpusLocation + theConfig.getValue("targetFileName");
		alignmentsOutFileName = selectedSubCorpusLocation + theConfig.getValue("alignmentsFileName");

		// Create a folder for the selected subcorpus if it does not exists
		FileUtil.createFolderIfNotExisting(selectedSubCorpusLocation);

		selectionOutputFileLocation = selectedSubCorpusLocation + theConfig.getStringIfPresentOrReturnDefault("selectedHATsFileName", "selectedHATs.txt");
		queryFileLocation = theConfig.getStringIfPresentOrReturnDefault(HatDBProperties.SelectionQueryFileNamePropertyName, "selectionQuery.txt");
		databaseLocation = theConfig.getStringIfPresentOrReturnDefault(HatDBProperties.DatabaseLocationPropertyName, "test.db");

		if (!writeSubCorpusFromDatabase) {
			this.haTsDBInputFileLocations = HATsDBFullInputFileLocations.createHaTsDBInputFileLocationsSpecifyingAllFileNamesExplicitly(theConfig);
		} else {
			this.haTsDBInputFileLocations = null;
		}
	}

	/**
	 * Wrapper method that takes a query, opens a new connection to the database and then calls the workhorse method that generates the sub-corpus for that
	 * query
	 * 
	 * @param query
	 * @return The number of selected HATs for the query
	 */
	public int generateSelectedHATsForQuery(String query) {
		int numSelectedHATs = 0;
		DatabaseInterface dbInterface = new DatabaseInterface(databaseLocation);

		numSelectedHATs = produceSubCorpusSatisfyingQuery(dbInterface, query, selectionOutputFileLocation, sourceOutFileName, targetOutFileName, alignmentsOutFileName, writeSubCorpusFromDatabase);

		if (!writeSubCorpusFromDatabase) {
			this.generateSubCorpusFromFiles(query);
		}
		dbInterface.closeInterface();

		return numSelectedHATs;
	}


	/**
	 * Method that reads a query from a file and returns it as a String
	 * 
	 * @param fileName
	 * @return The query as read from the file
	 */
	public String readQueryFromFile(String fileName) {
		String result = "";
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

			String line = "";
			while ((line = fileReader.readLine()) != null) {
				result += line + "\n";
			}
			fileReader.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;

	}

	/**
	 * Recompute the HATs properties database
	 */
	public void recomputeDatabase() {

		for (HATsDBTableInputFileLocations tableFileLocations : haTsDBInputFileLocations.getTableInputFileLocationsList()) {
			String tableName = tableFileLocations.getHATPropertiesTablesName();
			String hatPropertyFileName = tableFileLocations.getHATPropertiesFileName();

			DatabaseInterface dbInterface = new DatabaseInterface(databaseLocation);
			dbInterface.createHATsTableFromFiles(hatPropertyFileName, tableFileLocations.getSourceFileLocation(), tableFileLocations.getTargetFileLocation(),
					tableFileLocations.getAlignmentsFileLocation(), tableName);
			// close the connection to the database
			dbInterface.closeInterface();
		}

	}

	/**
	 * This method takes a query and locations of result Properties-, result Source-, result Target- and result Alignment- files and generates the sub-corpus
	 * satisfying the query, by firing the query to the database, then extracting the results and writing them to the corresponding files
	 * 
	 * @param databaseInterface
	 *            : DatabaseInterface required to get the data from
	 * @param query
	 *            : the query
	 * @param resultPropertiesLocation
	 *            : location were the produced properties file is written
	 * @param resultSourceLocation
	 *            : location were the produced source file is written
	 * @param resultTargetLocation
	 *            : location were the produced target file is written
	 * @param resultAlignmentsLocation
	 *            : location were the produced alignment file is written
	 * @return the number of generated rows that satisfied the query
	 */
	private int produceSubCorpusSatisfyingQuery(DatabaseInterface databaseInterface, String query, String resultPropertiesLocation, String resultSourceLocation, String resultTargetLocation,
			String resultAlignmentsLocation, boolean writeSubCorpusFromDatabase) {
		int numResults = 0;

		try {
			BufferedWriter propertiesWriter = new BufferedWriter(new FileWriter(resultPropertiesLocation));
			BufferedWriter sourceWriter = new BufferedWriter(new FileWriter(resultSourceLocation));
			BufferedWriter targetWriter = new BufferedWriter(new FileWriter(resultTargetLocation));
			BufferedWriter alignmentsWriter = new BufferedWriter(new FileWriter(resultAlignmentsLocation));
			propertiesWriter.write(HATProperties.propertiesHeader());

			// The arguments of the create statement are important to make looping over the resultset fast,
			// see also : http://www.coderanch.com/t/468813/java/java/Writing-results-ResultSet-File-VERY
			Statement statement = databaseInterface.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

			System.out.println("Executing query..." + query);
			ResultSet resultSet = statement.executeQuery(query);
			System.out.println("Query completed, looping over result set...");

			while (resultSet.next()) {
				if (((numResults + 1) % 100) == 0) {
					System.out.println("writing result : " + (numResults + 1));
				}
				String propertiesLine = databaseInterface.reconstructHATPropertiesLineFromResultSet(resultSet);
				propertiesWriter.write(propertiesLine + "\n");

				if (writeSubCorpusFromDatabase) {
					// Write the source string, target string and alignment string to their respective output files
					sourceWriter.write(resultSet.getString(15) + "\n");
					targetWriter.write(resultSet.getString(16) + "\n");
					alignmentsWriter.write(resultSet.getString(17) + "\n");
				}
				numResults++;
			}

			// Close the result set as soon as we are finished with it
			resultSet.close();

			propertiesWriter.close();
			sourceWriter.close();
			targetWriter.close();
			alignmentsWriter.close();

		} catch (IOException e) {

			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return numResults;

	}

	private void generateSubCorpusFromFiles(String query) {
		int languagePairIndex = -1;

		System.out.println("query from subcorpus generation: \n" + query);

		// The . matches everything except newlines
		String anyCharacters = "(.|\\n)*";
		String anyWhiteSpace = "\\s*";

		// Regular expressions recognizing English-Dutch, English-German and English-French respectively

		// Find a matching table after FROM statement in the query
		boolean matchingTableFound = false;
		String tableName;
		do {
			languagePairIndex++;
			tableName = this.haTsDBInputFileLocations.getTableInputFileLocations(languagePairIndex).getHATPropertiesTablesName();
			String tableRecognitionExpression = anyCharacters + "FROM" + anyWhiteSpace + tableName + anyCharacters;

			if (query.matches(tableRecognitionExpression)) {
				matchingTableFound = true;
				System.out.println("Table " + tableName + " chosen");
			}
		} while (!matchingTableFound && languagePairIndex < this.haTsDBInputFileLocations.getTableInputFileLocationsList().size() - 1);

		HATsDBTableInputFileLocations tableFileLocations = this.haTsDBInputFileLocations.getTableInputFileLocations(languagePairIndex);

		HATSubCorpusGenerator subCorpusGenerator = new HATSubCorpusGenerator(theConfig, tableFileLocations.getSourceFileLocation(), tableFileLocations.getTargetFileLocation(),
				tableFileLocations.getAlignmentsFileLocation(), this.selectionOutputFileLocation);
		subCorpusGenerator.writeSelectedCorpus();
	}

	public static void main(String[] args) {
		if ((args.length < 1) || (args.length > 2)) {
			System.out.println("Wrong usage, uage: java -jar hatsSelectionGenerator configFileName RecomputeDatabase");
			System.out.println("The first argument is requiered, the second argument (true/false) is optional");
			return;
		}

		String configFileLocation = args[0];

		boolean recomputeDatabase = false;

		if (args.length == 2) {
			recomputeDatabase = Boolean.parseBoolean(args[1].trim().toLowerCase());
		}

		ConfigFile theConfig;
		try {
			theConfig = new ConfigFile(configFileLocation);

			HATSelectionGenerator selector = new HATSelectionGenerator(theConfig, true);
			String query = selector.readQueryFromFile(selector.queryFileLocation);
			if (recomputeDatabase) {
				selector.recomputeDatabase();
			}
			selector.generateSelectedHATsForQuery(query);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}