package hat_database;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class HATsDatabaseComputationPanelButtonArea extends JPanel {

	private static final long serialVersionUID = 1L;
	protected final JButton computeDatabaseButton, resetExampleConfigFileButton, createExampleCorperaButton;

	private HATsDatabaseComputationPanelButtonArea(JButton computeDatabaseButton, JButton resetExampleConfigFileButton, JButton createExampleCorperaButton) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.resetExampleConfigFileButton = resetExampleConfigFileButton;
		this.computeDatabaseButton = computeDatabaseButton;
		this.createExampleCorperaButton = createExampleCorperaButton;

		// this.add(resetExampleConfigFile);

		this.add(createButonContainingPanel(createExampleCorperaButton));

		// Add empty horizontal space
		this.add(Box.createVerticalStrut(20));

		this.add(createButonContainingPanel(resetExampleConfigFileButton));

		// Add empty horizontal space
		this.add(Box.createVerticalStrut(20));
		
		// this.setLayout(new GridLayout());
		this.add(createButonContainingPanel(computeDatabaseButton));
	}

	public static JPanel createButonContainingPanel(JButton button) {
		JPanel buttonContainingPanel = new JPanel();
		buttonContainingPanel.setLayout(new GridLayout());
		buttonContainingPanel.setPreferredSize(new Dimension(400, 100));
		buttonContainingPanel.setMaximumSize(new Dimension(400, 100));
		buttonContainingPanel.add(button);
		return buttonContainingPanel;
	}

	public static HATsDatabaseComputationPanelButtonArea createHaTsDatabaseComputationPanelButtonArea(HATsDatabaseBuilderPanel containingPanel) {

		JButton computeDatabaseButton = HatSelectionPanelButtonArea.createButton(containingPanel, "Create HAT Properties Database", new Dimension(300, 100));
		JButton resetExampleConfigFile = HatSelectionPanelButtonArea.createButton(containingPanel, "Reset example config file", new Dimension(300, 100));
		JButton createExampleCorperaButton = HatSelectionPanelButtonArea.createButton(containingPanel, "Create Example Corpera for database creation", new Dimension(300, 100));
		// computeDatabaseButton.setMinimumSize(new Dimension(100,100));
		return new HATsDatabaseComputationPanelButtonArea(computeDatabaseButton, resetExampleConfigFile, createExampleCorperaButton);
	}
}
