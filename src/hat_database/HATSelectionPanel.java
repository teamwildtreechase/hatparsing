/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_database;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import util.ConfigCacher;
import util.ConfigFile;

public class HATSelectionPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final HatSelectionPanelButtonArea hatSelectionPanelButtonArea;
	private final HATSelectionPanelSelectionArea hatSelectionPanelSelectionArea;

	private ConfigFile theConfig;

	private JTextArea resultReportField;
	public static final String TableNameEnglishDutch = "hatPropertiesEnNe";
	public static final String TableNameEnglishGerman = "hatPropertierecomputeDatabasesEnDe";
	public static final String TableNameEnglishFrench = "hatPropertiesEnFr";

	private JTextField configFileField;

	public HATSelectionPanel(JTextField configFileField, ConfigCacher configCacher) {
		this.configFileField = configFileField;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.hatSelectionPanelButtonArea = new HatSelectionPanelButtonArea(this);

		this.add(hatSelectionPanelButtonArea);

		this.hatSelectionPanelSelectionArea = new HATSelectionPanelSelectionArea(configCacher);
		this.add(hatSelectionPanelSelectionArea);

		Font f = new Font("sansserif", Font.PLAIN, 20);

		// Add a field were information about the obtained result of the query is displayed
		this.add(new JLabel("Selection Result"));
		this.resultReportField = new JTextArea("");
		f = new Font("monospaced", Font.BOLD, 20);
		this.resultReportField.setFont(f);
		this.resultReportField.setMaximumSize(new Dimension(800, 100));
		this.resultReportField.setEditable(false);
		this.add(resultReportField);
	}

	public JTextArea getQueryArea() {
		return this.hatSelectionPanelSelectionArea.getQueryArea();
	}

	public void setConfig(ConfigFile theConfig) {
		this.theConfig = theConfig;
	}

	private void setWhereField(String value) {
		this.hatSelectionPanelSelectionArea.setWhereField(value);
	}

	private String generateNumberSelectionString(List<Integer> numbers) {
		String result = "alignmentTripleNumber IN (";

		Iterator<Integer> numberIterator = numbers.iterator();

		while (numberIterator.hasNext()) {
			result += numberIterator.next();

			if (numberIterator.hasNext()) {
				result += ",";
			}
		}

		result += ")";

		return result;

	}

	public void performSelectionOnAlignmentTripleNumbers(String languagePair, List<Integer> selection) {
		this.hatSelectionPanelSelectionArea.setFromField(languagePair);
		this.setWhereField(generateNumberSelectionString(selection));
		this.performSelection();
	}

	private void loadConfig() {
		try {
			theConfig = new ConfigFile(this.configFileField.getText());
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(hatSelectionPanelButtonArea.performQueryButton)) {
			performSelection();
		}

		else if (e.getSource().equals(hatSelectionPanelButtonArea.resetQueryButton)) {
			this.hatSelectionPanelSelectionArea.resetWhereFieldToDefault();
		}
		
		else if (e.getSource().equals(hatSelectionPanelButtonArea.resetFromButton)) {
			this.hatSelectionPanelSelectionArea.resetFromFieldToDefault();
		}
	}

	private void performSelection() {
		System.out.println("Selection button pressed!!!");

		loadConfig();

		if (theConfig != null) {
			long timeStart = System.currentTimeMillis();
			boolean writeSubCorpusFromDatabase = !this.hatSelectionPanelButtonArea.useOriginalCorperaSpeedupCheckBox.isSelected();
			HATSelectionGenerator selector = new HATSelectionGenerator(theConfig, writeSubCorpusFromDatabase);
			String query = "SELECT* FROM " + this.hatSelectionPanelSelectionArea.fromField.getText() + " WHERE " + this.getQueryArea().getText();
			int numSelectedHATs = selector.generateSelectedHATsForQuery(query);

			long timeEnd = System.currentTimeMillis();
			long timePassed = timeEnd - timeStart;

			String resultString = ">>> Found " + numSelectedHATs + " HATs satisfying query conditions. \n(Computation took " + timePassed + " milliseconds)";
			this.resultReportField.setText(resultString);

		}

		else {
			System.out.println("configfile could  not be loaded - please specify valid config file location");
		}
	}

	public void setFromField(List<String> tableNames) {
		this.hatSelectionPanelSelectionArea.setFromField(tableNames);
	}

}