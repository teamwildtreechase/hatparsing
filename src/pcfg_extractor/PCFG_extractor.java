/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package pcfg_extractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import tsg.TSNodeLabel;
import util.FileUtil;
import util.Utility;


public class PCFG_extractor {

	public static void extractPCFG(File treebankFile, File grammarOutputFile, File lexicalsOutputFile, File tagFreqsOutputFile) throws Exception {
		ArrayList<TSNodeLabel> treebank = TSNodeLabel.getTreebank(treebankFile);
		Hashtable<String, int[]> cfgFreq = new Hashtable<String, int[]>();
		
		
		System.out.println("Extracting pcfg...");
		
		Hashtable<String, Hashtable<String,int[]>> lexicalFreqs = new Hashtable<String, Hashtable<String,int[]>> ();
		// the frequency of tags
		Hashtable<String, int[]> tagFreqs = new Hashtable<String, int[]>();
		
		
		for(TSNodeLabel t : treebank) 
		{
			ArrayList<TSNodeLabel> nodes = t.collectAllNodes();
			for(TSNodeLabel n : nodes) 
			{
				if (n.isLexical) continue;
				
				
				if(n.isPreLexical())
				{
					String tag = n.label();
					String word = n.daughters[0].label();
					
					// increase the lexical frequency of the tag given the word
					PCFG_extractor.increaseLexicalFreq(lexicalFreqs,word,tag);
					
					// increase the frequency of the tag only
					Utility.increaseStringIntArray(tagFreqs, tag);
				}
				else
				{	
				
					String rule = n.cfgRule();		
					Utility.increaseStringIntArray(cfgFreq, rule);
				}	
			}
		}
		
		// Write the grammar file
		PrintWriter pw = FileUtil.getPrintWriter(grammarOutputFile);
		System.out.println("Writing grammar file");
		printRuleFreqsBitparFormatToFile(pw,cfgFreq);	
	
		
		// Write the liexical file
		PrintWriter pw2 = FileUtil.getPrintWriter(lexicalsOutputFile);
		System.out.println("Writing lexicals file");
		PCFG_extractor.printLexicalFreqsBitparFormatToFile(pw2, lexicalFreqs); 
		

		
		// Write the liexical file
		PrintWriter pw3 = FileUtil.getPrintWriter(tagFreqsOutputFile);
		System.out.println("Writing tag frequencies file");
		printTagFreqsBitparFormatToFile(pw3, tagFreqs  );

		System.out.println("lexicalFreqs.get(\"The\").get(\"DT\")[0] " + lexicalFreqs.get("The").get("DT")[0] );
		
		System.out.println("finished");
		
		pw.close();
		pw2.close();
		pw3.close();
	}

	
	public static void printRuleFreqsBitparFormatToFile(PrintWriter pw, Hashtable<String, int[]> cfgFreq  )
	{
		for(Entry<String, int[]> e : cfgFreq.entrySet()) 
		{
			pw.println( e.getValue()[0] + "\t" + e.getKey());			
		}
	}
	

	public static void printTagFreqsBitparFormatToFile(PrintWriter pw, Hashtable<String, int[]> cfgFreq  )
	{
		for(Entry<String, int[]> e : cfgFreq.entrySet()) 
		{
			pw.println( e.getKey() + "\t" + e.getValue()[0]);			
		}
	}
	
	
	
	public static void printLexicalFreqsBitparFormatToFile(PrintWriter pw, Hashtable<String, Hashtable<String,int[]>> lexicalFreqs )
	{
		for(Entry<String, Hashtable<String,int[]>> wordTableEntry : lexicalFreqs.entrySet()) 
		{				
			pw.print("\n" + wordTableEntry.getKey() + "\t" );
			
			Hashtable<String,int[]> wordTable =  wordTableEntry.getValue();
			
			for(Entry<String, int[]> e : wordTable.entrySet())
			{
				pw.print(e.getKey() + "\t" + e.getValue()[0] + "\t");
			}
				
		}
	}
	
	
	
	/**
	 * Method that increases the frequency of a tag in the sub-table for a certain word.
	 * If the sub-table is not yet present, it is created
	 * @param lexicalFreqs
	 * @param word
	 * @param tag
	 */
	public static void increaseLexicalFreq(Hashtable<String, Hashtable<String,int[]>> lexicalFreqs, String word, String tag)
	{
		
		Hashtable<String, int[]> wordTable;
		
		// Sub-table for word already exists, retrieve it
		if(lexicalFreqs.containsKey(word))
		{
			wordTable = lexicalFreqs.get(word);
		}
		else
		{
			// table for word not yet present, create it
			wordTable = new Hashtable<String, int[]> ();
			// add created sub-table to Hashtable
			lexicalFreqs.put(word, wordTable);
		}
		
		
		// Increase the frequency of the tag int the retrieved or newly created sub-table
		Utility.increaseStringIntArray(wordTable, tag);
		
	}
	
	public static void transformInputFileToBitparInputFormat(String inputFileName, String outputFileName)
	{
		try 
		{
			System.out.println(">>>Transforming input file to bitpar format...");
			
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			
			String line = "";
			while((line = inputReader.readLine()) !=  null)
			{
				String[] words = line.split("\\s");
				
				// Write the words, one word per line to the output file
				for(int i = 0; i < words.length; i++)
				{
					outputWriter.write("\n" + words[i]);
				}
				
				// Required: write exta empty line to separate sentence from the next
				outputWriter.write("\n");

			}
			
			// Close files
			outputWriter.close();
			inputReader.close();
			
			System.out.println(">>>Done");
			
			
			
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * Removes the @'s that were added for binarization
	 * @param inputFileName
	 * @param outputFileName
	 */
	public static void removeStrudels(String inputFileName, String outputFileName)
	{
		try 
		{
			System.out.println(">>>Transforming input file to bitpar format...");
			
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			
			String line = "";
			while((line = inputReader.readLine()) !=  null)
			{
				if(!line.trim().equals(""))
				{
					
					try 
					{
						System.out.println("Flattening tree: " + line);
						
						// replace @ with # since we already use @ 
						line = line.replace('@', '#');
						
						TSNodeLabel tree = new TSNodeLabel(line);
						 performStrudelRemoveFlattening(tree);
						 
						 System.out.println("Flattening result: " + tree.toString());
						 outputWriter.write("\n" + tree.toString());
						
					} catch (Exception e) 
					{
						e.printStackTrace();
						break;
					}
				}
				else
				{
					outputWriter.write("\n");
				}

			}
			
			// Close files
			outputWriter.close();
			inputReader.close();
			
			System.out.println(">>>Done");
			
			
			
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void performStrudelRemoveFlattening(TSNodeLabel tree)
	{
		
		boolean nodesToFlattenLeft = true;
		
		
		while(nodesToFlattenLeft)
		{
			nodesToFlattenLeft = flattenFirstEncounteredStrudelNode(tree);
		}
		
	
		
	}

	
	private static boolean  flattenFirstEncounteredStrudelNode(TSNodeLabel tree)
	{
	
		for(TSNodeLabel internalNode : tree.collectInternalNodes())
		{
			// If this is an artificial node that was included during binarization
			if(internalNode.label().contains("#"))
			{
				internalNode.flattenParentByRemovingNodeAndInsertingChildren();
				System.out.println("A # was found");
				return true;
			}
		}
		
		System.out.println("No #'s found");
		return false;
	
	}
	
	
	public static void main(String[] args) throws Exception {
		//File treebankFile = new File(args[0]);
		//File grammarOutputFile = new File(args[1]);
		/*
		File treebankFile = new File("../Data/wsj.02-21.training(1).nounary");
		File grammarOutputFile = new File("/home/gemaille/Elements of Language Learning/BitPar/Experiment1/PennPCFG/Grammar-Bitpar.txt");
		File lexicalsOutputFile = new File("/home/gemaille/Elements of Language Learning/BitPar/Experiment1/PennPCFG/Lexicals-Bitpar.txt");
		File tagFreqsOutputFile = new File("/home/gemaille/Elements of Language Learning/BitPar/Experiment1/PennPCFG/TagFreqs-Bitpar.txt");
		extractPCFG(treebankFile, grammarOutputFile, lexicalsOutputFile,tagFreqsOutputFile);
		FileUtil.sortFile(grammarOutputFile);		
		
		
		String sentencesFile = "/home/gemaille/Elements of Language Learning/BitPar/Experiment1/Input/test.sentence.23";
		String bitparFormatSentencesFileName =  "/home/gemaille/Elements of Language Learning/BitPar/Experiment1/Input/test.sentence.23-bitparFormat";
		transformInputFileToBitparInputFormat(sentencesFile, bitparFormatSentencesFileName );
		*/
		
		String  bitparFormatResultPostProcessedFileName =  "/home/gemaille/Elements of Language Learning/BitPar/Experiment1/Output/result-wsj_test-post_edited.txt";
		String  bitparFormatResultPostProcessedFlattenedFileName =  "/home/gemaille/Elements of Language Learning/BitPar/Experiment1/Output/result-wsj_test-post_edited-Flattened.txt";
		
		removeStrudels(bitparFormatResultPostProcessedFileName,  bitparFormatResultPostProcessedFlattenedFileName);
	}
		
	
}
