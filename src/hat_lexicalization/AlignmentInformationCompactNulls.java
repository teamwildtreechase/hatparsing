/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import bitg.IntegerSetMethods;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import alignment.Alignment;
import alignment.AlignmentPair;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class AlignmentInformationCompactNulls extends AlignmentInformation
{

	private final SentencePairAlignmentChunkingZerosGrouped compactedChunking;
	private final PositionMappingTable sourcePositionMappingTable;
	private final PositionMappingTable targetPositionMappingTable;
	private final SentencePairNullBinding sentencePairNullBinding;

	private  AlignmentInformationCompactNulls(SentencePairAlignmentChunkingZerosGrouped originalChunking, 
			SentencePairAlignmentChunkingZerosGrouped compactedChunking,
			PositionMappingTable sourcePositionMappingTable,
			PositionMappingTable targetPositionMappingTable,
			SentencePairNullBinding sentencePairNullBinding)
	{
	
		super(originalChunking);
		this.compactedChunking = compactedChunking;
		this.sourcePositionMappingTable = sourcePositionMappingTable;
		this.targetPositionMappingTable = targetPositionMappingTable;
		this.sentencePairNullBinding = sentencePairNullBinding;
	}
	
	public static AlignmentInformationCompactNulls createAlignmentInformationCompactedNulls(String alignmentString, int sourceLength, int targetLength)
	{
		SentencePairAlignmentChunkingZerosGrouped originalChunking = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceLength, targetLength);
		PositionMappingTable sourcePositionMappingTable = PositionMappingTable.createPositionMappingTable(originalChunking.getNonZeroSourcePositions());
		PositionMappingTable targetPositionMappingTable = PositionMappingTable.createPositionMappingTable(originalChunking.getNonZeroTargetPositions());
		SentencePairNullBinding sentencePairNullBinding = SentencePairNullBinding.createSentencePairNullBinding(originalChunking);
		
		Alignment compactedAlignment = createCompactedAlignment(alignmentString, sourcePositionMappingTable, targetPositionMappingTable);
		String compactedAlignmentString = compactedAlignment.getAlignmentAsString();
		SentencePairAlignmentChunkingZerosGrouped compactedChunking = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(compactedAlignmentString,compactedAlignment.getMinCompatibleSourceLengt(), compactedAlignment.getMinCompatibleTargetLength());
		AlignmentInformationCompactNulls result =  new AlignmentInformationCompactNulls(originalChunking, compactedChunking, sourcePositionMappingTable, targetPositionMappingTable,sentencePairNullBinding);
		result.testConsistency();
		return result;
	}
	
	public SentencePairAlignmentChunkingZerosGrouped getCompactedAlignmentChunking()
	{
		return this.compactedChunking;
	}
	
	public Set<Integer> getTargetBoundNulls(int boundTargetPosition)
	{
		return this.sentencePairNullBinding.getTargetBoundNulls(boundTargetPosition);
	}
	
	public Set<Integer> getSourceBoundNulls(int boundSourcePosition)
	{
		return this.sentencePairNullBinding.getSourceBoundNulls(boundSourcePosition);
	}

	public Set<Integer> getSourceLeftBoundNulls(int boundSourcePosition)
	{
		return this.sentencePairNullBinding.getSourceLeftBoundNulls(boundSourcePosition);
	}

	public Set<Integer> getSourceRightBoundNulls(int boundSourcePosition)
	{
		return this.sentencePairNullBinding.getSourceRightBoundNulls(boundSourcePosition);
	}

	
	
	private static AlignmentPair createCompactedAlignmentPair(AlignmentPair pair, PositionMappingTable sourcePositionMappingTable, PositionMappingTable targetPositionMappingTable)
	{
		AlignmentPair result = new AlignmentPair(sourcePositionMappingTable.getCompactedPosition(pair.getSourcePos()), targetPositionMappingTable.getCompactedPosition(pair.getTargetPos()));  
		return result;
	}
	
	
	private static Alignment createCompactedAlignment(String alignmentString,PositionMappingTable sourcePositionMappingTable, PositionMappingTable targetPositionMappingTable)
	{
		Alignment alignment = Alignment.createAlignment(alignmentString);
		List<AlignmentPair> compactedAlignmentList = new ArrayList<AlignmentPair>(); 
		
		for(int i = 0; i < alignment.size() - 1; i++)
		{
			AlignmentPair pair = alignment.get(i);	
			compactedAlignmentList.add(createCompactedAlignmentPair(pair, sourcePositionMappingTable, targetPositionMappingTable));
		}
		compactedAlignmentList.add(createCompactedAlignmentPair(alignment.get(alignment.size() - 1), sourcePositionMappingTable, targetPositionMappingTable));
		
		Alignment result = Alignment.createAlignment(compactedAlignmentList);
		return result;
	}
	
	public Integer getOriginalSourcePosition(int compactedSourcePosition)
	{
		return this.sourcePositionMappingTable.getOriginalPosition(compactedSourcePosition);
	}
	
	public Integer getCompactedSourcePosition(int originalSourcePosition)
	{
		return this.sourcePositionMappingTable.getCompactedPosition(originalSourcePosition);
	}
	
	public boolean sourcePositionInCompactedChart(int originalSourcePosition)
	{
		return this.sourcePositionMappingTable.hasOriginalPosition(originalSourcePosition);
	}
	
	public Integer getOriginalTargetPosition(int compactedTargetPosition)
	{
		return this.targetPositionMappingTable.getOriginalPosition(compactedTargetPosition);
	}
	
	public Integer getCompactedTargetPosition(int originalTargetPosition)
	{
		return this.targetPositionMappingTable.getCompactedPosition(originalTargetPosition);
	}
	
	public void testConsistency()
	{
		for(AlignmentPair pair : this.compactedChunking.getSentenceAlignments())
		{
			assert(pair != null);
			
			if((this.getOriginalSourcePosition(pair.getSourcePos()) == null) || (this.getOriginalTargetPosition(pair.getTargetPos()) == null))
			{
				throw new RuntimeException("Inconsistent Alignment Iformation, incomplete lookup tables") ;
			}
			
		}
		assert(this.getOriginalTargetPosition(Collections.max(this.getCompactedAlignmentChunking().getNonZeroTargetPositions())) != null);
	}

	@Override
	public SentencePairAlignmentChunkingZerosGrouped getChartInternalAlignmentChunking() 
	{
		return this.compactedChunking;
	}

	@Override
	protected EbitgInferenceTargetProperties createReLexicalizedTargetProperties(
			EbitgInferenceTargetProperties bareTargetProperties) 
	{
		Set<Integer> basicBoundTargetPositions = createReLexicalizedBasicBoundTargetPositions(bareTargetProperties.getBasicBoundTargetPositions());
		Set<Integer> extraBoundTargetPositions = createReLexicalizedExtraBoundTargetPositions(bareTargetProperties.getBasicBoundTargetPositions());
		Set<Integer> targetPositions = new HashSet<Integer>();
		targetPositions.addAll(basicBoundTargetPositions);
		targetPositions.addAll(extraBoundTargetPositions);
		return EbitgInferenceTargetProperties.createEbitgInferenceTargetProperties(Collections.min(targetPositions), Collections.max(targetPositions), basicBoundTargetPositions, extraBoundTargetPositions);
	}
		
	
	private Set<Integer> createReLexicalizedBasicBoundTargetPositions(Set<Integer> compactedTargetPositions)
	{
		Set<Integer> result = new HashSet<Integer>();
		
		for(Integer compactedTargetPosition : compactedTargetPositions)
		{
			result.add(this.getOriginalTargetPosition(compactedTargetPosition));
		}
		return result;
	}

	private Set<Integer> createReLexicalizedExtraBoundTargetPositions(Set<Integer> compactedTargetPositions)
	{
		Set<Integer> result = new HashSet<Integer>();
		for(int compactedTargetPosition : compactedTargetPositions)
		{
			int originalTargetPosition = this.getOriginalTargetPosition(compactedTargetPosition);
			result.addAll(this.getTargetBoundNulls(originalTargetPosition));
		}
		return result;
	}

	
	private Set<Integer> computeExtraBoundTargetPositions(Set<Integer> basicBoundTargetPositions,Set<Integer> targetPositions )
	{
		Set<Integer> extraBoundTargetPositions = new HashSet<Integer>(targetPositions);
		extraBoundTargetPositions.removeAll(basicBoundTargetPositions);
		return extraBoundTargetPositions;
	}
	
	@Override
	protected EbitgInferenceTargetProperties createReLexicalizedTightTargetProperties(
			EbitgInferenceTargetProperties bareTargetProperties) 
	{
		Set<Integer> basicBoundTargetPositions = createReLexicalizedBasicBoundTargetPositions(bareTargetProperties.getBasicBoundTargetPositions());
		Set<Integer> targetPositions = IntegerSetMethods.createConsecutiveRangeSet(Collections.min(basicBoundTargetPositions), Collections.max(basicBoundTargetPositions));
		Set<Integer> extraBoundTargetPositions = computeExtraBoundTargetPositions(basicBoundTargetPositions, targetPositions);
		return EbitgInferenceTargetProperties.createEbitgInferenceTargetProperties(Collections.min(targetPositions), Collections.max(targetPositions), basicBoundTargetPositions, extraBoundTargetPositions);
	}
	
	
	
	@Override
	protected Span createLexicalizedNullExtendedSourceSpan(
			EbitgChartEntry containingChartEntry) 
	{
		return new Span(getLeftNullExtendedPosition(containingChartEntry),getRightNullExtendedPosition(containingChartEntry));
	}

	
	private  int getLeftNullExtendedPosition(EbitgChartEntry containingChartEntry)
	{
		int leftOriginalSourcePosition = this.getOriginalSourcePosition(containingChartEntry.getI());
		int leftNullExtendedSourcePosition = Collections.min(getElementAugmentedSet(this.getSourceLeftBoundNulls(leftOriginalSourcePosition),leftOriginalSourcePosition));
		return leftNullExtendedSourcePosition;
	}
	
	private int getRightNullExtendedPosition(EbitgChartEntry containingChartEntry)
	{
		int rightOriginalSourcePosition = this.getOriginalSourcePosition(containingChartEntry.getJ());
		int rightNullExtendedSourcePosition = Collections.max(getElementAugmentedSet(this.getSourceRightBoundNulls(rightOriginalSourcePosition),rightOriginalSourcePosition));
		return rightNullExtendedSourcePosition;
	}

	private static <E> Set<E> getElementAugmentedSet(Set<E> originalSet,E elementToAdd)
	{
		Set<E> result = new HashSet<E>(originalSet);
		result.add(elementToAdd);
		return result;
	}
	
	protected Span createLexicalizedTightSourceSpan(EbitgChartEntry containingChartEntry)
	{
		return new Span(this.getOriginalSourcePosition(containingChartEntry.getI()),this.getOriginalSourcePosition(containingChartEntry.getJ()));
	}
	
	
	private boolean sourceSpanExistsInCompactedChart(Span originalSourceSpan)
	{
		return  (sourcePositionInCompactedChart(originalSourceSpan.getFirst())  &&
			     sourcePositionInCompactedChart(originalSourceSpan.getSecond()));
	}
	
	
	public Span getChartSpan(Span originalSourceSpan) 
	{
		if(sourceSpanExistsInCompactedChart(originalSourceSpan))	
		{
			Integer i = this.getCompactedSourcePosition(originalSourceSpan.getFirst());
			Integer j = this.getCompactedSourcePosition(originalSourceSpan.getSecond());
			return new Span(i,j);
		}
		return null;
	}

	@Override
	public boolean nullsAreIncludedDuringParsing() 
	{
		return false;
	}
	
	@Override
	public EbitgLexicalizedInference createTightLexicalizedInference(
			EbitgInference inference, Lexicalizer lexicalizer) 
	{
		assert(!(inference instanceof EbitgLexicalizedInference));
		//this.alignmentInformation.testConsistency();
		//this.testInferenceConsistency(inference);
		return EbitgLexicalizedInference.createEbitgTightLexicalizedInferenceFromRawInference(inference, lexicalizer,lexicalizer.getCCGLabeler());
	}

	@Override
	public Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos) 
	{
		int originalTargetPosition = getOriginalTargetPosition(compactedTargetPos);
		Set<Integer> result = new TreeSet<Integer>();
		result.add(originalTargetPosition);
		result.addAll(this.getTargetBoundNulls(originalTargetPosition));
		return result;
		
	}
	
}
