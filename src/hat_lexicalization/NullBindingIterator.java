/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.ArrayList;
import java.util.List;

public class NullBindingIterator 
{
	private final List<NullBindingItem> nullBindingList;
	
	
	private NullBindingIterator(List<NullBindingItem> nullBindingList)
	{
		this.nullBindingList = nullBindingList;
	}
	
	public static NullBindingIterator createNullBindingIterator(SentencePairNullBinding sentencePairNullBinding)
	{
		return new NullBindingIterator(createSourcePlusTargetNullBindingList(sentencePairNullBinding));	
	}
	
	private static  List<NullBindingItem> createSourcePlusTargetNullBindingList(SentencePairNullBinding sentencePairNullBinding)
	{
		List<NullBindingItem> result = new ArrayList<NullBindingItem>();
		result.addAll(sentencePairNullBinding.getSourceNullBindingList());
		result.addAll(sentencePairNullBinding.getTargetNullBindingList());
		return result;
		
	}
	
	private void setPrecedingNullBindingItemsBackToFirstBinding(int changedPos)
	{
		for(int i = 0; i < changedPos; i++)
		{
			this.gotoFirstNullBinding(this.nullBindingList.get(i));
		}
	}
	
	
	private int getPosNullBindingItemToChange()
	{
		int posNullBindingItemToChange = 0;
		
		while(!this.nullBindingList.get(posNullBindingItemToChange).hasMoreGroupings() && (posNullBindingItemToChange < this.nullBindingList.size()))
		{
			posNullBindingItemToChange++;
		}
		return posNullBindingItemToChange;
	}

	
	public boolean hasMoreNullBindings()
	{
		return getPosNullBindingItemToChange() < this.nullBindingList.size();
	}
	
	
	public void nextNullBinding()
	{
		int posToChange = getPosNullBindingItemToChange();
		gotoNextNullBinding(this.nullBindingList.get(posToChange));
		this.setPrecedingNullBindingItemsBackToFirstBinding(posToChange);
	}
	
	private void gotoFirstNullBinding(NullBindingItem nullBindingItem)
	{
		nullBindingItem.gotoFirstGrouping();
	}
	
	
	private void gotoNextNullBinding(NullBindingItem nullBindingItem)
	{
		nullBindingItem.gotoLastGrouping();
	}
}