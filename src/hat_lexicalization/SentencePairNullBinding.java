/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.List;
import java.util.Set;

import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class SentencePairNullBinding 
{
	private final NullBindingTable sourceNullBinding;;
	private final NullBindingTable targetNullBinding;
	
	private SentencePairNullBinding(NullBindingTable sourceNullBinding, NullBindingTable targetNullBinding)
	{
		this.sourceNullBinding = sourceNullBinding;
		this.targetNullBinding = targetNullBinding;
	}

	public static SentencePairNullBinding createSentencePairNullBinding(SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking)
	{
		return new SentencePairNullBinding(NullBindingTable.createSourceNullBindingTable(sentencePairAlignmentChunking), NullBindingTable.createTargetNullBindingTable(sentencePairAlignmentChunking));
	}
	
	public NullBindingItem getLeftSourceNullBindingItem(int sourcePos)
	{
		return sourceNullBinding.getLeftNullBindingItem(sourcePos);
	}
	
	public NullBindingItem getRightSourceNullBindingItem(int sourcePos)
	{
		return sourceNullBinding.getRightNullBindingItem(sourcePos);
	}
	
	public NullBindingItem getLeftTargetNullBindingItem(int targetPos)
	{
		return targetNullBinding.getLeftNullBindingItem(targetPos);
	}
	
	public NullBindingItem getRightTargetNullBindingItem(int targetPos)
	{
		return targetNullBinding.getRightNullBindingItem(targetPos);
	}
	
	public int  getNumSourceNullBindingItems()
	{
		return this.sourceNullBinding.getNumNullBindingItems();
	}
	
	public int  getNumTargetNullBindingItems()
	{
		return this.targetNullBinding.getNumNullBindingItems();
	}
	
	protected List<NullBindingItem> getSourceNullBindingList()
	{
		return this.sourceNullBinding.getNullBindingList();
	}
	
	protected List<NullBindingItem> getTargetNullBindingList()
	{
		return this.targetNullBinding.getNullBindingList();
	}
	
	
	public Set<Integer> getTargetBoundNulls(int boundTargetPosition)
	{
		return this.targetNullBinding.getBoundNulls(boundTargetPosition);
	}

	
	public Set<Integer> getSourceLeftBoundNulls(int boundSourcePosition)
	{
		return this.sourceNullBinding.getLeftBoundNulls(boundSourcePosition);
	}
	
	public Set<Integer> getSourceRightBoundNulls(int boundSourcePosition)
	{
		return this.sourceNullBinding.getRightBoundNulls(boundSourcePosition);
	}
	
	public Set<Integer> getSourceBoundNulls(int boundSourcePosition)
	{
		return this.sourceNullBinding.getBoundNulls(boundSourcePosition);
	}
	
	
	public String toString()
	{
		String result = "SentencePairNullBinding: ";
		result += this.sourceNullBinding.toString();
		result += this.targetNullBinding.toString();
		return result;
	}
	
}
