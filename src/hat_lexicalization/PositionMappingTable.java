/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import util.Sorting;

public class PositionMappingTable 
{
	private final Map<Integer,Integer> originalToCompactedPositionMap;
	private final Map<Integer,Integer> compactedToOriginalPositionMap;
	
	private PositionMappingTable(Map<Integer,Integer> originalToCompactedPosMap,
			Map<Integer,Integer> compactedToOriginalPosMap)
	{
		this.originalToCompactedPositionMap = originalToCompactedPosMap;
		this.compactedToOriginalPositionMap = compactedToOriginalPosMap;
	}

	public static PositionMappingTable createPositionMappingTable(Set<Integer> positionSet)
	{
		List<Integer> sortedPositionsList = Sorting.getSetAsSortedListAscending(positionSet);
		//System.out.println("PositionMappingTable, sortedPositionsList:  " + sortedPositionsList);
		Map<Integer,Integer> originalToCompactedPosMap = createOriginalToCompactedPosMap(sortedPositionsList);
		Map<Integer,Integer> compactedToOriginalPosMap = createCompactedToOriginalPosMap(sortedPositionsList);
		return new PositionMappingTable(originalToCompactedPosMap, compactedToOriginalPosMap);
	}
	
	private static Map<Integer,Integer> createOriginalToCompactedPosMap(List<Integer> sortedPositionsList)
	{
		Map<Integer,Integer> result = new HashMap<Integer, Integer>();
		for(int i = 0; i < sortedPositionsList.size(); i++)
		{
			result.put(sortedPositionsList.get(i),i);
		}
		return result;
	}

	private static Map<Integer,Integer> createCompactedToOriginalPosMap(List<Integer> sortedPositionsList)
	{
		Map<Integer,Integer> result = new HashMap<Integer, Integer>();
		for(int i = 0; i < sortedPositionsList.size(); i++)
		{
			result.put(i,sortedPositionsList.get(i));
		}
		return result;
	}
	
	public Integer getOriginalPosition(int compactedPosition)
	{
		assert(this.compactedToOriginalPositionMap != null);
		//System.out.println("compactedPosition: " + compactedPosition);
		return this.compactedToOriginalPositionMap.get(compactedPosition);
	}
	
	public Integer getCompactedPosition(int originalPosition)
	{
		assert(this.originalToCompactedPositionMap != null);
		return this.originalToCompactedPositionMap.get(originalPosition);
	}
	
	
	public boolean hasOriginalPosition(int originalPosition)
	{
		return this.originalToCompactedPositionMap.containsKey(originalPosition);
	}
	
	public boolean hasCompactedPosition(int compactedPosition)
	{
		return this.compactedToOriginalPositionMap.containsKey(compactedPosition);
	}
	
	public boolean valuesCompactToOriginalEqualsKeysOriginalsToCompact()
	{
		Set<Integer> valuesCompactToOriginal = new HashSet<Integer>( (this.compactedToOriginalPositionMap.values()));
		Set<Integer> keysOriginalToCompact = this.originalToCompactedPositionMap.keySet();
		System.out.println("valuesCompactToOriginal : "  + valuesCompactToOriginal);
		System.out.println("keysOriginalToCompact : "  + keysOriginalToCompact);
		boolean result = valuesCompactToOriginal.equals(keysOriginalToCompact); 
		assert(result);
		return result;
	}

	public boolean keysCompactToOriginalEqualsValuessOriginalsToCompact()
	{
		Set<Integer> keysCompactToOriginal =  this.compactedToOriginalPositionMap.keySet();
		Set<Integer> valuesOriginalToCompact = new HashSet<Integer>(this.originalToCompactedPositionMap.values());
		System.out.println("keysCompactToOriginal : "  + keysCompactToOriginal);
		System.out.println("valuesOriginalToCompact : "  + valuesOriginalToCompact);
		boolean result = keysCompactToOriginal.equals(valuesOriginalToCompact); 
		assert(result);
		return result;
	}
	
	public boolean hasConsitentMappingTables()
	{
		boolean valuesEqualsKeys =  valuesCompactToOriginalEqualsKeysOriginalsToCompact();
		boolean keysEqualsValues =  keysCompactToOriginalEqualsValuessOriginalsToCompact();
		
		assert(keysEqualsValues);
		return valuesEqualsKeys && keysEqualsValues; 
	}   
	
}
