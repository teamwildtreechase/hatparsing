/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class AlignmentInformationExplicitNulls extends AlignmentInformation{

	protected AlignmentInformationExplicitNulls(
			SentencePairAlignmentChunkingZerosGrouped originalChunking) 
	{
		super(originalChunking);
	}

	@Override
	public SentencePairAlignmentChunkingZerosGrouped getChartInternalAlignmentChunking() 
	{
		return this.originalChunking;
	}

	public static AlignmentInformation createAlignmentInformationExplicitNulls(String alignmentString, int sourceLength, int targetLength)
	{
		SentencePairAlignmentChunkingZerosGrouped originalChunking = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceLength, targetLength);
		return new AlignmentInformationExplicitNulls(originalChunking);
	}	
	
	@Override
	public EbitgInferenceTargetProperties createReLexicalizedTargetProperties(
			EbitgInferenceTargetProperties bareTargetProperties) 
	{
		return bareTargetProperties;
	}

	@Override
	public Span createLexicalizedNullExtendedSourceSpan(
			EbitgChartEntry containingChartEntry) 
	{
	
		return new Span(containingChartEntry.getI(), containingChartEntry.getJ());
	}

	@Override
	protected Span createLexicalizedTightSourceSpan(
			EbitgChartEntry containingChartEntry) 
	{
		return new Span(containingChartEntry.getI(), containingChartEntry.getJ());
	}
	

	@Override
	public Span getChartSpan(Span originalSourceSpan) 
	{
		return originalSourceSpan;
	}
	
	@Override
	public boolean nullsAreIncludedDuringParsing() 
	{
		return true;
	}
	
	@Override
	public EbitgLexicalizedInference createTightLexicalizedInference(
			EbitgInference inference, Lexicalizer lexicalizer) 
	{
		assert(!(inference instanceof EbitgLexicalizedInference));
		return EbitgLexicalizedInference.createEbitgLexicalizedInference(inference, lexicalizer,lexicalizer.getCCGLabeler());
	}
	
	
	private boolean bindsExtraNullsLeft(EbitgInferenceTargetProperties targetProperties)
	{
		return targetProperties.getMinTargetPos() < Collections.min(targetProperties.getBasicBoundTargetPositions());
	}
	
	private boolean bindsExtraNullsRight(EbitgInferenceTargetProperties targetProperties)
	{
		return targetProperties.getMaxTargetPos() > Collections.max(targetProperties.getBasicBoundTargetPositions());
	}
	
	
	private boolean targetPropertiesAreTight(EbitgInferenceTargetProperties targetProperties)
	{
		return (!bindsExtraNullsLeft(targetProperties)) && (!bindsExtraNullsRight(targetProperties));
	}
	
	@Override
	protected EbitgInferenceTargetProperties createReLexicalizedTightTargetProperties(
			EbitgInferenceTargetProperties bareTargetProperties) 
	{
		if(!targetPropertiesAreTight(bareTargetProperties))
		{
			throw new RuntimeException("AlignmentInformationExplicitNulls - Error : trying to generate tight target properties form target properties that are not tight");
		}
		
		return bareTargetProperties;
	}

	@Override
	public Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos) 
	{
		Set<Integer> result = new TreeSet<Integer>();
		result.add(compactedTargetPos);
		return result;
	}
	
	
}
