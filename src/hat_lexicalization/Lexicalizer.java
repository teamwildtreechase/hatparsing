/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.List;
import java.util.Set;
import ccgLabeling.CCGLabeler;
import junit.framework.Assert;

import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import util.Span;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;
import util.InputReading;
import util.Utility;

public class Lexicalizer 
{
	final private List<String> sourceWordArray;
	final private List<String> targetWordArray;
	protected final AlignmentInformation alignmentInformation;
	private final CCGLabeler ccgLabeler;
	
	
	protected Lexicalizer (List<String> sourceWordArray,
			List<String> targetWordArray,
			AlignmentInformation alignmentInformation,
			CCGLabeler ccgLabeler)
	{
		this.sourceWordArray = sourceWordArray;
		this.targetWordArray = targetWordArray;
		this.alignmentInformation = alignmentInformation;
		this.ccgLabeler = ccgLabeler;
	}
	
	
	public static Lexicalizer createExplicitNullsLexicalizer(String alignmentString, String sourceString, String targetString, CCGLabeler ccgLabeler) 
	{
		List<String> sourceWordArray = InputReading.constructSentenceWordsArrayList(sourceString);
		List<String> targetWordArray = InputReading.constructSentenceWordsArrayList(targetString);
		AlignmentInformation alignmentInformation = AlignmentInformationExplicitNulls.createAlignmentInformationExplicitNulls(alignmentString, sourceWordArray.size(), targetWordArray.size());
		return new Lexicalizer(sourceWordArray, targetWordArray, alignmentInformation,ccgLabeler);
	}
	

	public static Lexicalizer createCompactLexicalizer(String alignmentString, String sourceString, String targetString, CCGLabeler ccgLabeler) 
	{
		List<String> sourceWordArray = InputReading.constructSentenceWordsArrayList(sourceString);
		List<String> targetWordArray = InputReading.constructSentenceWordsArrayList(targetString);
		AlignmentInformation alignmentInformation = AlignmentInformationCompactNulls.createAlignmentInformationCompactedNulls(alignmentString,sourceWordArray.size(), targetWordArray.size());
		return new Lexicalizer(sourceWordArray, targetWordArray, alignmentInformation,ccgLabeler);
	}
	
	public List<String> getSourceWords()
	{
		return this.sourceWordArray;
	}
	
	public List<String> getTargetWords()
	{
		return this.targetWordArray;
	}
	
	
	
	public SentencePairAlignmentChunkingZerosGrouped getOriginalSentencePairAlignmentChunking()
	{
		return this.alignmentInformation.getOriginalAlignmentChunking();
	}
	
	public EbitgLexicalizedInference createLexicalizedInference(EbitgInference inference)
	{
		return EbitgLexicalizedInference.createEbitgLexicalizedInference(inference, this,ccgLabeler);
	}
	
	public  EbitgLexicalizedInference createTightLexicalizedInference(EbitgInference inference)
	{
		return alignmentInformation.createTightLexicalizedInference(inference, this);
	}
	public Span getChartSpan(Span originalSourceSpan)
	{
		return alignmentInformation.getChartSpan(originalSourceSpan);
	}
	
	public boolean nullsAreIncludedDuringParsing()
	{
		return alignmentInformation.nullsAreIncludedDuringParsing();
	}
	
	
	public EbitgInferenceTargetProperties createReLexicalizedTargetProperties(EbitgInferenceTargetProperties bareTargetProperties)
	{
		return alignmentInformation.createReLexicalizedTargetProperties(bareTargetProperties);
	}
	
	public EbitgInferenceTargetProperties createReLexicalizedTightTargetProperties(EbitgInferenceTargetProperties bareTargetProperties)
	{
		return alignmentInformation.createReLexicalizedTightTargetProperties(bareTargetProperties);
	}
	
	
	public Span createLexicalizedNullExtendedSourceSpan(EbitgChartEntry containingChartEntry)
	{
		return alignmentInformation.createLexicalizedNullExtendedSourceSpan(containingChartEntry);
	}
	
	public Span createLexicalizedTightSourceSpan(EbitgChartEntry containingChartEntry)
	{
		return alignmentInformation.createLexicalizedTightSourceSpan(containingChartEntry);
	}
	
	public SentencePairAlignmentChunkingZerosGrouped getChartInternalAlignmentChunking()
	{
		return this.alignmentInformation.getChartInternalAlignmentChunking();
	}

	
	public String getTargetWord(int position)
	{
		return this.targetWordArray.get(position);
	}
	

	public int getSourceLength()
	{
		return this.sourceWordArray.size();
	}
	
	public int getTargetLength()
	{
		return this.targetWordArray.size();
	}
	
	public void printOriginalAlignmentTriple()
	{
		String result = "";
		result += "<Lexicalizer - original alignment triple> \nSourceString: " + Utility.stringListStringWithoutBrackets(sourceWordArray);
		result += "\ntargetString: " + Utility.stringListStringWithoutBrackets(targetWordArray);
		result += "\nalignments: " + this.alignmentInformation.getOriginalAlignmentChunking().getAlignmentsAsString();
		result += "\n</Lexicalizer - original alignment triple>";
		System.out.println(result);
	}
	
	public void testInferenceConsistency(EbitgInference inference)
	{
		System.out.println("testInferenceConsitency inference: " + inference);
		Assert.assertTrue(this.getChartInternalAlignmentChunking().getTargetPositions().containsAll(inference.getBoundTargetPositions()));
	}
	
	protected CCGLabeler getCCGLabeler()
	{
		return this.ccgLabeler;
	}

	public Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos)
	{
		return this.alignmentInformation.getOriginalPlusAssociatedExtraBoundTargetPositions(compactedTargetPos);
	}
	
	public boolean hasNonEmptyCCGLabeler()
	{
		return ccgLabeler.hasLabelChart();
	}
}
