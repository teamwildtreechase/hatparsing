/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.Set;
import util.Span;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInferenceTargetProperties;
import extended_bitg.EbitgLexicalizedInference;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public abstract class AlignmentInformation 
{

	protected final SentencePairAlignmentChunkingZerosGrouped originalChunking;
	
	
	protected AlignmentInformation(SentencePairAlignmentChunkingZerosGrouped originalChunking)
	{
		this.originalChunking = originalChunking;
	}	
	
	public SentencePairAlignmentChunkingZerosGrouped getOriginalAlignmentChunking()
	{
		return this.originalChunking;
	}
	
	public abstract SentencePairAlignmentChunkingZerosGrouped getChartInternalAlignmentChunking();
	

	public abstract EbitgLexicalizedInference createTightLexicalizedInference(EbitgInference inference, Lexicalizer lexicalizer); 
	protected abstract EbitgInferenceTargetProperties createReLexicalizedTargetProperties(EbitgInferenceTargetProperties bareTargetProperties);
	protected abstract EbitgInferenceTargetProperties createReLexicalizedTightTargetProperties(EbitgInferenceTargetProperties bareTargetProperties);
	protected abstract Span createLexicalizedNullExtendedSourceSpan(EbitgChartEntry containingChartEntry);
	protected abstract Span createLexicalizedTightSourceSpan(EbitgChartEntry containingChartEntry);
	protected abstract Span getChartSpan(Span originalSourceSpan);
	public abstract boolean nullsAreIncludedDuringParsing();
	public abstract Set<Integer> getOriginalPlusAssociatedExtraBoundTargetPositions(int compactedTargetPos);
}
