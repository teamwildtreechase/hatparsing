/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import alignmentStatistics.AlignmentChunk;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class NullBindingTable 
{
	private final List<NullBindingItem> nullBindingList;
	private final Map<Integer,NullBindingItem> leftOfPosNullBindingMap;
	private final Map<Integer,NullBindingItem> rightOfPosNullBindingMap;
	
	private NullBindingTable(List<NullBindingItem> nullBindingList,Map<Integer,NullBindingItem> leftNullBindingMap,Map<Integer,NullBindingItem> rightNullBindingMap)
	{
		this.nullBindingList = nullBindingList;
		this.leftOfPosNullBindingMap = leftNullBindingMap;
		this.rightOfPosNullBindingMap = rightNullBindingMap;
	}

	
	public NullBindingItem getLeftNullBindingItem(int pos)
	{
		return this.leftOfPosNullBindingMap.get(pos);
	}
	
	public NullBindingItem getRightNullBindingItem(int pos)
	{
		return this.rightOfPosNullBindingMap.get(pos);
	}
	
	
	private static NullBindingTable createNullBindingTable(List<NullBindingItem> nullBindingList)
	{
		return new NullBindingTable(nullBindingList, createLeftBindingMap(nullBindingList), createRightBindingMap(nullBindingList));
	}
	
	public static NullBindingTable createSourceNullBindingTable(SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking)
	{
		return createNullBindingTable(createSourceNullBindingList(sentencePairAlignmentChunking));
	}
	
	public static NullBindingTable createTargetNullBindingTable(SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking)
	{
		return createNullBindingTable(createTargetNullBindingList(sentencePairAlignmentChunking));
	}
	
	
	private static Map<Integer,NullBindingItem> createLeftBindingMap(List<NullBindingItem> nullBindingList)
	{
		Map<Integer,NullBindingItem> result = new HashMap<Integer, NullBindingItem>();
		
		for(NullBindingItem item : nullBindingList)
		{
			if(item.getRightAjacentBoundPosition() != null)
			{	
				result.put(item.getRightAjacentBoundPosition(), item);
			}	
		}
		return result;
	}
	
	private static Map<Integer,NullBindingItem> createRightBindingMap(List<NullBindingItem> nullBindingList)
	{
		Map<Integer,NullBindingItem> result = new HashMap<Integer, NullBindingItem>();
		
		for(NullBindingItem item : nullBindingList)
		{
			if(item.getLeftAjacentBoundPosition() != null)
			{	
				result.put(item.getLeftAjacentBoundPosition(), item);
			}	
		}
		return result;
	}
	
	
	private static List<NullBindingItem> createSourceNullBindingList(SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking)
	{
		List<NullBindingItem> result = new ArrayList<NullBindingItem>();
		
		for(AlignmentChunk alignmentChunk: sentencePairAlignmentChunking.getAlignmentChunks())
		{
			if(alignmentChunk.isUnalignedSourceChunk())
			{
				result.add(NullBindingItem.createSourceNullBindingItem(alignmentChunk, sentencePairAlignmentChunking.getSourceLength()));
			}
		}
		return result;
	}
	
	private static List<NullBindingItem> createTargetNullBindingList(SentencePairAlignmentChunkingZerosGrouped sentencePairAlignmentChunking)
	{
		List<NullBindingItem> result = new ArrayList<NullBindingItem>();
		
		for(AlignmentChunk alignmentChunk: sentencePairAlignmentChunking.getAlignmentChunks())
		{
			if(alignmentChunk.isUnalignedTargetChunk())
			{
				result.add(NullBindingItem.createTargetNullBindingItem(alignmentChunk, sentencePairAlignmentChunking.getTargetLength()));
			}
		}
		return result;
	}

	public int getNumNullBindingItems()
	{
		return this.nullBindingList.size();
	}

	protected List<NullBindingItem> getNullBindingList()
	{
		return this.nullBindingList;
	}
	
	
	protected Set<Integer> getLeftBoundNulls(int boundPosition)
	{
		if(this.leftOfPosNullBindingMap.containsKey(boundPosition))
		{	
			NullBindingItem nullBindingItem = this.leftOfPosNullBindingMap.get(boundPosition);
			//System.out.println("boundPosition: " + boundPosition + " nullBindingItem: " + nullBindingItem);
			return new HashSet<Integer>(nullBindingItem.getLeftBoundPositions());
		}
		return new HashSet<Integer>();
	}

	protected Set<Integer> getRightBoundNulls(int boundPosition)
	{
		if(this.rightOfPosNullBindingMap.containsKey(boundPosition))
		{	
			return new HashSet<Integer>(this.rightOfPosNullBindingMap.get(boundPosition).getRightBoundPositions());
		}
		return new HashSet<Integer>();
	}
	
	
	public Set<Integer> getBoundNulls(int boundPosition)
	{
		Set<Integer> result = new HashSet<Integer>();
		result.addAll(getLeftBoundNulls(boundPosition));
		result.addAll(getRightBoundNulls(boundPosition));
		return result;
	}
	
	
	public String toString()
	{
		String result = "<NullBindingItems: >\n";
		
		for(NullBindingItem item : this.nullBindingList)
		{
			result += item + "\n";
		}
		result += "</NullBindingItems: >";
		return result;
	}
	
	
}
