/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package hat_lexicalization;

import java.util.List;

import alignmentStatistics.AlignmentChunk;

public class NullBindingItem 
{
	private static final int AllNullsToRightSplitPoint = -1;
	
	private final Integer leftAjacentBoundPosition;
	private final Integer rightAjacentBoundPosition;
	private final List<Integer> bindablePositions;
	
	
	private int splitPoint; // can range from -1 ... )bindableTargetPositions.size() -1)
	
	private NullBindingItem(Integer leftAjacentBoundPosition, Integer rightAjacentBoundPosition, List<Integer> bindablePositions, int splitPoint)
	{
		this.leftAjacentBoundPosition = leftAjacentBoundPosition;
		this.rightAjacentBoundPosition = rightAjacentBoundPosition;
		this.bindablePositions = bindablePositions;
		this.splitPoint = splitPoint;
	}


	public Integer getLeftAjacentBoundPosition()
	{
		return leftAjacentBoundPosition;
	}
	
	public Integer getRightAjacentBoundPosition()
	{
		return rightAjacentBoundPosition;
	}
	
	public List<Integer> getBindablePositions()
	{
		return bindablePositions;
	}
	
	private static Integer createLeftAjacantSourcePos(AlignmentChunk alignmentChunk, int sourceLength)
	{
		Integer leftAjacentBoundPosition = null;
		int minSourcePos = alignmentChunk.getMinSourcePos();
		
		if(minSourcePos > 0)
		{
			leftAjacentBoundPosition = minSourcePos - 1;
		}
		return leftAjacentBoundPosition;
	}

	private static Integer createRightAjacentSourcePos(AlignmentChunk alignmentChunk, int sourceLength)
	{
		Integer rightAjacentBoundPosition = null;
		
		int maxSourcePos = alignmentChunk.getMaxSourcePos();
	
		if(maxSourcePos < sourceLength - 1)
		{
			rightAjacentBoundPosition = maxSourcePos + 1;
		}
		return rightAjacentBoundPosition;
	}
	

	private static Integer createLeftAjacantTargetPos(AlignmentChunk alignmentChunk, int targetLength)
	{
		Integer leftAjacentBoundPosition = null;
		int minTargetPos = alignmentChunk.getMinTargetPos();
		
		if(minTargetPos > 0)
		{
			leftAjacentBoundPosition = minTargetPos - 1;
		}
		return leftAjacentBoundPosition;
	}

	private static Integer createRightAjacentTargetPos(AlignmentChunk alignmentChunk, int targetLength)
	{
		Integer rightAjacentBoundPosition = null;
		
		int maxTargetPos = alignmentChunk.getMaxTargetPos();
	
		if(maxTargetPos < targetLength - 1)
		{
			rightAjacentBoundPosition = maxTargetPos + 1;
		}
		return rightAjacentBoundPosition;
	}

	
	
	private static int allNullsToLeftSplitPoint(List<Integer> positions)
	{
		return positions.size() -1 ;
	}
	
	private static int createFirstSplitPoint(Integer leftAjacentBoundPosition, Integer rightAjacentBoundPosition, List<Integer> positions)
	{
		int result = allNullsToLeftSplitPoint(positions);
		
		if(leftAjacentBoundPosition == null)
		{
			result = AllNullsToRightSplitPoint;
		}
		return result;
		
	}
	
	public static NullBindingItem createSourceNullBindingItem(AlignmentChunk alignmentChunk, int sourceLength)
	{
		assert(alignmentChunk.isUnalignedSourceChunk());
		Integer leftAjacentBoundPosition = createLeftAjacantSourcePos(alignmentChunk, sourceLength);
		Integer rightAjacentBoundPosition = createRightAjacentSourcePos(alignmentChunk, sourceLength);
		List<Integer> bindablePositions = alignmentChunk.getSourcePositionsAsSortedListAscending();
		int splitPoint = createFirstSplitPoint(leftAjacentBoundPosition, rightAjacentBoundPosition, bindablePositions);
		return new NullBindingItem(leftAjacentBoundPosition, rightAjacentBoundPosition, bindablePositions, splitPoint);
	}
	
	public static NullBindingItem createTargetNullBindingItem(AlignmentChunk alignmentChunk, int targetLength)
	{
		assert(alignmentChunk.isUnalignedTargetChunk());
		assert(alignmentChunk.getTargetPositions().size() > 0);
		
		Integer leftAjacentBoundPosition = createLeftAjacantTargetPos(alignmentChunk, targetLength);
		Integer rightAjacentBoundPosition = createRightAjacentTargetPos(alignmentChunk, targetLength);
		List<Integer> bindablePositions = alignmentChunk.getTargetPositionsAsSortedListAscending();
		int splitPoint = createFirstSplitPoint(leftAjacentBoundPosition, rightAjacentBoundPosition, bindablePositions);
		return new NullBindingItem(leftAjacentBoundPosition, rightAjacentBoundPosition, bindablePositions, splitPoint);
	}
	
	/**
	 * 
	 * @return The set of positions that are bound on the right (from the point of view of the aligned, binding position)
	 */
	public List<Integer> getRightBoundPositions()
	{
		return this.bindablePositions.subList(0, splitPoint + 1);
	}

	/**
	 * 
	 * @return The set of positions that are bound on the left (from the point of view of the aligned, binding position)
	 */
	public List<Integer> getLeftBoundPositions()
	{
		return this.bindablePositions.subList(splitPoint + 1,this.bindablePositions.size());
	}
	
	
	
	private boolean hasLeftAjacantBoundPosition()
	{
		return (this.leftAjacentBoundPosition != null);
	}
	
	private boolean hasRightAjacantBoundPosition()
	{
		return (this.rightAjacentBoundPosition != null);
	}
	
	protected boolean hasMultipleGroupings()
	{
		return (this.hasLeftAjacantBoundPosition() && this.hasRightAjacantBoundPosition());
	}

	private boolean isLastGrouping()
	{
		return this.splitPoint == AllNullsToRightSplitPoint;
	}
	
	protected boolean hasMoreGroupings()
	{
		return (!isLastGrouping());
	}
	
	protected void gotoFirstGrouping()
	{
		assert(hasMultipleGroupings());  // This will go wrong if the assertion fails
		this.splitPoint = allNullsToLeftSplitPoint(this.bindablePositions);
	}
	
	protected void gotoLastGrouping()
	{
		assert(hasMultipleGroupings());  // This will go wrong if the assertion fails
		this.splitPoint = NullBindingItem.AllNullsToRightSplitPoint;
	}
	
	
	public String toString()
	{
		String result = "NullBindingItem<";
		result += this.leftAjacentBoundPosition  + "," + this.bindablePositions  + "," + this.rightAjacentBoundPosition ;
		result += ">";
		return result;
	}
	

}

