/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package arraySkipList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.ThreadLocalRandom;
//This code was inspired on the blog at : 
//http://www.informit.com/guides/content.aspx?g=dotnet&seqNum=876

public class ArraySkipList<T extends Object> implements Map<Integer, T> , Serializable{

	/*
	 * Contents of the array: Indices 0 .. MaxLevel : The positions of the first nodes at these levels 
	 * It is possible to use the Indices MaxLevel+1 ... (MaxLevel * 2 - 1) to store the positions
	 * of the first free (deleted) nodes at level 0 ... MaxLevel. However, we are not really interested 
	 * in deleting nodes, and in order to save memory will leave this optional implementation away for now.
	 */

	// The allowed levels in the skip-list are 0 ... MaxLevel

	/*
	 * The node structure struct node { int key; int valueIndex; int level; int link* 0; int link1; // link2 through link<level-1>
	 * 
	 * }
	 */

	private static final long serialVersionUID = 1L;
	
	// Should be safe for up to 2^MaxLevel + 1 Values
	public static final int MaxLevel = 22;
	private static boolean KeepPointersToDeletedEntries = false;

	// The factor with which the array gets resized, traditionally 2, but
	// See
	// for good arguments why 1.5 seems a better value
	private static final double CapacityGrowthFactor = 1.5;
	private static final int StartCapacity = 1;  // The start capacity in no nodes that will directly fit

	private int[] skipListArray;
	private int currentMaxNodeLevel;
	private int currentLastNodeInArrayIndex;
	private final List<T> valuesList;

	private ArraySkipList(int[] skipListArray, int currentMaxNodeLevel, int currentLastNodeInArrayIndex, List<T> valuesList) {
		this.skipListArray = skipListArray;
		this.currentMaxNodeLevel = currentMaxNodeLevel;
		this.currentLastNodeInArrayIndex = currentLastNodeInArrayIndex;
		
		// We wrap the list by a synchronized list to assure thread safety during 
		// resizing of the list while getting elements at the same time from it.
		this.valuesList = Collections.synchronizedList(valuesList);
	}

	private static <T> ArraySkipList<T> createArraySkipList(int[] skipListArray, List<T> valuesList, int currentMaxNodeLevel, int currentLastNodeInArrayIndex) {
		return new ArraySkipList<T>(skipListArray, currentMaxNodeLevel, currentLastNodeInArrayIndex, valuesList);
	}

	private static int getMaxNodeEncodingSize()
	{
		return getNodeEncodingLengthForNodeWithLevel(MaxLevel);
	}
	
	private static int getDefaultSkipListArrayStartsize() {
		// In order to assure that definitely one node will fit and we won't get problems 
		// in resizing when multiplying the size with factor 1.5 , we 
		// choose this initial size like this (We could otherwise still 
		// get a new array that is to small to hold the new element, if we choose the start size 
		// smaller then firstNodeIndex()  +   getMaxNodeEncodingSize() )
		//
		
		// getArraySkipListIndicesSize() is equal to the size of the pre-amble in the table that is used 
		// for storing the links to the first nodes at different levels and optionally the first
		// free nodes at different levels
		// At least using (2 * getMaxNodeEncodingSize() assures that resizing will always result in sufficient 
		// size to store an extra element
		return Math.max((getArraySkipListIndicesSize()  + (StartCapacity * getMaxNodeEncodingSize())), (2 * getMaxNodeEncodingSize()));
	}

	public static <T> ArraySkipList<T> createArraySkipList() {
		int[] skipListArray = new int[getDefaultSkipListArrayStartsize()];
		initializeSkipListArray(skipListArray);
		List<T> valuesList = new ArrayList<T>(StartCapacity);
		return new ArraySkipList<T>(skipListArray, -1, -1, valuesList);
	}

	private static void initializeSkipListArray(int[] skipListArray) {
		// Set all values to -1 at start
		Arrays.fill(skipListArray, -1);
	}

	public static <T> ArraySkipList<T> createTestArraySkipList() {
		int numStartElements = ((ArraySkipList.MaxLevel + 1) * 1) + 400;
		int[] skipListArray = new int[numStartElements];
		List<T> valuesList = new ArrayList<T>();
		initializeSkipListArray(skipListArray);
		int firstContentsIndex = (ArraySkipList.MaxLevel + 1) * 1;
		skipListArray[firstContentsIndex] = 0; // Key
		skipListArray[firstContentsIndex + 1] = 0; // Value Index
		valuesList.add(null);
		skipListArray[firstContentsIndex + 2] = 1; // Level
		skipListArray[firstContentsIndex + 3] = 5 + firstContentsIndex; // Forward
																		// Pointer
																		// at
																		// level
																		// 0
		skipListArray[firstContentsIndex + 4] = 13 + firstContentsIndex; // Forward
																			// pointer
																			// at
																			// level
																			// 1
		skipListArray[firstContentsIndex + 5] = 2; // Key
		skipListArray[firstContentsIndex + 6] = 1; // Value Index
		valuesList.add(null);
		skipListArray[firstContentsIndex + 7] = 0; // Level
		skipListArray[firstContentsIndex + 8] = 9 + firstContentsIndex; // Forward
																		// pointer
																		// at
																		// level
																		// 0
		skipListArray[firstContentsIndex + 9] = 4; // Key
		skipListArray[firstContentsIndex + 10] = 2; // Value Index
		valuesList.add(null);
		skipListArray[firstContentsIndex + 11] = 0; // Level
		skipListArray[firstContentsIndex + 12] = 13 + firstContentsIndex; // Forward
																			// pointer
																			// at
																			// level
																			// 0
		skipListArray[firstContentsIndex + 13] = 8; // Key
		skipListArray[firstContentsIndex + 14] = 3; // Value Index
		valuesList.add(null);
		skipListArray[firstContentsIndex + 15] = 1; // Level
		skipListArray[firstContentsIndex + 16] = 18 + firstContentsIndex; // Forward
																			// pointer
																			// at
																			// level
																			// 0
		skipListArray[firstContentsIndex + 17] = 18 + firstContentsIndex; // Forward
																			// pointer
																			// at
																			// level
																			// 1
		skipListArray[firstContentsIndex + 18] = 16; // Key
		skipListArray[firstContentsIndex + 19] = 4; // Value Index
		valuesList.add(null);
		skipListArray[firstContentsIndex + 20] = 1; // Level
		skipListArray[firstContentsIndex + 21] = -1; // Forward Pointer at level
														// 0
		skipListArray[firstContentsIndex + 22] = -1; // Forward Pointer at level
														// 1
		skipListArray[0] = firstContentsIndex;
		skipListArray[1] = firstContentsIndex;

		int currentLastNodeInArrayIndex = firstContentsIndex + 18;
		return ArraySkipList.createArraySkipList(skipListArray, valuesList, 1, currentLastNodeInArrayIndex);
	}

	
	private static int getArraySkipListIndicesSize()
	{
		if(KeepPointersToDeletedEntries)
		{
			return ArraySkipList.getNumAllowedLevels() * 2;
		}
		return getNumAllowedLevels();
	}
	
	private static int firstNodeIndex()
	{
		return getArraySkipListIndicesSize();
	}
	
	private int getFirstNodeAtLevel(int level) {
		return skipListArray[level];
	}

	private void setFirstNodeAtLevel(int level, int firstNodeIndex) {
		// System.out.println("setFirstNodeAtLevel(" + level + "," + firstNodeIndex + ")");
		skipListArray[level] = firstNodeIndex;
	}

	private static int getNumAllowedLevels() {
		return MaxLevel + 1;
	}

	/*
	 * This is for when we will implement removing elements, which currently we are not 
	 * doing yet
	private int getFirstFreeNodeAtLevel(int level) {
		return skipListArray[level + getNumAllowedLevels()];
	}*/

	private int getKey(int nodeIndex) {
		return this.skipListArray[nodeIndex];
	}

	private boolean isProperNodeIndex(int nodeIndex) {
		return nodeIndex >= (1 * getNumAllowedLevels());
	}

	private int getLevel(int nodeIndex) {
		assert (isProperNodeIndex(nodeIndex));
		// System.out.println("getLevel - nodeIndex: " + nodeIndex + " level: " + this.skipListArray[nodeIndex + 2]);
		assert (this.skipListArray[nodeIndex + 2] <= MaxLevel);
		int result = this.skipListArray[nodeIndex + 2];
		assert (result >= 0);
		return result;
	}

	private int getNextNodeIndexAtLevel(int nodeIndex, int level) {
		// System.out.println(" getNextNodeIndexAtLevel - nodeIndex: " +
		// nodeIndex + " level: " + level);
		assert (getLevel(nodeIndex) >= level);
		int index = nodeIndex + level + 3;
		int result = this.skipListArray[index];
		// System.out.println("getNextNodeIndexAtLevel - result: " + result);
		assert (isValidNextNodeIndex(result));
		return result;
	}

	private boolean isValidNextNodeIndex(int nextNodeIndex) {
		// System.out.println("isValidNextNode: " + nextNodeIndex);
		if (!((nextNodeIndex == -1) || (nextNodeIndex >= firstNodeIndex()))) {
			System.exit(1);
		}
		return ((nextNodeIndex == -1) || (nextNodeIndex >= firstNodeIndex()));
	}

	private void setNextNodeIndexAtLevel(int nodeIndex, int level, int nextNodeIndex) {
		// System.out.println(" setNextNodeIndexAtLevel - nodeIndex: " +
		// nodeIndex + " level: " + level);
		assert (getLevel(nodeIndex) >= level);
		assert (nextNodeIndex != nodeIndex);
		assert (isValidNextNodeIndex(nextNodeIndex));
		int index = nodeIndex + level + 3;
		// System.out.println("setNextNodeIndexAtLevel : setting skipListArray[" + index + "] = " + nextNodeIndex);
		this.skipListArray[index] = nextNodeIndex;
	}

	private int getHighestFirstNodeIndex(int key) {
		for (int level = currentMaxNodeLevel; level >= 0; level--) {
			int levelFirstNodeIndex = getFirstNodeAtLevel(level);
			// System.out.println("levelFirstNodeIndex: " +
			// levelFirstNodeIndex);
			if (getKey(levelFirstNodeIndex) <= key) {
				return levelFirstNodeIndex;
			}
		}
		return -1;
	}

	private int getSmallerNextNodeIndex(int currentNodeIndex, int key) {
		// System.out.println("getSmallerNextNodeIndex");
		for (int level = getLevel(currentNodeIndex); level >= 0; level--) {
			int nextNodeIndexAtLevel = getNextNodeIndexAtLevel(currentNodeIndex, level);
			// System.out.println("key: " + key + " nextNodeIndexAtLevel : " + nextNodeIndexAtLevel);
			if (isValidIndex(nextNodeIndexAtLevel) && (getKey(nextNodeIndexAtLevel) <= key)) {
				// System.out.println("getSmallerNextNodeIndex returns");
				return nextNodeIndexAtLevel;
			}
		}
		// System.out.println("getSmallerNextNodeIndex returns");
		return -1;
	}

	/**
	 * Takes a key and finds the index of the skip list element with the smallest key smaller then or equal to key
	 * 
	 * @param key
	 * @return
	 */
	private int skipSearch(int key) {
		int nodeIndex = getHighestFirstNodeIndex(key);
		// System.out.println("getHighestFirstNodeIndex(" + key + ") = " + nodeIndex);

		if (nodeIndex > 0) {
			int highestSmallerNextNodeIndex = getSmallerNextNodeIndex(nodeIndex, key);
			while (highestSmallerNextNodeIndex >= 0) {
				nodeIndex = highestSmallerNextNodeIndex;
				// System.out.println("skipSearch - nodeIndex: " + nodeIndex + " node value: " + getKey(nodeIndex));

				highestSmallerNextNodeIndex = getSmallerNextNodeIndex(nodeIndex, key);
			}
		}
		return nodeIndex;
	}

	/**
	 * Takes a key and finds the index of the skip list element with the smallest key smaller then or equal to key
	 * 
	 * @param key
	 * @return
	 */
	private List<Integer> skipSearchWithVisitedNodesReturn(int key) {
		List<Integer> result = new ArrayList<Integer>();
		int nodeIndex = getHighestFirstNodeIndex(key);
		// System.out.println("getHighestFirstNodeIndex(" + key + ") = " + nodeIndex);

		if (nodeIndex > 0) {
			result.add(nodeIndex);
			int highestSmallerNextNodeIndex = getSmallerNextNodeIndex(nodeIndex, key);
			while (highestSmallerNextNodeIndex >= 0) {
				nodeIndex = highestSmallerNextNodeIndex;
				// System.out.println("skipSearchWithVisitedNodesReturn - nodeIndex" + nodeIndex);
				result.add(nodeIndex);
				highestSmallerNextNodeIndex = getSmallerNextNodeIndex(nodeIndex, key);
			}
		}
		return result;
	}

	private boolean isValidIndex(int index) {
		return (index >= (firstNodeIndex()));
	}

	/*
	private int getIdexForKey(int key) {
		int closestSmallerOrEqualElementIndex = skipSearch(key);
		if (isValidIndex(closestSmallerOrEqualElementIndex) && getKey(closestSmallerOrEqualElementIndex) == key) {
			return closestSmallerOrEqualElementIndex;
		}
		return -1;
	}*/

	private boolean trueWithFiftyPercentChance() {
		// Fixed : use the Thread local version of random, which does not suffer from lock contention
		// See : http://code-o-matic.blogspot.nl/2009/01/beware-of-hidden-contention-of.html 
		// for an explanation on this
		return (ThreadLocalRandom.current().nextDouble() < 0.5);
		
	}

	private int getLevelForNewElelement() {
		int level = 0;

		while ((level < MaxLevel) && (trueWithFiftyPercentChance())) {
			level++;
		}
		return level;
	}

	private static int getNodeEncodingLengthForNodeWithLevel(int level) {
		return (level + 1) + 3;
	}

	private int getNodeEncodingLength(int nodeIndex) {
		return getNodeEncodingLengthForNodeWithLevel(getLevel(nodeIndex));
	}

	private int getNewNodeInsertionIndex() {
		if (this.currentLastNodeInArrayIndex > 0) {
			return this.currentLastNodeInArrayIndex + getNodeEncodingLength(this.currentLastNodeInArrayIndex);
		}
		return firstNodeIndex();
	}

	private void insertAfterNodeAtLevel(int visitedNodeIndex, int nodeInsertionIndex, int level) {
		int nextNodeIndexAtLevel = getNextNodeIndexAtLevel(visitedNodeIndex, level);
		// Set the pointer from the new node to the next node at level level
		// System.out.println(" insertAfterNodeAtLevel " + level +
		// " nextNodeIndexAtLevel :" + nextNodeIndexAtLevel);
		setNextNodeIndexAtLevel(nodeInsertionIndex, level, nextNodeIndexAtLevel);
		// Set the pointer from the visited node to the inserted node at level i
		setNextNodeIndexAtLevel(visitedNodeIndex, level, nodeInsertionIndex);
	}

	private void updateFirstNodeAtLevelValues(int nodeInsertionIndex, int level, int visitedNodeLevel) {
		// System.out.println("updateFirstNodeAtLevelValues - visitedNodLevel:" + visitedNodeLevel + " level " + level);
		if (visitedNodeLevel < level) {
			for (int i = level; i > visitedNodeLevel; i--) {
				setFirstNodeAtLevel(i, nodeInsertionIndex);
			}
		}
	}

	private void setNextNodeAtLevelValuesForLevelsWhereNoSmallerNodeAtLevelExistsButBiggerNodeAtLevelMight(int nodeInsertionIndex, int level, int visitedNodeLevel) {
		if (visitedNodeLevel < level) {
			for (int i = level; i > visitedNodeLevel; i--) {
				if (getFirstNodeAtLevel(i) != -1) {
					setNextNodeIndexAtLevel(nodeInsertionIndex, i, getFirstNodeAtLevel(i));
				}
			}
		}
	}

	private void addNodeLinksForNodeInsertedAfterOtherNodes(int nodeInsertionIndex, int level, List<Integer> skipListNodesVisitedToReachNode) {
		int visitedNodeLevel = -1;
		int previousVisitedNodeLevel = -1;
		int listIndex = skipListNodesVisitedToReachNode.size() - 1;
		while ((listIndex >= 0) && (visitedNodeLevel < level)) {
			int visitedNodeIndex = skipListNodesVisitedToReachNode.get(listIndex);
			// System.out.println("Visited node index: " + visitedNodeIndex);
			visitedNodeLevel = getLevel(visitedNodeIndex);
			// System.out.println("Visited node level: " + visitedNodeLevel);

			assert (visitedNodeIndex != nodeInsertionIndex);

			if (visitedNodeLevel > previousVisitedNodeLevel) {
				for (int i = Math.min(visitedNodeLevel, level); i > previousVisitedNodeLevel; i--) {
					insertAfterNodeAtLevel(visitedNodeIndex, nodeInsertionIndex, i);
				}
				previousVisitedNodeLevel = visitedNodeLevel;
			}
			listIndex--;
		}

		setNextNodeAtLevelValuesForLevelsWhereNoSmallerNodeAtLevelExistsButBiggerNodeAtLevelMight(nodeInsertionIndex, level, visitedNodeLevel);
		updateFirstNodeAtLevelValues(nodeInsertionIndex, level, visitedNodeLevel);
	}

	private void addNodeLinksForNodeInsertedBeforeOtherNodes(int nodeInsertionIndex, int level) {
		setNextNodeAtLevelValuesForLevelsWhereNoSmallerNodeAtLevelExistsButBiggerNodeAtLevelMight(nodeInsertionIndex, level, -1);
		updateFirstNodeAtLevelValues(nodeInsertionIndex, level, -1);
	}

	private void addNodeLinks(int nodeInsertionIndex, int level, List<Integer> skipListNodesVisitedToReachNode) {

		// System.out.println("addNodeLinks - level: " + level);

		if (skipListNodesVisitedToReachNode.isEmpty()) {
			addNodeLinksForNodeInsertedBeforeOtherNodes(nodeInsertionIndex, level);
		} else {
			addNodeLinksForNodeInsertedAfterOtherNodes(nodeInsertionIndex, level, skipListNodesVisitedToReachNode);
		}

	}

	private void setCurrentLastNodeInArrayIndex(int index) {
		// System.out.println(">>>Setting currentLastNodeInArrayIndex to " + index);
		this.currentLastNodeInArrayIndex = index;
	}

	private int getValueIndex(int nodeIndex) {
		return this.skipListArray[nodeIndex + 1];
	}

	private T getValue(int nodeIndex) {
		//System.out.println("getValue for index: " + getValueIndex(nodeIndex) + " list lize: " + this.valuesList.size());
		return this.valuesList.get(getValueIndex(nodeIndex));
	}

	private T getValueWaitIfNotYetAdded(int nodeIndex) {
		int valueIndex = getValueIndex(nodeIndex);
		
		while(valueIndex >= this.valuesList.size())
		{
			System.out.println("Waiting for value to be added...");
		}
		return this.valuesList.get(valueIndex);
	}
	
	private void setValueIndex(int nodeIndex, int newValueIndex) {
		this.skipListArray[nodeIndex + 1] = newValueIndex;
	}

	private void updateCurrentMaxNodeLevel(int level) {
		this.currentMaxNodeLevel = Math.max(this.currentMaxNodeLevel, level);
	}

	private boolean spaceForNewNodeExists(int nodeInsertionIndex, int level) {
		int lastIndexNewNode = nodeInsertionIndex + getNodeEncodingLengthForNodeWithLevel(level) - 1;
		return ((this.skipListArray.length - 1) >= lastIndexNewNode);
	}

	private void resizeArrayIfNotEnoughSpaceLeft(int nodeInsertionIndex, int level) {
		// this.printSkipList();

		if (!spaceForNewNodeExists(nodeInsertionIndex, level)) {
			int newCapacity = (int) ((skipListArray.length * CapacityGrowthFactor) + 1);
			// Assure that at least the new element will fit. (If the Capacity growth factor change this may be necessary)
			newCapacity = Math.max(newCapacity, skipListArray.length + getNodeEncodingLengthForNodeWithLevel(level));
			int[] newSkipList = Arrays.copyOf(skipListArray, newCapacity);
			// Instantiate the extra space in the new array with value -1
			Arrays.fill(newSkipList, skipListArray.length, newSkipList.length, -1);
			this.skipListArray = newSkipList;
			assert (this.skipListArray[nodeInsertionIndex] == -1);
			assert(spaceForNewNodeExists(nodeInsertionIndex, level));
		}
	}

	private void insertNode(int nodeInsertionIndex, int key, int valueIndex, int level, List<Integer> skipListNodesVisitedToReachNode) {

		resizeArrayIfNotEnoughSpaceLeft(nodeInsertionIndex, level);


		this.skipListArray[nodeInsertionIndex] = key;
		this.skipListArray[nodeInsertionIndex + 1] = valueIndex;
		this.skipListArray[nodeInsertionIndex + 2] = level;

		// Set new next node indices to -1 at start
		for (int i = nodeInsertionIndex + 3; i <= nodeInsertionIndex + 3 + level; i++) {
			this.skipListArray[i] = -1;
		}

		setCurrentLastNodeInArrayIndex(nodeInsertionIndex);
		addNodeLinks(nodeInsertionIndex, level, skipListNodesVisitedToReachNode);
		updateCurrentMaxNodeLevel(level);
	}

	private int getLastElementSkipListNodesVisitedToReachNode(List<Integer> skipListNodesVisitedToReachNode) {
		int lastElementSkipListNodesVisitedToReachNode = -1;
		if (!skipListNodesVisitedToReachNode.isEmpty()) {
			lastElementSkipListNodesVisitedToReachNode = skipListNodesVisitedToReachNode.get(skipListNodesVisitedToReachNode.size() - 1);
		}
		return lastElementSkipListNodesVisitedToReachNode;
	}

	private boolean keyNotPresentInSkipList(List<Integer> skipListNodesVisitedToReachNode, int key) {

		return skipListNodesVisitedToReachNode.isEmpty() || (getKey(getLastElementSkipListNodesVisitedToReachNode(skipListNodesVisitedToReachNode)) != key);
	}

	private boolean insertKeyValueIndex(List<Integer> skipListNodesVisitedToReachNode, int key, int valueIndex) {
		int levelNewElement = getLevelForNewElelement();
		int nodeInsertionIndex = getNewNodeInsertionIndex();
		insertNode(nodeInsertionIndex, key, valueIndex, levelNewElement, skipListNodesVisitedToReachNode);
		return true;
	}

	private boolean putNewValueIndexForExistingKey(List<Integer> skipListNodesVisitedToReachNode, int key, int valueIndex) {
		int lastElementSkipListNodesVisitedToReachNode = skipListNodesVisitedToReachNode.get(skipListNodesVisitedToReachNode.size() - 1);
		int lastElementKey = getKey(lastElementSkipListNodesVisitedToReachNode);
		assert (lastElementKey == key);
		setValueIndex(lastElementSkipListNodesVisitedToReachNode, valueIndex);
		return true;
	}

	
	private int insertKeyValueIndexIfKeyAbsent(int key, int valueIndex) {
		List<Integer> skipListNodesVisitedToReachNode = skipSearchWithVisitedNodesReturn(key);

		if (keyNotPresentInSkipList(skipListNodesVisitedToReachNode, key)) {
			insertKeyValueIndex(skipListNodesVisitedToReachNode, key, valueIndex);
			return -1;
		}
		else
		{
			int oldValueIndex = getValueIndex(getLastElementSkipListNodesVisitedToReachNode(skipListNodesVisitedToReachNode));
			//System.out.println(" oldValueIndex: " + oldValueIndex + " insertKeyValueIndexIfKeyAbsent : key " + key + " already present in skiplist");
			//System.out.println("skipListNodesVisitedToReachNode.isEmpty() : " + skipListNodesVisitedToReachNode.isEmpty());
			return oldValueIndex;
		}
	}

	private int putValueIndexForKey(int key, int valueIndex) {
		List<Integer> skipListNodesVisitedToReachNode = skipSearchWithVisitedNodesReturn(key);

		if (keyNotPresentInSkipList(skipListNodesVisitedToReachNode, key)) {
			insertKeyValueIndex(skipListNodesVisitedToReachNode, key, valueIndex);
			return -1;
		} else {
			//System.out.println(" putValueIndexForKey : key " + key + " already present in skiplist");
			//System.out.println("skipListNodesVisitedToReachNode.isEmpty() : " + skipListNodesVisitedToReachNode.isEmpty());
			int oldValueIndex = getValueIndex(getLastElementSkipListNodesVisitedToReachNode(skipListNodesVisitedToReachNode));
			putNewValueIndexForExistingKey(skipListNodesVisitedToReachNode, key, valueIndex);
			return oldValueIndex;
		}
	}

	public void printRawArray() {
		System.out.println("<skipListArray>");
		for (int i = 0; i < this.skipListArray.length; i++) {
			System.out.print("A[" + i + "]=" + skipListArray[i] + ", ");
			if ((i % 10) == 0) {
				System.out.print("\n");
			}
		}
		System.out.println("\n</skipListArray>");
	}

	private String skipListItemString(int index) {
		String result = "<Skip list Item>";
		result += "\n ArrayIndex[" + index + "]  " + skipListArray[index] + " key";
		result += "\n ArrayIndex[" + (index + 1) + "]  " + skipListArray[index + 1] + " value-index";
		result += "\n ArrayIndex[" + (index + 2) + "]  " + skipListArray[index + 2] + " level";

		for (int level = 0; level <= getLevel(index); level++) {
			int arrayIndex = index + level + 3;
			result += "\n ArrayIndex[" + arrayIndex + "]  " + skipListArray[arrayIndex] + " forward pointer at level " + level;
		}
		result += "\n</Skip list Item>";
		return result;
	}

	public void printSkipList() {
		System.out.println("<skipList>");
		System.out.println("<first node indices at level>");
		for (int i = 0; i <= MaxLevel; i++) {
			System.out.println("First index at level " + i + " : " + skipListArray[i]);
		}
		System.out.println("</first node indices at level>");

		System.out.println("<first free indices at level>");
		for (int i = 0; i <= MaxLevel; i++) {
			System.out.println("First free index at level " + i + " : " + skipListArray[i + MaxLevel]);
		}
		System.out.println("</first free indices at level>");

		int index = firstNodeIndex();
		System.out.println("index:  " + index + " getKey(index) " + getKey(index));
		while ((index < this.currentLastNodeInArrayIndex) && (getKey(index) != -1)) {
			System.out.println(skipListItemString(index));

			index = index + getNodeEncodingLength(index);
		}
		// Print the last element if present
		if (currentLastNodeInArrayIndex >= firstNodeIndex()) {
			System.out.println(skipListItemString(this.currentLastNodeInArrayIndex));
		}

		System.out.println("\nUsable array places : [" + index + "- " + (this.skipListArray.length - 1) + "]");

		System.out.println("\n</skipList>");
	}

	@Override
	public int size() {
		// This is correct if we assure that no keys can be inserted without inserting a
		// corresponding value, which should be the case anyhow
		return this.valuesList.size();
	}

	@Override
	public boolean isEmpty() {
		return this.valuesList.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		if (key instanceof Integer) {
			return (this.skipSearch((Integer) key) >= 0);
		}
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		return this.valuesList.contains(value);
	}

	@Override
	public T get(Object key) {
		T result = null;
		if (key instanceof Integer) {
			int index = this.skipSearch((Integer) key);
			//System.out.println("ArraySkipList.get() - Index: " + index);
			if ((index >= 0) && (getKey(index) == ((Integer)key).intValue())) {
				result = getValueWaitIfNotYetAdded(index);
			}
		}
		return result;
	}

	private void addValueToValuesList(T value)
	{
		this.valuesList.add(value);
	}
	
	@Override
	public T put(Integer key, T value) {
		int newValueIndex = this.size();
		int oldValueIndex = this.putValueIndexForKey(key, newValueIndex);
		T oldValue = null;
		if (oldValueIndex >= 0) {
			oldValue = this.valuesList.get(oldValueIndex);
		}
		addValueToValuesList(value);
		return oldValue;
	}

	public T putIfAbsent(Integer key, T value) {
		int newValueIndex = this.size();
		int oldValueIndex = this.insertKeyValueIndexIfKeyAbsent(key, newValueIndex);
		if(oldValueIndex >= 0)
		{
			return this.valuesList.get(oldValueIndex);
		}
		else
		{
			addValueToValuesList(value);
			return null;
		}
	}
	
	@Override
	public void putAll(Map<? extends Integer, ? extends T> m) {
		for (Entry<? extends Integer, ? extends T> entry : m.entrySet()) {
			this.put(entry.getKey(), entry.getValue());
		}
	}

	private List<Integer> getNodeIndices() {
		List<Integer> result = new ArrayList<Integer>();
		int firstNodeIndex = getFirstNodeAtLevel(0);

		if (firstNodeIndex >= 0) {
			result.add(firstNodeIndex);

			int nextNodeIndex = getNextNodeIndexAtLevel(firstNodeIndex, 0);
			while (nextNodeIndex >= 0) {
				result.add(nextNodeIndex);
				nextNodeIndex = getNextNodeIndexAtLevel(firstNodeIndex, 0);
			}
		}
		return result;
	}

	@Override
	public Set<Integer> keySet() {

		Set<Integer> result = new HashSet<Integer>();

		for (Integer nodeIndex : getNodeIndices()) {
			result.add(getKey(nodeIndex));
		}

		return result;
	}

	@Override
	public Collection<T> values() {
		return this.valuesList;
	}

	@Override
	public Set<java.util.Map.Entry<Integer, T>> entrySet() {
		Set<java.util.Map.Entry<Integer, T>> result = new HashSet<Map.Entry<Integer, T>>();

		for (Integer nodeIndex : getNodeIndices()) {
			Entry<Integer, T> entry = new SimpleEntry<Integer, T>(getKey(nodeIndex), getValue(nodeIndex));
			result.add(entry);
		}
		return null;
	}

	@Override
	public void clear() {
		throw new RuntimeException("ArraySkipList - clear - Not Implemented");
	}

	@Override
	public T remove(Object key) {
		throw new RuntimeException("ArraySkipList - remove - Not Implemented");
	}

}
