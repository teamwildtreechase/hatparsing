/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package arraySkipList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArraySkipListPerformanceEvaluation {
	

	public static void plainInsertionSpeedTest() 
	{
		System.out.println("==========================\n<Testing  plain insertion> time\n==========================\n");
		
		ArraySkipList<Integer> testSkipList = ArraySkipList.createArraySkipList();
		List<Integer> list = new ArrayList<Integer>();
		int numTestElements = 100000;
		for(int i = 0; i < numTestElements; i++)
		{
			list.add(i);
		}
		// Randomly permute the list
		Collections.shuffle(list);
		
		
		long beginTime = System.currentTimeMillis();
		for(Integer element : list)
		{
			Integer previousElement = testSkipList.put(element, element);
			
			if(previousElement != null )
			{
				testSkipList.printRawArray();
			}
			
			assert(previousElement == null);
		}
		
		long passedTime = System.currentTimeMillis() - beginTime;
		System.out.println(">>>Insertion into skiplist took " + passedTime + "ms");
		
		List<Integer> arrayInsertTestList = new ArrayList<Integer>();
		beginTime = System.currentTimeMillis();
		for(Integer element : list)
		{

			int insertionIndex = Collections.binarySearch(arrayInsertTestList, element);
			if(insertionIndex < 0)
			{
				insertionIndex  = ~insertionIndex;
				arrayInsertTestList.add(insertionIndex, element);
			}
		}	

		passedTime = System.currentTimeMillis() - beginTime;
		System.out.println(">>>Insertion into array took " + passedTime + "ms");
		System.out.println("==========================\n</Testing  plain insertion> time\n==========================\n");
	}

	
	private static void testInsertionTime(List<Integer> list, ArraySkipList<Integer> testSkipList, List<Integer> arrayInsertTestList, int numBatches, int batchLength)
	{

		System.out.println("==========================\n<Testing insertion> time\n==========================\n");
		
		long beginTime = System.currentTimeMillis();
		long totalSkipListInsertionTime = 0;
		long totalArrayInsertionTime = 0;
		
		for(int i = 0; i < numBatches; i++)
		{
			beginTime = System.currentTimeMillis();
			for(int j = 0; j < batchLength; j++)
			{	
				int elementIndex = i * batchLength + j;
				Integer element = list.get(elementIndex);
				Integer previousElement = testSkipList.put(element, element);
				assert(previousElement == null);
			}	
			long passedTimeSkipList = System.currentTimeMillis() - beginTime;
			totalSkipListInsertionTime += passedTimeSkipList;
			
			beginTime = System.currentTimeMillis();
			for(int j = 0; j < batchLength; j++)
			{
				int elementIndex = i * batchLength + j;
				int element = list.get(elementIndex);
				int insertionIndex = Collections.binarySearch(arrayInsertTestList, element);
				if(insertionIndex < 0)
				{
					insertionIndex  = ~insertionIndex;
					arrayInsertTestList.add(insertionIndex, element);
				}
			}	
			long passedTimeArray = System.currentTimeMillis() - beginTime;
			totalArrayInsertionTime += passedTimeArray;
			
			if(passedTimeSkipList < passedTimeArray)
			{
				String batchString = " : elements " + (i * batchLength) + " - " + (((i+1) * batchLength) - 1); 
				System.out.println("Skip list faster then array for adding elements in batch  number "+ i + batchString);
				System.out.println("skip list took : " + passedTimeSkipList + "ms for insertion");
				System.out.println("Array with binary search and shifting took : " + passedTimeArray + "ms for insertion");
			}
		}
	
		System.out.println("--------------------------");
		System.out.println("Total time passed insertion skip list: " + totalSkipListInsertionTime + "ms");
		System.out.println("Total time passed insertion array : " + totalArrayInsertionTime + "ms");
		System.out.println("--------------------------");
		System.out.println("==========================\n</Testing insertion> time\n==========================\n");
	}
	
	
	private static void testRetrievalTime(List<Integer> list, ArraySkipList<Integer> testSkipList, List<Integer> arrayInsertTestList )
	{
		System.out.println("==========================\n<Testing retrieval time>\n==========================");
		int numTestElements = list.size();
		long beginTime = System.currentTimeMillis();
		for(int i = 0; i < numTestElements; i++)
		{
			Integer key = list.get(i);
			Integer value = testSkipList.get(key);
			assert(value.equals(key));
		}
		long passedTime = System.currentTimeMillis() - beginTime;
		System.out.println("Skip list : " + passedTime + "ms for retrieval");
		
		
		beginTime = System.currentTimeMillis();
		for(int i = 0; i < numTestElements; i++)
		{
			Integer element = list.get(i);
			Collections.binarySearch(arrayInsertTestList , element);
		}
		passedTime = System.currentTimeMillis() - beginTime;
		System.out.println("Array with binary search and shifting took : " + passedTime + "ms for retrieval");
		System.out.println("==========================\n</Testing retrieval time>\n==========================\n");
		
	}
	
	

	public static void whenIsSkipListFasterTest() 
	{
		ArraySkipList<Integer> testSkipList = ArraySkipList.createArraySkipList();
		List<Integer> list = new ArrayList<Integer>();
		
		int numBatches = 10;
		int batchLength = 10000;
		int numTestElements = numBatches * batchLength;
		for(int i = 0; i < numTestElements; i++)
		{
			list.add(i);
		}
		// Randomly permute the list
		Collections.shuffle(list);
		
		List<Integer> arrayInsertTestList = new ArrayList<Integer>();
		
		testInsertionTime(list, testSkipList, arrayInsertTestList, numBatches, batchLength);
		testRetrievalTime(list, testSkipList, arrayInsertTestList);
	
		
	}
	
	public static void main(String[] args)
	{
		plainInsertionSpeedTest();
		whenIsSkipListFasterTest();
	}

}
