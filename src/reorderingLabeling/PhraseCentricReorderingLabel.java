/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.List;


public abstract class PhraseCentricReorderingLabel extends ReorderingLabel{ 

	private static final long serialVersionUID = 1L;
	protected static final String MONOTONE_STRING = "MONO";
	protected static final String ATOMIC_STRING = "ATOMIC";
	protected static final String INVERTED_STRING = "BITT";
	protected static final String PERMUTATION_STRING = "PET";
	protected static final String HAT_STRING = "HAT";
	
	protected PhraseCentricReorderingLabel(String labelString) {
		super(labelString);
	}

	protected abstract List<ReorderingLabel> getAllReorderingLabelsExceptPhrase();
	

	@Override
	public List<ReorderingLabel> getReorderingLabelsConsideredForSmoothing() {
		return getAllReorderingLabelsExceptPhrase();
	}
	
}
