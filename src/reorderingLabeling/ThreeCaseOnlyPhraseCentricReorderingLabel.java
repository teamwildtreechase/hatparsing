/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import extended_bitg.EbitgInference;
import extended_bitg.EbitgInference.InferenceType;

public class ThreeCaseOnlyPhraseCentricReorderingLabel extends PhraseCentricReorderingLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String CATCH_ALL_COMPLEX_LABEL = "COMPLEX";

	private static final String THREE_CASES_ONLY_PHRASE_CENTRIC_REORDER_LABEL = "threeCasesOnlyPhraseCentricReorderLabel";

	public static final ThreeCaseOnlyPhraseCentricReorderingLabel MONOTONE_LABEL = new ThreeCaseOnlyPhraseCentricReorderingLabel(MONOTONE_STRING);
	public static final ThreeCaseOnlyPhraseCentricReorderingLabel INVERTED_LABEL = new ThreeCaseOnlyPhraseCentricReorderingLabel(INVERTED_STRING);
	public static final ThreeCaseOnlyPhraseCentricReorderingLabel COMPLEX_LABEL = new ThreeCaseOnlyPhraseCentricReorderingLabel(CATCH_ALL_COMPLEX_LABEL);

	private static final List<ThreeCaseOnlyPhraseCentricReorderingLabel> ALL_REORDERING_LABELS = Arrays.asList(MONOTONE_LABEL, INVERTED_LABEL, COMPLEX_LABEL);

	protected ThreeCaseOnlyPhraseCentricReorderingLabel() {
		super(null);
	}

	protected ThreeCaseOnlyPhraseCentricReorderingLabel(String labelString) {
		super(labelString);
	}
	
	@Override
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
		if (inference.getInferenceType().equals(InferenceType.BITT)) {
			if (inference.isInverted()) {
				return INVERTED_LABEL;
			} else {
				return MONOTONE_LABEL;
			}
		} else if (inference.getInferenceType().equals(InferenceType.PET) || inference.getInferenceType().equals(InferenceType.HAT) || inference.getInferenceType().equals(InferenceType.Atomic)) {
			return COMPLEX_LABEL;
		} else {
			throw new RuntimeException("Error : The inference is of the wrong type to get a reordering label - observed type: " + inference.getInferenceType());
		}
	}

	private static ReorderingLabel createCoarsePhraseCentricReorderingLabelForLabelContainingString(String labelContainingString) {
		if (labelContainingString.contains(MONOTONE_STRING)) {
			return MONOTONE_LABEL;
		} else if (labelContainingString.contains(INVERTED_STRING)) {
			return INVERTED_LABEL;
		} else if (labelContainingString.contains(CATCH_ALL_COMPLEX_LABEL)) {
			return COMPLEX_LABEL;
		} else {
			throw new RuntimeException("Error :  createParentRelativeReorderingLabelForLabelContainingString( - String " + labelContainingString + " contains no recognized reordering label");
		}
	}

	public static String getLabelTypeName() {
		return THREE_CASES_ONLY_PHRASE_CENTRIC_REORDER_LABEL;
	}

	@Override
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(String reorderingLabelContainingString) {
		return createCoarsePhraseCentricReorderingLabelForLabelContainingString(reorderingLabelContainingString);
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return new ThreeCaseOnlyPhraseCentricReorderingLabel();
	}

	@Override
	protected List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(ALL_REORDERING_LABELS);
		return result;
	}

	@Override
	public int getHatFineReorderingFeaturesCategoryMappingForLabel() {

		if (this.equals(MONOTONE_LABEL)) {
			return 0;
		} else if (this.equals(INVERTED_LABEL)) {
			return 1;
		} else if (this.equals(COMPLEX_LABEL)) {
			return 2;
		} else {
			return -1;
		}
	}

	@Override
	public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
		if (this.equals(MONOTONE_LABEL)) {
			return 0;
		} else if (this.equals(INVERTED_LABEL) || this.equals(COMPLEX_LABEL)) {
			return 1;
		} else {
			return -1;
		}
	}

	@Override
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
		if (labelName.equals(MONOTONE_STRING)) {
			return MONOTONE_LABEL;
		} else if (labelName.equals(INVERTED_STRING)) {
			return INVERTED_LABEL;
		} else if (labelName.equals(CATCH_ALL_COMPLEX_LABEL)) {
			return COMPLEX_LABEL;
		} else {
			throw new RuntimeException("Error: unrecognized label name");
		}
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
		return 3;
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
		return 2;
	}
	
	@Override
	public boolean isBinaryReorderingLabel() {
		return (this.equals(MONOTONE_LABEL) || this.equals(INVERTED_LABEL));
	}

	@Override
	public boolean producesDoubleReorderingLabels() {
		return false;
	}
	
}
