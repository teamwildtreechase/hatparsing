/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.List;


public abstract class ParentRelativeReorderingLabel extends ReorderingLabel {
	private static final long serialVersionUID = 1L;
	protected static final String TOP_NODE_STRING = "TOP.";
	protected static final String EMBEDDED_FULLY_DISCONTINUOUS_STRING = "E.F.D.";
	protected static final String EMBEDDED_FULLY_MONOTONE_STRING = "E.F.M.";
	protected static final String EMBEDDED_FULLY_INVERTED_STRING = "E.F.I.";

	
	protected ParentRelativeReorderingLabel(String labelString) {
		super(labelString);
	}

	

	
	protected abstract List<ReorderingLabel> getReorderingLabelsListWithoutTopAndPhrase();

	@Override
	public List<ReorderingLabel> getReorderingLabelsConsideredForSmoothing() {
		return getReorderingLabelsListWithoutTopAndPhrase();
	}


	@Override
	public int getHatFineReorderingFeaturesCategoryMappingForLabel() {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public boolean isBinaryReorderingLabel() {
		throw new RuntimeException("Not Implemented yet");
	}

	@Override
	public boolean producesDoubleReorderingLabels() {
		return false;
	}
	
	/*
	protected abstract ParentRelativeReorderingLabel getReorderingLabelLeftBindingInvertedCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelRightBindingInvertedCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelLeftBindingMonotoneCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelRightBindingMonotoneCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyMonotoneCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyInvertedCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyDiscontinuousCase();
	protected abstract ParentRelativeReorderingLabel getReorderingLabelTopNodeCase();
	*/
	

	protected abstract boolean isOneSideMonotoneLabel();
	protected abstract boolean isOneSideInvertedLabel();
	protected abstract boolean isEmbeddedFullyMonotoneLabel();
	protected abstract boolean isEmbeddedFullyInvertedLabel();
	protected abstract boolean isEmbeddedFullyDiscontinuousLabel();
	protected abstract boolean isTopNodeLabel();
	
	
}
