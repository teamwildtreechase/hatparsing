/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;

public class ReorderingLabeler {

	private final EbitgChart theChart;

	private ReorderingLabeler(EbitgChart theChart) {
		this.theChart = theChart;
	}

	public static ReorderingLabeler createReorderingLabeler(EbitgChart theChart) {
		return new ReorderingLabeler(theChart);
	}


	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
		ParentRelativeReorderingLabelComputer reorderingLabelComputer = ParentRelativeReorderingLabelComputer.createReorderingLabelComputer(inference, theChart);
		return reorderingLabelComputer.getReorderingLabel();
	}

	public FineParentRelativeReorderingLabel getReorderingLabelForChartEntry(EbitgChartEntry chartEntry) {
		ParentRelativeReorderingLabelComputer reorderingLabelComputer = ParentRelativeReorderingLabelComputer.createReorderingLabelComputer(chartEntry, theChart);
		return reorderingLabelComputer.getReorderingLabel();
	}

}
