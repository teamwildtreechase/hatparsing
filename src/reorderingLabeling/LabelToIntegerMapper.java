/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

public interface LabelToIntegerMapper {
	public int getHatFineReorderingFeaturesCategoryMappingForLabel();
	public int getCoarseReorderingFeaturesCategoryMappingForLabel();
}
