/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.List;

import extended_bitg.EbitgInference;

public interface ReorderingLabelCreater {

	public List<ReorderingLabel> getReorderingLabelsForPhraseSwitchRules();
	public List<ReorderingLabel> getReorderingLabelsConsideredForSmoothing();
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference);
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(String reorderingLabelContainingString);
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName);
	
	/**
	 *  Returns the number of reordering labels that are used in (binary) reordering features.
	 *  Note that the PhrasePair label is not used in the reordering features, so that the 
	 *  actual number of used labels is one less than the total number of labels including 
	 *  the PhrasePair label.
	 * @return
	 */
	public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures();
	public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures();
	
	/**
	 * Function that returns true when double reordering labels are created by the
	 * ReorderingLabelCreater. Such "double" labels consist of a concatenation 
	 * of the labels from multiple sources, e.g. a phrase-centric reordering label combined
	 * with a  parent-relative reordering label
	 * @return
	 */
	public boolean producesDoubleReorderingLabels();
}
