/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.io.Serializable;
import java.util.List;

public abstract class ReorderingLabel implements ReorderingLabelCreater, LabelToIntegerMapper, Serializable {

	private static final long serialVersionUID = 1L;

	public static String getPhraseReorderingLabel() {
		return PHRASE_PAIR_LABEL_TAG;
	}

	public static final String PHRASE_PAIR_LABEL_TAG = "PhrasePair";

	protected final String labelString;

	protected ReorderingLabel(String labelString) {
		this.labelString = labelString;
	}

	public String getLabel() {
		return this.labelString;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ReorderingLabel) {
			return ((ReorderingLabel) o).getLabel().equals(this.getLabel());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.labelString.hashCode();
	}

	public boolean isPhraseReorderingLabel() {
		return this.labelString.equals(PHRASE_PAIR_LABEL_TAG);
	}

	public String toString() {
		return "ReorderingLabel: " + this.labelString;
	}

	protected abstract List<ReorderingLabel> getAllReorderingLabelsExceptPhrase();
	
	@Override
	public List<ReorderingLabel> getReorderingLabelsForPhraseSwitchRules() {
		return getAllReorderingLabelsExceptPhrase();
	}
	
	public abstract boolean isBinaryReorderingLabel();
}
