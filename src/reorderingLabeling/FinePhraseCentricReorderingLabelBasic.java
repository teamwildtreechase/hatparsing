/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import extended_bitg.EbitgInference;

public class FinePhraseCentricReorderingLabelBasic extends
		FinePhraseCentricReorderingLabel {

	private static final long serialVersionUID = 1L;

	private static final String FINE_PHRASE_CENTRIC_REORDER_LABEL = "finePhraseCentricReorderLabel";

	public static final FinePhraseCentricReorderingLabelBasic PHRASE_PAIR_LABEL = new FinePhraseCentricReorderingLabelBasic(
			PHRASE_PAIR_LABEL_TAG);
	public static final FinePhraseCentricReorderingLabelBasic ATOMIC_LABEL = new FinePhraseCentricReorderingLabelBasic(
			ATOMIC_STRING);
	public static final FinePhraseCentricReorderingLabelBasic MONOTONE_LABEL = new FinePhraseCentricReorderingLabelBasic(
			MONOTONE_STRING);
	public static final FinePhraseCentricReorderingLabelBasic INVERTED_LABEL = new FinePhraseCentricReorderingLabelBasic(
			INVERTED_STRING);
	public static final FinePhraseCentricReorderingLabelBasic PERMUTATION_LABEL = new FinePhraseCentricReorderingLabelBasic(
			PERMUTATION_STRING);
	public static final FinePhraseCentricReorderingLabelBasic HAT_LABEL = new FinePhraseCentricReorderingLabelBasic(
			HAT_STRING);

	private static final List<FinePhraseCentricReorderingLabelBasic> ALL_REORDERING_LABELS = Arrays
			.asList(ATOMIC_LABEL, MONOTONE_LABEL, INVERTED_LABEL,
					PERMUTATION_LABEL, HAT_LABEL);

	@Override
	public List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(ALL_REORDERING_LABELS);
		return result;
	}

	protected FinePhraseCentricReorderingLabelBasic() {
		super(null);
	}

	protected FinePhraseCentricReorderingLabelBasic(String labelString) {
		super(labelString);
	}


	public static String getLabelTypeName() {
		return FINE_PHRASE_CENTRIC_REORDER_LABEL;
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return createFinePhraseCentricReorderingLabelBasic();
	}
	
	public static FinePhraseCentricReorderingLabelBasic createFinePhraseCentricReorderingLabelBasic() {
		return new FinePhraseCentricReorderingLabelBasic();
	}


	@Override
	protected ReorderingLabel getPhrasePairLabel() {
		return PHRASE_PAIR_LABEL;
	}

	@Override
	protected ReorderingLabel getAtomicLabel() {
		return ATOMIC_LABEL;
	}

	@Override
	protected ReorderingLabel getMonotoneLabel() {
		return MONOTONE_LABEL;
	}

	@Override
	protected ReorderingLabel getInvertedLabel() {
		return INVERTED_LABEL;
	}

	@Override
	protected ReorderingLabel getPermutationLabel() {
		return PERMUTATION_LABEL;
	}

	@Override
	protected ReorderingLabel getHatLabelForLabelContainingString(
			String labelContainingString) {
		return HAT_LABEL;
	}

	@Override
	protected ReorderingLabel getHatLabelForLabelHATInference(
			EbitgInference inference) {
		return HAT_LABEL;
	}

	@Override
	public boolean producesDoubleReorderingLabels() {
		return false;
	}
}
