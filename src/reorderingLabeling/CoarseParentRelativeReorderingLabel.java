/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import extended_bitg.EbitgInference;

/**
 * This class implements a a coarse version of the ParentRelativeReordering label.
 * This is done by collapsing the pairs left- and right binding monotone / inverted labels
 * into two new combined labels : ONE_SIDE_INVERTED and ONE_SIDE_MONOTONE.
 * The motivation is to decrease sparseness when working with the Parent Relative Reordering
 * labels. As it is, there are theoretical advantages both to finer and coarser grained labels.
 * What works best in practice is mainly an empirical question though, a reason to experiment 
 * with the coarser variant as well as the fine one.
 * 
 * @author Gideon Maillette de Buy Wenniger
 *
 */
public class CoarseParentRelativeReorderingLabel extends ParentRelativeReorderingLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final String COARSE_PARENT_RELATIVE_REORDER_LABEL = "coarseParentRelativeReorderLabel";
	protected static final String ONE_SIDE_INVERTED_STRING = "O.S.I.";
	protected static final String ONE_SIDE_MONOTONE_STRING = "O.S.M.";

	public static final CoarseParentRelativeReorderingLabel PHRASE_PAIR_LABEL = new CoarseParentRelativeReorderingLabel(
			PHRASE_PAIR_LABEL_TAG);
	public static final CoarseParentRelativeReorderingLabel ONE_SIDE_INVERTED_LABEL = new CoarseParentRelativeReorderingLabel(
			ONE_SIDE_INVERTED_STRING);
	public static final CoarseParentRelativeReorderingLabel ONE_SIDE_MONOTONE_LABEL = new CoarseParentRelativeReorderingLabel(
			ONE_SIDE_MONOTONE_STRING);
	public static final CoarseParentRelativeReorderingLabel TOP_NODE_LABEL = new CoarseParentRelativeReorderingLabel(
			TOP_NODE_STRING);
	public static final CoarseParentRelativeReorderingLabel EMBEDDED_FULLY_DISCONTINUOUS_LABEL = new CoarseParentRelativeReorderingLabel(
			EMBEDDED_FULLY_DISCONTINUOUS_STRING);
	public static final CoarseParentRelativeReorderingLabel EMBEDDED_FULLY_MONOTONE_LABEL = new CoarseParentRelativeReorderingLabel(
			EMBEDDED_FULLY_MONOTONE_STRING);
	public static final CoarseParentRelativeReorderingLabel EMBEDDED_FULLY_INVERTED_LABEL = new CoarseParentRelativeReorderingLabel(
			EMBEDDED_FULLY_INVERTED_STRING);

	private static final List<CoarseParentRelativeReorderingLabel> REORDERING_LABELS_LIST_WITHOUT_PHRASE = Arrays
			.asList(ONE_SIDE_MONOTONE_LABEL, ONE_SIDE_INVERTED_LABEL,
					EMBEDDED_FULLY_DISCONTINUOUS_LABEL, EMBEDDED_FULLY_INVERTED_LABEL,
					EMBEDDED_FULLY_MONOTONE_LABEL, TOP_NODE_LABEL);

	private static final List<CoarseParentRelativeReorderingLabel> REORDERING_LABELS_LIST_WITHOUT_TOP_AND_PHRASE = Collections
			.unmodifiableList(Arrays.asList(ONE_SIDE_MONOTONE_LABEL, ONE_SIDE_INVERTED_LABEL,
					EMBEDDED_FULLY_DISCONTINUOUS_LABEL, EMBEDDED_FULLY_INVERTED_LABEL,
					EMBEDDED_FULLY_MONOTONE_LABEL));

	protected CoarseParentRelativeReorderingLabel(String labelString) {
		super(labelString);
	}

	private CoarseParentRelativeReorderingLabel() {
		super(null);
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return createCoarseParentRelativeReorderingLabel();
	}

	public static CoarseParentRelativeReorderingLabel createCoarseParentRelativeReorderingLabel() {
		return new CoarseParentRelativeReorderingLabel();
	}

	public static String getLabelTypeName() {
		return COARSE_PARENT_RELATIVE_REORDER_LABEL;
	}

	@Override
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
		return createCoarseParentRelativeReorderingLabelByLeftAndRigthSideLabelCollapsing(inference
				.getParentRelativeReorderingLabel());
	}

	private static ReorderingLabel createParentRelativeReorderingLabelForLabelContainingString(
			String labelContainingString) {
		if (labelContainingString.contains(ReorderingLabel.PHRASE_PAIR_LABEL_TAG)) {
			return PHRASE_PAIR_LABEL;
		} else if (labelContainingString.contains(TOP_NODE_STRING)) {
			return TOP_NODE_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_MONOTONE_STRING)) {
			return EMBEDDED_FULLY_MONOTONE_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_INVERTED_STRING)) {
			return EMBEDDED_FULLY_INVERTED_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_DISCONTINUOUS_STRING)) {
			return EMBEDDED_FULLY_DISCONTINUOUS_LABEL;
		} else if (labelContainingString.contains(ONE_SIDE_MONOTONE_STRING)) {
			return ONE_SIDE_MONOTONE_LABEL;
		} else if (labelContainingString.contains(ONE_SIDE_INVERTED_STRING)) {
			return ONE_SIDE_INVERTED_LABEL;
		} else {
			throw new RuntimeException(
					"Error :  createParentRelativeReorderingLabelForLabelContainingString( - String "
							+ labelContainingString + " contains no recognized reordering label");
		}
	}

	@Override
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(
			String reorderingLabelContainingString) {
		return createParentRelativeReorderingLabelForLabelContainingString(reorderingLabelContainingString);
	}

	/**
	 * This method takes as input a FineParentRelativeReorderingLabel and collapses it into 
	 * a CoarseParentRelativeReorderingLabel. To this end the boolean methods 
	 * like  "isOneSideMonotoneLabel(), ... , isEmbeddedFullyInvertedLabel()" are used 
	 * which are defined such that left- and right- binding labels are collapsed into the
	 * coarser superset "OneSidedLabel" i.e. OneSideMonotoneLabel and OneSideInvertedLabel.
	 * The implementation of the boolean methods in FineParentRelativeReorderingLabel
	 * reflects this.
	 * @param parentRelativeReorderingLabel
	 * @return
	 */
	public static CoarseParentRelativeReorderingLabel createCoarseParentRelativeReorderingLabelByLeftAndRigthSideLabelCollapsing(
			FineParentRelativeReorderingLabel parentRelativeReorderingLabel) {
		if (parentRelativeReorderingLabel.isOneSideMonotoneLabel()) {
			return ONE_SIDE_MONOTONE_LABEL;
		} else if (parentRelativeReorderingLabel.isOneSideInvertedLabel()) {
			return ONE_SIDE_INVERTED_LABEL;
		} else if (parentRelativeReorderingLabel.isEmbeddedFullyMonotoneLabel()) {
			return EMBEDDED_FULLY_MONOTONE_LABEL;
		} else if (parentRelativeReorderingLabel.isEmbeddedFullyInvertedLabel()) {
			return EMBEDDED_FULLY_INVERTED_LABEL;
		} else if (parentRelativeReorderingLabel.isEmbeddedFullyDiscontinuousLabel()) {
			return EMBEDDED_FULLY_DISCONTINUOUS_LABEL;
		} else if (parentRelativeReorderingLabel.isTopNodeLabel()) {
			return TOP_NODE_LABEL;
		} else {
			throw new RuntimeException(
					"Error - CoarseParentRelativeReorderingLabel.getCoarseParentRelativeReorderingLabelByLeftAndRigthSideLabelCollapsing( : Unknown label type");
		}
	}

	@Override
	protected List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(REORDERING_LABELS_LIST_WITHOUT_PHRASE);
		return result;
	}

	@Override
	protected List<ReorderingLabel> getReorderingLabelsListWithoutTopAndPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(REORDERING_LABELS_LIST_WITHOUT_TOP_AND_PHRASE);
		return result;
	}

	@Override
	protected boolean isOneSideMonotoneLabel() {
		return this.equals(ONE_SIDE_MONOTONE_LABEL);
	}

	@Override
	protected boolean isOneSideInvertedLabel() {
		return this.equals(ONE_SIDE_INVERTED_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyMonotoneLabel() {
		return this.equals(EMBEDDED_FULLY_MONOTONE_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyInvertedLabel() {
		return this.equals(EMBEDDED_FULLY_INVERTED_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyDiscontinuousLabel() {
		return this.equals(EMBEDDED_FULLY_DISCONTINUOUS_LABEL);
	}

	@Override
	protected boolean isTopNodeLabel() {
		return this.equals(TOP_NODE_LABEL);
	}

}
