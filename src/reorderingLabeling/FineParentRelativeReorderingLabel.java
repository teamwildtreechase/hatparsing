/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import extended_bitg.EbitgInference;

public class FineParentRelativeReorderingLabel extends ParentRelativeReorderingLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final String FINE_PARENT_RELATIVE_REORDER_LABEL = "parentRelativeReorderLabel";
	protected static final String LEFT_BINDING_INVERTED_STRING = "L.B.I.";
	protected static final String RIGHT_BINDING_INVERTED_STRING = "R.B.I.";
	protected static final String LEFT_BINDING_MONOTONE_STRING = "L.B.M.";
	protected static final String RIGHT_BINDING_MONOTONE_STRING = "R.B.M.";
	
	public static final FineParentRelativeReorderingLabel PHRASE_PAIR_LABEL = new FineParentRelativeReorderingLabel(
			PHRASE_PAIR_LABEL_TAG);
	public static final FineParentRelativeReorderingLabel LEFT_BINDING_INVERTED_LABEL = new FineParentRelativeReorderingLabel(
			LEFT_BINDING_INVERTED_STRING);
	public static final FineParentRelativeReorderingLabel RIGHT_BINDING_INVERTED_LABEL = new FineParentRelativeReorderingLabel(
			RIGHT_BINDING_INVERTED_STRING);
	public static final FineParentRelativeReorderingLabel LEFT_BINDING_MONOTONE_LABEL = new FineParentRelativeReorderingLabel(
			LEFT_BINDING_MONOTONE_STRING);
	public static final FineParentRelativeReorderingLabel RIGHT_BINDING_MONOTONE_LABEL = new FineParentRelativeReorderingLabel(
			RIGHT_BINDING_MONOTONE_STRING);
	public static final FineParentRelativeReorderingLabel TOP_NODE_LABEL = new FineParentRelativeReorderingLabel(
			TOP_NODE_STRING);
	public static final FineParentRelativeReorderingLabel EMBEDDED_FULLY_DISCONTINUOUS_LABEL = new FineParentRelativeReorderingLabel(
			EMBEDDED_FULLY_DISCONTINUOUS_STRING);
	public static final FineParentRelativeReorderingLabel EMBEDDED_FULLY_MONOTONE_LABEL = new FineParentRelativeReorderingLabel(
			EMBEDDED_FULLY_MONOTONE_STRING);
	public static final FineParentRelativeReorderingLabel EMBEDDED_FULLY_INVERTED_LABEL = new FineParentRelativeReorderingLabel(
			EMBEDDED_FULLY_INVERTED_STRING);

	private static final List<FineParentRelativeReorderingLabel> REORDERING_LABELS_LIST_WITHOUT_PHRASE = Arrays
			.asList(LEFT_BINDING_INVERTED_LABEL, RIGHT_BINDING_INVERTED_LABEL,
					LEFT_BINDING_MONOTONE_LABEL, RIGHT_BINDING_MONOTONE_LABEL,
					EMBEDDED_FULLY_DISCONTINUOUS_LABEL, EMBEDDED_FULLY_INVERTED_LABEL,
					EMBEDDED_FULLY_MONOTONE_LABEL, TOP_NODE_LABEL);

	private static final List<FineParentRelativeReorderingLabel> REORDERING_LABELS_LIST_WITHOUT_TOP_AND_PHRASE = Collections
			.unmodifiableList(Arrays.asList(LEFT_BINDING_INVERTED_LABEL,
					RIGHT_BINDING_INVERTED_LABEL, LEFT_BINDING_MONOTONE_LABEL,
					RIGHT_BINDING_MONOTONE_LABEL, EMBEDDED_FULLY_DISCONTINUOUS_LABEL,
					EMBEDDED_FULLY_INVERTED_LABEL, EMBEDDED_FULLY_MONOTONE_LABEL));

	private FineParentRelativeReorderingLabel() {
		super(null);
	}

	protected FineParentRelativeReorderingLabel(String labelString) {
		super(labelString);
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return createParentRelativeReorderingLabel();
	}

	public static FineParentRelativeReorderingLabel createParentRelativeReorderingLabel() {
		return new FineParentRelativeReorderingLabel();
	}
	
	public static String getLabelTypeName() {
		return FINE_PARENT_RELATIVE_REORDER_LABEL;
	}

	@Override
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
		return inference.getParentRelativeReorderingLabel();
	}
	
	@Override
	public List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(REORDERING_LABELS_LIST_WITHOUT_PHRASE);
		return result;
	}

	private static ReorderingLabel createParentRelativeReorderingLabelForLabelContainingString(
			String labelContainingString) {
		if (labelContainingString.contains(ReorderingLabel.PHRASE_PAIR_LABEL_TAG)) {
			return PHRASE_PAIR_LABEL;
		} else if (labelContainingString.contains(TOP_NODE_STRING)) {
			return TOP_NODE_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_MONOTONE_STRING)) {
			return EMBEDDED_FULLY_MONOTONE_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_INVERTED_STRING)) {
			return EMBEDDED_FULLY_INVERTED_LABEL;
		} else if (labelContainingString.contains(EMBEDDED_FULLY_DISCONTINUOUS_STRING)) {
			return EMBEDDED_FULLY_DISCONTINUOUS_LABEL;
		} else if (labelContainingString.contains(LEFT_BINDING_MONOTONE_STRING)) {
			return LEFT_BINDING_MONOTONE_LABEL;
		} else if (labelContainingString.contains(LEFT_BINDING_INVERTED_STRING)) {
			return LEFT_BINDING_INVERTED_LABEL;
		} else if (labelContainingString.contains(RIGHT_BINDING_MONOTONE_STRING)) {
			return RIGHT_BINDING_MONOTONE_LABEL;
		} else if (labelContainingString.contains(RIGHT_BINDING_INVERTED_STRING)) {
			return RIGHT_BINDING_INVERTED_LABEL;
		} else {
			throw new RuntimeException(
					"Error :  createParentRelativeReorderingLabelForLabelContainingString( - String "
							+ labelContainingString + " contains no recognized reordering label");
		}
	}

	@Override
	protected List<ReorderingLabel> getReorderingLabelsListWithoutTopAndPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(REORDERING_LABELS_LIST_WITHOUT_TOP_AND_PHRASE);
		return result;
	}

	@Override
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(
			String reorderingLabelContainingString) {
		return createParentRelativeReorderingLabelForLabelContainingString(reorderingLabelContainingString);
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelLeftBindingInvertedCase() {
		return LEFT_BINDING_INVERTED_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelRightBindingInvertedCase() {
		return RIGHT_BINDING_INVERTED_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelLeftBindingMonotoneCase() {
		return LEFT_BINDING_MONOTONE_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelRightBindingMonotoneCase() {
		return RIGHT_BINDING_MONOTONE_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyMonotoneCase() {
		return EMBEDDED_FULLY_MONOTONE_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyInvertedCase() {
		return EMBEDDED_FULLY_INVERTED_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelEmbeddedFullyDiscontinuousCase() {
		return EMBEDDED_FULLY_DISCONTINUOUS_LABEL;
	}

	protected FineParentRelativeReorderingLabel getReorderingLabelTopNodeCase() {
		return TOP_NODE_LABEL;
	}
	
	
	@Override
	protected boolean isOneSideMonotoneLabel() {
		return this.equals(LEFT_BINDING_MONOTONE_LABEL) || this.equals(RIGHT_BINDING_MONOTONE_LABEL) ;
	}

	@Override
	protected boolean isOneSideInvertedLabel() {
		return this.equals(LEFT_BINDING_INVERTED_LABEL) || this.equals(RIGHT_BINDING_INVERTED_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyMonotoneLabel() {
		return this.equals(EMBEDDED_FULLY_MONOTONE_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyInvertedLabel() {
		return this.equals(EMBEDDED_FULLY_INVERTED_LABEL);
	}

	@Override
	protected boolean isEmbeddedFullyDiscontinuousLabel() {
		return this.equals(EMBEDDED_FULLY_DISCONTINUOUS_LABEL);
	}

	@Override
	protected boolean isTopNodeLabel() {
		return this.equals(TOP_NODE_LABEL);
	}


}
