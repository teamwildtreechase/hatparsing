/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import util.Pair;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgChartEntry.EntryType;
import extended_bitg.EbitgInference;

public class ParentRelativeReorderingLabelComputer {

	private final EbitgChartEntry leftExtendedCompleteChartEntry;
	private final EbitgChartEntry rightExtendedCompleteChartEntry;
	private final boolean isTopNode;
	private final FineParentRelativeReorderingLabel parentRelativeReorderingLabel;

	private ParentRelativeReorderingLabelComputer(EbitgChartEntry leftExtendedCompleteChartEntry,
			EbitgChartEntry rightExtendedCompleteChartEntry, boolean isTopNode,
			FineParentRelativeReorderingLabel parentRelativeReorderingLabel) {
		this.leftExtendedCompleteChartEntry = leftExtendedCompleteChartEntry;
		this.rightExtendedCompleteChartEntry = rightExtendedCompleteChartEntry;
		this.isTopNode = isTopNode;
		this.parentRelativeReorderingLabel = parentRelativeReorderingLabel;
	}

	public static boolean isTopNodeChartEntry(EbitgChartEntry containingChartEntry,
			EbitgChart theChart) {
		if ((containingChartEntry.getI() == 0)
				&& (containingChartEntry.getJ() == theChart.getSourceLength() - 1)) {
			return true;
		}
		return false;
	}

	protected static ParentRelativeReorderingLabelComputer createReorderingLabelComputer(
			EbitgInference inference, EbitgChart theChart) {
		return new ParentRelativeReorderingLabelComputer(
				getSmallestCompleteAndNotNullBindingLeftExtendedChartEntry(
						inference.getContainingChartEntry(), theChart),
				getSmallestCompleteAndNotNullBindingRightExtendedChartEntry(
						inference.getContainingChartEntry(), theChart), isTopNodeChartEntry(
						inference.getContainingChartEntry(), theChart),
				FineParentRelativeReorderingLabel.createParentRelativeReorderingLabel());
	}

	public static ParentRelativeReorderingLabelComputer createReorderingLabelComputer(
			EbitgChartEntry chartEntry, EbitgChart theChart) {
		return new ParentRelativeReorderingLabelComputer(
				getSmallestCompleteAndNotNullBindingLeftExtendedChartEntry(chartEntry, theChart),
				getSmallestCompleteAndNotNullBindingRightExtendedChartEntry(chartEntry, theChart),
				isTopNodeChartEntry(chartEntry, theChart),
				FineParentRelativeReorderingLabel.createParentRelativeReorderingLabel());
	}

	private static boolean isSourceNullBinding(EbitgChartEntry chartEntry) {
		return chartEntry.getEntryType().equals(EntryType.SourceNullsBinding);
	}

	private static EbitgChartEntry getSmallestCompleteAndNotNullBindingLeftExtendedChartEntry(
			EbitgChartEntry chartEntry, EbitgChart theChart) {
		Pair<Integer> inferenceChartEntryIndices = new Pair<Integer>(chartEntry.getI(),
				chartEntry.getJ());
		for (int i = inferenceChartEntryIndices.getFirst() - 1; i >= 0; i--) {
			EbitgChartEntry leftExtendedChartEntry = theChart.getChartEntry(i,
					inferenceChartEntryIndices.getSecond());
			if (leftExtendedChartEntry.isComplete() && !isSourceNullBinding(leftExtendedChartEntry)) {
				return leftExtendedChartEntry;
			}
		}
		return null;
	}

	private static EbitgChartEntry getSmallestCompleteAndNotNullBindingRightExtendedChartEntry(
			EbitgChartEntry chartEntry, EbitgChart theChart) {
		Pair<Integer> inferenceChartEntryIndices = new Pair<Integer>(chartEntry.getI(),
				chartEntry.getJ());
		for (int j = inferenceChartEntryIndices.getSecond() + 1; j < theChart.getSourceLength(); j++) {
			EbitgChartEntry rightExtendedChartEntry = theChart.getChartEntry(
					inferenceChartEntryIndices.getFirst(), j);
			if (rightExtendedChartEntry.isComplete()
					&& !isSourceNullBinding(rightExtendedChartEntry)) {
				return rightExtendedChartEntry;
			}
		}
		return null;
	}

	private FineParentRelativeReorderingLabel getReorderingLabelForLeftBoundInference() {
		if (leftExtendedCompleteChartEntry.containsMonontoneBittInferences()) {
			return parentRelativeReorderingLabel.getReorderingLabelLeftBindingMonotoneCase();

		} else if (leftExtendedCompleteChartEntry.getEntryType().equals(EntryType.Complex)) {
			return parentRelativeReorderingLabel.getReorderingLabelEmbeddedFullyDiscontinuousCase();
		} else {
			return parentRelativeReorderingLabel.getReorderingLabelLeftBindingInvertedCase();
		}
	}

	private FineParentRelativeReorderingLabel getReorderingLabelForRightBoundInference() {
		if (rightExtendedCompleteChartEntry.containsMonontoneBittInferences()) {
			return parentRelativeReorderingLabel.getReorderingLabelRightBindingMonotoneCase();

		} else if (rightExtendedCompleteChartEntry.getEntryType().equals(EntryType.Complex)) {
			return parentRelativeReorderingLabel.getReorderingLabelEmbeddedFullyDiscontinuousCase();
		} else {
			return parentRelativeReorderingLabel.getReorderingLabelRightBindingInvertedCase();
		}
	}

	private boolean hasValidLeftBinding() {
		return (leftExtendedCompleteChartEntry != null);
	}

	private boolean hasValidRightBinding() {
		return (rightExtendedCompleteChartEntry != null);
	}

	public FineParentRelativeReorderingLabel getReorderingLabelForBothSidesBindingInference() {
		if (leftExtendedCompleteChartEntry.containsMonontoneBittInferences()
				&& rightExtendedCompleteChartEntry.containsMonontoneBittInferences()) {
			return parentRelativeReorderingLabel.getReorderingLabelEmbeddedFullyMonotoneCase();
		} else if ((!leftExtendedCompleteChartEntry.containsMonontoneBittInferences())
				&& (!rightExtendedCompleteChartEntry.containsMonontoneBittInferences())) {
			return parentRelativeReorderingLabel.getReorderingLabelEmbeddedFullyInvertedCase();
		} else
			System.out
					.println("Error :  a inference that can be bound on both sides should be either fully inverted or fully monotonous");
		throw new RuntimeException(
				"Error :  a inference that can be bound on both sides should be either fully inverted or fully monotonous");
	}

	public FineParentRelativeReorderingLabel getReorderingLabel() {
		if (isTopNode) {
			return parentRelativeReorderingLabel.getReorderingLabelTopNodeCase();
		} else if (hasValidLeftBinding() && hasValidRightBinding()) {
			// System.out.println("leftExtendedCompleteChartEntry:" +
			// leftExtendedCompleteChartEntry);
			// System.out.println("rightExtendedCompleteChartEntry:" +
			// rightExtendedCompleteChartEntry);
			return getReorderingLabelForBothSidesBindingInference();
		} else if (hasValidLeftBinding()) {
			return getReorderingLabelForLeftBoundInference();
		} else if (hasValidRightBinding()) {
			return getReorderingLabelForRightBoundInference();
		} else {
			return parentRelativeReorderingLabel.getReorderingLabelEmbeddedFullyDiscontinuousCase();
		}
	}

}
