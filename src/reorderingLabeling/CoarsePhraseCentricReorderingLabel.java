/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import extended_bitg.EbitgInference;
import extended_bitg.EbitgInference.InferenceType;

public class CoarsePhraseCentricReorderingLabel extends PhraseCentricReorderingLabel {

	private static final long serialVersionUID = 1L;

	private static final String COARSE_PHRASE_CENTRIC_REORDER_LABEL = "coarsePhraseCentricReorderLabel";

	public static final CoarsePhraseCentricReorderingLabel PHRASE_PAIR_LABEL = new CoarsePhraseCentricReorderingLabel(PHRASE_PAIR_LABEL_TAG);
	public static final CoarsePhraseCentricReorderingLabel MONOTONE_LABEL = new CoarsePhraseCentricReorderingLabel(MONOTONE_STRING);
	public static final CoarsePhraseCentricReorderingLabel INVERTED_LABEL = new CoarsePhraseCentricReorderingLabel(INVERTED_STRING);
	public static final CoarsePhraseCentricReorderingLabel HAT_LABEL = new CoarsePhraseCentricReorderingLabel(HAT_STRING);

	private static final List<CoarsePhraseCentricReorderingLabel> ALL_REORDERING_LABELS = Arrays.asList(MONOTONE_LABEL, INVERTED_LABEL, HAT_LABEL);

	protected CoarsePhraseCentricReorderingLabel() {
		super(null);
	}

	protected CoarsePhraseCentricReorderingLabel(String labelString) {
		super(labelString);
	}

	@Override
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {
		
		// The inference is considered 'atomic' since it can not be further decomposed, and is labeled monotone
		if(inference.isMinimalPhrasePairInference())
		{
			return MONOTONE_LABEL;
		}
		
		if (inference.getInferenceType().equals(InferenceType.BITT)) {
			if (inference.isInverted()) {
				return INVERTED_LABEL;
			} else {
				return MONOTONE_LABEL;
			}
		} else if (inference.getInferenceType().equals(InferenceType.PET) || inference.getInferenceType().equals(InferenceType.HAT)) {
			return HAT_LABEL;
		} else if (inference.getInferenceType().equals(InferenceType.Atomic)) {
			return MONOTONE_LABEL;
		} else {
			throw new RuntimeException("Error : The inference is of the wrong type to get a reordering label - observed type: " + inference.getInferenceType());
		}
	}

	private static ReorderingLabel createCoarsePhraseCentricReorderingLabelForLabelContainingString(String labelContainingString) {
		if (labelContainingString.contains(ReorderingLabel.PHRASE_PAIR_LABEL_TAG)) {
			return PHRASE_PAIR_LABEL;
		} else if (labelContainingString.contains(MONOTONE_STRING)) {
			return MONOTONE_LABEL;
		} else if (labelContainingString.contains(INVERTED_STRING)) {
			return INVERTED_LABEL;
		} else if (labelContainingString.contains(HAT_STRING)) {
			return HAT_LABEL;
		} else {
			throw new RuntimeException("Error :  createParentRelativeReorderingLabelForLabelContainingString( - String " + labelContainingString
					+ " contains no recognized reordering label");
		}
	}

	public static String getLabelTypeName() {
		return COARSE_PHRASE_CENTRIC_REORDER_LABEL;
	}

	@Override
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(String reorderingLabelContainingString) {
		return createCoarsePhraseCentricReorderingLabelForLabelContainingString(reorderingLabelContainingString);
	}

	public static ReorderingLabelCreater createReorderingLabelCreater() {
		return new CoarsePhraseCentricReorderingLabel();
	}

	@Override
	protected List<ReorderingLabel> getAllReorderingLabelsExceptPhrase() {
		List<ReorderingLabel> result = new ArrayList<ReorderingLabel>();
		result.addAll(ALL_REORDERING_LABELS);
		return result;
	}

	@Override
	public int getHatFineReorderingFeaturesCategoryMappingForLabel() {

		if (this.equals(MONOTONE_LABEL)) {
			return 0;
		} else if (this.equals(INVERTED_LABEL)) {
			return 1;
		} else if (this.equals(HAT_LABEL)) {
			return 2;
		} else {
			return -1;
		}
	}
	
	@Override
	public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
		if (this.equals(MONOTONE_LABEL)) {
			return 0;
		} else if (this.equals(INVERTED_LABEL) || this.equals(HAT_LABEL)) {
			return 1;
		} else {
			return -1;
		}
	}
	@Override
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
		if (labelName.equals(MONOTONE_STRING)) {
			return MONOTONE_LABEL;
		} else if (labelName.equals(INVERTED_STRING)) {
			return INVERTED_LABEL;
		} else if (labelName.equals(HAT_STRING)) {
			return HAT_LABEL;
		} else if (labelName.equals(PHRASE_PAIR_LABEL_TAG)) {
			return PHRASE_PAIR_LABEL;
		} else {
			throw new RuntimeException("Error: unrecognized label name");
		}
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
		return 3;
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
		return 2;
	}

	@Override
	public boolean isBinaryReorderingLabel() {
		return (this.equals(MONOTONE_LABEL) || this.equals(INVERTED_LABEL));
	}

	@Override
	public boolean producesDoubleReorderingLabels() {
		return false;
	}

}
