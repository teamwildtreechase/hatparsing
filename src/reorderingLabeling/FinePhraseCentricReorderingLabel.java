/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package reorderingLabeling;

import extended_bitg.EbitgInference;
import extended_bitg.EbitgInference.InferenceType;

public abstract class FinePhraseCentricReorderingLabel extends PhraseCentricReorderingLabel {

	private static final long serialVersionUID = 1L;

	protected FinePhraseCentricReorderingLabel(String labelString) {
		super(labelString);
	}

	@Override
	public ReorderingLabel getReorderingLabelForReorderingLabelContainingString(String reorderingLabelContainingString) {
		return createPhraseCentricReorderingLabelForLabelContainingString(reorderingLabelContainingString);
	}

	protected abstract ReorderingLabel getPhrasePairLabel();

	protected abstract ReorderingLabel getAtomicLabel();

	protected abstract ReorderingLabel getMonotoneLabel();

	protected abstract ReorderingLabel getInvertedLabel();

	protected abstract ReorderingLabel getPermutationLabel();

	protected abstract ReorderingLabel getHatLabelForLabelContainingString(String labelContainingString);

	protected abstract ReorderingLabel getHatLabelForLabelHATInference(EbitgInference inference);

	private ReorderingLabel createPhraseCentricReorderingLabelForLabelContainingString(String labelContainingString) {
		if (labelContainingString.contains(ReorderingLabel.PHRASE_PAIR_LABEL_TAG)) {
			return getPhrasePairLabel();
		} else if (labelContainingString.contains(ATOMIC_STRING)) {
			return getAtomicLabel();
		} else if (labelContainingString.contains(MONOTONE_STRING)) {
			return getMonotoneLabel();
		} else if (labelContainingString.contains(INVERTED_STRING)) {
			return getInvertedLabel();
		} else if (labelContainingString.contains(PERMUTATION_STRING)) {
			return getPermutationLabel();
		} else if (labelContainingString.contains(HAT_STRING)) {
			return getHatLabelForLabelContainingString(labelContainingString);
		} else {
			throw new RuntimeException("Error :  createParentRelativeReorderingLabelForLabelContainingString( - String " + labelContainingString + " contains no recognized reordering label");
		}
	}

	@Override
	public ReorderingLabel createReorderingLabelFromLabelName(String labelName) {
		if (labelName.equals(ATOMIC_STRING)) {
			return getAtomicLabel();
		} else if (labelName.equals(MONOTONE_STRING)) {
			return getMonotoneLabel();
		} else if (labelName.equals(INVERTED_STRING)) {
			return getInvertedLabel();
		} else if (labelName.equals(PERMUTATION_STRING)) {
			return getPermutationLabel();
		} else if (labelName.contains(HAT_STRING)) {
			return getHatLabelForLabelContainingString(labelName);
		} else if (labelName.equals(PHRASE_PAIR_LABEL_TAG)) {
			return getPhrasePairLabel();
		} else {
			throw new RuntimeException("Error: unrecognized label name");
		}
	}

	@Override
	public ReorderingLabel getReorderingLabelForInference(EbitgInference inference) {

		// The inference is considered 'atomic' since it can not be further decomposed, and is labeled as such
		if (inference.isMinimalPhrasePairInference()) {
			return getAtomicLabel();
		}

		if (inference.getInferenceType().equals(InferenceType.BITT)) {
			if (inference.isInverted()) {
				return getInvertedLabel();
			} else {
				return getMonotoneLabel();
			}
		} else if (inference.getInferenceType().equals(InferenceType.PET)) {
			return getPermutationLabel();
		} else if (inference.getInferenceType().equals(InferenceType.HAT)) {
			return getHatLabelForLabelHATInference(inference);
		} else if (inference.getInferenceType().equals(InferenceType.Atomic)) {
			return getAtomicLabel();
		} else if (inference.getInferenceType().equals(InferenceType.SourceNullsBinding)) {
			return getAtomicLabel();
		} else {
			System.out.println(" inference.getInferenceType(): " + inference.getInferenceType());
			throw new RuntimeException("Error : The inference is of the wrong type to get a reordering label - observed type: " + inference.getInferenceType());
		}
	}

	@Override
	public int getHatFineReorderingFeaturesCategoryMappingForLabel() {
		if (this.equals(getAtomicLabel())) {
			return 0;
		} else if (this.equals(getMonotoneLabel())) {
			return 1;
		} else if (this.equals(getInvertedLabel())) {
			return 2;
		} else if (this.equals(getPermutationLabel())) {
			return 3;
		} else if (this.isHatLabel()) {
			return 4;
		} else {
			return -1;
		}
	}

	@Override
	public int getCoarseReorderingFeaturesCategoryMappingForLabel() {
		if (this.equals(getAtomicLabel()) || this.equals(getMonotoneLabel())) {
			return 0;
		} else if (this.equals(getInvertedLabel()) || this.equals(getPermutationLabel()) || this.isHatLabel()) {
			return 1;
		} else {
			return -1;
		}
	}

	public boolean isHatLabel() {
		return this.labelString.contains(HAT_STRING);
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForFineHatReorderingFeatures() {
		return 5;
	}

	@Override
	public int getNumberOfReorderingLabelsUsedForCoarseReorderingFeatures() {
		return 2;
	}

	@Override
	public boolean isBinaryReorderingLabel() {
		return (this.equals(getMonotoneLabel()) || this.equals(getInvertedLabel()));
	}

}
