/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;
import java.io.BufferedReader;
//import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedWriter;
//import java.io.FileNotFoundException;
import java.io.*; 


public class ParseToPennFormatConverter 
{
	private BufferedReader parseFileReader;
	private BufferedWriter outputWriter;
	private int maxSentences;
	

	public ParseToPennFormatConverter(String parseFileName,String pennOutputFileName, int maxSentences)
	{
		this.maxSentences = maxSentences;
		
		try
		{
			parseFileReader = new BufferedReader(new FileReader(parseFileName));
            outputWriter = new BufferedWriter(new FileWriter( pennOutputFileName));
		}
		catch(Exception e)
		{
			System.out.println("\nSomething went wrong" + e);
		}			
		
	}

	public void convertToPennFormat()
	{

		try
		{
			String line = parseFileReader.readLine();
			boolean outputWritingOn = false;
									
			int sentenceNo  = 0;
			while((line != null) && ((sentenceNo < maxSentences) || maxSentences == -1))
			{
				if(outputWritingOn)
				{
					// reached the end of this parsetree
					if(line.trim().equals("</tree>"))
					{
						outputWritingOn = false;
						// write a dot to end the current tree
						outputWriter.write(".");
						
						// increment the number of sentences porcessed
						sentenceNo++;
						
					}
					else
					{
						outputWriter.write("\n" + line);
					}	
					
				}
				else
				{
					// detected the tag indicating the start of the tree
					if(line.trim().equals("<tree style=\"penn\">"))
					{
						// set boolean for writing to true
						outputWritingOn = true;
						
					}
				}	
				
				line = parseFileReader.readLine();
			}
			
			outputWriter.close();
			
		}
		catch(Exception e)
		{
			
		}
	
		
	}
	


	public static void main(String [] args)
	{
		String parseFileName, pennOutputFileName;
		
		
		if((args.length >=  2) && (args.length <=  3))
		{
			System.out.println("args[0]: " + args[0] + "  args[1]: " + args[1]);
			parseFileName = args[0];
			pennOutputFileName = args[1];
			
			int maxSentences = -1; // no max unless specified otherwise in program argument no. 3
			
			if(args.length == 3)
			{
				maxSentences = Integer.parseInt(args[2]);
			}
			
			ParseToPennFormatConverter ptp = new ParseToPennFormatConverter (parseFileName,pennOutputFileName,maxSentences);
			ptp.convertToPennFormat();
		}
		else
		{
			System.out.println("Wrong usage usage.\nUsage: " + "java -jar convertPareseToPenn.jar ParseFileName OutputOPennFileName NUMTREES ");
			System.out.println("The last argument NUMTREES is optional, and can be used to extract a smaller treebank," +
							    "processing only a portion of the trees in  ParseFileName");
		}	
	}

}