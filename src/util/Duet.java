/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

public class Duet <T,S> {
	
	private T firstElement;
	private S secondElement;
	
	public Duet(T first, S second) {
		firstElement = first;
		secondElement = second;
	}
	
	public T getFirst() {
		return firstElement;
	}
	
	public S getSecond() {
		return secondElement;
	}
	
	/*public boolean equals(Object anObject) {
		if (this == anObject) return true;
		if (!(anObject instanceof Duet)) return false;
		Duet<T,S> anotherDuet = (Duet<T,S>)anObject;
		return (this.firstElement.equals(anotherDuet.firstElement));
	}*/
}
