/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

public class Pair2 <T1,T2> 
{
	
	private T1 firstElement;
	private T2 secondElement;
	
	public Pair2(T1 first, T2 second) {
		firstElement = first;
		secondElement = second;
	}
	
	public T1 getFirst() {
		return firstElement;
	}
	
	public T2 getSecond() {
		return secondElement;
	}
	
	public String toString() {
		return "(" + firstElement.toString() + ", " + 
		secondElement.toString() + ")";
	}
}
