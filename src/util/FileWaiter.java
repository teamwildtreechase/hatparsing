/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.File;

public class FileWaiter {

	private static final int TWO_MINUTES = 1000 * 60 * 2;
	private static final int ONE_HOUR = 1000 * 60 * 60;

	public static void main(String[] args) {

		Waiter waiter = new Waiter();
		File file = null;

		while (waiter.waitUntil(fileExists(file))) {

			waiter.backoffExponentiallyFor(TWO_MINUTES);
		}

	}

	public static void waitUntilFileExists(String filePath) {
		Waiter waiter = new Waiter();
		File file = new File(filePath);

		while (waiter.waitUntil(fileExists(file))) {

			waiter.backoffExponentiallyFor(ONE_HOUR);

		}
	}

	public static void waitWhileFileDoesNotExistsSimple(String filePath) {
		File file = new File(filePath);

		int waitTime = 2;

		try {
			while (!(file.exists()) && (waitTime < ONE_HOUR)) {
				Thread.sleep(waitTime);

				waitTime *= 2;
			}

			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static Until fileExists(final File file) {
		return new Until() {

			@Override
			public boolean ready() {
				return file.exists();
			}
		};
	}

	private static class Waiter {

		int wait = 1;
		int soFar = 0;
		boolean stillWaiting = true;

		public Waiter() {
		}

		void backoffExponentiallyFor(final int limit) {
			if (soFar >= limit) {
				stillWaiting = false;
				return;
			}

			if (soFar + wait > limit) {
				wait = limit - soFar - 1;
			}

			soFar += wait;
			sleep(wait);
			wait *= 2;

		}

		private void sleep(int time) {
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				throw new RuntimeException("recieved interupt");
			}
		}

		boolean waitUntil(Until until) {
			if (stillWaiting) {
				return !until.ready();
			}
			throw new RuntimeException("waited until limit");
		}

	}

	private interface Until {

		boolean ready();

	}

}
