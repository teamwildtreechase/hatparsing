/*
Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

public class LicenseHeaderAdaptor {

	private static final String COPYRIGHT_PATTERN_STRING = "Copyright\\s\\(c\\)\\s<2010>";
	private static final String LICENSE_HEADER_PATTERN_STRING = "\\s*/\\*.*?"
			+ COPYRIGHT_PATTERN_STRING + ".*?\\*/\\s"; // \\s*/\\*.*\\*/\\s";//"\\s*/\\*.*Copyright.*\\*/";
	private static final String LICENSE_HEADER_PATTERN_STRING_WITH_EXTRA_FOLLOWING_TEXT = "\\s*/\\*.*?"
			+ COPYRIGHT_PATTERN_STRING + ".*?\\*/.*?"; // \\s*/\\*.*\\*/\\s";//"\\s*/\\*.*Copyright.*\\*/";
	private static final Pattern LICENSE_HEADER_PATTERN = Pattern.compile(
			LICENSE_HEADER_PATTERN_STRING, Pattern.DOTALL);
	private static final Pattern LICENSE_HEADER_PATTERN_WITH_EXTRA_FOLLOWING_TEXT = Pattern
			.compile(LICENSE_HEADER_PATTERN_STRING_WITH_EXTRA_FOLLOWING_TEXT,
					Pattern.DOTALL);

	private final String fileToEditPath;
	private final String licenseFilePath;
	private final BufferedWriter resultFileWriter;

	private LicenseHeaderAdaptor(String fileToEditReader,
			String licenseFileReader, BufferedWriter resultFileWriter) {
		this.fileToEditPath = fileToEditReader;
		this.licenseFilePath = licenseFileReader;
		this.resultFileWriter = resultFileWriter;
	}

	private static String getTempFilePath(String filePath) {
		return filePath + ".temp";
	}

	public static LicenseHeaderAdaptor createLicenseHeaderAdaptor(
			String fileToEditPath, String licenseFilePath) {

		try {
			BufferedWriter resultFileWriter = new BufferedWriter(
					new FileWriter(getTempFilePath(fileToEditPath)));
			return new LicenseHeaderAdaptor(fileToEditPath, licenseFilePath,
					resultFileWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static void replaceLicenseHeader(String fileToEditPath,
			String licenseFilePath, boolean replaceLicenseHeaderIfNotPresent,
			boolean addLicenseHeaderIfPresent) {
		LicenseHeaderAdaptor licenseHeaderAdaptor = createLicenseHeaderAdaptor(
				fileToEditPath, licenseFilePath);
		licenseHeaderAdaptor.adaptLicenseHeader(
				replaceLicenseHeaderIfNotPresent, addLicenseHeaderIfPresent);
		FileUtil.renameFile(getTempFilePath(fileToEditPath), fileToEditPath);
	}

	public static void replaceLicenseHeaderForFilesWithExtensionInFolderAndSubfolders(
			String folderPath, String licenseFilePath, String extension,
			boolean replaceLicenseHeaderIfNotPresent,
			boolean addLicenseHeaderIfPresent) {

		if (!(replaceLicenseHeaderIfNotPresent || addLicenseHeaderIfPresent)) {
			System.out
					.println("Flags set to not replace or add anything anywhere : Quitting.");
			System.exit(0);
		}

		List<File> filePaths = FileUtil.getAllFilesInFolder(
				new File(folderPath), true);

		for (File file : filePaths) {
			if (file.getAbsolutePath().endsWith(extension)) {
				replaceLicenseHeader(file.getAbsolutePath(), licenseFilePath,
						replaceLicenseHeaderIfNotPresent,
						addLicenseHeaderIfPresent);
			}
		}
	}

	private boolean stringContainsCopyrightMentioning(String string) {
		return (string.toLowerCase()).contains("copyright");
	}
	
	private void adaptLicenseHeader(boolean replaceLicenseHeaderIfNotPresent,
			boolean addLicenseHeaderIfPresent) {
		String inputFileAsString = FileUtil.getFileAsString(fileToEditPath);
		String licenseFileAsString = FileUtil.getFileAsString(licenseFilePath);
		// Assert.assertTrue(stringContainsLicensePattern("/*Copyright (c) <2010>*/\n"));
		// Assert.assertTrue(stringContainsLicensePattern("/* Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>\n\nPermission is hereby granted, free of charge, to any person obtaining a copy)\n   */\n"));
		// Assert.assertTrue(stringContainsLicensePattern(inputFileAsString));

		String result = inputFileAsString;
		if (stringContainsLicensePattern(inputFileAsString)) {
			if (replaceLicenseHeaderIfNotPresent) {
				System.out.println("Replacing License header in file: "
						+ fileToEditPath);

				result = replaceMultiLinePattern(inputFileAsString,
						licenseFileAsString);
			}
		} else if (addLicenseHeaderIfPresent) {
			if (stringContainsCopyrightMentioning(inputFileAsString)) {
				System.out
						.println("File "
								+ fileToEditPath
								+ " already contains some copyright mention (but not in format appropriate for replacement) and will be skipped");
			} else {
				System.out.println("Adding new license header to file: "
						+ fileToEditPath);
				result = licenseFileAsString + "\n" + result;
			}
		}
		// System.out.println("Result file String: \n" + result);
		try {
			this.resultFileWriter.write(result);
			this.resultFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean stringContainsLicensePattern(String string) {
		boolean result = LICENSE_HEADER_PATTERN_WITH_EXTRA_FOLLOWING_TEXT
				.matcher(string).matches();
		return result;
	}

	private String replaceMultiLinePattern(String inputString,
			String replacementString) {
		// http://stackoverflow.com/questions/4154239/java-regex-replaceall-multiline

		String result = LICENSE_HEADER_PATTERN.matcher(inputString).replaceAll(
				replacementString);
		return result;
	}

	public static void main(String[] args) {
		if (args.length != 5) {
			// System.out.println("Usage: licenseHeaderAdaptor : INPUT_FILE_PATH LICENSE_FILE_PATH");
			System.out
					.println("Usage: java licenseHeaderAdaptor INPUT_FOLDER_PATH LICENSE_FILE_PATH FILE_EXTENSION_STRING RELPLACE_LICENSE_HEADER_IF_PRESENT ADD_LICENSE_HEADER_IF_NOT_PRESENT");
			System.out
					.println("e.g.: java licenseHeaderAdaptor /src/ /home/license.txt .java true true");
			System.exit(1);
		}

		boolean replaceLicenseHeaderIfNotPresent = Boolean
				.parseBoolean(args[3]);
		boolean addLicenseHeaderIfPresent = Boolean.parseBoolean(args[4]);

		// replaceLicenseHeader(args[0], args[1]);
		replaceLicenseHeaderForFilesWithExtensionInFolderAndSubfolders(args[0],
				args[1], args[2], replaceLicenseHeaderIfNotPresent,
				addLicenseHeaderIfPresent);
	}

}
