/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

import tsg.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class Utility {

	public static NAirs Nair = new NAirs(10, false);
	public static NAirs NairAdj = new NAirs(10, true);
	public static Binomials Bino = new Binomials(10, false);
	public static Binomials BinoAdj = new Binomials(10, true);

	/**
	 * Returns a String with n tabs
	 */
	public static String fillTab(int n) {
		return fillChar(n, '\t');
	}

	/**
	 * Returns a String of length n finishing with the input String s and starting with spaces
	 */
	public static String fsb(int n, String s) {
		return fillChar(n - s.length(), ' ') + s;
	}

	/**
	 * @param n
	 *            totalLength
	 * @param i
	 *            number
	 * @return A String with the number i padded by zeros so the total length of the String is n
	 */
	public static String padZero(int n, int i) {
		String s = Integer.toString(i);
		return fillChar(n - s.length(), '0') + s;
	}

	/**
	 * Returns a String of length n whose middle part is the input String s, surrounded by a repetition of the input char.
	 */
	public static String fca(int n, String s, char c) {
		int repBeginning = (n - s.length()) / 2;
		int repEnd = n - s.length() - repBeginning;
		return fillChar(repBeginning, c) + s + fillChar(repEnd, c);
	}

	/**
	 * Returns a String with n times c
	 */
	public static String fillChar(int n, char c) {
		String result = "";
		for (int i = 0; i < n; i++)
			result += c;
		return result;
	}

	public static <T> boolean increaseInTableInteger(Hashtable<T, Integer> table, T key, int toAdd) {
		Integer count = table.get(key);
		boolean newKey = (count == null);
		count = (newKey) ? toAdd : count + toAdd;
		table.put(key, count);
		return newKey;
	}

	public static <T> void increaseInTableInt(Hashtable<T, int[]> table, T key) {
		int[] count = table.get(key);
		if (count == null) {
			count = new int[] { 1 };
			table.put(key, count);
		} else
			count[0]++;
	}

	public static <T> void increaseInTableInt(Hashtable<T, int[]> table, T key, int toAdd) {
		int[] count = table.get(key);
		if (count == null) {
			count = new int[] { toAdd };
			table.put(key, count);
		} else
			count[0] += toAdd;
	}

	public static <T> void increaseInTableDoubleArray(Hashtable<T, double[]> table, T key, double toAdd) {
		double[] count = table.get(key);
		if (count == null) {
			count = new double[] { toAdd };
			table.put(key, count);
		} else
			count[0] += toAdd;
	}

	public static <T> void increaseInTableDoubleLogArray(Hashtable<T, double[]> table, T key, double toAdd) {
		double[] count = table.get(key);
		if (count == null) {
			count = new double[] { toAdd };
			table.put(key, count);
		} else
			count[0] = logSum(count[0], toAdd);
	}

	public static <T> void increaseInTableBigDecimalArray(Hashtable<T, BigDecimal[]> table, T key, BigDecimal toAdd) {
		BigDecimal[] count = table.get(key);
		if (count == null) {
			count = new BigDecimal[] { toAdd };
			table.put(key, count);
		} else
			count[0] = count[0].add(toAdd);
	}

	public static <T> boolean increaseInTableDouble(Hashtable<T, Double> table, T key, double toAdd) {
		Double count = table.get(key);
		boolean newKey = (count == null);
		count = (newKey) ? toAdd : count + toAdd;
		table.put(key, count);
		return newKey;
	}

	public static <T> boolean increaseInTableLong(Hashtable<T, Long> table, T key, long toAdd) {
		Long count = table.get(key);
		boolean newKey = (count == null);
		count = (newKey) ? toAdd : count + toAdd;
		table.put(key, count);
		return newKey;
	}

	/**
	 * Remove in the table all the keys occurring <= limit times
	 * 
	 * @param <T>
	 *            the type
	 * @param table
	 *            The table of key-freq pairs
	 * @param limit
	 *            The minimum number of occurrences required for items not to be removed
	 */
	public static <T> void removeInTable(Hashtable<T, Integer> table, int limit) {
		for (Iterator<Integer> i = table.values().iterator(); i.hasNext();) {
			Integer freq = i.next();
			if (freq <= limit)
				i.remove();
		}
	}

	/**
	 * 
	 * @param <T>
	 * @param <S>
	 * @param table
	 */
	public static <T, S> void printHashTable(Hashtable<T, S> table) {
		for (T key : table.keySet()) {
			System.out.println(key + "\t" + table.get(key));
		}
	}

	/**
	 * Given an Hashtable of type String-->Integer, the method adds a constant toAdd to the specific key in input.
	 */
	public static boolean increaseStringInteger(Hashtable<String, Integer> table, String key, int toAdd) {
		Integer count = table.get(key);
		boolean newKey = (count == null);
		if (newKey)
			count = toAdd;
		else
			count = count + toAdd;
		table.put(key, count);
		return newKey;
	}

	public static int countTotalTypesInTable(Hashtable<? extends Object, Integer> table) {
		return table.keySet().size();
	}

	public static int countTotalTokensInTable(Hashtable<? extends Object, Integer> table) {
		int totalTokens = 0;
		for (Object key : table.keySet()) {
			totalTokens += table.get(key);
		}
		return totalTokens;
	}

	public static IdentityHashMap<Integer, String> reverseStringIntegerTable(Hashtable<String, Integer> table) {
		IdentityHashMap<Integer, String> result = new IdentityHashMap<Integer, String>();
		for (Map.Entry<String, Integer> tuple : table.entrySet()) {
			result.put(new Integer(tuple.getValue()), tuple.getKey());
		}
		return result;
	}

	/**
	 * Given an Hashtable of type String-->int[], the method adds (toAdd) to the specific index of the value of the corresponding key in input.
	 */
	public static void increaseStringIntArray(Hashtable<String, int[]> table, int arraySize, String key, int index, int toAdd) {
		int[] stat = table.get(key);
		if (stat == null) {
			stat = new int[arraySize];
			table.put(key, stat);
		}
		stat[index]++;
	}

	public static void increaseStringDoubleArray(Hashtable<String, double[]> table, String key, double toAdd) {
		double[] stat = table.get(key);
		if (stat == null) {
			stat = new double[] { toAdd };
			table.put(key, stat);
			return;
		}
		stat[0] += toAdd;
	}

	public static String getMaxKey(Hashtable<String, double[]> table) {
		double max = -Double.MAX_VALUE;
		String maxKey = null;
		for (Entry<String, double[]> e : table.entrySet()) {
			double stat = e.getValue()[0];
			if (stat > max) {
				max = stat;
				maxKey = e.getKey();
			}
		}
		return maxKey;
	}

	public static void increaseStringIntArray(Hashtable<String, int[]> table, String key) {
		int[] freq = table.get(key);
		if (freq == null) {
			freq = new int[] { 1 };
			table.put(key, freq);
		} else
			freq[0]++;
	}

	/**
	 * Given an Hashtable of type String-->Long, the method adds a constant toAdd to the specific key in input.
	 */
	public static boolean increaseStringLong(Hashtable<String, Long> table, String key, long toAdd) {
		Long count = table.get(key);
		boolean newKey = (count == null);
		if (newKey)
			count = new Long(toAdd);
		else
			count = new Long(count.longValue() + toAdd);
		table.put(key, count);
		return newKey;
	}

	/**
	 * Given an Hashtable of type String-->Double, the method adds a constant toAdd to the specific key in input.
	 */
	public static boolean increaseStringDouble(Hashtable<String, Double> table, String key, double weight) {
		Double count = table.get(key);
		boolean newKey = (count == null);
		if (newKey)
			count = new Double(weight);
		else
			count = new Double(count.doubleValue() + weight);
		table.put(key, count);
		return newKey;
	}

	/**
	 * Given an Hashtable of type Object-->Integer, the method adds a constant toAdd to the specific key in input.
	 */
	public static boolean increaseIntegerListInteger(Hashtable<ArrayList<Integer>, Integer> table, ArrayList<Integer> key, int toAdd) {
		Integer count = table.get(key);
		boolean newKey = (count == null);
		if (newKey)
			count = new Integer(toAdd);
		else
			count = new Integer(count + toAdd);
		table.put(key, count);
		return newKey;
	}

	/**
	 * Given an Hashtable of type String-->Integer, the method subtracts a constant toAdd to the specific key in input. If the new counter is 0 or negative, the
	 * key will be removed from the hashtable.
	 */
	public static void decreaseStringInteger(Hashtable<String, Integer> table, String key, int weight) {
		int count = table.get(key);
		count = count - weight;
		if (count <= 0)
			table.remove(key);
		else
			table.put(key, count);
	}

	/**
	 * Given an Hashtable of type String-->Long, the method subtracts a constant toAdd to the specific key in input. If the new counter is 0 or negative, the
	 * key will be removed from the hashtable.
	 */
	public static void decreaseStringLong(Hashtable<String, Long> table, String key, long weight) {
		long count = table.get(key);
		count = count - weight;
		if (count <= 0)
			table.remove(key);
		else
			table.put(key, count);
	}

	/**
	 * Given an Hashtable of type String-->Double, the method subtracts a constant toAdd to the specific key in input. If the new counter is 0 or negative, the
	 * key will be removed from the hashtable.
	 */
	public static void decreaseStringDouble(Hashtable<String, Double> table, String key, double weight) {
		double count = table.get(key);
		count = count - weight;
		if (count <= 0)
			table.remove(key);
		else
			table.put(key, new Double(count));
	}

	/**
	 * Add the input TreeNode TN in the IdentityHashMap markTable with the corresponding mark
	 * 
	 * @param markTable
	 * @param TN
	 * @param mark
	 */
	public static void putTreeNodeMark(IdentityHashMap<TSNode, TreeSet<Integer>> markTable, TSNode TN, Integer mark) {
		TreeSet<Integer> markRecord = markTable.get(TN);
		if (markRecord == null) {
			markRecord = new TreeSet<Integer>();
			markTable.put(TN, markRecord);
		}
		markRecord.add(mark);
	}

	/**
	 * Given two Hashtable of type String-->Double, the method copies all the key-value pairs (where value is not null) from the originTable to the
	 * destinationTable
	 */
	public static void putStringDoubleFromToTable(String key[], Hashtable<String, Double> originTable, Hashtable<String, Double> destinationTable) {
		for (int i = 0; i < key.length; i++) {
			String tree = key[i];
			Double count = originTable.get(tree);
			if (count == null)
				continue;
			destinationTable.put(tree, count);
		}
	}

	/**
	 * Given two Hashtable of type String-->Integer, the method copies all the key-value pairs (where value is not null) from the originTable to the
	 * destinationTable
	 */
	public static void putStringIntegerFromToTable(String key[], Hashtable<String, Integer> originTable, Hashtable<String, Integer> destinationTable) {
		for (int i = 0; i < key.length; i++) {
			String tree = key[i];
			Integer count = originTable.get(tree);
			if (count == null)
				continue;
			destinationTable.put(tree, count);
		}
	}

	public static void arrayIntPlus(int[] source, int[] destination) {
		for (int i = 0; i < destination.length; i++)
			destination[i] += source[i];
	}

	public static int[] arrayListIntegerToArray(List<Integer> list) {
		int[] result = new int[list.size()];
		for (int i = 0; i < list.size(); i++) {
			result[i] = list.get(i);
		}
		return result;
	}

	public static void addToAll(int[] array, int toAdd) {
		for (int i = 0; i < array.length; i++) {
			array[i] += toAdd;
		}
	}

	/**
	 * Given two Hashtable of type String-->Double, the method adds the values of all the key-value pairs (where value is not null) from the originTable to the
	 * destinationTable
	 */
	public static void increaseAllStringDoubleFromToTable(Hashtable<String, Double> originTable, Hashtable<String, Double> destinationTable) {
		for (Enumeration<String> e = originTable.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Double toAdd = originTable.get(key);
			if (toAdd == null)
				continue;
			Double originalCount = destinationTable.get(key);
			if (originalCount == null)
				originalCount = toAdd;
			else
				originalCount = new Double(originalCount.doubleValue() + toAdd.doubleValue());
			destinationTable.put(key, originalCount);
		}
	}

	/**
	 * Given two Hashtable of type String-->Double, the method decreses the values of all the key-value pairs (where value is not null) from the originTable to
	 * the destinationTable
	 */
	public static void decreaseAllStringDoubleFromToTable(Hashtable<String, Double> originTable, Hashtable<String, Double> destinationTable) {
		for (Enumeration<String> e = originTable.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Double toAdd = originTable.get(key);
			Double originalCount = destinationTable.get(key);
			originalCount = new Double(originalCount.doubleValue() - toAdd.doubleValue());
			if (originalCount.doubleValue() == 0)
				destinationTable.remove(key);
			else
				destinationTable.put(key, originalCount);
		}
	}

	/**
	 * Given an Hashtable origin of type String-->Double, the method returns an Hashtable of the same type containing the key,value mappings as in the origin
	 * table of the keys present in table1 and table2. If a certain key in table1 or table2 is not present in the origin table the key will be in the returned
	 * table with value equal to new Double(0).
	 */
	public static Hashtable<String, Double> keepInTable(Hashtable<String, Double> origin, Hashtable<String, Double> table1, Hashtable<String, Double> table2) {
		Hashtable<String, Double> result = new Hashtable<String, Double>();
		for (Enumeration<String> e = table1.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Double value = origin.get(key);
			if (value == null)
				value = new Double(0);
			result.put(key, value);
		}
		for (Enumeration<String> e = table2.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Double value = origin.get(key);
			if (value == null)
				value = new Double(0);
			result.put(key, value);
		}
		return result;
	}

	/**
	 * Given two Hashtable of type Object-->Integer, the method adds the values of all the key-value pairs from the partialRules to Rules. If any key is mapped
	 * to null value in parialRules the method treats it as if it is mapped to value Double(0).
	 */
	public static void addAll(Hashtable<String, Integer> partialRules, Hashtable<String, Integer> Rules) {
		for (Enumeration<String> e = partialRules.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Integer partialCount = partialRules.get(key);
			Integer count = Rules.get(key);
			if (count == null)
				count = new Integer(0);
			int newCount = count.intValue() + partialCount.intValue();
			Rules.put(key, new Integer(newCount));
		}
	}

	/**
	 * Given two Hashtable of type Object-->Integer[2], the method adds the corresponding values of all the key-value pairs from the partialRules to Rules. The
	 * boolean removeUniqueIndex in input specifies if the key values in partialRules need to have the unique indexes cleaned before the adding procedure to
	 * take place.
	 */
	public static void addAllDuets(Hashtable<String, Integer[]> partialRules, Hashtable<String, Integer[]> Rules, boolean removeUniqueIndex) {
		for (Enumeration<String> e = partialRules.keys(); e.hasMoreElements();) {
			String key = e.nextElement();
			Integer[] partialCount = partialRules.get(key);
			if (removeUniqueIndex)
				key = key.replaceAll("@\\d+", "");
			Integer[] count = Rules.get(key);
			if (count == null)
				Rules.put(key, partialCount);
			else {
				count[0] = new Integer(count[0].intValue() + partialCount[0].intValue());
				count[1] = new Integer(count[1].intValue() + partialCount[1].intValue());
			}
		}
	}

	/**
	 * Print the time in standard output
	 */
	public static void printTime() {
		Date now = new Date();
		System.out.println(now);
	}

	/**
	 * Returns true iff the array of String match contains a String which is equal to the String s.
	 */
	public static boolean stringMatches(String s, String[] match) {
		for (int i = 0; i < match.length; i++) {
			if (s.equals(match[i]))
				return true;
		}
		return false;
	}

	/**
	 * Returns a random integer between 0 and (max-1)
	 */
	public static int randomInteger(int max) {
		return (int) (Math.random() * max);
	}

	public static float randomNoise(float value, float maxNoise) {
		return (float) (Math.random() * 2 * maxNoise - maxNoise + value);

	}

	/**
	 * Returns a random boolean
	 */
	public static boolean randomBoolean() {
		return (Math.random() > .5) ? true : false;
	}

	public static void increaseAllOne(int[] a) {
		for (int i = 0; i < a.length; i++)
			a[i]++;
	}

	public static int[][][][] multiCombinations(int[] list) {
		int length = list.length;
		int[][] sizeChoices = combinations(list);
		int sizeChoicesLength = sizeChoices.length;
		int[][][][] result = new int[sizeChoicesLength][][][];

		for (int i = 0; i < sizeChoicesLength; i++) {
			int[] sizeChoice = sizeChoices[i];
			increaseOne(sizeChoice);
			int[] sizeBino = makeSizeBino(list, sizeChoice);
			int[][] partialResult = combinations(sizeBino);
			int partialResultLength = partialResult.length;
			result[i] = new int[partialResultLength][length][];
			for (int j = 0; j < partialResultLength; j++) {
				for (int l = 0; l < length; l++) {
					int nairIndex = partialResult[j][l];
					result[i][j][l] = Nair.get(list[l], sizeChoice[l])[nairIndex];
				}
			}
		}
		return result;
	}

	public static int[] makeSizeBino(int[] list, int[] sizeChoices) {
		int[] result = new int[list.length];
		for (int i = 0; i < list.length; i++) {
			result[i] = Bino.get(list[i], sizeChoices[i]);
		}
		return result;
	}

	public static void increaseOne(int[] list) {
		for (int i = 0; i < list.length; i++)
			list[i]++;
	}

	/**
	 * Given a one dimensional array of integer (i.e. {2,3,2}) the methods returns a twodimensional array of integers given by means of all combinations of
	 * indexes i.e. [[0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1], [0, 2, 0], [0, 2, 1], [1, 0, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1], [1, 2, 0], [1, 2, 1]]
	 */
	public static int[][] combinations(int[] list) {
		int combinations = product(list);
		int[][] result = new int[combinations][list.length];
		if (list.length == 1) {
			int row = 0;
			for (int i = 0; i < list[0]; i++) {
				result[row][0] = i;
				row++;
			}
			return result;
		}
		if (list.length == 2) {
			int row = 0;
			for (int i = 0; i < list[0]; i++) {
				for (int j = 0; j < list[1]; j++) {
					result[row][0] = i;
					result[row][1] = j;
					row++;
				}
			}
			return result;
		}
		int[] newList = new int[list.length - 1];
		for (int i = 0; i < list.length - 1; i++)
			newList[i] = list[i];
		int[][] partialResult = combinations(newList);
		int row = 0;
		for (int i = 0; i < partialResult.length; i++) {
			for (int k = 0; k < list[list.length - 1]; k++) {
				for (int j = 0; j < list.length - 1; j++) {
					result[row][j] = partialResult[i][j];
				}
				result[row][list.length - 1] = k;
				row++;
			}
		}
		return result;
	}

	/*
	 * 1 * 2 * ... * n-1 * n max n = 12
	 */
	public static int factorial(int n) {
		return factorial(1, n);
	}

	/*
	 * 1 * 2 * ... * n-1 * n max n = 20
	 */
	public static long factorialLong(long n) {
		return factorialLong(1, n);
	}

	/*
	 * s * s+1 * ... * e-1 * e max s,e = 12
	 */
	public static int factorial(int s, int e) {
		int result = s;
		for (int i = s + 1; i <= e; i++)
			result *= i;
		return result;
	}

	/*
	 * s * s+1 * ... * e-1 * e max s,e = 20
	 */
	public static long factorialLong(long s, long e) {
		long result = s;
		for (long i = s + 1; i <= e; i++)
			result *= i;
		return result;
	}

	/**
	 * All possible ways of choosing n elements from c.
	 * 
	 * @param c
	 * @param n
	 * @return : int
	 */
	public static int binomial(int c, int n) {
		if (n == 0 || n == c)
			return 1;
		if (n == 1 || n == c - 1)
			return c;
		/*
		 * if (c>17 || n>17) { System.err.println("Binomial max variable = 17"); System.exit(-1); }
		 */
		if (n > c / 2)
			return factorial(n + 1, c) / factorial(c - n); // n = c - n
		return factorial(c - n + 1, c) / factorial(n);
	}

	/**
	 * All possible ways of choosing n consecutive elements from c.
	 * 
	 * @param c
	 * @param n
	 * @return : int
	 */
	public static int binomial_continuous(int c, int n) {
		return c - n + 1;
	}

	/**
	 * All possible ways of choosing n elements from c.
	 * 
	 * @param c
	 * @param n
	 * @return long
	 */
	public static long binomialLong(long c, long n) {
		if (c == 0 || c == n)
			return 1;
		if (c == 1 || c == n - 1)
			return n;
		if (n > c / 2)
			return factorialLong(n + 1, c) / factorialLong(c - n); // n = c - n
		return factorialLong(c - n + 1, c) / factorialLong(n);
	}

	/**
	 * Returns all possible sets of n elements taken from c possible elements. i.e. n_air(4,2): 0 1, 0 2, 0 3, 1 2, 1 3, 2 3
	 * 
	 * @param c
	 * @param n
	 * @return max c,n = 17
	 */
	public static int[][] n_air(int c, int n) {
		int combinations = binomial(c, n);
		int[][] result = new int[combinations][n];
		if (n == 1) {
			int row = 0;
			for (int i = 0; i < c; i++) {
				result[row][0] = i;
				row++;
			}
			return result;
		}
		if (n == 2) {
			int row = 0;
			for (int i = 0; i < c; i++) {
				for (int j = i + 1; j < c; j++) {
					result[row][0] = i;
					result[row][1] = j;
					row++;
				}
			}
			return result;
		}
		int[][] partialResult = n_air(c - 1, n - 1);
		int row = 0;
		for (int i = 0; i < partialResult.length; i++) {
			int k = partialResult[i][n - 2] + 1;
			do {
				for (int j = 0; j < n - 1; j++) {
					result[row][j] = partialResult[i][j];
				}
				result[row][n - 1] = k++;
				row++;
			} while (k < c);
		}
		return result;
	}

	/**
	 * Returns all possible NO DISCOUNTINUOUS sublist of n elements taken from a list of c elements. i.e. n_air(4,2): 0 1, 1 2, 2 3
	 * 
	 * @param c
	 * @param n
	 * @return max c,n = 17
	 */
	public static int[][] n_air_continuous(int c, int n) {
		int combinations = c - n + 1;
		int[][] result = new int[combinations][n];
		for (int s = 0; s < combinations; s++) {
			for (int e = 0; e < n; e++) {
				result[s][e] = s + e;
			}
		}
		return result;
	}

	/**
	 * Given an input array of int the method returns the product of the integers in it.
	 */
	public static int product(int[] array) {
		int result = array[0];
		for (int i = 1; i < array.length; i++) {
			result *= array[i];
		}
		return result;
	}

	public static double product(double[] array) {
		double result = array[0];
		for (int i = 1; i < array.length; i++) {
			result *= array[i];
		}
		return result;
	}

	/**
	 * Given an input array of long the method returns the product of the integers in it.
	 */
	public static long product(long[] array) {
		long result = array[0];
		for (int i = 1; i < array.length; i++) {
			result *= array[i];
		}
		return result;
	}

	/**
	 * Counting parenthesis in the input String
	 * 
	 * @return an integer i = 1* number_of_( - 1* number_of_)
	 */
	public static int countParenthesis(String line) {
		int result = 0;
		for (int i = 0; i < line.length(); i++) {
			if (line.charAt(i) == '(')
				result++;
			else if (line.charAt(i) == ')')
				result--;
		}
		return result;
	}

	/**
	 * Returns a array of int containing a permutation of the first n integers (0,1,...,n-1).
	 */
	public static int[] permutation(int n) {
		int[] result = new int[n];
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < n; i++)
			list.add(new Integer(i));
		Collections.shuffle(list);
		for (int i = 0; i < n; i++)
			result[i] = list.get(i);
		return result;
	}

	/**
	 * Return the root of the tree raprsented by the input bracketted String.
	 */
	public static String get_unique_root(String eTree) {
		return eTree.substring(eTree.indexOf('(') + 1, eTree.indexOf(' ')).trim();
	}

	/**
	 * Return the lhs part (category) of the CFG rule reported in the input String 'CFGRule' (if space(s) are in between lhs and rhs).
	 */
	public static String getCFGcategory(String CFGRule) {
		return CFGRule.substring(0, CFGRule.indexOf(' '));
	}

	/**
	 * Two Hashtable of type String-->Double are in the input: 'CFGrules' containing CFG rules and relative counts and 'CFGcategories' containing lhs
	 * (categories) and relative counts. The method updates the counts of the 'CFGrules' normalizing each rule by the count of the relative category contained
	 * in 'CFGcategories'.
	 */
	public static void normalizenCFRulesOnCategory(Hashtable<String, Double> CFGrules, Hashtable<String, Double> CFGcategories) {
		for (Enumeration<String> e = CFGrules.keys(); e.hasMoreElements();) {
			String rule = e.nextElement();
			String category = Utility.getCFGcategory(rule);
			double count_category = CFGcategories.get(category).doubleValue();
			double weight = CFGrules.get(rule).doubleValue() / count_category;
			CFGrules.put(rule, new Double(weight));
		}
	}

	/**
	 * Given a LinkedList of CFG rules in input (rules), the method updateds two Hashtable of type String-->Double 'CFGRules' and 'CFGcategories'
	 * ('CFGcategories' can be null and in this case will not be updated). Every rule in 'rules' will be added in 'CFGRules', and its category (lhs) in
	 * 'CFGcategories' with value 'count'. The boolean in input 'prelexical' and 'lexical' determine whether the non-lexical and lexical rules are allowed (if
	 * we set both variables to true all rules will be considered).
	 */
	public static void addAllCFGRules(LinkedList<String> rules, double count, Hashtable<String, Double> CFGRules, Hashtable<String, Double> CFGcategories, boolean prelexical, boolean lexical) {
		for (ListIterator<String> j = rules.listIterator(); j.hasNext();) {
			String rule = j.next();
			boolean isLex = (rule.indexOf('"') != -1);
			if (isLex && !lexical)
				continue;
			if (!isLex && !prelexical)
				continue;
			if (CFGcategories != null) {
				String category = getCFGcategory(rule);
				increaseStringDouble(CFGcategories, category, count);
			}
			increaseStringDouble(CFGRules, rule, count);
		}
	}

	/**
	 * Returns a double being the contribution of each stochastic variable instance 'weight' to the determine its global entropy.
	 */
	public static double pLogp(double weight) {
		if (weight == 0)
			return 0;
		return weight * Math.log(weight);
	}

	/**
	 * Given an Object 'o' which has to be either an Integer or a Double, the method returns the double value of 'o'.
	 */
	public static double getDouble(Object o) {
		if (o.getClass().isInstance(new Double(0)))
			return ((Double) o).doubleValue();
		return ((Integer) o).doubleValue();
	}

	/**
	 * Convert \' --> ', \$ --> $,
	 * 
	 * @param s
	 *            input string
	 * @return the converted string
	 */
	public static String cleanSlash(String s) {
		char slash = '\\';
		int index = s.lastIndexOf(slash);
		if (index == -1)
			return s;
		String result;
		char symbol = s.charAt(index + 1);
		if (symbol == '$' || symbol == '\'' || symbol == '#' || symbol == '=') {
			if (index == 0)
				result = s.substring(1);
			else
				result = s.substring(0, index) + s.substring(index + 1);
			if (symbol == '/')
				return result;
		} else
			return s;
		return cleanSlash(result);
	}

	public static String replaceDoubleSlash(String s) {
		int index = s.indexOf("\\\\");
		if (index == -1)
			return s;
		return s.substring(0, index) + "\\" + s.substring(index + 2);
	}

	/**
	 * Given a LinkedList of objects 'inputSet' and a double 'percentage' (0,1) the method returns a new LinkedList containing a random subset of element of the
	 * 'inputSet' being a portion of the entire set equivalent to the input percentage.
	 */
	public static LinkedList<Object> extractRandomPercentage(LinkedList<Object> inputSet, double percentage) {
		LinkedList<Object> result = new LinkedList<Object>();
		int input_size = inputSet.size();
		int target_size = (int) (input_size * percentage);
		int actual_size = 0;
		while (actual_size < target_size) {
			int index = Utility.randomInteger(input_size);
			Object o = inputSet.remove(index);
			result.add(o);
			input_size--;
			actual_size++;
		}
		return result;
	}

	/**
	 * Given a input TreeNode 'TN' representing a one-lexicalized etree, the method returns an array of two String representing two part of 'TN' being braken at
	 * depth 3, when a recursive structure is being found at the top (precisely when the TOP nod has a daughter labeled 'S' and when this 'S' has itself an
	 * other doughter labeled 'S'.) The method returns null if no such recursive feature is being detected.
	 */
	/*
	 * public static String[] cutTop(TreeNode TN) { if (TN.label.equals("TOP") && TN.daughters[0].label.equals("S")) { TreeNode TN_S = TN.daughters[0]; for(int
	 * j=0; j<TN_S.daughters.length; j++) { TreeNode D = TN_S.daughters[j]; if (D.daughters != null && D.label.equals("S")) { String[] result = new String[2];
	 * TreeNode[] daughters = D.daughters; D.daughters = null; result[0] = TN.toString(); D.daughters = daughters; TN = D; result[1] = TN.toString(); return
	 * result; } } } return null; }
	 */

	public static int indexOf(Object o, Object[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == o)
				return i;
		}
		return -1;
	}

	public static int indexOf(int[] o, int[][] array) {
		for (int i = 0; i < array.length; i++) {
			if (Arrays.equals(array[i], o))
				return i;
		}
		return -1;
	}

	public static int[] intArrayConversion(List<Integer> list) {
		int[] result = new int[list.size()];
		for (ListIterator<Integer> i = list.listIterator(); i.hasNext();) {
			int interger = i.next();
			result[i.previousIndex()] = interger;
		}
		return result;
	}

	/**
	 * Returns a new array of int with `value` inserted at the correct position (natural order) of the incoming array. Check for the present of value in the
	 * array. If it's present return the same array as the one in input without modification.
	 * 
	 * @param array
	 *            input ordered array
	 * @param value
	 * @return : int[]
	 */
	public static int[] appendIntArraySet(int[] array, int value) {
		if (array == null)
			return new int[] { value };
		int insertionPoint = Arrays.binarySearch(array, value);
		if (insertionPoint >= 0)
			return array;
		insertionPoint = -insertionPoint - 1;
		int[] newArray = new int[array.length + 1];
		int index = 0;
		for (; index < insertionPoint; index++)
			newArray[index] = array[index];
		newArray[index++] = value;
		for (; index < newArray.length; index++)
			newArray[index] = array[index - 1];
		return newArray;
	}

	public static boolean isPunctuation(String word) {
		return word.matches("[.,:;'`?!()]+");
	}

	public static boolean matchString(String key, String[] array) {
		Arrays.sort(array);
		return (Arrays.binarySearch(array, key) >= 0);
	}

	public static String removeDoubleQuotes(String word) {
		return word.replaceAll("\"", "");
	}

	public static String booleanToOnOff(boolean value) {
		return (value) ? "on" : "off";
	}

	public static void removeOneToIntArray(int[] array) {
		for (int i = 0; i < array.length; i++)
			array[i]--;
	}

	public static boolean containsFalse(boolean[] array) {
		for (boolean b : array) {
			if (!b)
				return true;
		}
		return false;
	}

	public static String hashtableStringIntegerToString(Hashtable<String, Integer> table) {
		String result = "";
		String[] keySet = table.keySet().toArray(new String[] {});
		Arrays.sort(keySet);
		for (String s : keySet) {
			result += s + "\t" + table.get(s) + "\n";
		}
		return result;
	}

	public static void printListStandardOutput(List<? extends Object> list) {
		for (Object o : list) {
			System.out.println(o.toString());
		}
	}

	public static void hashtableOrderedToFile(Hashtable<String, Integer> table, File outputFile) {
		PrintWriter out = FileUtil.getPrintWriter(outputFile);
		IdentityHashMap<Integer, String> reversedTable;
		reversedTable = Utility.reverseStringIntegerTable(table);
		Integer[] countSorted = reversedTable.keySet().toArray(new Integer[] {});
		Arrays.sort(countSorted);
		for (int i = countSorted.length - 1; i >= 0; i--) {
			Integer count = countSorted[i];
			String pair = reversedTable.get(count);
			out.println(pair + "\t" + count);
		}
		out.println();
		out.close();
	}

	public static void hashtableRankedToFile(Hashtable<String, Integer> table, File outputFile) {
		PrintWriter out = FileUtil.getPrintWriter(outputFile);
		out.println("Rank\tFreq\tTokens\tTotalFreq");
		IdentityHashMap<Integer, String> reversedTable;
		reversedTable = Utility.reverseStringIntegerTable(table);
		Integer[] countSorted = reversedTable.keySet().toArray(new Integer[] {});
		Arrays.sort(countSorted);
		int rank = 1;
		int rankFreq = countSorted[countSorted.length - 1], rankTokens = 0;
		for (int i = countSorted.length - 1; i >= 0; i--) {
			Integer count = countSorted[i];
			// String value = reversedTable.get(count);
			if (count.intValue() != rankFreq) {
				out.println(rank + "\t" + rankFreq + "\t" + rankTokens + "\t" + rankFreq * rankTokens);
				rank++;
				rankFreq = count;
				rankTokens = 0;
			}
			rankTokens++;
		}
		out.println(rank + "\t" + rankFreq + "\t" + rankTokens + "\t" + rankFreq * rankTokens);
		out.println();
		out.close();
	}

	public static int[] countIntegerListClasses(List<Integer> list, int binSize) {
		int max = maxIntegerList(list);
		int binNumber = max / binSize + 1;
		int[] result = new int[binNumber];
		for (Integer i : list) {
			int bin = i / binSize;
			result[bin]++;
		}
		return result;
	}

	public static String printIntegerListClasses(List<Integer> list, int binSize) {
		int[] classes = countIntegerListClasses(list, binSize);
		String result = "";
		for (int i = 0; i < classes.length; i++) {
			int lowerBound = i * binSize;
			int upperBound = (i + 1) * binSize - 1;
			result += lowerBound + "-" + upperBound + "\t" + classes[i] + "\n";
		}
		return result;
	}

	public static int maxIntegerList(List<Integer> list) {
		int max = Integer.MIN_VALUE;
		for (Integer i : list) {
			if (i > max)
				max = i;
		}
		return max;
	}

	public static int maxIndex(int[] list) {
		int maxIndex = 0;
		int max = list[0];
		for (int i = 1; i < list.length; i++) {
			if (list[i] > max) {
				maxIndex = i;
				max = list[i];
			}
		}
		return maxIndex;
	}

	public static int max(int[] list) {
		int max = list[0];
		for (int i = 1; i < list.length; i++) {
			if (list[i] > max)
				max = list[i];
		}
		return max;
	}

	public static int min(int[] list) {
		int min = list[0];
		for (int i = 1; i < list.length; i++) {
			if (list[i] < min)
				min = list[i];
		}
		return min;
	}

	public static void fillDoubleIntArray(int[][] array, int value) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = value;
			}
		}
	}

	public static void fillDoubleFloatArray(float[][] array, float value) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = value;
			}
		}
	}

	public static void printIntChart(int[][] array, String[] columnHeader, String[] rowHeader) {
		String result = "";
		for (int i = 0; i < columnHeader.length; i++)
			result += "\t" + columnHeader[i];
		result += "\n";
		for (int i = 0; i < array.length; i++) {
			result += rowHeader[i];
			for (int j = 0; j < array[i].length; j++) {
				result += "\t" + array[i][j];
			}
			result += "\n";
		}
		System.out.println(result);
	}

	public static void printFloatChart(float[][] array, String[] columnHeader, String[] rowHeader) {
		String result = "";
		for (int i = 0; i < columnHeader.length; i++)
			result += "\t" + columnHeader[i];
		result += "\n";
		for (int i = 0; i < array.length; i++) {
			result += rowHeader[i];
			for (int j = 0; j < array[i].length; j++) {
				result += "\t" + array[i][j];
			}
			result += "\n";
		}
		System.out.println(result);
	}

	public static void printFloatArray(float[][] array) {
		String result = "";
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				result += "\t" + array[i][j];
			}
			result += "\n";
		}
		System.out.println(result);
	}

	public static void printIntArray(int[][] array) {
		if (array == null) {
			System.out.println("null");
			return;
		}
		String result = "";
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				result += "\t" + array[i][j];
			}
			result += "\n";
		}
		System.out.println(result);
	}

	public static void printIntArray(int[][][][] array) {
		if (array == null) {
			System.out.println("null");
			return;
		}
		String result = "<\n";
		for (int i = 0; i < array.length; i++) {
			result += "\t< ";
			for (int j = 0; j < array[i].length; j++) {
				result += "<";
				for (int h = 0; h < array[i][j].length; h++) {
					result += "<";
					for (int k = 0; k < array[i][j][h].length; k++) {
						result += array[i][j][h][k];
						if (k != array[i][j][h].length - 1)
							result += ",";
					}
					result += (h != array[i][j].length - 1) ? ">," : ">";
				}
				result += (j != array[i].length - 1) ? ">, " : ">";
			}
			result += " >\n";
		}
		result += ">\n";
		System.out.println(result);
	}

	/**
	 * Computes the possible ways in which it's possible to split the integer n in two (non zero) parts. i.e. split(6): 1 5, 2 4, 3 3, 4 2, 5 1,
	 * 
	 * @param n
	 *            The integer to be split
	 * @return The number of ways of splitting
	 */
	public static int[][] split(int n) {
		int[][] result = new int[n - 1][2];
		int row = 0;
		for (int i = 1; i < n; i++) {
			result[row][0] = i;
			result[row][1] = n - i;
			row++;
		}
		return result;
	}

	public static int[] parseIndexList(String[] words) {
		int[] result = new int[words.length];
		for (int i = 0; i < words.length; i++) {
			result[i] = Integer.parseInt(words[i]);
		}
		return result;
	}

	public static void tartaglia() {
		for (int i = 1; i < Integer.MAX_VALUE; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(Utility.binomial(i, j) + "\t");
			}
			System.out.println();
		}
	}

	/**
	 * Returns true if a is greater than b in at least one of its components. and greater or equal in all the others a = 1,4,7,8 b = 1,3,7,8 true a = 1,4,7,8 b
	 * = 1,4,7,8 false a = 1,5,8,9 b = 2,4,7,8 false
	 * 
	 * @param a
	 *            ascending ordered list of integer
	 * @param b
	 *            ascending ordered list of integer
	 * @return Boolean indicating if greaterThan holds
	 */
	public static boolean greaterThan(int[] a, int[] b) {
		int length = a.length;
		boolean foundGreater = false;
		for (int i = length - 1; i > -1; i--) {
			if (a[i] < b[i])
				return false;
			if (a[i] > b[i])
				foundGreater = true;
		}
		return foundGreater;
	}

	public static boolean allZero(int[] a) {
		for (int ai : a) {
			if (ai != 0)
				return false;
		}
		return true;
	}

	public static boolean allZero(float[] a) {
		for (float ai : a) {
			if (ai != 0f)
				return false;
		}
		return true;
	}

	public static boolean allZero(double[] a) {
		for (double ai : a) {
			if (ai != 0)
				return false;
		}
		return true;
	}

	public static boolean greaterThan(float[] a, float[] b) {
		int length = a.length;
		boolean foundGreater = false;
		for (int i = length - 1; i > -1; i--) {
			if (a[i] < b[i])
				return false;
			if (a[i] > b[i])
				foundGreater = true;
		}
		return foundGreater;
	}

	public static boolean greaterThan(double[] a, double[] b) {
		int length = a.length;
		boolean foundGreater = false;
		for (int i = 0; i < length; i++) {
			if (a[i] < b[i])
				return false;
			if (a[i] > b[i])
				foundGreater = true;
		}
		return foundGreater;
	}

	public static boolean greaterThanPriority(double[] a, double[] b) {
		int length = a.length;
		for (int i = 0; i < length; i++) {
			if (a[i] < b[i])
				return false;
			if (a[i] > b[i])
				return true;
		}
		return false;
	}

	public static int[] concat(int[] a, int[] b) {
		int[] result = new int[a.length + b.length];
		int i = 0;
		for (int ai : a) {
			result[i] = ai;
			i++;
		}
		for (int bi : b) {
			result[i] = bi;
			i++;
		}
		return result;
	}

	public static int[] concat(int a, int[] b) {
		int[] result = new int[b.length + 1];
		result[0] = a;
		int i = 1;
		for (int bi : b) {
			result[i] = bi;
			i++;
		}
		return result;
	}

	public static int[] concat(int[] a, int b) {
		int[] result = new int[a.length + 1];
		int i = 0;
		for (int ai : a) {
			result[i] = ai;
			i++;
		}
		result[i] = b;
		return result;
	}

	public static double getCondProb(Hashtable<String, Integer> freqTable, Hashtable<String, Integer> condFreqTable, String key, String condKey) {
		Integer keyFreq = freqTable.get(key);
		if (keyFreq == null)
			return 0;
		Integer keyCondFreq = condFreqTable.get(condKey);
		return (double) keyFreq / keyCondFreq;
	}

	public static String joinStringArrayToString(String[] array, String separator) {
		if (array.length == 0)
			return "";
		String result = array[0];
		for (int i = 1; i < array.length; i++) {
			result += separator + array[i];
		}
		return result;
	}

	public static String joinIntArrayToString(int[] array, String separator) {
		String result = "";
		for (int i = 0; i < array.length; i++) {
			result += array[i];
			if (i != array.length - 1)
				result += separator;
		}
		return result;
	}

	public static String[] splitOnTabs(String a) {
		return a.split("\t");
	}

	public static String[] splitOnNewLine(String a) {
		return a.split("\n");
	}

	public static boolean isInInterval(int index, int[] c) {
		return (index >= c[0] && index <= c[1]);
	}

	public static boolean isInInterval(int[] c1, int[] c2) {
		return (c1[0] >= c2[0] && c1[1] <= c2[1]);
	}

	public static boolean isIntervalExtreme(int index, int[] c) {
		return (index == c[0] || index == c[1]);
	}

	public static boolean isIntervalExtreme(int[] c1, int[] c2) {
		return (c1[0] == c2[0] || c1[1] == c2[1]);
	}

	public static String[] concat(String[] as1, String[] as2) {
		String[] result = new String[as1.length + as2.length];
		int i = 0;
		for (String s : as1) {
			result[i] = s;
			i++;
		}
		for (String s : as2) {
			result[i] = s;
			i++;
		}
		return result;
	}

	public static boolean isIntegerList(String[] list) {
		Pattern p = Pattern.compile("-{0,1}\\d+");
		for (String s : list) {
			if (!p.matcher(s).matches())
				return false;
		}
		return true;
	}

	public static boolean isInteger(String s) {
		Pattern p = Pattern.compile("-{0,1}\\d+");
		return p.matcher(s).matches();
	}

	public static boolean xor(boolean b1, boolean b2) {
		return ((b1 || b2) && !(b1 && b2));
	}

	public static int sum(int[] array) {
		int result = 0;
		for (int i : array)
			result += i;
		return result;
	}

	public static boolean iff(boolean a, boolean b) {
		return ((a && b) || (!a && !b));
	}

	/**
	 * Remove each elements in a if there is an element in b equals to it and not previously associated with a removed element in a.
	 * 
	 * @param a
	 * @param b
	 * @return Boolean indicating whether a was changed
	 */
	public static <T> boolean removeAllOnce(ArrayList<T> a, ArrayList<T> b) {
		boolean modified = false;
		BitSet removedElementBIndex = new BitSet();
		ListIterator<T> aIter = a.listIterator();
		while (aIter.hasNext()) {
			T aNext = aIter.next();
			boolean found = false;
			ListIterator<T> bIter = b.listIterator();
			int index = 0;
			while (bIter.hasNext()) {
				T bNext = bIter.next();
				if (!removedElementBIndex.get(index) && aNext.equals(bNext)) {
					removedElementBIndex.set(index);
					found = true;
					break;
				}
				index++;
			}
			if (found) {
				aIter.remove();
				modified = true;
			}
		}
		return modified;
	}

	/**
	 * Addition in the log domain. Returns an approximation to ln(e^a + e^b). Just doing it naively might lead to underflow if a and b are very negative.
	 * Without loss of generality, let b<a . If a>-10, calculates it in the standard way. Otherwise, rewrite it as a + ln(1 + e^(b-a)) and approximate that by
	 * the first-order Taylor expansion to be a + (e^(b-a)). So if b is much smaller than a, there will still be underflow in the last term, but in that case,
	 * the error is small relative to the final answer.
	 */
	public static double logSum2(double a, double b) {
		if (a > b) {
			if (b == Double.NEGATIVE_INFINITY) {
				return a;
			} else if (a > -10) {
				return Math.log(Math.exp(a) + Math.exp(b));
			}

			else {
				return a + Math.exp(b - a);
			}
		} else {
			if (a == Double.NEGATIVE_INFINITY) {
				return b;
			} else if (b > -10) {
				return Math.log(Math.exp(a) + Math.exp(b));
			} else {
				return b + Math.exp(a - b);
			}
		}
	}

	/**
	 * From Markos (http://en.wikipedia.org/wiki/List_of_logarithmic_identities#Summation .2Fsubtraction) returns log(e^x + e^y) (log = natural logarithm)
	 */
	public static double logSum(double x, double y) {
		if (y == 0 || x == 0) {
			return logSumLongWay(x, y);
		}

		if (x < y) {
			double diff = x - y; // diff alway <= 0
			return y + Math.log1p(Math.exp(diff)); // Math.log(1. +
													// Math.exp(diff))
		}

		double diff = y - x; // diff alway <= 0
		return x + Math.log1p(Math.exp(diff)); // Math.log(1. + Math.exp(diff))
	}

	public static double logSumLongWay(double x, double y) {
		/*
		 * if (y==0 || x==0) { System.err.println("logSum with zero"); return -1; }
		 */

		double a = Math.exp(x);
		double b = Math.exp(y);
		return Math.log(a + b);

	}

	public static void regExTest() {
		String a = "  the `/Christian view they *argue |";
		a = a.replaceAll("[^\\w\\s]", "");
		System.out.println(a);
	}

	public static void testLog() {
		// for(int i=0; i<10; i++) {
		double a = Math.random();
		double b = 0;
		// double b = Math.random() * 0.1;
		// double b = a;
		// double a = 1;
		// double b = 1;

		double lnA = Math.log(a);
		double lnB = Math.log(b);

		double correct = Math.log(a + b);

		double logSumLongWay = logSumLongWay(lnA, lnB);
		double logSumLongWayError = Math.abs(correct - logSumLongWay);

		double logMarkos = logSum(lnA, lnB);
		double logMarkosError = Math.abs(correct - logMarkos);

		double logOtherVersion = logSum2(lnA, lnB);
		double logOtherVersionError = Math.abs(correct - logOtherVersion);

		System.out.println(lnA + " " + lnB + "\n\tLong Way Error:" + logSumLongWayError + "\t" + correct + "\n\tWiki Way Error:" + logMarkosError + "\n\tAlternative way Error:" + logOtherVersionError
				+ "\n");
		// }
	}

	public static void convertFreqTypesTokens() throws FileNotFoundException {
		File inputFile = new File("/Users/fedja/Desktop/danigraph/daniData2.txt");
		File outputFile = new File("/Users/fedja/Desktop/danigraph/daniData2_conv.txt");
		Scanner scan = new Scanner(inputFile);
		PrintWriter pw = new PrintWriter(outputFile);
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			String[] lineSplit = line.split("\t");
			int binSize = Integer.parseInt(lineSplit[0]);
			int freq = Integer.parseInt(lineSplit[1]);
			for (int i = 0; i < binSize; i++) {
				pw.println(freq);
			}
		}
		pw.close();
		scan.close();
	}

	public static void getFreqCount() throws FileNotFoundException {
		File inputFile = new File("/Users/fedja/Work/Papers/LREC10/Results/fragmentsTypesFreq.txt");
		File outputFile = new File("/Users/fedja/Work/Papers/LREC10/Results/freqCounts.txt");
		Scanner scan = new Scanner(inputFile);
		int currentFreq = -1;
		int currentTypesCount = 0;
		PrintWriter pw = new PrintWriter(outputFile);
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			String[] lineSplit = line.split("\t");
			Integer freq = Integer.parseInt(lineSplit[1]);
			if (freq != currentFreq) {
				if (currentFreq != -1) {
					pw.println(currentFreq + "\t" + currentTypesCount);
				}
				currentFreq = freq;
				currentTypesCount = 1;
			} else
				currentTypesCount++;
		}
		pw.println(currentFreq + "\t" + currentTypesCount);
		pw.close();
		scan.close();
	}

	public static void getColumn() throws FileNotFoundException {
		File inputFile = new File("/Users/fedja/Work/Papers/LREC10/Results/fragmentsTypesFreq.txt");
		File outputFile = new File("/Users/fedja/Work/Papers/LREC10/Results/freqs.txt");
		Scanner scan = new Scanner(inputFile);
		PrintWriter pw = new PrintWriter(outputFile);
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			String[] lineSplit = line.split("\t");
			int freq = Integer.parseInt(lineSplit[1]);
			pw.println(freq);
		}
		pw.close();
		scan.close();
	}

	public static <T> String arrayToStringDeep(T[][] array) {
		String result = "{";
		for (T[] t : array) {
			result += Arrays.toString(t);
		}
		result += "}";
		return result;

	}

	public static <T> int match(ArrayList<T> a, ArrayList<T> b) {
		if (a.size() != b.size())
			return -1;
		Iterator<T> aIter = a.iterator();
		Iterator<T> bIter = b.iterator();
		int count = 0;
		while (aIter.hasNext()) {
			T aNext = aIter.next();
			T bNext = bIter.next();
			if (aNext.equals(bNext))
				count++;
		}
		return count;
	}

	public static List<Integer> convertToDifferntNumericalSystem(int number, int base) {
		List<Integer> result = new ArrayList<Integer>();
		Stack<Integer> stack = new Stack<Integer>(); // create a stack
		while (number >= base) {
			stack.push(number % base); // compute rest
			number = number / base; // compute new number
		}

		stack.push(number % base); // add the last number, possibly 0

		// now it's time to collect the digits together
		while (!stack.empty()) {
			result.add(stack.pop());
		}
		return result;
	}

	/**
	 * Static method to generate a list representation for an ArrayList of Strings S1,...,Sn
	 * 
	 * @param list
	 *            A list of Strings
	 * @return A String consisting of the original list of strings concatenated and separated by commas, and enclosed by square brackets i.e. '[S1,S2,...,Sn]'
	 */
	public static String stringListString(List<String> list) {

		String result = "[";
		for (int i = 0; i < list.size() - 1; i++) {
			result += list.get(i) + ",";
		}

		if (list.size() > 0) {
			// get the generated word for the last target position
			result += list.get(list.size() - 1);
		}

		result += "]";

		return result;

	}

	public static <E> String objectListString(List<E> list) {
		String result = "[";
		for (int i = 0; i < list.size() - 1; i++) {
			result += list.get(i) + ",";
		}
		if (list.size() > 0) {
			// get the generated word for the last target position
			result += list.get(list.size() - 1);
		}
		result += "]";
		return result;
	}

	public static <E> String objectSetStringWithoutBrackets(Set<E> set) {
	        String result = "";
	        
	        Iterator<E> elementsIterator = set.iterator();
	        
	        
	        while(elementsIterator.hasNext()){	         	         
	            result += elementsIterator.next();
	            if(elementsIterator.hasNext()){	            
	              result += ",";
	            }  
	        }	     
	     	return result;
	}
	
	public static <E> String objectSetStringWithSetBrackets(Set<E> set) {
      return "{" + objectSetStringWithoutBrackets(set) + "}";
  }
	
	public static String stringGeneratorListString(List<StringGenerator> list) {
		String result = "[";
		for (int i = 0; i < list.size() - 1; i++) {
			result += list.get(i).generateValue() + ",";
		}
		if (list.size() > 0) {
			// get the generated word for the last target position
			result += list.get(list.size() - 1).generateValue();
		}
		result += "]";
		return result;

	}

	public static String stringGeneratorListStringWithoutBrackets(List<StringGenerator> list) {
		String result = "";
		for (int i = 0; i < list.size() - 1; i++) {
			result += list.get(i).generateValue() + " ";
		}
		if (list.size() > 0) {
			// get the generated word for the last target position
			result += list.get(list.size() - 1).generateValue();
		}
		return result;

	}

	/**
	 * Static method to generate a simple concatenated representation for an ArrayList of Strings S1,...,Sn
	 * 
	 * @param list
	 *            A list of Strings
	 * @return
	 */
	public static String stringListStringWithoutBrackets(List<String> list) {
		return stringListStringWithoutBracketsWithSpecifiedSeparator(list, " ");
	}

	public static String stringListStringWithoutBracketsCommaSeparated(List<String> list) {
		return stringListStringWithoutBracketsWithSpecifiedSeparator(list, ",");
	}

	public static String stringListStringWithoutBracketsWithSpecifiedSeparator(List<String> list, String separator) {

		String result = "";
		for (int i = 0; i < list.size() - 1; i++) {
			result += list.get(i) + separator;
		}

		if (list.size() > 0) {
			// get the generated word for the last target position
			result += list.get(list.size() - 1);
		}

		return result;

	}

	public static void deleteFile(String fileName) {

		// A File object to represent the filename
		File f = new File(fileName);

		// Make sure the file or directory exists and isn't write protected
		if (!f.exists())
			throw new IllegalArgumentException("Delete: no such file or directory: " + fileName);

		if (!f.canWrite())
			throw new IllegalArgumentException("Delete: write protected: " + fileName);

		// If it is a directory, make sure it is empty
		if (f.isDirectory()) {
			String[] files = f.list();
			if (files.length > 0)
				throw new IllegalArgumentException("Delete: directory not empty: " + fileName);
		}

		// Attempt to delete it
		boolean success = f.delete();

		if (!success) {
			throw new IllegalArgumentException("Delete: deletion failed");
		}
	}

	public static void main(String args[]) throws FileNotFoundException {
		getFreqCount();
	}

	public static boolean isEven(int i) {
		return ((i % 2) == 0);
	}

	/**
	 *  Removes all whitespaces and non visible characters such as tab, \n .
	 * @param string
	 * @return
	 */
	public static String removeWhitespace(String string) {
		return string.replaceAll("\\s+", "");
	}
}
