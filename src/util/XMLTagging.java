/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLTagging {

	public static String getLeftXMLTag(String propertyName) {
		return "<" + propertyName + ">";
	}

	public static String getRightXMLTag(String propertyName) {
		return "</" + propertyName + ">";
	}

	public static String getTagSandwidchedString(String propertyName, String value) {
		return getLeftXMLTag(propertyName) + value + getRightXMLTag(propertyName);
	}

	/**
	 * This method counts how often matchedString occurs in contentsString. It requires that the occurrences of contentsString will be separated by some other
	 * characters.
	 * 
	 * @param matchedString
	 * @param contentsString
	 * @return
	 */
	public static int countDisjointMatches(String matchedString, String contentsString) {
		Pattern pattern = Pattern.compile(matchedString);
		Matcher matcher = pattern.matcher(contentsString);
		int count = 0;
		while (matcher.find()) {
			count++;
		}
		return count;
	}

	public static String extractNamedPropertyFromString(String propertyName, String s) {
		int leftTagOccurences = countDisjointMatches(getLeftXMLTag(propertyName), s);
		int rightTagOccurences = countDisjointMatches(getRightXMLTag(propertyName), s);

		if ((leftTagOccurences < 1) || (rightTagOccurences < 1)) {
			throw new RuntimeException("XML property tags could not be found");
		} else if ((leftTagOccurences > 1) || (rightTagOccurences > 1)) {
			throw new RuntimeException("XML property tags found more than once");
		} else {
			int firstIndex = s.lastIndexOf(getLeftXMLTag(propertyName)) + getLeftXMLTag(propertyName).length();
			int lastIndex = s.lastIndexOf(getRightXMLTag(propertyName));
			String result = s.substring(firstIndex, lastIndex);
			return result;
		}
	}
}
