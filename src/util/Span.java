/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import util.Pair;

public class Span extends Pair<Integer> {

	public Span(Integer first, Integer second) {
		super(first, second);
	}

	public Span(Pair<Integer> span) {
		this(span.getFirst(), span.getSecond());
	}

	public int getSpanLength() {
		return (this.getSecond() - this.getFirst()) + 1;
	}

	public boolean contains(Span span2) {
		boolean firstPosContained = (span2.getFirst() >= this.getFirst()) && (span2.getFirst() <= this.getSecond());
		boolean secondPosContained = (span2.getSecond() >= this.getFirst()) && (span2.getSecond() <= this.getSecond());
		return firstPosContained && secondPosContained;
	}

	/**
	 * Boolean function that returns true if the second span starts directly after the first
	 * 
	 * @param secondSpan
	 * @return
	 */
	public boolean followsSpanConsecutively(Span secondSpan) {
		return (secondSpan.getFirst() == (this.getSecond() + 1));
	}

	/**
	 * Return a pair of Spans in order, based on the starting points of the two spans. If both start at the same point, return the first one as first of the
	 * pair
	 * 
	 * @param span1
	 * @param span2
	 * @return
	 */
	public static Pair<Span> spansInOrder(Span span1, Span span2) {
		if (span1.getFirst() <= span2.getFirst()) {
			return new Pair<Span>(span1, span2);
		}
		return new Pair<Span>(span2, span1);
	}

	public static boolean spansAreConsecutiveInSomeOrder(Span span1, Span span2) {
		Span firstSpan, secondSpan;
		Pair<Span> orderedSpans = spansInOrder(span1, span2);
		firstSpan = orderedSpans.getFirst();
		secondSpan = orderedSpans.getSecond();

		if (firstSpan.followsSpanConsecutively(secondSpan)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true if the secondSpan starts at the same point as this span
	 * 
	 * @param secondSpan
	 * @return
	 */
	public boolean startsAtSamePoint(Span secondSpan) {
		return this.getFirst() == secondSpan.getFirst();
	}

	/**
	 * Returns true if the secondSpan ends at the same point as this span
	 * 
	 * @param secondSpan
	 * @return
	 */
	public boolean endsAtSamePoint(Span secondSpan) {
		return this.getSecond() == secondSpan.getSecond();
	}

	public boolean isPartitionedBy(Span gap1AbsoluteSourceRange, Span gap2AbsoluteSourceRange) {
		Span firstSpan, secondSpan;
		Pair<Span> orderedSpans = spansInOrder(gap1AbsoluteSourceRange, gap2AbsoluteSourceRange);
		firstSpan = orderedSpans.getFirst();
		secondSpan = orderedSpans.getSecond();

		if (this.startsAtSamePoint(firstSpan) && this.endsAtSamePoint(secondSpan) && firstSpan.followsSpanConsecutively(secondSpan)) {
			return true;
		}
		return false;
	}
}
