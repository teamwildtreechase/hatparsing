/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

public class SubFileGenerator {
	BufferedReader inputFileReader;
	BufferedWriter outputFileWriter;
	// The first line to start with and the total number of lines to be written
	private int startLineNum, numLinesToWrite;
	private HashSet<Integer> acceptableLines;
	private boolean selectLinesFromList;

	public SubFileGenerator(String inputFileName, String outputFileName) {

		try {
			inputFileReader = new BufferedReader(new FileReader(inputFileName));
			outputFileWriter = new BufferedWriter(new FileWriter(outputFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * /** Constructor takes a range, acceptable lines will be generated for later reference by skipping lines that have problematic parts (for now just
	 * "&apos")
	 * 
	 * 
	 * @param inputFileName
	 *            : the name of the file to read from
	 * @param outputFileName
	 *            : the name of the file to write to
	 * @param startLineNum
	 *            : the first line to read
	 * @param numLinesToWrite
	 *            : the number of lines to write
	 */
	public SubFileGenerator(String inputFileName, String outputFileName, int startLineNum, int numLinesToWrite) {
		this(inputFileName, outputFileName);

		this.startLineNum = startLineNum;
		this.numLinesToWrite = numLinesToWrite;
		;
		acceptableLines = new HashSet<Integer>();
		this.selectLinesFromList = false;

	}

	/**
	 * Constructor that takes a list of acceptable lines
	 * 
	 * @param inputFileName
	 * @param outputFileName
	 * @param acceptableLines
	 */
	public SubFileGenerator(String inputFileName, String outputFileName, HashSet<Integer> acceptableLines) {
		this(inputFileName, outputFileName);
		this.acceptableLines = acceptableLines;
		this.selectLinesFromList = true;
		this.numLinesToWrite = acceptableLines.size();
	}

	public HashSet<Integer> generateSubFile() {
		String line;
		int lineNum = 1;
		int linesWritten = 0;
		try {
			while (((line = inputFileReader.readLine()) != null) && (linesWritten < numLinesToWrite)) {
				if (!selectLinesFromList) {

					if ((lineNum >= startLineNum)) {

						if (linesWritten == numLinesToWrite) {
							outputFileWriter.write(line);
							linesWritten++;
							acceptableLines.add(lineNum);
							break;
						} else {
							outputFileWriter.write(line + "\n");
							linesWritten++;
							acceptableLines.add(lineNum);
						}
					}
				} else {

					if (acceptableLines.contains(lineNum)) {
						outputFileWriter.write(line);
						if (linesWritten != numLinesToWrite) {
							outputFileWriter.write("\n");
						}
						linesWritten++;
					}
				}
				lineNum++;
			}

			inputFileReader.close();
			outputFileWriter.close();

			System.out.println("lines written" + linesWritten);

			return acceptableLines;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	public static void alignmentSubfilesGeneration() {
		String corpusLocation = "/scratch/wenniger/CorpusData2/europarl-clean/"; // "/scratch/wenniger/CorpusData/FullCorpus/";
		String trainFolder = "europarl-nl-en-short/train/";
		String testFolder = "europarl-nl-en-short/test/";

		String dutchInput = corpusLocation + "train.nl";
		String englishInput = corpusLocation + "train.en";
		// String dutchInput = corpusLocation + "train.OUT.nl";
		// String englishInput = corpusLocation + "train.OUT.en";
		// String alignmentInput = corpusLocation + "train_al.inter";
		String alignmentInput = corpusLocation + "aligned.grow-diag-final";

		String dutchTrainOutput = corpusLocation + trainFolder + "DutchTrain.txt";
		String englishTrainOutput = corpusLocation + trainFolder + "EnglishTrain.txt";
		String dutchTestOutput = corpusLocation + testFolder + "DutchTest.txt";
		String englishTestOutput = corpusLocation + testFolder + "EnglishTest.txt";

		String alignmentTrainOutput = corpusLocation + testFolder + "AlignmentTrain.txt";
		String alignmentTestOutput = corpusLocation + testFolder + "AlignmentTest.txt";

		SubFileGenerator sfg1 = new SubFileGenerator(englishInput, englishTrainOutput, 1, 10000);
		HashSet<Integer> set1 = sfg1.generateSubFile();
		System.out.println("set1.size():" + set1.size());
		SubFileGenerator sfg2 = new SubFileGenerator(englishInput, englishTestOutput, 200001, 1000);
		HashSet<Integer> set2 = sfg2.generateSubFile();
		System.out.println("set2.size():" + set2.size());
		SubFileGenerator sfg3 = new SubFileGenerator(dutchInput, dutchTrainOutput, set1);
		sfg3.generateSubFile();
		SubFileGenerator sfg4 = new SubFileGenerator(dutchInput, dutchTestOutput, set2);
		sfg4.generateSubFile();
		SubFileGenerator sfg5 = new SubFileGenerator(alignmentInput, alignmentTestOutput, set2);
		sfg5.generateSubFile();
		SubFileGenerator sfg6 = new SubFileGenerator(alignmentInput, alignmentTrainOutput, set1);
		sfg6.generateSubFile();

	}

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("Wrong usage. \nUsage : java -jar generateSubFile InputFile OutputFile FirstLine LastLine");
			return;
		} else {
			String inputFileName = args[0];
			String outputFileName = args[1];

			int firstLine = Integer.parseInt(args[2]);
			int linesToWrite = Integer.parseInt(args[3]);

			System.out.println("first line: " + firstLine + " lines to write: " + linesToWrite);

			SubFileGenerator sbfg = new SubFileGenerator(inputFileName, outputFileName, firstLine, linesToWrite);
			sbfg.generateSubFile();

		}
	}

}
