/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Locking {

	// The minimum wait time for a lock to report a warning
	public static final long MinWaitTimeLockWarning = 500;
	
	public static void acquireLockWitmMaxWaitTimeWarning(ReentrantLock lock, long maxWaitTimeInMillis, Object object, String className) {
		try {
	
			boolean lockObtained = false;
	
			while (!lockObtained) {
				lockObtained = lock.tryLock(maxWaitTimeInMillis, TimeUnit.MILLISECONDS);
				if (!lockObtained) {
					String id = Integer.toHexString(System.identityHashCode(object));
					System.out.println("WARNING : " + className + "  -  Aquiring write lock on node with objec id : " + id + " filed within "
							+ MinWaitTimeLockWarning + " milliseconds + \n Retrying...");
	
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
