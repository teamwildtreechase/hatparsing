/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

public class StringInteger {

	String string;
	private int integer;
	
	public StringInteger(String s, int i) {
		this.string = s;
		this.setInteger(i);
	}
	
	public String string() {
		return string;
	}
	
	public int integer() {
		return getInteger();
	}

	public void setInteger(int integer) {
		this.integer = integer;
	}

	public int getInteger() {
		return integer;
	}
	
}
