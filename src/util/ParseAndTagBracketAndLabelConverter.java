/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import static java.nio.file.StandardCopyOption.*;

public class ParseAndTagBracketAndLabelConverter {
	private static final String X = "X";
	public static final String X_SYNTACTIC_LABEL = "X-SyntacticLabel";
	
	
	public static void convertAllXLabelsInFile(String inputFileName) {
		LabelConverter.convertAllXLabelsInFile(inputFileName);
	}

	public static String getStringWithReplacedBrackets(String string) {
		return BracketsConverter.getStringWithReplacedBrackets(string);
	}

	public static List<String> getStringsWithReplacedXLabels(List<String> lines) {
		return LabelConverter.getStringsWithReplacedXLabels(lines);
	}

	public static List<String> getStringsWithReplacedBrackets(List<String> list) {
		return BracketsConverter.getStringsWithReplacedBrackets(list);
	}

	public static String xPatternMatchingPatternString() {
		return LabelConverter.xPatternMatchingPatternString();
	}
	
	public static String xSyntaxLabelPatternMatchingPatternString() 
	{
		return LabelConverter.xSyntaxLabelPatternMatchingPatternString();
	}

	private static List<String> getStringsWithReplacedCharacterUsingPatterns(List<String> lines, List<bitg.Pair<Pattern, String>> replacementPairs) {
		List<String> result = new ArrayList<String>();

		for (String line : lines) {
			assert (line != null);
			String replacedLine = new String(line);
			for (bitg.Pair<Pattern, String> replacementPair : replacementPairs) {
				replacedLine = replacementPair.first.matcher(replacedLine).replaceAll(replacementPair.last);
			}
			result.add(replacedLine);
		}
		return result;
	}

	private static List<String> getStringsWithReplacedCharacter(List<String> list, List<Pair<String>> replacementPairs) {
		List<bitg.Pair<Pattern, String>> replacementPairsWithPatterns = new ArrayList<bitg.Pair<Pattern, String>>();

		for (Pair<String> replacementPair : replacementPairs) {
			replacementPairsWithPatterns.add(new bitg.Pair<Pattern, String>(Pattern.compile(replacementPair.getFirst()), replacementPair.getSecond()));
		}
		return getStringsWithReplacedCharacterUsingPatterns(list, replacementPairsWithPatterns);
	}

	private static class BracketsConverter {
		public static String getStringWithReplacedBrackets(String string) {
			List<String> stringListWithReplacedBrackets = getStringsWithReplacedBrackets(Arrays.asList(string));
			return stringListWithReplacedBrackets.get(0);
		}

		public static List<String> getStringsWithReplacedBrackets(List<String> list) {
			List<Pair<String>> replacementPairs = new ArrayList<Pair<String>>();
			// LRB and RRB are standards in the Penn format for encoding ( and )
			// without getting conflicts with the parse-tree brackets.
			// In principle { and } also work but are less exact and may lead to
			// different labels and/or worse parsing accuracy (?)
			replacementPairs.add(new Pair<String>("\\(", "-LRB-"));
			replacementPairs.add(new Pair<String>("\\)", "-RRB-"));
			return ParseAndTagBracketAndLabelConverter.getStringsWithReplacedCharacter(list, replacementPairs);
		}
	}

	private static class LabelConverter {
		private static String BEGINNING_OF_LINE_EXPRESSION = "^";
		private static String END_OF_LINE_EXPRESSION = "$";
		private static String WHITESPACE_EXPRESSION = "\\s";
		private static Pattern X_CONSTITUENT_OR_TAG_PATTERN_MATCHING_PATTERN = Pattern.compile(xPatternMatchingPatternString());

		private final BufferedReader inputReader;
		private final BufferedWriter outputWriter;
		private final String inputFileName;
		private final String tempOutputFileName;

		private LabelConverter(BufferedReader inputReader, BufferedWriter outputWriter, String inputFileName, String tempOutputFileName) {
			this.inputReader = inputReader;
			this.outputWriter = outputWriter;
			this.inputFileName = inputFileName;
			this.tempOutputFileName = tempOutputFileName;
		}

		private static void convertAllXLabelsInFile(String inputFileName) {
			LabelConverter labelConverter = createLabelConverter(inputFileName);
			labelConverter.convertXLabels();
		}

		public static LabelConverter createLabelConverter(String inputFileName) {
			try {
				BufferedReader inputReader = new BufferedReader(new FileReader(inputFileName));
				String tempOutputFileName = inputFileName + ".tempLabelConversion";
				BufferedWriter outputWriter = new BufferedWriter(new FileWriter(tempOutputFileName));

				return new LabelConverter(inputReader, outputWriter, inputFileName, tempOutputFileName);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				throw new RuntimeException(e1);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		private void convertXLabels() {
			String line, previousLine = null;
			try {
				while ((line = inputReader.readLine()) != null) {

					if (previousLine != null) {
						outputWriter.write("\n");
					}
					outputWriter.write(getStringsWithReplacedXLabels(Collections.singletonList(line)).get(0));
					previousLine = line;
				}
				inputReader.close();
				outputWriter.close();
				Files.copy(Paths.get(tempOutputFileName), Paths.get(inputFileName), REPLACE_EXISTING);
				Utility.deleteFile(tempOutputFileName);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		private static String disjunctionExpression(List<String> disjunctionParts) {
			String result = "";

			Iterator<String> iterator = disjunctionParts.iterator();
			while (iterator.hasNext()) {
				result += iterator.next();
				if (iterator.hasNext()) {
					result += "|";
				}
			}
			return result;

		}

		private static String labelPatternMatchingPatternString(String label) {
			String lookBehindPattern = "(?<=" + disjunctionExpression(Arrays.asList(WHITESPACE_EXPRESSION, BEGINNING_OF_LINE_EXPRESSION, END_OF_LINE_EXPRESSION, "\\(")) + ")";
			String lookAheadPattern = "(?=" + disjunctionExpression(Arrays.asList(WHITESPACE_EXPRESSION, BEGINNING_OF_LINE_EXPRESSION, END_OF_LINE_EXPRESSION, "\\)")) + ")";
			String xPatternString = lookBehindPattern + label + lookAheadPattern;
			return xPatternString;
		}
		
		public static String xPatternMatchingPatternString() {
			return labelPatternMatchingPatternString(X);
		}

		public static String xSyntaxLabelPatternMatchingPatternString() {
			return labelPatternMatchingPatternString(X_SYNTACTIC_LABEL);
		}
		
		/**
		 * This method is used to replace any X labels in tag strings or parse strings
		 * 
		 * @param lines
		 * @return
		 */
		public static List<String> getStringsWithReplacedXLabels(List<String> lines) {
			List<bitg.Pair<Pattern, String>> replacementPairs = new ArrayList<bitg.Pair<Pattern, String>>();
			// LRB and RRB are standards in the Penn format for encoding ( and )
			// without getting conflicts with the parse-tree brackets.
			// In principle { and } also work but are less exact and may lead to
			// different labels and/or worse parsing accuracy (?)

			replacementPairs.add(new bitg.Pair<Pattern, String>(X_CONSTITUENT_OR_TAG_PATTERN_MATCHING_PATTERN, X_SYNTACTIC_LABEL));
			return ParseAndTagBracketAndLabelConverter.getStringsWithReplacedCharacterUsingPatterns(lines, replacementPairs);
		}

	}

}
