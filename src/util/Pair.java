/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;


public class Pair <T> {
	
	private T firstElement, secondElement;
	
	public Pair(T first, T second) {
		firstElement = first;
		secondElement = second;
	}
	
	public T getFirst() {
		return firstElement;
	}
	
	public T getSecond() {
		return secondElement;
	}
	
	public void setFirst(T newValue)
	{
		firstElement = newValue;
	}
	
	public void setSecond(T newValue)
	{
		secondElement = newValue;
	}
	
	public String toString() {
		return "(" + firstElement.toString() + ", " + 
		secondElement.toString() + ")";
	}
	
	/**
	 *  Method that specifies when two pairs are the same . This is the case when their two contained elements are the same
	 * @param pairObject
	 * @return Whether the pair is the same as the pair provided in the argument
	 */
	public boolean equals(Object pairObject)
	{
		//System.out.println("Equals method called...");
		
		if(pairObject instanceof Pair)
		{	
			@SuppressWarnings("rawtypes")
			Pair pair = (Pair) pairObject;
			
			if((pair.firstElement.equals(this.firstElement)) && (pair.secondElement.equals(this.secondElement)))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public int hashCode()
	{
		// See http://macchiato.com/columns/Durable6.html for implementation guidelines
		
		// A big prime number used for hashing	
		int  prime = 1000003; 
		
		int result = 0;
		
		// We start with the hascode for the first integer Hashset
		result = this.firstElement.hashCode();
		// we multiply the result with a big prime, and add the hascode for the second set to get our final result
		result = (result * prime) +  this.secondElement.hashCode();
		
		return result;		
	}
	
}
