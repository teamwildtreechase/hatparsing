/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.Assert;

public class FilePartSplitter {

	private final String baseFileName;
	private final int noParts;
	private final int totalNumLines;

	private FilePartSplitter(String baseFileName, int noParts, int totalNumLines) {
		this.baseFileName = baseFileName;
		this.noParts = noParts;
		this.totalNumLines = totalNumLines;
	}

	private static FilePartSplitter createFilePartSplitter(String baseFileName, int noParts) {
		int totalNumLines = computeTotalNumLines(baseFileName);
		return new FilePartSplitter(baseFileName, noParts, totalNumLines);
	}

	public static void splitFileIntoParts(String baseFileName, int noParts) {
		FilePartSplitter filePartSplitter = createFilePartSplitter(baseFileName, noParts);
		filePartSplitter.splitIntoParts();
	}

	private static int computeTotalNumLines(String baseFileName) {
		try {
			return FileStatistics.countLines(baseFileName);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private int linesPerPart() {
		return totalNumLines / noParts;
	}

	private int noRestLines() {
		int result = totalNumLines % noParts;
		// System.out.println("FilePartSplitter - noRest lines: " + result);
		return result;
	}

	private String partFileName(int partNum) {
		return partFileName(partNum, baseFileName);
	}

	public static String partFileName(int partNum, String baseFileName) {
		return baseFileName + "Part" + partNum + ".txt";
	}

	private boolean writeExtraLineToDealWithRestLines(int partNum) {
		// partNum starts from 1, so hence <= and not <
		return partNum <= noRestLines();
	}

	private int getNumLinesToWrite(int partNum) {
		int linesToWrite = linesPerPart();
		if (writeExtraLineToDealWithRestLines(partNum)) {
			linesToWrite++;
		}
		return linesToWrite;
	}

	private void splitIntoParts() {
		try {
			int originalNumLines = FileStatistics.countLines(baseFileName);
			System.out.println("splitIntoParts: originalNumLines " + originalNumLines);
			BufferedReader baseFileReader = new BufferedReader(new FileReader(baseFileName));

			int totalPartLines = 0;
			for (int partNum = 1; partNum <= noParts; partNum++) {
				BufferedWriter partWriter = new BufferedWriter(new FileWriter(partFileName(partNum)));
				for (int i = 0; i < getNumLinesToWrite(partNum) - 1; i++) {
					partWriter.write(baseFileReader.readLine() + "\n");
				}

				if (getNumLinesToWrite(partNum) > 0) {
					partWriter.write(baseFileReader.readLine());
				}

				partWriter.close();
				totalPartLines += getNumLinesToWrite(partNum);
			}
			baseFileReader.close();
			Assert.assertEquals(originalNumLines, totalPartLines);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Wrong usage, uage: java -jar splitIntoParts FileName NumParts");
			return;
		}
		String fileName = args[0];
		int numParts = Integer.parseInt(args[1]);
		splitFileIntoParts(fileName, numParts);
	}

}
