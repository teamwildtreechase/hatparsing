/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;
import javax.swing.text.JTextComponent;

/**
 * Class used to load cahced config files from a GUI and save them
 * 
 * @author gemaille
 * 
 */
public class ConfigCacher {
	private HashMap<String, ConfigCacherEntry> cacherEntries;
	private ConfigFile theConfig;
	private String configFileName;
	private static String LineBreakEncoding = "###LINEBREAK###";

	public static ConfigCacher createConfigCacher(String configFileName) {
		ConfigFile theConfig = null;
		try {
			FileUtil.createFileIfNotExisting(configFileName);

			theConfig = new ConfigFile(configFileName);
		} catch (FileNotFoundException e) {
			System.out.println("No cached config values found for gui, use defaults");
		}

		return new ConfigCacher(configFileName, theConfig);
	}

	public ConfigCacher(String configFileName, ConfigFile theConfig) {
		this.configFileName = configFileName;
		this.theConfig = theConfig;
		this.cacherEntries = new HashMap<String, ConfigCacherEntry>();
	}

	public void registerEntry(String configValueName, String configValueDefault, JTextComponent configValueGuiInput) {
		ConfigCacherEntry entry = new ConfigCacherEntry(configValueName, configValueDefault, configValueGuiInput);
		cacherEntries.put(configValueName, entry);
	}

	public String getValue(String name) {
		// return the value from the ConfigFile if it exists
		if (hasConfig() && this.theConfig.hasValue(name)) {
			return decodeLineBreaks(theConfig.getValue(name));
		} else if (this.cacherEntries.containsKey(name)) {
			// Return the default value from the corresponding ConfigCacherEntry
			return this.cacherEntries.get(name).getDefaultValue();
		} else {
			// Return the empty String is nothing is present for the name
			return "";
		}
	}

	private boolean hasConfig() {
		return (theConfig != null);
	}

	public void updateRegisteredTextFields() {
		for (String configValueName : this.cacherEntries.keySet()) {
			String value = this.getValue(configValueName);
			// Set the value of the associated textfield
			this.cacherEntries.get(configValueName).setTextField(value);
		}
	}

	public static String encodeLineBreaks(String s) {
		String result = s.replaceAll("[\r\n]", LineBreakEncoding);
		return result;
	}

	public static String decodeLineBreaks(String s) {
		String result = s.replaceAll(LineBreakEncoding, "\n");
		return result;
	}

	public void saveGuiConfig() {
		System.out.println("Saving gui configuration...");

		try {
			BufferedWriter outWriter = new BufferedWriter(new FileWriter(configFileName));

			outWriter.write("% This is an automatically generated configuration file \n");
			outWriter.write("% The file contains the values of parameters changed in and automatically stored by the GUI \n");

			for (String configValueName : this.cacherEntries.keySet()) {
				String value = encodeLineBreaks(this.cacherEntries.get(configValueName).getTextFieldValue());
				String entryToWrite = configValueName + " = " + value + "\n";
				outWriter.write(entryToWrite);
			}

			outWriter.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
