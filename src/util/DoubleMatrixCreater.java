/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

public class DoubleMatrixCreater extends NumberMatrixCreator<Double> 
{

	@Override
	public Double createZero() 
	{
		return new Double(0);
	}

	@Override
	public Double parseNumber(String numberString) 
	{
		return Double.parseDouble(numberString);
	}


}
