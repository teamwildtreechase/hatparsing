/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import tsg.*;

public class FileUtil {

    public static String defaultEncoding = "UTF-8";

    /**
     * Return a printWriter writing a specific outoutFile with a specific
     * encoding
     * 
     * @param outputFile
     * @param encoding
     * @return : PrintWriter
     */
    public static PrintWriter getPrintWriter(File outputFile, String encoding) {
	try {
	    PrintWriter output = new PrintWriter(outputFile, encoding);
	    return output;
	} catch (IOException e) {
	    FileUtil.handleExceptions(e);
	}
	return null;
    }

    /**
     * Return a printWriter writing a specific outputFile with default encoding
     * 
     * @param outputFile
     * @return : PrintWriter
     */
    public static PrintWriter getPrintWriter(File outputFile) {
	return getPrintWriter(outputFile, defaultEncoding);
    }

    public static void cleanFile(File outputFile) {
	PrintWriter pw = getPrintWriter(outputFile);
	pw.close();
    }

    /**
     * Returns a Scanner of the inputFile using a specific encoding
     * 
     * @param inputFile
     * @return : Scanner
     */
    public static Scanner getScanner(File inputFile, String encoding) {
	Scanner scan = null;
	try {
	    scan = new Scanner(inputFile, encoding);
	} catch (IOException e) {
	    FileUtil.handleExceptions(e);
	}
	return scan;
    }

    /**
     * Returns a Scanner of the inputFile using default encoding
     * 
     * @param inputFile
     * @return : Scanner
     */
    public static Scanner getScanner(File inputFile) {
	return getScanner(inputFile, defaultEncoding);
    }

    /**
     * Convert the inputFile in a LInkedList<String> one String for each line
     * using a specific encoding.
     * 
     * @param inputFile
     * @param encoding
     * @return : List<String> with result
     */
    public static List<String> convertFileToStringList(File inputFile, String encoding) {
	List<String> list = new ArrayList<String>();
	Scanner scan = getScanner(inputFile, encoding);
	while (scan.hasNextLine()) {
	    String line = scan.nextLine();
	    line = line.trim();
	    if (line.length() == 0)
		continue;
	    line = line.replaceAll("\n", "");
	    line = line.replaceAll("\\s+", " ");
	    list.add(line);
	}
	scan.close();
	return list;
    }

    /**
     * Convert the inputFile in a LInkedList<String> one String for each line
     * using the default encoding.
     * 
     * @param inputFile
     * @return : List<String> - A list of Strings corresponding the to the lines
     *         of the file
     */
    public static List<String> convertFileToStringList(File inputFile) {
	return convertFileToStringList(inputFile, defaultEncoding);
    }

    /**
     * Append in the input file fileName the line contained in the input String
     * line.
     */
    public static void append(String line, File fileName) {
	try {
	    FileWriter OutputWriter = new FileWriter(fileName, true);
	    OutputWriter.write(line + "\n");
	    OutputWriter.close();
	} catch (Exception e) {
	    handleExceptions(e);
	}
    }

    /**
     * Clear the content of the input file fileName
     */
    public static void clear(File fileName) {
	try {
	    FileWriter OutputWriter = new FileWriter(fileName, false);
	    // OutputWriter.write(line+"\n");
	    OutputWriter.close();
	} catch (Exception e) {
	    handleExceptions(e);
	}
    }

    /**
     * Returns a String giving a compact representation of the current date and
     * time.
     */
    public static String dataFolder() {
	Date d = new Date();
	String data = ((d).toString());
	data = data.substring(0, data.length() - 9);
	data = data.replace(' ', '_');
	data = data.replace(':', '_');
	return data;
    }

    /**
     * Method to compare two files of bracketted sentences.
     */
    public static void compareTreesInFiles(File fileA, File fileB) {
	List<String> linesA = getSentences(fileA, true);
	List<String> linesB = getSentences(fileB, true);
	if (linesA.size() != linesB.size()) {
	    System.out.println("Unequal number of lines: " + linesA.size() + " " + linesB.size());
	    return;
	}
	for (int i = 0; i < linesA.size(); i++) {
	    String lineA = linesA.get(i);
	    String lineB = linesB.get(i);
	    TSNode TNA = new TSNode(lineA, false);
	    TSNode TNB = new TSNode(lineB, false);
	    if (lineA.length() == 0 || lineB.length() == 0) {
		if (lineA.length() == 0 && lineB.length() == 0)
		    System.out.println(i + ": OK");
		else
		    System.out.println(i + ": Length unmatch (one line is empty)");
		continue;
	    }
	    int lengthA = TNA.collectTerminals().size();
	    int lengthB = TNB.collectTerminals().size();
	    if (lengthA != lengthB)
		System.out.println(i + ": Length unmatch (" + lengthA + "|" + lengthB + ")");
	    else
		System.out.println(i + ": OK");
	}
    }

    /**
     * Method to handle standard IO xceptions. catch (Exception e)
     * {Utility.handleIO_exception(e);}
     */
    public static void handleExceptions(Exception e) {
	e.printStackTrace();
	System.exit(-1);
    }

    /**
     * This method returns a List of String. Each element of the list
     * corresponds to a sentence from the untagged input file. The boolean
     * keepDuplicates in the input determines if duplicate sentences are allowed
     * in the output LinkedList or not.
     */
    static public List<String> getSentences(File file, boolean keepDuplicates) {
	LinkedList<String> list = new LinkedList<String>();
	String line = "";
	try {
	    BufferedReader InputReader = new BufferedReader(new FileReader(file));
	    for (;;) { // this loop writes writes in a Sting each sentence of
		       // the file and process it
		int current = InputReader.read();
		if (current == -1 || current == '\n') {
		    if (keepDuplicates || !list.contains(line))
			list.add(line);
		    line = "";
		    if (current == -1)
			break; // EOF
		} else
		    line += (char) current;
	    }
	    InputReader.close();
	} catch (Exception e) {
	    handleExceptions(e);
	}
	return list;
    }

    public static List<String> getLinesInFile(String filePath, boolean printProgress) {

	LineIterator it = null;
	try {

	    it = FileUtils.lineIterator(new File(filePath), "UTF-8");
	    ArrayList<String> list = new ArrayList<String>();

	    int numLines = FileStatistics.countLines(filePath);
	    int lineNum = 1;
	    int previousPercentageCompleted = 0;

	    while (it.hasNext()) {
		list.add(it.nextLine());
		lineNum++;

		int percentageCompleted = (100 * lineNum) / numLines;
		if (printProgress && (percentageCompleted > previousPercentageCompleted)) {
		    System.out.println(">>> " + percentageCompleted + "% completed");
		    previousPercentageCompleted = percentageCompleted;
		}
	    }
	    return list;
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    if (it != null) {
		it.close();
	    }
	}

    }
    
    /**
     * A convenience method that tests if two files contain the same lines in the same order. This is handy for
     * unit testing, for checking whether the produced output file matches the reference output file.
     * @param filePath1
     * @param filePath2
     * @param printProgress
     * @return
     */
    public static boolean filesContainSameLinesInSameOrder(String filePath1,String filePath2, boolean printProgress){
	List<String> filePathsOne = getLinesInFile(filePath1, printProgress);
	List<String> filePathsTwo = getLinesInFile(filePath2, printProgress);
	
	if(!(filePathsOne.size() == filePathsTwo.size())){
	    return false;
	}
	
	for(int i = 0; i < filePathsOne.size(); i++){
	    String line1 = filePathsOne.get(i);
	    String line2 = filePathsTwo.get(i);
	    if(!line1.equals(line2)){
		System.out.println("Found unequal lines at line " + i 
			+ ": \n" + line1 + "\n \t != \n" + line2 );
		return false;
	    }
	}
	return true;
    }
    
    

    static public String getFirstLineInFile(File inputFile) {
	Scanner scan = FileUtil.getScanner(inputFile);
	if (!scan.hasNextLine())
	    return null;
	String line = scan.nextLine();
	scan.close();
	return line;
    }

    static public String getLineInFileStartingWith(File inputFile, String prefix) {
	Scanner scan = FileUtil.getScanner(inputFile);
	while (scan.hasNextLine()) {
	    String line = scan.nextLine();
	    if (line.startsWith(prefix)) {
		scan.close();
		return line;
	    }
	}
	scan.close();
	return null;
    }

    public static void toBinaryFile(File outputFile, Object o) {
	ObjectOutputStream out = null;
	try {
	    out = new ObjectOutputStream(new FileOutputStream(outputFile));
	    out.writeObject(o);
	} catch (Exception e) {
	    FileUtil.handleExceptions(e);
	} finally {
	    try {
		out.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }

    public static Object fromBinaryFile(File inputFile) {
	Object o = null;
	try {
	    ObjectInputStream in = new ObjectInputStream(new FileInputStream(inputFile));
	    o = in.readObject();
	    in.close();
	} catch (Exception e) {
	    FileUtil.handleExceptions(e);
	}
	return o;
    }

    public static Object fromBinaryInputStream(InputStream inputStream) {
	Object o = null;
	try {
	    ObjectInputStream in = new ObjectInputStream(inputStream);
	    o = in.readObject();
	} catch (Exception e) {
	    FileUtil.handleExceptions(e);
	}
	return o;
    }

    public static void printHashtableToPw(Hashtable<String, Integer> table, PrintWriter writer) {
	for (Enumeration<String> e = table.keys(); e.hasMoreElements();) {
	    String rule = e.nextElement();
	    Integer count = table.get(rule);
	    writer.println(count.toString() + "\t" + rule);
	}
    }

    public static void printHashtableToFile(Hashtable<String, Integer> table, File outputFile) {
	PrintWriter writer = FileUtil.getPrintWriter(outputFile);
	printHashtableToPw(table, writer);
	writer.close();
    }

    public static void printHashtableToPwOrder(Hashtable<String, Integer> table, PrintWriter writer) {
	TreeSet<Integer> values = new TreeSet<Integer>(table.values());
	for (Integer i : values) {
	    for (Enumeration<String> e = table.keys(); e.hasMoreElements();) {
		String rule = e.nextElement();
		Integer count = table.get(rule);
		if (count.equals(i))
		    writer.println(count.toString() + "\t" + rule);
	    }
	}
    }

    public static void printHashtableToFileOrder(Hashtable<String, Integer> table, File outputFile) {
	PrintWriter writer = FileUtil.getPrintWriter(outputFile);
	printHashtableToPwOrder(table, writer);
	writer.close();
    }

    public static File fileWithPadding(String filePath, String extension, int n) {
	int count = 1;
	File f = null;
	do {
	    String padding = "" + count;
	    padding = Utility.fillChar(n - padding.length(), '0') + padding;
	    f = new File(filePath + padding + extension);
	    count++;
	} while (f.exists());
	return f;
    }

    public static File changeExtention(File selectedFile, String ext) {
	String extWithDot = "." + ext;
	String path = selectedFile.getParent();
	String fileName = selectedFile.getName();
	if (fileName.endsWith(extWithDot))
	    return selectedFile;
	int dotIndex = fileName.lastIndexOf('.');
	if (dotIndex != -1)
	    fileName = fileName.substring(0, dotIndex);
	fileName += extWithDot;
	selectedFile = new File(path + File.separator + fileName);
	// System.out.println(selectedFile);
	return selectedFile;
    }

    public static File postpendAndChangeExtension(File selectedFile, String postpend, String ext) {
	String extWithDot = "." + ext;
	String path = selectedFile.getParent();
	String fileName = selectedFile.getName();
	int dotIndex = fileName.lastIndexOf('.');
	if (dotIndex != -1)
	    fileName = fileName.substring(0, dotIndex);
	fileName += postpend + extWithDot;
	selectedFile = new File(path + File.separator + fileName);
	// System.out.println(selectedFile);
	return selectedFile;
    }

    public static File appendAndAddExtention(File selectedFile, String toAdd, String ext) {
	String fileName = selectedFile.getName();
	int dotIndex = fileName.indexOf('.');
	if (dotIndex != -1) {
	    fileName = fileName.substring(0, dotIndex);

	}
	fileName += toAdd;
	String path = selectedFile.getParent();
	selectedFile = new File(path + File.separator + fileName + "." + ext);
	// System.out.println(selectedFile);
	return selectedFile;
    }

    public static File appendBeforeExtention(File selectedFile, String toAdd) {
	String originalName = selectedFile.getName();
	int dotIndex = originalName.lastIndexOf('.');
	String fileName = originalName;
	String extension = "";
	if (dotIndex != -1) {
	    fileName = originalName.substring(0, dotIndex);
	    extension = originalName.substring(dotIndex);
	}
	fileName += toAdd + extension;
	String path = selectedFile.getParent();
	selectedFile = new File(path + File.separator + fileName);
	// System.out.println(selectedFile);
	return selectedFile;
    }

    public static String getFileNameWithoutExtensions(File testFile) {
	String fileName = testFile.getName();
	int dotIndex = fileName.indexOf('.');
	if (dotIndex == -1 || dotIndex == 0)
	    return fileName;
	return fileName.substring(0, dotIndex);
    }

    public static void append(File inputFile, PrintWriter pwComplete) {
	Scanner scan = getScanner(inputFile);
	while (scan.hasNextLine()) {
	    String line = scan.nextLine();
	    pwComplete.println(line);
	}
	scan.close();
    }

    // Returns the contents of the file in a byte array.
    public static byte[] getBytesFromFile(File file) throws IOException {
	InputStream is = null;

	try {
	    is = new FileInputStream(file);
	    // Get the size of the file
	    long length = file.length();
	    // You cannot create an array using a long type.
	    // It needs to be an int type.
	    // Before converting to an int type, check
	    // to ensure that file is not larger than Integer.MAX_VALUE.
	    if (length > Integer.MAX_VALUE) {
		// File is too large
		System.err.println("Lenght of the file in bytes bigger than " + Integer.MAX_VALUE);
		return null;
	    }
	    // Create the byte array to hold the data
	    byte[] bytes = new byte[(int) length];
	    // Read in the bytes
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
		    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
		offset += numRead;
	    }
	    // Ensure all the bytes have been read in
	    if (offset < bytes.length) {
		throw new IOException("Could not completely read file " + file.getName());
	    }
	    return bytes;
	} finally {
	    is.close();
	}
    }

    public static List<File> getAllEntriesInDirectory(File directory) {
	if (directory == null)
	    return Collections.emptyList();
	if (!directory.exists())
	    return Collections.emptyList();
	if (!directory.isDirectory())
	    return Collections.emptyList();

	String[] list = directory.list();

	List<File> result = new ArrayList<File>();
	// Some JVMs return null for File.list() when the
	// directory is empty.
	if (list != null) {
	    for (int i = 0; i < list.length; i++) {
		File entry = new File(directory, list[i]);
		result.add(entry);
	    }
	}
	return result;
    }

    public static List<File> getAllFilesInFolder(File directory, boolean allowRecursion) {
	List<File> result = new ArrayList<File>();
	for (File entry : getAllEntriesInDirectory(directory)) {
	    if (entry.isFile()) {
		result.add(entry);
	    }
	    if (allowRecursion && entry.isDirectory()) {
		result.addAll(getAllFilesInFolder(entry, allowRecursion));
	    }
	}
	return result;
    }

    public static List<File> getAllFoldersInFolder(File directory) {
	List<File> result = new ArrayList<File>();
	for (File entry : getAllEntriesInDirectory(directory)) {
	    if (entry.isDirectory()) {
		result.add(entry);
	    }
	}
	return result;
    }

    /**
     * Return a list of the name of the files (without full path) directly
     * stored in the directory at fileFolderPath
     * 
     * @param fileFolderPath
     * @return List of file names (without full path)
     */
    public static List<String> getFileNamesInDirectory(String fileFolderPath) {
	List<File> fileList = FileUtil.getAllFilesInFolder(new File(fileFolderPath), false);
	List<String> fileNameList = new ArrayList<String>();
	for (File file : fileList) {
	    if (file.isFile()) {
		String fileName = file.getName();
		fileNameList.add(fileName);
	    }
	}
	return fileNameList;
    }

    /**
     * Remove a directory and all of its contents.
     * 
     * The results of executing File.delete() on a File object that represents a
     * directory seems to be platform dependent. This method removes the
     * directory and all of its contents.
     * 
     * @return true if the complete directory was removed, false if it could not
     *         be. If false is returned then some of the files in the directory
     *         may have been removed.
     */
    public static boolean removeDirectory(File directory) {
	for (File entry : getAllEntriesInDirectory(directory)) {
	    // System.out.println("\tremoving entry " + entry);
	    if (entry.isDirectory()) {
		if (!removeDirectory(entry))
		    return false;
	    } else {
		if (!entry.delete())
		    return false;
	    }
	}
	return directory.delete();
    }

    private static boolean fileOrFolderExists(String fileName) {
	File file = new File(fileName);

	return file.exists();
    }

    public static boolean fileExists(String fileName) {
	return fileOrFolderExists(fileName);
    }

    public static boolean folderExists(String folderName) {
	return fileOrFolderExists(folderName);
    }

    public static void copyFile(String inputFilePath, String outputFilePath) {
	try {
	    TextFileIterator textFileIterator = new TextFileIterator(inputFilePath);
	    BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));

	    while (textFileIterator.hasNext()) {
		outputWriter.write(textFileIterator.next() + "\n");
	    }
	    outputWriter.close();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static void copyFileNative(String inputFilePath, String outputFilePath,
	    boolean replaceExistingFile) {
	System.out.println("copyFileNative: copying " + inputFilePath + " to " + outputFilePath);
	Path sourcePath = Paths.get(inputFilePath);
	Path targetPath = Paths.get(outputFilePath);
	try {
	    // See :
	    // http://docs.oracle.com/javase/tutorial/essential/io/copy.html
	    if (replaceExistingFile) {
		Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
	    } else {
		Files.copy(sourcePath, targetPath);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    public static void renameFile(String originalFilePath, String renamedFilePath) {
	File file = new File(originalFilePath);

	// File (or directory) with new name
	File file2 = new File(renamedFilePath);

	// Rename file (or directory)
	boolean success = file.renameTo(file2);
	if (!success) {
	    throw new RuntimeException("Error : failed to rename " + originalFilePath + " to "
		    + renamedFilePath);
	}
    }

    public static void appendFile(String appendToFilePath, String appendFilePath) {
	InputStream in = null;
	OutputStream os = null;
	try {
	    in = new FileInputStream(appendFilePath);
	    byte[] buffer = new byte[1 << 20]; // loads 1 MB of the file
	    os = new FileOutputStream(new File(appendToFilePath), true);
	    int count;
	    while ((count = in.read(buffer)) != -1) {
		os.write(buffer, 0, count);
		os.flush();
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} finally {
	    FileUtil.closeCloseableIfNotNull(in);
	    FileUtil.closeCloseableIfNotNull(os);
	}

    }

    public static void createAppendFilesFile(String resultFilePath,
	    List<String> appendFilePathsList, String encodingString) {
	BufferedWriter outputWriter = null;
	int linesWritten = 0;
	int newLinesWritten = 0;
	try {
	    outputWriter = createBufferedWriterWithSpecificEncoding(resultFilePath, encodingString);
	    int fileIndex = 0;
	    for (String appendFilePath : appendFilePathsList) {
		System.out.println("Appending file with "
			+ FileStatistics.countLines(appendFilePath) + " lines");
		LineIterator it = FileUtils.lineIterator(new File(appendFilePath), encodingString);

		while (it.hasNext()) {
		    String line = it.nextLine();
		    System.out.println("line: " + line);
		    boolean appendNewLine = it.hasNext()
			    || (fileIndex < (appendFilePathsList.size() - 1));
		    // if(line.length() > 0)
		    {
			outputWriter.write(line);
			if (appendNewLine) {
			    outputWriter.write("\n");
			    // Assert.assertTrue(line.length() > 0);
			    newLinesWritten++;
			}
			// System.out.println("line to append: " + line);
			linesWritten++;

		    }

		}
		it.close();
		fileIndex++;
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

	Assert.assertNotNull(outputWriter);
	FileUtil.closeCloseableIfNotNull(outputWriter);
	System.out.println("line written: " + linesWritten);
	System.out.println("newlines written: " + newLinesWritten);
	try {
	    Assert.assertEquals(linesWritten, FileStatistics.countLines(resultFilePath));
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public static String getFileAsString(String filePath) {
	List<String> lines = FileUtil.getSentences(new File(filePath), true);
	String result = "";
	for (int i = 0; i < (lines.size() - 1); i++) {
	    result += lines.get(i) + "\n";
	}
	result += lines.get(lines.size() - 1);
	return result;
    }

    public static void main(String args[]) {
	System.out.println(dataFolder());
    }

    public static void closeCloseableIfNotNull(Closeable fileWriter) {
	if (fileWriter != null) {
	    try {
		fileWriter.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }

    public static void createFileIfNotExisting(String fileName) {
	File newFile = new File(fileName);
	try {
	    newFile.createNewFile();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public static void createFolderIfNotExisting(String folderName) {
	File f = new File(folderName);
	if (!f.isDirectory()) {
	    System.out.println(" createFolderIfNotExisting -- Making directory: " + folderName);
	    f.mkdirs();
	} else {
	    System.out.println(" createFolderIfNotExisting -- Directory: " + folderName
		    + " already existed");
	}
    }

    /**
     * Check if a file is empty
     * 
     * @param file
     * @return
     */
    public static boolean fileIsEmpty(File file) {
	return file.length() == 0;
    }

    public static BufferedReader createBufferedReaderWithSpecificEncoding(String inputFilePath,
	    String encodingString) throws UnsupportedEncodingException, FileNotFoundException {
	Reader reader = new InputStreamReader(new FileInputStream(inputFilePath), encodingString);
	BufferedReader inputReader = new BufferedReader(reader);
	return inputReader;
    }

    public static BufferedWriter createBufferedWriterWithSpecificEncoding(String outputFilePath,
	    String encodingString, boolean append) throws UnsupportedEncodingException,
	    FileNotFoundException {
	Writer writer = new OutputStreamWriter(new FileOutputStream(outputFilePath, append),
		encodingString);
	BufferedWriter outputWriter = new BufferedWriter(writer);
	return outputWriter;
    }

    public static BufferedWriter createBufferedWriterWithSpecificEncoding(String outputFilePath,
	    String encodingString) throws UnsupportedEncodingException, FileNotFoundException {
	return createBufferedWriterWithSpecificEncoding(outputFilePath, encodingString, false);

    }

    public static void writeLinesListToFile(List<String> linesList, String outputFilePath) {
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    for (String line : linesList) {
		outputWriter.write(line + "\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);

	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }
}
