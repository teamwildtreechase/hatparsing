/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

public class ObjectDoubleValuePair<T> implements Comparable<ObjectDoubleValuePair<T>> {
    private T object;
    private double sortValue;

    public ObjectDoubleValuePair(T o, double i) {
	this.object = o;
	this.setDouble(i);
    }

    public T getObject() {
	return object;
    }

    public double getDouble() {
	return sortValue;
    }

    public void setDouble(double sortValue) {
	this.sortValue = sortValue;
    }
    
    public int compareTo(ObjectDoubleValuePair<T> o) {
	if (this.sortValue < o.sortValue) {
	    return -1;
	}
	if (this.sortValue > o.sortValue) {
	    return 1;
	}
	return 0;
    }

}
