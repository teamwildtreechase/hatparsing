/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

import java.util.ArrayList;
import util.Pair;

/**
 * The array of nodes a1 should contain in order all the nodes in a2 (only the operation of skipping one is allowed).
 * All the matched pairs of combinations are found. 
 * @author fsangati
 *
 */
public class AllOrderedStringExactSubsequences {

	public static final int maxComb = 1000000;
	
	Cell[][] cellMatrix;
	String[] a1, a2;	
	int xMax, yMax;
	int xLength, yLength;
	int lengthToMatch, lengthToMatchMinusOne;
	int totalMatched;
	
	protected class Cell {
		int x,y;
		boolean match, leftReachable, diagonalReachable;
		int maxSequenceLength;
		
		protected Cell(int x, int y, boolean match) {
			this.x = x;
			this.y = y;
			if (match) {
				this.match = true;
				totalMatched++;
			}
		}

		public void addAllSubSequences(
				ArrayList<Pair<String>> currentThread, ArrayList<ArrayList<Pair<String>>> result) {
			if (leftReachable) {
				if (diagonalReachable) {
					ArrayList<Pair<String>> newThread = new ArrayList<Pair<String>>(currentThread);
					cellMatrix[x][y+1].addAllSubSequences(newThread, result);
				}
				else cellMatrix[x][y+1].addAllSubSequences(currentThread, result);
			}
			if (match) {
				currentThread.add(0,new Pair<String>(a1[x],a2[y]));
				if (y==0) {
					result.add(currentThread);
					return;
				}
			}
			if (diagonalReachable) cellMatrix[x][y].addAllSubSequences(currentThread, result);			 			
		}
				
	}
	
	public AllOrderedStringExactSubsequences(String[] a1, String[] a2) {
		this.a1 = a1;
		this.a2 = a2;
		this.xLength = a1.length+1;
		this.yLength = a2.length+1;
		lengthToMatch = a2.length;
		lengthToMatchMinusOne = lengthToMatch - 1;
		this.xMax = a1.length;
		this.yMax = a2.length;
		this.cellMatrix = new Cell[xLength][yLength];
		for(int x=0; x<xLength; x++) this.cellMatrix[x][0] = new Cell(x-1, -1, false);
		for(int y=0; y<yLength; y++) this.cellMatrix[0][y] = new Cell(-1, y-1, false);
		for(int x=1; x<xLength; x++) {
			for(int y=1; y<yLength; y++) {
				this.cellMatrix[x][y] = new Cell(x-1, y-1, equal(a1[x-1],a2[y-1]));
			}
		}
	}
	
	public static boolean equal(String a, String b) {
		return a.charAt(0)==b.charAt(0);
	}
		
	public static ArrayList<ArrayList<Pair<String>>> getallExactSubsequences(
			String[] t1, String[] t2) {
		if (t1.length<t2.length) return null;
		AllOrderedStringExactSubsequences O = new AllOrderedStringExactSubsequences(t1, t2);
		return O.getExactSubsequences();
	}
	
	public ArrayList<ArrayList<Pair<String>>> getExactSubsequences() {
		if (totalMatched<lengthToMatch) return null;
		int firstindexXmatchFirstY = -1;
		int lastIndexXmatchLastY = -1;
		for(int x=1; x<xLength; x++) {
			if (cellMatrix[x][1].match) {
				firstindexXmatchFirstY = x;
				break;
			}			
		}
		for(int x=xMax; x>=yMax; x--) {
			if (cellMatrix[x][yMax].match) {
				lastIndexXmatchLastY = x;
				break;
			}			
		}
		if (lastIndexXmatchLastY-firstindexXmatchFirstY+1<lengthToMatch) return null;
		boolean previousReachable = false;
		for(int x=1; x<xLength; x++) {
			Cell c = cellMatrix[x][1];						
			if (c.match) {							
				c.maxSequenceLength = 1; 
				previousReachable = true;
			}
			else {
				if (previousReachable) {
					c.leftReachable = true;
					c.maxSequenceLength = 1; 
					previousReachable = true;
				}
			}
		}
		for(int y=2; y<yLength; y++) {
			for(int x=1; x<xLength; x++) {
				Cell c = cellMatrix[x][y];
				Cell cl = cellMatrix[x-1][y];				
				int maxSequenceLenghtFromL=cl.maxSequenceLength;
				int maxSequenceLenghtFromD=0;
				if (c.match) {
					Cell cd = cellMatrix[x-1][y-1];
					maxSequenceLenghtFromD = cd.maxSequenceLength+1;					
				}					
				int maxSL = Math.max(maxSequenceLenghtFromL, maxSequenceLenghtFromD);
				if (maxSL==0) continue;
				c.maxSequenceLength = maxSL;				
				if (maxSequenceLenghtFromL==maxSL) c.leftReachable = true;
				if (maxSequenceLenghtFromD==maxSL) c.diagonalReachable = true;					
			}
		}
		ArrayList<ArrayList<Pair<String>>> result = new ArrayList<ArrayList<Pair<String>>>();
		for(int x=1; x<xLength; x++) {
			Cell c = cellMatrix[x][yMax];
			if (!c.match) continue;
			if (c.maxSequenceLength != lengthToMatch) continue;
			c.addAllSubSequences(new ArrayList<Pair<String>>(), result);			
		}
		return result;
	}
	
	public static void main(String[] args) {		 
		String[] a1 = new String[]{"A1","B2","A3"};
		String[] a2 = new String[]{"A1","A2","A3"};
		ArrayList<ArrayList<Pair<String>>> result = getallExactSubsequences(a1,a2);
		System.out.println(result.size());
		for(ArrayList<Pair<String>> pList : result) {
			System.out.println(pList);
		}
	}

	
}
