/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class EmptyLineChecker 
{
	
	public static void checkFileHasEmptyLines(String sourceFileName)
	{
		BufferedReader sourceReader;
	
	
		System.out.println("Checking if file contains empty lines...");
		
			try 
			{
				sourceReader = new BufferedReader(new FileReader(sourceFileName));
				
				int lineNum = 1;
				String line;
				boolean containsEmptyLines = false;
				while( ((line = sourceReader.readLine()) != null))
				{
					if(line.matches("\\s*"))
					{
						System.out.println("File contains empty line: line " + lineNum);
						containsEmptyLines = true;
					}
					lineNum++;
				}
				
				if(!containsEmptyLines)
				{
					System.out.println("File contains no empty lines");	
				}
				
				
				
			}
			 catch (FileNotFoundException e) 
			 {
					e.printStackTrace();
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

	}
	
	public static void main(String[] args)
	{
		if(! (args.length == 1))
		{
			System.out.println("Wrong usage. \nUsage: java -jar checkForEmptyLines FILENAME");
			return;
		}
		
		String fileName = args[0];
		checkFileHasEmptyLines(fileName);
	}

}
