/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

public interface ObjectFactory 
{
	  public Object makeObject();
}