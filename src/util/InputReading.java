/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import alignment.Alignment;

public class InputReading 
{

	public static List<List<String>> getOneSenceteSentenceList(String sentence)
	
	{
		List<List<String>> result = new ArrayList<List<String>> ();
		List<String> sentenceWords = InputReading.constructSentenceWordsArrayList(sentence);
		
		// Add the generated word ArrayList of this sentence to the bigger structure
		// that contains the lists for all sentences
		result.add(sentenceWords);
		
		return result;
	}
	
	
	public static List<Alignment> getOneSentenceAlignmentsList(String alignmentString)  
	{
		List<Alignment> result = new ArrayList<Alignment> ();
		result.add(Alignment.createAlignment(alignmentString));
		return result;
	}
	
		
	
	public static List<List<String>> getSentences(File inputFile) throws Exception 
	{
		List<List<String>> result = new ArrayList<List<String>> ();
		
		
		if(!inputFile.canRead())
		{
			String fileName = inputFile.getAbsolutePath();
			throw new FileNotFoundException("File " + fileName + " cannot be read");
		}
		
		
		Scanner reader = FileUtil.getScanner(inputFile);
		
		String sentence = "";
	
		while(reader.hasNextLine()) 
		{
		
			sentence = reader.nextLine();
			
			// Construct and ArrayList of the words in the sentence
			List<String> sentenceWords = InputReading.constructSentenceWordsArrayList(sentence);
		
			// Add the generated word ArrayList of this sentence to the bigger structure
			// that contains the lists for all sentences
			result.add(sentenceWords);
							
		}
		reader.close();
	
			
		return result;
	}
	
	public static List<Alignment> getAlignments(File inputFile) throws Exception 
	{
		List<Alignment> result = new ArrayList<Alignment> ();
		
		Scanner reader; 
	
		assert(inputFile != null);
		
		if(!inputFile.canRead())
		{
			String fileName = inputFile.getAbsolutePath();
			throw new FileNotFoundException("File " + fileName + " cannot be read");
		}
		
		reader = FileUtil.getScanner(inputFile);
	
			
		String alignmentString = "";
		while(reader.hasNextLine()) 
		{
			alignmentString = reader.nextLine();
		
			// Construct the ArrayList of AlignmentPairs from this input String
			Alignment alignment = Alignment.createAlignment(alignmentString);
		
			// Add the generated word ArrayList of this sentence to the bigger structure
			// that contains the lists for all sentences
			result.add(alignment);
							
		}
		reader.close();
	
			
		return result;
	}
	
	

	
	public static ArrayList<String> constructSentenceWordsArrayList(String sentence)
	{
		// Split the line into words based on whitespace
		String[] words = sentence.split("\\s");
		
		ArrayList<String> sentenceWords = new ArrayList<String>();
		// add the words from the array to the ArrayList
		for(int i = 0; i < words.length; i++)
		{
			sentenceWords.add(words[i]);
		}
		
		return sentenceWords;
	}
	
	
	public static ArrayList<String> getWordsFromString(String s)
	{
		ArrayList<String> list = new ArrayList<String> ();
		
		String[] words = s.split(" ");
		
		for(int i = 0; i < words.length; i++)
		{
			list.add(words[i]);
		}
		
		return list;
	}
	
    public static int determineNumWordsString(String s)
    {
    	String[] parts = s.split(" ");
    	return parts.length;
    }
	
    
	
	
	
}
