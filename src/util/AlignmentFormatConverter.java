/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import alignment.AlignmentPair;

public class AlignmentFormatConverter 
{

	
	public static void perFormConversion(String sourceFileName, String targetFileName, boolean performIndicesMinusOneConversion, boolean flipAlignments)
	{
		BufferedReader sourceReader;
		BufferedWriter outputWriter;
	
	
		System.out.println("Perform conversion of alignments...");
		
		int lineIndex = 1;
		String alignmentString = null;
			try 
			{
				sourceReader = new BufferedReader(new FileReader(sourceFileName));
				outputWriter = new BufferedWriter(new FileWriter(targetFileName));
		
				while( ((alignmentString = sourceReader.readLine()) != null)) 
				{
					// We trim alignmentString for more robustness
					alignmentString = alignmentString.trim();
					
					// Split the line into words based on whitespace
					String[] wordAlignments = alignmentString.split("\\s");
					
					String outputString = "";
					
					// add the words from the array to the ArrayList
					for(int i = 0; i <  wordAlignments.length; i++)
					{
						String alignmentSubString =  wordAlignments[i];
						// Split the wordAlignment pair String further to get its parts
						String[] alignmentParts = alignmentSubString.split("-");
						// generate the actual AlignmentPair from the numbers extracted from the String
						AlignmentPair a = new AlignmentPair(Integer.parseInt(alignmentParts[0]),Integer.parseInt(alignmentParts[1]));
						
						if(performIndicesMinusOneConversion)
						{	
							AlignmentPair aConverted = convertAlignmentPair(a);
							a = aConverted;
						}
							
						if(a != null)
						{	
							// flip the target and source for our purposel
							
							if(flipAlignments)
							{	
								a = AlignmentPair.createInvertedAlignmentPair(a);
							}
							outputString += a.getSourcePos() + "-" + a.getTargetPos() + " ";
						}	
					}
					
					outputWriter.write(outputString +"\n");
					lineIndex++;
				}
			
				outputWriter.close();
				System.out.println("Finished...");
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			catch(NumberFormatException e)
			{
				System.out.println("Problem encountered at line " + lineIndex + 
						"\n: " + 	alignmentString);
				e.printStackTrace();
			}
	}	
	
	
	public static AlignmentPair convertAlignmentPair(AlignmentPair a)
	{
		
		if(a.getSourcePos() == 0)
		{
			// Alignment from null cept to something
			return null;
		}
		else
		{	
			AlignmentPair aNew = new AlignmentPair(a.getSourcePos() - 1, a.getTargetPos() - 1);
			return aNew;
		}	
	}
	
	
	public static void printUsage()
	{
		System.out.println("Usage: java -jar alignmentFormatConverter InputAlignmentsFile OutputAlignmentsFile PERFORM_MINUS_CONVERSION PERFORM_SWAP_CONVERSION");
		System.out.println("PERFORM_MINUS_CONVERSION : true / false");
		System.out.println("PERFORM_SWAP_CONVERSION : true / false");
		System.out.println("\n\n");
		System.out.println("PERFORM_SWAP_CONVERSION  : The source and target indices will be swapped to produce a new inverted alignment");
		System.out.println("PERFORM_MINUS_CONVERSION: Alignment pairs with 0 ont the source will be ignored"  + "" +
		"\n(these correspond to the empty cept mapping to something in the target in IBM-model notation)" +
		"\nAlignmentIndices will be decreased by 1 on both source and target side, to convert the " +		
		" IBM-model notation that goes from 1-m and 1-n into 0-(m-1), 0-(n-1)");
			
	}
	
	
	/**
	 * Read a boolean value "true" or "false". 
	 * @param argument
	 * @return : Pair2<Boolean, Integer> -   
	 * The first value of the returned  Pair2 is the Boolean value read. The second value of this pair is an integer 
	 * that will be -1 if there were problems in reading the value, i.e. it did not match 
	 * a legal value
	 */
	public static Pair2<Boolean, Integer> readBooleanArgument(String argument)
	{
		
		if(argument.equals("true"))
		{
			return new Pair2<Boolean, Integer>(true,1);
		}
		else if(argument.equals("false"))
		{
			return new Pair2<Boolean, Integer>(false,1);
		}
		else
		{
			return new Pair2<Boolean, Integer>(false,-1);
		}	
	}
	
	
	public static void main (String[] args)
	{
		if(args.length != 4)
		{
			printUsage();
			return;
			
		}
		
		boolean performMinusConversion, performSwapConversion;
		
		String sourceFileName = args[0];
		String targetFileName  = args[1];
		
	

		Pair2<Boolean,Integer> readValue = readBooleanArgument(args[2]);
		performMinusConversion = readValue.getFirst();
		
		if(readValue.getSecond() < 0) // check if a value could be read without errors
		{
			System.out.println("Error: Unrecognized option arguments, must be 'true' OR 'false'");
			return;
		}
	
		readValue = readBooleanArgument(args[3]);
		performSwapConversion = readValue.getFirst();
		
		if(readValue.getSecond() < 0)  // check if a value could be read without errors
		{
			System.out.println("Error: Unrecognized option arguments, must be 'true' OR 'false'");
			return;
		}
		
		AlignmentFormatConverter.perFormConversion(sourceFileName, targetFileName, performMinusConversion, performSwapConversion);
		
		
		/*
		String sourceFileName = "/home/gemaille/smallcorpus/test/AlignmentTest-EN-NL.txt";
		String targetFileName  = "/home/gemaille/smallcorpus/test/AlignmentTest-EN-NL-conveted.txt";
 		
		AlignmentFormatConverter.perFormConversion(sourceFileName, targetFileName, true, true); 
		
		String sourceFileName2 = "/home/gemaille/smallcorpus/test/AlignmentTest-NL-EN.txt";
		String targetFileName2  = "/home/gemaille/smallcorpus/test/AlignmentTest-NL-EN-conveted.txt";
 		
		AlnmentFormatConverter.perFormConversion(sourceFileName2, targetFileName2, true, false);
		*/ 
	}
	
}
				
			
	
	