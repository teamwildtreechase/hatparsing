package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

public class StreamUtil {

    /**
     * Write a stream to a file, while adding white space characters
     * (this is what you will usually want).
     *
     * @param stream
     * @param outputFilePath
     * @throws IOException
     */
    public static void writeStreamToFileWhileAddingNewlineCharacters(Stream<String> stream,
	    String outputFilePath) {

	/**
	 * Note: did not find a way yet to add the newline characters while only working 
	 * with Strings. 
	 * Java Streams cannot be copied, see: http://stackoverflow.com/questions/24474838/can-i-duplicate-a-stream-in-java-8
	 * This seemingly makes it impossible to get the first element of the Stream 
	 * and then process the rest.
	 * Getting an iterator itself also "consumes" the stream apparently,
	 * so once we use the iterator, we seem to be stuck with it. 
	 */
	BufferedWriter outputWriter = null;
	try {
	    outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
	    Iterator<String> streamIterator = stream.iterator();
	    if (streamIterator.hasNext()) {
		outputWriter.write(streamIterator.next());
		while (streamIterator.hasNext()) {
		    outputWriter.write("\n" + streamIterator.next());
		}
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    FileUtil.closeCloseableIfNotNull(outputWriter);
	}
    }

    /**
     * Write a stream to a file See also :
     * http://stackoverflow.com/questions/32054180/java-8-stream-to-file
     * 
     */
    public static void writeStreamToFile(Stream<String> stream, String outputFilePath) {
	try {
	    Files.write(Paths.get(outputFilePath), (Iterable<String>) stream::iterator);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
