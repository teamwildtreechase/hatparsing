/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Serialization 
{

	public static void serializeOBject(Serializable toSerialize,
			String serializationFileName) 
	{
		try 
		{
			FileOutputStream fileOutputStream = new FileOutputStream(serializationFileName);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(toSerialize);
			objectOutputStream.flush();
			objectOutputStream.close();
		}
		catch(Exception e) 
		{
			System.out.println("Exception during serialization: " + e);
			System.exit(0);
		} 
		
	}

	public static <T extends Object>  T unSerializeOBject(String serializationFileName) 
	{
		try
		{
			System.out.println("unserialize objec from file: " + serializationFileName);
			FileInputStream fileInputStream = new FileInputStream(serializationFileName);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			@SuppressWarnings("unchecked")
			T result = (T)objectInputStream.readObject();
			objectInputStream.close();
			return result;
		}
		catch(Exception e) 
		{
			throw new RuntimeException("Exception during deserialization of file " + serializationFileName +  " : " + e);
			
		}
	}

}
