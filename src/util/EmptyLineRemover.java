/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmptyLineRemover {

    public static List<String> readLinesAndStoreAsList(String inputFileName) {

	List<String> tempLinesList = new ArrayList<String>();

	try (Stream<String> stream = Files.lines(Paths.get(inputFileName))) {

	    // 1. filter line 3
	    // 2. convert it into a List
	    tempLinesList = stream.filter(line -> line != null).collect(Collectors.toList());

	} catch (IOException e) {
	    e.printStackTrace();
	}
	return tempLinesList;

    }

    private static Stream<String> getNonEmptyLines(Stream<String> lines) {
	return lines.filter(u -> !u.isEmpty());
    }

    public static void writeNonEmptyLines(List<String> lines, String outputFileName)
	    throws IOException {
	// BufferedWriter outputWriter = new BufferedWriter(new
	// FileWriter(outputFileName));

	/*
	 * for (Iterator<String> iterator = lines.iterator();
	 * iterator.hasNext();) { String lineToWrite = iterator.next();
	 * 
	 * if (!lineToWrite.isEmpty()) { if (iterator.hasNext()) { lineToWrite
	 * += "\n"; } outputWriter.write(lineToWrite); } }
	 * //outputWriter.close();
	 */

	// Java 8 Stream-based implementation
	Stream<String> nonEmptyLines = getNonEmptyLines(lines.stream());
	StreamUtil.writeStreamToFileWhileAddingNewlineCharacters(nonEmptyLines, outputFileName);
    }

    

    public static void removeEmptyLines(String inputFilePath, String outputFilePath) {
	try {
	    Stream<String> inputStream = Files.lines(Paths.get(inputFilePath));
	    Stream<String> nonEmptyLinesStream = getNonEmptyLines(inputStream);
	    StreamUtil.writeStreamToFileWhileAddingNewlineCharacters(nonEmptyLinesStream, outputFilePath);
	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    public static void removeEmptyLinesInPlace(String inputFileName) {

	System.out.println("Checking if file contains empty lines...");

	try {
	    List<String> tempLines = readLinesAndStoreAsList(inputFileName);
	    writeNonEmptyLines(tempLines, inputFileName);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}

    }

}
