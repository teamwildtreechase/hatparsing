/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

public class PrintProgress {
	
	private static int index = 0;
	private static int printEvery;
	
	public static void start(String startMessage) {
		start(startMessage, 1, 0);			
	}
	
	public static void start(String startMessage, int printEvery, int startIndex) {
		if (index != 0) end();
		index = startIndex;
		PrintProgress.printEvery = printEvery;
		System.out.print(startMessage + "        ");
	}
	
	public static void next() {
		index++;
		if (index % printEvery == 0) {
			int lastPrintedIndex = index==0 ? 0 : index - printEvery;
			int backSpaces = (lastPrintedIndex==0) ? 0 : Integer.toString(lastPrintedIndex).length();
			for(int i=0; i<backSpaces; i++) System.out.print("\b");
			System.out.print(index);
		}
	}
	
	public static int currentIndex() {
		return index;
	}
		
	public static void end() {
		end("...done");
	}
	
	public static void end(String endMessage) {
		System.out.print(" " + endMessage + "\n");
		index = 0;
	}
}
