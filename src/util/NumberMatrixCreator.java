/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class NumberMatrixCreator<E extends Number> {

	public abstract E createZero();

	public abstract E parseNumber(String numberString);

	public List<E> createNumberList(int length) {
		List<E> result = new ArrayList<E>();

		for (int i = 0; i < length; i++) {
			result.add((E) createZero());
		}

		return result;
	}

	public List<E> copyNumberList(List<E> toCopy) {
		List<E> result = new ArrayList<E>();

		for (int i = 0; i < toCopy.size(); i++) {
			result.add(toCopy.get(i));
		}

		return result;
	}

	public List<List<E>> createNumberMatrix(int noRows, int noColumns) {
		List<List<E>> result = new ArrayList<List<E>>();

		for (int i = 0; i < noRows; i++) {
			result.add((List<E>) createNumberList(noColumns));
		}

		return result;
	}

	public List<List<E>> readNumberMatrix(String fileName, boolean ignooreFirstRow, boolean ignoreFirstColumn) {
		List<List<E>> result = new ArrayList<List<E>>();

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
			String line;

			if (ignooreFirstRow) {
				fileReader.readLine();
			}

			while ((line = fileReader.readLine()) != null) {
				String[] parts = line.split(",");
				List<E> row = new ArrayList<E>();

				int index = 0;
				if (ignoreFirstColumn) {
					index = 1;
				}
				while (index < parts.length) {
					String part = parts[index];
					row.add(parseNumber(part));
					index++;
				}
				result.add(row);
			}
			fileReader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		// System.out.println("Read number matrix");
		return result;
	}

	public List<List<E>> cloneNumberMatrix(List<List<E>> toCopy) {
		int noRows = toCopy.size();

		List<List<E>> result = new ArrayList<List<E>>();

		for (int i = 0; i < noRows; i++) {
			result.add(copyNumberList(toCopy.get(i)));
		}

		return result;
	}

}
