/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

import java.util.Date;

public class Timer {
	static long startTimerValue;
	
	public static void timerStart() {	
		startTimerValue = new Date().getTime();
	}
	
	public static long timerStop() {
		return new Date().getTime() - startTimerValue;
	}

}
