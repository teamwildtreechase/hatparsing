/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import alignmentStatistics.HATProperties;

public class FilePartMerger 
{

	
	public static void produceMergedPartsFileHatProperties(String baseFileName, int numParts)
	{
		try 
		{
			System.out.println(">>> FilePartMerger.produceMergedPartsFile numThreads: " + numParts);
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(baseFileName));
			
			// Write the properties header to the file
			outputWriter.write(HATProperties.propertiesHeader());
			
			produceMergedPartsFile(outputWriter, baseFileName, numParts,true);
		} 
		catch (IOException e) 
		{
			
			e.printStackTrace();
		}
	}
	

	
	public static void produceMergedPartsFile(String outputFileName, int numParts)
	{
		try 
		{
			System.out.println(">>> FilePartMerger.produceMergedPartsFile numThreads: " + numParts);
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
			produceMergedPartsFile(outputWriter, outputFileName, numParts,false);
		} 
		catch (IOException e) 
		{
			
			e.printStackTrace();
		}
	}
	
	
	public static void produceMergedPartsFile(BufferedWriter outputWriter, String baseFileName, int numParts, boolean skipFirstLine)
	{
		try 
		{
			System.out.println(">>> FilePartMerger.produceMergedPartsFile numThreads: " + numParts);
			
			
			for(int partNum = 1; partNum <= numParts; partNum++)
			{
				 // Name of the temporary file that will store the properties of the alignment triples processed by the thread
				String threadPartFileName = baseFileName + "Part" + partNum + ".txt";
				//System.out.println("threadPartFileName: " + threadPartFileName);
				BufferedReader partFileReader = new BufferedReader(new FileReader(threadPartFileName ));
				addReaderLines(partFileReader, outputWriter,skipFirstLine,(partNum == 1));
				partFileReader.close();
			}
			outputWriter.close();
			
			// Delete the part properties files
			deletePartFiles(baseFileName, numParts);
		
		} 
		catch (IOException e) 
		{
			
			e.printStackTrace();
		}
	}
	
	
	private static void addReaderLines(BufferedReader reader, BufferedWriter writer, boolean skipFirstLine, boolean isFirstPartReader)
	{
		try 
		{
			String line;
			
			if(skipFirstLine)
			{	
				// Read the first line, containing headers which will be skipped
				 reader.readLine();
			}	
			
			boolean isFirstLineWritten = isFirstPartReader;
			// Read the data from the file
			while((line = reader.readLine()) != null)
			{
				// Write only non empty lines
				if(!line.equals(""))
				{	
				    	if(!isFirstLineWritten)
				    	{
				    	    writer.write("\n");
				    	}
				    	else
				    	{
				    	    // If it is the first line written, we don't write a newline in front
				    	    isFirstLineWritten = false;
				    	}
					writer.write(line);
				}	
			}
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void deletePartFiles(String baseFileName, int numParts)
	{
		for(int partNum = 1; partNum <= numParts; partNum++)
		{
			 // Name of the temporary file that will store the properties of the alignment triples processed by the thread
			String partFileName = baseFileName + "Part" + partNum + ".txt";
			System.out.println("Deleting file:" + partFileName);
			Utility.deleteFile(partFileName);
		}
	}
	
	
}
