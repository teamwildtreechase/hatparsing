/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import javax.swing.text.JTextComponent;

public class ConfigCacherEntry 
{
	private String configValueDefault;
	private JTextComponent configValueGuiInput;
	
	public ConfigCacherEntry(String configValueName, String configValueDefault, JTextComponent configValueGuiInput)
	{
		this.configValueDefault = configValueDefault;
		this.configValueGuiInput =configValueGuiInput;
	}
	
	public String getTextFieldValue()
	{
		return this.configValueGuiInput.getText();
	}
	
	public void setTextField(String value)
	{
		this.configValueGuiInput.setText(value);
	}
	
	public String getDefaultValue()
	{
		return configValueDefault;
	}
}