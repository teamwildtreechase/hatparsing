/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

public class IntegerMatrixCreator extends NumberMatrixCreator<Integer>
{

	@Override
	public Integer createZero() 
	{
		return ((int)0);
	}

	@Override
	public Integer parseNumber(String numberString) 
	{
		return Integer.parseInt(numberString);
	}

}
