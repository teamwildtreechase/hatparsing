/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

public class SpecialCharacters {
	public static final String LeftAngleBracket = "\u3008";
	public static final String RightAngleBracket = "\u3009";// " ; "\u9E9F"

	public static final String LeftSquareBracket = "[";
	public static final String RightSquareBracket = "]";// " ; "\u9E9F"
}
