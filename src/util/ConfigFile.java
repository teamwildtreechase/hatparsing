/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;

/**
 * Stores the configuration as specified in the file "configFile".
 * 
 * @author Gideon Maillette de Buy Wenniger
 * 
 */
public class ConfigFile {
	private final Hashtable<String, String> configValues;

	private static Hashtable<String, String> getConfigValues(List<String> configFileLines) {
		Hashtable<String, String> configValues = new Hashtable<String, String>();

		// Read config file
		for (String line : configFileLines) {
			line = line.trim();
			if (!(line.startsWith("%")) && !(line.equals(""))) {

				int firstIndexIsSign = line.indexOf("=");

				if (firstIndexIsSign < 0) {
					throw new RuntimeException("Error : incorrect config file line:" + line);
				}

				//System.out.println("line: " + line);

				// extract the parameter name and value and remove whitespace
				String parameterName = line.substring(0, firstIndexIsSign).trim();
				String parameterValue = line.substring(firstIndexIsSign + 1, line.length()).trim();
				// Add the value to the lookup table
				configValues.put(parameterName, parameterValue);

			}
		}
		return configValues;
	}

	public ConfigFile(String inputFile) throws FileNotFoundException {

		List<String> configFileLines = FileUtil.getSentences(new File(inputFile), true);
		Hashtable<String, String> configValues = getConfigValues(configFileLines);
		this.configValues = configValues;

	}

	public ConfigFile(List<String> configFileLines) {
		Hashtable<String, String> configValues = getConfigValues(configFileLines);
		this.configValues = configValues;
	}

	public boolean hasValue(String parameterName) {
		return configValues.containsKey(parameterName);
	}

	public String getValue(String parameterName) {
		return configValues.get(parameterName);
	}

	
	private ArrayList<String> getMultiValueParmeterAsListPrivate(String parameterName, boolean doPresenceCheck) {
		ArrayList<String> result = null;

		String value;
		if(doPresenceCheck){    
		    value = getValueWithPresenceCheck(parameterName);
		}
		else{
		    value = configValues.get(parameterName);
		}
		    

		System.out.println(" getMultiValueParmeterAsList - value" + value);

		if (value != null) {
			result = new ArrayList<String>();

			// Split the value on the comma into the sub-values (i.e. elements of the list)
			String[] values = value.split(",");

			for (int i = 0; i < values.length; i++) {
				// Remove any heading and trailing whitespace
				result.add(values[i].trim());
			}

		}

		return result;
	}
	
	/**
	 * Method that returns the values of a multi-valued parameter as a list (Warning: no consistency checks implemented yet)
	 * 
	 * @param parameterName
	 * @return : ArrayList<String> of the values for this parameter
	 */
	public ArrayList<String> getMultiValueParmeterAsList(String parameterName) {
		return getMultiValueParmeterAsListPrivate(parameterName, false);
	}
	
	
	/**
	 * Method that returns the values of a multi-valued parameter as a list (Warning: no consistency checks implemented yet)
	 * Perform presence check on the parameter first
	 * @param parameterName
	 * @return : ArrayList<String> of the values for this parameter
	 */
	public ArrayList<String> getMultiValueParmeterAsListWithPresenceCheck(String parameterName) {
		return getMultiValueParmeterAsListPrivate(parameterName, true);
	}


	public String getStringIfPresentOrReturnDefault(String parameterName, String defaultValue) {
		if (hasValue(parameterName)) {
			return getValue(parameterName);
		} else {
			return defaultValue;
		}
	}

	public int getIntIfPresentOrReturnDefault(String parameterName, int defaultValue) {
		if (hasValue(parameterName)) {
			return Integer.parseInt(getValue(parameterName));
		} else {
			return defaultValue;
		}
	}

	public boolean getBooleanIfPresentOrReturnDefault(String parameterName, boolean defaultValue) {
		if (hasValue(parameterName)) {
			return Boolean.parseBoolean(getValue(parameterName));
		} else {
			return defaultValue;
		}
	}

	public String getCorpusLocation() {
		return this.getValue("corpusLocation");
	}

	public String getSourceFilePath() {
		return this.getCorpusLocation() + this.getValue("sourceFileName");
	}

	public String getTargetFilePath() {
		return this.getCorpusLocation() + this.getValue("targetFileName");
	}

	public String getAlignmentsFilePath() {
		return this.getCorpusLocation() + this.getValue("alignmentsFileName");
	}

	public String getValueWithPresenceCheck(String parameterName) {
		if (hasValue(parameterName)) {
			return getValue(parameterName);
		}

		throw new RuntimeException("Required parameter" + " : \"" + parameterName + "\" unspecified in config file");
	}

	public boolean getBooleanWithPresenceCheck(String parameterName) {
		return Boolean.parseBoolean(getValueWithPresenceCheck(parameterName));
	}
}
