/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Sorting 
{

	public static <E extends Comparable<E>>  List<E> getSetAsSortedListAscending(Set<E > set)
	{
		List<E> result = new ArrayList<E>(set);
		Collections.sort(result);
		return result;
	}
	
	public static <E extends Comparable<E>>  List<E> getSetAsSortedListDescending(Set<E > set)
	{
		List<E> result = new ArrayList<E>(set);
		Collections.sort(result,Collections.reverseOrder());
		return result;
	}
}
