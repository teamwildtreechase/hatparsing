/*
 * Copyright (c) <2010> <Gideon Maillette de Buy Wenniger, Federico Sangati>.  All Rights Reserved.
 */
package util;


public class NAirs {
	
	private static int absoluteMax = 15;
	
	int max;
	int[][][][] nair;
	boolean noDiscontinuous;
	
	public NAirs(int max) {
		this(max, false);
	}
	
	public NAirs(int max, boolean noDiscontinuous) {
		this.noDiscontinuous = noDiscontinuous;
		if (max > absoluteMax) max = absoluteMax;
		this.max = max;		
		initNair();
	}
	
	private void initNair() {
		//  binomial(int c, int n) : | ways of choosing n elements from c |
		// c >= n		
		nair = new int[max+1][max+1][][];
		fillNairWithNulls();
		if (noDiscontinuous) {
			for(int c=1; c<=max; c++) {
				for(int n=1; n<=c; n++) {
					nair[c][n] = Utility.n_air(c, n);
				}
			}	
		}
		else {
			for(int c=1; c<=max; c++) {
				for(int n=1; n<=c; n++) {
					nair[c][n] = Utility.n_air_continuous(c, n);
				}
			}
		}		
	}
	
	private void fillNairWithNulls() {
		for(int i=0; i<=max; i++) {
			for(int j=0; j<=max; j++) {
				nair[i][j] = null;
			}
		}
	}
	
	public int[][] get(int c, int n) {		
		return nair[c][n];
	}
	
	public static void main(String[] args) {
		NAirs N = new NAirs(10);
		for(int c=0; c<=10; c++) {
			for(int n=0; n<=c; n++) {
				System.out.println("Nair\t" + c + "\t" + n);
				Utility.printIntArray(N.get(c, n));
				System.out.println();
			}			
		}
	}
	
}
