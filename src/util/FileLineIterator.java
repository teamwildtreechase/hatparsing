/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;



public class FileLineIterator 
{
	String fileName;
	BufferedReader reader;
	String bufferedLine;
	
	public  FileLineIterator(String fileName)
	{
		try 
		{
			reader = new BufferedReader(new FileReader(fileName));
			// read first line and store in buffer
			bufferedLine = reader.readLine();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public boolean hasNextLine()
	{
		return (bufferedLine != null);
	}
	
	public String nextLine()
	{
		String result = null;
		
		try 
		{
			// store bufferedline
			result = bufferedLine;
			// read new line
			bufferedLine = reader.readLine();
			// return previously buffered line
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return result;
	}	
	


}
