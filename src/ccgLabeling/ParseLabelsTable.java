/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import tsg.TSNodeLabel;
import util.Pair;
import util.Pair2;

public class ParseLabelsTable 
{
	private final Hashtable<Pair<Integer>, List<String>> indexedParseLabels;
	private final int sourceLength;

	public ParseLabelsTable(Hashtable<Pair<Integer>, List<String>> indexedParseLabels, int sourceLength)
	{
		this.indexedParseLabels = indexedParseLabels;
		this.sourceLength = sourceLength;
	}

	public static ParseLabelsTable createParseLabelsTable(TSNodeLabel tree)
	{
		Pair2<Hashtable<Pair<Integer>, List<String>>, Integer> pair = generateIndexedParseLabels(tree);
		int sourceLength = pair.getSecond();
		Hashtable<Pair<Integer>, List<String>> indexedParseLabels = pair.getFirst();
	
		return new ParseLabelsTable(indexedParseLabels, sourceLength);
	}
	
	
	/**
	 *  Method that takes a parse (In Standford Parser Format), and returns a Hashtable
	 *  with keys containing integer pairs corresponding to source ranges and as values 
	 *  the parse labels associated with these ranges 
	 * @param parse
	 * @return
	 */
	private static Pair2<Hashtable<Pair<Integer>, List<String>>, Integer> generateIndexedParseLabels(TSNodeLabel tree)
	{
		Hashtable<Pair<Integer>, List<String>> indexedParseLabels = new Hashtable<Pair<Integer>, List<String>>();
		int sourceLength = 0;
		
		
		sourceLength = tree.countTerminalNodes();
		// First we compute the source spans for all nodes in the tree, which is 
		// not done by default
		tree.computeSourceSpans();
		List<TSNodeLabel> nonLexicalNodes = tree.collectNonLexicalNodes();
		
		
		for(TSNodeLabel node : nonLexicalNodes)
		{
			String label = node.label();
			Pair<Integer> sourceSpan = node.getSourceSpan();
			
			if(indexedParseLabels.containsKey(sourceSpan))
			{
				indexedParseLabels.get(sourceSpan).add(label);
			}
			else
			{
				List<String> labels = new ArrayList<String>();
				labels.add(label);
				indexedParseLabels.put(sourceSpan,labels);
			}
		}
		
		 
		
		return new Pair2<Hashtable<Pair<Integer>, List<String>>, Integer>(indexedParseLabels, sourceLength);
	}
	
	public List<String> getLabels(int sourceMin, int sourceMax)
	{
		Pair<Integer> sourceRange = new  Pair<Integer>(sourceMin,sourceMax);
		return this.indexedParseLabels.get(sourceRange);
	}
	
	public List<String> getLabels(Pair<Integer> sourceRange)
	{
		return this.indexedParseLabels.get(sourceRange);
	}
	
	public String getTag(int pos)
	{
		List<String> labels = getLabels(pos,pos);
		if((labels != null) && (labels.size() > 0) )
		{	
			return labels.get(labels.size() - 1);
		}
		
		return null;	
	}
	
	public int getSourceLength()
	{
		return sourceLength;
	}
	
	public boolean containsKey(Pair<Integer> sourceRange)
	{
		return this.indexedParseLabels.containsKey(sourceRange);
	}
	
	public void printIndexedParseLabels()
	{
		for(Entry<Pair<Integer>, List<String>> entry  : this.indexedParseLabels.entrySet())
		{
			System.out.println("\n\n>>Key: " + entry.getKey());
			System.out.print(">>values: \t");
			for(String label : entry.getValue())
			{
				System.out.print(label + "\t");
			}
		}
	}
	
}
