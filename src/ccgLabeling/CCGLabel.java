/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;


/**
 *  A label of Categorial Grammar Format with extensions
 * @author gemaille
 *
 */
public class CCGLabel 
{
	public enum CCGLabelType{ExactMatchLabel,TwoConcatenationLabel,MinusLabel,ThreeConcatenationLabel,TagsLabel};
	private String label;
	CCGLabelType labelType;
	
	private CCGLabel(String label, CCGLabel.CCGLabelType labelType)
	{
		this.label = label;
		this.labelType = labelType;
	}

	public static CCGLabel createCCGLabel(String label,  CCGLabel.CCGLabelType labelType)
	{
		return new  CCGLabel(label, labelType);
	}
	
	public String getLabel()
	{
		return label;
	}
	
	public static String getCCGLabelProperty(String category)
	{
		return "CCG" + category + "Label";
	}
	
}
