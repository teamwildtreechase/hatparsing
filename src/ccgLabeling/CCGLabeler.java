/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import tsg.TSNodeLabel;
import extended_bitg.EbitgInferenceTargetProperties;
import util.Span;

public class CCGLabeler 
{
	private final CCGLabelChart sourceLabelChart;
	private final CCGLabelChart targetLabelChart;
	
	private CCGLabeler(CCGLabelChart sourceLabelChart, CCGLabelChart targetLabelChart)
	{
		this.sourceLabelChart = sourceLabelChart;
		this.targetLabelChart = targetLabelChart;
	}
	
	public static CCGLabeler createSourceCCGLabeler(TSNodeLabel sourceParse, UnaryCategoryHandlerType unaryCategoryHandlerType,boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		CCGLabelChart sourceLabelChart = CCGLabelChart.createLabelChart(sourceParse,unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		return new CCGLabeler(sourceLabelChart,null);
	}
	
	public static  CCGLabeler createTargetCCGLabeler(TSNodeLabel targetParse, UnaryCategoryHandlerType unaryCategoryHandlerType,boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		CCGLabelChart targetLabelChart = CCGLabelChart.createLabelChart(targetParse,unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		return new CCGLabeler(null,targetLabelChart);
	}
	
	public static CCGLabeler createSourceAndTargetCCGLabeler(TSNodeLabel sourceParse, TSNodeLabel targetParse, UnaryCategoryHandlerType unaryCategoryHandlerType,boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		CCGLabelChart sourceLabelChart = CCGLabelChart.createLabelChart(sourceParse,unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		CCGLabelChart targetLabelChart = CCGLabelChart.createLabelChart(targetParse,unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		return new CCGLabeler(sourceLabelChart,targetLabelChart);
	}

	
	public static  CCGLabeler createEmptyCCGLabeler()
	{
		return new CCGLabeler(null,null);
	}
	
	
	private CCGLabels createCCGLabels(Span sourceSpan, Span targetSpan)
	{
		CCGLabel sourceLabel = null;
		CCGLabel targetLabel = null;
		
		if(hasSourceLabelChart())
		{
			sourceLabel = this.sourceLabelChart.getCCGLabel(sourceSpan);
		}
		
		if(hasTargetLabelChart())
		{
			targetLabel = this.targetLabelChart.getCCGLabel(targetSpan);
		}
		
		return new CCGLabels(sourceLabel,targetLabel);
	}
	
	public boolean hasSourceLabelChart()
	{
		return this.sourceLabelChart != null;
	}
	
	public boolean hasTargetLabelChart()
	{
		return this.targetLabelChart != null;
	}
	
	public boolean hasLabelChart()
	{
		return hasSourceLabelChart() || hasTargetLabelChart();
	}

	public CCGLabels computeCCGLabels(Span nullExtendedSourceSpan, EbitgInferenceTargetProperties nullExtendedTargetProperties)
	{
		return createCCGLabels(nullExtendedSourceSpan, nullExtendedTargetProperties.getTargetSpan()); 
	}
	
	
}	
