/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import java.util.List;

import util.Pair;

public class LabelGenerator {
	public static enum UnaryCategoryHandlerType {
		BOTTOM, TOP, ALL
	};

	private final ParseLabelsTable parseLabelsTable;
	private final TagLabelGenerator tagLabelGenerator;
	private final UnaryCategoryHandlerType unaryCategoryHandlerType;
	private final boolean allowDoublePlus;

	private LabelGenerator(ParseLabelsTable parseLabelsTable, TagLabelGenerator tagLabelGenerator, UnaryCategoryHandlerType unaryCategoryHandlerType,
			boolean allowDoublePlus) {
		this.parseLabelsTable = parseLabelsTable;
		this.tagLabelGenerator = tagLabelGenerator;
		this.unaryCategoryHandlerType = unaryCategoryHandlerType;
		this.allowDoublePlus = allowDoublePlus;
	}

	public static LabelGenerator createLabelGenerator(ParseLabelsTable parseLabelsTable, UnaryCategoryHandlerType unaryCategoryHandlerType,
			boolean allowDoublePlus, boolean allowPosTagLabelFallback) {
		TagLabelGenerator tagLabelGenerator;

		if (allowPosTagLabelFallback) {
			tagLabelGenerator = SimpleTagLabelGenerator.createTagLabelGenerator(parseLabelsTable);
		} else {
			tagLabelGenerator = FailTagLabelGenerator.createFailTagLabelGenerator();
		}
		return new LabelGenerator(parseLabelsTable, tagLabelGenerator, unaryCategoryHandlerType, allowDoublePlus);
	}

	public CCGLabel generateLabel(Pair<Integer> sourceRange) {

		CCGLabel label = null;

		// First try to find an exactly matching label, i.e. matching exactly
		// the source range
		label = this.getExaxtMatchLabel(sourceRange);
		if (label != null) {
			return label;
		}

		int rangeMin = sourceRange.getFirst();
		int rangeMax = sourceRange.getSecond();

		// As the next best option try to find a covering label by concatenation
		// of two sub-labels
		label = this.getTwoConcatenatedLabel(rangeMin, rangeMax);
		if (label != null) {
			return label;
		}

		// As the next best option try to find a minus label
		label = this.getMinusLabel(rangeMin, rangeMax);
		if (label != null) {
			return label;
		}

		// As the next best option try to combine three sub labels to cover the
		// span
		if (allowDoublePlus) {
			label = this.getThreePartsLabel(rangeMin, rangeMax);
			if (label != null) {
				return label;
			}
		}

		// Finally if all other options failed, build a label from the boundary
		// and intermediate tags over the span rangeMin - rangeMax, using
		// tagLabelGenerator
		label = this.tagLabelGenerator.getTagsLabel(rangeMin, rangeMax);
		if (label != null) {
			return label;
		}

		return null;
	}

	public CCGLabel getExaxtMatchLabel(Pair<Integer> sourceRange) {
		if (parseLabelsTable.containsKey(sourceRange)) {
			// System.out.println("A label exactly matching the source range was found");
			List<String> labels = parseLabelsTable.getLabels(sourceRange);
			// Combine the list of labels into one label and return that
			return CCGLabel.createCCGLabel(combinedLabelString(labels), CCGLabel.CCGLabelType.ExactMatchLabel);
		} else {
			return null;
		}
	}

	public CCGLabel getTwoConcatenatedLabel(int rangeMin, int rangeMax) {

		CCGLabel result = null;

		for (int splitPoint = rangeMin + 1; splitPoint <= rangeMax; splitPoint++) {
			List<String> leftLabels = this.parseLabelsTable.getLabels(new Pair<Integer>(rangeMin, splitPoint - 1));
			List<String> rightLabels = this.parseLabelsTable.getLabels(new Pair<Integer>(splitPoint, rangeMax));

			if ((leftLabels != null) && (rightLabels != null)) {
				return CCGLabel.createCCGLabel((combinedLabelString(leftLabels) + "+" + combinedLabelString(rightLabels)),
						CCGLabel.CCGLabelType.TwoConcatenationLabel);
			}
		}

		return result;

	}

	/**
	 * Method that generates a label of the form
	 * "SuperCategory / RightSubcategory" or "LeftSubstracted \ SuperCategory"
	 * where SuperCategory is the smallest super category that enables this
	 * devision in a bigger super category minus some Left- or RightSubstracted
	 * category.
	 * 
	 * @param sourceRange
	 *            The span in the source for which a label needs to be found
	 * @return
	 */
	public CCGLabel getMinusLabel(int rangeMin, int rangeMax) {
		int spanLength = (rangeMax - rangeMin) + 1;

		/*
		 * Preference is given to not adding and subtracting more than necessary
		 * - giving preference to labels found closer to the leaves of the tree 
		 * - as stated in the original paper
		 *"The syntax augmented MT (SAMT) system for the shared task in the 2007 ACL Workshop on Statistical Machine Translation"
		 */
		for (int superCatSize = spanLength + 1; superCatSize <= this.getChartLength(); superCatSize++) {
			CCGLabel label = createRightMinusLabel(rangeMin, rangeMax, superCatSize);
			if (label != null) {
				return label;
			}

			label = createLeftMinusLabel(rangeMin, rangeMax, superCatSize);
			if (label != null) {
				return label;
			}

		}

		return null;
	}

	/**
	 * Create a right minus label
	 * 
	 * @param rangeMin
	 * @param rangeMax
	 * @param superCatSize
	 * @return The right minus label or null if it could not be constructed
	 */
	private CCGLabel createRightMinusLabel(int rangeMin, int rangeMax, int superCatSize) {
		int superCatMax = rangeMin + superCatSize - 1;
		List<String> superCategoryLabels = this.parseLabelsTable.getLabels(rangeMin, superCatMax);
		List<String> minusCategoryLabels = this.parseLabelsTable.getLabels(rangeMax + 1, superCatMax);

		if ((superCategoryLabels != null) && (minusCategoryLabels != null)) {
			return CCGLabel.createCCGLabel(combinedLabelString(superCategoryLabels) + "/" + combinedLabelString(minusCategoryLabels),
					CCGLabel.CCGLabelType.MinusLabel);
		}

		return null;
	}

	/**
	 * 
	 * @param rangeMin
	 * @param rangeMax
	 * @param superCatSize
	 * @return The left minus label or null if it could not be constructed
	 */
	private CCGLabel createLeftMinusLabel(int rangeMin, int rangeMax, int superCatSize) {
		int superCatMin = rangeMax - superCatSize + 1;
		List<String> superCategoryLabels = this.parseLabelsTable.getLabels(superCatMin, rangeMax);
		List<String> minusCategoryLabels = this.parseLabelsTable.getLabels(superCatMin, rangeMin - 1);

		if ((superCategoryLabels != null) && (minusCategoryLabels != null)) {
			return CCGLabel.createCCGLabel(combinedLabelString(minusCategoryLabels) + "\\" + combinedLabelString(superCategoryLabels),
					CCGLabel.CCGLabelType.MinusLabel);
		}

		return null;
	}

	/**
	 * 
	 * @param rangeMin
	 *            The minimum of the range
	 * @param rangeMax
	 *            The maximum of the range
	 * @return A label consisting of four parts, by combining the labels of a
	 *         valid partition of rangeMin - rangeMax in sub-categories that
	 *         exist in the original parse Returns null if no label of three
	 *         parts could be found for the range
	 */
	public CCGLabel getThreePartsLabel(int rangeMin, int rangeMax) {
		int spanLength = (rangeMax - rangeMin) + 1;

		for (int leftCatSize = 1; leftCatSize < spanLength - 1; leftCatSize++) {
			for (int middleCatSize = 1; middleCatSize < spanLength - leftCatSize; middleCatSize++) {
				int leftMax = rangeMin + leftCatSize - 1;
				Pair<Integer> leftRange = new Pair<Integer>(rangeMin, leftMax);
				int middleMax = leftMax + middleCatSize;
				Pair<Integer> middleRange = new Pair<Integer>(leftMax + 1, middleMax);
				Pair<Integer> rightRange = new Pair<Integer>(middleMax + 1, rangeMax);

				List<String> leftLabels = this.parseLabelsTable.getLabels(leftRange);
				List<String> middleLabels = this.parseLabelsTable.getLabels(middleRange);
				List<String> rightLabels = this.parseLabelsTable.getLabels(rightRange);

				if ((leftLabels != null) && (middleLabels != null) && (rightLabels != null)) {
					return CCGLabel.createCCGLabel(combinedLabelString(leftLabels) + "+" + combinedLabelString(middleLabels) + "+"
							+ combinedLabelString(rightLabels), CCGLabel.CCGLabelType.ThreeConcatenationLabel);
				}

			}
		}

		// Failed to find a label consisting of three parts
		return null;
	}

	public int getChartLength() {
		return this.parseLabelsTable.getSourceLength();
	}

	/**
	 * @param labels
	 *            A list of Labels to be combined
	 * @return A string that combines the labels in the list, concatenating them
	 *         with a ":" in between
	 */
	public String combinedLabelString(List<String> labels) {
		switch (this.unaryCategoryHandlerType) {
		case ALL:
			return allCombinedLabelsString(labels);
		case TOP:
			return topLabelString(labels);
		case BOTTOM:
			return bottomLabelString(labels);
		default:
			throw new RuntimeException("Error : unrecognized unaryCategoryHandler type");
		}
	}

	private String allCombinedLabelsString(List<String> labels) {
		String result = "";

		if (labels.size() > 0) {
			result = labels.get(0);
		}

		for (int i = 1; i < labels.size(); i++) {
			result += ":" + labels.get(i);
		}
		return result;
	}

	private String topLabelString(List<String> labels) {
		String result = "";

		if (labels.size() > 0) {
			result = labels.get(0);
		}

		return result;
	}

	private String bottomLabelString(List<String> labels) {
		String result = "";

		if (labels.size() > 0) {
			result = labels.get(labels.size() - 1);
		}

		return result;
	}

}
