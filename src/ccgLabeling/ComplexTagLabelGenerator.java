/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class ComplexTagLabelGenerator implements TagLabelGenerator
{
	// Specification of important tags that are used to build up the span 
	// tag labels from the boundary tags and the tags in between
	private static final String[] importantTagsArray = new String[]
	 {"PTKVZ","KOUI","KOUS","PRELS","PRELAT","PWAV","PRELAT","PWAV","PWS"};
	
	private static final String[] boundaryImportantTagsArray = new String[]
	 {"PTKVZ","KOUI","KOUS","PRELS","PRELAT","PWAV","PRELAT","PWAV","PWS","KON"};
	                                                  	
	private static final String[] importantTagFragmentsArray = new String[]
	 {"FIN"};
	
	private static final String[] boundaryImportantTagFragmentsArray = new String[]
	 {"FIN", "$"};
	
	private static final String[] nounArray = new String[] 
	{"NN", "NE", "PPER"};
	
	private final Set<String> importantTags;
	private final Set<String> importantTagFragments;
	private final Set<String> boundaryImportantTags;
	private final Set<String> boundaryImportantTagFragments;
	private final Set<String> nouns;
	
	private final ParseLabelsTable parseLabelsTable;
	
	public ComplexTagLabelGenerator(Set<String> importantTags,  Set<String> importantTagFragments,Set<String> boundaryImportantTags, Set<String> boundaryImportantTagFragments , ParseLabelsTable parseLabelsTable, Set<String> nouns)
	{
		this.importantTags = importantTags;
		this.importantTagFragments = importantTagFragments;
		this.boundaryImportantTags = boundaryImportantTags;
		this.boundaryImportantTagFragments = boundaryImportantTagFragments;
		this.parseLabelsTable = parseLabelsTable;
		this.nouns = nouns;
	}
	
	public static ComplexTagLabelGenerator createTagLabelGenerator(ParseLabelsTable parseLabelsTable)
	{
		Set<String> importantTags = new HashSet<String>();
		Set<String> importantTagFragments = new HashSet<String>();
		Set<String> boundaryImportantTags = new HashSet<String>();
		Set<String> boundaryImportantTagFragments = new HashSet<String>();
		Set<String> nouns = new HashSet<String>();
		
		importantTags.addAll(Arrays.asList(ComplexTagLabelGenerator.importantTagsArray));
		importantTagFragments.addAll(Arrays.asList(ComplexTagLabelGenerator.importantTagFragmentsArray));
		boundaryImportantTags.addAll(Arrays.asList(ComplexTagLabelGenerator.boundaryImportantTagsArray));
		boundaryImportantTagFragments.addAll(Arrays.asList(ComplexTagLabelGenerator.boundaryImportantTagFragmentsArray));
		nouns.addAll(Arrays.asList(ComplexTagLabelGenerator.nounArray));
		
		return new ComplexTagLabelGenerator(importantTags, importantTagFragments, boundaryImportantTags, boundaryImportantTagFragments, parseLabelsTable, nouns);
	}
	
	
	private String getBoundaryTag(int position)
	{
		String tag =  this.geLabelForPosition(position, this.boundaryImportantTags, this.boundaryImportantTagFragments);
		if(tag != null)
		{
			return tag;
		}
		
		// Failed to find a boundaryImportantTag or boundaryImportantTagFragment for this position
		return "Y";
	}
	
	private String getIntermediateTag(int position)
	{
		return this.geLabelForPosition(position, this.importantTags, this.importantTagFragments);
	}
	
	
	private String geLabelForPosition(int position, Set<String> importantTags, Set<String> importantTagFragments)
	{
		String tag = this.parseLabelsTable.getTag(position); 
		if(tag != null)
		{
			if(this.importantTags.contains(tag))
			{
				return tag;
			}
			
			for(String labelFragment : importantTagFragments)
			{
				if(tag.contains(labelFragment))
				{
					return tag;
				}
			}
		} 
		return null;
	}
	
	private String createAttachedTagString(String tag, int nounCount)
	{
		if(nounCount > 0)
		{
			return "-N+-" + tag;
		}
		return "-" + tag;
	}
	
	public CCGLabel getTagsLabel(int rangeMin, int rangeMax)
	{
		
		// Attach a label for the first tag
		String label = this.getBoundaryTag(rangeMin);
		
		int nounCount = 0;
		
		// Attach labels for informative intermediate tags
		for(int pos = rangeMin + 1; pos < rangeMax; pos++)
		{
			String tagLabel = this.getIntermediateTag(pos);
			
			if(tagLabel != null)
			{
				label += this.createAttachedTagString(tagLabel,nounCount);
				nounCount = 0;
			}
			else
			{
				if(this.nouns.contains(this.parseLabelsTable.getTag(pos)))
				{
					nounCount++;
				}
			}
		}
		
		// Attach the label for the last tag
		String lastTagLabel = this.getBoundaryTag(rangeMax);
		label += this.createAttachedTagString(lastTagLabel, nounCount);
		
		
		return CCGLabel.createCCGLabel(label, CCGLabel.CCGLabelType.TagsLabel);
	}
	
}
