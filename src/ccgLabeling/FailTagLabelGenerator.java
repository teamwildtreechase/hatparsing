/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import ccgLabeling.CCGLabel.CCGLabelType;

public class FailTagLabelGenerator implements TagLabelGenerator {

	public static final String FailLabelString =  "FAIL";
	private static final CCGLabel FailLabel = CCGLabel.createCCGLabel(FailLabelString, CCGLabelType.TagsLabel);

	public static FailTagLabelGenerator createFailTagLabelGenerator() {
		return new FailTagLabelGenerator();
	}

	@Override
	public CCGLabel getTagsLabel(int rangeMin, int rangeMax) {
		return FailLabel;
	}

}
