/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

public interface TagLabelGenerator {
	public CCGLabel getTagsLabel(int rangeMin, int rangeMax);
}
