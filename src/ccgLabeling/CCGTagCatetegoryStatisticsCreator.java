/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CCGTagCatetegoryStatisticsCreator {

	private final static String ForwardSlashCategory = "ForwardSlash";
	private final static String BackwardSlashCategory = "BackSlash";
	private final static String SinglePlusCategory = "SinglePlus";
	private final static String DoublePlusCategory = "DoublePlus";
	private final static String BasicCategory = "Basic";
	private final static String TwoWordBoundaryTagCategory = "TwoWordsBoundaryTag";
	private final static String MultipleWordBoundaryTagCategory = "MultipleWordsBoundaryTag";

	private final static Pattern ForwardSlashPattern = Pattern.compile(".*/.*");
	private final static Pattern BackSlashPattern = Pattern.compile(".*\\\\.*");
	private final static Pattern SinglePlusPattern = Pattern.compile("[^\\+]*\\+[^\\+]*");
	private final static Pattern DoublePlusPattern = Pattern.compile("[^\\+]*\\+[^\\+]*\\+[^\\+]*");
	//private final static Pattern BasicTagPattern = Pattern.compile("[^+/\\\\-(\\.\\.\\.)]*");
	//TODO fix this
	private final static Pattern BasicTagPattern = Pattern.compile(".*");
	private final static Pattern TwoWordsBoundaryTagPattern = Pattern.compile(".*-.*");
	private final static Pattern MultipleWordsBoundaryTagPattern = Pattern.compile(".*\\.\\.\\..*");

	int noForwardSlashCategories, noBackwardSlashCategories, noPlusCategories, noDoublePlusCategories, noBasicTagCategories, noTwoWordBoundaryTagCategories,
			noMultipleWordBoundaryTagCategories;

	private CCGTagCatetegoryStatisticsCreator() {
		this.noForwardSlashCategories = 0;
		this.noBackwardSlashCategories = 0;
		this.noBasicTagCategories = 0;
		this.noPlusCategories = 0;
		this.noDoublePlusCategories = 0;
		this.noMultipleWordBoundaryTagCategories = 0;
		this.noTwoWordBoundaryTagCategories = 0;
	}

	private static boolean isForwardSlashCategory(String category) {
		Matcher matcher = ForwardSlashPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isBackwardSlashCategory(String category) {
		Matcher matcher = BackSlashPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isSinglePlusCategory(String category) {
		Matcher matcher = SinglePlusPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isDoublePlusCategory(String category) {
		Matcher matcher = DoublePlusPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isTwoWordBoundaryTagCategory(String category) {
		Matcher matcher = TwoWordsBoundaryTagPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isMultipleWordBoundaryTagCategory(String category) {
		Matcher matcher = MultipleWordsBoundaryTagPattern.matcher(category);
		return matcher.matches();
	}

	private static boolean isBasicTagCategory(String category) {
		Matcher matcher = BasicTagPattern.matcher(category);
		return matcher.matches();
	}

	private boolean isNonGlueingRule(String ruleLine) {
		return !ruleLine.contains("[S,1]");
	}
	
	private String getCategory(String nonGlueRuleLine)
	{
		String[] parts = nonGlueRuleLine.split("\\|\\|\\|");
		String part =  parts[1].trim();
		return part.substring(1,part.length() - 3);
	}

	private void addCategoryCounts(String fileName) {
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

			String line = "";
			while ((line = fileReader.readLine()) != null) {

				if (isNonGlueingRule(line)) {
					String category = getCategory(line);
					System.out.println("category: " + category);
					increaseCategoryCount(category);
				}

			}
			fileReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void increaseCategoryCount(String category) {
		
		if(category.contains("\\\\-"))
		{
			System.out.println(category);
		}
		
		if (isTwoWordBoundaryTagCategory(category)) {
			this.noTwoWordBoundaryTagCategories++;
		} else if (isMultipleWordBoundaryTagCategory(category)) {
			this.noMultipleWordBoundaryTagCategories++;
		} else if (isSinglePlusCategory(category)) {
			this.noPlusCategories++;
		} else if (isDoublePlusCategory(category)) {
			this.noDoublePlusCategories++;
		} else if (isForwardSlashCategory(category)) {
			this.noForwardSlashCategories++;
		} else if (isBackwardSlashCategory(category)) {
			this.noBackwardSlashCategories++;
		}
		else if (isBasicTagCategory(category)) {
			this.noBasicTagCategories++;
		}
		else
		{
			throw new RuntimeException("Error : unrecognized CCG category detected: " + category);
		}
	}

	private String numberString(String category, int number, int total) {
		double percentage = (((double) number) / total) * 100;
		String result = "Number of " + category + " categories: " + number +"\t\t\t" + percentage + "% \n";
		return result;
	}

	private void printCounts() {
		int total = this.noBackwardSlashCategories + this.noForwardSlashCategories + this.noPlusCategories + this.noDoublePlusCategories
				+ this.noBasicTagCategories + this.noTwoWordBoundaryTagCategories + this.noMultipleWordBoundaryTagCategories;
		String result = "Total number of tags: " + total + "\n";

		result += numberString(BasicCategory, this.noBasicTagCategories,total);
		result += numberString(SinglePlusCategory, this.noPlusCategories,total);
		result += numberString(DoublePlusCategory, this.noDoublePlusCategories,total);
		result += numberString(ForwardSlashCategory, this.noForwardSlashCategories,total);
		result += numberString(BackwardSlashCategory, this.noBackwardSlashCategories,total);
		result += numberString(TwoWordBoundaryTagCategory, this.noTwoWordBoundaryTagCategories,total);
		result += numberString(MultipleWordBoundaryTagCategory, this.noMultipleWordBoundaryTagCategories,total);

		System.out.println(result);
	}

	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.out.println("wrong usage");
			return;
		}
		
		CCGTagCatetegoryStatisticsCreator categoryStatisticsCreator = new CCGTagCatetegoryStatisticsCreator();
		categoryStatisticsCreator.addCategoryCounts(args[0]);
		categoryStatisticsCreator.printCounts();
		
	}
	
}
