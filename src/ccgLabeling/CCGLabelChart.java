/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;


import util.Span;

import java.util.ArrayList;
import java.util.List;

import ccgLabeling.LabelGenerator.UnaryCategoryHandlerType;
import tsg.TSNodeLabel;
import util.Pair;
import util.Utility;

/**
 * The class LabelChart has as purpose to construct a chart of Labels 
 * for all the sub-ranges given a constituency parse
 * @author gemaille
 *
 */
public class CCGLabelChart 
{
	private static final String LATEX_COLUMN_SEPARATOR = " & ";
	private static final String LATEX_END_OF_LINE_STRING = "\\\\";
	private static final String LATEX_EMPTY_CELL_SYMBOL = "---";
	private static final String LATEX_HORIZONTAL_LINE_STRING = " \\hline";
	
	private final ParseLabelsTable parseLabelsTable;
	private final List<List<CCGLabel>> labelChart;
	
	/**
	 * Constructor that initializes the indexedParseLabels and labelChart with 
	 * the given arguments
	 * @param indexedParseLabels
	 */
	public CCGLabelChart( ParseLabelsTable parseLabelsTable,List<List<CCGLabel>> labelChart)
	{
		this.parseLabelsTable = parseLabelsTable;
		this.labelChart = labelChart;
	}
	
	
	public static CCGLabelChart createLabelChart(TSNodeLabel tree, UnaryCategoryHandlerType unaryCategoryHandlerType, boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		
		ParseLabelsTable parseLabelsTable =  ParseLabelsTable.createParseLabelsTable(tree);
		List<List<CCGLabel>> labelChart = CCGLabelChart.generateLabelChart(parseLabelsTable, unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		return new CCGLabelChart(parseLabelsTable,labelChart);
	}
	
	
	
	
	public static CCGLabelChart createLabelChart(String parse, UnaryCategoryHandlerType unaryCategoryHandlerType,boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		TSNodeLabel tree = null;
		try 
		{
			tree = new TSNodeLabel(parse);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return createLabelChart(tree, unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
	}
	

	
	public static List<List<CCGLabel>> generateLabelChart(ParseLabelsTable parseLabelsTable, UnaryCategoryHandlerType unaryCategoryHandlerType, boolean allowDoublePlus,boolean allowPosTagLabelFallback)
	{
		LabelGenerator labelGenerator = LabelGenerator.createLabelGenerator(parseLabelsTable, unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		
		List<List<CCGLabel>> labelChart = new ArrayList<List<CCGLabel>>();
		
		int sourceLength = parseLabelsTable.getSourceLength();
		
		for(int spanBegin = 0; spanBegin < sourceLength; spanBegin++)
		{
			List<CCGLabel> list = new ArrayList<CCGLabel>();
			
			for(int spanEnd = 0; spanEnd < sourceLength; spanEnd++)
			{
				if(spanBegin <= spanEnd)
				{	
					CCGLabel label = labelGenerator.generateLabel(new Pair<Integer>(spanBegin,spanEnd));
					list.add(label);
				}
				else
				{
					list.add(null);
				}
			}
			labelChart.add(list);
		}
		
		
		return labelChart;
	}
	
	public void printIndexedParseLabels()
	{
		this.parseLabelsTable.printIndexedParseLabels();
	}
	
	
	private void printLabelChartEntry(int i,int j)
	{
			if(j >= i)
			{	
				System.out.print("LChart[" + Utility.fsb(2, "" + i) + "," + Utility.fsb(2, "" + j) + "] :" + Utility.fsb(16,this.getCCGLabelString(i,j)) + "  ");
				return;
			}
			
			System.out.print(Utility.fsb(33, ""));
	}
	
	
	
	public void printLabelChart()
	{
		System.out.println("\nLabelChart: \n");
		for(int i = 0; i < labelChart.size(); i++)
		{
			
			System.out.println();
			for(int j = 0; j < labelChart.size(); j++)
			{
				 printLabelChartEntry(i,j);
			}	
		}
	}
	
	public String getTableColumnSpecificationString(int noColumns)
	{
		String result = "{|";
		for(int i = 0; i < noColumns; i++){
			result += "c" + "|";
		}
		result += "}";
		return result;
	}
	
	public String getSpansHeaderString(int noColumns)
	{
		String result = "Begin\\textbackslash End";
		for(int i = 0; i < noColumns; i++){
			result += LATEX_COLUMN_SEPARATOR + i;
		}
		result += " " + LATEX_END_OF_LINE_STRING + LATEX_HORIZONTAL_LINE_STRING + "\n";
		return result;
	}
	
	private static String getSlashReplacedLabelString(String labelString)
	{
		return labelString.replaceAll("\\\\", "\\\\textbackslash ");
	}
	
	public String  getLabelChartStringLatex()
	{
		int noColumns = labelChart.size();
		String result = "\\begin{tabular}" + getTableColumnSpecificationString(noColumns + 1) + "\n";
		result += "\\hline" + "\n";
		result += getSpansHeaderString(noColumns);
		for(int i = 0; i < labelChart.size(); i++)
		{
			result += i + LATEX_COLUMN_SEPARATOR;
		
			for(int j = 0; j < labelChart.size(); j++){
				if(j >= i){	
					
					result += getSlashReplacedLabelString(this.getCCGLabelString(i, j));
				}
				else{
					result += LATEX_EMPTY_CELL_SYMBOL;
				}
					
				if(j < (labelChart.size() - 1)){
					result += LATEX_COLUMN_SEPARATOR;
				}
				else{
					result +=  " " + LATEX_END_OF_LINE_STRING + " \\hline" + "\n";
				}					 
			}	
		}
		result += "\\end{tabular}";
		return result;
	}
	
	protected CCGLabel getCCGLabel(Span span)
	{
		return this.labelChart.get(span.getFirst()).get(span.getSecond());
	}
	
	private CCGLabel getCCGLabel(int i, int j){
		return this.getCCGLabel(new Span(i,j));
	}
	
	public String getCCGLabelString(int i, int j){
		return this.getCCGLabel(i, j).getLabel();
	}
	
	private static UnaryCategoryHandlerType getUnaryCategoryHandlerTypeFromString(String string)
	{
		if(string.equalsIgnoreCase("top")){
			return UnaryCategoryHandlerType.TOP;
		}
		else if(string.equalsIgnoreCase("bottom")){
			return UnaryCategoryHandlerType.BOTTOM;
		}
		else if(string.equalsIgnoreCase("all")){
			return UnaryCategoryHandlerType.ALL;
		}
		else{
			throw new RuntimeException("Error: unrecognized unary category handler type - choose from top,bottom,all");
		}
	}
	
	
	public static void main(String[] args)
	{
		if(args.length != 4){
			System.out.println("Usage : ccgLabelChart PARSE_TREE_STRING_WSJ_FORMAT  ALLOW_DOUBLE_PLUS ALLOW_POS_TAG_LABEL_FALLBACK UNARY_CATEGORY_HANDLER");
			System.out.println("ALLOW_DOUBLE_PLUS:  whether double plus is allowed in the labels (true or false)");
			System.out.println("ALLOW_POS_TAG_LABEL_FALLBACK: whether (boundary) pos tags can be used to form lables if no standard SAMT labels can be formed");
			System.out.println("UNARY_CATEGORY_HANDLER: how to label unaries:");
			System.out.println("\t bottom: use only the bottom label of the unary chain");
			System.out.println("\t top: use only the top label of the unary chain");
			System.out.println("\t all: use all labels in the unary chain");
			return;
		}
		// "(S1 (S (NP (DT the) (NN jurisprudence)) (VP (AUX is) (ADJP (JJ consistent) (CC and) (JJ clear))) (. .)))"  false false top
		//  (ROOT
		// "(ROOT  (S    (SBAR (IN after) (S (NP (PRP we)) (VP (VBP bake) (PP (IN in) (NP (DT the) (NN oven)))))) (, ,) (NP (PRP we)) (VP (VBP visit) (NP (DT the) (NN dentist)))(. .)))."  false false bottom
		//  (S (NP (NNP cooks))(VP (MD can) (VP (VB bake) (ADVP (RB fast))))(. .)))
		String parse = args[0];
		 boolean allowDoublePlus = Boolean.parseBoolean(args[1]); 
		 boolean allowPosTagLabelFallback = Boolean.parseBoolean(args[2]);
		 UnaryCategoryHandlerType unaryCategoryHandlerType = getUnaryCategoryHandlerTypeFromString(args[3]);
		//String parse = "(ROOT(S(NP(NP (JJ many))(PP (IN of)(NP (PRP$ our) (NNS countries))))(VP (VBP have)" 
		//	+ "(VP (VBN had)(NP(NP (NNS influxes))" 
		//	+ "(PP (IN of)(NP (NNS people))))(PP (IN from) (NN outside) (IN over)(NP (DT the) (NNS centuries)))))(. .)))";
		
		CCGLabelChart labelChart = CCGLabelChart.createLabelChart(parse,unaryCategoryHandlerType,allowDoublePlus,allowPosTagLabelFallback);
		labelChart.printIndexedParseLabels();
		labelChart.printLabelChart();
		
		System.out.println("\n\n" + labelChart.getLabelChartStringLatex());
	}
	
}
