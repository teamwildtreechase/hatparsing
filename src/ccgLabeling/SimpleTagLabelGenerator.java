/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

public class SimpleTagLabelGenerator implements TagLabelGenerator
{	
	private final ParseLabelsTable parseLabelsTable;
	private final static String TwoWordPhraseSeperator = "-";
	private final static String MulitpleWordPhraseSeperator = "...";
	
	public SimpleTagLabelGenerator(ParseLabelsTable parseLabelsTable)
	{
		this.parseLabelsTable = parseLabelsTable;
	}
	
	public static SimpleTagLabelGenerator createTagLabelGenerator(ParseLabelsTable parseLabelsTable)
	{	
		return new SimpleTagLabelGenerator(parseLabelsTable);
	}
	
	
	private String getBoundaryTag(int position)
	{
		return this.parseLabelsTable.getTag(position); 
	}
	
	public CCGLabel getTagsLabel(int rangeMin, int rangeMax)
	{
		String label;
		int noWords = (rangeMax - rangeMin) + 1;
		
		// Attach a label for the first tag
		if(noWords > 2)
		{	
			label = this.getBoundaryTag(rangeMin) + MulitpleWordPhraseSeperator + this.getBoundaryTag(rangeMax);
		}
		else if(noWords > 1)
		{
			label = this.getBoundaryTag(rangeMin) + TwoWordPhraseSeperator + this.getBoundaryTag(rangeMax);
		}
		else
		{
			label = this.getBoundaryTag(rangeMin);
		}
		
		return CCGLabel.createCCGLabel(label, CCGLabel.CCGLabelType.TagsLabel);
	}
}
