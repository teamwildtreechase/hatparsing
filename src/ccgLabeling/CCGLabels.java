/*
 * Copyright (c) <2013> <Gideon Maillette de Buy Wenniger>.  All Rights Reserved.
 */
package ccgLabeling;

public class CCGLabels
{
        private final CCGLabel ccgSourceLabel;
        private final CCGLabel ccgTargetLabel;

        protected CCGLabels(CCGLabel ccgSourceLabel, CCGLabel ccgTargetLabel)
        {
                this.ccgSourceLabel = ccgSourceLabel;
                this.ccgTargetLabel = ccgTargetLabel;
        }

        public String getCCGSourceLabelString()
        {
                return ccgSourceLabel.getLabel();
        }

        public String getCCGTargetLabelString()
        {
                return  ccgTargetLabel.getLabel();
        }

        public boolean hasSourceLabel()
        {
                return (this.ccgSourceLabel != null);
        }

        public boolean hasTargetLabel()
        {
                return (this.ccgTargetLabel != null);
        }
}
