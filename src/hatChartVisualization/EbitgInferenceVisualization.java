package hatChartVisualization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

import util.Utility;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;

public class EbitgInferenceVisualization extends JPanel {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private static final int BORDER_THICKNESS = 1;
  private static final int INFERENCE_PANEL_WIDTH = 180;
  private final EbitgInference inference;

  private EbitgInferenceVisualization(EbitgInference inference) {
    this.inference = inference;
    setupPanelBackgroundProperties();
    addDashedBorder();
  }

  public static EbitgInferenceVisualization createEbitgInferenceVisualization(
      EbitgInference inference) {
    return new EbitgInferenceVisualization(inference);
  }

  private static EbitgInference createComplexTestEbitgInference() {
    return EbitgChartVisualization.createMononteTestAlignmentChartBuilder().getChart()
        .getChartEntry(0, 2).getFirstCompleteMinimumTargetBindingInference();
  }

  private static EbitgInference createSimpleTestEbitgInference() {
    HashSet<Integer> basicBoundTargetPositions = new HashSet<Integer>();
    basicBoundTargetPositions.add(0);
    basicBoundTargetPositions.add(1);
    basicBoundTargetPositions.add(2);
    HashSet<Integer> extraBoundTargetPositions = new HashSet<Integer>();
    EbitgInference inference = EbitgInference.createEbitgInference(basicBoundTargetPositions,
        extraBoundTargetPositions, null);
    return inference;
  }

  public static EbitgInferenceVisualization createTestEbitgInferenceVisualization() {

    return new EbitgInferenceVisualization(createComplexTestEbitgInference());
    // return new EbitgInferenceVisualization(createSimpleTestEbitgInference());
  }

  private void setupPanelBackgroundProperties(){
    EbitgChartVisualization.setBackroundToWhite(this);
  }  
  
  @Override
  public Dimension getPreferredSize() {
    return new Dimension(INFERENCE_PANEL_WIDTH, 20);
  }
 
  @Override
  public Dimension getMinimumSize() {
    return new Dimension(INFERENCE_PANEL_WIDTH, 20);
  }
  
  @Override
  public Dimension getMaximumSize() {
    return new Dimension(INFERENCE_PANEL_WIDTH, 20);
  }

  private static String chartEntrySpanString(EbitgChartEntry chartEntry) {
    return "[" + chartEntry.getI() + "," + chartEntry.getJ() + "]";
  }

  private String getChartEntriesString() {
    List<EbitgChartEntry> chartEntries = inference.getNodeChartEntries();
    String result = "Partition: ";
    for (EbitgChartEntry chartEntry : chartEntries) {
      result += " " + chartEntrySpanString(chartEntry);
    }
    return result;
  }

  private void addDashedBorder() {
    int length = 10;
    int spacing = 2;
    boolean rounded = false;
    this.setBorder(BorderFactory.createDashedBorder(Color.BLACK, BORDER_THICKNESS, length, spacing, rounded));
  }
  
  private void drawInference(Graphics g) {
    g.drawString(getChartEntriesString(), 5, 15);
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    drawInference(g);
  }

  public static void main(String[] args) {
    JFrame mainFrame = new JFrame();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    EbitgInferenceVisualization ebitgInferenceVisualization = createTestEbitgInferenceVisualization();
    mainFrame.add(ebitgInferenceVisualization);
    mainFrame.pack();
    mainFrame.setVisible(true);
  }

}
