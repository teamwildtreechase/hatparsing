package hatChartVisualization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;

public class EbitgChartEntryVisualization extends JPanel {

  private static final int BORDER_THICKNESS = 5;
  private static final int INFERENCE_SPACING = 5;

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private final EbitgChartEntry chartEntry;
  private final boolean showChartEntryContents;

  private EbitgChartEntryVisualization(EbitgChartEntry chartEntry, boolean showChartEntryContents) {
    this.chartEntry = chartEntry;
    this.showChartEntryContents = showChartEntryContents;
    setupPanelBackgroundProperties();
    addInferences();
  }

  public static EbitgChartEntryVisualization createEbitgChartEntryVisualization(
      EbitgChartEntry chartEntry, boolean showChartEntryContents) {
    return new EbitgChartEntryVisualization(chartEntry, showChartEntryContents);
  }

  public static EbitgChartEntryVisualization createTestChartEntryVisualization() {
    return new EbitgChartEntryVisualization(createComplexTestChartEntry(), true);
  }

  private static EbitgChartEntry createComplexTestChartEntry() {
    return EbitgChartVisualization.createMononteTestAlignmentChartBuilder().getChart()
        .getChartEntry(0, 2);
  }

  private void setupPanelBackgroundProperties() {
    EbitgChartVisualization.setBackroundToWhite(this);
  }

  private void addBlackBorder() {
    this.setBorder(BorderFactory.createMatteBorder(BORDER_THICKNESS, BORDER_THICKNESS,
        BORDER_THICKNESS, BORDER_THICKNESS, new Color(0, 0, 0)));
  }

  private void addVerticalSpacing(JPanel thePanel) {
    thePanel.add(Box.createRigidArea(new Dimension(0, INFERENCE_SPACING))); // n
    // pixels
    // of
    // vertical
    // space.
  }

  public int getNumDisplayedInferences() {
    if (showChartEntryContents) {
      return this.chartEntry.getAllInferences().size();
    }
    return 0;
  }

  private void addInferences() {

    int numInferences = 0;
    this.addBlackBorder();
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

    JPanel innerPanel = new JPanel();
    EbitgChartVisualization.setBackroundToWhite(innerPanel);
    this.add(Box.createRigidArea(new Dimension(INFERENCE_SPACING, 0)));
    this.add(innerPanel);
    this.add(Box.createRigidArea(new Dimension(INFERENCE_SPACING, 0)));

    innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));

    if (showChartEntryContents) {
      innerPanel.add(EbitgChartEntrySharedInformationVisualization
          .createEbitgChartEntrySharedInformationVisualization(this.chartEntry));

      addVerticalSpacing(innerPanel);
      for (EbitgInference inference : chartEntry.getAllInferences()) {
        EbitgInferenceVisualization inferenceVisualization = EbitgInferenceVisualization
            .createEbitgInferenceVisualization(inference);
        innerPanel.add(inferenceVisualization);
        addVerticalSpacing(innerPanel);
        numInferences++;
      }
    }
    System.out.println("number of inferences:" + numInferences);

  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
  }

  public static void main(String[] args) {
    JFrame mainFrame = new JFrame();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    EbitgChartEntryVisualization ebitgChartEntryVisualization = EbitgChartEntryVisualization
        .createTestChartEntryVisualization();
    mainFrame.add(ebitgChartEntryVisualization);
    mainFrame.pack();
    mainFrame.setVisible(true);
  }

}
