package hatChartVisualization;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Set;

import javax.swing.JPanel;

import util.Utility;
import extended_bitg.EbitgChartEntry;
import extended_bitg.EbitgInference;

public class EbitgChartEntrySharedInformationVisualization extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private static final int PANEL_WIDTH = 190;
  private final EbitgChartEntry ebitgChartEntry;

  private EbitgChartEntrySharedInformationVisualization(EbitgChartEntry ebitgChartEntry) {
    this.ebitgChartEntry = ebitgChartEntry;
    setupPanelBackgroundProperties();
  }

  public static EbitgChartEntrySharedInformationVisualization createEbitgChartEntrySharedInformationVisualization(
      EbitgChartEntry ebitgChartEntry) {
    return new EbitgChartEntrySharedInformationVisualization(ebitgChartEntry);
  }

  private void setupPanelBackgroundProperties() {
    EbitgChartVisualization.setBackroundToWhite(this);
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension(PANEL_WIDTH, 75);
  }

  @Override
  public Dimension getMinimumSize() {
    return new Dimension(PANEL_WIDTH, 75);
  }

  @Override
  public Dimension getMaximumSize() {
    return new Dimension(PANEL_WIDTH, 75);
  }

  private EbitgInference getFirstInference() {
    EbitgInference result = null;
    if (this.ebitgChartEntry.hasInferences()) {
      result = this.ebitgChartEntry.getAllInferences().get(0);
    }
    return result;
  }

  private String getSetPermutationLabelString() {
    return "Set-permutation label:"
        + EbitgInference
            .sortedPermutationString(getFirstInference().getSortedSourceSetPermuation());
  }

  private String getCompletenessString() {
    if (this.ebitgChartEntry.isComplete()) {
      return "Complete";
    } else {
      return "Incomplete";
    }

  }

  private String getBoundTargetPositionsString() {
    Set<Integer> boundTargetPositions = getFirstInference().getBoundTargetPositions();
    return "Target positions: " + Utility.objectSetStringWithSetBrackets(boundTargetPositions);
  }

  private void drawSharedInformation(Graphics g) {

    if (this.getFirstInference() != null) {
      g.drawString(getSetPermutationLabelString(), 5, 20);
      g.drawString(getCompletenessString(), 5, 40);
      g.drawString(getBoundTargetPositionsString(), 5, 60);
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    drawSharedInformation(g);
  }

}
