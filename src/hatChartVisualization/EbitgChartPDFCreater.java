package hatChartVisualization;

import java.io.File;

import javax.swing.JComponent;
import javax.swing.JFrame;

import viewer_io.ExporterPDF;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilderBasic;

public class EbitgChartPDFCreater {

  private final EbitgChart chart;

  private EbitgChartPDFCreater(EbitgChart chart) {
    this.chart = chart;
  }

  public static EbitgChart createEbitgChart(String sourceString, String targetString,
      String alignmentString) {
    EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic
        .createEbitgChartBuilderWithDefaultSettings(sourceString, targetString, alignmentString);
    chartBuilder.findDerivationsAlignment();
    return chartBuilder.getChart();
  }

  public static EbitgChartPDFCreater createEbitgChartPDFCreater(String sourceString,
      String targetString, String alignmentString) {
    return new EbitgChartPDFCreater(createEbitgChart(sourceString, targetString, alignmentString));
  }

  private String maxSourceSpanSpecificationString(int maxSourceSpan) {
    return "maxSourceSpan:" + maxSourceSpan;
  }

  private String getOutputFileName(int maxSourceSpan) {
    return "EbitgChartVisualizationTestOutput-" + maxSourceSpanSpecificationString(maxSourceSpan)
        + ".pdf";
  }

  private void writeChartPDF(int maxSourceSpan) {
    EbitgChartVisualization ebitgChartVisualization = EbitgChartVisualization
        .createEbitgChartVisualization(this.chart, maxSourceSpan);
    // We need to force rendering because we are not using the panel inside a GUI
    // (JFrame), which would force it automatically
    ebitgChartVisualization.forceRenderingForITextByAddingToTemporaryFrame();

    System.out.println("ebitgChartVisualization.getSize(): " + ebitgChartVisualization.getSize());

    ExporterPDF.exportToPDF(new File(getOutputFileName(maxSourceSpan)), ebitgChartVisualization);
  }

  private void writeChartPDFs() {
    for (int maxSourceSpan = 1; maxSourceSpan <= this.chart.getSourceLength(); maxSourceSpan++) {
      writeChartPDF(maxSourceSpan);
    }
  }

  public static void main(String[] args) {
    String sourceString = "s1 s2 s3";
    String targetString = "t1 t2 t3";
    String alignmentString = "0-2 1-1 2-0";    
    
    //String sourceString = "don't want it";
    //String targetString = "ne le veux pas";
    //String alignmentString = "0-0 0-3 1-2 2-1";
    EbitgChartPDFCreater ebitgChartPDFCreater = createEbitgChartPDFCreater(sourceString,
        targetString, alignmentString);
    //ebitgChartPDFCreater.writeChartPDF(2);
    ebitgChartPDFCreater.writeChartPDFs();
  }

}
