package hatChartVisualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.junit.Assert;

import bitg.Pair;
import viewer_io.ExportablePanel;
import extended_bitg.EbitgChart;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartEntry;

public class EbitgChartVisualization extends JPanel implements ExportablePanel {
  private static int VERY_LARGE_MAX_SOURCE_SPAN = 1000;

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private final EbitgChart theChart;
  private final int maxSourceSpanDisplayed;

  private EbitgChartVisualization(EbitgChart theChart, int maxSourceSpan) {
    this.theChart = theChart;
    this.maxSourceSpanDisplayed = maxSourceSpan;
    setupPanelBackgroundProperties();
    addChartEntries();
  }

  public static EbitgChartVisualization createEbitgChartVisualization(EbitgChart theChart,
      int maxSourceSpan) {
    return new EbitgChartVisualization(theChart, maxSourceSpan);
  }

  public static EbitgChartVisualization createEbitgChartVisualization(String sourceString,
      String targetString, String alignmentString, int maxSourceSpan) {
    EbitgChartBuilderBasic ebitgChartBuilderBasic = EbitgChartBuilderBasic
        .createEbitgChartBuilderWithDefaultSettings(sourceString, targetString, alignmentString);
    ebitgChartBuilderBasic.findDerivationsAlignment();
    return EbitgChartVisualization.createEbitgChartVisualization(ebitgChartBuilderBasic.getChart(),
        maxSourceSpan);
  }

  public static EbitgChartVisualization createTestChartVisualization() {
    return new EbitgChartVisualization(createMononteTestAlignmentChartBuilder().getChart(),
        VERY_LARGE_MAX_SOURCE_SPAN);
  }

  public static EbitgChartVisualization createTestChartVisualization2() {
    return new EbitgChartVisualization(createDiscontiguousTestAlignmentChartBuilderEnglishFrench()
        .getChart(), VERY_LARGE_MAX_SOURCE_SPAN);
  }

  public static EbitgChartVisualization createTestChartVisualizationFullyInverted() {
    return new EbitgChartVisualization(createFullyInvertedTestAlignmentChartBuilder().getChart(),
        VERY_LARGE_MAX_SOURCE_SPAN);
  }

  public static EbitgChartBuilderBasic createMononteTestAlignmentChartBuilder() {
    String sourceString = "s1 s2 s3 s4";
    String targetString = "t1 t2 t3 t4";
    String alignmentString = "0-0 1-1 2-2 3-3";
    EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic
        .createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, 1000000,
            true, false);
    chartBuilder.findDerivationsAlignment();
    return chartBuilder;
  }

  public static EbitgChartBuilderBasic createDiscontiguousTestAlignmentChartBuilder() {
    String sourceString = "ne le veux pas";
    String targetString = "don't want it";
    String alignmentString = "0-0 1-2 2-1 3-0";
    EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic
        .createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, 1000000,
            true, false);
    chartBuilder.findDerivationsAlignment();
    return chartBuilder;
  }

  public static EbitgChartBuilderBasic createDiscontiguousTestAlignmentChartBuilderEnglishFrench() {
    String sourceString = "don't want it";
    String targetString = "ne le veux pas";
    String alignmentString = "0-0 0-3 1-2 2-1";
    EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic
        .createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, 1000000,
            true, false);
    chartBuilder.findDerivationsAlignment();
    return chartBuilder;
  }

  public static EbitgChartBuilderBasic createFullyInvertedTestAlignmentChartBuilder() {
    String sourceString = "s1 s2 s3 s4";
    String targetString = "t1 t2 t3 t4";
    String alignmentString = "0-3 1-2 2-1 3-0";
    EbitgChartBuilderBasic chartBuilder = EbitgChartBuilderBasic
        .createCompactHATsEbitgChartBuilder(sourceString, targetString, alignmentString, 1000000,
            true, false);
    chartBuilder.findDerivationsAlignment();
    return chartBuilder;
  }

  private AxisLabelPanel createSpanAxisLabelPanel(int span, Dimension componentSize) {
    String labelString = "span = " + span;
    return new AxisLabelPanel(labelString, componentSize, 90);
  }

  private AxisLabelPanel createStartPositionAxisLabelPanel(int startPosition) {
    String labelString = "start position = " + startPosition;
    return AxisLabelPanel.createAxisLabelPanel(labelString, 50, 50);
  }

  public static void setBackroundToWhite(JPanel panel) {
    panel.setOpaque(true);
    panel.setBackground(Color.WHITE);
  }

  private void setupPanelBackgroundProperties() {
    setBackroundToWhite(this);
  }

  public int getChartSourceLength() {
    return this.theChart.getSourceLength();
  }

  private JPanel createSpanLabelsPanel(Dimension componentSize) {
    JPanel result = new JPanel();
    setBackroundToWhite(result);
    result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));

    for (int span = getChartSourceLength(); span >= 1; span--) {
      System.out.println("result.add(createSpanAxisLabelPanel(span));");
      result.add(createSpanAxisLabelPanel(span, componentSize));
    }
    // result.setBorder(BorderFactory.createDashedBorder(Color.BLACK, 1, 10,
    // 10, true));
    return result;
  }

  private boolean showChartChartEntryContents(int spanLength) {
    return spanLength <= maxSourceSpanDisplayed;
  }

  private Pair<JPanel, EbitgChartEntryVisualization> createChartEntriesPanel() {
    int numSourcePositions = getChartSourceLength();
    int hgap = 2;
    int vgap = 2;

    JPanel result = new JPanel();
    EbitgChartEntryVisualization sizeDeterminingChartEntryVisualization = null;

    setBackroundToWhite(result);

    result.setLayout(new GridLayout(numSourcePositions + 1, numSourcePositions, hgap, vgap));

    for (int span = numSourcePositions; span >= 1; span--) {

      // result.add(createSpanAxisLabelPanel(span));

      for (int beginPos = 0; beginPos < numSourcePositions; beginPos++) {

        int remappedIndexI = beginPos; // getIndexReMappedForInvertingPositionsGrid(i);
        int remappedIndexJ = beginPos + span - 1;

        System.out.println("remappedIndexI: " + remappedIndexI);
        System.out.println("remappedIndexJ: " + remappedIndexJ);

        if ((remappedIndexJ >= remappedIndexI) && (remappedIndexJ < numSourcePositions)) {
          EbitgChartEntry chartEntry = this.theChart.getChartEntry(remappedIndexI, remappedIndexJ);
          EbitgChartEntryVisualization chartEntryVisualization = EbitgChartEntryVisualization
              .createEbitgChartEntryVisualization(chartEntry, showChartChartEntryContents(span));
          result.add(chartEntryVisualization);

          // If the newly added chart entry visualization has more
          // inferences than the
          // current one determining the size, we must update the size
          // determining chart entry
          // visualization with it
          if ((sizeDeterminingChartEntryVisualization == null)
              || (chartEntryVisualization.getNumDisplayedInferences() > sizeDeterminingChartEntryVisualization
                  .getNumDisplayedInferences())) {
            sizeDeterminingChartEntryVisualization = chartEntryVisualization;
          }

        } else {
          result.add(createEmptyWhitePanel());
        }

      }
    }

    // result.add(createEmptyWhitePanel());
    for (int beginPos = 0; beginPos < numSourcePositions; beginPos++) {
      result.add(createStartPositionAxisLabelPanel(beginPos));
    }
    return new Pair<JPanel, EbitgChartEntryVisualization>(result,
        sizeDeterminingChartEntryVisualization);
  }

  private void addChartEntries() {
    this.setLayout(new BorderLayout());
    Pair<JPanel, EbitgChartEntryVisualization> chartEntriesPanelAndSizeDeterminingChartEntryVisualization = createChartEntriesPanel();
    JPanel chartEntriesPanel = chartEntriesPanelAndSizeDeterminingChartEntryVisualization.first;
    EbitgChartEntryVisualization sizeDeterminingChartEntryVisualization = chartEntriesPanelAndSizeDeterminingChartEntryVisualization.last;
    chartEntriesPanel.setVisible(true);
    this.add(chartEntriesPanel, BorderLayout.CENTER);
    this.add(createSpanLabelsPanel(sizeDeterminingChartEntryVisualization.getPreferredSize()),
        BorderLayout.WEST);
  }

  private JPanel createEmptyWhitePanel() {
    JPanel result = new JPanel();
    setBackroundToWhite(result);
    return result;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
  }

  private static class AxisLabelPanel extends JPanel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private final Dimension gridElementDimension;
    private final int width;
    private final String labelString;

    private AxisLabelPanel(String labelString, Dimension dimension, int width) {
      this.labelString = labelString;
      this.gridElementDimension = dimension;
      this.width = width;
      setBackroundToWhite(this);
      // addDashedBorder();
    }

    public static AxisLabelPanel createAxisLabelPanel(String labelString, int width, int height) {
      return new AxisLabelPanel(labelString, new Dimension(width, height), width);
    }

    private Dimension getSizeDimension() {
      return new Dimension(width, gridElementDimension.height);
    }

    @Override
    public Dimension getPreferredSize() {
      return getSizeDimension();
    }

    @Override
    public Dimension getMinimumSize() {
      return getSizeDimension();
    }

    @Override
    public Dimension getMaximumSize() {
      return getSizeDimension();
    }

    @Override
    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawString(labelString, 20, getSizeDimension().height / 2);
    }

    private void addDashedBorder() {
      int length = 10;
      int spacing = 2;
      boolean rounded = false;
      this.setBorder(BorderFactory.createDashedBorder(Color.BLACK, 1, length, spacing, rounded));
    }

  }

  /**
   * This method is used to force rendering of the panel, by adding it to a
   * temporary frame, which is necessary for exporting to PDF without a GUI
   */
  public void forceRenderingForITextByAddingToTemporaryFrame() {
    // this.addNotify( );
    // this.validate( );

    // See:
    // http://stackoverflow.com/questions/425590/how-do-i-paint-swing-components-to-a-pdf-file-with-itext
    // http://stackoverflow.com/questions/12480181/can-i-create-a-bufferedimage-from-a-jpanel-without-rendering-in-a-jframe
    JFrame frame = EbitgChartVisualization.createChartVisualizationFrame(this);
    frame.dispose();
  }

  /**
   * This method is used by itext for PDF rendering
   */
  @Override
  public Dimension getArea() {
    return this.getSize();
    // return new Dimension(500,500);
  }

  /**
   * This method is used by itext for PDF rendering
   */
  @Override
  public void render(Graphics2D g2) {
    // See:
    // http://stackoverflow.com/questions/425590/how-do-i-paint-swing-components-to-a-pdf-file-with-itext
    // We need to use the paint method of panel to produce the correct
    // result for itext
    this.paint(g2);
  }

  public static JFrame createChartVisualizationFrame(EbitgChartVisualization chartVisualzation) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(chartVisualzation);
    frame.pack();
    frame.setVisible(true);
    return frame;
  }

  public static void main(String[] args) {
    // EbitgChartVisualization chartVisualzation =
    // createTestChartVisualization();
    // EbitgChartVisualization chartVisualzation =
    // createTestChartVisualization2();
    EbitgChartVisualization chartVisualzation = createTestChartVisualizationFullyInverted();
    createChartVisualizationFrame(chartVisualzation);
  }

}
