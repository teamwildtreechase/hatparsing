The program was originally designed to work with ARIALUNI.TTF font, which supports all unicode characters.
(Including all sorts of brackets and things like Chinese, Japanese Characters)
This font however is not open source nor in the public domain, so could not be delivered with the software. 
We therefore use this alternative font DejavVuSans which is freely available.

11-5-2013
To get full coverage after all we now moved to 
usage of the CODE2002.TTF font which is freeware.
From Wikipedia:

"Code2000 is a pan-Unicode digital font, which includes characters and symbols from a very large range of writing systems. As of the current final version 1.171 released in 2008, Code2000 is designed and implemented by James Kass to include as much of the Unicode 5.2 standard as practical (whereas 6.2 is the currently-released version), and to support OpenType digital typography features. Code2000 supports Basic Multilingual Plane, while Code2001 and Code2002 as related beta fonts created by James Kass, supports characters in higher Unicode planes.

The Code2000 font was available as unrestricted shareware, and the Code2001 and Code2002 fonts as freeware, from the author's website until January 2011. The website subsequently went down, and the domain name was later taken by an Australian programming site. As of December 2011 there is no known official download site for the fonts.
"

The exact legal status of CODE2000.TTF is unclear, but since it has been published as shareware we might assume that it can be used at least 
for non-commercial usage. But this is not certain.
In any case it can be readily downloaded

We will use the fonts in the following order of preference from the Fonts directory:
1. ARIALUNI.TTF
2. CODE2000.TTF
3. DejaVuSans.ttf

With regard to the CODE2000 font, this is readily available, e.g. from:
http://www.fontspace.com/james-kass/code2000

BUT:
Trademark notice
Notice: Code2000 is shareware, users are required to register. US$5.00 (James Kass, 744 Camelia Ct., Ripon, California U.S.A. 95366)[e-mail: jameskass@worldnet.att.net]
See also : 
ftp://ftp.crosswire.org/pub/sword/iso/latest/FONTS-EXTRA/CODE2000.HTM
(However this information is also no longer up to date it seems).

To conclude, if you decide to download and use this font by adding it to the Fonts directory, do so at your own responsibility


(See also LICENSE).
