package test_suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import unit_tests.*;
 
/**
 * JUnit Suite Test
 * @author gemaille
 *
 */
 
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
        AlignmentChunkTest.class,
        BinarizationScoresTest.class,
        BITGChartBuilderTest.class,
        EbitgChartInitializerTest.class,
        EbitgNodeTest.class,
        EbitgTransductionOperationTest.class,
        EbitgTreeGrowerTest.class,
        EbitgTreeStateTest.class,
        TreeGrowerTest.class,        
        EbitgTreeStateTest.class,
        GizaPPAlignmentsConverterTest.class,
        TreeGrowerTest.class,
        TreeStatisticsComputationTest.class,
        EbitgChartInitializerGreedyNullBindingsTest.class,
        SentencePairAlignmentChunkingZerosGroupedTest.class,
        SentencePairNullBindingTest.class,
        AlignmentInformationTest.class,
        SentencePairNullBindingTest.class,
        FilePartSplitterTest.class,
        EbitgInferencePermutationPropertiesTest.class,
        SpanTest.class
})
public class UnitTestSuite 
{
}
