package test_suites;

import integragtion_tests.AlignmentVisualizerTest;
import integragtion_tests.BasicAlignmentVisualizerTest;
import integragtion_tests.ConstTreeViewerTest;
import integragtion_tests.EbitgTransductionOperationTest;
import integragtion_tests.MultiThreadTreeStatisticsGeneratorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

 
/**
 * JUnit Suite Test
 * @author gemmaille
 *
 */
 
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
     //   EbitgViewerTest.class,
        BasicAlignmentVisualizerTest.class,
        ConstTreeViewerTest.class,
        AlignmentVisualizerTest.class,
        MultiThreadTreeStatisticsGeneratorTest.class,
        EbitgTransductionOperationTest.class,
})
public class IntegrationTestSuite {
}