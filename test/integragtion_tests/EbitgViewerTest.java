package integragtion_tests;

import org.junit.Test;

import extended_bitg.HATViewer;


public class EbitgViewerTest 
{
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }


	@Test
	public void testEbitgViewer()
	{
		HATViewer viewer = HATViewer.createAndShowGUI();
		viewer.loadEveryThingFromConfigFile();
		
		//for(int i = 1; i <= viewer.getMaxAlignmentSpinnerValue(); i++)
		for(int i = 1; i <= Math.min(100,viewer.getMaxAlignmentSpinnerValue()); i++)
		{	
			viewer.loadAlignmentTripleThreadSafe(i);
			boolean noErrors = viewer.computeHATsThreadSafe();
			//viewer.loadAlignmentTriple(i);
			//boolean noErrors = viewer.computeHATs();
            try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!noErrors)
			{
				System.out.println("Detected an error in the visualization of tree pair number " + i);
				System.out.println("closing");
				
			}
						
		}	
	}
	
}
