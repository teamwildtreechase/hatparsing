package integragtion_tests;

import junit.framework.Assert;
import org.junit.Test;
import viewer.AlignmentVisualizer;
import viewer.PanelTester;

public class AlignmentVisualizerTest 
{
	@Test
	public void testAlignmentVisualizer ()
	{
		AlignmentVisualizer viewer = AlignmentVisualizer.createAndShowGUI();
		
		PanelTester panelTester = PanelTester.createPanelTester(viewer.getAlignmentTreePanel());
		
		int numNonBackgroundPixels = panelTester.countNonBackgroundPixels();
		viewer.loadEverythingFromConfigFileSafe();
		
		
		Assert.assertTrue(numNonBackgroundPixels > 0);

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Scale the trees
		for(int i = 0 ; i < 1; i++)
		{	
			viewer.getAlignmentTreePanel().increaseFontSize();
		}	
		
		// The total number of non background pixels should have increased
		Assert.assertTrue(numNonBackgroundPixels < panelTester.countNonBackgroundPixels());
	}
	
	
}
