package integragtion_tests;

import junit.framework.Assert;

import org.junit.Test;

import alignment.ArtificialCorpusProperties;
import alignmentStatistics.EbitgTreeStatisticsData;


public class EbitgTransductionOperationTest 
{

	ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	  
		private void typeCountHasValue(EbitgTreeStatisticsData data, int branchingFactor, int expectedValue)
		{
			Assert.assertEquals(expectedValue, data.getObservedTypesCount(branchingFactor));
		}
		
		private void tokenCountHasValue(EbitgTreeStatisticsData data, int branchingFactor, int expectedValue)
		{
			Assert.assertEquals(expectedValue, data.getObservedTokensCount(branchingFactor));
		}
		
		private int expectedMonotoneTokens(int branchingFactor)
		{
			if(branchingFactor == 2)
			{	
				return this.artificialCorpusProperties.noMonotoneAlignments * (this.artificialCorpusProperties.monotoneLength - 1);
			}
			return 0;
		}
		
		private int expectedFullConnectedAlignmentTokens(int branchingFactor)
		{
			if(branchingFactor == this.artificialCorpusProperties.fullConnectedAlignmentLength)
			{
				return this.artificialCorpusProperties.noFullConnectedAlignments;
			}
			return 0;
		}
		
		private int expectedFourPermutationTokens(int branchingFactor)
		{
			if(branchingFactor == 4)
			{
				return this.artificialCorpusProperties.fourPermutationAlignmentGroups * this.artificialCorpusProperties.noFourPermutationAlignments;
			}
			else if(branchingFactor == 2)
			{
				return (this.artificialCorpusProperties.fourPermutationAlignmentGroups - 1) *  this.artificialCorpusProperties.noFourPermutationAlignments;
			}
			return 0;
		}
		
		private int expectedTokenCount(int branchingFactor)
		{
			return this.expectedMonotoneTokens(branchingFactor) +
				   this.expectedFullConnectedAlignmentTokens(branchingFactor) +
				   this.expectedFourPermutationTokens(branchingFactor);
		}
		
		@Test
		public void testTransductionOperationCountsComputation()
		{
			EbitgTreeStatisticsData mergedData = MultiThreadTreeStatisticsGeneratorTest.produceStatistics(this.artificialCorpusProperties);
			typeCountHasValue(mergedData,2,1);
			typeCountHasValue(mergedData,4,1);
			tokenCountHasValue(mergedData,4,expectedTokenCount(4));
			tokenCountHasValue(mergedData,2,expectedTokenCount(2));
		}
	
}
