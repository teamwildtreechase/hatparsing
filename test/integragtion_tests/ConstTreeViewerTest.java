package integragtion_tests;

import static org.junit.Assert.*;
import org.junit.Test;
import viewer.ConstTreeViewer;
import viewer.PanelTester;

public class ConstTreeViewerTest 
{

	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	  

		@Test
		public void testConstTreeViewerGeneratesNonEmptyTreePanel ()
		{
			ConstTreeViewer constTreeViewer = ConstTreeViewer.createAndShowGUI();
			
			PanelTester panelTester = PanelTester.createPanelTester(constTreeViewer.getTreePanel());
			
			int numNonBackgroundPixels = panelTester.countNonBackgroundPixels();
			assertTrue(numNonBackgroundPixels > 0);
			
			
			// Scale the trees
			for(int i = 0 ; i < 10; i++)
			{	
				constTreeViewer.getTreePanel().increaseFontSize();
			}	
			
			// The total number of non background pixels should have increased
			assertTrue(numNonBackgroundPixels < panelTester.countNonBackgroundPixels());
			
		}
		
	

}
