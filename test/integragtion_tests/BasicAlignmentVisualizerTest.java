package integragtion_tests;

import static org.junit.Assert.*;
import org.junit.Test;
import viewer.BasicAlignmentVisualizer;
import viewer.PanelTester;


public class BasicAlignmentVisualizerTest 
{
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }


	@Test
	public void testBasicAlignmentVisualizerTest ()
	{
		BasicAlignmentVisualizer viewer = BasicAlignmentVisualizer.createAndShowGUI();
		
		PanelTester panelTester = PanelTester.createPanelTester(viewer.getAlignmentPanel());
		
		assertFalse(panelTester.panelContainsNonBackgroundColor());
		
		viewer.showAlignmentsForInputFields();
		assertTrue(panelTester.panelContainsNonBackgroundColor());
	}
	
}
