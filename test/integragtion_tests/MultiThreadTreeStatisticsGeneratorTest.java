package integragtion_tests;

import java.io.IOException;
import junit.framework.Assert;
import org.junit.Test;
import alignment.ArtificialCorpusCreator;
import alignment.ArtificialCorpusProperties;
import alignmentStatistics.BITTBinarizationScoreComputation;
import alignmentStatistics.BinarizationScoreComputation;
import alignmentStatistics.CorpusBinarizationScoreComputation;
import alignmentStatistics.EbitgTreeStatisticsData;
import alignmentStatistics.MultiThreadTreeStatisticsGenerator;
import alignmentStatistics.TranslationEquivalentsCoverageComputation;

public class MultiThreadTreeStatisticsGeneratorTest 
{
	
	final ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();
	final BinarizationScoreExpectedValuePredictor binarizationScoreExpectedValuePredictor = new BinarizationScoreExpectedValuePredictor(artificialCorpusProperties);
	final TranslationEquivalentsCoverageExpectedValuePredictor translationEquivalentsCoverageExpectedValuePredictor = new TranslationEquivalentsCoverageExpectedValuePredictor(artificialCorpusProperties);
	/*
	private final int noMonotoneAlignments = 5;
	private final int noFullConnectedAlignments = 5;
	private final int monotoneLength = 10;
	private final int fullConnectedAlignmentLength = 5;
	private final int noFourPermutationAlignments = 5;
	private final int fourPermutationAlignmentGroups = 2;
	private final int lengthOneAlignments = 5;
	private final int noNoInternalNodesAlignments = 2;
	private final int noNoInternalNodesLengths = 5;
	*/
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
	public static void createTestCorpus(ArtificialCorpusProperties artificialCorpusProperties, String  testCorpusFolderName)
	{
		
		try 
		{
			ArtificialCorpusCreator.createArtificialCorpusCreator(artificialCorpusProperties, testCorpusFolderName);
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void binarizationScoreHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		BinarizationScoreComputation bsc = mergedData.getBinarizationScores().getBinarizationScoreComputation();
		double actual = bsc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}

	
	public static void binarizationScoreSTDHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		BinarizationScoreComputation bsc = mergedData.getBinarizationScores().getBinarizationScoreSTDComputation();
		double actual = bsc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}
	
	public static void binarizationCorpusScoreRelativeHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		CorpusBinarizationScoreComputation cbsc = mergedData.getNodeExtractionScores().getCorpusBinarizationScoreComputation();
		double actual = cbsc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	} 
	
	public static void binarizationScoreRelativeHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		BinarizationScoreComputation  inesc = (BinarizationScoreComputation) mergedData.getNodeExtractionScores().getBinarizationScoreComputation();
		double actual = inesc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}
	

	public static void binarizationScoreRelativeSTDHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		BinarizationScoreComputation bsc = mergedData.getNodeExtractionScores().getBinarizationScoreSTDComputation();
		double actual = bsc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}

	
	public static void bittBinarizationScoreHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, int maxSourceLength, double expected)
	{
		BITTBinarizationScoreComputation bsc = mergedData.getBinarizationScores().getBittBinarizationScoreComputation();
		double actual = bsc.getScore(maxBranchingFactor, maxSourceLength);
		double delta = 0.001;
		
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}
	
	public static void translationEquivalentsCoverageHasValue(EbitgTreeStatisticsData mergedData, int maxBranchingFactor, double expected)
	{
		TranslationEquivalentsCoverageComputation tecc = mergedData.getBinarizationScores().getTranslationEquivalentsCoverageComputation();
		double actual = tecc.getScore(1,maxBranchingFactor);
		double delta = 0.001;
		
		System.out.println("expected: " + expected + " actual: " + actual);
		Assert.assertEquals(expected, actual, delta);
	}
	
	
	public static EbitgTreeStatisticsData produceStatistics(ArtificialCorpusProperties artificialCorpusProperties)
	{
		String testCorpusFolderName = "./testCorpus/"; //"/home/gemaille/atv-bitg-workingdirectory/trunk/testCorpus/";
	
		createTestCorpus(artificialCorpusProperties, testCorpusFolderName);
		String configFileName = testCorpusFolderName + "configFile.txt";
		MultiThreadTreeStatisticsGenerator treeStatisticsGenerator = MultiThreadTreeStatisticsGenerator.createMultiThreadTreeStatisticsGenerator(configFileName,1);
		EbitgTreeStatisticsData mergedData = treeStatisticsGenerator.produceStatistics(false);
		return mergedData;
	}
	
	
	@Test
	public void testMultilThreadTreeStatisticsGenerator()
	{
		EbitgTreeStatisticsData mergedData = produceStatistics(this.artificialCorpusProperties);
		binarizationScoreHasValue(mergedData, 4,4,binarizationScoreExpectedValuePredictor.expectedBinarizationScore(4,4));
		binarizationScoreHasValue(mergedData, 5,4,binarizationScoreExpectedValuePredictor.expectedBinarizationScore(5,4));
		binarizationScoreHasValue(mergedData, 5,2,binarizationScoreExpectedValuePredictor.expectedBinarizationScore(5,2));
		binarizationScoreRelativeHasValue(mergedData,5,5,100);
		bittBinarizationScoreHasValue(mergedData, 1,10, binarizationScoreExpectedValuePredictor.expectedBITTBinarizationScore(10));
		binarizationScoreSTDHasValue(mergedData, 5,10,binarizationScoreExpectedValuePredictor.expectedBinarizationScoreSTD(5,10));
		binarizationScoreRelativeSTDHasValue(mergedData, 5,10,0);
		binarizationCorpusScoreRelativeHasValue(mergedData, 10, 1, 100);
		binarizationScoreRelativeHasValue(mergedData, 5,1, 100);
		translationEquivalentsCoverageHasValue(mergedData, 3,translationEquivalentsCoverageExpectedValuePredictor.expectedTranslationEquivalentsCoverageScore(3));
	}	

	

	
	@Test
	public void testDoubleMachinePrecision()
	{
        double machEps = 1.0f;
 
        do {
           machEps /= 2.0f;
        }
        while ((double)(1.0 + (machEps/2.0)) != 1.0);
 
        System.out.println( "Calculated machine epsilon: " + machEps );
        
        System.out.println("3.004 * 100 = "  + (3.004 * 100));
        System.out.println("3.004 * 100000000 = "  + (3.004 * 100000000));
        
        System.out.println("(((double)0) / 0) : " + (((double)0) / 0));
	}
	
}
