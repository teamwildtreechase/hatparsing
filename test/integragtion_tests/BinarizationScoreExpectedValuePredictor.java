package integragtion_tests;

import alignment.ArtificialCorpusProperties;

public class BinarizationScoreExpectedValuePredictor 
{

	private final ArtificialCorpusProperties artificialCorpusProperties;
	
	public BinarizationScoreExpectedValuePredictor(ArtificialCorpusProperties artificialCorpusProperties)
	{
		this.artificialCorpusProperties = artificialCorpusProperties;
	}
	
	public int expectedMonotoneScore()
	{
		return 100;
	}
	
	public double expectedFullConnectedAlignmentScore(int maxBrachingFactor)
	{
		System.out.println(maxBrachingFactor + " == " + artificialCorpusProperties.fullConnectedAlignmentLength + " " + (maxBrachingFactor ==  artificialCorpusProperties.fullConnectedAlignmentLength)) ;
		if(maxBrachingFactor ==  artificialCorpusProperties.fullConnectedAlignmentLength)
		{
			System.out.println("Return higher score!!!");
			return (0 / ( artificialCorpusProperties.fullConnectedAlignmentLength - 1)) * 100;
		}
		
		return 0;
	}
	
	private int fourPermutationAcceptedInternalNodes(int maxBranchingFactor, int maxSourceLengthAtomicFragment)
	{
		if(maxBranchingFactor >= 4)
		{
			return (this.artificialCorpusProperties.fourPermutationAlignmentGroups * 2) - 1;
		}
		else if(maxSourceLengthAtomicFragment >= 4)
		{
			return (this.artificialCorpusProperties.fourPermutationAlignmentGroups) - 1;
		}
		else
		{
			return 0;
		}
	}
	
	public double expectedFourPermutationAlignmentScore(int maxBranchingFactor, int maxSourceLengthAtomicFragment)
	{
		int expectedeFourPermutationAcceptedInternalNodes = fourPermutationAcceptedInternalNodes(maxBranchingFactor, maxSourceLengthAtomicFragment); 
		System.out.println("expectedeFourPermutationAcceptedInternalNodes :" + expectedeFourPermutationAcceptedInternalNodes);
		return ((double)expectedeFourPermutationAcceptedInternalNodes) / (artificialCorpusProperties.fourPermuationLength() - 1) * 100;
	}
	
	private double expectedLengthOneAlignmentScore()
	{
		return 100;
	}
	
	private double expectedNoInternalNodesAlignmentScore()
	{
		return 0;
	}
	
	public double expectedBinarizationScore(int maxBranchingFactor, int maxSourceLengthAtomicFragment)
	{
		double fullyConnectedScore = artificialCorpusProperties.fullyConnectedFraction() * expectedFullConnectedAlignmentScore(maxBranchingFactor); 
		double monotoneScore = artificialCorpusProperties.monotoneFraction() * expectedMonotoneScore();
		double fourPermutationscore = artificialCorpusProperties.fourPermutationFraction() * this.expectedFourPermutationAlignmentScore(maxBranchingFactor, maxSourceLengthAtomicFragment);
		double lengthOneScore = artificialCorpusProperties.lengthOneFraction() * this.expectedLengthOneAlignmentScore();
		double noInternalNodesScore = artificialCorpusProperties.noInternalNodesFraction() * this.expectedNoInternalNodesAlignmentScore();
		
		System.out.println("fullyConnectedScore: " + fullyConnectedScore);
		System.out.println("monontoneScore: " + monotoneScore);
		System.out.println("fourPermutationScore: " + fourPermutationscore);
		return fullyConnectedScore + monotoneScore + fourPermutationscore + lengthOneScore + noInternalNodesScore;
	}
	  
	
	private double expectedMonotoneSTD(int maxBranchingFactor,int maxSourceLengthAtomicFragment)
	{
		double expectedScore = expectedBinarizationScore(maxBranchingFactor,maxSourceLengthAtomicFragment);
		return Math.pow((expectedMonotoneScore() - expectedScore),2);
	}

	private double expectedFourPermutationSTD(int maxBranchingFactor,int maxSourceLengthAtomicFragment)
	{
		double expectedScore = expectedBinarizationScore(maxBranchingFactor,maxSourceLengthAtomicFragment);
		return Math.pow((expectedFourPermutationAlignmentScore(maxBranchingFactor, maxSourceLengthAtomicFragment) - expectedScore),2);
	}
	
	private double expectedFullConnectedSTD(int maxBranchingFactor,int maxSourceLengthAtomicFragment)
	{
		double expectedScore = expectedBinarizationScore(maxBranchingFactor,maxSourceLengthAtomicFragment);
		return Math.pow((expectedFullConnectedAlignmentScore(maxBranchingFactor) - expectedScore),2);
	}
	
	private double expectedLengthOneSTD(int maxBranchingFactor,int maxSourceLengthAtomicFragment)
	{
		double expectedScore = expectedBinarizationScore(maxBranchingFactor,maxSourceLengthAtomicFragment);
		return Math.pow((expectedLengthOneAlignmentScore() - expectedScore),2);
	}

	
	private double expectedNoInternalNodesSTD(int maxBranchingFactor,int maxSourceLengthAtomicFragment)
	{
		double expectedScore = expectedBinarizationScore(maxBranchingFactor,maxSourceLengthAtomicFragment);
		return Math.pow((expectedNoInternalNodesAlignmentScore() - expectedScore),2);
	}

	
	
	/**
	 * Predict the binarizationScoreStandard deviation based on the expected binarization score and 
	 * the standard deviations this implies for the three alignment types and combine this with their 
	 * counts and the total number of alignments to find the final expected std score
	 * @param maxBranchingFactor
	 * @param maxSourceLengthAtomicFragment
	 * @return
	 */
	public double expectedBinarizationScoreSTD(int maxBranchingFactor, int maxSourceLengthAtomicFragment)
	{
		double result = this.artificialCorpusProperties.noFullConnectedAlignments * expectedFullConnectedSTD(maxBranchingFactor, maxSourceLengthAtomicFragment);
		result += this.artificialCorpusProperties.noMonotoneAlignments * expectedMonotoneSTD(maxBranchingFactor, maxSourceLengthAtomicFragment);
		result += this.artificialCorpusProperties.noFourPermutationAlignments * expectedFourPermutationSTD(maxBranchingFactor, maxSourceLengthAtomicFragment);
		result += this.artificialCorpusProperties.noLengthOneAlignments * expectedLengthOneSTD(maxBranchingFactor, maxSourceLengthAtomicFragment);
		result += this.artificialCorpusProperties.noNoInternalNodesAlignments * expectedNoInternalNodesSTD(maxBranchingFactor, maxSourceLengthAtomicFragment);
		
		result = Math.sqrt(result / (this.artificialCorpusProperties.totalAlignments() - 1));
		
		return result;
		
	}
	

	public double expectedBITTBinarizationScore(int maxSourceLengthAtomicFragment)
	{
		double monotoneScore = artificialCorpusProperties.monotoneFraction() * expectedMonotoneScore();
		double fourPermutationAlignmentScore = artificialCorpusProperties.fourPermutationFraction() * this.expectedFourPermutationAlignmentScore(2, maxSourceLengthAtomicFragment);
		double lengthOneScore = artificialCorpusProperties.lengthOneFraction() * this.expectedLengthOneAlignmentScore();
		return monotoneScore + fourPermutationAlignmentScore + lengthOneScore;

	}

	
}
