package integragtion_tests;

import alignment.ArtificialCorpusProperties;

public class TranslationEquivalentsCoverageExpectedValuePredictor 
{

	private final ArtificialCorpusProperties artificialCorpusProperties;
	
	public TranslationEquivalentsCoverageExpectedValuePredictor(ArtificialCorpusProperties artificialCorpusProperties)
	{
		this.artificialCorpusProperties = artificialCorpusProperties;
	}
	
	private int expectedMonotoneTranslationEquivalentNodes()
	{
		int result =0;
		int n = artificialCorpusProperties.monotoneLength;
		result = ((n + 1) * n) / 2 ;
		return result;
	}
	  
	private int expectedMonotoneAcceptedTranslationEquivalentNodes(int maxBranchingFactor)
	{
		if(maxBranchingFactor >= 2 )
		{	
			return expectedMonotoneTranslationEquivalentNodes();
		}
		else
		{	
			return 0;
		}	
	}
	
	
	
	private double expectedFullConnectedTranslationEquivalentNodes()
	{
		return 1;
	}
	
	
	private int expectedFourPermutationTranslationEquivalentNodes()
	{
		return (this.artificialCorpusProperties.fourPermutationAlignmentGroups * 2) - 1 + (this.artificialCorpusProperties.fourPermutationAlignmentGroups * 4);
	}
	
	
	private int expectedFourPermutationAcceptedTranslationEquivalentNodes(int maxBranchingFactor)
	{
		if(maxBranchingFactor >= 4)
		{
			return expectedFourPermutationTranslationEquivalentNodes();
		}
		else
		{
			return 0;
		}
	}
	
	private int expectedNoInternalNodesTranslationEquivalentNodes()
	{
		return 1;
	}
	
	private int expectedLenghtOneTranslationEquivalentNodes()
	{
		return 1;
	}
	
	private int expectedTranslationEquivalentsTotalPossible()
	{
		int result = 0;
		result += this.expectedMonotoneTranslationEquivalentNodes() * this.artificialCorpusProperties.noMonotoneAlignments;
		result += this.expectedNoInternalNodesTranslationEquivalentNodes() * this.artificialCorpusProperties.noNoInternalNodesAlignments;
		result += this.expectedFourPermutationTranslationEquivalentNodes() *  this.artificialCorpusProperties.noFourPermutationAlignments;
		result += this.expectedFullConnectedTranslationEquivalentNodes() * this.artificialCorpusProperties.noFullConnectedAlignments;
		result += this.expectedLenghtOneTranslationEquivalentNodes() * this.artificialCorpusProperties.noLengthOneAlignments;
		
		return result;
	}
	
	private int expectedTranslationEquivalentsExtracted(int maxBranchingFactor)
	{
		int result = 0;
		result += this.expectedMonotoneAcceptedTranslationEquivalentNodes(maxBranchingFactor) * this.artificialCorpusProperties.noMonotoneAlignments;
		result += this.expectedNoInternalNodesTranslationEquivalentNodes() * this.artificialCorpusProperties.noNoInternalNodesAlignments;
		result += this.expectedFourPermutationAcceptedTranslationEquivalentNodes(maxBranchingFactor) *  this.artificialCorpusProperties.noFourPermutationAlignments;
		result += this.expectedFullConnectedTranslationEquivalentNodes() * this.artificialCorpusProperties.noFullConnectedAlignments;
		result += this.expectedLenghtOneTranslationEquivalentNodes() * this.artificialCorpusProperties.noLengthOneAlignments;
		
		return result;
	}
	
	public double expectedTranslationEquivalentsCoverageScore(int maxBranchingFactor)
	{
		return 100 * (((double)expectedTranslationEquivalentsExtracted(maxBranchingFactor)) / expectedTranslationEquivalentsTotalPossible());
	}
	
}
