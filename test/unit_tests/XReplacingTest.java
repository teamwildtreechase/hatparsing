package unit_tests;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import junit.framework.Assert;
import org.junit.Test;

import util.ParseAndTagBracketAndLabelConverter;

public class XReplacingTest {

	private void showMatches(String sentence, String patternString) {
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(sentence);

		boolean found = false;
		while (matcher.find()) {
			System.out.println("I found the text: " + matcher.group().toString());
			found = true;
		}
		if (!found) {
			System.out.println("I didn't found the text");
		}
	}

	@Test
	public void testXPatternMatchingPatternString() {
		String string1 = "foobarbarfoo";
		String patternString = "(?<=foo)bar(?=bar)";
		string1.matches(patternString);
		showMatches(string1, patternString);

		String testString = "(X)";
		String replacementString = "X-label";

		String simpleMatchLabel = "(?<=\\()X(?=\\))";
		// String simpleMatchLabel = "X(?=))";
		showMatches(testString, simpleMatchLabel);
		String result = testString.replaceAll(simpleMatchLabel, replacementString);
		System.out.println("result: " + result);
		String expectedResultString = "(" + replacementString + ")";
		Assert.assertEquals(expectedResultString, result);

		patternString = ParseAndTagBracketAndLabelConverter.xPatternMatchingPatternString();
		System.out.println("patternString: " + patternString);
		Assert.assertEquals(expectedResultString, testString.replaceAll(patternString, replacementString));

		Assert.assertEquals("X-label X-label X-label", "X X X".replaceAll(patternString, replacementString));
		Assert.assertEquals("X-label X-label XX", "X X XX".replaceAll(patternString, replacementString));
		Assert.assertEquals(")X X-label XX", ")X X XX".replaceAll(patternString, replacementString));
		Assert.assertEquals("(X-Bar (X-label X-Bar))", "(X-Bar (X X-Bar))".replaceAll(patternString, replacementString));
		Assert.assertEquals("(X-Bar (X-label man))", "(X-Bar (X man))".replaceAll(patternString, replacementString));

		showMatches(
				"(S1 (X (ADVP (RB Firstly)) (, ,) (NP (NP (DT the) (JJ classic) (NNP Eurofederalist) (JJ ideological) (NN shift)) (, ,) (NP (DT a) (JJ single) (NN market)) (, ,) (NP (DT a) (JJ single) (NN VAT)) (, ,) (NP (DT a) (JJ single) (JJ diplomatic) (NN service)) (, ,) (NP (DT a) (JJ single) (NN army)) (CC and) (ADVP (RB now)) (NP (DT a) (JJ single) (JJ criminal) (NN code)) (CC and) (NP (DT a) (JJ single) (JJ public) (NN prosecutor))) (. .)))",
				ParseAndTagBracketAndLabelConverter.xPatternMatchingPatternString());
	}

	@Test
	public void testGetStringsWithReplacedXLabels() {
		List<String> list = new ArrayList<String>();
		list.add("(X-Bar (X man))");

		List<String> result = ParseAndTagBracketAndLabelConverter.getStringsWithReplacedXLabels(list);
		Assert.assertEquals("(X-Bar (" + ParseAndTagBracketAndLabelConverter.X_SYNTACTIC_LABEL + " man))", result.get(0));

		List<String> list2 = new ArrayList<String>();
		list2.add("(S1 (X (ADVP (RB Absolutely)) (, ,) (NP (NNP Mr) (NNPS Papayannakis)) (. .)))");

		List<String> result2 = ParseAndTagBracketAndLabelConverter.getStringsWithReplacedXLabels(list2);
		Assert.assertEquals("(S1 (" + ParseAndTagBracketAndLabelConverter.X_SYNTACTIC_LABEL + " (ADVP (RB Absolutely)) (, ,) (NP (NNP Mr) (NNPS Papayannakis)) (. .)))", result2.get(0));

	}

}
