package unit_tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import extended_bitg.EbitgNode;

public class EbitgNodeTest 
{
	
	public EbitgNode n1, n2;
	
	

	@Before
	public void setUp() throws Exception 
	{
		HashSet<Integer> basicBoundTargetPositions = new 	HashSet<Integer>();
		basicBoundTargetPositions.add(1);
		basicBoundTargetPositions.add(3);
	
		HashSet<Integer> extraBoundTargetPositions = new 	HashSet<Integer>();
		extraBoundTargetPositions.add(2);

		HashSet<Integer> basicBoundTargetPositions2 = new 	HashSet<Integer>();
		basicBoundTargetPositions2.add(1);
		basicBoundTargetPositions2.add(3);
	
		HashSet<Integer> extraBoundTargetPositions2 = new 	HashSet<Integer>();
		extraBoundTargetPositions2.add(2);


		
		n1 = new EbitgNode(basicBoundTargetPositions,extraBoundTargetPositions,1,3);
		n2 = new EbitgNode(basicBoundTargetPositions2,extraBoundTargetPositions2,1,3);
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEqualsExtendedBITGNode() 
	{
		assert(n1.equals(n2));
		
		HashMap< EbitgNode, ArrayList<Integer>> map = new  HashMap< EbitgNode, ArrayList<Integer>>();
	
		assert(map.keySet().size() == 0);
		map.put(n1,new ArrayList<Integer>());
		assert(map.keySet().size() == 1);
		map.put(n2,new ArrayList<Integer>());
		// two nodes should be put in the same thing
		System.out.println("map: " + map);
		assert(map.keySet().size() == 1);
		
	}

}
