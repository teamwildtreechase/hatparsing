package unit_tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeStateBasic;
import extended_bitg.EbitgTreeState.TreeType;
import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SourceTreeStatisticsComputation;
import static  extended_bitg.EbitgChartBuilderBasic.createEbitgChartBuilder;
import static extended_bitg.EbitgTreeGrower.createEbitgTreeGrower;

public class EbitgTreeStateTest 
{
	String frenchString0, englishString0, alignmentString0;
	String frenchString1, englishString1, alignmentString1;
	String dutchString2, englishString2, alignmentString2;
	String dutchString3, englishString3, alignmentString3;


	EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder0, chartBuilder1, chartBuilder2, chartBuilder3;
	
	@Before
	public void setUp() throws Exception 
	{
		englishString0 = "no smoking";
		frenchString0 = "ne fume pas";
		alignmentString0 = "0-0 0-2 1-1";
		
		
		englishString1 = "Do not like it";
		frenchString1 = "Ne l' aime pas";
		alignmentString1 = "0-2 1-0 1-3 2-2 3-1";
	
		dutchString2 = "The man walks fast";
		englishString2 = "De man loopt snel";
		alignmentString2 = "0-0 1-1 2-2 3-3";
				
		dutchString3 = "0 1 2 3 4 5 6 7 8 9"; 
		englishString3 = "0 1 3 5 2 4 8 7 6 9";
		alignmentString3 = "0-0 1-1 2-3 3-5 4-2 5-4 6-8 7-7 8-6 9-9";
		
		
		 int maxAllowedInferencesPerNode = 100000;
		
		chartBuilder0 = createEbitgChartBuilder(englishString0,frenchString0, alignmentString0, maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder1 = createEbitgChartBuilder(englishString1,frenchString1, alignmentString1, maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder2 = createEbitgChartBuilder(dutchString2,englishString2, alignmentString2, maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder3 = createEbitgChartBuilder(dutchString3,englishString3, alignmentString3, maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,EbitgChartBuilderProperties.USE_CACHING);
		
		chartBuilder0.findDerivationsAlignment();
		chartBuilder1.findDerivationsAlignment();
		chartBuilder2.findDerivationsAlignment();
		chartBuilder3.findDerivationsAlignment();
	
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDetermineTreeType()
	{
		 EbitgTreeStatisticsComputation treeStatisticsComputation = new SourceTreeStatisticsComputation(true);
		 
		 EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg0 = createEbitgTreeGrower(chartBuilder0);
		 TreeType type0 =  treeStatisticsComputation.determineTreeType(tg0.getRootState(),1, tg0.getChart());

		 
		 EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg1 = createEbitgTreeGrower(chartBuilder1);
		 TreeType type1 =  treeStatisticsComputation.determineTreeType(tg1.getRootState(),1, tg1.getChart());
	
		 EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg2 = createEbitgTreeGrower(chartBuilder2);
		 TreeType type2 = treeStatisticsComputation.determineTreeType(tg2.getRootState(),1,tg2.getChart());
		 
		 EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg3 = createEbitgTreeGrower(chartBuilder3);
		 TreeType type3 = treeStatisticsComputation.determineTreeType(tg3.getRootState(),1, tg3.getChart());

		 System.out.println("type0: " + type0);
		 assert(type0 == TreeType.HAT);
		 
		 System.out.println("type1: " + type1);
		 assert(type1 == TreeType.HAT);

		 System.out.println("type2: " + type2);
		 assert(type2 == TreeType.BITT);
		 
		 System.out.println("type3: " + type3);
		 assert(type3 == TreeType.PET);		
		
	}
}
