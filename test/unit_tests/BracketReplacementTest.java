package unit_tests;

import java.util.ArrayList;
import java.util.List;


import org.junit.Test;

import util.ParseAndTagBracketAndLabelConverter;

public class BracketReplacementTest 
{

	@Test(expected=AssertionError.class)
	public void testAssertionsEnabled() 
	{
		assert(false);
	}
	
	
	@Test
	public void testBracketReplacement()
	{
		List<String> original = new ArrayList<String>();
		original.add("peter(");
		original.add("piet)");
		
		List<String> result = ParseAndTagBracketAndLabelConverter.getStringsWithReplacedBrackets(original);
		
		System.out.println("original: " + original);
		System.out.println("result: " + result);
	
		for(String resultString : result)
		{
			assert(!resultString.contains("("));
			assert(!resultString.contains(")"));
		}
	}
}
