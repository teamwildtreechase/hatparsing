package unit_tests;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;


import arraySkipList.ArraySkipList;

public class ArraySkipListTest {

	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert (false);
	}

	

	private static void testContains(List<Integer> containedKeys, ArraySkipList<Integer> arraySkipList) {
		int maxContainedKey = Collections.max(containedKeys);

		for (int key = 0; key < maxContainedKey * 3; key++) {

			if (containedKeys.contains(key)) {
				//System.out.println("Testing that key" + key + " is present");
				//System.out.println("");
				
				Integer element = arraySkipList.get(key);
				if(element == null)
				{
					System.out.println("testContains - error: skiplist:\n\nContainedKeys:\n");
					for(Integer i : containedKeys)
					{
						System.out.print(i + " "); 
					}
					arraySkipList.printRawArray();
				}
					
				assert (element != null);
			} else {
				System.out.println("Testing that key " + key + " is not present");
				
				if(arraySkipList.get(key) != null)
				{
					//arraySkipList.printRawArray();
				}
				
				assert (arraySkipList.get(key) == null);
			}
		}
	}

	//@Test
	public void test() {
		ArraySkipList<Integer> testSkipList = ArraySkipList.createTestArraySkipList();

		Assert.assertNotNull(testSkipList.get(100));
		testContains(Arrays.asList(0, 2, 4, 8, 16), testSkipList);
		
		System.out.println("putting pair 28 =>  5");
		testSkipList.put(28, 5);
		
		Assert.assertEquals(new Integer(5),testSkipList.get(28));
		
		//testSkipList.printArray();
		
		testSkipList.put(29,6);
		Assert.assertEquals(new Integer(6),testSkipList.get(29));
		
		//testSkipList.printRawArray();
		//testSkipList.printSkipList();
	}
	
	@Test
	public void randomShuffleListAddAndRetrieveTest() 
	{
		ArraySkipList<Integer> testSkipList = ArraySkipList.createArraySkipList();
		List<Integer> list = new ArrayList<Integer>();
		int numTestElements = 1000;
		for(int i = 0; i < numTestElements; i++)
		{
			list.add(i);
		}
		// Randomly permute the list
		Collections.shuffle(list);
		
		
		long beginTime = System.currentTimeMillis();
		for(Integer element : list)
		{
			Integer previous = testSkipList.put(element,element);
			
			if(previous != null)
			{
				testSkipList.printRawArray();
			}
			assert(previous == null);
		}
		
		long passedTime = System.currentTimeMillis() - beginTime;
		System.out.println(">>>Insertion into skiplist took " + passedTime + "ms");
		
		testContains(list, testSkipList);
		
	}

	//@Test
	public void testFillEmptySkipList() {

		ArraySkipList<Integer> testSkipList = ArraySkipList.createArraySkipList();
		testSkipList.put(5,0);
		testSkipList.put(10,1);
		testSkipList.put(2,2);
		testSkipList.put(0,3);
		testSkipList.put(30,5);
		//testSkipList.printSkipList();
		
		testContains(Arrays.asList(5,10,2,0,30), testSkipList);
	}	
}
