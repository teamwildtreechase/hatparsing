package unit_tests;

import org.junit.Assert;
import org.junit.Test;
import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SymmetricTreeStatisticsComputation;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeStateBasic;

public class EbitgTreeStatisticsComputationTest {

  private static String SOURCE_STRING_EXAMPLE1 = "s1 s2 s3 s4 s5 s6 s7";
  private static String TARGET_STRING_EXAMPLE1 = "t1 t2 t3 t4 t5 t6 t7 t8 t9 t10";
  private static String ALIGNMENT_STRING_EXAMPLE1 = "0-0 0-5 1-1 1-6 2-2 2-7 3-3 4-4 5-8 6-9";
  
  private static String SOURCE_STRING_EXAMPLE2 = "s1 s2 s3 s4";
  private static String TARGET_STRING_EXAMPLE2 = "t1 t2 t3 t4";
  private static String ALIGNMENT_STRING_EXAMPLE2 = "0-0 0-1 1-0 1-1 2-2 3-1 3-3";
  private static boolean ATOMIC_PARTS_MUST_ALL_SHARE_SAME_ALIGNMENT_CHUNK = false;
  public static final EbitgTreeStatisticsComputation TREE_STATISTICS_COMPUTATION = new SymmetricTreeStatisticsComputation(ATOMIC_PARTS_MUST_ALL_SHARE_SAME_ALIGNMENT_CHUNK);

  @Test
  public void testDetermineMaxNumberOfExtraAlignmentsInTree() {
    int maxAllowedInferencesPerNode = 100000;
    EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(SOURCE_STRING_EXAMPLE1, TARGET_STRING_EXAMPLE1,
        ALIGNMENT_STRING_EXAMPLE1, maxAllowedInferencesPerNode,
        EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,
        EbitgChartBuilderProperties.USE_CACHING);
    chartBuilder.findDerivationsAlignment();
    EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic>  treeGrower = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
    int maxNumberExtraAlignments = TREE_STATISTICS_COMPUTATION.determineMaxNumberOfExtraAlignmentsInTree(treeGrower);
    Assert.assertEquals(3,maxNumberExtraAlignments);    
  }

  /**
   * This tests illustrates a problem with the method determineMaxNumberOfExtraAlignmentsInTree.
   * It computes the number of extra alignments in an example with a discontiguous translation equivalent, that 
   * could be transformed into a contiguous translation equivalent by removing just a single link.
   * However, realizing this requires more complicated optimization, which is beyond the capability of the current 
   * algorithm, which yields only an approximation of the actual metric - this (edit) distance from 
   * bijective alignment - we want to compute. For computing the actual metric, possibly application of the 
   * Viterbi algorithm could be suitable.
   */
  @Test
  public void testDetermineMaxNumberOfExtraAlignmentsInTreeTwo() {
    int maxAllowedInferencesPerNode = 100000;
    EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(SOURCE_STRING_EXAMPLE2, TARGET_STRING_EXAMPLE2,
        ALIGNMENT_STRING_EXAMPLE2, maxAllowedInferencesPerNode,
        EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING,
        EbitgChartBuilderProperties.USE_CACHING);
    chartBuilder.findDerivationsAlignment();
    EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic>  treeGrower = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
    int maxNumberExtraAlignments = TREE_STATISTICS_COMPUTATION.determineMaxNumberOfExtraAlignmentsInTree(treeGrower);
    Assert.assertEquals(1,maxNumberExtraAlignments);    
  }
  
}
