package unit_tests;


import java.util.HashSet;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import util.Pair;
import canonicalFormAlignmentFactorization.CanonicalFormHATFactorizationShiftReduce;
import alignment.Alignment;

public class CanonicalFormHATFactorizationTest {

  // Global variable determining the verboisty of the CanonicalFormPETFactorization algorithm
  // tested
  private static final int TEST_VERBOSITY = 1;
  
	@Test
	public void testShiftReduceAlgorithm() {
		Alignment alignment = Alignment.createAlignment("0-0 1-1 2-2 3-3");
		CanonicalFormHATFactorizationShiftReduce canonicalFormHATFactorization = CanonicalFormHATFactorizationShiftReduce
				.createCanonicalFormHATFactorizationShiftReduce(alignment, 4, 4,TEST_VERBOSITY);

		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.shiftReduceAlgorithm());
		System.out.println(sourcePhraseRanges);

		Set<Pair<Integer>> expectedResult = new HashSet<Pair<Integer>>();
		for (int i = 0; i < 4; i++) {
			expectedResult.add(new Pair<Integer>(i, i));
		}
		expectedResult.add(new Pair<Integer>(0, 1));
		expectedResult.add(new Pair<Integer>(0, 2));
		expectedResult.add(new Pair<Integer>(0, 3));

		System.out.println(sourcePhraseRanges);
		Assert.assertEquals(expectedResult, sourcePhraseRanges);
	}

	@Test
	public void testShiftReduceAlgorithm2() {

		// [6,0,3,5,2,4,7,1]
		Alignment alignment = Alignment.createAlignment("0-6 1-0 2-3 3-5 4-2 5-4 6-7 7-1");
		CanonicalFormHATFactorizationShiftReduce canonicalFormHATFactorization = CanonicalFormHATFactorizationShiftReduce
				.createCanonicalFormHATFactorizationShiftReduce(alignment, 8, 8,TEST_VERBOSITY);
		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.shiftReduceAlgorithm());

		Set<Pair<Integer>> expectedResult = new HashSet<Pair<Integer>>();
		for (int i = 0; i < 8; i++) {
			expectedResult.add(new Pair<Integer>(i, i));
		}
		expectedResult.add(new Pair<Integer>(2, 5));
		expectedResult.add(new Pair<Integer>(0, 7));

		System.out.println(sourcePhraseRanges);
		Assert.assertEquals(expectedResult, sourcePhraseRanges);
	}

	@Test
	public void testShiftReduceAlgorithm3() {

		// 0,2,4,1,3,5,7,9,6,8]
		Alignment alignment = Alignment.createAlignment("0-0 1-2 2-4 3-1 4-3 5-5 6-7 7-9 8-6 9-8");
		CanonicalFormHATFactorizationShiftReduce canonicalFormHATFactorization = CanonicalFormHATFactorizationShiftReduce
				.createCanonicalFormHATFactorizationShiftReduce(alignment, 10, 10,TEST_VERBOSITY);
		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.shiftReduceAlgorithm());

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
		for (int i = 0; i < 10; i++) {
			expectedFactorization.add(new Pair<Integer>(i, i));
		}

		// The two reductions corresponding to the un-reducable
		// 4-element permutations contained in the permutation
		expectedFactorization.add(new Pair<Integer>(1, 4));
		// (Found during merge(0,7,9)
		expectedFactorization.add(new Pair<Integer>(6, 9));

		// Two reductions found when calling merge on range 0-7
		expectedFactorization.add(new Pair<Integer>(0, 4));
		expectedFactorization.add(new Pair<Integer>(0, 5));

		// Final reduction found when merging 0-9
		expectedFactorization.add(new Pair<Integer>(0, 9));
		System.out.println(sourcePhraseRanges);
		Assert.assertEquals(expectedFactorization, sourcePhraseRanges);
	}

	@Test
	public void testShiftReduceAlgorithm4() {

		// 0-0 1-1 1-3 2-2 3-2 4-4
		Alignment alignment = Alignment.createAlignment("0-0 1-1 1-3 2-2 3-2 4-4");
		CanonicalFormHATFactorizationShiftReduce canonicalFormHATFactorization = CanonicalFormHATFactorizationShiftReduce
				.createCanonicalFormHATFactorizationShiftReduce(alignment, 5, 5,TEST_VERBOSITY);
		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.shiftReduceAlgorithm());

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();

		expectedFactorization.add(new Pair<Integer>(0, 0));
		expectedFactorization.add(new Pair<Integer>(4, 4));
		expectedFactorization.add(new Pair<Integer>(0, 3));
		expectedFactorization.add(new Pair<Integer>(2, 3));
		expectedFactorization.add(new Pair<Integer>(1, 3));
		expectedFactorization.add(new Pair<Integer>(0, 4));

		System.out.println(sourcePhraseRanges);
		Assert.assertEquals(expectedFactorization, sourcePhraseRanges);
	}

}
