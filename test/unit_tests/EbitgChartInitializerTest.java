package unit_tests;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartInitializer;
import extended_bitg.EbitgChartInitializerAllNullBindings;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeStateBasic;

public class EbitgChartInitializerTest {
	
	private static final boolean USE_GREEDY_NULL_BINDING = false;
	
	String dutchString, englishString, alignmentString;


	EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder;
	
	@Before
	public void setUp() throws Exception 
	{
	
		dutchString = "Do not like it";
		englishString = "Ne l' aime pas";
		//String alignmentString2 = "0-2 1-0 1-3 2-2 3-1 ";
		alignmentString = "1-0 1-3";
		//alignmentString = "0-2 1-0 1-3 2-2 3-1 ";
		
		int maxAllowedInferencesPerNode = 100000;
		
		chartBuilder = EbitgChartBuilderBasic.createExplicitNullsHATsEbitgChartBuilder(dutchString,englishString, alignmentString, maxAllowedInferencesPerNode, USE_GREEDY_NULL_BINDING,true);
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindAllLeftStartingConsectutiveSubLists() 
	{
		
		System.out.println("}}}}testFindAllLeftStartingConsectutiveSubLists()");
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++)
		{	
			list.add(i);
		}
		HashSet<HashSet<Integer>> subLists = EbitgChartInitializerAllNullBindings.findAllLeftStartingConsectutiveSubLists(list);
		
		assert(subLists.size() == 11);
		
	
		for(HashSet<Integer> sublist : subLists)
		{
			
			System.out.println("sublist: " + sublist);
			int size = sublist.size();
			
			if(size > 0)
			{
				assert(sublist.contains(size-1));
			}	
			
			assert(!sublist.contains(size));
				
		}
		

	}

	@Test
	public void testFindDirectLeftFreeTargetPositions() 
	{
		System.out.println(chartBuilder.getAlignmentChunking());
		List<Integer> freePositions1 = chartBuilder.chartInitializer.findDirectLeftFreeTargetPositionsRightToLeft(0);
		assert(freePositions1.size() == 0);
		
		List<Integer> freePositions2 = chartBuilder.chartInitializer.findDirectLeftFreeTargetPositionsRightToLeft(3);
		Assert.assertEquals(2,freePositions2.size());
		Assert.assertEquals(2,freePositions2.get(0).intValue());
		assert(freePositions2.get(1) == 1);	
	}

	@Test
	public void testFindDirectRightFreeTargetPositions() 
	{
		List<Integer> freePositions1 = chartBuilder.chartInitializer.findDirectRightFreeTargetPositionsLeftToRight(0);
		assert(freePositions1.size() == 2);
		assert(freePositions1.get(0) == 1);
		assert(freePositions1.get(1) == 2);	
		
		List<Integer> freePositions2 = chartBuilder.chartInitializer.findDirectRightFreeTargetPositionsLeftToRight(3);
		assert(freePositions2.size() == 0);
	
		
	}
	
	@Test
	public void testFindAllSetCombinations() 
	{
		HashSet<Integer> set1 = new HashSet<Integer>();
		HashSet<Integer> set2 = new HashSet<Integer>();
		HashSet<Integer> set3 = new HashSet<Integer>();
		HashSet<Integer> set4 = new HashSet<Integer>();
	
		set1.add(1);
		set1.add(2);
		set2.add(3);
		set2.add(4);
		set3.add(5);
		set3.add(6);
		set3.add(4);
		set4.add(7);
		set4.add(8);

		HashSet<HashSet<Integer>> sets1 = new HashSet<HashSet<Integer>> ();
		HashSet<HashSet<Integer>> sets2 = new HashSet<HashSet<Integer>> ();
		
		sets1.add(set1);
		sets1.add(set2);
		sets2.add(set3);
		sets2.add(set4);
		

		HashSet<HashSet<Integer>> allCombinations = EbitgChartInitializer.findAllSetCombinations(sets1, sets2);
		
		System.out.println(allCombinations.size() + " = " + sets1.size()+ " * " + sets2.size());
		assert(allCombinations.size() == (sets1.size() * sets2.size()));
		System.out.println("allCombinations: " );
		for(HashSet<Integer> combination : allCombinations)
		{
			System.out.println("combinaiton:chartInitializer. " + combination );
		}
		
	}
	
	
	@Test
	public void  testFindPossibleExtraAlignmentSets()
	{
		int sourcePos = 1;
		HashSet<HashSet<Integer>> setCombinations = chartBuilder.chartInitializer.findPossibleExtendedAlignmentSets(sourcePos,false);
		
		Assert.assertEquals(4, setCombinations.size());
		
		
		System.out.println("setCombinations: " + setCombinations);
		
		HashSet<Integer> set = new HashSet<Integer>(); 
		//HashSet<Integer> targetMappings = chartBuilder.getAlignmentChunking().getAlignmentChunkForSourcePosition(sourcePos).getTargetPositions();
		// add the original target mappings to the set
		
		//System.out.println("targetMappings: " + targetMappings);
		
		//contains []
		assert(setCombinations.contains(set));
		
		// contains[1]
		set.add(1);
		assert(setCombinations.contains(set));
		
		// contains also [1,2]
		set.add(2);
		assert(setCombinations.contains(set));
	}	
}


