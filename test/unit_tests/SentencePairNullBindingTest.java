package unit_tests;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import hat_lexicalization.NullBindingItem;
import hat_lexicalization.SentencePairNullBinding;
import junit.framework.Assert;
import org.junit.Test;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class SentencePairNullBindingTest 
{
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	

	@Test
	public void testSentencePairNullBinding() 
	{
		
		String sourceString = "s1 s2 sr s4 s5 s6";
		String targetString = "t1 t2 t3 t4 t5 t6";
		String alignmentString = "1-1 4-4";
		SentencePairAlignmentChunkingZerosGrouped spac = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceString, targetString);
		SentencePairNullBinding spnb = SentencePairNullBinding.createSentencePairNullBinding(spac);
		
		System.out.println(spnb);
		
		
		Assert.assertEquals(3,spnb.getNumSourceNullBindingItems());
		Assert.assertEquals(3,spnb.getNumTargetNullBindingItems());
		
		NullBindingItem nb1 = spnb.getRightSourceNullBindingItem(1);
		Assert.assertEquals(Arrays.asList(new Integer[]{2,3}), nb1.getBindablePositions());
		
		NullBindingItem nb2 = spnb.getRightTargetNullBindingItem(1);
		Assert.assertEquals(Arrays.asList(new Integer[]{2,3}), nb2.getBindablePositions());
		
		NullBindingItem nb3 = spnb.getRightSourceNullBindingItem(4);
		Assert.assertEquals(Arrays.asList(new Integer[]{5}), nb3.getBindablePositions());
		
		NullBindingItem nb4 = spnb.getRightTargetNullBindingItem(4);
		Assert.assertEquals(Arrays.asList(new Integer[]{5}), nb4.getBindablePositions());
		
		NullBindingItem nb5 = spnb.getLeftSourceNullBindingItem(4);
		Assert.assertEquals(Arrays.asList(new Integer[]{2,3}), nb5.getBindablePositions());
		
		NullBindingItem nb6 = spnb.getLeftTargetNullBindingItem(4);
		Assert.assertEquals(Arrays.asList(new Integer[]{2,3}), nb6.getBindablePositions());
	}

	@Test
	public void testSentencePairNullBinding2() 
	{
		
		String sourceString = "Do not like it";
		String targetString = "Ne l' aime pas";
		//String alignmentString2 = "0-2 1-0 1-3 2-2 3-1 ";
		String alignmentString = "1-0 1-3";
		SentencePairAlignmentChunkingZerosGrouped spac = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceString, targetString);
		SentencePairNullBinding spnb = SentencePairNullBinding.createSentencePairNullBinding(spac);
		
		Assert.assertEquals(2,spnb.getNumSourceNullBindingItems());
		Assert.assertEquals(1,spnb.getNumTargetNullBindingItems());
		
		NullBindingItem nb1 = spnb.getLeftSourceNullBindingItem(1);
		Assert.assertEquals(Arrays.asList(new Integer[]{0}), nb1.getBindablePositions());
	
		NullBindingItem nb2 = spnb.getRightSourceNullBindingItem(1);
		Assert.assertEquals(Arrays.asList(new Integer[]{2,3}), nb2.getBindablePositions());
	
		NullBindingItem nb3 = spnb.getRightTargetNullBindingItem(0);
		Assert.assertEquals(Arrays.asList(new Integer[]{1,2}), nb3.getBindablePositions());

		NullBindingItem nb4 = spnb.getLeftTargetNullBindingItem(3);
		Assert.assertEquals(Arrays.asList(new Integer[]{1,2}), nb4.getBindablePositions());

	}	
	
	@Test
	public void testSentencePairNullBinding3() 
	{
		String sourceString = "s1 s2 sr s4 s5 s6";
		String targetString = "t1 t2 t3 t4 t5 t6";
		String alignmentString = "1-1 4-4";
		SentencePairAlignmentChunkingZerosGrouped spac = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceString, targetString);
		SentencePairNullBinding spnb = SentencePairNullBinding.createSentencePairNullBinding(spac);
		
		Set<Integer> expectedResult = new HashSet<Integer>(Arrays.asList(new Integer[]{0,2,3}));
		Assert.assertEquals(expectedResult, spnb.getSourceBoundNulls(1));
		Assert.assertEquals(expectedResult, spnb.getTargetBoundNulls(1));
		
		expectedResult = new HashSet<Integer>(Arrays.asList(new Integer[]{5}));
		Assert.assertEquals(expectedResult, spnb.getSourceBoundNulls(4));
		Assert.assertEquals(expectedResult, spnb.getTargetBoundNulls(4));
	}
		
}
