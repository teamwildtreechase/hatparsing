package unit_tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import alignment.AlignmentPair;
import alignmentStatistics.AlignmentChunk;


public class AlignmentChunkTest {
	
	AlignmentChunk chunk1, chunk2, chunk3, chunk4;

	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
	@Before
	public void setUp() throws Exception 
	{
		chunk1 = new AlignmentChunk(new AlignmentPair(1,2));
		chunk2 = new AlignmentChunk(new AlignmentPair(2,2));
		chunk3 = new AlignmentChunk(new AlignmentPair(3,3));
		chunk4 = new AlignmentChunk(new AlignmentPair(3,4));
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAlignmentType() 
	{
		assert(chunk1.determineAlignmentType() == AlignmentChunk.AlignmentType.OneToOne);
		chunk1.mergeWithChunk(chunk2);
		assert(chunk1.determineAlignmentType() == AlignmentChunk.AlignmentType.ManyToOne);
		
		chunk3.mergeWithChunk(chunk4);
		//System.out.println("chunk3.getSourcePositions().size(): " + chunk3.getSourcePositions().size());
		//System.out.println("chunk3.getTargetPositions().size(): " + chunk3.getTargetPositions().size());
		assert(chunk3.determineAlignmentType() == AlignmentChunk.AlignmentType.OneToMany);
		
		// Add one more chunk, making it 2-2 partially connected
		AlignmentChunk chunk5 = new AlignmentChunk(new AlignmentPair(4,4));
		chunk3.mergeWithChunk(chunk5);
		assert(chunk3.determineAlignmentType() == AlignmentChunk.AlignmentType.ManyToManyPartialConnected);	
		
		// Make it fully connected
		AlignmentChunk chunk6 = new AlignmentChunk(new AlignmentPair(4,3));
		chunk3.mergeWithChunk(chunk6);
		assert(chunk3.determineAlignmentType() == AlignmentChunk.AlignmentType.ManyToManyFullConnected);	
		
		
		
	}	
	
	@Test
	public void testCanBeMergedWith() 
	{
		// Four simple blocks of three checks that should hold 
		// For the chunks 1 and 2 that can be merged and the chunks 
		// 3 and 4 that can also be merged
		
		assert(chunk1.canBeMergedWith(chunk2));
		assert(!(chunk1.canBeMergedWith(chunk3)));
		assert(!(chunk1.canBeMergedWith(chunk4)));
	
		assert(chunk2.canBeMergedWith(chunk1));
		assert(!(chunk2.canBeMergedWith(chunk3)));
		assert(!(chunk2.canBeMergedWith(chunk4)));
		
		assert(chunk3.canBeMergedWith(chunk4));
		assert(!(chunk3.canBeMergedWith(chunk1)));
		assert(!(chunk3.canBeMergedWith(chunk2)));
		
		assert(chunk4.canBeMergedWith(chunk3));
		assert(!(chunk4.canBeMergedWith(chunk1)));
		assert(!(chunk4.canBeMergedWith(chunk2)));
	}

	@Test
	public void testMergeWithChunk() 
	{
		// Conditions that should hold before merging
		assert(chunk1.getAlignmentPairs().size() == 1);
		assert(chunk1.getSourcePositions().size() == 1);
		assert(chunk1.getTargetPositions().size() == 1);
		
		chunk1.mergeWithChunk(chunk2);
		
		// conditions that should hold after merging
		assert(chunk1.getAlignmentPairs().size() == 2);
		assert(chunk1.getSourcePositions().size() == 2);
		assert(chunk1.getTargetPositions().size() == 1);
		
		
		// Conditions that should hold before merging
		assert(chunk3.getAlignmentPairs().size() == 1);
		assert(chunk3.getSourcePositions().size() == 1);
		assert(chunk3.getTargetPositions().size() == 1);
		
		chunk3.mergeWithChunk(chunk4);
		
		// conditions that should hold after merging
		assert(chunk3.getAlignmentPairs().size() == 2);
		assert(chunk3.getSourcePositions().size() == 1);
		assert(chunk3.getTargetPositions().size() == 2);
	}

	@Test
	public void testMergeChunks() 
	{
		
	
		// Conditions that should hold before merging
		assert(chunk1.getAlignmentPairs().size() == 1);
		assert(chunk1.getSourcePositions().size() == 1);
		assert(chunk1.getTargetPositions().size() == 1);
		
		AlignmentChunk newChunk = AlignmentChunk.mergeChunks(chunk1, chunk2);
		
		
		// conditions that should hold after merging
		assert(newChunk.getAlignmentPairs().size() == 2);
		assert(newChunk.getSourcePositions().size() == 2);
		assert(newChunk.getTargetPositions().size() == 1);
		
		
		
	}

}
