package unit_tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import extended_bitg.EbitgTransductionOperation;


public class EbitgTransductionOperationTest
{
	
	@Test
	public void testEbitgTransductionOperation()
	{
		EbitgTransductionOperation operation1, operation2;
		List<Set<Integer>> perm1, perm2;
		
		perm1 = new ArrayList<Set<Integer>>();
		perm2 = new ArrayList<Set<Integer>>();
		
		HashSet<Integer> set1, set2, set3, set4;
		set1 = new HashSet<Integer>();
		set2 = new HashSet<Integer>();
		set3 = new HashSet<Integer>();
		set4 = new HashSet<Integer>();
		
		set1.add(1);
		set1.add(2);
		
		set3.add(1);
		set3.add(2);
	
		set2.add(5);
		set2.add(6);
		
		set4.add(5);
		set4.add(6);
		//set4.add(7);
		
		perm1.add(set1);
		perm1.add(set2);
		
		perm2.add(set3);
		perm2.add(set4);
		
		operation1 = new EbitgTransductionOperation(perm1);
		operation2 = new EbitgTransductionOperation(perm2);
		
		System.out.println("operation1.hashCode():" + operation1.hashCode());
		System.out.println("operation2.hashCode():" + operation2.hashCode());
		
		HashMap<EbitgTransductionOperation, Integer> map = new 		HashMap<EbitgTransductionOperation, Integer>();
		map.put(operation1, 1);
		System.out.println("map before: " + map);
		map.put(operation2, 3);
		
		System.out.println("map after : " + map);
		
	}

}
