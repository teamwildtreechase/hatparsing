package unit_tests;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import junit.framework.Assert;
import org.junit.Test;
import util.FilePartSplitter;
import util.FileStatistics;

public class FilePartSplitterTest {

	private static String FileSplitterTestFileName = "fileSplitterTestFile.txt";

  @Test(expected=AssertionError.class)
	public void testAssertionsEnabled() {
		assert (false);
	}
	
  
  	private void createTestFile()
  	{
  		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(FileSplitterTestFileName));
			
			for(int i = 0; i < 9; i++)
			{
				fileWriter.write("line " + i + "\n");
			}
			fileWriter.write("line " + 10);
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	}
  
  	private int countSummedNumLinesPartFiles(String baseFileName, int numParts)
  	{
  		int result = 0;
  		for(int i =1; i <= numParts; i++ )
  		{
  			String partFileName = baseFileName + "Part" + i +".txt";
  			try {
  				int numLines = FileStatistics.countLines(partFileName); 
				result += numLines;
				System.out.println(partFileName + " - numLines :" + numLines);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
  		}
  		return result;
  	}
  	
	@Test
	public void test() {
		int numParts = 4;
		createTestFile();
		try {
			System.out.print(FileSplitterTestFileName + " - no lines: " + FileStatistics.countLines(FileSplitterTestFileName) +"\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		FilePartSplitter.splitFileIntoParts(FileSplitterTestFileName,numParts );
		Assert.assertEquals(10, countSummedNumLinesPartFiles(FileSplitterTestFileName, numParts));
	}

}
