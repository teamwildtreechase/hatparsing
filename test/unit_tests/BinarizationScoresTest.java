package unit_tests;


import org.junit.Assert;
import org.junit.Test;

import alignmentStatistics.AbsoluteBinarizationScoreNormalizer;
import alignmentStatistics.BITTBinarizationScoreComputation;
import alignmentStatistics.BasicNormalization;
import alignmentStatistics.BinarizationScoreComputation;
import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SourceTreeStatisticsComputation;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeStateBasic;


public class BinarizationScoresTest 
{
	
	private static final boolean USE_GREEDY_NULL_BINDING = false;
	private static final boolean USE_CACHING = true;
	
	private final EbitgTreeStatisticsComputation treeStatisticsComputation = new SourceTreeStatisticsComputation(true);

	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }

	
	  
	public static EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> createEbitgTreeGrower(String sourceString, String targetString, String alignmentString)
	{
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, 100000, USE_GREEDY_NULL_BINDING,USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		System.out.println("alignment: " + alignmentString);
		return EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
	}
	
	  
	public double computeBinarizationScore(String sourceString, String targetString, String alignmentString, int maxSourceLengthAtomicFragment, int maxBranchingFactor)
	{
		double result = 0;
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = createEbitgTreeGrower(sourceString,targetString,alignmentString);
		BinarizationScoreComputation binarizationScoreComputation = BinarizationScoreComputation.createBinarizationScoreComputation(2,2, new AbsoluteBinarizationScoreNormalizer(treeStatisticsComputation), new BasicNormalization(), treeStatisticsComputation);
		result  = binarizationScoreComputation.computeBinarizationScore(tg, maxSourceLengthAtomicFragment, maxBranchingFactor); 
		System.out.println("Binarization score:" + result);
		return result;
	}	
	
	public double computeBITTBinarizationScore(String sourceString, String targetString, String alignmentString, int maxSourceLengthAtomicFragment)
	{
		double result = 0;
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = createEbitgTreeGrower(sourceString,targetString,alignmentString);
		BITTBinarizationScoreComputation bittBinarizationScoreComputation = BITTBinarizationScoreComputation.createBITTBinarizationScoreComputation(1, new AbsoluteBinarizationScoreNormalizer(treeStatisticsComputation), new BasicNormalization(), treeStatisticsComputation);
		result  = bittBinarizationScoreComputation.computeBinarizationScore(tg, maxSourceLengthAtomicFragment); 
		System.out.println("Binarization score:" + result);
		return result;
	}
	  
	@Test
	public void monotonousTest()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 2-2 3-3 4-4";
		int maxSourceLengthAtomicFragment = 1;
		int maxBranchingFactor = 2;
		
		Assert.assertEquals(computeBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment, maxBranchingFactor),100, 0.001);
		
	}
	
	@Test
	public void disContinuousTest()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 2-2 2-3 2-4 3-3 3-4 4-4";
		int maxSourceLengthAtomicFragment = 1;
		
		int maxBranchingFactor = 2;
		Assert.assertEquals(50,computeBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment, maxBranchingFactor), 0.001);
		
		maxBranchingFactor = 3;
		Assert.assertEquals(50,computeBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment, maxBranchingFactor), 0.001);
		
	}
	
	
	@Test
	public void disContinuousTest2()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 2-2 3-3 3-4 4-3 4-4";
		int maxSourceLengthAtomicFragment = 1;
		
		int maxBranchingFactor = 2;
		Assert.assertEquals(75,computeBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment, maxBranchingFactor), 0.001);
		
		maxBranchingFactor = 3;
		Assert.assertEquals(75,computeBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment, maxBranchingFactor), 0.001);
		
	}
	
	@Test
	public void bittMonotoneTest()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 2-2 3-3 4-4";
		
		System.out.println("alignmentString : " + alignmentString);
		int maxSourceLengthAtomicFragment = 1;
		
		Assert.assertEquals(computeBITTBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment),100, 0.001);
	}
	
	@Test
	public void bittInversionTest()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-2 2-1 3-4 4-3";
		int maxSourceLengthAtomicFragment = 1;
		
		Assert.assertEquals(computeBITTBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment),100, 0.001);
	}
	
	@Test
	public void bittDiscontinuousTest()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 2-2 3-1 3-4 4-3 4-4";
		int maxSourceLengthAtomicFragment = 1;
		Assert.assertEquals(0,computeBITTBinarizationScore(sourceString, targetString, alignmentString, maxSourceLengthAtomicFragment), 0.001);
	}
	  
	  
	@Test
	public void testBinarizationScore()
	{
		
	}
}
