package unit_tests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Assert;

import org.junit.Test;

import alignment.Alignment;
import alignment.AlignmentPair;
import alignmentConversion.GizaPPAlignmentsConverter;

import util.FileUtil;

public class GizaPPAlignmentsConverterTest {

	private static final String TestSourceFileName = "./testGizaCorpus.txt";
	private static final String TestResultFileName = "./testGizaCorpusConcersionResult.txt";
	
	private static String testFileString()
	{
		String result = "# Sentence pair (1) source length 7 target length 7 alignment score : 4.00066e-06\n" +
		"europese groei is ondenkbaar zonder solidariteit .\n" + 
		"NULL ({ }) european ({ 1 }) growth ({ 2 }) is ({ 3 }) inconceivable ({ 4 }) without ({ 5 }) solidarity ({ 6 }) . ({ 7 })\n" + 
		"# Sentence pair (2) source length 32 target length 31 alignment score : 1.64578e-41\n" +
		"ik wil ook mijn goede collega en vriend toomas ilves gelukwensen in zijn nieuwe functie en bij het promoten van meer europese ideeën in estland en meer estse ideeën hier .\n" + 
		"NULL ({ }) i ({ 1 }) would ({ 2 }) also ({ 3 }) like ({ }) to ({ }) wish ({ }) my ({ 4 }) good ({ 5 }) colleague ({ 6 }) and ({ 7 }) friend ({ 8 }) toomas ({ 9 }) ilves ({ 10 11 }) luck ({ }) in ({ 12 }) his ({ 13 }) new ({ 14 }) position ({ 15 }) and ({ 16 }) in ({ 17 }) promoting ({ 18 19 20 }) more ({ 21 }) of ({ }) europe ({ 22 }) in ({ 24 }) estonia ({ 25 }) and ({ 26 }) more ({ 27 }) estonian ({ 28 }) ideas ({ 23 29 }) here ({ 30 }) . ({ 31 })\n" + 
		"# Sentence pair (3) source length 14 target length 21 alignment score : 3.26566e-60\n" +
		"in ieder geval moet er vaart worden gezet achter het opzetten van decentrale instituties en het terugtrekken van buitenlandse troepen .\n" + 
		"NULL ({ 1 }) it ({ 10 }) is ({ }) essential ({ 12 }) to ({ 4 }) establish ({ 5 7 11 }) decentralised ({ 2 13 }) institutions ({ 14 }) and ({ 15 }) the ({ 16 }) withdrawal ({ 3 6 8 9 17 }) of ({ 18 }) foreign ({ 19 }) troops ({ 20 }) . ({ 21 })\n" + 
		"# Sentence pair (4) source length 14 target length 22 alignment score : 1.19368e-66\n" +
		"als wij denken die sector in een strak keurslijf van voorschriften te kunnen dwingen ,  zitten wij er toch echt naast .\n" + 
		"NULL ({ 10 }) it ({ 1 }) would ({ }) be ({ 2 }) a ({ 7 }) mistake ({ }) to ({ 12 13 }) enshrine ({ 3 5 8 9 11 14 16 19 20 }) this ({ 4 }) and ({ 15 }) set ({ }) it ({ 17 18 }) in ({ 6 }) stone ({ 21 }) . ({ 22 })\n"; 

		return result;
	}
	
	private static List<String> getTestLines()
	{
		List<String> result = Arrays.asList(testFileString().split("\\n"));
		return result;
	}
	
	private static String getGizaTestAlignment(int number)
	{
		return getTestLines().get(number * 3 - 1);
	}
	
	public static List<Integer> getGizaTestAlignmentTargetPositions(int number)
	{
		List<Integer> result = new ArrayList<Integer>();
		String gizaString = getGizaTestAlignment(number);
		
		// Remove the first part containing the target positions of null alignments
		String gizaStringWithoutNullAlignments = gizaString.substring(gizaString.indexOf("})") + 2);
		System.out.println("gizaStringWithoutNullAlignments: " + gizaStringWithoutNullAlignments);
		
		String decimalPatternString = "\\d+";
		Pattern pattern = Pattern.compile(decimalPatternString);
		Matcher matcher = pattern.matcher(gizaStringWithoutNullAlignments);

		// Find all matches after the first
		while (matcher.find()) 
	    {
			// Get the matching string
			String match = matcher.group();
			result.add(Integer.parseInt(match) - 1);
	    }  
		return result;
	}
	
	private void checkEquivalencyNumberTargetPositions(List<AlignmentPair> alignment, int exampleNum)
	{
		 int numTargetPositionsExample = getGizaTestAlignmentTargetPositions(exampleNum).size();
		 Assert.assertEquals(numTargetPositionsExample, alignment.size());
	}
	
	
	private static void writeTestFile()
	{
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(TestSourceFileName));
			outputWriter.write(testFileString());
			outputWriter.close();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
 
	  
	private static void checkRightNumberAlignmentPairs(Alignment alignment, int expected)
	{
		Assert.assertEquals(expected, alignment.size());
	}
	  
	  
	private static void checkContainsAlignmentPair(Alignment alignment, AlignmentPair expectedPair)
	{
		if(!alignment.getAlignmentPairs().contains(expectedPair))
		{
			assert(false);
		}
	}
	  
	
	
	  
	@Test
	public void testPerFormConversion() 
	{
		writeTestFile();
		GizaPPAlignmentsConverter.perFormConversion(TestSourceFileName, TestResultFileName);
		List<String> alignmentStrings = FileUtil.convertFileToStringList(new File(TestResultFileName));
		List<Alignment> alignments = new ArrayList<Alignment>();
		
		int exampleNumber = 1;
		for(String alignmentString : alignmentStrings)
		{
			System.out.println(getGizaTestAlignment(exampleNumber));
			System.out.println("Alignment String: " + alignmentString);
			Alignment alignment = Alignment.createAlignment(alignmentString);
			alignments.add(alignment);
			
			// Check that the number of pairs generated matches the number of target positions in the 
			// example GIZA++ String of the alignment
			checkEquivalencyNumberTargetPositions(alignment.getAlignmentPairs(), exampleNumber);
			
			exampleNumber++;
		}
	
		checkRightNumberAlignmentPairs(alignments.get(0),7);
		checkContainsAlignmentPair(alignments.get(0), new AlignmentPair(0,0));
		checkContainsAlignmentPair(alignments.get(3), new AlignmentPair(2,1));
		checkContainsAlignmentPair(alignments.get(3), new AlignmentPair(3,6));
	}
}
