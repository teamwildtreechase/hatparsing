package unit_tests;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import junit.framework.Assert;
import org.junit.Test;
import extended_bitg.EbitgInferencePermutationProperties;

public class EbitgInferencePermutationPropertiesTest {

	public static <T> Set<T> getAllElementsFromListOfSets(List<Set<T>> listOfSets) {
		Set<T> result = new HashSet<T>();
		for (Set<T> setInList : listOfSets) {
			result.addAll(setInList);
		}
		return result;
	}

	@Test
	public void testCreateAtomicPermutationProperties() {
		Set<Integer> targetPositions = new HashSet<Integer>();
		targetPositions.add(0);
		EbitgInferencePermutationProperties ebitgInferencePermutationProperties = EbitgInferencePermutationProperties.createAtomicPermutationProperties(targetPositions);
		Assert.assertEquals(getAllElementsFromListOfSets(ebitgInferencePermutationProperties.getSourcePermutation()).size(),targetPositions.size()); 
		
		targetPositions = new HashSet<Integer>();
		targetPositions.add(1);
		ebitgInferencePermutationProperties = EbitgInferencePermutationProperties.createAtomicPermutationProperties(targetPositions);
		Assert.assertEquals(targetPositions.size(),getAllElementsFromListOfSets(ebitgInferencePermutationProperties.getSourcePermutation()).size());
	}
}
