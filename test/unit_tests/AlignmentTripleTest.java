package unit_tests;

import junit.framework.Assert;
import org.junit.Test;
import alignment.AlignmentTriple;

public class AlignmentTripleTest {

	@Test
	public void testCreateNullExtendedAlignmentTriple() 
	{
		String sourceString = "i declare resumed the session of the european parliament adjourned on friday 17 december 1999 , and i would like once again to wish you a happy new year in the hope that you enjoyed a pleasant festive period .";
		String targetString = "reprise de la session je déclare reprise la session du parlement européen qui avait été interrompue le vendredi 17 décembre dernier et je vous renouvelle tous mes vux en espérant que vous avez passé de bonnes vacances .";
		String alignmentString = "0-0 1-1 2-2 3-3 4-4 5-5 5-6 7-7 6-8 8-9 9-9 10-9 11-9 12-10 13-11 14-12 15-13 16-13 16-14 17-16 18-17 19-24 24-29 25-30 25-31 26-32 27-33 28-33 29-34 30-35 31-36 20-37 21-37 22-37 23-37 32-37 32-38 33-39";
		
		AlignmentTriple extendedTriple = AlignmentTriple.createNullExtendedAlignmentTriple(sourceString, targetString, alignmentString);
		
		System.out.println("Extended alignment: " + extendedTriple.getAlignmentAsString());
		System.out.println("Extended sourceSentence: " + extendedTriple.getSourceSentenceAsString());
		System.out.println("Extended targetSentence: " + extendedTriple.getTargetSentenceAsString());
		
		Assert.assertFalse(extendedTriple.hasUnalignedSourcePositions());
		
	}
	
	@Test
	public void testCreateNullExtendedAlignmentTriple2() 
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4";
		String alignmentString = "0-0 3-3";
		
		AlignmentTriple extendedTriple = AlignmentTriple.createNullExtendedAlignmentTriple(sourceString, targetString, alignmentString);
		
		System.out.println("Extended alignment: " + extendedTriple.getAlignmentAsString());
		System.out.println("Extended sourceSentence: " + extendedTriple.getSourceSentenceAsString());
		System.out.println("Extended targetSentence: " + extendedTriple.getTargetSentenceAsString());
		
		Assert.assertFalse(extendedTriple.hasUnalignedSourcePositions());
		
	}
	
}
