package unit_tests;

import junit.framework.Assert;
import org.junit.Test;
import alignmentStatistics.SentencePairAlignmentChunking;
import alignmentStatistics.SentencePairAlignmentChunkingZerosGrouped;

public class SentencePairAlignmentChunkingZerosGroupedTest 
{


	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	@Test
	public void testSentencePairAlignmentChunkingZerosGrouped() 
	{
		String sourceString = "s1 s2 sr s4 s5 s6";
		String targetString = "t1 t2 t3 t4 t5 t6";
		String alignmentString = "1-1 4-4";
		SentencePairAlignmentChunking spac = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceString, targetString);
		
		Assert.assertEquals(8,spac.getAlignmentChunks().size() );
	}

	@Test
	public void testSentencePairAlignmentChunkingZerosGrouped2() 
	{
		String sourceString = "s1 s2 sr s4 s5 s6";
		String targetString = "t1 t2 t3 t4 t5 t6";
		String alignmentString = "0-0";
		SentencePairAlignmentChunking spac = SentencePairAlignmentChunkingZerosGrouped.createSentencePairAlignmentChunkingZerosGrouped(alignmentString, sourceString, targetString);
		
		Assert.assertEquals(3,spac.getAlignmentChunks().size() );
	}

}
