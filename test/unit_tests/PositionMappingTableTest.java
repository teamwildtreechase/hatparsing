package unit_tests;

import hat_lexicalization.PositionMappingTable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import junit.framework.Assert;
import org.junit.Test;

public class PositionMappingTableTest {

	 @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	@Test
	public void test() 
	{
		Set<Integer> positionSet = new HashSet<Integer>(Arrays.asList(new Integer[]{0, 1, 2, 3, 4, 5, 6, 8, 9, 11, 13, 14}));
		PositionMappingTable pmt = PositionMappingTable.createPositionMappingTable(positionSet);
		Assert.assertTrue(pmt.hasConsitentMappingTables());
		
	}

}
