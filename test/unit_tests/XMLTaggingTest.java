package unit_tests;

import junit.framework.Assert;
import org.junit.Test;
import util.XMLTagging;

public class XMLTaggingTest {

	@Test
	public void testExtractContentsWithPropertyNameFromString() {
		String propertyContainingString = "The big <ReorderingLabel>HAT</ReorderingLabel> label string HAT PET";
		String propertyString = "ReorderingLabel";
		String extractedPropertyContents = XMLTagging.extractNamedPropertyFromString(propertyString, propertyContainingString);
		Assert.assertEquals("HAT", extractedPropertyContents);
	}

}
