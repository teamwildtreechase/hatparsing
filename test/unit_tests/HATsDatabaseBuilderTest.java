package unit_tests;


import hat_database.CorpusFileLocations;
import hat_database.HATsDBFullInputFileLocations;
import hat_database.HATsDatabaseBuilder;
import hat_database.HatDBProperties;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.Test;
import util.FileUtil;
import alignment.ArtificialCorpusCreator;
import alignment.ArtificialCorpusProperties;

public class HATsDatabaseBuilderTest {

	private static String SOURCE_FILE_NAME = "sourceFile.txt";
	private static String TARGET_FILE_NAME = "targetFile.txt";
	private static String ALIGNMENT_FILE_NAME = "alignmentFile.txt";
	private static String MULTIPLE_CORPERA_DB_CREATION_TEST_CONFIGURATION_FILE_NAME = "configurationMultiLanguagePairDBCreation_Test.txt";

	private static CorpusFileLocations createCorpusFileLocations() {
		return CorpusFileLocations.createCorpusFileLocations(HATsDatabaseBuilder.HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME, SOURCE_FILE_NAME, TARGET_FILE_NAME, ALIGNMENT_FILE_NAME);
	}

	private static String createMutlipleLanguagePairDatabaseCreationConfig() {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(MULTIPLE_CORPERA_DB_CREATION_TEST_CONFIGURATION_FILE_NAME));
			outputWriter.write(HatDBProperties.FullCorperaLocationsPropertyName + " = " + HATsDatabaseBuilder.HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME + ","
					+ HATsDatabaseBuilder.HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME2 + "\n");
			outputWriter.write(HatDBProperties.FullCorperaSourceNamesPropertyName + " = " + SOURCE_FILE_NAME + "," + SOURCE_FILE_NAME + "\n");
			outputWriter.write(HatDBProperties.FullCorperaTargetNamesPropertyName + " = " + TARGET_FILE_NAME + "," + TARGET_FILE_NAME + "\n");
			outputWriter.write(HatDBProperties.FullCorperaAlignmentsNamesPropertyName + " = " + ALIGNMENT_FILE_NAME + "," + ALIGNMENT_FILE_NAME + "\n");
			outputWriter.write(HATsDBFullInputFileLocations.LANGUAGE_PAIR_DESCRPITION_STRINGS_PROPERTY + " = " + "S1_T1,S2_T2" + "\n");
			outputWriter.write(HATsDatabaseBuilder.ROOT_OUTPUT_FOLDER_PATH_PROPERTY + "  = " + "./DB_Experiments_Folder/" + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			FileUtil.closeCloseableIfNotNull(outputWriter);
		}
		return MULTIPLE_CORPERA_DB_CREATION_TEST_CONFIGURATION_FILE_NAME;
	}

	// @Test
	public void testComputeHATPropertiesFileForCorpus() {

		ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();
		try {
			ArtificialCorpusCreator.createArtificialCorpusCreator(artificialCorpusProperties, HATsDatabaseBuilder.HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME);

			HATsDatabaseBuilder.computeHATsPropertiesFileForCorpus(createCorpusFileLocations(), "SourceTarget", ".");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Test
	public void testComputeSingleLanuagePairDatabase() {

		ArtificialCorpusProperties artificialCorpusProperties = ArtificialCorpusProperties.createArtificialCorpusProperties();
		try {
			ArtificialCorpusCreator.createArtificialCorpusCreator(artificialCorpusProperties, HATsDatabaseBuilder.HATS_DATABASE_BUILDER_TEST_CORPUS_FOLDER_NAME);
			HATsDatabaseBuilder.computeSingleLanuagePairDatabase(createCorpusFileLocations(), "SourceTarget", ".", "./testdb.db");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testComputeMultipleLanuagePairDatabase() {

		HATsDatabaseBuilder.createTestExampleCorpera();
		String configFilePath = createMutlipleLanguagePairDatabaseCreationConfig();
		HATsDatabaseBuilder.computeMultipleLanguagePairsDatabase(configFilePath, "./testDBTwoLanguagePairs.db");
	}

}
