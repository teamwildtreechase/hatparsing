package unit_tests;

import org.junit.Assert;
import org.junit.Test;
import util.Pair;
import util.Span;

public class SpanTest {

	@Test 
	public void testSpansInOrder()
	{
		Span span1 = new Span(0,1);
		Span span2 = new Span(2,3);
		Pair<Span> spansInOrderPair = Span.spansInOrder(span1, span2);
		Assert.assertEquals(span1, spansInOrderPair.getFirst());
		Assert.assertEquals(span2, spansInOrderPair.getSecond());
		spansInOrderPair = Span.spansInOrder(span2, span1);
		Assert.assertEquals(span1, spansInOrderPair.getFirst());
		Assert.assertEquals(span2, spansInOrderPair.getSecond());
		
	}

	@Test
	public void testFollowsSpanConsecutively() {
		Span span1 = new Span(0,1);
		Span span2 = new Span(2,3);
		Assert.assertTrue(span1.followsSpanConsecutively(span2));
	}	
	
	@Test
	public void testSpansAreConsecutiveInSomeOrder() {
		Span span1 = new Span(0,1);
		Span span2 = new Span(2,3);
		Assert.assertTrue(Span.spansAreConsecutiveInSomeOrder(span1, span2));
		Assert.assertTrue(Span.spansAreConsecutiveInSomeOrder(span2, span1));
		
		Span span3 = new Span(0,1);
		Span span4 = new Span(3,4);
		Assert.assertFalse(Span.spansAreConsecutiveInSomeOrder(span3, span4));
		Assert.assertFalse(Span.spansAreConsecutiveInSomeOrder(span4, span3));
	}

}
