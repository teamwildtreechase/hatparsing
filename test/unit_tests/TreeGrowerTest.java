package unit_tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bitg.BITGChartBuilder;
import bitg.PartialTree;
import bitg.TreeGrower;

import util.Pair;

public class TreeGrowerTest 
{
	BITGChartBuilder b;
	TreeGrower tg;

	@Before
	public void setUp() throws Exception 
	{
		
		int[] permutation = new int[]{4,5,6,7,1,2,3,8,9,10,11}; 
		b = new BITGChartBuilder(permutation);
		b.findDerivationsPermutation();
		
		tg = new TreeGrower(b);
		tg.growTrees();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGrowTrees() 
	{
		System.out.println("\n>>>Executing testGrowTrees...");
		// At bracketing level 2 none of these numbers can be bracketed, so there should be no derivation and 
		// no derivable entry of span length 2 either
		
		//tg.printCompletedTrees();
		
		// The correct number of brackets is equal to the length of the permutation times two minus one
		// this is a property of bracketing with binary trees
		int expectedBrackets = (tg.getBitg().permutation.length * 2) - 1;  
		
		System.out.println("Check consistency of produced trees");
		System.out.println("Level one: Check that the number of opening and closing brackets is the same in every tree");
		for(PartialTree tree : tg.getCompletedTrees().getStack())
		{
			Pair<Integer> bracketCounts = countBrackets(tree.getTreeString());
			//System.out.println("opening brackets: " + bracketCounts.first + " closing brackets:" + bracketCounts.last);
			assertEquals("Equal Bracket Counts", bracketCounts.getFirst(), bracketCounts.getSecond());
		
			assertEquals("Bracket count matches expected count", (int)bracketCounts.getFirst(), expectedBrackets);
		}
		
		System.out.println("Level two: Check that the number sequence produced by the tree mathces that of the permutation");
		for(PartialTree tree : tg.getCompletedTrees().getStack())
		{

			leavesMatchPermutation(tree.getTreeString(), tg.getBitg().permutation);	
		}
		
		System.out.println("Test passed succesfully");
	}

	public boolean leavesMatchPermutation(String treeString, int[] permutation)
	{
		
		boolean numberStarted = false;
		
		String numberString = "";
		ArrayList<Integer> treeNumbers = new ArrayList<Integer>();
		
		for(int i = 0; i < treeString.length(); i++)
		{
			String s = treeString.substring(i,i + 1);
			//System.out.println("s: " + s);
			if(isInteger(s))
			{
				numberStarted = true;
				numberString += s;
			}
			else
			{
				if(numberStarted)
				{
					int number = Integer.parseInt(numberString);
					treeNumbers.add(number);
					numberStarted = false;
					numberString = "";
				}
			}
		}
		
		//System.out.println("Comparing numbers of permutation and tree:");
		//System.out.println("treeNumbers.size(): " + treeNumbers.size());
		for(int i = 0; i < permutation.length; i++)
		{
			//System.out.println(i);
			//System.out.println("Permutation[" + i +"]:" + permutation[i] + " tree["+i+"]: " + treeNumbers.get(i));
			int numTree = treeNumbers.get(i);
			int numPerm = permutation[i];
			assertEquals("Equal " + i + "th number in tree and permutation",numTree,numPerm);
		}
		
		return true;
		
	}
	
	public static boolean isInteger(String s)
	{
		return s.matches("[0-9]+");
	}
	
	
	public Pair<Integer> countBrackets(String treeString)
	{
		
		int openBracketCount = 0;
		int closeBracketCount = 0;
		
		for(int i = 0; i < treeString.length(); i++)
		{
			char c = treeString.charAt(i);
			
			if(c == '(')
			{
				openBracketCount++;
			}
			else if(c == ')')
			{
				closeBracketCount++;
			}
		}
		
		
		Pair<Integer> bracketCounts = new Pair<Integer>(openBracketCount,closeBracketCount);
		
		return bracketCounts;
	}
		
}
