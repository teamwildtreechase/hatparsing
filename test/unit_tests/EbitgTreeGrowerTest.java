package unit_tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import tsg.TSNodeLabel;
import tsg.TreeStringConsistencyTester;
import ccgLabeling.CCGLabeler;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgChartBuilderProperties;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeStateBasic;
import static extended_bitg.EbitgTreeGrower.createEbitgTreeGrower;


public class EbitgTreeGrowerTest 
{

	
	String dutchString, englishString, alignmentString;


	EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder;
	EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg;
	
	

	@Test(expected=AssertionError.class)
	public void testAssertionsEnabled() 
	{
		assert(false);
	}
	

	@Before
	public void setUp() throws Exception 
	{
		
		dutchString = "Do not like it";
		englishString = "Ne l' aime pas";
		//String alignmentString2 = "0-2 1-0 1-3 2-2 3-1 ";
		//alignmentString = "1-0 1-3";
		alignmentString = "0-1 1-0 1-3 2-2";
		
		int maxAllowedInferencesPerNode = 100000;
		
		chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(dutchString,englishString, alignmentString, maxAllowedInferencesPerNode,EbitgChartBuilderProperties.USE_GREEDY_NULL_BINDING, EbitgChartBuilderProperties.USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		tg = createEbitgTreeGrower(chartBuilder);

		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGrowTrees() 
	{
		tg.generateNextTree();
	}
	
	
	private boolean sourceHatContainsNumberOfLeafNodesEqualToNumberSourceWords(String sourceString, EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> treeGrower) throws Exception
	{

		String treeString = treeGrower.getTreeString();
		Assert.assertTrue(treeString.length() > 0);
		System.out.println("treeString: " + treeString);
		TSNodeLabel tree = new TSNodeLabel(treeString);
		return TreeStringConsistencyTester.treeIsConsistenWithString(sourceString, tree);
	}
	
	private boolean targetHatContainsNumberOfLeafNodesEqualToNumberTargetWords(String targetString, EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> treeGrower) throws Exception
	{
		String twinTreeString = treeGrower.getTwinTreeString();
		Assert.assertTrue(twinTreeString.length() > 0);
		TSNodeLabel tree = new TSNodeLabel(twinTreeString);
		return TreeStringConsistencyTester.treeIsConsistenWithString(targetString, tree);
	}
	
	
	
	@Test
	public void testTSNodeLabel()
	{
		String treeString = "( 〈{1,2},{1,2}〉<ExtraLabels><VarName>@0@</VarName></ExtraLabels>(NULLBINDING〈{1,2}〉=>[t1,t2,t3,t4,t5] s1 s2 s3 s4)  s5  )";
		try 
		{
			new TSNodeLabel(treeString);
		} catch (Exception e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}	
	
	@Test
	public void testBasicTreeGrower()
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 0-4 4-0 4-4";
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> treeGrower = EbitgChartBuilderBasic.buildAndWriteTreePairsForAlignment(EbitgChartBuilderProperties.MaxAllowedInferencesPerNode, sourceString, targetString, alignmentString, CCGLabeler.createEmptyCCGLabeler(), false, true);
		try 
		{
			Assert.assertTrue(sourceHatContainsNumberOfLeafNodesEqualToNumberSourceWords(sourceString, treeGrower));
			Assert.assertTrue(targetHatContainsNumberOfLeafNodesEqualToNumberTargetWords(targetString, treeGrower));
		} catch (Exception e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
