package unit_tests;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import util.Pair;
import canonicalFormAlignmentFactorization.CanonicalFormHATFactorizationLinearTime;
import canonicalFormAlignmentFactorization.CanonicalFormHATFactorizationShiftReduce;
import alignment.Alignment;

public class CanonicalFormHATFactorizationLinearTimeTest {

  // Global variable determining the verboisty of the CanonicalFormPETFactorization algorithm
  // tested
  private static final int TEST_VERBOSITY = 1;
  
	@Test
	public void testLininearTimeAlgorithm() {
		Alignment alignment = Alignment.createAlignment("0-5 1-4 1-6 2-3 3-0 3-2 4-1 5-0 5-2");
		int sourceLength = 6;
		int targetLength = 7;

		CanonicalFormHATFactorizationLinearTime canonicalFormHATFactorizationLinearTime = CanonicalFormHATFactorizationLinearTime
				.createCanonicialFormHATFactorizationLinearTime(alignment, sourceLength,
						targetLength,TEST_VERBOSITY);
		Set<Pair<Integer>> reductions = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorizationLinearTime.linearTimeAlgorithm());

		System.out.println("Reductions: " + reductions);
		Set<Pair<Integer>> expextedReductions = new HashSet<Pair<Integer>>();
		// (0, 0), (0, 1), (2, 2), (0, 2), (4, 4), (3, 5), (0, 5)
		expextedReductions.add(new Pair<Integer>(0, 0));
		expextedReductions.add(new Pair<Integer>(0, 1));
		expextedReductions.add(new Pair<Integer>(2, 2));
		expextedReductions.add(new Pair<Integer>(0, 2));
		expextedReductions.add(new Pair<Integer>(4, 4));
		expextedReductions.add(new Pair<Integer>(3, 5));
		expextedReductions.add(new Pair<Integer>(0, 5));

		Assert.assertEquals(expextedReductions, reductions);
	}

	@Test
	public void testShiftReduceAlgorithm3() {

		// 0,2,4,1,3,5,7,9,6,8]
		Alignment alignment = Alignment.createAlignment("0-0 1-2 2-4 3-1 4-3 5-5 6-7 7-9 8-6 9-8");
		CanonicalFormHATFactorizationLinearTime canonicalFormHATFactorization = CanonicalFormHATFactorizationLinearTime
				.createCanonicialFormHATFactorizationLinearTime(alignment, 10, 10,TEST_VERBOSITY);
		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.linearTimeAlgorithm());

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
		for (int i = 0; i < 10; i++) {
			expectedFactorization.add(new Pair<Integer>(i, i));
		}

		// The two reductions corresponding to the un-reducable
		// 4-element permutations contained in the permutation
		expectedFactorization.add(new Pair<Integer>(1, 4));
		// (Found during merge(0,7,9)
		expectedFactorization.add(new Pair<Integer>(6, 9));

		// Two reductions found when calling merge on range 0-7
		expectedFactorization.add(new Pair<Integer>(0, 4));
		expectedFactorization.add(new Pair<Integer>(0, 5));

		// Final reduction found when merging 0-9
		expectedFactorization.add(new Pair<Integer>(0, 9));
		System.out.println("result: " + sourcePhraseRanges);
		System.out.println("expected reductions: " + expectedFactorization);
		Assert.assertEquals(expectedFactorization, sourcePhraseRanges);
	}

	@Test
	public void testShiftReduceAlgorithm4() {

		// 0-0 1-1 1-3 2-2 3-2 4-4
		Alignment alignment = Alignment.createAlignment("0-0 1-1 1-3 2-2 3-2 4-4");
		CanonicalFormHATFactorizationLinearTime canonicalFormHATFactorization = CanonicalFormHATFactorizationLinearTime
				.createCanonicialFormHATFactorizationLinearTime(alignment, 5, 5,TEST_VERBOSITY);
		Set<Pair<Integer>> sourcePhraseRanges = new HashSet<Pair<Integer>>(
				canonicalFormHATFactorization.linearTimeAlgorithm());

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();

		expectedFactorization.add(new Pair<Integer>(0, 0));
		expectedFactorization.add(new Pair<Integer>(4, 4));
		expectedFactorization.add(new Pair<Integer>(0, 3));
		expectedFactorization.add(new Pair<Integer>(2, 3));
		expectedFactorization.add(new Pair<Integer>(1, 3));
		expectedFactorization.add(new Pair<Integer>(0, 4));

		System.out.println(sourcePhraseRanges);
		Assert.assertEquals(expectedFactorization, sourcePhraseRanges);
	}

}
