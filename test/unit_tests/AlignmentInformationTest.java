package unit_tests;

import junit.framework.Assert;
import hat_lexicalization.AlignmentInformationCompactNulls;

import org.junit.Test;

import util.InputReading;

public class AlignmentInformationTest {

	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }

	
	@Test
	public void testCreateAlignmentInformation() 
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 t5 t-6";
		String alignmentString = "0-0 2-2 2-3 3-2 3-3" ;

		AlignmentInformationCompactNulls alignmentInformation = AlignmentInformationCompactNulls.createAlignmentInformationCompactedNulls(alignmentString,InputReading.getWordsFromString(sourceString).size(), InputReading.getWordsFromString(targetString).size());
		Assert.assertEquals("0-0 1-1 1-2 2-1 2-2",alignmentInformation.getCompactedAlignmentChunking().getAlignmentsAsString());
		
	}
	
	

}
