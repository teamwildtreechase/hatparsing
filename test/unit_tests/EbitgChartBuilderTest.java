package unit_tests;

import java.util.List;
import java.util.Set;
import junit.framework.Assert;
import util.Span;
import org.junit.Test;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgInference;
import extended_bitg.EbitgInference.InferenceType;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeState;
import extended_bitg.EbitgTreeStateBasic;

public class EbitgChartBuilderTest {

	@Test
	public void test() {
		String sourceString = "s1 s2";
		String targetString = "t1 t2";
		String alignmentString = "0-0";

		EbitgChartBuilder<EbitgLexicalizedInference, EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic
				.createEbitgChartBuilder(sourceString, targetString, alignmentString, 100000, true, true);
		chartBuilder.findDerivationsAlignment();
		chartBuilder.printChartContents();

		EbitgInference inference = chartBuilder.getTightPhrasePair(new Span(0, 0));
		EbitgLexicalizedInference lexicalizedInference = chartBuilder.getLexicalizer().createLexicalizedInference(inference);
		System.out.println("lexicalized inference: " + lexicalizedInference);
		Assert.assertTrue(lexicalizedInference.getInferenceLengthProperties().getSourceSpanLength() == 2);
		assert (lexicalizedInference.getBoundTargetPositions().size() == 2);

		EbitgTreeGrower<EbitgLexicalizedInference, EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
		EbitgTreeState<EbitgLexicalizedInference, EbitgTreeStateBasic> rootState = tg.getRootState();
		System.out.println("root state: " + rootState);
		System.out.println("Tree String: " + tg.getTreeString());
		System.out.println("Twin Tree String: " + tg.getTwinTreeString());

	}

	@Test
	public void test2() {
		String sourceString = "s1 s2";
		String targetString = "t1 t2";
		String alignmentString = "0-0";

		EbitgChartBuilder<EbitgLexicalizedInference, EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createExplicitNullsHATsEbitgChartBuilder(sourceString, targetString, alignmentString,
				100000, true, true);
		chartBuilder.findDerivationsAlignment();
		chartBuilder.printChartContents();

		EbitgInference inference = chartBuilder.getTightPhrasePair(new Span(0, 0));
		EbitgLexicalizedInference lexicalizedInference = chartBuilder.getLexicalizer().createLexicalizedInference(inference);
		System.out.println("lexicalized inference: " + lexicalizedInference);
		Assert.assertEquals(1, lexicalizedInference.getInferenceLengthProperties().getSourceSpanLength());
		assert (lexicalizedInference.getBoundTargetPositions().size() == 1);

		EbitgTreeGrower<EbitgLexicalizedInference, EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
		EbitgTreeState<EbitgLexicalizedInference, EbitgTreeStateBasic> rootState = tg.getRootState();
		System.out.println("root state: " + rootState);
		System.out.println("Tree String: " + tg.getTreeString());
		System.out.println("Twin Tree String: " + tg.getTwinTreeString());

	}

	private void assertAtomicInferenceHasSPermutationWithOneElementForOneToOneAlignment(EbitgLexicalizedInference atomicInference) {
		List<Set<Integer>> sourcePermutation = atomicInference.getUnsortedSourceSetPermutation();
		Assert.assertEquals(1, EbitgInferencePermutationPropertiesTest.getAllElementsFromListOfSets(sourcePermutation).size());
	}

	@Test
	public void testPermutations() {
		System.out.println("\n<testPermutations>");
		String sourceString = "s1 s2 s3";
		String targetString = "t1 t2 t3";
		String alignmentString = "0-0 1-2 2-1";

		EbitgChartBuilder<EbitgLexicalizedInference, EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createExplicitNullsHATsEbitgChartBuilder(sourceString, targetString, alignmentString,
				100000, true, true);
		chartBuilder.findDerivationsAlignment();
		// chartBuilder.printChartContents();

		for (int spanLength = 1; spanLength <= chartBuilder.getChartSourceLength(); spanLength++) {
			for (int spanBegin = 0; spanBegin < chartBuilder.getChartSourceLength() - spanLength + 1; spanBegin++) {
				Span sourceSpan = new Span(spanBegin, spanBegin + spanLength - 1);
				EbitgLexicalizedInference lexicalizedInference = chartBuilder.getTightPhrasePairForChartSpan(sourceSpan);

				if (lexicalizedInference != null) {
					System.out.println("Source span: " + sourceSpan);
					System.out.println("lexicalizedInference.getSortedPermuation().toString()" + lexicalizedInference.getSortedSourceSetPermuation().toString());
					System.out.println("lexicalizedInference.getSourcePermutation().toString()" + lexicalizedInference.getUnsortedSourceSetPermutation().toString());
					Assert.assertTrue(lexicalizedInference.getSortedSourceSetPermuation().toString().equals(lexicalizedInference.getUnsortedSourceSetPermutation().toString()));

					if (lexicalizedInference.getInferenceType().equals(InferenceType.Atomic)) {
						assertAtomicInferenceHasSPermutationWithOneElementForOneToOneAlignment(lexicalizedInference);
					}
				}
			}
		}
		System.out.println("\n</testPermutations>");
	}

	/*
	 * @Test public void test2() { String sourceString = "s1 s2"; String targetString = "t1 t2 t3"; String alignmentString = "0-0 1-2";
	 * 
	 * EbitgChartBuilder chartBuilder = EbitgChartBuilder.createEbitgChartBuilder(sourceString, targetString, alignmentString, 100000, true);
	 * chartBuilder.findDerivationsAlignment(); chartBuilder.printChartContents();
	 * 
	 * 
	 * System.out.println("Source Right bound nulls: " + chartBuilder.getLexicalizer().getAlignmentInformation().getSourceRightBoundNulls(0));
	 * 
	 * EbitgInference inference = chartBuilder.getChart().getChartEntry(0, 0).getMinimumTargetPositionsBindingInferences().get(0); EbitgLexicalizedInference
	 * lexicalizedInference = chartBuilder.getLexicalizer().createLexicalizedInference(inference); System.out.println("lexicalized inference: " +
	 * lexicalizedInference); Assert.assertTrue(lexicalizedInference.getSourceSpanLength() == 1);
	 * 
	 * EbitgTreeGrower tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder); EbitgTreeState rootState = tg.getRootState(); System.out.println("root state: "
	 * + rootState); System.out.println("rootState.getChosenInference(): " + rootState.getChosenInference()); System.out.println("Tree String: " +
	 * tg.getTreeString()); System.out.println("Twin Tree String: " + tg.getTwinTreeString());
	 * 
	 * }
	 */
}
