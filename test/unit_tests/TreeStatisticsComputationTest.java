package unit_tests;

import org.junit.Assert;
import org.junit.Test;

import alignmentStatistics.EbitgTreeStatisticsComputation;
import alignmentStatistics.SourceTreeStatisticsComputation;
import alignmentStatistics.SymmetricTreeStatisticsComputation;
import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeGrower;
import extended_bitg.EbitgTreeStateBasic;
import extended_bitg.EbitgTreeState.TreeType;


public class TreeStatisticsComputationTest 
{
	private static final boolean USE_GREEDY_NULL_BINDING = false;
	private static final boolean USE_CACHING = true;

	private final EbitgTreeStatisticsComputation symmetricComputation = new SymmetricTreeStatisticsComputation(true);
	private final EbitgTreeStatisticsComputation sourceComputation = new SourceTreeStatisticsComputation(true);
	
	
	  @Test(expected=AssertionError.class)
	  public void testAssertionsEnabled() 
	  {
	    assert(false);
	  }
	
	
	public void symmetricMaxBranchingFactorHasValue(EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg, int maxSourceLengthAtomicFragment, int expected)
	{
		int observed = symmetricComputation.determineMaximalOccuringBranching(tg, maxSourceLengthAtomicFragment);
		Assert.assertEquals(expected,observed);
	}

	public void sourceMaxBranchingFactorHasValue(EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg, int maxSourceLengthAtomicFragment, int expected)
	{
		int observed = sourceComputation.determineMaximalOccuringBranching(tg, maxSourceLengthAtomicFragment);
		Assert.assertEquals(expected,observed);
	}
	
	public void sourceHasTreeType(EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg, int maxSourceLengthAtomicFragment, TreeType expected)
	{
		TreeType treeType = sourceComputation.determineTreeType(tg, maxSourceLengthAtomicFragment);
		Assert.assertEquals(expected,treeType);
	}
	
	 @Test
	public void testSymmetricTreeStatisticsComputation()
	{
		
		final String sourceString = "1 2 3 4";
		final String targetString = "1 2 3 4 5";
		final String alignmentString = "0-0 1-1 1-2 1-3 2-4 3-4";
		
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, 50000,USE_GREEDY_NULL_BINDING,USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
		sourceMaxBranchingFactorHasValue(tg, 1, 2);
		symmetricMaxBranchingFactorHasValue(tg, 1, 2);
		sourceHasTreeType(tg, 1, TreeType.BITT);
	}

	 
	 @Test
	public void testAlignmentClassificationBonBon()
	{
		
		 // A "Bonbon" alignment according to Sogaards terminology 
		final String sourceString = "1 2 3 4 5";
		final String targetString = "1 2 3 4 5";
		final String alignmentString = "0-0 3-2 3-4 2-3 4-3";
		
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, 50000,USE_GREEDY_NULL_BINDING,USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
		
		// Bonbon should give rise to BITT for continuous translation equivalents definition
		boolean atomicPartsMustAllShareSameAlignmentChunk = false;
		SymmetricTreeStatisticsComputation stc1 = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);
		Assert.assertEquals(TreeType.BITT,stc1.determineTreeType(tg, 0));

		//Bonbon should give rise to HAT for discontinuous translation equivalents definition
		atomicPartsMustAllShareSameAlignmentChunk = true;
		SymmetricTreeStatisticsComputation stc2 = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);
		Assert.assertEquals(TreeType.HAT,stc2.determineTreeType(tg, 0));
	}
	
	 @Test
	public void testAlignmentClassificationCrossSerial()
	{
		
		 // A "Cross Serial" alignment according to Sogaards terminology 
		final String sourceString = "1 2 3 4";
		final String targetString = "1 2 3 4 5 6";
		final String alignmentString = "0-0 2-2 2-4 3-3 3-5";
		
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, 50000,USE_GREEDY_NULL_BINDING,USE_CACHING);
		chartBuilder.findDerivationsAlignment();
		EbitgTreeGrower<EbitgLexicalizedInference,EbitgTreeStateBasic> tg = EbitgTreeGrower.createEbitgTreeGrower(chartBuilder);
		
		// Cross Serial should give rise to BITT for continuous translation equivalents definition
		boolean atomicPartsMustAllShareSameAlignmentChunk = false;
		SymmetricTreeStatisticsComputation stc1 = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);
		Assert.assertEquals(TreeType.BITT,stc1.determineTreeType(tg, 0));

		//Cross Serial should give rise to HAT for discontinuous translation equivalents definition
		atomicPartsMustAllShareSameAlignmentChunk = true;
		SymmetricTreeStatisticsComputation stc2 = new SymmetricTreeStatisticsComputation(atomicPartsMustAllShareSameAlignmentChunk);
		Assert.assertEquals(TreeType.HAT,stc2.determineTreeType(tg, 0));
	}
	 
	 

}
