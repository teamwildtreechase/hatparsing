package unit_tests;

import junit.framework.Assert;
import org.junit.Test;
import alignmentStatistics.MultiFolderFilePathListCreater;

public class DataPropertiesLatexTableCreaterTest {


	@Test(expected=AssertionError.class)
	public void testAssertionsEnabled() 
	{
		assert(false);
	}
	
	@Test
	public void testGetLanguagePairStringFromSubFolderName() 
	{
		String subFolderName = "treeStatistics-LREC2008-EN-FR-SureOnly2011-10-05";
		MultiFolderFilePathListCreater multiFolderFilePathListCreater = MultiFolderFilePathListCreater.createCorpusPropertiesPathListCreater();
		String languagePairName = multiFolderFilePathListCreater.getLanguagePairStringFromSubFolderPath(subFolderName);
		Assert.assertEquals("EN-FR", languagePairName);
	}

	@Test
	public void testGetLanguagePairStringFromSubFolderNameThrowsProperExceptions() 
	{
		String subFolderName = "treeStatistics-LREC2008-EN-FR-SureOnly2011-EN-FR-10-05";
		
		boolean causedEcxception = false;
		try
		{
			MultiFolderFilePathListCreater multiFolderFilePathListCreater = MultiFolderFilePathListCreater.createCorpusPropertiesPathListCreater();
			String languagePairName = multiFolderFilePathListCreater.getLanguagePairStringFromSubFolderPath(subFolderName);
			Assert.assertEquals("EN-FR", languagePairName);
		}
		catch(RuntimeException exception)
		{
			System.out.println(exception);
			causedEcxception = true;
		}
		Assert.assertEquals(true, causedEcxception);
	}
	
}
