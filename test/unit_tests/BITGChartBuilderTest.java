/**
 * 
 */
package unit_tests;

import util.Pair;
import bitg.BITGChartEntry;
import bitg.EntryIndicesIterator;
import bitg.BITGChartBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author gemaille
 *
 */
public class BITGChartBuilderTest {
	
	
	BITGChartBuilder b2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
	
		b2 = new BITGChartBuilder(new int[]{6,5,7,4,3,1,2});
		b2.findDerivationsPermutation();
	
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link bitg.BITGChartBuilder#findDerivationsPermutation()}.
	 */
	@Test
	public void testFindDerivationsPermutation() 
	{
		System.out.println("\n>>>Executing testFindDerivationsPermutation...");
		boolean result = testChartConsistencyShallow(b2);
		assert(result);
	}

	
	
	
	/**
	 * Test method for {@link bitg.BITGChartBuilder#findDerivationsForChartEntry(int, int)}.
	 */
	@Test
	public void testFindDerivationsForChartEntry() 
	{
		System.out.println("\n>>>Executing testFindDerivationsForChartEntry...");
		assert(testEntryConsistencyShallow(b2, 0, b2.getSourceLength() - 1));
		assert(testEntryConsistencyShallow(b2, 0, 1));
		assert(testEntryConsistencyShallow(b2, b2.getSourceLength() - 1,  b2.getSourceLength() - 1));
		System.out.println("Test passed succesfully");
	}
	
	// Test the chart consistency by checking for all the chart entries shallowly if they are consistent
	public boolean testChartConsistencyShallow(BITGChartBuilder bitg)
	{
		
		System.out.println("Performing shallow chart consistency check...");
		
		for(int spanLength = 1; spanLength <= bitg.getSourceLength(); spanLength++)
		{
			//System.out.println("\nEntries with spanLength: "+ spanLength);
			
			for(int beginIndex = 0; beginIndex < (bitg.getSourceLength() - spanLength + 1); beginIndex++)
			{	 
			
				int endIndex = beginIndex + spanLength - 1;
				
				//Check if specific chart entry is consistent, and if not return false for the test
				if(!testEntryConsistencyShallow(bitg,beginIndex, endIndex))
				{
					return false;
				}
			}
		}
		
		System.out.println("Test passed succesfully");
		return true;
	}	
	
	public boolean testEntryConsistencyShallow(BITGChartBuilder bitg, int i, int j)
	{
		
		boolean entryListedDerivable = bitg.getChartEntry(i,j).isDerivable();
		boolean derivationFound = false;
		
		
		// block for simple entries with span 1
		if(i == j)
		{
			if(!entryListedDerivable)
			{
				String errormessage = "Error: inconsistency found. Entry["+ i + "," + j + "] listed as un-derivable but should be " +
				" derivable because it is an atomic unit with span length 1"; 
				
				assert(entryListedDerivable) : errormessage;
				System.out.println(errormessage);
				

			}
		}
		
		// block for complex entries with span > 1
		else
		{
			for(int splitPoint = i; splitPoint < j; splitPoint++)
			{
				BITGChartEntry leftPart = bitg.getChartEntry(i,splitPoint);
				BITGChartEntry rightPart = bitg.getChartEntry(splitPoint + 1,j);
			
				
		
				if(leftPart.isDerivable() && rightPart.isDerivable())
				{		
					
					int  adjacency = bitg.determineAdjacency(leftPart, rightPart);
					
					// all conditions for valid derivation met: left and right part derivable, and parts are adjacent
					if(adjacency >= 0)
					{	
						// Error type 1: Derivation has been found, but the entry is listed as underivable
						if(!entryListedDerivable)
						{
							String errormessage =  "Error: inconsistency found. Entry["+ i + "," + j + "] listed as un-derivable" +
							"\nBut derivation was found for splitpoint= " + splitPoint + " with left part :" +
							leftPart.toString() + "\n and right part:\n " + rightPart.toString();
							
							assert(entryListedDerivable) : errormessage;
							System.out.println(errormessage);
							return false;
						}
						
						derivationFound = true;
					}
				}
			}
			
			// Error type 2: listed as derivable but no derivation can be found
			if((!derivationFound) && entryListedDerivable)
			{
				String errormessage = "Error: inconsistency found. Entry["+ i + "," + j + "] listed as derivable" +
				"\nBut no possible derivation was found";
				
				assert(derivationFound || (!entryListedDerivable)) : errormessage;
				System.out.println(errormessage);
				
				return false;
			}
		}	
		
		// Everything went as it should
		return true;
	}

	/**
	 * Test method for {@link bitg.BITGChartBuilder#findDerivationsPermutation()}.
	 */
	@Test
	public void testChartWithUnderivabilityCheck() 
	{
		
		System.out.println("\n>>>Executing testChartWithUnderivabilityCheck...");
		System.out.println("Performing  length 2 underivability check...");
		// At bracketing level 2 none of these numbers can be bracketed, so there should be no derivation and 
		// no derivable entry of span length 2 either
		int[] permutation = new int[]{4,1,3,5,2}; 
		BITGChartBuilder b2 = new BITGChartBuilder(permutation);
		
		b2.findDerivationsPermutation();
		//b2. printChartContents();
		
		EntryIndicesIterator iterator = new EntryIndicesIterator(5,2,2);
		
		while(iterator.hasMoreElements())
		{
			Pair<Integer> indexPair = iterator.nextElement();
			
			BITGChartEntry entry = b2.getChartEntry(indexPair.getFirst(), indexPair.getSecond());
			if(entry.isDerivable())
			{
				String errormessage = "Error: Entry " + b2.chartEntryStringExtra(entry) + "is wrongly listed as derivable";
				
				
				assert(!entry.isDerivable()): errormessage;
				System.out.println(errormessage); 
			}
		}
		
		System.out.println("Test passed succesfully");
		
	}
	
}
