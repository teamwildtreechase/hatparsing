package unit_tests;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import util.Pair;
import canonicalFormAlignmentFactorization.CanonicFormPETFactorization;

public class CanonicFormPETFactorizationTest {
	
	// Global variable determining the verboisty of the CanonicalFormPETFactorization algorithm
	// tested
	private static final int TEST_VERBOSITY = 3;

	@Test
	public void testGetMinimalFactorization() {
		CanonicFormPETFactorization canonicFormPETFactorization = CanonicFormPETFactorization
				.createCanonicFormPETFactorizationFromPermutationString("[6,0,3,5,2,4,7,1]",TEST_VERBOSITY);
		Set<Pair<Integer>> minimalFactorization = canonicFormPETFactorization
				.getMinimalFactorizationAsSetOfMinimalReductionRanges();

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
	
		// Add valid reduction range 2-5
		expectedFactorization.add(new Pair<Integer>(2, 5));
		// Add valid reduction range 0-7 (entire range)
		expectedFactorization.add(new Pair<Integer>(0, 7));

		// Test that obtained factorization matches expeted factorization
		Assert.assertEquals(expectedFactorization, minimalFactorization);

	}
	
	@Test
	public void testGetMinimalFactorization2() {
		CanonicFormPETFactorization canonicFormPETFactorization = CanonicFormPETFactorization
				.createCanonicFormPETFactorizationFromPermutationString("[0,1,2,3]",TEST_VERBOSITY);
		Set<Pair<Integer>> minimalFactorization = canonicFormPETFactorization
				.getMinimalFactorizationAsSetOfMinimalReductionRanges();

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
	
		expectedFactorization.add(new Pair<Integer>(0, 1));
		expectedFactorization.add(new Pair<Integer>(2, 3));
		expectedFactorization.add(new Pair<Integer>(0, 3));

		// Test that obtained factorization matches expeted factorization
		Assert.assertEquals(expectedFactorization, minimalFactorization);
	}
	
	@Test
	public void testGetMinimalFactorization3() {
		CanonicFormPETFactorization canonicFormPETFactorization = CanonicFormPETFactorization
				.createCanonicFormPETFactorizationFromPermutationString("[3,2,1,0]",TEST_VERBOSITY);
		Set<Pair<Integer>> minimalFactorization = canonicFormPETFactorization
				.getMinimalFactorizationAsSetOfMinimalReductionRanges();

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
	
		expectedFactorization.add(new Pair<Integer>(0, 1));
		expectedFactorization.add(new Pair<Integer>(2, 3));
		expectedFactorization.add(new Pair<Integer>(0, 3));

		// Test that obtained factorization matches expeted factorization
		Assert.assertEquals(expectedFactorization, minimalFactorization);
	}
	
	@Test
	public void testGetMinimalFactorization4() {
		CanonicFormPETFactorization canonicFormPETFactorization = CanonicFormPETFactorization
				.createCanonicFormPETFactorizationFromPermutationString("[0,2,4,1,3,5,7,9,6,8]",TEST_VERBOSITY);
		Set<Pair<Integer>> minimalFactorization = canonicFormPETFactorization
				.getMinimalFactorizationAsSetOfMinimalReductionRanges();

		Set<Pair<Integer>> expectedFactorization = new HashSet<Pair<Integer>>();
	
		
		// The two reductions corresponding to the un-reducable
		// 4-element permutations contained in the permutation
		expectedFactorization.add(new Pair<Integer>(1, 4));
		// (Found during merge(0,7,9)
		expectedFactorization.add(new Pair<Integer>(6, 9));
		
		// Two reductions found when calling merge on range 0-7
		expectedFactorization.add(new Pair<Integer>(0, 4));
		expectedFactorization.add(new Pair<Integer>(0, 5));
		
		// Final reduction found when merging 0-9
		expectedFactorization.add(new Pair<Integer>(0, 9));
		

		// Test that obtained factorization matches expeted factorization
		Assert.assertEquals(expectedFactorization, minimalFactorization);
	}



	@Test
	public void testReorderRangeWithProperPermutation() {
		List<Integer> integerList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7);
		List<Integer> permutation = Arrays.asList(1, 2, 3, 0);
		CanonicFormPETFactorization.reorderRangeWithProperPermutation(integerList, permutation, 4,
				7);
		List<Integer> expextedList = Arrays.asList(0, 1, 2, 3, 5, 6, 7, 4);
		Assert.assertEquals(expextedList, integerList);

	}

	@Test
	public void testComputeUnshiftedPermutation() {
		List<Integer> permutation = Arrays.asList(3, 5, 8, 2);
		List<Integer> expextedList = Arrays.asList(1, 2, 3, 0);
		List<Integer> shiftedPermutation = CanonicFormPETFactorization
				.computeUnShiftedPermutation(permutation);
		Assert.assertEquals(expextedList, shiftedPermutation);
	}

}
