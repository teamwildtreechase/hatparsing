package unit_tests;

import junit.framework.Assert;

import org.junit.Test;

import extended_bitg.EbitgChartBuilder;
import extended_bitg.EbitgChartBuilderBasic;
import extended_bitg.EbitgLexicalizedInference;
import extended_bitg.EbitgTreeStateBasic;

public class EbitgChartInitializerGreedyNullBindingsTest 
{
	final static int MaxAllowedInferencesPerNode = 100000;
	private static final boolean USE_CACHING = true;

	@Test
	public void testBorderNullBinding() 
	{
		String sourceString = "s1 s2 s3 s4";
		String targetString = "t1 t2 t3 t4 t5";
		String alignmentString = "0-0 1-1 1-2 1-3 2-4";
		
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, MaxAllowedInferencesPerNode, true,USE_CACHING);
		boolean result = chartBuilder.findDerivationsAlignment();
		
		Assert.assertEquals(true,result);
	}

	@Test
	public void testBorderNullBinding1() 
	{
		String sourceString = "s1 s2 s3 s4 s5";
		String targetString = "t1 t2 t3 t4 s5";
		String alignmentString = "1-1 3-3";
		
		EbitgChartBuilder<EbitgLexicalizedInference,EbitgTreeStateBasic> chartBuilder = EbitgChartBuilderBasic.createEbitgChartBuilder(sourceString, targetString, alignmentString, MaxAllowedInferencesPerNode, true,USE_CACHING);
		boolean result = chartBuilder.findDerivationsAlignment();
		
		Assert.assertEquals(true,result);
	}
	
	

}
